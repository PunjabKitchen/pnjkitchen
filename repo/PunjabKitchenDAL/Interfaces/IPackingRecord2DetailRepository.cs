﻿using PunjabKitchenEntities.EntityModel;
using PunjabKitchenEntities.Interfaces;

namespace PunjabKitchenDAL.Interfaces
{
    public interface IPackingRecord2DetailRepository : IGenericRepository<tPackingRecord2Detail>
    {
        #region Get By ID
        /// <summary>
        /// Get By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>tPackingRecord2Detail</returns>
        tPackingRecord2Detail GetById(long id); 
        #endregion
    }
}
