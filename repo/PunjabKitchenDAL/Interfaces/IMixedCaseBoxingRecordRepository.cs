﻿using PunjabKitchenEntities.EntityModel;
using PunjabKitchenEntities.Interfaces;

namespace PunjabKitchenDAL.Interfaces
{
    public interface IMixedCaseBoxingRecordRepository : IGenericRepository<tMixedCaseBoxingRecord>
    {
        #region GetById
        /// <summary>
        /// GetById
        /// </summary>
        /// <param name="id"></param>
        /// <returns>tMixedCaseBoxingRecord</returns>
        tMixedCaseBoxingRecord GetById(long id); 
        #endregion
    }
}
