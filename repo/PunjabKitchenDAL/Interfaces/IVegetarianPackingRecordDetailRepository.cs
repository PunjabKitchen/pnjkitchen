﻿using PunjabKitchenEntities.EntityModel;
using PunjabKitchenEntities.Interfaces;

namespace PunjabKitchenDAL.Interfaces
{
    public interface IVegetarianPackingRecordDetailRepository : IGenericRepository<tVegetarianPackingRecordDetail>
    {
        #region GetById
        /// <summary>
        /// GetById
        /// </summary>
        /// <param name="id"></param>
        /// <returns>tVegetarianPackingRecordDetail</returns>
        tVegetarianPackingRecordDetail GetById(long id); 
        #endregion
    }
}