﻿using System.Collections.Generic;
using PunjabKitchenEntities.EntityModel;
using PunjabKitchenEntities.Interfaces;

namespace PunjabKitchenDAL.Interfaces
{
    public interface IProductRepository : IGenericRepository<tProduct>
    {
        #region GetById
        /// <summary>
        /// GetById
        /// </summary>
        /// <param name="id"></param>
        /// <returns>tProduct</returns>
        tProduct GetById(long id);
        #endregion

        #region GetByCode
        /// <summary>
        /// GetByCode
        /// </summary>
        /// <param name="code"></param>
        /// <returns>IEnumerable of tProduct</returns>
        IEnumerable<tProduct> GetByCode(string code);
        #endregion

        #region Product Code
        /// <summary>
        /// GetProductCode
        /// </summary>
        /// <param name="productTypeId"></param>
        /// <param name="productCodePrefix"></param>
        /// <returns>string</returns>
        string GetProductCode(int productTypeId, string productCodePrefix);

        /// <summary>
        /// ValidateCode
        /// </summary>
        /// <param name="code"></param>
        /// <returns>bool</returns>
        bool ValidateCode(string code); 
        #endregion
    }
}
