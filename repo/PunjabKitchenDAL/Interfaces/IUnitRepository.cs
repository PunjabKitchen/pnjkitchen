﻿
using PunjabKitchenEntities.EntityModel;
using PunjabKitchenEntities.Interfaces;

namespace PunjabKitchenDAL.Interfaces
{
    public interface IUnitRepository : IGenericRepository<tUnit>
    {
        #region GetById
        /// <summary>
        /// GetById
        /// </summary>
        /// <param name="id"></param>
        /// <returns>tUnit</returns>
        tUnit GetById(long id); 
        #endregion
    }
}