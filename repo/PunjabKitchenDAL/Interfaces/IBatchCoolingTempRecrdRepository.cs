﻿using PunjabKitchenEntities.EntityModel;
using PunjabKitchenEntities.Interfaces;

namespace PunjabKitchenDAL.Interfaces
{
    public interface IBatchCoolingTempRecrdRepository : IGenericRepository<tBatchCoolingTempRecrd>
    {
        #region Get By ID
        /// <summary>
        /// Get By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>tBatchCoolingTempRecrd</returns>
        tBatchCoolingTempRecrd GetById(long id); 
        #endregion
    }
}