﻿using PunjabKitchenEntities.EntityModel;
using PunjabKitchenEntities.Interfaces;

namespace PunjabKitchenDAL.Interfaces
{
    public interface INotificationTypeRepository : IGenericRepository<tNotificationType>
    {
    }
}
