﻿using System;
using System.Collections.Generic;
using PunjabKitchenEntities.EntityModel;
using PunjabKitchenEntities.Interfaces;

namespace PunjabKitchenDAL.Interfaces

{
    public interface INotificationRepository : IGenericRepository<tNotification>
    {
        #region GetById
        /// <summary>
        /// GetById
        /// </summary>
        /// <param name="id"></param>
        /// <returns>tNotification</returns>
        tNotification GetById(long id);
        #endregion

        #region GetNotificationsCount
        /// <summary>
        /// GetNotificationsCount
        /// </summary>
        /// <returns>int</returns>
        int GetNotificationsCount();
        #endregion

        #region CanAssignNotificationToUser
        /// <summary>
        /// CanAssignNotificationToUser
        /// </summary>
        /// <param name="notificationId"></param>
        /// <returns>bool</returns>
        bool CanAssignNotificationToUser(int notificationId);
        #endregion

        #region Get Notifications
        /// <summary>
        /// GetNotificationByDate
        /// </summary>
        /// <param name="dateTime"></param>
        /// <returns>IEnumerable of tNotification</returns>
        IEnumerable<tNotification> GetNotificationByDate(DateTime dateTime);

        /// <summary>
        /// GetLatestNotificationByType
        /// </summary>
        /// <param name="notificationTypeId"></param>
        /// <returns>tNotification</returns>
        tNotification GetLatestNotificationByType(int notificationTypeId); 
        #endregion
    }
}
