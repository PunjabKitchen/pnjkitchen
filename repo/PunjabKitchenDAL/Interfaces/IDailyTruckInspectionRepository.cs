﻿using System;
using System.Collections.Generic;
using PunjabKitchenEntities.EntityModel;
using PunjabKitchenEntities.Interfaces;

namespace PunjabKitchenDAL.Interfaces
{
    public interface IDailyTruckInspectionRepository : IGenericRepository<tDailyTruckInspection>
    {
        #region Get By Id

        /// <summary>
        /// Get By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Daily Truck Inspection</returns>
        tDailyTruckInspection GetById(long id);
        #endregion

        #region Get Daily Truck Inspection History
        IEnumerable<tDailyTruckInspection> GetDailyTruckInspectionHistory(DateTime startDateTime, DateTime endDateTime);
        #endregion

        #region Get Daily Truck Inspection History By Id
        /// <summary>
        /// Get Daily Truck Inspection History By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>tDailyTruckInspection</returns>
        tDailyTruckInspection GetDailyTruckInspectionHistoryById(long id);
        #endregion
    }
}
