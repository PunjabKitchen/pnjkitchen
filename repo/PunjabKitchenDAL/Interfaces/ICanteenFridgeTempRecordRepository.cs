﻿using System;
using System.Collections.Generic;
using PunjabKitchenEntities.EntityModel;
using PunjabKitchenEntities.Interfaces;

namespace PunjabKitchenDAL.Interfaces
{
    public interface ICanteenFridgeTempRecordRepository : IGenericRepository<tCanteenFridgeTempRecordDaily>
    {
        #region Get By Id
        /// <summary>
        /// Get By ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns>tCanteenFridgeTempRecordDaily</returns>
        tCanteenFridgeTempRecordDaily GetById(long id);
        #endregion

        #region GetCanteenFridgeTemperatureHistory
        /// <summary>
        /// GetCanteenFridgeTemperatureHistory
        /// </summary>
        /// <param name="startDateTime"></param>
        /// <param name="endDateTime"></param>
        /// <returns>IEnumerable of tCanteenFridgeTempRecordDaily</returns>
        IEnumerable<tCanteenFridgeTempRecordDaily> GetCanteenFridgeTemperatureHistory(DateTime startDateTime, DateTime endDateTime);
        #endregion

        #region GetCanteenFridgeChecklistHistoryById
        /// <summary>
        /// GetCanteenFridgeChecklistHistoryById
        /// </summary>
        /// <param name="id"></param>
        /// <returns>tCanteenFridgeTempRecordDaily</returns>
        tCanteenFridgeTempRecordDaily GetCanteenFridgeChecklistHistoryById(long id); 
        #endregion
    }
}
