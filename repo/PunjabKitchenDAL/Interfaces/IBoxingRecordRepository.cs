﻿using PunjabKitchenEntities.EntityModel;
using PunjabKitchenEntities.Interfaces;

namespace PunjabKitchenDAL.Interfaces
{
    public interface IBoxingRecordRepository : IGenericRepository<tBoxingRecord>
    {
        #region Get By Id
        /// <summary>
        /// Get By ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns>tBoxingRecord</returns>
        tBoxingRecord GetById(long id);
        #endregion

        #region Batch COde
        /// <summary>
        /// Get Batch Code
        /// </summary>
        /// <returns>string</returns>
        string GetBatchCode();


        /// <summary>
        /// ValidateBatchCode
        /// </summary>
        /// <param name="batchCode"></param>
        /// <returns>bool</returns>
        bool ValidateBatchCode(string batchCode);
        #endregion
    }
}
