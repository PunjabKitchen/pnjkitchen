﻿using PunjabKitchenEntities.EntityModel;
using PunjabKitchenEntities.Interfaces;

namespace PunjabKitchenDAL.Interfaces
{
    public interface IPortionWeightsCheckRecordRepository : IGenericRepository<tPortionWeightCheck>
    {
        #region GetById
        /// <summary>
        /// GetById
        /// </summary>
        /// <param name="id"></param>
        /// <returns>tPortionWeightCheck</returns>
        tPortionWeightCheck GetById(long id); 
        #endregion
    }
}
