﻿using PunjabKitchenEntities.EntityModel;
using PunjabKitchenEntities.Interfaces;

namespace PunjabKitchenDAL.Interfaces
{
    public interface IColdDessertPackingRecordRepository : IGenericRepository<tColdDesertPackingRecord>
    {
        #region Get By Id
        /// <summary>
        /// GetById
        /// </summary>
        /// <param name="id"></param>
        /// <returns>tColdDesertPackingRecord</returns>
        tColdDesertPackingRecord GetById(long id); 
        #endregion
    }
}
