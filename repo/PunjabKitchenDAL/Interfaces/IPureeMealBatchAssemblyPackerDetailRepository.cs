﻿using PunjabKitchenEntities.EntityModel;
using PunjabKitchenEntities.Interfaces;

namespace PunjabKitchenDAL.Interfaces
{
    public interface IPureeMealBatchAssemblyPackerDetailRepository : IGenericRepository<tPureeMealBatchAssemblyPackerDetail>
    {
        #region GetById
        /// <summary>
        /// GetById
        /// </summary>
        /// <param name="id"></param>
        /// <returns>tPureeMealBatchAssemblyPackerDetail</returns>
        tPureeMealBatchAssemblyPackerDetail GetById(long id); 
        #endregion
    }
}