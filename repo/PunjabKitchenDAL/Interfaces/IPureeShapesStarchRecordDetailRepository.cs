﻿using PunjabKitchenEntities.EntityModel;
using PunjabKitchenEntities.Interfaces;

namespace PunjabKitchenDAL.Interfaces
{
    public interface IPureeShapesStarchRecordDetailRepository : IGenericRepository<tPureeShapesStarchRecordDetail>
    {
        #region GetById
        /// <summary>
        /// GetById
        /// </summary>
        /// <param name="id"></param>
        /// <returns>tPureeShapesStarchRecordDetail</returns>
        tPureeShapesStarchRecordDetail GetById(long id); 
        #endregion
    }
}


