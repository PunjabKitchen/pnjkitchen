﻿using PunjabKitchenEntities.EntityModel;
using PunjabKitchenEntities.Interfaces;

namespace PunjabKitchenDAL.Interfaces
{
    public interface IPureeMealInBlastFreezerRepository : IGenericRepository<tPureeMealInBlastFreezer>
    {
        #region GetById
        /// <summary>
        /// GetById
        /// </summary>
        /// <param name="id"></param>
        /// <returns>tPureeMealInBlastFreezer</returns>
        tPureeMealInBlastFreezer GetById(long id); 
        #endregion
    }
}