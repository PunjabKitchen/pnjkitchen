﻿using PunjabKitchenEntities.EntityModel;
using PunjabKitchenEntities.Interfaces;

namespace PunjabKitchenDAL.Interfaces
{
    public interface IShapesControlRecordDetailRepository : IGenericRepository<tShapeControlRecordDetail>
    {
        #region GetById
        /// <summary>
        /// GetById
        /// </summary>
        /// <param name="id"></param>
        /// <returns>tShapeControlRecordDetail</returns>
        tShapeControlRecordDetail GetById(long id); 
        #endregion
    }
}