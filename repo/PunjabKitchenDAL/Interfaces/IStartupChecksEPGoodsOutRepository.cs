﻿using System;
using System.Collections.Generic;
using PunjabKitchenEntities.EntityModel;
using PunjabKitchenEntities.Interfaces;

namespace PunjabKitchenDAL.Interfaces
{
    public interface IStartupChecksEPGoodsOutRepository : IGenericRepository<tStartupChecksEPGoodsOut>
    {
        #region Get By Id
        /// <summary>
        /// GetById
        /// </summary>
        /// <param name="id"></param>
        /// <returns>tStartupChecksEPGoodsOut</returns>
        tStartupChecksEPGoodsOut GetById(long id);
        #endregion

        #region Get Startup Checks EP Goods Out History
        IEnumerable<tStartupChecksEPGoodsOut> GetStartupChecksEPGoodsOutHistory(DateTime startDateTime, DateTime endDateTime);
        #endregion

        #region Get Startup Checks - EP Goods Out History By Id
        /// <summary>
        /// Get Startup Checks - EP Goods Out History By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>tGoodsOutFreezerUnit</returns>
        tStartupChecksEPGoodsOut GetStartupChecksEpGoodsOutHistoryById(long id);
        #endregion
    }
}

