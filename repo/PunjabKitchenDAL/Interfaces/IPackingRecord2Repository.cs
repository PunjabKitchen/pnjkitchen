﻿using PunjabKitchenEntities.EntityModel;
using PunjabKitchenEntities.Interfaces;

namespace PunjabKitchenDAL.Interfaces
{
    public interface IPackingRecord2Repository : IGenericRepository<tPackingRecord2>
    {
        #region Get By ID
        /// <summary>
        /// GetById
        /// </summary>
        /// <param name="id"></param>
        /// <returns>tPackingRecord2</returns>
        tPackingRecord2 GetById(long id); 
        #endregion
    }
}