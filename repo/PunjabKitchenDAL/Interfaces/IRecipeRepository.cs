﻿using System.Collections.Generic;
using PunjabKitchenEntities.EntityModel;
using PunjabKitchenEntities.Interfaces;

namespace PunjabKitchenDAL.Interfaces
{
    public interface IRecipeRepository : IGenericRepository<tRecipe>
    {
        #region GetById
        /// <summary>
        /// GetById
        /// </summary>
        /// <param name="id"></param>
        /// <returns>tRecipe</returns>
        tRecipe GetById(long id);
        #endregion

        #region GetByCode
        /// <summary>
        /// GetByCode
        /// </summary>
        /// <param name="code"></param>
        /// <returns>IEnumerable of tRecipe</returns>
        IEnumerable<tRecipe> GetByCode(string code); 
        #endregion
    }
}
