﻿using PunjabKitchenEntities.EntityModel;
using PunjabKitchenEntities.Interfaces;

namespace PunjabKitchenDAL.Interfaces
{
    public interface IPackingRecordDetailRepository : IGenericRepository<tPackingRecordDetail>
    {
        #region GetById
        /// <summary>
        /// GetById
        /// </summary>
        /// <param name="id"></param>
        /// <returns>tPackingRecordDetail</returns>
        tPackingRecordDetail GetById(long id); 
        #endregion
    }
}