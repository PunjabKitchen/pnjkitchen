﻿using System;
using System.Collections.Generic;
using PunjabKitchenEntities.EntityModel;
using PunjabKitchenEntities.Interfaces;

namespace PunjabKitchenDAL.Interfaces
{
    public interface IGoodsOutFreezerUnitRepository : IGenericRepository<tGoodsOutFreezerUnit>
    {
        #region Get By Id
        /// <summary>
        /// GetById
        /// </summary>
        /// <param name="id"></param>
        /// <returns>tGoodsOutFreezerUnit</returns>
        tGoodsOutFreezerUnit GetById(long id);
        #endregion

        #region Get Goods Out Freezer Unit History
        IEnumerable<tGoodsOutFreezerUnit> GetGoodsOutFreezerUnitHistory(DateTime startDateTime, DateTime endDateTime);
        #endregion

        #region Get Goods Out Freezer Unit History By Id
        /// <summary>
        /// Get Goods Out Freezer Unit History By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>tGoodsOutFreezerUnit</returns>
        tGoodsOutFreezerUnit GetGoodsOutFreezerUnitHistoryById(long id);
        #endregion
    }
}