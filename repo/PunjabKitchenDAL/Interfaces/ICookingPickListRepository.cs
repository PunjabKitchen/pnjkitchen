﻿using PunjabKitchenEntities.EntityModel;
using PunjabKitchenEntities.Interfaces;

namespace PunjabKitchenDAL.Interfaces
{
    public interface ICookingPickListRepository : IGenericRepository<tPickList>
    {
        #region Get By ID
        /// <summary>
        /// Get By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>tPickList</returns>
        tPickList GetById(long id); 
        #endregion
    }
}
