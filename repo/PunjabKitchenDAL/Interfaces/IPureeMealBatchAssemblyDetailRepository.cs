﻿using PunjabKitchenEntities.EntityModel;
using PunjabKitchenEntities.Interfaces;

namespace PunjabKitchenDAL.Interfaces
{
    public interface IPureeMealBatchAssemblyDetailRepository : IGenericRepository<tPureeMealBatchAssemblyDetail>
    {
        #region GetById
        /// <summary>
        /// GetById
        /// </summary>
        /// <param name="id"></param>
        /// <returns>tPureeMealBatchAssemblyDetail</returns>
        tPureeMealBatchAssemblyDetail GetById(long id); 
        #endregion
    }
}
