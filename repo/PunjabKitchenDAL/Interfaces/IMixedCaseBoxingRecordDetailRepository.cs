﻿using PunjabKitchenEntities.EntityModel;
using PunjabKitchenEntities.Interfaces;

namespace PunjabKitchenDAL.Interfaces
{
    public interface IMixedCaseBoxingRecordDetailRepository : IGenericRepository<tMixedCaseBoxingRecordDetail>
    {
        #region GetById
        /// <summary>
        /// GetById
        /// </summary>
        /// <param name="id"></param>
        /// <returns>tMixedCaseBoxingRecordDetail</returns>
        tMixedCaseBoxingRecordDetail GetById(long id); 
        #endregion
    }
}

