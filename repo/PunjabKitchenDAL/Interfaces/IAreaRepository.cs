﻿using PunjabKitchenEntities.EntityModel;
using PunjabKitchenEntities.Interfaces;

namespace PunjabKitchenDAL.Interfaces
{
    public interface IAreaRepository : IGenericRepository<tArea>
    {
        #region Get By Id

        /// <summary>
        /// Get By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>tArea</returns>
        tArea GetById(long id); 
        #endregion
    }
}
