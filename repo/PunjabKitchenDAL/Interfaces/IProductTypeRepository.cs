﻿using PunjabKitchenEntities.EntityModel;
using PunjabKitchenEntities.Interfaces;

namespace PunjabKitchenDAL.Interfaces
{
    public interface IProductTypeRepository : IGenericRepository<tProductType>
    {
        #region GetById
        /// <summary>
        /// GetById
        /// </summary>
        /// <param name="id"></param>
        /// <returns>tProductType</returns>
        tProductType GetById(long id);
        #endregion

        #region GetPrefixById
        /// <summary>
        /// GetPrefixById
        /// </summary>
        /// <param name="id"></param>
        /// <returns>string</returns>
        string GetPrefixById(int id); 
        #endregion
    }
}
