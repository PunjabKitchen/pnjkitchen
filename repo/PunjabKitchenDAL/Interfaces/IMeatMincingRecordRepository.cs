﻿using PunjabKitchenEntities.EntityModel;
using PunjabKitchenEntities.Interfaces;

namespace PunjabKitchenDAL.Interfaces
{
    public interface IMeatMincingRecordRepository : IGenericRepository<tMeatMincingRecord>
    {
        #region GetById
        /// <summary>
        /// GetById
        /// </summary>
        /// <param name="id"></param>
        /// <returns>tMeatMincingRecord</returns>
        tMeatMincingRecord GetById(long id); 
        #endregion
    }
}