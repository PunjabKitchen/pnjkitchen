﻿using System.Collections.Generic;
using PunjabKitchenEntities.EntityModel;
using PunjabKitchenEntities.Interfaces;

namespace PunjabKitchenDAL.Interfaces
{
    public interface IPackingPlanRepository : IGenericRepository<tPackingProductionPlan>
    {
        #region Get By ID
        /// <summary>
        /// GetById
        /// </summary>
        /// <param name="id"></param>
        /// <returns>tPackingProductionPlan</returns>
        tPackingProductionPlan GetById(long id);
        #endregion

        #region Bet By Code

        /// <summary>
        /// GetByCode
        /// </summary>
        /// <param name="code"></param>
        /// <returns>IEnumerable of tPackingProductionPlan</returns>
        IEnumerable<tPackingProductionPlan> GetByCode(string code); 
        #endregion

        #region Get Packing Plans By Line Id
        /// <summary>
        /// GetPackingPlansByLineId
        /// </summary>
        /// <param name="lineId"></param>
        /// <returns>List of tPackingProductionPlan</returns>
        List<tPackingProductionPlan> GetPackingPlansByLineId(int lineId);


        /// <summary>
        /// GetPackingPlansByLineId
        /// </summary>
        /// <param name="isLine1"></param>
        /// <param name="isLine2"></param>
        /// <param name="isLine3"></param>
        /// <returns> List of tPackingProductionPlan</returns>
        List<tPackingProductionPlan> GetPackingPlansByLineId(bool isLine1, bool isLine2, bool isLine3); 
        #endregion

        #region Batch Code
        /// <summary>
        /// GetBatchCode
        /// </summary>
        /// <returns>string</returns>
        string GetBatchCode();

        /// <summary>
        /// ValidateCode
        /// </summary>
        /// <param name="code"></param>
        /// <returns>bool</returns>
        bool ValidateCode(string code); 
        #endregion
    }
}
