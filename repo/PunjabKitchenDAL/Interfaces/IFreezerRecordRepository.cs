﻿using PunjabKitchenEntities.EntityModel;
using PunjabKitchenEntities.Interfaces;

namespace PunjabKitchenDAL.Interfaces
{
    public interface IFreezerRecordRepository : IGenericRepository<tFreezerRecord>
    {
        #region Get By Id
        /// <summary>
        /// Get By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>tFreezerRecord</returns>
        tFreezerRecord GetById(long id); 
        #endregion
    }
}
