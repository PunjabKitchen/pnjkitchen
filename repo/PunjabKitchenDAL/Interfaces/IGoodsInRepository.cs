﻿using PunjabKitchenEntities.EntityModel;
using PunjabKitchenEntities.Interfaces;

namespace PunjabKitchenDAL.Interfaces
{
    public interface IGoodsInRepository : IGenericRepository<tGoodsIn>
    {
        #region GetById
        /// <summary>
        /// Get By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>tGoodsIn</returns>
        tGoodsIn GetById(long id); 
        #endregion
    }
}
