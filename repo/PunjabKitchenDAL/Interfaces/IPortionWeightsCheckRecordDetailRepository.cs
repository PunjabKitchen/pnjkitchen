﻿using PunjabKitchenEntities.EntityModel;
using PunjabKitchenEntities.Interfaces;

namespace PunjabKitchenDAL.Interfaces
{
    public interface IPortionWeightsCheckRecordDetailRepository : IGenericRepository<tPortionWeightCheckDetail>
    {
        #region GetById
        /// <summary>
        /// GetById
        /// </summary>
        /// <param name="id"></param>
        /// <returns>tPortionWeightCheckDetail</returns>
        tPortionWeightCheckDetail GetById(long id); 
        #endregion
    }
}