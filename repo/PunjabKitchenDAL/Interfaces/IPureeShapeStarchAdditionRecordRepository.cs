﻿using PunjabKitchenEntities.EntityModel;
using PunjabKitchenEntities.Interfaces;

namespace PunjabKitchenDAL.Interfaces
{
    public interface IPureeShapeStarchAdditionRecordRepository : IGenericRepository<tPureeShapesStarchAdditionRecord>
    {
        #region GetById
        /// <summary>
        /// GetById
        /// </summary>
        /// <param name="id"></param>
        /// <returns>tPureeShapesStarchAdditionRecord</returns>
        tPureeShapesStarchAdditionRecord GetById(long id); 
        #endregion
    }
}
