﻿using PunjabKitchenEntities.EntityModel;
using PunjabKitchenEntities.Interfaces;

namespace PunjabKitchenDAL.Interfaces
{
    public interface IPureeMealBatchAssemblyRepository : IGenericRepository<tPureeMeanBatchAssembly>
    {
        #region GetById
        /// <summary>
        /// GetById
        /// </summary>
        /// <param name="id"></param>
        /// <returns>tPureeMeanBatchAssembly</returns>
        tPureeMeanBatchAssembly GetById(long id); 
        #endregion
    }
}