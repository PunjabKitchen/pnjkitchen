﻿using PunjabKitchenEntities.EntityModel;
using PunjabKitchenEntities.Interfaces;

namespace PunjabKitchenDAL.Interfaces
{
    public interface IGoodsOutPicklistDetailRepository : IGenericRepository<tGoodsOutPicklistDetail>
    {
        #region GetById
        /// <summary>
        /// GetById
        /// </summary>
        /// <param name="id"></param>
        /// <returns>tGoodsOutPicklistDetail</returns>
        tGoodsOutPicklistDetail GetById(long id); 
        #endregion
    }
}
