﻿using PunjabKitchenEntities.EntityModel;
using PunjabKitchenEntities.Interfaces;

namespace PunjabKitchenDAL.Interfaces
{
    public interface IHotPackingRercordRepository : IGenericRepository<tHotPackingRercord>
    {
        #region GetById
        /// <summary>
        /// GetById
        /// </summary>
        /// <param name="id"></param>
        /// <returns>tHotPackingRercord</returns>
        tHotPackingRercord GetById(long id); 
        #endregion
    }
}