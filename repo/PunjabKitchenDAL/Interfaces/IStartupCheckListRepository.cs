﻿using PunjabKitchenEntities.EntityModel;
using PunjabKitchenEntities.Interfaces;

namespace PunjabKitchenDAL.Interfaces
{
    public interface IStartupCheckListRepository : IGenericRepository<tStartUpCheckList>
    {
        #region GetById
        /// <summary>
        /// GetById
        /// </summary>
        /// <param name="id"></param>
        /// <returns>tStartUpCheckList</returns>
        tStartUpCheckList GetById(long id); 
        #endregion
    }
}
