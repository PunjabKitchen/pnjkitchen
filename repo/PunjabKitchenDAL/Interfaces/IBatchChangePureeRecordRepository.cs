﻿using PunjabKitchenEntities.EntityModel;
using PunjabKitchenEntities.Interfaces;

namespace PunjabKitchenDAL.Interfaces
{
    public interface IBatchChangePureeRecordRepository : IGenericRepository<tBatchChangeChkPureeRecrd>
    {
        #region Get By Id
        /// <summary>
        /// Get By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>tBatchChangeChkPureeRecrd</returns>
        tBatchChangeChkPureeRecrd GetById(long id); 
        #endregion
    }
}
