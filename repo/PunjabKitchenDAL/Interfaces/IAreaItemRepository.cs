﻿using System.Collections.Generic;
using PunjabKitchenEntities.EntityModel;
using PunjabKitchenEntities.Interfaces;

namespace PunjabKitchenDAL.Interfaces
{
    public interface IAreaItemRepository : IGenericRepository<tAreaItem>
    {
        #region Get By Id
        /// <summary>
        /// GetById
        /// </summary>
        /// <param name="id"></param>
        /// <returns>tAreaItem</returns>
        tAreaItem GetById(long id);
        #endregion

        #region Get Item By Area Id
        /// <summary>
        /// GetItemByAreaId
        /// </summary>
        /// <param name="id"></param>
        /// <returns>List of tAreaItem</returns>
        List<tAreaItem> GetItemByAreaId(long id);  
        #endregion
    }
}