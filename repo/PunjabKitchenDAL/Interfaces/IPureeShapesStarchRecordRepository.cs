﻿using PunjabKitchenEntities.EntityModel;
using PunjabKitchenEntities.Interfaces;

namespace PunjabKitchenDAL.Interfaces
{
    public interface IPureeShapesStarchRecordRepository : IGenericRepository<tPureeShapesStarchRecord>
    {
        #region GetById
        /// <summary>
        /// GetById
        /// </summary>
        /// <param name="id"></param>
        /// <returns>tPureeShapesStarchRecord</returns>
        tPureeShapesStarchRecord GetById(long id); 
        #endregion
    }
}

