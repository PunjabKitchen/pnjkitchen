﻿using System;
using System.Collections.Generic;
using PunjabKitchenEntities.EntityModel;
using PunjabKitchenEntities.Interfaces;

namespace PunjabKitchenDAL.Interfaces
{

    public interface ICleaningCheckListDailyRepository : IGenericRepository<CleaningChecklistDaily>
    {
        #region GetById
        /// <summary>
        /// GetById
        /// </summary>
        /// <param name="id"></param>
        /// <returns>CleaningChecklistDaily</returns>
        CleaningChecklistDaily GetById(long id);
        #endregion

        #region GetDistinctDailyCheck
        /// <summary>
        /// GetDistinctDailyCheck
        /// </summary>
        /// <param name="currentDateTime"></param>
        /// <returns>IEnumerable of CleaningChecklistDaily</returns>
        IEnumerable<CleaningChecklistDaily> GetDistinctDailyCheck(DateTime currentDateTime);
        #endregion

        #region GetDailyCleaningChecklistHistory
        /// <summary>
        /// GetDailyCleaningChecklistHistory
        /// </summary>
        /// <param name="startDateTime"></param>
        /// <param name="endDateTime"></param>
        /// <returns>IEnumerable of CleaningChecklistDaily</returns>
        IEnumerable<CleaningChecklistDaily> GetDailyCleaningChecklistHistory(DateTime startDateTime, DateTime endDateTime);
        #endregion

        #region GetDailyCleaningChecklistHistoryById
        /// <summary>
        /// GetDailyCleaningChecklistHistoryById
        /// </summary>
        /// <param name="id"></param>
        /// <param name="areaId"></param>
        /// <returns>IEnumerable of CleaningChecklistDaily</returns>
        IEnumerable<CleaningChecklistDaily> GetDailyCleaningChecklistHistoryById(long id, long areaId); 
        #endregion
    }
}
