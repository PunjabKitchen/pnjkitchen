﻿using System;
using System.Collections.Generic;
using PunjabKitchenEntities.EntityModel;
using PunjabKitchenEntities.Interfaces;

namespace PunjabKitchenDAL.Interfaces
{
    public interface IStartUpcheckDailyRepository : IGenericRepository<tStartUpcheckDaily>
    {
        #region GetById
        /// <summary>
        /// GetById
        /// </summary>
        /// <param name="id"></param>
        /// <returns>tStartUpcheckDaily</returns>
        tStartUpcheckDaily GetById(long id);
        #endregion

        #region GetStartupChecklistHistory
        /// <summary>
        /// GetStartupChecklistHistory
        /// </summary>
        /// <param name="startDateTime"></param>
        /// <param name="endDateTime"></param>
        /// <returns>IEnumerable of tStartUpcheckDaily</returns>
        IEnumerable<tStartUpcheckDaily> GetStartupChecklistHistory(DateTime startDateTime, DateTime endDateTime);
        #endregion

        #region GetStartupChecklistHistoryById
        /// <summary>
        /// GetStartupChecklistHistoryById
        /// </summary>
        /// <param name="id"></param>
        /// <returns>IEnumerable of tStartUpcheckDaily</returns>
        IEnumerable<tStartUpcheckDaily> GetStartupChecklistHistoryById(long id); 
        #endregion
    }
}
