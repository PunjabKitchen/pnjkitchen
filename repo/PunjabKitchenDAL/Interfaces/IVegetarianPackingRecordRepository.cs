﻿using PunjabKitchenEntities.EntityModel;
using PunjabKitchenEntities.Interfaces;

namespace PunjabKitchenDAL.Interfaces
{
    public interface IVegetarianPackingRecordRepository : IGenericRepository<tVegetarianPackingRecord>
    {
        #region GetById
        /// <summary>
        /// GetById
        /// </summary>
        /// <param name="id"></param>
        /// <returns>tVegetarianPackingRecord</returns>
        tVegetarianPackingRecord GetById(long id); 
        #endregion
    }
}