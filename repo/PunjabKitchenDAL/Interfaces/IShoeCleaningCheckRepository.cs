﻿using System;
using System.Collections.Generic;
using PunjabKitchenEntities.EntityModel;
using PunjabKitchenEntities.Interfaces;

namespace PunjabKitchenDAL.Interfaces
{
    public interface IShoeCleaningCheckRepository : IGenericRepository<tShoeCleaningRecrdWeekly>
    {
        #region GetById
        /// <summary>
        /// GetById
        /// </summary>
        /// <param name="id"></param>
        /// <returns>tShoeCleaningRecrdWeekly</returns>
        tShoeCleaningRecrdWeekly GetById(long id);
        #endregion

        #region CheckWeeklyRecord
        /// <summary>
        /// CheckWeeklyRecord
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="userId"></param>
        /// <returns>bool</returns>
        bool CheckWeeklyRecord(DateTime startDate, string userId);
        #endregion

        #region GetShoesCleaningHistory
        /// <summary>
        /// GetShoesCleaningHistory
        /// </summary>
        /// <param name="startDateTime"></param>
        /// <param name="endDateTime"></param>
        /// <returns>IEnumerable of tShoeCleaningRecrdWeekly</returns>
        IEnumerable<tShoeCleaningRecrdWeekly> GetShoesCleaningHistory(DateTime startDateTime, DateTime endDateTime);
        #endregion

        #region IsCheckPerformed
        /// <summary>
        /// IsCheckPerformed
        /// </summary>
        /// <param name="startDateTime"></param>
        /// <param name="endDateTime"></param>
        /// <param name="userId"></param>
        /// <returns>bool</returns>
        bool IsCheckPerformed(DateTime startDateTime, DateTime endDateTime, string userId);
        #endregion

        #region GetShoeCleaningChecklistHistoryById
        /// <summary>
        /// GetShoeCleaningChecklistHistoryById
        /// </summary>
        /// <param name="id"></param>
        /// <returns>tShoeCleaningRecrdWeekly</returns>
        tShoeCleaningRecrdWeekly GetShoeCleaningChecklistHistoryById(long id); 
        #endregion
    }
}
