﻿using PunjabKitchenEntities.EntityModel;
using PunjabKitchenEntities.Interfaces;

namespace PunjabKitchenDAL.Interfaces
{
    public interface IPackingRecordRepository : IGenericRepository<tPackingRecord>
    {
        #region GetById
        /// <summary>
        /// GetById
        /// </summary>
        /// <param name="id"></param>
        /// <returns>tPackingRecord</returns>
        tPackingRecord GetById(long id); 
        #endregion
    }
}