﻿using PunjabKitchenEntities.EntityModel;
using PunjabKitchenEntities.Interfaces;

namespace PunjabKitchenDAL.Interfaces
{
    // ReSharper disable once InconsistentNaming
    public interface IFMDetectionAndPackWeightRecordRepository : IGenericRepository<tFMDetectionAndPackWeightTest>
    {
        #region Get By ID
        /// <summary>
        /// GetById
        /// </summary>
        /// <param name="id"></param>
        /// <returns>tFMDetectionAndPackWeightTest</returns>
        tFMDetectionAndPackWeightTest GetById(long id); 
        #endregion
    }
}
