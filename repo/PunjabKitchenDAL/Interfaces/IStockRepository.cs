﻿using System.Collections.Generic;
using PunjabKitchenEntities.EntityModel;
using PunjabKitchenEntities.Interfaces;

namespace PunjabKitchenDAL.Interfaces
{
    public interface IStockRepository : IGenericRepository<tStock>
    {
        #region GetById
        /// <summary>
        /// GetById
        /// </summary>
        /// <param name="id"></param>
        /// <returns>tStock</returns>
        tStock GetById(long id);
        #endregion

        #region GetAllDistinctStockLocations
        /// <summary>
        /// GetAllDistinctStockLocations
        /// </summary>
        /// <returns>List of tStock</returns>
        List<tStock> GetAllDistinctStockLocations();
        #endregion

        #region GetAllDistinctStockPoints
        /// <summary>
        /// GetAllDistinctStockPoints
        /// </summary>
        /// <param name="stockLoc"></param>
        /// <returns>List of tStock</returns>
        List<tStock> GetAllDistinctStockPoints(string stockLoc);
        #endregion

        #region validateStockPoint
        /// <summary>
        /// validateStockPoint
        /// </summary>
        /// <param name="stock"></param>
        /// <returns>bool</returns>
        // ReSharper disable once InconsistentNaming
        bool validateStockPoint(tStock stock);
        #endregion

        #region GetAllowedStockQuanity
        /// <summary>
        /// GetAllowedStockQuanity
        /// </summary>
        /// <param name="productCode"></param>
        /// <returns>int</returns>
        int GetAllowedStockQuanity(string productCode);
        #endregion

        #region GetStockByProductCode
        /// <summary>
        /// GetStockByProductCode
        /// </summary>
        /// <param name="productCode"></param>
        /// <returns>List of tStock</returns>
        List<tStock> GetStockByProductCode(string productCode);
        #endregion

        #region GetStockByProductId
        /// <summary>
        /// GetStockByProductId
        /// </summary>
        /// <param name="productId"></param>
        /// <returns>tStock</returns>
        tStock GetStockByProductId(int productId); 
        #endregion
    }
}
