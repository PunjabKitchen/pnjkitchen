﻿using PunjabKitchenEntities.EntityModel;
using PunjabKitchenEntities.Interfaces;

namespace PunjabKitchenDAL.Interfaces
{
    public interface ISlicingRecordRepository : IGenericRepository<tSlicingRecord>
    {
        #region GetById
        /// <summary>
        /// GetById
        /// </summary>
        /// <param name="id"></param>
        /// <returns>tSlicingRecord</returns>
        tSlicingRecord GetById(long id); 
        #endregion
    }
}
