﻿using PunjabKitchenEntities.EntityModel;
using PunjabKitchenEntities.Interfaces;

namespace PunjabKitchenDAL.Interfaces
{
    public interface IShapesControlRecordBatchDetailRepository : IGenericRepository<tShapesControlRecordBatchDetail>
    {
        #region GetById
        /// <summary>
        /// GetById
        /// </summary>
        /// <param name="id"></param>
        /// <returns>tShapesControlRecordBatchDetail</returns>
        tShapesControlRecordBatchDetail GetById(long id); 
        #endregion
    }
}