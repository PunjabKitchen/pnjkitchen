﻿using PunjabKitchenEntities.EntityModel;
using PunjabKitchenEntities.Interfaces;

namespace PunjabKitchenDAL.Interfaces
{
    public interface IGoodsOutPicklistRepository : IGenericRepository<tGoodsOutPicklist>
    {
        #region GetById
        /// <summary>
        /// GetById
        /// </summary>
        /// <param name="id"></param>
        /// <returns>tGoodsOutPicklist</returns>
        tGoodsOutPicklist GetById(long id); 
        #endregion
    }
}
