﻿using PunjabKitchenEntities.EntityModel;
using PunjabKitchenEntities.Interfaces;

namespace PunjabKitchenDAL.Interfaces
{
    public interface IRiceCoolingTempRecordRepository : IGenericRepository<tRiceCoolingTempRecord>
    {
        #region GetById
        /// <summary>
        /// GetById
        /// </summary>
        /// <param name="id"></param>
        /// <returns>tRiceCoolingTempRecord</returns>
        tRiceCoolingTempRecord GetById(long id); 
        #endregion
    }
}