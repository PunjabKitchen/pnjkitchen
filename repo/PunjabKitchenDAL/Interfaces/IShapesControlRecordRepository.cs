﻿using PunjabKitchenEntities.EntityModel;
using PunjabKitchenEntities.Interfaces;

namespace PunjabKitchenDAL.Interfaces
{
    public interface IShapesControlRecordRepository : IGenericRepository<tShapeControlRecord>
    {
        #region GetById
        /// <summary>
        /// GetById
        /// </summary>
        /// <param name="id"></param>
        /// <returns>tShapeControlRecord</returns>
        tShapeControlRecord GetById(long id); 
        #endregion
    }
}