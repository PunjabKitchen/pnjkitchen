﻿using PunjabKitchenEntities.EntityModel;
using PunjabKitchenEntities.Interfaces;

namespace PunjabKitchenDAL.Interfaces
{
    public interface IGoodsOutPlanDetailRepository : IGenericRepository<tGoodsOutPlanDetail>
    {
        #region GetById
        /// <summary>
        /// GetById
        /// </summary>
        /// <param name="id"></param>
        /// <returns>tGoodsOutPlanDetail</returns>
        tGoodsOutPlanDetail GetById(long id); 
        #endregion
    }
}
