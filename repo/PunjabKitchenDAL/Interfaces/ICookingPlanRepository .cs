﻿using System.Collections.Generic;
using PunjabKitchenEntities.EntityModel;
using PunjabKitchenEntities.Interfaces;

namespace PunjabKitchenDAL.Interfaces
{
    public interface ICookingPlanRepository : IGenericRepository<tCookingPlan>
    {
        #region Get By Id
        /// <summary>
        /// GetById
        /// </summary>
        /// <param name="id"></param>
        /// <returns>GetById</returns>
        tCookingPlan GetById(long id);
        #endregion

        #region Get By Code
        /// <summary>
        /// GetByCode
        /// </summary>
        /// <param name="code"></param>
        /// <returns>IEnumerable of tCookingPlan</returns>
        IEnumerable<tCookingPlan> GetByCode(string code);
        #endregion

        #region Get CookingPlan By Cookpot Id
        /// <summary>
        /// GetCookingPlansByCookpotId
        /// </summary>
        /// <param name="cookPotId"></param>
        /// <returns>List of tCookingPlan</returns>
        List<tCookingPlan> GetCookingPlansByCookpotId(int cookPotId);

        /// <summary>
        /// GetCookingPlansByCookpotId
        /// </summary>
        /// <param name="isCookPot1"></param>
        /// <param name="isCookPot2"></param>
        /// <param name="isCookPot3"></param>
        /// <returns>List of tCookingPlan</returns>
        List<tCookingPlan> GetCookingPlansByCookpotId(bool isCookPot1, bool isCookPot2, bool isCookPot3); 
        #endregion

        #region Batch Code
        /// <summary>
        /// GetBatchCode
        /// </summary>
        /// <returns>string</returns>
        string GetBatchCode();


        /// <summary>
        /// ValidateBatchCode
        /// </summary>
        /// <param name="batchCode"></param>
        /// <returns>bool</returns>
        bool ValidateBatchCode(string batchCode);
        #endregion
    }
}
