﻿using System;
using System.Collections.Generic;
using PunjabKitchenEntities.EntityModel;
using PunjabKitchenEntities.Interfaces;

namespace PunjabKitchenDAL.Interfaces
{
    public interface IAttendanceRepository : IGenericRepository<tEmployeeAttendance>
    {
        #region Get By Id

        /// <summary>
        /// Get By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>tArea</returns>
        tEmployeeAttendance GetById(string id);
        #endregion

        #region Get All By Id
        /// <summary>
        /// Get All By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        IEnumerable<tEmployeeAttendance> GetAllRecordsById(string id);
        #endregion

        #region Get Today's Record by User ID
        /// <summary>
        /// Get Today's Record by User ID
        /// </summary>
        /// <param name="id"></param>
        /// <param name="dateTime"></param>
        /// <returns></returns>
        tEmployeeAttendance GetTodaysRecordByUserId(string id, DateTime dateTime);
        #endregion

        #region Get Record for Editing
        /// <summary>
        /// Get Record for Editing
        /// </summary>
        /// <param name="iEmployeeId"></param>
        /// <param name="Sys_CreatedDateTime"></param>
        /// <returns></returns>
        // ReSharper disable once InconsistentNaming
        tEmployeeAttendance GetRecordForEditing(string iEmployeeId, DateTime Sys_CreatedDateTime);
        #endregion
    }
}