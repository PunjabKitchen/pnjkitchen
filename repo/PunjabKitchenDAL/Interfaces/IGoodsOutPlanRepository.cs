﻿using System;
using PunjabKitchenEntities.EntityModel;
using PunjabKitchenEntities.Interfaces;

namespace PunjabKitchenDAL.Interfaces
{
    public interface IGoodsOutPlanRepository : IGenericRepository<tGoodsOutPlan>
    {
        #region GetById
        /// <summary>
        /// GetById
        /// </summary>
        /// <param name="id"></param>
        /// <returns>tGoodsOutPlan</returns>
        tGoodsOutPlan GetById(long id);
        #endregion

        #region GetBatchCode
        /// <summary>
        /// GetBatchCode
        /// </summary>
        /// <returns>string</returns>
        string GetBatchCode();
        #endregion

        #region ValidateBatchCode
        /// <summary>
        /// ValidateBatchCode
        /// </summary>
        /// <param name="batchCode"></param>
        /// <returns>bool</returns>
        bool ValidateBatchCode(string batchCode);
        #endregion

        #region GetByCategoryAndDate
        /// <summary>
        /// GetByCategoryAndDate
        /// </summary>
        /// <param name="category"></param>
        /// <param name="date"></param>
        /// <returns>tGoodsOutPlan</returns>
        tGoodsOutPlan GetByCategoryAndDate(string category, DateTime date);
        #endregion
    }
}
