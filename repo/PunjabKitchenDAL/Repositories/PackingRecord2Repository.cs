﻿using System.Data.Entity;
using System.Linq;
using PunjabKitchenDAL.Interfaces;
using PunjabKitchenEntities.Common;
using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchenDAL.Repositories
{
    public class PackingRecord2Repository : GenericRepository<tPackingRecord2>, IPackingRecord2Repository
    {
        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="context"></param>
        public PackingRecord2Repository(DbContext context) : base(context)
        {
        }
        #endregion

        #region Get by Id
        /// <summary>
        /// Get By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>tPackingRecord2</returns>
        public tPackingRecord2 GetById(long id)
        {
            return _dbset.FirstOrDefault(x => x.iPackingRecordId == id);
        }
        #endregion
    }
}
