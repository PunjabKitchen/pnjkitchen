﻿using System.Linq;
using System.Data.Entity;
using PunjabKitchenDAL.Interfaces;
using PunjabKitchenEntities.Common;
using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchenDAL.Repositories
{
    class BatchCoolingTempRecrdRepository : GenericRepository<tBatchCoolingTempRecrd>, IBatchCoolingTempRecrdRepository
    {
        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="context"></param>
        public BatchCoolingTempRecrdRepository(DbContext context) : base(context)
        {
        }
        #endregion

        #region Get By Id
        /// <summary>
        /// Get By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>tBatchCoolingTempRecrd</returns>
        public tBatchCoolingTempRecrd GetById(long id)
        {
            return _dbset.FirstOrDefault(x => x.iBatchCoolingTempRecrdId == id);
        }
        #endregion
    }
}
