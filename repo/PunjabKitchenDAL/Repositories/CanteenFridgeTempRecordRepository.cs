﻿using System.Linq;
using System.Data.Entity;
using PunjabKitchenDAL.Interfaces;
using System;
using System.Collections.Generic;
using PunjabKitchenEntities.Common;
using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchenDAL.Repositories
{
    public class CanteenFridgeTempRecordRepository : GenericRepository<tCanteenFridgeTempRecordDaily>, ICanteenFridgeTempRecordRepository
    {
        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="context"></param>
        public CanteenFridgeTempRecordRepository(DbContext context) : base(context)
        {
        }
        #endregion

        #region Get By Id
        /// <summary>
        /// Get By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>tCanteenFridgeTempRecordDaily</returns>
        public tCanteenFridgeTempRecordDaily GetById(long id)
        {
            return _dbset.FirstOrDefault(x => x.iCanteenFridgeTempRecordId == id);
        }
        #endregion

        #region Get Canteen Fridge Temperature History

        /// <summary>
        /// //Get Canteen Fridge Temperature History
        /// </summary>
        /// <param name="startDateTime"></param>
        /// <param name="endDateTime"></param>
        /// <returns>IEnumerable of tCanteenFridgeTempRecordDaily</returns>
        public IEnumerable<tCanteenFridgeTempRecordDaily> GetCanteenFridgeTemperatureHistory(DateTime startDateTime, DateTime endDateTime)
        {
            var list =
                _dbset.Where(x => DbFunctions.TruncateTime(x.Sys_CreatedDateTime.Value) >= DbFunctions.TruncateTime(startDateTime) && DbFunctions.TruncateTime(x.Sys_CreatedDateTime.Value) <= DbFunctions.TruncateTime(endDateTime)).ToList();
            return list;
        }
        #endregion

        #region Get Canteen Fridge Temperature History By ID
        /// <summary>
        /// Get Canteen Fridge Temperature History By ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns>tCanteenFridgeTempRecordDaily</returns>
        public tCanteenFridgeTempRecordDaily GetCanteenFridgeChecklistHistoryById(long id)
        {
            var dbItem = _dbset.FirstOrDefault(x => x.iCanteenFridgeTempRecordId == id);

            return dbItem;
        }
        #endregion
    }
}
