﻿using System.Linq;
using System.Data.Entity;
using PunjabKitchenDAL.Interfaces;
using PunjabKitchenEntities.Common;
using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchenDAL.Repositories
{
    public class PortionWeightsCheckRecordRepository : GenericRepository<tPortionWeightCheck>, IPortionWeightsCheckRecordRepository
    {
        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="context"></param>
        public PortionWeightsCheckRecordRepository(DbContext context) : base(context)
        {
        }
        #endregion

        #region Get by Id
        /// <summary>
        /// GetById
        /// </summary>
        /// <param name="id"></param>
        /// <returns>tPortionWeightCheck</returns>
        public tPortionWeightCheck GetById(long id)
        {
            return _dbset.FirstOrDefault(x => x.iPortionWeightCheckId == id);
        }
        #endregion
    }
}
