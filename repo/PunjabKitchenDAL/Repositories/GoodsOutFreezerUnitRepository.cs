﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using PunjabKitchenDAL.Interfaces;
using PunjabKitchenEntities.Common;
using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchenDAL.Repositories
{
    public class GoodsOutFreezerUnitRepository : GenericRepository<tGoodsOutFreezerUnit>, IGoodsOutFreezerUnitRepository
    {
        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="context"></param>
        public GoodsOutFreezerUnitRepository(DbContext context) : base(context)
        {
        }
        #endregion

        #region Get By Id

        /// <summary>
        /// Get By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>tAreaItem</returns>
        public tGoodsOutFreezerUnit GetById(long id)
        {
            return _dbset.FirstOrDefault(x => x.iGoodsOutFreezerUnit == id);
        }

        #endregion

        #region Get Goods Out Freezer Unit History
        /// <summary>
        /// Get Goods Out Freezer Unit History
        /// </summary>
        /// <param name="startDateTime"></param>
        /// <param name="endDateTime"></param>
        /// <returns>IEnumerable of tGoodsOutFreezerUnit</returns>
        public IEnumerable<tGoodsOutFreezerUnit> GetGoodsOutFreezerUnitHistory(DateTime startDateTime, DateTime endDateTime)
        {
            var list =
                _dbset.Where(x => DbFunctions.TruncateTime(x.Sys_CreatedDateTime.Value) >= DbFunctions.TruncateTime(startDateTime) && DbFunctions.TruncateTime(x.Sys_CreatedDateTime.Value) <= DbFunctions.TruncateTime(endDateTime)).ToList();
            return list;
        }
        #endregion

        #region Get Goods Out Freezer Unit History By ID
        /// <summary>
        /// Get Goods Out Freezer Unit History By ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns>tGoodsOutFreezerUnit</returns>
        public tGoodsOutFreezerUnit GetGoodsOutFreezerUnitHistoryById(long id)
        {
            var dbItem = _dbset.FirstOrDefault(x => x.iGoodsOutFreezerUnit == id);

            return dbItem;
        }
        #endregion
    }
}
