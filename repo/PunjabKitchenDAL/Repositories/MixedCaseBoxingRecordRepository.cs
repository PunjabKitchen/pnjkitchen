﻿using System.Linq;
using System.Data.Entity;
using PunjabKitchenDAL.Interfaces;
using PunjabKitchenEntities.Common;
using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchenDAL.Repositories
{
    public class MixedCaseBoxingRecordRepository : GenericRepository<tMixedCaseBoxingRecord>, IMixedCaseBoxingRecordRepository
    {
        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="context"></param>
        public MixedCaseBoxingRecordRepository(DbContext context) : base(context)
        {
        }
        #endregion

        #region Get By Id
        /// <summary>
        /// Get By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>tMixedCaseBoxingRecord</returns>
        public tMixedCaseBoxingRecord GetById(long id)
        {
            return _dbset.FirstOrDefault(x => x.iMixedCaseBoxingRecordId == id);
        }
        
        #endregion
    }
}
