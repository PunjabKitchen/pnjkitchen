﻿using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using PunjabKitchenDAL.Interfaces;
using PunjabKitchenEntities.Common;
using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchenDAL.Repositories
{
    public class RecipeRepository : GenericRepository<tRecipe>, IRecipeRepository
    {
        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="context"></param>
        public RecipeRepository(DbContext context) : base(context)
        {
        }
        #endregion

        #region Get By ID
        /// <summary>
        /// Get By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>tRecipe</returns>
        public tRecipe GetById(long id)
        {
            return _dbset.FirstOrDefault(x => x.iRecipeId == id);
        } 
        #endregion

        #region Get By COde
        /// <summary>
        /// Get By Code
        /// </summary>
        /// <param name="code"></param>
        /// <returns>IEnumerable of tRecipe</returns>
        public IEnumerable<tRecipe> GetByCode(string code)
        {
            var distinctList = _dbset.GroupBy(x => x.vRecipeCode).Select(y => y.FirstOrDefault()).Distinct();
            var list = distinctList.Where(x => x.vRecipeCode.Contains(code)).ToList();
            return list;
        } 
        #endregion
    }
}
