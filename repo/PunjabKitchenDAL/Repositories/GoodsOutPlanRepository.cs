﻿using System.Linq;
using System.Data.Entity;
using PunjabKitchenDAL.Interfaces;
using PunjabKitchenEntities.Common;
using PunjabKitchenEntities.EntityModel;
using System.Text.RegularExpressions;
using System.Web.WebPages;
using System;

namespace PunjabKitchenDAL.Repositories
{
    public class GoodsOutPlanRepository : GenericRepository<tGoodsOutPlan>, IGoodsOutPlanRepository
    {
        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="context"></param>
        public GoodsOutPlanRepository(DbContext context) : base(context)
        {
        }
        #endregion

        #region Get By Id
        /// <summary>
        /// Get By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>tGoodsOutPlan</returns>
        public tGoodsOutPlan GetById(long id)
        {
            return _dbset.Include(plan => plan.tGoodsOutPlanDetails.Select(s => s.tProduct)).FirstOrDefault(x => x.iGoodsOutPlanId == id);
        }

        #endregion

        #region Batch Code

        /// <summary>
        /// Get Batch Code
        /// </summary>
        /// <returns>batch Code</returns>
        public string GetBatchCode()
        {
            var result =
                _dbset.OrderByDescending(x => x.iGoodsOutPlanId).FirstOrDefault(x => x.vBatchCode != null);
            var numPart = result != null ? Regex.Match(result.vBatchCode, @"\d+").Value.AsInt() + 1 : 1;
            var code = numPart.ToString("D" + 6);
            return code;
        }

        /// <summary>
        /// Validate Batch Code
        /// </summary>
        /// <param name="batchCode"></param>
        /// <returns>Batch Code to validate</returns>
        public bool ValidateBatchCode(string batchCode)
        {
            return _dbset.Any(x => x.vBatchCode == batchCode);
        }

        #endregion

        #region Get By Category and Date

        /// <summary>
        /// Get By Category And Date
        /// </summary>
        /// <param name="category"></param>
        /// <param name="date"></param>
        /// <returns>tGoodsOutPlan</returns>
        public tGoodsOutPlan GetByCategoryAndDate(string category, DateTime date)
        {
            return _dbset.FirstOrDefault(f => f.vCategory == category && f.dDate == date.Date);
        }

        #endregion
    }
}
