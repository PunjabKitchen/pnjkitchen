﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using PunjabKitchenDAL.Interfaces;
using PunjabKitchenEntities.Common;
using CleaningChecklistDaily = PunjabKitchenEntities.EntityModel.CleaningChecklistDaily;


namespace PunjabKitchenDAL.Repositories
{
    public class CleaningCheckListDailyRepository : GenericRepository<CleaningChecklistDaily>, ICleaningCheckListDailyRepository
    {
        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="context"></param>
        public CleaningCheckListDailyRepository(DbContext context) : base(context)
        {
        }
        #endregion

        #region Get By Id
        /// <summary>
        /// Get By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>CleaningChecklistDaily</returns>
        public CleaningChecklistDaily GetById(long id)
        {
            return _dbset.FirstOrDefault(x => x.iCleaningChecklistDailyId == id);
        }
        #endregion

        #region Get Distinct Daily Check
        /// <summary>
        /// Get Distinct Daily Check
        /// </summary>
        /// <param name="currentDateTime"></param>
        /// <returns>IEnumerable of CleaningChecklistDaily</returns>
        public IEnumerable<CleaningChecklistDaily> GetDistinctDailyCheck(DateTime currentDateTime)
        {
            var list = _dbset.Where(x => DbFunctions.TruncateTime(x.Sys_CreatedDateTime) == currentDateTime.Date).GroupBy(y => y.iAreaId).Select(z => z.FirstOrDefault()).ToList();
            return list;
        } 
        #endregion

        #region Get Daily Cleaning Checklist History

        /// <summary>
        /// //Get Daily Cleaning Checklist History
        /// </summary>
        /// <param name="startDateTime"></param>
        /// <param name="endDateTime"></param>
        /// <returns>IEnumerable of CleaningChecklistDaily</returns>
        public IEnumerable<CleaningChecklistDaily> GetDailyCleaningChecklistHistory(DateTime startDateTime, DateTime endDateTime)
        {
            var list = _dbset.Where(x => DbFunctions.TruncateTime(x.Sys_CreatedDateTime.Value) >= DbFunctions.TruncateTime(startDateTime) && DbFunctions.TruncateTime(x.Sys_CreatedDateTime.Value) <= DbFunctions.TruncateTime(endDateTime)).ToList();
            return list;
        }
        #endregion

        #region Get Daily Cleaning Checklist History By Id

        /// <summary>
        /// //Get Daily Cleaning Checklist History
        /// </summary>
        /// <param name="id"></param>
        /// <param name="areaId"></param>
        /// <returns>IEnumerable of CleaningChecklistDaily</returns>
        public IEnumerable<CleaningChecklistDaily> GetDailyCleaningChecklistHistoryById(long id, long areaId)
        {
            var dbItem = _dbset.FirstOrDefault(x => x.iCleaningChecklistDailyId == id);
            var list =
                _dbset.Where(x => DbFunctions.TruncateTime(x.dDateTime.Value) == DbFunctions.TruncateTime(dbItem.dDateTime) && x.iAreaId == areaId).ToList();
            return list;
        }
        #endregion
    }


}
