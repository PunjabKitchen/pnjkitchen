﻿using System.Data.Entity;
using System.Linq;
using PunjabKitchenDAL.Interfaces;
using PunjabKitchenEntities.Common;
using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchenDAL.Repositories
{
    public class ShapesControlRecordBatchDetailRepository : GenericRepository<tShapesControlRecordBatchDetail>, IShapesControlRecordBatchDetailRepository
    {
        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="context"></param>
        public ShapesControlRecordBatchDetailRepository(DbContext context) : base(context)
        {
        }
        #endregion

        #region Get by Id
        /// <summary>
        /// Get By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>tShapesControlRecordBatchDetail</returns>
        public tShapesControlRecordBatchDetail GetById(long id)
        {
            return _dbset.FirstOrDefault(x => x.iShapesControlRecordBatchDetailId == id);
        }
        #endregion
    }
}
