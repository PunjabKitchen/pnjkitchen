﻿using System.Linq;
using System.Data.Entity;
using PunjabKitchenDAL.Interfaces;
using PunjabKitchenEntities.Common;
using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchenDAL.Repositories
{
    public class PureeShapesStarchRecordDetailRepository : GenericRepository<tPureeShapesStarchRecordDetail>, IPureeShapesStarchRecordDetailRepository
    {
        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="context"></param>
        public PureeShapesStarchRecordDetailRepository(DbContext context) : base(context)
        {
        }
        #endregion

        #region Get By Id
        /// <summary>
        /// Get By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>tPureeShapesStarchRecordDetail</returns>
        public tPureeShapesStarchRecordDetail GetById(long id)
        {
            return _dbset.FirstOrDefault(x => x.iPureeShapeStarchRecordDetailId == id);
        } 
        #endregion
    }
}
