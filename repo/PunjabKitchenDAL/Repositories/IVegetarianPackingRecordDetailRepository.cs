﻿using System.Data.Entity;
using System.Linq;
using PunjabKitchenDAL.Interfaces;
using PunjabKitchenEntities.Common;
using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchenDAL.Repositories
{
    public class VegetarianPackingRecordDetailRepository : GenericRepository<tVegetarianPackingRecordDetail>, IVegetarianPackingRecordDetailRepository
    {
        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="context"></param>
        public VegetarianPackingRecordDetailRepository(DbContext context) : base(context)
        {
        }
        #endregion

        #region Get by Id
        /// <summary>
        /// Get By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>tVegetarianPackingRecordDetail</returns>
        public tVegetarianPackingRecordDetail GetById(long id)
        {
            return _dbset.FirstOrDefault(x => x.iVegeRecordDetailId == id);
        }
        #endregion
    }
}
