﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using PunjabKitchenDAL.Interfaces;
using PunjabKitchenEntities.Common;
using PunjabKitchenEntities.EntityModel;


namespace PunjabKitchenDAL.Repositories
{
    public class AttendanceRepository : GenericRepository<tEmployeeAttendance>, IAttendanceRepository
    {
        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="context"></param>
        public AttendanceRepository(DbContext context) : base(context)
        {
        }
        #endregion

        #region Get By Id
        /// <summary>
        /// Get By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>tArea</returns>
        public tEmployeeAttendance GetById(string id)
        {
            return _dbset.FirstOrDefault(x => x.iEmployeeId == id.ToString());
        }
        #endregion

        #region Get All Records By Id
        /// <summary>
        /// Get All Records By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public IEnumerable<tEmployeeAttendance> GetAllRecordsById(string id)
        {
            var allRecords = _dbset.Where(x => x.iEmployeeId == id).ToList();
            return allRecords;
        }

        public tEmployeeAttendance GetTodaysRecordByUserId(string id, DateTime dateTime)
        {
            return _dbset.FirstOrDefault(x => x.iEmployeeId == id
                                            //&& x.dDateTime.HasValue
                                            && DbFunctions.TruncateTime(x.dDateTime) == dateTime.Date);
            //return result;
        }
        #endregion

        #region Get Record if it exists otherwise create one
        /// <summary>
        /// Get All Records By Id
        /// </summary>
        /// <param name="iEmployeeId"></param>
        /// <param name="Sys_CreatedDateTime"></param>
        /// <returns></returns>
        // ReSharper disable once InconsistentNaming
        public tEmployeeAttendance GetRecordForEditing(string iEmployeeId, DateTime Sys_CreatedDateTime)
        {
            var record = _dbset.FirstOrDefault(m => (m.iEmployeeId == iEmployeeId) && (DbFunctions.TruncateTime(m.Sys_CreatedDateTime) == DbFunctions.TruncateTime(Sys_CreatedDateTime)));

            if (record == null)
            {
                record = new tEmployeeAttendance
                {
                    iEmployeeId = iEmployeeId,
                    dDateTime = Sys_CreatedDateTime,
                    Sys_CreatedBy = iEmployeeId,
                    Sys_CreatedDateTime = Sys_CreatedDateTime
                };

                _dbset.Add(record);
            }

            return record;
        }
        #endregion
    }
}
