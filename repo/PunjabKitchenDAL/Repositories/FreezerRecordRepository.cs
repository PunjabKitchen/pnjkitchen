﻿using System.Linq;
using System.Data.Entity;
using PunjabKitchenDAL.Interfaces;
using PunjabKitchenEntities.Common;
using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchenDAL.Repositories
{
    public class FreezerRecordRepository : GenericRepository<tFreezerRecord>, IFreezerRecordRepository
    {
        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="context"></param>
        public FreezerRecordRepository(DbContext context) : base(context)
        {
        }
        #endregion

        #region Get By Id
        /// <summary>
        /// Get By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>tFreezerRecord</returns>
        public tFreezerRecord GetById(long id)
        {
            return _dbset.FirstOrDefault(x => x.iFreezerRecordId == id);
        }
        #endregion
    }
}
