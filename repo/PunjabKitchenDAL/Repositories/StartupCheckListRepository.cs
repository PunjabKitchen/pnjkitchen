﻿using System.Linq;
using System.Data.Entity;
using PunjabKitchenDAL.Interfaces;
using PunjabKitchenEntities.Common;
using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchenDAL.Repositories
{
    public class StartupCheckListRepository : GenericRepository<tStartUpCheckList>, IStartupCheckListRepository
    {
        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="context"></param>
        public StartupCheckListRepository(DbContext context) : base(context)
        {
        }
        #endregion

        #region Get By Id
        /// <summary>
        /// Get By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>tStartUpCheckList</returns>
        public tStartUpCheckList GetById(long id)
        {
            return _dbset.FirstOrDefault(x => x.iStartUpCheckListId == id);
        }

        #endregion
    }
}
