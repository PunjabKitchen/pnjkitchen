﻿using System.Data.Entity;
using System.Linq;
using PunjabKitchenDAL.Interfaces;
using PunjabKitchenEntities.Common;
using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchenDAL.Repositories
{
    public class PortionWeightsCheckRecordDetailRepository : GenericRepository<tPortionWeightCheckDetail>, IPortionWeightsCheckRecordDetailRepository
    {
        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="context"></param>
        public PortionWeightsCheckRecordDetailRepository(DbContext context) : base(context)
        {
        }
        #endregion

        #region Get by Id
        /// <summary>
        /// GetById
        /// </summary>
        /// <param name="id"></param>
        /// <returns>tPortionWeightCheckDetail</returns>
        public tPortionWeightCheckDetail GetById(long id)
        {
            return _dbset.FirstOrDefault(x => x.iPortionWeightCheckDetailId == id);
        }
        #endregion
    }
}
