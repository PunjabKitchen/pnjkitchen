﻿using System.Linq;
using System.Data.Entity;
using PunjabKitchenDAL.Interfaces;
using PunjabKitchenEntities.Common;
using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchenDAL.Repositories
{
    public class ProductTypeRepository : GenericRepository<tProductType>, IProductTypeRepository
    {
        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="context"></param>
        public ProductTypeRepository(DbContext context) : base(context)
        {
        }
        #endregion

        #region Get By Id
        /// <summary>
        /// Get By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>tProductType</returns>
        public tProductType GetById(long id)
        {
            return _dbset.FirstOrDefault(x => x.iProductTypeId == id);
        }
        #endregion

        #region Get Prefix
        /// <summary>
        /// Get Prefix By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>string</returns>
        public string GetPrefixById(int id)
        {
            return GetById(id).vPrefixCode;
        } 
        #endregion
    }
}
