﻿using System.Data.Entity;
using PunjabKitchenDAL.Interfaces;
using PunjabKitchenEntities.Common;
using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchenDAL.Repositories
{
    public class EventRepository : GenericRepository<Event>, IEventRepository
    {
        #region Constructor
        public EventRepository(DbContext context) : base(context)
        { }
        #endregion
    }
}
