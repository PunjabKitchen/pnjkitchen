﻿using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Text.RegularExpressions;
using System.Web.WebPages;
using PunjabKitchenDAL.Interfaces;
using PunjabKitchenEntities.Common;
using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchenDAL.Repositories
{
    public class ProductRepository : GenericRepository<tProduct>, IProductRepository
    {

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="context"></param>
        public ProductRepository(DbContext context) : base(context)
        {
        }
        #endregion

        #region Get By Id
        /// <summary>
        /// Get By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>tProduct</returns>
        public tProduct GetById(long id)
        {
            return _dbset.FirstOrDefault(x => x.iProductId == id);
        }
        #endregion

        #region Get: By Code

        /// <summary>
        ///  Get By Code
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public IEnumerable<tProduct> GetByCode(string code)
        {
            var distinctList = _dbset.GroupBy(x => x.vProductCode).Select(y => y.FirstOrDefault()).Distinct();
            var list = distinctList.Where(x => x.vProductCode.Contains(code)).ToList();
            return list;
        }

        #endregion

        #region Product Code
        /// <summary>
        /// Get Product Code 
        /// </summary>
        /// <param name="productTypeId"></param>
        /// <param name="productCodePrefix"></param>
        /// <returns>string</returns>
        public string GetProductCode(int productTypeId, string productCodePrefix)
        {
            var result = _dbset.Where(x => x.iProductTypeId != null && x.iProductTypeId == productTypeId).ToList();
            var numPart = result.Count > 0 ? Regex.Match(result[result.Count - 1].vProductCode, @"\d+").Value.AsInt() + 1 : 1;
            var code = productCodePrefix + numPart.ToString("D" + 4);
            return code;
        }

        /// <summary>
        /// Validate Product Code
        /// </summary>
        /// <param name="code"></param>
        /// <returns>bool</returns>
        public bool ValidateCode(string code)
        {
            return _dbset.Any(x => x.vProductCode == code);
        } 
        #endregion
    }
}
