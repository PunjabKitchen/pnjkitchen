﻿using System.Data.Entity;
using System.Linq;
using PunjabKitchenDAL.Interfaces;
using PunjabKitchenEntities.Common;
using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchenDAL.Repositories
{
    public class ShapesControlRecordRepository : GenericRepository<tShapeControlRecord>, IShapesControlRecordRepository
    {
        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="context"></param>
        public ShapesControlRecordRepository(DbContext context) : base(context)
        {
        }
        #endregion

        #region Get by Id
        /// <summary>
        /// GetById
        /// </summary>
        /// <param name="id"></param>
        /// <returns>tShapeControlRecord</returns>
        public tShapeControlRecord GetById(long id)
        {
            return _dbset.FirstOrDefault(x => x.iShapeControlRecordId == id);
        }
        #endregion
    }
}