﻿using System.Linq;
using System.Data.Entity;
using PunjabKitchenDAL.Interfaces;
using PunjabKitchenEntities.Common;
using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchenDAL.Repositories
{
    class RiceCoolingTempRecordRepository : GenericRepository<tRiceCoolingTempRecord>, IRiceCoolingTempRecordRepository
    {
        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="context"></param>
        public RiceCoolingTempRecordRepository(DbContext context) : base(context)
        {
        }
        #endregion

        #region Get By Id
        /// <summary>
        /// Get By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>tRiceCoolingTempRecord</returns>
        public tRiceCoolingTempRecord GetById(long id)
        {
            return _dbset.FirstOrDefault(x => x.iRiceCoolingTempRecordId == id);
        }
        #endregion
    }
}
