﻿using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using PunjabKitchenDAL.Interfaces;
using PunjabKitchenEntities.Common;
using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchenDAL.Repositories
{
    public class AreaItemRepository : GenericRepository<tAreaItem>, IAreaItemRepository
    {
        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="context"></param>
        public AreaItemRepository(DbContext context) : base(context)
        {
        }
        #endregion

        #region Get By Id
        
        /// <summary>
        /// Get By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>tAreaItem</returns>
        public tAreaItem GetById(long id)
        {
            return _dbset.FirstOrDefault(x => x.iAreaItemId == id);
        }

        /// <summary>
        /// Get Item By Area Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>List of tAreaItem</returns>
        public List<tAreaItem> GetItemByAreaId(long id)
        {
            return _dbset.Where(x => x.iAreaId == id).ToList();
        }
        
        #endregion
    }
}
