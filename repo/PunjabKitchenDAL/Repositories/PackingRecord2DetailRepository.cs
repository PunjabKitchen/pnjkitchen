﻿using System.Data.Entity;
using System.Linq;
using PunjabKitchenDAL.Interfaces;
using PunjabKitchenEntities.Common;
using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchenDAL.Repositories
{
    public class PackingRecord2DetailRepository : GenericRepository<tPackingRecord2Detail>, IPackingRecord2DetailRepository
    {
        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="context"></param>
        public PackingRecord2DetailRepository(DbContext context) : base(context)
        {
        }
        #endregion

        #region Get by Id
        /// <summary>
        /// Get By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>tPackingRecord2Detail</returns>
        public tPackingRecord2Detail GetById(long id)
        {
            return _dbset.FirstOrDefault(x => x.iPackingRecord2Detail == id);
        }
        #endregion
    }
}
