﻿using System.Linq;
using System.Data.Entity;
using PunjabKitchenDAL.Interfaces;
using PunjabKitchenEntities.Common;
using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchenDAL.Repositories
{
    class MeatMincingRecordRepository : GenericRepository<tMeatMincingRecord>, IMeatMincingRecordRepository
    {
        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="context"></param>
        public MeatMincingRecordRepository(DbContext context) : base(context)
        {
        }
        #endregion

        #region Get By Id
        /// <summary>
        /// Get By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>tMeatMincingRecord</returns>
        public tMeatMincingRecord GetById(long id)
        {
            return _dbset.FirstOrDefault(x => x.iMeatMincingRecordId == id);
        }
        #endregion
    }
}
