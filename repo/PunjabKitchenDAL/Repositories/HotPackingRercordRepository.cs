﻿using System.Linq;
using System.Data.Entity;
using PunjabKitchenDAL.Interfaces;
using PunjabKitchenEntities.Common;
using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchenDAL.Repositories
{
    public class HotPackingRercordRepository : GenericRepository<tHotPackingRercord>, IHotPackingRercordRepository
    {
        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="context"></param>
        public HotPackingRercordRepository(DbContext context) : base(context)
        {
        }
        #endregion

        #region Get By Id
        /// <summary>
        /// Get By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>tHotPackingRercord</returns>
        public tHotPackingRercord GetById(long id)
        {
            return _dbset.FirstOrDefault(x => x.iHotPackingRercordId == id);
        } 
        #endregion
    }
}