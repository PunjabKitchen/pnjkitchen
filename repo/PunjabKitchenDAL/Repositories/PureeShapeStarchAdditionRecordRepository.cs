﻿using System.Linq;
using System.Data.Entity;
using PunjabKitchenDAL.Interfaces;
using PunjabKitchenEntities.Common;
using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchenDAL.Repositories
{
    public class PureeShapesStarchAdditionRecordRepository : GenericRepository<tPureeShapesStarchAdditionRecord>, IPureeShapeStarchAdditionRecordRepository
    {
        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="context"></param>
        public PureeShapesStarchAdditionRecordRepository(DbContext context) : base(context)
        {
        }
        #endregion

        #region Get By Id
        /// <summary>
        /// Get By ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns>tPureeShapesStarchAdditionRecord</returns>
        public tPureeShapesStarchAdditionRecord GetById(long id)
        {
            return _dbset.FirstOrDefault(x => x.iPureeShapeStarchRecordAdditionId == id);
        } 
        #endregion
    }
}
