﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using PunjabKitchenDAL.Interfaces;
using PunjabKitchenEntities.Common;
using PunjabKitchenEntities.EntityModel;
// ReSharper disable All

namespace PunjabKitchenDAL.Repositories
{
    public class StartupChecksEPGoodsOutRepository : GenericRepository<tStartupChecksEPGoodsOut>, IStartupChecksEPGoodsOutRepository
    {
        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="context"></param>
        public StartupChecksEPGoodsOutRepository(DbContext context) : base(context)
        {
        }
        #endregion

        #region Get By Id
        /// <summary>
        /// Get By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>tStartupChecksEPGoodsOut</returns>
        public tStartupChecksEPGoodsOut GetById(long id)
        {
            return _dbset.FirstOrDefault(x => x.iStartupChecksEPGoodsOut == id);
        }
        #endregion

        #region Get Startup Checks EP Goods Out History
        /// <summary>
        /// Get Goods Out Freezer Unit History
        /// </summary>
        /// <param name="startDateTime"></param>
        /// <param name="endDateTime"></param>
        /// <returns>IEnumerable of tStartupChecksEPGoodsOut</returns>
        public IEnumerable<tStartupChecksEPGoodsOut> GetStartupChecksEPGoodsOutHistory(DateTime startDateTime, DateTime endDateTime)
        {
            var list =
                _dbset.Where(x => DbFunctions.TruncateTime(x.Sys_CreatedDateTime.Value) >= DbFunctions.TruncateTime(startDateTime) && DbFunctions.TruncateTime(x.Sys_CreatedDateTime.Value) <= DbFunctions.TruncateTime(endDateTime)).ToList();
            return list;
        }
        #endregion

        #region Get Startup Check EP Goods Out History By ID
        /// <summary>
        /// Get Startup Check EP Goods Out History By ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns>tStartupChecksEPGoodsOut</returns>
        public tStartupChecksEPGoodsOut GetStartupChecksEpGoodsOutHistoryById(long id)
        {
            var dbItem = _dbset.FirstOrDefault(x => x.iStartupChecksEPGoodsOut == id);

            return dbItem;
        }
        #endregion
    }
}
