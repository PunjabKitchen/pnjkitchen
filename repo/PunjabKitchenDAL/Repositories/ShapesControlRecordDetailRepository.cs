﻿
using System.Data.Entity;
using System.Linq;
using PunjabKitchenDAL.Interfaces;
using PunjabKitchenEntities.Common;
using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchenDAL.Repositories
{
    public class ShapesControlRecordDetailRepository : GenericRepository<tShapeControlRecordDetail>, IShapesControlRecordDetailRepository
    {
        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="context"></param>
        public ShapesControlRecordDetailRepository(DbContext context) : base(context)
        {
        }
        #endregion

        #region Get by Id
        /// <summary>
        /// GetById
        /// </summary>
        /// <param name="id"></param>
        /// <returns>tShapeControlRecordDetail</returns>
        public tShapeControlRecordDetail GetById(long id)
        {
            return _dbset.FirstOrDefault(x => x.iShapeControlRecordDetailId == id);
        }
        #endregion
    }
}