﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using PunjabKitchenDAL.Interfaces;
using PunjabKitchenEntities.Common;
using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchenDAL.Repositories
{
    public class ShoeCleaningCheckRepository : GenericRepository<tShoeCleaningRecrdWeekly>, IShoeCleaningCheckRepository
    {
        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="context"></param>
        public ShoeCleaningCheckRepository(DbContext context) : base(context)
        {
        }
        #endregion

        #region Get By Id
        /// <summary>
        /// Get By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>tShoeCleaningRecrdWeekly</returns>
        public tShoeCleaningRecrdWeekly GetById(long id)
        {
            return _dbset.FirstOrDefault(x => x.iShoeCleaningRecordId == id);
        }

        /// <summary>
        /// CheckWeeklyRecord
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="userId"></param>
        /// <returns>bool</returns>
        public bool CheckWeeklyRecord(DateTime startDate, string userId)
        {
            DateTime endDate = startDate.AddDays(7);

            var result = _dbset.Any(x => x.dCheckPerformDate >= startDate.Date && x.dCheckPerformDate <= endDate.Date && x.iUserId == userId);
            return result;

        }
        #endregion

        #region Get Shoes Cleaning History
        /// <summary>
        /// Get Shoes Cleaning History
        /// </summary>
        /// <param name="startDateTime"></param>
        /// <param name="endDateTime"></param>
        /// <returns>IEnumerable of tShoeCleaningRecrdWeekly</returns>
        public IEnumerable<tShoeCleaningRecrdWeekly> GetShoesCleaningHistory(DateTime startDateTime, DateTime endDateTime)
        {
            var list =
                _dbset.Where(x => DbFunctions.TruncateTime(x.Sys_CreatedDateTime) >= DbFunctions.TruncateTime(startDateTime) && DbFunctions.TruncateTime(x.Sys_CreatedDateTime) <= DbFunctions.TruncateTime(endDateTime));
            return list;
        }

        #endregion

        #region Check Shoes Cleaning Status
        /// <summary>
        /// Check Shoes Cleaning Status
        /// </summary>
        /// <param name="startDateTime"></param>
        /// <param name="endDateTime"></param>
        /// <param name="userId"></param>
        /// <returns>bool</returns>
        public bool IsCheckPerformed(DateTime startDateTime, DateTime endDateTime, string userId)
        {
            var item =
                _dbset.Where(
                    x =>
                        DbFunctions.TruncateTime(x.dCheckPerformDate) >= DbFunctions.TruncateTime(startDateTime) &&
                        DbFunctions.TruncateTime(x.dCheckPerformDate) <= DbFunctions.TruncateTime(endDateTime) && 
                        x.Sys_CreatedBy == userId).ToList();
            return item.Count <= 0;
        }
        #endregion

        #region Get Shoe Cleaning Checklist History By Id

        /// <summary>
        /// GetShoeCleaningChecklistHistoryById
        /// </summary>
        /// <param name="id"></param>
        /// <returns>tShoeCleaningRecrdWeekly</returns>
        public tShoeCleaningRecrdWeekly GetShoeCleaningChecklistHistoryById(long id)
        {
            var dbItem = _dbset.FirstOrDefault(x => x.iShoeCleaningRecordId == id);
            return dbItem;
        }
        #endregion
    }
}
