﻿using System.Data.Entity;
using System.Linq;
using PunjabKitchenDAL.Interfaces;
using PunjabKitchenEntities.Common;
using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchenDAL.Repositories
{
    public class PackingRecordDetailRepository : GenericRepository<tPackingRecordDetail>, IPackingRecordDetailRepository
    {
        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="context"></param>
        public PackingRecordDetailRepository(DbContext context) : base(context)
        {
        }
        #endregion

        #region Get by Id
        /// <summary>
        /// Get By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>tPackingRecordDetail</returns>
        public tPackingRecordDetail GetById(long id)
        {
            return _dbset.FirstOrDefault(x => x.iPackingRecordDetailId == id);
        }
        #endregion
    }
}