﻿using System.Linq;
using System.Data.Entity;
using PunjabKitchenDAL.Interfaces;
using System.Collections.Generic;
using PunjabKitchenEntities.Common;
using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchenDAL.Repositories
{
    public class StockRepository : GenericRepository<tStock>, IStockRepository
    {
        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="context"></param>
        public StockRepository(DbContext context) : base(context)
        {
        }
        #endregion

        #region Get All Distinct Stock Locations
        /// <summary>
        /// Get All Distinct Stock Locations
        /// </summary>
        /// <returns>List of tStock</returns>
        public List<tStock> GetAllDistinctStockLocations()
        {
            var stockLocations = _dbset.Where(x => x.iProductId == null || x.iQuantity == 0).GroupBy(x => x.vStorageLocation).Select(y => y.FirstOrDefault()).Distinct().ToList();
            return stockLocations;
        }

        #endregion

        #region Get All Distinct Stock Points

        /// <summary>
        /// Get All Distinct Stock Points
        /// </summary>
        /// <param name="stockLoc"></param>
        /// <returns>List of tStock</returns>
        public List<tStock> GetAllDistinctStockPoints(string stockLoc)
        {
            var stockPoints = _dbset.Where(x => x.vStorageLocation == stockLoc && (x.iProductId == null || x.iQuantity == 0)).ToList();
            return stockPoints;
        }

        #endregion

        #region Get Stock By Id
        /// <summary>
        /// Get Stock By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>tStock</returns>
        public tStock GetById(long id)
        {
            return _dbset.FirstOrDefault(x => x.iStockId == id);
        }
        #endregion

        #region Validate Stock Location and Stock Point
        /// <summary>
        /// Validate Stock Location and Stock Point To Avoid Duplication
        /// </summary>
        /// <param name="stock"></param>
        /// <returns>bool</returns>
        public bool validateStockPoint(tStock stock)
        {
            var stockLoc = _dbset.Where(x => x.vStorageLocation.ToLower() == stock.vStorageLocation.ToLower() &&
                                        x.vStoragePoint.ToLower() == stock.vStoragePoint.ToLower() && x.iStockId!=stock.iStockId);
            if (stockLoc.ToList().Count == 0)
            {
                return true;
            }
            return false;
        }
        #endregion

        #region Get Allowed Stock Quanity

        /// <summary>
        /// Get Allowed Stock Quanity
        /// </summary>
        /// <param name="productCode"></param>
        /// <returns>int</returns>
        public int GetAllowedStockQuanity(string productCode)
        {
            var items = _dbset.Where(x => x.tProduct.vProductCode == productCode);
            var sumQty = items.Sum(x => x.iQuantity);
            return sumQty ?? 0;
        }
        #endregion

        #region Get All Stock By Product Code

        /// <summary>
        /// Get All Stock By Product Code
        /// </summary>
        /// <param name="productCode"></param>
        /// <returns>List of tStock</returns>
        public List<tStock> GetStockByProductCode(string productCode)
        {
            var items = _dbset.Where(x => x.tProduct != null && x.iQuantity > 0 && x.tProduct.vProductCode == productCode).ToList();
            return items;
        }
        #endregion

        #region Get All Stock By Product Id

        /// <summary>
        /// Get All Stock By Product id
        /// </summary>
        /// <param name="productId"></param>
        /// <returns>tStock</returns>
        public tStock GetStockByProductId(int productId)
        {
            var items = _dbset.FirstOrDefault(x => x.tProduct != null && x.iQuantity > 0 && x.tProduct.iProductId == productId);
            return items;
        }
        #endregion
    }
}
