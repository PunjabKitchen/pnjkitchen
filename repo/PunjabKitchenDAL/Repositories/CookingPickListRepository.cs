﻿using PunjabKitchenDAL.Interfaces;
using PunjabKitchenEntities.Common;
using PunjabKitchenEntities.EntityModel;
using System.Data.Entity;
using System.Linq;

namespace PunjabKitchenDAL.Repositories
{
    class CookingPickListRepository : GenericRepository<tPickList>, ICookingPickListRepository
    {
        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="context"></param>
        public CookingPickListRepository(DbContext context) : base(context)
        {
        }
        #endregion

        #region Get By Id
        /// <summary>
        /// Get By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>tPickList</returns>
        public tPickList GetById(long id)
        {
            return _dbset.FirstOrDefault(x => x.iPickListId == id);
        }
        #endregion
    }
}
