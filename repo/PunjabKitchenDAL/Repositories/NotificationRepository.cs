﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using PunjabKitchenDAL.Interfaces;
using PunjabKitchenEntities.Common;
using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchenDAL.Repositories
{
    public class NotificationRepository : GenericRepository<tNotification>, INotificationRepository
    {
        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="context"></param>
        public NotificationRepository(DbContext context) : base(context)
        {
        }
        #endregion

        #region Get Notifications Count
        /// <summary>
        /// Get Notifications Count
        /// </summary>
        /// <returns>int</returns>
        public int GetNotificationsCount()
        {
            return _dbset.Count();
        }
        #endregion

        #region Get Notification By Id
        /// <summary>
        /// Get Notification By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>tNotification</returns>
        tNotification INotificationRepository.GetById(long id)
        {
            return _dbset.FirstOrDefault(x => x.iNotificationId == id);
        }
        #endregion

        #region Check if Notification can be assigned to user
        /// <summary>
        /// //Check if Notification can be assigned to user
        /// </summary>
        /// <param name="notificationId"></param>
        /// <returns>bool</returns>
        public bool CanAssignNotificationToUser(int notificationId)
        {
            var dbNotification = _dbset.FirstOrDefault(x => x.iNotificationId == notificationId);

            //Condition: Goods Out Freezer Unit will be checked twice in a single day.
            if ((dbNotification != null) && (dbNotification.vSubject.Equals("Goods Out Freezer Unit Check")))
            { return true; }

            return dbNotification != null && dbNotification.iWorkStarterId == null;           
        }

        #endregion

        #region Get Notifications By date

        /// <summary>
        /// Get Notifications By date
        /// </summary>
        /// <param name="dateTime"></param>
        /// <returns>IEnumerable of tNotification</returns>
        public IEnumerable<tNotification> GetNotificationByDate(DateTime dateTime)
        {
            var notifications = _dbset.Where(x => DbFunctions.TruncateTime(x.Sys_CreatedDateTime) == dateTime.Date);
            return notifications;
        }


        #endregion

        #region Get Latest Notification By Notification Type

        /// <summary>
        /// Get Latest Notification By Notification Type
        /// </summary>
        /// <param name="notificationTypeId"></param>
        /// <returns>tNotification</returns>
        public tNotification GetLatestNotificationByType(int notificationTypeId)
        {
            if(_dbset.Any(x => x.iNotificationTypeId == notificationTypeId))
            {
                var notification = _dbset.Where(x => x.iNotificationTypeId == notificationTypeId).OrderByDescending(x => x.Sys_CreatedDateTime).FirstOrDefault();
                return notification;
            }

            return null;
        }
        #endregion
    }
}
