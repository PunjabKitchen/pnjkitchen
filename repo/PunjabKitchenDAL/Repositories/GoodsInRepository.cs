﻿using System.Linq;
using System.Data.Entity;
using PunjabKitchenDAL.Interfaces;
using PunjabKitchenEntities.Common;
using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchenDAL.Repositories
{
    public class GoodsInRepository : GenericRepository<tGoodsIn>, IGoodsInRepository
    {
        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="context"></param>
        public GoodsInRepository(DbContext context) : base(context)
        {
        }
        #endregion

        #region Get By Id
        /// <summary>
        /// Get By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>tGoodsIn</returns>
        public tGoodsIn GetById(long id)
        {
            return _dbset.FirstOrDefault(x => x.tGoodsInId == id);
        }
        #endregion
    }
}
