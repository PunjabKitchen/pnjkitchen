﻿using System.Linq;
using System.Data.Entity;
using PunjabKitchenDAL.Interfaces;
using PunjabKitchenEntities.Common;
using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchenDAL.Repositories
{
    public class PickListRepository : GenericRepository<tPickList>, IPickListRepository
    {
        #region Contructor
        /// <summary>
        /// Contructor
        /// </summary>
        /// <param name="context"></param>
        public PickListRepository(DbContext context) : base(context)
        {
        } 
        #endregion
    }
}
