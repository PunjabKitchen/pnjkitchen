﻿using System.Linq;
using System.Data.Entity;
using PunjabKitchenDAL.Interfaces;
using System.Collections.Generic;
using System;
using PunjabKitchenEntities.Common;
using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchenDAL.Repositories
{
    public class StartUpcheckDailyRepository : GenericRepository<tStartUpcheckDaily>, IStartUpcheckDailyRepository
    {
        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="context"></param>
        public StartUpcheckDailyRepository(DbContext context) : base(context)
        {
        }
        #endregion

        #region Get By id
        /// <summary>
        /// Get By id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public tStartUpcheckDaily GetById(long id)
        {
            return _dbset.Where(x => x.iStartUpCheckListId == id).FirstOrDefault();
        }
        #endregion

        #region Get Startup Checklist - LR Cooking Room History

        /// <summary>
        /// //Get Startup Checklist - LR Cooking Room History
        /// </summary>
        /// <param name="startDateTime"></param>
        /// <param name="endDateTime"></param>
        /// <returns>IEnumerable of tStartUpcheckDaily</returns>
        public IEnumerable<tStartUpcheckDaily> GetStartupChecklistHistory(DateTime startDateTime, DateTime endDateTime)
        {
            var list =
                _dbset.Where(x => DbFunctions.TruncateTime(x.Sys_CreatedDateTime.Value) >= DbFunctions.TruncateTime(startDateTime)
                && DbFunctions.TruncateTime(x.Sys_CreatedDateTime.Value) <= DbFunctions.TruncateTime(endDateTime)).ToList();
            return list;
        }
        #endregion

        #region Get Startup Checklist - LR Cooking Room History By Id

        /// <summary>
        /// //Get Startup Checklist - LR Cooking Room History By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>IEnumerable of tStartUpcheckDaily</returns>
        public IEnumerable<tStartUpcheckDaily> GetStartupChecklistHistoryById(long id)
        {
            var dbItem = _dbset.FirstOrDefault(x => x.iStartUpcheckDailyId == id);

            var list =
                _dbset.Where(x => DbFunctions.TruncateTime(x.dDateTime.Value) == DbFunctions.TruncateTime(dbItem.dDateTime)).ToList();
            return list;
        }
        #endregion
    }
}
