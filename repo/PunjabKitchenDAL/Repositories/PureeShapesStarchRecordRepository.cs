﻿using System.Linq;
using System.Data.Entity;
using PunjabKitchenDAL.Interfaces;
using PunjabKitchenEntities.Common;
using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchenDAL.Repositories
{
    public class PureeShapesStarchRecordRepository : GenericRepository<tPureeShapesStarchRecord>, IPureeShapesStarchRecordRepository
    {
        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="context"></param>
        public PureeShapesStarchRecordRepository(DbContext context) : base(context)
        {
        }
        #endregion

        #region Get By Id
        /// <summary>
        /// Get By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>tPureeShapesStarchRecord </returns>
        public tPureeShapesStarchRecord GetById(long id)
        {
            return _dbset.FirstOrDefault(x => x.iPureeShapeStarchRecordId == id);
        } 
        #endregion
    }
}
