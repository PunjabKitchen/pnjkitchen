﻿using System.Linq;
using System.Data.Entity;
using PunjabKitchenDAL.Interfaces;
using PunjabKitchenEntities.Common;
using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchenDAL.Repositories
{
    class SlicingRecordRepository : GenericRepository<tSlicingRecord>, ISlicingRecordRepository
    {
        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="context"></param>
        public SlicingRecordRepository(DbContext context) : base(context)
        {
        }
        #endregion

        #region Get By Id
        /// <summary>
        /// Get By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>tSlicingRecord</returns>
        public tSlicingRecord GetById(long id)
        {
            return _dbset.FirstOrDefault(x => x.iSlicingRecordId == id);
        }
        #endregion
    }
}
