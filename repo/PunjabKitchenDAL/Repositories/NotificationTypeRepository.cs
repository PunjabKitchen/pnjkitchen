﻿using System.Data.Entity;
using PunjabKitchenDAL.Interfaces;
using PunjabKitchenEntities.Common;
using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchenDAL.Repositories
{
    public class NotificationTypeRepository : GenericRepository<tNotificationType>, INotificationTypeRepository
    {
        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="context"></param>
        public NotificationTypeRepository(DbContext context) : base(context)
        {
        } 
        #endregion
    }
}
