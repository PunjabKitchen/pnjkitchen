﻿using System.Linq;
using System.Data.Entity;
using PunjabKitchenDAL.Interfaces;
using PunjabKitchenEntities.Common;
using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchenDAL.Repositories
{
    public class PureeMealBatchAssemblyRepository : GenericRepository<tPureeMeanBatchAssembly>, IPureeMealBatchAssemblyRepository
    {
        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="context"></param>
        public PureeMealBatchAssemblyRepository(DbContext context) : base(context)
        {
        }
        #endregion

        #region Get By Id
        /// <summary>
        /// Get By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>tPureeMeanBatchAssembly</returns>
        public tPureeMeanBatchAssembly GetById(long id)
        {
            return _dbset.FirstOrDefault(x => x.iPureeMeanBatchAssemblyId == id);
        } 
        #endregion
    }
}