﻿using System.Linq;
using System.Data.Entity;
using PunjabKitchenDAL.Interfaces;
using PunjabKitchenEntities.Common;
using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchenDAL.Repositories
{
    // ReSharper disable once InconsistentNaming
    public class FMDetectionAndPackWeightRecordRepository : GenericRepository<tFMDetectionAndPackWeightTest>, IFMDetectionAndPackWeightRecordRepository
    {
        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="context"></param>
        public FMDetectionAndPackWeightRecordRepository(DbContext context) : base(context)
        {
        }
        #endregion

        #region Get By Id
        /// <summary>
        /// Get By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>tFMDetectionAndPackWeightTest</returns>
        public tFMDetectionAndPackWeightTest GetById(long id)
        {
            return _dbset.FirstOrDefault(x => x.iDetectionPackWeightTestId == id);
        } 
        #endregion
    }
}
