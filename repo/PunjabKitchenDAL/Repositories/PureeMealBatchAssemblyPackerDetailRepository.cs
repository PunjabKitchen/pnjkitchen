﻿using System.Linq;
using System.Data.Entity;
using PunjabKitchenDAL.Interfaces;
using PunjabKitchenEntities.Common;
using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchenDAL.Repositories
{
    public class PureeMealBatchAssemblyPackerDetailRepository : GenericRepository<tPureeMealBatchAssemblyPackerDetail>, IPureeMealBatchAssemblyPackerDetailRepository
    {
        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="context"></param>
        public PureeMealBatchAssemblyPackerDetailRepository(DbContext context) : base(context)
        {
        }
        #endregion

        #region Get By Id
        /// <summary>
        /// Get By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>tPureeMealBatchAssemblyPackerDetail</returns>
        public tPureeMealBatchAssemblyPackerDetail GetById(long id)
        {
            return _dbset.FirstOrDefault(x => x.iPureeMealBatchAssemblyPackerDetailId == id);
        } 
        #endregion
    }
}