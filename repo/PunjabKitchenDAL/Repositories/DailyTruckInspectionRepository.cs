﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using PunjabKitchenDAL.Interfaces;
using PunjabKitchenEntities.Common;
using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchenDAL.Repositories
{
    public class DailyTruckInspectionRepository : GenericRepository<tDailyTruckInspection>, IDailyTruckInspectionRepository
    {
        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="context"></param>
        public DailyTruckInspectionRepository(DbContext context) : base(context)
        {
        }
        #endregion

        #region Get By Id
        /// <summary>
        /// Get By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>tDailyTruckInspection</returns>
        public tDailyTruckInspection GetById(long id)
        {
            return _dbset.FirstOrDefault(x => x.iTruckInspectionId == id);
        }
        #endregion

        #region Get Daily Truck Inspection History
        /// <summary>
        /// Get Get Daily Truck Inspection History
        /// </summary>
        /// <param name="startDateTime"></param>
        /// <param name="endDateTime"></param>
        /// <returns>IEnumerable of tDailyTruckInspection</returns>
        public IEnumerable<tDailyTruckInspection> GetDailyTruckInspectionHistory(DateTime startDateTime, DateTime endDateTime)
        {
            var list =
                _dbset.Where(x => DbFunctions.TruncateTime(x.Sys_CreatedDateTime.Value) >= DbFunctions.TruncateTime(startDateTime) && DbFunctions.TruncateTime(x.Sys_CreatedDateTime.Value) <= DbFunctions.TruncateTime(endDateTime)).ToList();
            return list;
        }
        #endregion

        #region Get Daily Truck Inspection History By ID
        /// <summary>
        /// Get Daily Truck Inspection History By ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns>tDailyTruckInspection</returns>
        public tDailyTruckInspection GetDailyTruckInspectionHistoryById(long id)
        {
            var dbItem = _dbset.FirstOrDefault(x => x.iTruckInspectionId == id);

            return dbItem;
        }
        #endregion
    }
}
