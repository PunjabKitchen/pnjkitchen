﻿using System.Data.Entity;
using PunjabKitchenDAL.Interfaces;
using PunjabKitchenEntities.Common;
using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchenDAL.Repositories
{
    public class StockTypeRepository : GenericRepository<tStockType>, IStockTypeRepository
    {
        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="context"></param>
        public StockTypeRepository(DbContext context) : base(context)
        {
        }
        #endregion
    }
}
