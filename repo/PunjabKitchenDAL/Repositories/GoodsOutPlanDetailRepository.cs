﻿using System.Linq;
using System.Data.Entity;
using PunjabKitchenDAL.Interfaces;
using PunjabKitchenEntities.Common;
using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchenDAL.Repositories
{
    public class GoodsOutPlanDetailRepository : GenericRepository<tGoodsOutPlanDetail>, IGoodsOutPlanDetailRepository
    {
        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="context"></param>
        public GoodsOutPlanDetailRepository(DbContext context) : base(context)
        {
        }
        #endregion

        #region Get By Id
        /// <summary>
        /// Get By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>tGoodsOutPlanDetail</returns>
        public tGoodsOutPlanDetail GetById(long id)
        {
            return _dbset.FirstOrDefault(x => x.iGoodsOutPlanDetailId == id);
        }
        #endregion
    }
}
