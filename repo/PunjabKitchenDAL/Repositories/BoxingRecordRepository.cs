﻿using System.Linq;
using System.Data.Entity;
using System.Text.RegularExpressions;
using System.Web.WebPages;
using PunjabKitchenDAL.Interfaces;
using PunjabKitchenEntities.Common;
using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchenDAL.Repositories
{
    public class BoxingRecordRepository : GenericRepository<tBoxingRecord>, IBoxingRecordRepository
    {
        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="context"></param>
        public BoxingRecordRepository(DbContext context) : base(context)
        {
        }
        #endregion

        #region Get By Id
        /// <summary>
        /// Get By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>tBoxingRecord</returns>
        public tBoxingRecord GetById(long id)
        {
            return _dbset.FirstOrDefault(x => x.iBoxingRecordId == id);
        }

        #endregion

        #region Get Batch Code

        /// <summary>
        /// Get Batch Code
        /// </summary>
        /// <returns>string</returns>
        public string GetBatchCode()
        {
            var result =
                _dbset.OrderByDescending(x => x.iBoxingRecordId).FirstOrDefault(x => x.vBatchNo != null);
            var numPart = result != null ? Regex.Match(result.vBatchNo, @"\d+").Value.AsInt() + 1 : 1;
            var code = numPart.ToString("D" + 6);
            return code;
        }

        #endregion

        #region Validate Batch Code

        /// <summary>
        /// Validate Batch Code
        /// </summary>
        /// <param name="batchCode">batch code to validate</param>
        /// <returns>bool</returns>
        public bool ValidateBatchCode(string batchCode)
        {
            return _dbset.Any(x => x.vBatchNo == batchCode);
        }

        #endregion

    }
}
