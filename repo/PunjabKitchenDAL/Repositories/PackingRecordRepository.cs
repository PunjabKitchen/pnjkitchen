﻿using System.Data.Entity;
using System.Linq;
using PunjabKitchenDAL.Interfaces;
using PunjabKitchenEntities.Common;
using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchenDAL.Repositories
{
    public class PackingRecordRepository : GenericRepository<tPackingRecord>, IPackingRecordRepository
    {
        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="context"></param>
        public PackingRecordRepository(DbContext context) : base(context)
        {
        }
        #endregion

        #region Get by Id
        /// <summary>
        /// Get By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>tPackingRecord</returns>
        public tPackingRecord GetById(long id)
        {
            return _dbset.FirstOrDefault(x => x.iPackingRecordId == id);
        }
        #endregion
    }
}