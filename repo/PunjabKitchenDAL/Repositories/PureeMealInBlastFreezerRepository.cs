﻿using System.Linq;
using System.Data.Entity;
using PunjabKitchenDAL.Interfaces;
using PunjabKitchenEntities.Common;
using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchenDAL.Repositories
{
    public class PureeMealInBlastFreezerRepository : GenericRepository<tPureeMealInBlastFreezer>, IPureeMealInBlastFreezerRepository
    {
        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="context"></param>
        public PureeMealInBlastFreezerRepository(DbContext context) : base(context)
        {
        }
        #endregion

        #region Get By Id
        /// <summary>
        /// Get By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>tPureeMealInBlastFreezer</returns>
        public tPureeMealInBlastFreezer GetById(long id)
        {
            return _dbset.FirstOrDefault(x => x.iPureeMealInBlastFreezerId == id);
        } 
        #endregion
    }
}