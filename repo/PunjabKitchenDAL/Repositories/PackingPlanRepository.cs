﻿using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Text.RegularExpressions;
using System.Web.WebPages;
using PunjabKitchenDAL.Interfaces;
using PunjabKitchenEntities.Common;
using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchenDAL.Repositories
{
    public class PackingPlanRepository : GenericRepository<tPackingProductionPlan>, IPackingPlanRepository
    {
        #region Contructor
        /// <summary>
        /// Contructor
        /// </summary>
        /// <param name="context"></param>
        public PackingPlanRepository(DbContext context) : base(context)
        {
        }
        #endregion

        #region Get By Id
        /// <summary>
        /// Get By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>tPackingProductionPlan</returns>
        public tPackingProductionPlan GetById(long id)
        {
            return _dbset.FirstOrDefault(x => x.iPackingProductionPlanId == id);
        }
        #endregion


        #region Get Packing Plans by Line Id
        /// <summary>
        /// Get packing Plan by line id
        /// </summary>
        /// <param name="lineId"></param>
        /// <returns>List of tPackingProductionPlan</returns>
        public List<tPackingProductionPlan> GetPackingPlansByLineId(int lineId)
        {
            var result = _dbset
                .Where(x => x.iLineNo == lineId)
                .Include("tCookingPlan")
                .OrderByDescending(x => x.dPackingPlan)
                .ToList();

            return result;
        }

        /// <summary>
        /// Get PackingPlans By LineId
        /// </summary>
        /// <param name="isLine1"></param>
        /// <param name="isLine2"></param>
        /// <param name="isLine3"></param>
        /// <returns>List of tPackingProductionPlan</returns>
        public List<tPackingProductionPlan> GetPackingPlansByLineId(bool isLine1, bool isLine2, bool isLine3)
        {
            var result =
                _dbset.Where(x => (x.iLineNo == 1 && isLine1)
            || (x.iLineNo == 2 && isLine2)
            || (x.iLineNo == 3 && isLine3)).Include("tCookingPlan").ToList();

            var t = result.AsEnumerable()
                .GroupBy(x => x.dPackingPlan)
                .Select(x => x.OrderByDescending(c => c.dPackingPlan))
                .OrderByDescending(v => v.First().dPackingPlan).
                SelectMany(x => x).ToList();


            return t;
        } 
        #endregion

        #region Get By Code
        /// <summary>
        /// Get By Code
        /// </summary>
        /// <param name="code"></param>
        /// <returns>IEnumerable of tPackingProductionPlan</returns>
        public IEnumerable<tPackingProductionPlan> GetByCode(string code)
        {
            IEnumerable<tPackingProductionPlan> list;
            var distinctList = _dbset.GroupBy(x => x.vBatchNo).Select(y => y.FirstOrDefault()).Distinct();
            list = distinctList.Where(x => x.vBatchNo.Contains(code)).ToList();
            return list;
        }
        #endregion

        #region Batch Code
        /// <summary>
        /// Get Batch Code
        /// </summary>
        /// <returns>batch code</returns>
        public string GetBatchCode()
        {
            var result =
                _dbset.OrderByDescending(x => x.iPackingProductionPlanId).FirstOrDefault(x => x.vBatchNo != null);
            var numPart = result != null ? Regex.Match(result.vBatchNo, @"\d+").Value.AsInt() + 1 : 1;
            var code = numPart.ToString("D" + 6);
            return code;
        }

        /// <summary>
        /// Validate Batch Code
        /// </summary>
        /// <param name="code"></param>
        /// <returns>bool</returns>
        public bool ValidateCode(string code)
        {
            return _dbset.Any(x => x.vBatchNo == code);
        } 
        #endregion
    }
}
