﻿using System.Linq;
using System.Data.Entity;
using PunjabKitchenDAL.Interfaces;
using PunjabKitchenEntities.Common;
using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchenDAL.Repositories
{
    public class ColdDessertPackingRecordRepository : GenericRepository<tColdDesertPackingRecord>, IColdDessertPackingRecordRepository
    {
        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="context"></param>
        public ColdDessertPackingRecordRepository(DbContext context) : base(context)
        {
        }
        #endregion

        #region Get By Id
        /// <summary>
        /// Get By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>tColdDesertPackingRecord</returns>
        public tColdDesertPackingRecord GetById(long id)
        {
            return _dbset.FirstOrDefault(x => x.iColdDesertPackingRecordId == id);
        } 
        #endregion
    }
}
