﻿using System.Linq;
using System.Data.Entity;
using PunjabKitchenDAL.Interfaces;
using PunjabKitchenEntities.Common;
using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchenDAL.Repositories
{
    public class PureeMealBatchAssemblyDetailRepository : GenericRepository<tPureeMealBatchAssemblyDetail>, IPureeMealBatchAssemblyDetailRepository
    {
        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="context"></param>
        public PureeMealBatchAssemblyDetailRepository(DbContext context) : base(context)
        {
        }
        #endregion

        #region Get By Id
        /// <summary>
        /// Get By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>tPureeMealBatchAssemblyDetail</returns>
        public tPureeMealBatchAssemblyDetail GetById(long id)
        {
            return _dbset.FirstOrDefault(x => x.iPureeMealBatchAssemblyDetailId == id);
        } 
        #endregion
    }
}
