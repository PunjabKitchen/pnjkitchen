﻿using System.Linq;
using System.Data.Entity;
using PunjabKitchenDAL.Interfaces;
using PunjabKitchenEntities.Common;
using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchenDAL.Repositories
{
    public class BatchChangePureeRecordRepository : GenericRepository<tBatchChangeChkPureeRecrd>, IBatchChangePureeRecordRepository
    {
        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="context"></param>
        public BatchChangePureeRecordRepository(DbContext context) : base(context)
        {
        }
        #endregion

        #region Get By Id
        /// <summary>
        /// Get BatchChangeChkPureeRecrd By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>tBatchChangeChkPureeRecrd</returns>
        public tBatchChangeChkPureeRecrd GetById(long id)
        {
            return _dbset.FirstOrDefault(x => x.iBatchChangeChkPureeRecrdId == id);
        } 
        #endregion
    }
}
