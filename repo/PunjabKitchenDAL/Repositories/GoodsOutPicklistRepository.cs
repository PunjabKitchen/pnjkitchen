﻿using System.Linq;
using System.Data.Entity;
using PunjabKitchenDAL.Interfaces;
using PunjabKitchenEntities.Common;
using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchenDAL.Repositories
{
    public class GoodsOutPicklistRepository : GenericRepository<tGoodsOutPicklist>, IGoodsOutPicklistRepository
    {
        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="context"></param>
        public GoodsOutPicklistRepository(DbContext context) : base(context)
        {
        }
        #endregion

        #region Get By Id
        /// <summary>
        /// Get By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>tGoodsOutPicklist</returns>
        public tGoodsOutPicklist GetById(long id)
        {
            return _dbset.FirstOrDefault(x => x.iGoodsOutPickListId == id);
        }

        #endregion
    }
}
