﻿using System.Linq;
using System.Data.Entity;
using PunjabKitchenDAL.Interfaces;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Web.WebPages;
using PunjabKitchenEntities.Common;
using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchenDAL.Repositories
{
    public class CookingPlanRepository : GenericRepository<tCookingPlan>, ICookingPlanRepository
    {
        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="context"></param>
        public CookingPlanRepository(DbContext context) : base(context)
        {
        }
        #endregion

        #region Get By Id
        /// <summary>
        /// Get By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>tCookingPlan</returns>
        public tCookingPlan GetById(long id)
        {
            return _dbset.FirstOrDefault(x => x.iCookingPlanId == id);
        }
        #endregion

        #region Get Cooking Plans by CookpotId
        /// <summary>
        /// Get Cooking Plan by Cook Pot Id
        /// </summary>
        /// <param name="cookPotId"></param>
        /// <returns>List of tCookingPlan</returns>
        public List<tCookingPlan> GetCookingPlansByCookpotId(int cookPotId)
        {
            return _dbset.Where(x => x.iCookPotNo == cookPotId).Include("tProduct").Include("tRecipe").ToList();
        }
        
        /// <summary>
        /// Get CookingPlans By Cookpot Id
        /// </summary>
        /// <param name="isCookPot1"></param>
        /// <param name="isCookPot2"></param>
        /// <param name="isCookPot3"></param>
        /// <returns>List of tCookingPlan</returns>
        public List<tCookingPlan> GetCookingPlansByCookpotId(bool isCookPot1, bool isCookPot2, bool isCookPot3)
        {
            return
                _dbset.Where(x => (x.iCookPotNo == 1 && isCookPot1)
            || (x.iCookPotNo == 2 && isCookPot2)
            || (x.iCookPotNo == 3 && isCookPot3)).Include("tProduct").Include("tRecipe").ToList();
        } 
        #endregion

        #region Get By Code

        /// <summary>
        /// Get By Code
        /// </summary>
        /// <param name="code"></param>
        /// <returns>IEnumerable of tCookingPlan</returns>
        public IEnumerable<tCookingPlan> GetByCode(string code)
        {
            IEnumerable<tCookingPlan> list;
            var distinctList = _dbset.Include(i => i.tProduct).Where(w => w.bIsTask == false).GroupBy(x => x.vBatchNo).Select(y => y.FirstOrDefault()).Distinct();
            list = distinctList.Where(x => x.vBatchNo.Contains(code)).ToList();
            return list;
        }

        #endregion

        #region Get Batch Code
        /// <summary>
        /// Get Batch Code
        /// </summary>
        /// <returns>string</returns>
        public string GetBatchCode()
        {
            var result =
                _dbset.OrderByDescending(x => x.iCookingPlanId).FirstOrDefault(x => x.vBatchNo != null);
            var numPart = result != null ? Regex.Match(result.vBatchNo, @"\d+").Value.AsInt() + 1 : 1;
            var code = numPart.ToString("D" + 6);
            return code;
        }
        #endregion

        #region Validate Batch Code
        /// <summary>
        /// Validate Batch Code
        /// </summary>
        /// <param name="batchCode"></param>
        /// <returns>bool</returns>
        public bool ValidateBatchCode(string batchCode)
        {
            return _dbset.Any(x => x.vBatchNo == batchCode);
        } 
        #endregion
    }
}
