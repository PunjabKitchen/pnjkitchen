﻿using System.Data.Entity;
using System.Linq;
using PunjabKitchenDAL.Interfaces;
using PunjabKitchenEntities.Common;
using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchenDAL.Repositories
{
    public class VegetarianPackingRecordRepository : GenericRepository<tVegetarianPackingRecord>, IVegetarianPackingRecordRepository
    {
        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="context"></param>
        public VegetarianPackingRecordRepository(DbContext context) : base(context)
        {
        }
        #endregion

        #region Get by Id
        /// <summary>
        /// GetById
        /// </summary>
        /// <param name="id"></param>
        /// <returns>tVegetarianPackingRecord</returns>
        public tVegetarianPackingRecord GetById(long id)
        {
            return _dbset.FirstOrDefault(x => x.iVegeRecordId == id);
        }
        #endregion
    }
}