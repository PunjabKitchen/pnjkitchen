﻿using System.Data.Entity;
using System.Linq;
using PunjabKitchenEntities.EntityModel;
using PunjabKitchenEntities.Interfaces;

namespace PunjabKitchenEntities.Common
{
    public class PunjabKitchenContext : DbContext
    {

        public PunjabKitchenContext()
            : base("Name=PunjabKitchenContext")
        {

        }

        public DbSet<tGoodsIn> GoodsIn { get; set; }
        public DbSet<tNotification> Notifications { get; set; }
        public DbSet<tStock> Stocks { get; set; }
        public DbSet<tStockType> StockTypes { get; set; }
        public DbSet<tProduct> Products { get; set; }
        public DbSet<tProductType> ProductTypes { get; set; }
        public DbSet<tUnit> Units{ get; set; }
        public DbSet<CleaningChecklistDaily> CleaningChecklistDaily { get; set; }
        public DbSet<tStartUpCheckList> StartUpCheckList { get; set; }
        public DbSet<tStartUpcheckDaily> StartUpcheckDaily { get; set; }
        public DbSet<tNotificationType> NotificationTypes { get; set; }
        public DbSet<tCanteenFridgeTempRecordDaily> CanteenFridgeTempRecordDaily { get; set; }
        public DbSet<tArea> tArea { get; set; }
        public DbSet<tAreaItem> tAreaItem { get; set; }
        public DbSet<tShoeCleaningRecrdWeekly> tShoeCleaningRecrdWeekly { get; set; }
        public DbSet<tCookingPlan> tCookingPlan { get; set; }
        public DbSet<tFreezerRecord> tFreezerRecord { get; set; }
        public DbSet<tSlicingRecord> tSlicingRecord { get; set; }
        public DbSet<tRiceCoolingTempRecord> tRiceCoolingTempRecord { get; set; }
        public DbSet<tMeatMincingRecord> tMeatMincingRecord { get; set; }
        public DbSet<tBatchCoolingTempRecrd> tBatchCoolingTempRecrd { get; set; }
        public DbSet<tRecipe> TRecipes { get; set; }
        public DbSet<tPackingProductionPlan> TPackingProductionPlans{ get; set; }
        public DbSet<tColdDesertPackingRecord> TColdDesertPackingRecords { get; set; }
        public DbSet<tHotPackingRercord> THotPackingRercords { get; set; }
        public DbSet<tPickList> TPickList { get; set; }
        public DbSet<tPortionWeightCheck> TPortionWeightChecksList { get; set; }
        public DbSet<tShapeControlRecord> TShapesControlRecordsList { get; set; }
        public DbSet<tPackingRecord> TPackingRecordsList { get; set; }
        public DbSet<tPackingRecordDetail> TPackingRecordsDetailList { get; set; }
        public DbSet<tPackingRecord2> TPackingRecords2List { get; set; }
        public DbSet<tPackingRecord2Detail> TPackingRecords2DetailList { get; set; }
        public DbSet<tShapeControlRecordDetail> TShapeControlRecordDetails { get; set; }
        public DbSet<tShapesControlRecordBatchDetail> TShapesControlRecordBatchDetail { get; set; }
        public DbSet<tPureeShapesStarchRecord> TPureeShapesStarchRecords { get; set; }
        public DbSet<tBatchChangeChkPureeRecrd> TBatchChangeChkPureeRecrds { get; set; }
        public DbSet<tFMDetectionAndPackWeightTest> TFMDetectionAndPackWeightTests { get; set; }
        public DbSet<tPortionWeightCheckDetail> TPortionWeightCheckDetails { get; set; }
        public DbSet<tPureeShapesStarchRecordDetail> TPureeShapesStarchRecordDetails { get; set; }
        public DbSet<tPureeShapesStarchAdditionRecord> TPureeShapesStarchAdditionRecord { get; set; }
        public DbSet<tBoxingRecord> tBoxingRecord { get; set; }
        public DbSet<tMixedCaseBoxingRecord> tMixedCaseBoxingRecord { get; set; }
        public DbSet<tMixedCaseBoxingRecordDetail> tMixedCaseBoxingRecordDetail { get; set; }
        public DbSet<tVegetarianPackingRecord> tVegetarianPackingRecord { get; set; }
        public DbSet<tVegetarianPackingRecordDetail> tVegetarianPackingRecordDetail { get; set; }
        public DbSet<tPureeMeanBatchAssembly> tPureeMeanBatchAssembly { get; set; }
        public DbSet<tPureeMealBatchAssemblyDetail> tPureeMealBatchAssemblyDetail { get; set; }
        public DbSet<tPureeMealBatchAssemblyPackerDetail> tPureeMealBatchAssemblyPackerDetail { get; set; }
        public DbSet<tPureeMealInBlastFreezer> tPureeMealInBlastFreezer { get; set; }
        public DbSet<tGoodsOutPlan> tGoodsOutPlan { get; set; }
        public DbSet<tGoodsOutPlanDetail> tGoodsOutPlanDetail { get; set; }
        public DbSet<tEmployeeAttendance> TEmployeeAttendances{ get; set; }
        public DbSet<Event> Event{ get; set; }
        public DbSet<tDailyTruckInspection> tDailyTruckInspections{ get; set; }
        public DbSet<tGoodsOutFreezerUnit> tGoodsOutFreezerUnit { get; set; }
        public DbSet<tStartupChecksEPGoodsOut> tStartupChecksEPGoodsOut { get; set; }
        public override int SaveChanges()
        {
            var modifiedEntries = ChangeTracker.Entries()
                .Where(x => x.Entity is IAuditableEntity
                    && (x.State == System.Data.Entity.EntityState.Added || x.State == System.Data.Entity.EntityState.Modified));

            //foreach (var entry in modifiedEntries)
            //{
            //    IAuditableEntity entity = entry.Entity as IAuditableEntity;
            //    if (entity != null)
            //    {
            //        string identityName = Thread.CurrentPrincipal.Identity.Name;
            //        DateTime now = DateTime.UtcNow;

            //        if (entry.State == System.Data.Entity.EntityState.Added)
            //        {
            //            entity.CreatedBy = identityName;
            //            entity.CreatedDate = now;
            //        }
            //        else {
            //            base.Entry(entity).Property(x => x.CreatedBy).IsModified = false;
            //            base.Entry(entity).Property(x => x.CreatedDate).IsModified = false;                   
            //        }

            //        entity.UpdatedBy = identityName;
            //        entity.UpdatedDate = now;
            //    }
            //}

            return base.SaveChanges();
        }
    }    
}
