//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PunjabKitchenEntities.EntityModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class tShapesControlRecordBatchDetail
    {
        public int iShapesControlRecordBatchDetailId { get; set; }
        public Nullable<int> iShapeControlRecordId { get; set; }
        public string vBatchCode { get; set; }
        public Nullable<System.DateTime> Sys_CreatedDateTime { get; set; }
        public string Sys_CreatedBy { get; set; }
        public Nullable<System.DateTime> Sys_ModifyDateTime { get; set; }
        public string Sys_ModifyBy { get; set; }
    
        public virtual tShapeControlRecord tShapeControlRecord { get; set; }
    }
}
