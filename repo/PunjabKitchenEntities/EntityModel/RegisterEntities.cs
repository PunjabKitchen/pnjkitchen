﻿

using PunjabKitchenEntities.Common;

namespace PunjabKitchenEntities.EntityModel
{
    public partial class tNotification : AuditableEntity<long>
    {
        //[NotMapped]
        //public string UserCreatedBy { get; set; }
    }
    public partial class tGoodsIn : AuditableEntity<long>{}
    public partial class CleaningChecklistDaily : AuditableEntity<long>{}
    public partial class tStock : AuditableEntity<long> { }
    public partial class tStockType : AuditableEntity<long> { }
    public partial class tProduct : AuditableEntity<long> { }
    public partial class tProductType : AuditableEntity<long> { }
    public partial class tUnit : AuditableEntity<long> { }
    public partial class tStartUpCheckList : AuditableEntity<long> { }
    public partial class tStartUpcheckDaily : AuditableEntity<long> { }
    public partial class tNotificationType : AuditableEntity<long> { }
    public partial class tCanteenFridgeTempRecordDaily : AuditableEntity<long> { }

    public partial class tArea : AuditableEntity<long> { }
    public partial class tAreaItem : AuditableEntity<long> { }
    public partial class tShoeCleaningRecrdWeekly : AuditableEntity<long> { }
    public partial class tCookingPlan : AuditableEntity<long> { }
    public partial class tFreezerRecord : AuditableEntity<long> { }
    public partial class tSlicingRecord : AuditableEntity<long> { }
    public partial class tRiceCoolingTempRecord : AuditableEntity<long> { }
    public partial class tMeatMincingRecord : AuditableEntity<long> { }
    public partial class tBatchCoolingTempRecrd : AuditableEntity<long> { }
    public partial class tRecipe : AuditableEntity<long> { }
    public partial class tPackingProductionPlan : AuditableEntity<long> { }
    public partial class tColdDesertPackingRecord : AuditableEntity<long> { }
    public partial class tHotPackingRercord : AuditableEntity<long> { }
    public partial class tPickList : AuditableEntity<long> { }
    public partial class tPortionWeightCheck : AuditableEntity<long> { }
    public partial class tShapeControlRecord : AuditableEntity<long> { }
    public partial class tShapeControlRecordDetail : AuditableEntity<long> { }
    public partial class tShapesControlRecordBatchDetail : AuditableEntity<long> { }
    public partial class tPackingRecord : AuditableEntity<long> { }
    public partial class tPackingRecordDetail : AuditableEntity<long> { }
    public partial class tPackingRecord2 : AuditableEntity<long> { }
    public partial class tPackingRecord2Detail : AuditableEntity<long> { }
    public partial class tBatchChangeChkPureeRecrd : AuditableEntity<long> { }
    public partial class tFMDetectionAndPackWeightTest : AuditableEntity<long> { }
    public partial class tPureeShapesStarchRecord : AuditableEntity<long> { }
    public partial class tPortionWeightCheckDetail : AuditableEntity<long> { }
    public partial class tPureeShapesStarchRecordDetail : AuditableEntity<long> { }
    public partial class tPureeShapesStarchAdditionRecord : AuditableEntity<long> { }
    public partial class tBoxingRecord : AuditableEntity<long> { }
    public partial class tMixedCaseBoxingRecord : AuditableEntity<long> { }
    public partial class tMixedCaseBoxingRecordDetail : AuditableEntity<long> { }
    public partial class tVegetarianPackingRecord : AuditableEntity<long> { }
    public partial class tVegetarianPackingRecordDetail : AuditableEntity<long> { }
    public partial class tPureeMeanBatchAssembly : AuditableEntity<long> { }
    public partial class tPureeMealBatchAssemblyDetail : AuditableEntity<long> { }
    public partial class tPureeMealBatchAssemblyPackerDetail : AuditableEntity<long> { }
    public partial class tPureeMealInBlastFreezer : AuditableEntity<long> { }
    public partial class tGoodsOutPlan : AuditableEntity<long> { }
    public partial class tGoodsOutPlanDetail : AuditableEntity<long> { }
    public partial class tGoodsOutPicklist : AuditableEntity<long> { }
    public partial class tGoodsOutPicklistDetail : AuditableEntity<long> { }
    public partial class tEmployeeAttendance : AuditableEntity<long> { }
    public partial class Event : AuditableEntity<long> { }
    public partial class tDailyTruckInspection : AuditableEntity<long> { }
    public partial class tGoodsOutFreezerUnit : AuditableEntity<long> { }
    public partial class tStartupChecksEPGoodsOut : AuditableEntity<long> { }
}
