//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PunjabKitchenEntities.EntityModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class tPreOpTruckInspection
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tPreOpTruckInspection()
        {
            this.tPreOpTruckInspectionDetails = new HashSet<tPreOpTruckInspectionDetail>();
        }
    
        public int iPreOpTruckInspectionId { get; set; }
        public string vTruckType { get; set; }
        public string vTruckMade { get; set; }
        public string vMaxLoad { get; set; }
        public string vPropulsion { get; set; }
        public string vChanges { get; set; }
        public Nullable<System.DateTime> dDate { get; set; }
        public string vSupervisorSign { get; set; }
        public string vActionTaken { get; set; }
        public string vManagmentSign { get; set; }
        public Nullable<bool> bEnabled { get; set; }
        public Nullable<bool> bDeleted { get; set; }
        public Nullable<System.DateTime> Sys_CreatedDateTime { get; set; }
        public string Sys_CreatedBy { get; set; }
        public Nullable<System.DateTime> Sys_ModifyDateTime { get; set; }
        public string Sys_ModifyBy { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tPreOpTruckInspectionDetail> tPreOpTruckInspectionDetails { get; set; }
    }
}
