//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PunjabKitchenEntities.EntityModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class tDailyTruckInspection
    {
        public int iTruckInspectionId { get; set; }
        public Nullable<System.DateTime> dDateTime { get; set; }
        public string vTruckType { get; set; }
        public string vTruckNumber { get; set; }
        public string vTruckMake { get; set; }
        public Nullable<int> iMaxLoad { get; set; }
        public string iPropulsion { get; set; }
        public string vChanges { get; set; }
        public Nullable<bool> bForkArms { get; set; }
        public Nullable<bool> bMast { get; set; }
        public Nullable<bool> bCarriagePlate { get; set; }
        public Nullable<bool> bLiftChainAndLubrication { get; set; }
        public Nullable<bool> bWheelAndTyres { get; set; }
        public Nullable<bool> bBackRest { get; set; }
        public Nullable<bool> bSeatAndSeatBelt { get; set; }
        public Nullable<bool> bSteering { get; set; }
        public Nullable<bool> bLightAndIndicators { get; set; }
        public Nullable<bool> bHornAndBeeper { get; set; }
        public Nullable<bool> bMastController { get; set; }
        public Nullable<bool> bHandAndParkingBreak { get; set; }
        public Nullable<bool> bDrivingAndServiceBreak { get; set; }
        public Nullable<bool> bHydraulics { get; set; }
        public Nullable<bool> bOilLevel { get; set; }
        public Nullable<bool> bFuelAndPower { get; set; }
        public Nullable<bool> bBatteries { get; set; }
        public Nullable<bool> bLPG { get; set; }
        public string vActionTaken { get; set; }
        public Nullable<System.DateTime> Sys_CreatedDateTime { get; set; }
        public string Sys_CreatedBy { get; set; }
        public Nullable<System.DateTime> Sys_ModifyDateTime { get; set; }
        public string Sys_ModifyBy { get; set; }
    
        public virtual AspNetUser AspNetUser { get; set; }
        public virtual AspNetUser AspNetUser1 { get; set; }
    }
}
