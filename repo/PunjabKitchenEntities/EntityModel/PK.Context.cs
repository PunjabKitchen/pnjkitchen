﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PunjabKitchenEntities.EntityModel
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class PunjabKitchen2Entities : DbContext
    {
        public PunjabKitchen2Entities()
            : base("name=PunjabKitchen2Entities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<C__MigrationHistory> C__MigrationHistory { get; set; }
        public virtual DbSet<AspNetRole> AspNetRoles { get; set; }
        public virtual DbSet<AspNetUserClaim> AspNetUserClaims { get; set; }
        public virtual DbSet<AspNetUserLogin> AspNetUserLogins { get; set; }
        public virtual DbSet<AspNetUser> AspNetUsers { get; set; }
        public virtual DbSet<ELMAH_Error> ELMAH_Error { get; set; }
        public virtual DbSet<tCookingSummery> tCookingSummeries { get; set; }
        public virtual DbSet<tEPGoodsOutStartUpCheck> tEPGoodsOutStartUpChecks { get; set; }
        public virtual DbSet<tEPGoodsOutStartUpCheckList> tEPGoodsOutStartUpCheckLists { get; set; }
        public virtual DbSet<tFreezerStockRecord> tFreezerStockRecords { get; set; }
        public virtual DbSet<tGoodsoutFreezerTempRecrd> tGoodsoutFreezerTempRecrds { get; set; }
        public virtual DbSet<tIngredient> tIngredients { get; set; }
        public virtual DbSet<tLiftTruckCheckList> tLiftTruckCheckLists { get; set; }
        public virtual DbSet<tMealComponent> tMealComponents { get; set; }
        public virtual DbSet<tMeatUsageRecord> tMeatUsageRecords { get; set; }
        public virtual DbSet<tOrder> tOrders { get; set; }
        public virtual DbSet<tPickingList> tPickingLists { get; set; }
        public virtual DbSet<tPickingListDetail> tPickingListDetails { get; set; }
        public virtual DbSet<tPreOpTruckInspection> tPreOpTruckInspections { get; set; }
        public virtual DbSet<tPreOpTruckInspectionDetail> tPreOpTruckInspectionDetails { get; set; }
        public virtual DbSet<tProductBatchAssemblyRecord> tProductBatchAssemblyRecords { get; set; }
        public virtual DbSet<tProductBatchAssemblyRecordDetail> tProductBatchAssemblyRecordDetails { get; set; }
        public virtual DbSet<tProductType> tProductTypes { get; set; }
        public virtual DbSet<tPureeExceptionPlan> tPureeExceptionPlans { get; set; }
        public virtual DbSet<tShapesBoxingExpPlan> tShapesBoxingExpPlans { get; set; }
        public virtual DbSet<tStartUpCheckList> tStartUpCheckLists { get; set; }
        public virtual DbSet<tStockType> tStockTypes { get; set; }
        public virtual DbSet<tSupplier> tSuppliers { get; set; }
        public virtual DbSet<tSupplierVehicleCheck> tSupplierVehicleChecks { get; set; }
        public virtual DbSet<tUnit> tUnits { get; set; }
        public virtual DbSet<Event> Events { get; set; }
        public virtual DbSet<tNotificationType> tNotificationTypes { get; set; }
        public virtual DbSet<tArea> tAreas { get; set; }
        public virtual DbSet<tAreaItem> tAreaItems { get; set; }
        public virtual DbSet<tNotification> tNotifications { get; set; }
        public virtual DbSet<tShoeCleaningRecrdWeekly> tShoeCleaningRecrdWeeklies { get; set; }
        public virtual DbSet<CleaningChecklistDaily> CleaningChecklistDailies { get; set; }
        public virtual DbSet<tCanteenFridgeTempRecordDaily> tCanteenFridgeTempRecordDailies { get; set; }
        public virtual DbSet<tStartUpcheckDaily> tStartUpcheckDailies { get; set; }
        public virtual DbSet<tFreezerRecord> tFreezerRecords { get; set; }
        public virtual DbSet<tPureeMealComponentDetail> tPureeMealComponentDetails { get; set; }
        public virtual DbSet<tPortionWeightCheckDetail> tPortionWeightCheckDetails { get; set; }
        public virtual DbSet<tPureeShapesStarchRecordDetail> tPureeShapesStarchRecordDetails { get; set; }
        public virtual DbSet<tPureeShapesStarchAdditionRecord> tPureeShapesStarchAdditionRecords { get; set; }
        public virtual DbSet<tShapeControlRecordDetail> tShapeControlRecordDetails { get; set; }
        public virtual DbSet<tShapesControlRecordBatchDetail> tShapesControlRecordBatchDetails { get; set; }
        public virtual DbSet<tPackingRecord2Detail> tPackingRecord2Detail { get; set; }
        public virtual DbSet<tPackingRecordDetail> tPackingRecordDetails { get; set; }
        public virtual DbSet<tVegetarianPackingRecordDetail> tVegetarianPackingRecordDetails { get; set; }
        public virtual DbSet<tPureeMealBatchAssemblyPackerDetail> tPureeMealBatchAssemblyPackerDetails { get; set; }
        public virtual DbSet<tPureeMealBatchAssemblyDetail> tPureeMealBatchAssemblyDetails { get; set; }
        public virtual DbSet<tPureeMealInBlastFreezer> tPureeMealInBlastFreezers { get; set; }
        public virtual DbSet<tMixedCaseBoxingRecord> tMixedCaseBoxingRecords { get; set; }
        public virtual DbSet<tMixedCaseBoxingRecordDetail> tMixedCaseBoxingRecordDetails { get; set; }
        public virtual DbSet<tRecipe> tRecipes { get; set; }
        public virtual DbSet<tGoodsOutPlan> tGoodsOutPlans { get; set; }
        public virtual DbSet<tGoodsOutPlanDetail> tGoodsOutPlanDetails { get; set; }
        public virtual DbSet<tCookingPlan> tCookingPlans { get; set; }
        public virtual DbSet<tPackingProductionPlan> tPackingProductionPlans { get; set; }
        public virtual DbSet<tGoodsOutPicklist> tGoodsOutPicklists { get; set; }
        public virtual DbSet<tGoodsOutPicklistDetail> tGoodsOutPicklistDetails { get; set; }
        public virtual DbSet<tColdDesertPackingRecord> tColdDesertPackingRecords { get; set; }
        public virtual DbSet<tHotPackingRercord> tHotPackingRercords { get; set; }
        public virtual DbSet<tPackingRecord> tPackingRecords { get; set; }
        public virtual DbSet<tPackingRecord2> tPackingRecord2 { get; set; }
        public virtual DbSet<tPortionWeightCheck> tPortionWeightChecks { get; set; }
        public virtual DbSet<tPureeMeanBatchAssembly> tPureeMeanBatchAssemblies { get; set; }
        public virtual DbSet<tPureeShapesStarchRecord> tPureeShapesStarchRecords { get; set; }
        public virtual DbSet<tShapeControlRecord> tShapeControlRecords { get; set; }
        public virtual DbSet<tVegetarianPackingRecord> tVegetarianPackingRecords { get; set; }
        public virtual DbSet<tFMDetectionAndPackWeightTest> tFMDetectionAndPackWeightTests { get; set; }
        public virtual DbSet<tBatchChangeChkPureeRecrd> tBatchChangeChkPureeRecrds { get; set; }
        public virtual DbSet<tBatchCoolingTempRecrd> tBatchCoolingTempRecrds { get; set; }
        public virtual DbSet<tMeatMincingRecord> tMeatMincingRecords { get; set; }
        public virtual DbSet<tRiceCoolingTempRecord> tRiceCoolingTempRecords { get; set; }
        public virtual DbSet<tSlicingRecord> tSlicingRecords { get; set; }
        public virtual DbSet<tPickList> tPickLists { get; set; }
        public virtual DbSet<tStock> tStocks { get; set; }
        public virtual DbSet<tStaffTimeSheet> tStaffTimeSheets { get; set; }
        public virtual DbSet<tDailyTruckInspection> tDailyTruckInspections { get; set; }
        public virtual DbSet<tGoodsOutFreezerUnit> tGoodsOutFreezerUnits { get; set; }
        public virtual DbSet<tStartupChecksEPGoodsOut> tStartupChecksEPGoodsOuts { get; set; }
        public virtual DbSet<tEmployeeAttendance> tEmployeeAttendances { get; set; }
        public virtual DbSet<sysdiagram> sysdiagrams { get; set; }
        public virtual DbSet<tGoodsIn> tGoodsIns { get; set; }
        public virtual DbSet<tBoxingRecord> tBoxingRecords { get; set; }
        public virtual DbSet<tProduct> tProducts { get; set; }
    }
}
