﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using PunjabKitchenEntities.Interfaces;

namespace PunjabKitchenEntities.Common
{
    public abstract class GenericRepository<T> : IGenericRepository<T>
       where T : BaseEntity
    {
        protected DbContext _entities;
        protected readonly IDbSet<T> _dbset;

        public GenericRepository(DbContext context)
        {
            //using (var context1 = new PunjabKitchenContext())
            //{
                _entities = context;
                _dbset = context.Set<T>();
            //}
        }

        public virtual IEnumerable<T> GetAll()
        {
            
                return _dbset.AsEnumerable<T>();
            
        }

        public IEnumerable<T> FindBy(System.Linq.Expressions.Expression<Func<T, bool>> predicate)
        {

            IEnumerable<T> query = _dbset.Where(predicate).AsEnumerable(); 
            return query;
        }

        public virtual T Add(T entity)
        {
            return _dbset.Add(entity);
        }

        public virtual T Delete(T entity)
        {
            return _dbset.Remove(entity);
        }

        public virtual void Edit(T entity)
        {
            _entities.Entry(entity).State = System.Data.Entity.EntityState.Modified;
            if (entity.GetType().GetProperty("Sys_CreatedDateTime") != null)
            {
                _entities.Entry(entity).Property("Sys_CreatedDateTime").IsModified = false;
            }
            if (entity.GetType().GetProperty("Sys_CreatedDateTime") != null)
            {
                _entities.Entry(entity).Property("Sys_CreatedBy").IsModified = false;
            }
            if (entity.GetType().GetProperty("Sys_ModifyBy") != null)
            {
                _entities.Entry(entity).Property("Sys_ModifyBy").IsModified = true;
            }
            if (entity.GetType().GetProperty("Sys_ModifyDateTime") != null)
            {
                _entities.Entry(entity).Property("Sys_ModifyDateTime").IsModified = true;
            }
        }

        public virtual void Save()
        {
            _entities.SaveChanges();
        }
    }
}
