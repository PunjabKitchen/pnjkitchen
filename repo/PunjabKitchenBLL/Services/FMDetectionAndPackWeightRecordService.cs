﻿using PunjabKitchenBLL.Common;
using PunjabKitchenDAL.Interfaces;
using PunjabKitchenBLL.Interfaces;
using PunjabKitchenEntities.EntityModel;
using PunjabKitchenEntities.Interfaces;

namespace PunjabKitchenBLL.Services
{
    // ReSharper disable once InconsistentNaming
    public class FMDetectionAndPackWeightRecordService : EntityService<tFMDetectionAndPackWeightTest>, IFMDetectionAndPackWeightRecordService
    {
        #region Data Members
        // ReSharper disable once ArrangeTypeMemberModifiers
        readonly IFMDetectionAndPackWeightRecordRepository _fmDetectionPackWeightRecordRepository;
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="unitOfWork"></param>
        /// <param name="fmDetectionPackWeightRecordRepository"></param>
        public FMDetectionAndPackWeightRecordService(IUnitOfWork unitOfWork, IFMDetectionAndPackWeightRecordRepository fmDetectionPackWeightRecordRepository)
            : base(unitOfWork, fmDetectionPackWeightRecordRepository)
        {
            _fmDetectionPackWeightRecordRepository = fmDetectionPackWeightRecordRepository;
        }
        #endregion

        #region Get By Id
        /// <summary>
        /// Get By Id
        /// </summary>
        /// <param name="id"></param>
        public tFMDetectionAndPackWeightTest GetById(long id)
        {
            return _fmDetectionPackWeightRecordRepository.GetById(id);
        }
        #endregion
    }
}