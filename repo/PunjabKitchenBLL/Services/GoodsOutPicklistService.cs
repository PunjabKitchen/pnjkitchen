﻿using PunjabKitchenBLL.Common;
using PunjabKitchenDAL.Interfaces;
using PunjabKitchenBLL.Interfaces;
using PunjabKitchenEntities.EntityModel;
using PunjabKitchenEntities.Interfaces;

namespace PunjabKitchenBLL.Services
{
    public class GoodsOutPicklistService : EntityService<tGoodsOutPicklist>, IGoodsOutPicklistService
    {
        // ReSharper disable once NotAccessedField.Local
        IUnitOfWork _unitOfWork;
        readonly IGoodsOutPicklistRepository _goodsOutPicklistRepository;

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="unitOfWork"></param>
        /// <param name="goodsOutPicklistRepository"></param>
        public GoodsOutPicklistService(IUnitOfWork unitOfWork, IGoodsOutPicklistRepository goodsOutPicklistRepository)
            : base(unitOfWork, goodsOutPicklistRepository)
        {
            _unitOfWork = unitOfWork;
            _goodsOutPicklistRepository = goodsOutPicklistRepository;
        }
        #endregion

        #region Get By Id
        /// <summary>
        /// Get By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public tGoodsOutPicklist GetById(long id)
        {
            return _goodsOutPicklistRepository.GetById(id);
        }
        #endregion
    }
}