﻿using PunjabKitchenBLL.Common;
using PunjabKitchenDAL.Interfaces;
using PunjabKitchenBLL.Interfaces;
using PunjabKitchenEntities.EntityModel;
using PunjabKitchenEntities.Interfaces;

namespace PunjabKitchenBLL.Services
{
    public class PureeMealBatchAssemblyPackerDetailService : EntityService<tPureeMealBatchAssemblyPackerDetail>, IPureeMealBatchAssemblyPackerDetailService
    {
        #region Data Members
        readonly IPureeMealBatchAssemblyPackerDetailRepository _pureeMealBatchAssemblyPackerDetailRepository;
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="unitOfWork"></param>
        /// <param name="pureeMealBatchAssemblyPackerDetailRepository"></param>
        public PureeMealBatchAssemblyPackerDetailService(IUnitOfWork unitOfWork, IPureeMealBatchAssemblyPackerDetailRepository pureeMealBatchAssemblyPackerDetailRepository)
            : base(unitOfWork, pureeMealBatchAssemblyPackerDetailRepository)
        {
            _pureeMealBatchAssemblyPackerDetailRepository = pureeMealBatchAssemblyPackerDetailRepository;
        }
        #endregion

        #region Get By Id
        /// <summary>
        /// Get By Id
        /// </summary>
        /// <param name="id"></param>
        public tPureeMealBatchAssemblyPackerDetail GetById(long id)
        {
            return _pureeMealBatchAssemblyPackerDetailRepository.GetById(id);
        }
        #endregion
    }
}