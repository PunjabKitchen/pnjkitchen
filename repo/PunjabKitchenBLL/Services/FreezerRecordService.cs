﻿using PunjabKitchenBLL.Common;
using PunjabKitchenDAL.Interfaces;
using PunjabKitchenBLL.Interfaces;
using PunjabKitchenEntities.EntityModel;
using PunjabKitchenEntities.Interfaces;

namespace PunjabKitchenBLL.Services
{
    public class FreezerRecordService : EntityService<tFreezerRecord>, IFreezerRecordService
    {
        #region Data Members
        // ReSharper disable once NotAccessedField.Local
        IUnitOfWork _unitOfWork;
        // ReSharper disable once ArrangeTypeMemberModifiers
        readonly IFreezerRecordRepository _freezerRecordRepository;
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="unitOfWork"></param>
        /// <param name="freezerRecordRepository"></param>
        public FreezerRecordService(IUnitOfWork unitOfWork, IFreezerRecordRepository freezerRecordRepository)
            : base(unitOfWork, freezerRecordRepository)
        {
            _unitOfWork = unitOfWork;
            _freezerRecordRepository = freezerRecordRepository;
        }
        #endregion

        #region Get By Id
        /// <summary>
        /// Get By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public tFreezerRecord GetById(long id)
        {
            return _freezerRecordRepository.GetById(id);
        }
        #endregion
    }
}