﻿using PunjabKitchenBLL.Common;
using PunjabKitchenBLL.Interfaces;
using PunjabKitchenDAL.Interfaces;
using PunjabKitchenEntities.EntityModel;
using PunjabKitchenEntities.Interfaces;

namespace PunjabKitchenBLL.Services
{
    public class VegetarianPackingRecordDetailService : EntityService<tVegetarianPackingRecordDetail>, IVegetarianPackingRecordDetailService
    {
        #region Data Members
        readonly IVegetarianPackingRecordDetailRepository _vegetarianPackingRecordDetailRepository;
        #endregion

        #region Constructor
        /// <summary>
        /// 
        /// </summary>
        /// <param name="unitOfWork"></param>
        /// <param name="vegetarianPackingRecordDetailRepository"></param>
        public VegetarianPackingRecordDetailService(IUnitOfWork unitOfWork, IVegetarianPackingRecordDetailRepository vegetarianPackingRecordDetailRepository)
            : base(unitOfWork, vegetarianPackingRecordDetailRepository)
        {
            _vegetarianPackingRecordDetailRepository = vegetarianPackingRecordDetailRepository;
        }
        #endregion

        #region Get By Id
        /// <summary>
        /// Get By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public tVegetarianPackingRecordDetail GetById(long id)
        {
            return _vegetarianPackingRecordDetailRepository.GetById(id);
        }
        #endregion
    }
}