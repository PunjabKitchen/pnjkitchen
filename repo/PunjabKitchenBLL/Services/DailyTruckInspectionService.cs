﻿using System;
using System.Collections.Generic;
using PunjabKitchenBLL.Common;
using PunjabKitchenDAL.Interfaces;
using PunjabKitchenBLL.Interfaces;
using PunjabKitchenEntities.EntityModel;
using PunjabKitchenEntities.Interfaces;

namespace PunjabKitchenBLL.Services
{
    public class DailyTruckInspectionService : EntityService<tDailyTruckInspection>, IDailyTruckInspectionService
    {
        #region Data Members
        // ReSharper disable once NotAccessedField.Local
        IUnitOfWork _unitOfWork;
        readonly IDailyTruckInspectionRepository _dailyTruckInspectionRepository;
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="unitOfWork"></param>
        /// <param name="dailyTruckInspectionRepository"></param>
        public DailyTruckInspectionService(IUnitOfWork unitOfWork, IDailyTruckInspectionRepository dailyTruckInspectionRepository)
            : base(unitOfWork, dailyTruckInspectionRepository)
        {
            _unitOfWork = unitOfWork;
            _dailyTruckInspectionRepository = dailyTruckInspectionRepository;
        }
        #endregion

        #region Get By Id
        /// <summary>
        /// Get By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public tDailyTruckInspection GetById(long id)
        {
            return _dailyTruckInspectionRepository.GetById(id);
        }
        #endregion

        #region Get Daily Truck Inspection History
        /// <summary>
        /// Get Daily Truck Inspection History
        /// </summary>
        /// <param name="startDateTime"></param>
        /// <param name="endDateTime"></param>
        /// <returns></returns>
        public IEnumerable<tDailyTruckInspection> GetDailyTruckInspectionHistory(DateTime startDateTime, DateTime endDateTime)
        {
            return _dailyTruckInspectionRepository.GetDailyTruckInspectionHistory(startDateTime, endDateTime);
        }
        #endregion

        #region Get Daily Truck Inspection History By ID
        /// <summary>
        /// Get Daily Truck Inspection History By ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public tDailyTruckInspection GetDailyTruckInspectionHistoryById(long id)
        {
            return _dailyTruckInspectionRepository.GetDailyTruckInspectionHistoryById(id);
        }
        #endregion
    }
}
