﻿using System.Collections.Generic;
using PunjabKitchenBLL.Common;
using PunjabKitchenDAL.Interfaces;
using PunjabKitchenBLL.Interfaces;
using PunjabKitchenEntities.EntityModel;
using PunjabKitchenEntities.Interfaces;

namespace PunjabKitchenBLL.Services
{
    public class AreaItemService : EntityService<tAreaItem>, IAreaItemService
    {
        #region Data Members
        // ReSharper disable once NotAccessedField.Local
        IUnitOfWork _unitOfWork;
        // ReSharper disable once FieldCanBeMadeReadOnly.Local
        IAreaItemRepository _areaItemRepository;
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="unitOfWork"></param>
        /// <param name="areaItemRepository"></param>
        public AreaItemService(IUnitOfWork unitOfWork, IAreaItemRepository areaItemRepository)
            : base(unitOfWork, areaItemRepository)
        {
            _unitOfWork = unitOfWork;
            _areaItemRepository = areaItemRepository;
        }
        #endregion

        #region Get By Id
        /// <summary>
        /// Get By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public tAreaItem GetById(long id)
        {
            return _areaItemRepository.GetById(id);
        }

        public List<tAreaItem> GetItemByAreaId(long id)
        {
            return _areaItemRepository.GetItemByAreaId(id);
        }

        #endregion
    }
}