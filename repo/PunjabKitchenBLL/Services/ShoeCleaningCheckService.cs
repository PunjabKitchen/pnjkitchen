﻿using System;
using System.Collections.Generic;
using PunjabKitchenBLL.Common;
using PunjabKitchenDAL.Interfaces;
using PunjabKitchenBLL.Interfaces;
using PunjabKitchenEntities.EntityModel;
using PunjabKitchenEntities.Interfaces;

namespace PunjabKitchenBLL.Services
{
    public class ShoeCleaningCheckService : EntityService<tShoeCleaningRecrdWeekly>, IShoeCleaningCheckService
    {
        #region Data Members
        // ReSharper disable once NotAccessedField.Local
        IUnitOfWork _unitOfWork;
        IShoeCleaningCheckRepository _shoeCleaningCheckRepository;
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="unitOfWork"></param>
        /// <param name="shoeCleaningCheckRepository"></param>
        public ShoeCleaningCheckService(IUnitOfWork unitOfWork, IShoeCleaningCheckRepository shoeCleaningCheckRepository)
            : base(unitOfWork, shoeCleaningCheckRepository)
        {
            _unitOfWork = unitOfWork;
            _shoeCleaningCheckRepository = shoeCleaningCheckRepository;
        }
        #endregion

        #region Get By Id
        /// <summary>
        /// Get By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public tShoeCleaningRecrdWeekly GetById(long id)
        {
            return _shoeCleaningCheckRepository.GetById(id);
        }
        #endregion

        #region Check Weekly Record
        /// <summary>
        /// Check Weekly Record
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public bool CheckWeeklyRecord(DateTime startDate, string userId)
        {
            return _shoeCleaningCheckRepository.CheckWeeklyRecord(startDate, userId);

        }
        #endregion

        #region Get Shoes Cleaning History
        /// <summary>
        /// Get Shoes Cleaning History
        /// </summary>
        /// <param name="startDateTime"></param>
        /// <param name="endDateTime"></param>
        /// <returns></returns>
        public IEnumerable<tShoeCleaningRecrdWeekly> GetShoesCleaningHistory(DateTime startDateTime, DateTime endDateTime)
        {
            return _shoeCleaningCheckRepository.GetShoesCleaningHistory(startDateTime, endDateTime);
        }
        #endregion

        #region Is Check Performed
        /// <summary>
        /// Is Check Performed
        /// </summary>
        /// <param name="startDateTime"></param>
        /// <param name="endDateTime"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public bool IsCheckPerformed(DateTime startDateTime, DateTime endDateTime, string userId)
        {
            return _shoeCleaningCheckRepository.IsCheckPerformed(startDateTime, endDateTime, userId);
        }
        #endregion

        #region Get Shoe Cleaning History By ID
        /// <summary>
        /// Get Shoe Cleaning History By ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public tShoeCleaningRecrdWeekly GetShoeCleaningChecklistHistoryById(long id)
        {
            return _shoeCleaningCheckRepository.GetShoeCleaningChecklistHistoryById(id);
        }
        #endregion
    }
}