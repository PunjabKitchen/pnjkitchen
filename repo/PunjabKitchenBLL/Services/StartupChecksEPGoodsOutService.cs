﻿using System;
using System.Collections.Generic;
using PunjabKitchenBLL.Common;
using PunjabKitchenDAL.Interfaces;
using PunjabKitchenBLL.Interfaces;
using PunjabKitchenEntities.EntityModel;
using PunjabKitchenEntities.Interfaces;
// ReSharper disable All

namespace PunjabKitchenBLL.Services
{
    public class StartupChecksEPGoodsOutService : EntityService<tStartupChecksEPGoodsOut>, IStartupCheckEPGoodsOutService
    {
        #region Data Members
        // ReSharper disable once NotAccessedField.Local
        IUnitOfWork _unitOfWork;
        // ReSharper disable once FieldCanBeMadeReadOnly.Local
        IStartupChecksEPGoodsOutRepository _startupChecksEpGoodsOutRepository;
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="unitOfWork"></param>
        /// <param name="startupChecksEpGoodsOutRepository"></param>
        public StartupChecksEPGoodsOutService(IUnitOfWork unitOfWork, IStartupChecksEPGoodsOutRepository startupChecksEpGoodsOutRepository)
            : base(unitOfWork, startupChecksEpGoodsOutRepository)
        {
            _unitOfWork = unitOfWork;
            _startupChecksEpGoodsOutRepository = startupChecksEpGoodsOutRepository;
        }
        #endregion

        #region Get By Id
        /// <summary>
        /// Get By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public tStartupChecksEPGoodsOut GetById(long id)
        {
            return _startupChecksEpGoodsOutRepository.GetById(id);
        }
        #endregion

        #region Get Startup Checks EP Goods Out History
        /// <summary>
        /// Get Startup Checks EP Goods Out History
        /// </summary>
        /// <param name="startDateTime"></param>
        /// <param name="endDateTime"></param>
        /// <returns></returns>
        public IEnumerable<tStartupChecksEPGoodsOut> GetStartupChecksEpGoodsOutHistory(DateTime startDateTime, DateTime endDateTime)
        {
            return _startupChecksEpGoodsOutRepository.GetStartupChecksEPGoodsOutHistory(startDateTime, endDateTime);
        }
        #endregion

        #region Get Startup Checks EP Goods Out History By ID
        /// <summary>
        /// Get Startup Checks EP Goods Out History By ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public tStartupChecksEPGoodsOut GetStartupChecksEpGoodsOutHistoryById(long id)
        {
            return _startupChecksEpGoodsOutRepository.GetStartupChecksEpGoodsOutHistoryById(id);
        }
        #endregion
    }
}