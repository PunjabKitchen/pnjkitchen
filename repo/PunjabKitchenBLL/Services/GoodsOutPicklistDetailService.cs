﻿using PunjabKitchenBLL.Common;
using PunjabKitchenDAL.Interfaces;
using PunjabKitchenBLL.Interfaces;
using PunjabKitchenEntities.EntityModel;
using PunjabKitchenEntities.Interfaces;

namespace PunjabKitchenBLL.Services
{
    public class GoodsOutPicklistDetailService : EntityService<tGoodsOutPicklistDetail>, IGoodsOutPicklistDetailService
    {
        #region
        // ReSharper disable once NotAccessedField.Local
        IUnitOfWork _unitOfWork;
        readonly IGoodsOutPicklistDetailRepository _goodsOutPicklistDetailRepository;
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="unitOfWork"></param>
        /// <param name="goodsOutPicklistDetailRepository"></param>
        public GoodsOutPicklistDetailService(IUnitOfWork unitOfWork, IGoodsOutPicklistDetailRepository goodsOutPicklistDetailRepository)
            : base(unitOfWork, goodsOutPicklistDetailRepository)
        {
            _unitOfWork = unitOfWork;
            _goodsOutPicklistDetailRepository = goodsOutPicklistDetailRepository;
        }
        #endregion

        #region Get By Id
        /// <summary>
        /// Get By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public tGoodsOutPicklistDetail GetById(long id)
        {
            return _goodsOutPicklistDetailRepository.GetById(id);
        }
        #endregion
    }
}
