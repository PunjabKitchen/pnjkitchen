﻿using System;
using PunjabKitchenDAL.Interfaces;
using PunjabKitchenBLL.Interfaces;
using System.Collections.Generic;
using System.Linq;
using PunjabKitchenBLL.Common;
using PunjabKitchenEntities.EntityModel;
using PunjabKitchenEntities.Interfaces;

namespace PunjabKitchenBLL.Services
{
    public class CookingPlanService : EntityService<tCookingPlan>, ICookingPlanService
    {
        #region Data Members
        // ReSharper disable once NotAccessedField.Local
        // ReSharper disable once ArrangeTypeMemberModifiers
        IUnitOfWork _unitOfWork;
        readonly ICookingPlanRepository _cookingPlanRepository;
        // ReSharper disable once ArrangeTypeMemberModifiers
        readonly IStockRepository _stockRepository;
        // ReSharper disable once ArrangeTypeMemberModifiers
        readonly IProductRepository _productRepository;
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="unitOfWork"></param>
        /// <param name="cookingPlanRepository"></param>
        /// <param name="stockRepository"></param>
        /// <param name="productRepository"></param>
        public CookingPlanService(IUnitOfWork unitOfWork, ICookingPlanRepository cookingPlanRepository, IStockRepository stockRepository, IProductRepository productRepository)
            : base(unitOfWork, cookingPlanRepository)
        {
            _unitOfWork = unitOfWork;
            _cookingPlanRepository = cookingPlanRepository;
            _stockRepository = stockRepository;
            _productRepository = productRepository;
        }
        #endregion

        #region Get By Id
        /// <summary>
        /// Get By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public tCookingPlan GetById(long id)
        {
            return _cookingPlanRepository.GetById(id);
        }
        #endregion

        #region Get Cooking Plans By CookpotId
        /// <summary>
        /// Get Cooking Plans By CookpotId
        /// </summary>
        /// <param name="cookPotId"></param>
        /// <returns></returns>
        public List<tCookingPlan> GetCookingPlansByCookpotId(int cookPotId)
        {
            return _cookingPlanRepository.GetCookingPlansByCookpotId(cookPotId);
        }
        #endregion

        #region Create Cooking Plan
        /// <summary>
        /// Create Cooking Plan
        /// </summary>
        /// <param name="cookingPlan"></param>
        /// <returns></returns>
        public tCookingPlan CreateCookingPlan(tCookingPlan cookingPlan)
        {
            //Updating Stock Count
            UpdateStock(cookingPlan);

            //Adding Cooking Plan
            var result = _cookingPlanRepository.Add(cookingPlan);
            _cookingPlanRepository.Save();
            return result;
        }
        #endregion

        #region Update Cooking Plan
        /// <summary>
        /// Update Cooking Plan
        /// </summary>
        /// <param name="cookingPlan"></param>
        /// <returns></returns>
        public tCookingPlan UpdateCookingPlan(tCookingPlan cookingPlan)
        {
            //Updating Cooking Plan
            _cookingPlanRepository.Edit(cookingPlan);
            return cookingPlan;
        }
        #endregion

        #region Update Stock
        /// <summary>
        /// Update Stock
        /// </summary>
        /// <param name="cookingPlan"></param>
        /// <returns></returns>
        public void UpdateStock(tCookingPlan cookingPlan)
        {
            var list = new List<tStock>();
            //Getting Stock Items By Product Id
            if (cookingPlan.iProductId != null)
            {
                var product = _productRepository.GetById((long)cookingPlan.iProductId);
                list = _stockRepository.GetStockByProductCode(product.vProductCode).OrderBy(x => x.Sys_CreatedDateTime).ToList();
            }

            var quantityToAllocate = cookingPlan.iQuantity;

            foreach (var stockItem in list)
            {
                var stockItemAvailableQty = stockItem.iQuantity;
                //demand > available
                if (quantityToAllocate > stockItemAvailableQty)
                {
                    //demand = demand - available
                    quantityToAllocate = quantityToAllocate - stockItemAvailableQty;
                    stockItem.iQuantity = 0;
                }
                //Available greater than demand
                else
                {
                    //available = available - demand
                    stockItem.iQuantity = stockItemAvailableQty - quantityToAllocate;
                    break;
                }
                _stockRepository.Edit(stockItem);
            }
            //Saving Changes
            _stockRepository.Save();
        }
        #endregion

        #region Get By Code
        /// <summary>
        /// Get By Code
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public IEnumerable<tCookingPlan> GetByCode(string code)
        {
            return _cookingPlanRepository.GetByCode(code);
        }
        #endregion

        #region Get Cooking Plans By CookpotId
        /// <summary>
        /// Get Cooking Plans By CookpotId
        /// </summary>
        /// <param name="isCookPot1"></param>
        /// <param name="isCookPot2"></param>
        /// <param name="isCookPot3"></param>
        /// <returns></returns>
        public List<tCookingPlan> GetCookingPlansByCookpotId(bool isCookPot1, bool isCookPot2, bool isCookPot3)
        {
            return _cookingPlanRepository.GetCookingPlansByCookpotId(isCookPot1, isCookPot2, isCookPot3);
        }
        #endregion

        #region Get Batch Code
        /// <summary>
        /// Get Batch Code
        /// </summary>
        /// <returns></returns>
        public string GetBatchCode()
        {
            return _cookingPlanRepository.GetBatchCode();
        }
        #endregion

        #region Validate Batch Code
        /// <summary>
        /// Validate Batch Code
        /// </summary>
        /// <param name="batchCode"></param>
        /// <returns></returns>
        public bool ValidateBatchCode(string batchCode)
        {
            return _cookingPlanRepository.ValidateBatchCode(batchCode);
        }
        #endregion

        #region Get Todays Time Slot Availability
        /// <summary>
        /// Get Todays Time Slot Availability
        /// </summary>
        /// <param name="iCookPotNo"></param>
        /// <param name="startDateTime"></param>
        /// <param name="finishDateTime"></param>
        /// <returns></returns>
        public string GetTodaysTimeSlotAvailability(int iCookPotNo, DateTime startDateTime, DateTime finishDateTime)
        {
            var todaysDate = DateTime.Now;
            // ReSharper disable once PossibleInvalidOperationException
            var cookingRecords = _cookingPlanRepository.GetAll().Where(m => (m.iCookPotNo == iCookPotNo) && (m.tCookDate.Value.ToString("d/M/yyyy") == todaysDate.ToString("d/M/yyyy"))).ToList();

            if (cookingRecords.Count != 0)
            {
                // ReSharper disable once LoopCanBeConvertedToQuery
                foreach (var record in cookingRecords)
                {
                    if (((startDateTime >= record.tPlannedStartTime) && (startDateTime < record.tActualFinishTime)) || ((finishDateTime > record.tPlannedStartTime) && (finishDateTime < record.tActualFinishTime)))
                    {
                        return "Time slot already filled.";
                    }
                }
            }

            return "Time slot available.";
        }
        #endregion

        #region Check Finishing Sequence
        /// <summary>
        /// Check Finishing Sequence
        /// </summary>
        /// <param name="iCookingPlanId"></param>
        /// <returns></returns>
        public string CheckFinishingSequence(int iCookingPlanId)
        {
            var todaysDate = DateTime.Now;
            // ReSharper disable once PossibleInvalidOperationException
            var cookingRecord = _cookingPlanRepository.GetAll()?.FirstOrDefault(m => (m.tActualStartTime == null) 
                                                                                  && (m.tActualFinishedTime == null));

            //var cookingRecord = _cookingPlanRepository.GetAll()?.FirstOrDefault(m => (m.tActualStartTime == null)
            //                                                                      && (m.tActualFinishedTime == null)
            //                                                                      && (m.tCookDate.Value.ToString("d/M/yyyy") == todaysDate.ToString("d/M/yyyy")));

            // ReSharper disable once PossibleNullReferenceException
            if (cookingRecord == null || (cookingRecord != null && cookingRecord.iCookingPlanId == iCookingPlanId))
            {
                return "Valid Sequence";
            }

            return "Please finish the plans in sequence.";
        }
        #endregion
    }
}