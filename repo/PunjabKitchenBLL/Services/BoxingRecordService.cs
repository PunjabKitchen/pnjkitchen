﻿using PunjabKitchenBLL.Common;
using PunjabKitchenDAL.Interfaces;
using PunjabKitchenBLL.Interfaces;
using PunjabKitchenEntities.EntityModel;
using PunjabKitchenEntities.Interfaces;

namespace PunjabKitchenBLL.Services
{
    public class BoxingRecordService : EntityService<tBoxingRecord>, IBoxingRecordService
    {
        #region Data Members
        // ReSharper disable once NotAccessedField.Local
        IUnitOfWork _unitOfWork;
        readonly IBoxingRecordRepository _boxingRecordRepository;
        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="unitOfWork"></param>
        /// <param name="boxingRecordRepository"></param>
        public BoxingRecordService(IUnitOfWork unitOfWork, IBoxingRecordRepository boxingRecordRepository)
            : base(unitOfWork, boxingRecordRepository)
        {
            _unitOfWork = unitOfWork;
            _boxingRecordRepository = boxingRecordRepository;
        }
        #endregion

        #region Get By Id
        /// <summary>
        /// Get By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public tBoxingRecord GetById(long id)
        {
            return _boxingRecordRepository.GetById(id);
        }
        #endregion

        #region Get Batch Code
        /// <summary>
        /// Get Batch Code
        /// </summary>
        /// <returns></returns>
        public string GetBatchCode()
        {
            return _boxingRecordRepository.GetBatchCode();
        }
        #endregion

        #region Validate Batch Code
        /// <summary>
        /// Validate Batch Code
        /// </summary>
        /// <param name="batchCode"></param>
        /// <returns></returns>
        public bool ValidateBatchCode(string batchCode)
        {
            return _boxingRecordRepository.ValidateBatchCode(batchCode);
        }
        #endregion
    }
}