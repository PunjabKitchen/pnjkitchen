﻿using PunjabKitchenBLL.Common;
using PunjabKitchenDAL.Interfaces;
using PunjabKitchenBLL.Interfaces;
using PunjabKitchenEntities.EntityModel;
using PunjabKitchenEntities.Interfaces;

namespace PunjabKitchenBLL.Services
{
    public class GoodsOutPlanDetailService : EntityService<tGoodsOutPlanDetail>, IGoodsOutPlanDetailService
    {
        #region Data Members
        // ReSharper disable once NotAccessedField.Local
        IUnitOfWork _unitOfWork;
        readonly IGoodsOutPlanDetailRepository _goodsOutPlanDetailRepository;
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="unitOfWork"></param>
        /// <param name="goodsOutPlanDetailRepository"></param>
        public GoodsOutPlanDetailService(IUnitOfWork unitOfWork, IGoodsOutPlanDetailRepository goodsOutPlanDetailRepository)
            : base(unitOfWork, goodsOutPlanDetailRepository)
        {
            _unitOfWork = unitOfWork;
            _goodsOutPlanDetailRepository = goodsOutPlanDetailRepository;
        }
        #endregion

        #region Get By Id
        /// <summary>
        /// Get By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public tGoodsOutPlanDetail GetById(long id)
        {
            return _goodsOutPlanDetailRepository.GetById(id);
        }
        #endregion
    }
}