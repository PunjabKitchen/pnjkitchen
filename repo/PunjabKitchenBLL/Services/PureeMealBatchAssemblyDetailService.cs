﻿using PunjabKitchenBLL.Common;
using PunjabKitchenDAL.Interfaces;
using PunjabKitchenBLL.Interfaces;
using PunjabKitchenEntities.EntityModel;
using PunjabKitchenEntities.Interfaces;

namespace PunjabKitchenBLL.Services
{
    public class PureeMealBatchAssemblyDetailService : EntityService<tPureeMealBatchAssemblyDetail>, IPureeMealBatchAssemblyDetailService
    {
        #region Data Members
        readonly IPureeMealBatchAssemblyDetailRepository _pureeMealBatchAssemblyDetailRepository;
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="unitOfWork"></param>
        /// <param name="pureeMealBatchAssemblyDetailRepository"></param>
        public PureeMealBatchAssemblyDetailService(IUnitOfWork unitOfWork, IPureeMealBatchAssemblyDetailRepository pureeMealBatchAssemblyDetailRepository)
            : base(unitOfWork, pureeMealBatchAssemblyDetailRepository)
        {
            _pureeMealBatchAssemblyDetailRepository = pureeMealBatchAssemblyDetailRepository;
        }
        #endregion

        #region Get By Id
        /// <summary>
        /// Get By Id
        /// </summary>
        /// <param name="id"></param>
        public tPureeMealBatchAssemblyDetail GetById(long id)
        {
            return _pureeMealBatchAssemblyDetailRepository.GetById(id);
        }
        #endregion
    }
}