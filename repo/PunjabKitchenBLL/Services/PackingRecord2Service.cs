﻿using System;
using System.Collections.Generic;
using Audit.Core;
using PunjabKitchenBLL.Common;
using PunjabKitchenBLL.Interfaces;
using PunjabKitchenDAL.Interfaces;
using PunjabKitchenEntities.EntityModel;
using PunjabKitchenEntities.Interfaces;

namespace PunjabKitchenBLL.Services
{
    public class PackingRecord2Service : EntityService<tPackingRecord2>, IPackingRecord2Service
    {
        #region Data Members
        // ReSharper disable once ArrangeTypeMemberModifiers
        readonly IPackingRecord2Repository _packingRecord2Repository;
        // ReSharper disable once ArrangeTypeMemberModifiers
        readonly IPackingRecord2DetailRepository _packingRecord2DetailRepository;
        #endregion

        #region Constructor
        /// <summary>
        ///  Constructor
        /// </summary>
        /// <param name="unitOfWork"></param>
        /// <param name="packingRecord2Repository"></param>
#pragma warning disable 1573
#pragma warning disable 1573
        public PackingRecord2Service(IUnitOfWork unitOfWork, IPackingRecord2Repository packingRecord2Repository, IPackingRecord2DetailRepository packingRecord2DetailRepository)
#pragma warning restore 1573
#pragma warning restore 1573
            : base(unitOfWork, packingRecord2Repository)
        {
            _packingRecord2Repository = packingRecord2Repository;
            _packingRecord2DetailRepository = packingRecord2DetailRepository;
        }
        #endregion

        #region Get By Id
        /// <summary>
        /// Get By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public tPackingRecord2 GetById(long id)
        {
            return _packingRecord2Repository.GetById(id);
        }
        #endregion

        #region Create Packing Record 2
        /// <summary>
        /// Create Packing Record 2
        /// </summary>
        /// <param name="record"></param>
        /// <param name="usersList"></param>
        /// <returns></returns>
        public int CreateRecord(tPackingRecord2 record, List<string> usersList)
        {
            // ReSharper disable once RedundantAssignment
            var savedRecord = 0;

            try
            {
                _packingRecord2Repository.Add(record);
                _packingRecord2Repository.Save();
                savedRecord = record.iPackingRecordId;

                foreach (var user in usersList)
                {
                    _packingRecord2DetailRepository.Add(new tPackingRecord2Detail { UserId = user, iPackingRecordId = record.iPackingRecordId, Sys_CreatedBy = record.Sys_CreatedBy, Sys_CreatedDateTime = record.Sys_CreatedDateTime });
                }
                _packingRecord2DetailRepository.Save();
            }
            catch (Exception ex)
            {
                // ReSharper disable once PossibleIntendedRethrow
                throw ex;
            }

            return savedRecord;
        }
        #endregion

        #region Create User Detail Record 
        /// <summary>
        /// Create User Detail Record 
        /// </summary>
        /// <param name="record"></param>
        /// <param name="userId"></param>
        public void CreatePackerRecord(tPackingRecord2 record, string userId)
        {
            try
            {
                _packingRecord2DetailRepository.Add(new tPackingRecord2Detail
                {
                    iPackingRecordId = record.iPackingRecordId,
                    UserId = userId,
                    Sys_CreatedBy = record.Sys_CreatedBy,
                    Sys_CreatedDateTime = record.Sys_CreatedDateTime
                });
                _packingRecord2DetailRepository.Save();
            }
            catch (Exception ex)
            {
                AuditScope.CreateAndSave("Error saving Packer record for Packing Room Record.", new { Exception = ex.Message });
            }
        }
        #endregion
    }
}