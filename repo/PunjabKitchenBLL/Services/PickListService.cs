﻿using System;
using PunjabKitchenBLL.Common;
using PunjabKitchenDAL.Interfaces;
using PunjabKitchenBLL.Interfaces;
using PunjabKitchenEntities.EntityModel;
using PunjabKitchenEntities.Interfaces;


namespace PunjabKitchenBLL.Services
{
    public class PickListService : EntityService<tPickList>, IPickListService
    {
        #region Data Members
        // ReSharper disable once NotAccessedField.Local
        IUnitOfWork _unitOfWork;
        readonly IPickListRepository _pickListRepository;
        readonly IStockRepository _stockRepository;
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="unitOfWork"></param>
        /// <param name="pickListRepository"></param>
        /// <param name="stockRepository"></param>
        public PickListService(IUnitOfWork unitOfWork, IPickListRepository pickListRepository, IStockRepository stockRepository)
            : base(unitOfWork, pickListRepository)
        {
            _unitOfWork = unitOfWork;
            _pickListRepository = pickListRepository;
            _stockRepository = stockRepository;
        }
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="pickListItem"></param>
        public tPickList AddPickList(tPickList pickListItem)
        {
            //check id product allow this entry
            // ReSharper disable once PossibleInvalidOperationException
            var dbStock = _stockRepository.GetStockByProductId((int)pickListItem.iProductId);
            if (dbStock.iQuantity < pickListItem.iQuantity)
            {
                throw new Exception("Product Items is less then items picking up!");
            }
            var dbProductQty = dbStock.iQuantity;
            dbStock.iQuantity = (int?)(dbProductQty - pickListItem.iQuantity);
            _stockRepository.Edit(dbStock);
            _pickListRepository.Add(pickListItem);
            _pickListRepository.Save();
            return pickListItem;
        }
        #endregion
    }
}