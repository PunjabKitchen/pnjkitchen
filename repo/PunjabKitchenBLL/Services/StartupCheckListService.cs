﻿using PunjabKitchenBLL.Common;
using PunjabKitchenDAL.Interfaces;
using PunjabKitchenBLL.Interfaces;
using PunjabKitchenEntities.EntityModel;
using PunjabKitchenEntities.Interfaces;

namespace PunjabKitchenBLL.Services
{
    public class StartupCheckListService : EntityService<tStartUpCheckList>, IStartupCheckListService
    {
        #region Data Members
        // ReSharper disable once NotAccessedField.Local
        IUnitOfWork _unitOfWork;
        readonly IStartupCheckListRepository _startupCheckListRepository;
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="unitOfWork"></param>
        /// <param name="startupCheckListRepository"></param>
        public StartupCheckListService(IUnitOfWork unitOfWork, IStartupCheckListRepository startupCheckListRepository)
            : base(unitOfWork, startupCheckListRepository)
        {
            _unitOfWork = unitOfWork;
            _startupCheckListRepository = startupCheckListRepository;
        }
        #endregion

        #region Get By Id
        /// <summary>
        /// Get By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public tStartUpCheckList GetById(long id)
        {
            return _startupCheckListRepository.GetById(id);
        }
        #endregion
    }
}