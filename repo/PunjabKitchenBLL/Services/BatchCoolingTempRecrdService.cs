﻿using PunjabKitchenBLL.Common;
using PunjabKitchenBLL.Interfaces;
using PunjabKitchenDAL.Interfaces;
using PunjabKitchenEntities.EntityModel;
using PunjabKitchenEntities.Interfaces;

namespace PunjabKitchenBLL.Services
{
    class BatchCoolingTempRecrdService : EntityService<tBatchCoolingTempRecrd>, IBatchCoolingTempRecrdService
    {
        #region Data Members
        // ReSharper disable once NotAccessedField.Local
        IUnitOfWork _unitOfWork;
        // ReSharper disable once FieldCanBeMadeReadOnly.Local
        IBatchCoolingTempRecrdRepository _batchCoolingTempRecrdRepository;
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="unitOfWork"></param>
        /// <param name="batchCoolingTempRecrdRepository"></param>
        public BatchCoolingTempRecrdService(IUnitOfWork unitOfWork, IBatchCoolingTempRecrdRepository batchCoolingTempRecrdRepository)
            : base(unitOfWork, batchCoolingTempRecrdRepository)
        {
            _unitOfWork = unitOfWork;
            _batchCoolingTempRecrdRepository = batchCoolingTempRecrdRepository;
        }
        #endregion

        #region Get By Id
        /// <summary>
        /// Get By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public tBatchCoolingTempRecrd GetById(long id)
        {
            return _batchCoolingTempRecrdRepository.GetById(id);
        }
        #endregion
    }
}