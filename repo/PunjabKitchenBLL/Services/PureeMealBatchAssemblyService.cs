﻿using System;
using System.Collections.Generic;
using Audit.Core;
using PunjabKitchenBLL.Common;
using PunjabKitchenDAL.Interfaces;
using PunjabKitchenBLL.Interfaces;
using PunjabKitchenEntities.EntityModel;
using PunjabKitchenEntities.Interfaces;

namespace PunjabKitchenBLL.Services
{
    public class PureeMealBatchAssemblyService : EntityService<tPureeMeanBatchAssembly>, IPureeMealBatchAssemblyService
    {
        #region Data Members
        readonly IPureeMealBatchAssemblyRepository _pureeMealBatchAssemblyRepository;
        readonly IPureeMealBatchAssemblyPackerDetailRepository _pureeMealBatchAssemblyPackerDetailRepository;
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="unitOfWork"></param>
        /// <param name="pureeMealBatchAssemblyRepository"></param>
#pragma warning disable 1573
#pragma warning disable 1573
        public PureeMealBatchAssemblyService(IUnitOfWork unitOfWork, IPureeMealBatchAssemblyRepository pureeMealBatchAssemblyRepository, IPureeMealBatchAssemblyPackerDetailRepository pureeMealBatchAssemblyPackerDetailRepository)
#pragma warning restore 1573
#pragma warning restore 1573
            : base(unitOfWork, pureeMealBatchAssemblyRepository)
        {
            _pureeMealBatchAssemblyRepository = pureeMealBatchAssemblyRepository;
            _pureeMealBatchAssemblyPackerDetailRepository = pureeMealBatchAssemblyPackerDetailRepository;
        }
        #endregion

        #region Get By Id
        /// <summary>
        /// Get By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>tPureeMeanBatchAssembly</returns>
        public tPureeMeanBatchAssembly GetById(long id)
        {
            return _pureeMealBatchAssemblyRepository.GetById(id);
        }
        #endregion

        #region Create Puree Meal Batch Assembly Record
        /// <summary>
        /// Create Puree Meal Batch Assembly Record
        /// </summary>
        /// <param name="record"></param>
        /// <param name="usersList"></param>
        /// <returns></returns>
        public int CreateRecord(tPureeMeanBatchAssembly record, List<string> usersList)
        {
            // ReSharper disable once RedundantAssignment
            var savedRecord = 0;

            try
            {
                _pureeMealBatchAssemblyRepository.Add(record);
                _pureeMealBatchAssemblyRepository.Save();
                savedRecord = record.iPureeMeanBatchAssemblyId;

                foreach (var user in usersList)
                {
                    _pureeMealBatchAssemblyPackerDetailRepository.Add(new tPureeMealBatchAssemblyPackerDetail { UserId = user, iPureeMeanBatchAssemblyId = record.iPureeMeanBatchAssemblyId, Sys_CreatedBy = record.Sys_CreatedBy, Sys_CreatedDateTime = record.Sys_CreatedDateTime });
                }
                _pureeMealBatchAssemblyPackerDetailRepository.Save();
            }
            catch (Exception ex)
            {
                // ReSharper disable once PossibleIntendedRethrow
                throw ex;
            }

            return savedRecord;
        }
        #endregion

        #region Create User Detail Record 
        /// <summary>
        /// Create User Detail Record 
        /// </summary>
        /// <param name="record"></param>
        /// <param name="userId"></param>
        public void CreatePackerRecord(tPureeMeanBatchAssembly record, string userId)
        {
            try
            {
                _pureeMealBatchAssemblyPackerDetailRepository.Add(new tPureeMealBatchAssemblyPackerDetail
                {
                    iPureeMeanBatchAssemblyId = record.iPureeMeanBatchAssemblyId,
                    UserId = userId,
                    Sys_CreatedBy = record.Sys_CreatedBy,
                    Sys_CreatedDateTime = record.Sys_CreatedDateTime
                });
                _pureeMealBatchAssemblyPackerDetailRepository.Save();
            }
            catch (Exception ex)
            {
                AuditScope.CreateAndSave("Error saving Packer record for Puree Meal Batch Assembly Record.", new { Exception = ex.Message });
            }
        }
        #endregion
    }
}