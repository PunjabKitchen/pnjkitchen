﻿using PunjabKitchenBLL.Common;
using PunjabKitchenDAL.Interfaces;
using PunjabKitchenBLL.Interfaces;
using PunjabKitchenEntities.EntityModel;
using PunjabKitchenEntities.Interfaces;

namespace PunjabKitchenBLL.Services
{
    public class PureeShapesStarchAdditionRecordService : EntityService<tPureeShapesStarchAdditionRecord>, IPureeShapesStarchAdditionRecordService
    {
        #region Data Members
        readonly IPureeShapeStarchAdditionRecordRepository _pureeShapesStarchAdditionRecordRepository;
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="unitOfWork"></param>
        /// <param name="pureeShapesStarchAdditionRecordRepository"></param>
        public PureeShapesStarchAdditionRecordService(IUnitOfWork unitOfWork, IPureeShapeStarchAdditionRecordRepository pureeShapesStarchAdditionRecordRepository)
            : base(unitOfWork, pureeShapesStarchAdditionRecordRepository)
        {
            _pureeShapesStarchAdditionRecordRepository = pureeShapesStarchAdditionRecordRepository;
        }
        #endregion

        #region Get By Id
        /// <summary>
        /// Get By Id
        /// </summary>
        /// <param name="id"></param>
        public tPureeShapesStarchAdditionRecord GetById(long id)
        {
            return _pureeShapesStarchAdditionRecordRepository.GetById(id);
        }
        #endregion
    }
}