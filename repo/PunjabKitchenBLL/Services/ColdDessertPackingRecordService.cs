﻿using PunjabKitchenBLL.Common;
using PunjabKitchenDAL.Interfaces;
using PunjabKitchenBLL.Interfaces;
using PunjabKitchenEntities.EntityModel;
using PunjabKitchenEntities.Interfaces;

namespace PunjabKitchenBLL.Services
{
    public class ColdDessertPackingRecordService : EntityService<tColdDesertPackingRecord>, IColdDessertPackingRecordService
    {
        #region Data Members
        readonly IColdDessertPackingRecordRepository _colddessertPackingRepository;
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="unitOfWork"></param>
        /// <param name="colddessertPackingRepository"></param>
        public ColdDessertPackingRecordService(IUnitOfWork unitOfWork, IColdDessertPackingRecordRepository colddessertPackingRepository)
            : base(unitOfWork, colddessertPackingRepository)
        {
            _colddessertPackingRepository = colddessertPackingRepository;
        }
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="id"></param>
        public tColdDesertPackingRecord GetById(long id)
        {
            return _colddessertPackingRepository.GetById(id);
        }
        #endregion
    }
}