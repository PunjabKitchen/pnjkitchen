﻿using PunjabKitchenBLL.Common;
using PunjabKitchenDAL.Interfaces;
using PunjabKitchenBLL.Interfaces;
using PunjabKitchenEntities.EntityModel;
using PunjabKitchenEntities.Interfaces;

namespace PunjabKitchenBLL.Services
{
    public class EventService : EntityService<Event>, IEventService
    {
        #region Data Members
        // ReSharper disable once NotAccessedField.Local
        IUnitOfWork _unitOfWork;
        // ReSharper disable once NotAccessedField.Local
        readonly IEventRepository _eventRepository;
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="unitOfWork"></param>
        /// <param name="eventRepository"></param>
        public EventService(IUnitOfWork unitOfWork, IEventRepository eventRepository)
            : base(unitOfWork, eventRepository)
        {
            _unitOfWork = unitOfWork;
            _eventRepository = eventRepository;
        }
        #endregion
    }
}