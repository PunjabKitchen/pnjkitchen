﻿using System;
using System.Collections.Generic;
using PunjabKitchenBLL.Common;
using PunjabKitchenDAL.Interfaces;
using PunjabKitchenBLL.Interfaces;
using PunjabKitchenEntities.EntityModel;
using PunjabKitchenEntities.Interfaces;

namespace PunjabKitchenBLL.Services
{
    public class CleaningCheckListDailyService : EntityService<CleaningChecklistDaily>, ICleaningCheckListDailyService
    {
        #region Data Members
        // ReSharper disable once NotAccessedField.Local
        IUnitOfWork _unitOfWork;
        // ReSharper disable once ArrangeTypeMemberModifiers
        readonly ICleaningCheckListDailyRepository _cleaningCheckListDailyRepository;
        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="unitOfWork"></param>
        /// <param name="cleaningCheckListDailyRepository"></param>
        public CleaningCheckListDailyService(IUnitOfWork unitOfWork, ICleaningCheckListDailyRepository cleaningCheckListDailyRepository)
            : base(unitOfWork, cleaningCheckListDailyRepository)
        {
            _unitOfWork = unitOfWork;
            _cleaningCheckListDailyRepository = cleaningCheckListDailyRepository;
        }
        #endregion

        #region Get By Id
        /// <summary>
        /// Get By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public CleaningChecklistDaily GetById(long id)
        {
            return _cleaningCheckListDailyRepository.GetById(id);
        }
        #endregion

        #region Get Distinct Daily Check
        /// <summary>
        /// Get Distinct Daily Check
        /// </summary>
        /// <param name="currentDateTime"></param>
        /// <returns></returns>
        public IEnumerable<CleaningChecklistDaily> GetDistinctDailyCheck(DateTime currentDateTime)
        {
            return _cleaningCheckListDailyRepository.GetDistinctDailyCheck(currentDateTime);
        }
        #endregion

        #region Get Daily Cleaning Checklist History
        /// <summary>
        /// Get Daily Cleaning Checklist History
        /// </summary>
        /// <param name="startDateTime"></param>
        /// <param name="endDateTime"></param>
        /// <returns></returns>
        public IEnumerable<CleaningChecklistDaily> GetDailyCleaningChecklistHistory(DateTime startDateTime, DateTime endDateTime)
        {
            return _cleaningCheckListDailyRepository.GetDailyCleaningChecklistHistory(startDateTime, endDateTime);
        }
        #endregion

        #region Get Daily Cleaning Check list History By Id
        /// <summary>
        /// Get Daily Cleaning Check list History By Id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="areaId"></param>
        /// <returns></returns>
        public IEnumerable<CleaningChecklistDaily> GetDailyCleaningChecklistHistoryById(long id, long areaId)
        {
            return _cleaningCheckListDailyRepository.GetDailyCleaningChecklistHistoryById(id, areaId);
        }
        #endregion
    }
}