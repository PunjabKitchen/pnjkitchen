﻿using PunjabKitchenBLL.Common;
using PunjabKitchenBLL.Interfaces;
using PunjabKitchenDAL.Interfaces;
using PunjabKitchenEntities.EntityModel;
using PunjabKitchenEntities.Interfaces;

namespace PunjabKitchenBLL.Services
{
    class SlicingRecordService : EntityService<tSlicingRecord>, ISlicingRecordService
    {
        #region Data Members
        // ReSharper disable once NotAccessedField.Local
        IUnitOfWork _unitOfWork;
        ISlicingRecordRepository _slicingRecordRepository;
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="unitOfWork"></param>
        /// <param name="slicingRecordRepository"></param>
        public SlicingRecordService(IUnitOfWork unitOfWork, ISlicingRecordRepository slicingRecordRepository)
            : base(unitOfWork, slicingRecordRepository)
        {
            _unitOfWork = unitOfWork;
            _slicingRecordRepository = slicingRecordRepository;
        }
        #endregion

        #region Get By Id
        /// <summary>
        /// Get By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public tSlicingRecord GetById(long id)
        {
            return _slicingRecordRepository.GetById(id);
        }
        #endregion
    }
}