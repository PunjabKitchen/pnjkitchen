﻿using PunjabKitchenBLL.Common;
using PunjabKitchenBLL.Interfaces;
using PunjabKitchenDAL.Interfaces;
using PunjabKitchenEntities.EntityModel;
using PunjabKitchenEntities.Interfaces;

namespace PunjabKitchenBLL.Services
{
    public class PackingRecordDetailService : EntityService<tPackingRecordDetail>, IPackingRecordDetailService
    {
        #region Data Members
        readonly IPackingRecordDetailRepository _packingRecordDetailRepository;
        #endregion

        #region Constructor
        /// <summary>
        /// 
        /// </summary>
        /// <param name="unitOfWork"></param>
        /// <param name="packingRecordDetailRepository"></param>
        public PackingRecordDetailService(IUnitOfWork unitOfWork, IPackingRecordDetailRepository packingRecordDetailRepository)
            : base(unitOfWork, packingRecordDetailRepository)
        {
            _packingRecordDetailRepository = packingRecordDetailRepository;
        }
        #endregion

        #region Get By Id
        /// <summary>
        /// Get By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public tPackingRecordDetail GetById(long id)
        {
            return _packingRecordDetailRepository.GetById(id);
        }
        #endregion
    }
}