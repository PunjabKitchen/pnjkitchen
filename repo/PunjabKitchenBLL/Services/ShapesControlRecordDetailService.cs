﻿using PunjabKitchenBLL.Common;
using PunjabKitchenBLL.Interfaces;
using PunjabKitchenDAL.Interfaces;
using PunjabKitchenEntities.EntityModel;
using PunjabKitchenEntities.Interfaces;

namespace PunjabKitchenBLL.Services
{
    public class ShapesControlRecordDetailService : EntityService<tShapeControlRecordDetail>, IShapesControlRecordDetailService
    {
        #region Data Members
        readonly IShapesControlRecordDetailRepository _shapesControlRecordDetailRepository;
        #endregion

        #region Constructor
        /// <summary>
        /// 
        /// </summary>
        /// <param name="unitOfWork"></param>
        /// <param name="shapesControlRecordDetailRepository"></param>
        public ShapesControlRecordDetailService(IUnitOfWork unitOfWork, IShapesControlRecordDetailRepository shapesControlRecordDetailRepository)
            : base(unitOfWork, shapesControlRecordDetailRepository)
        {
            _shapesControlRecordDetailRepository = shapesControlRecordDetailRepository;
        }
        #endregion

        #region Get By Id
        /// <summary>
        /// Get By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public tShapeControlRecordDetail GetById(long id)
        {
            return _shapesControlRecordDetailRepository.GetById(id);
        }
        #endregion
    }
}