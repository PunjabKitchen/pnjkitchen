﻿using PunjabKitchenBLL.Common;
using PunjabKitchenDAL.Interfaces;
using PunjabKitchenBLL.Interfaces;
using PunjabKitchenEntities.EntityModel;
using PunjabKitchenEntities.Interfaces;

namespace PunjabKitchenBLL.Services
{
    public class MixedCaseBoxingRecordService : EntityService<tMixedCaseBoxingRecord>, IMixedCaseBoxingRecordService
    {
        #region Data Members
        // ReSharper disable once NotAccessedField.Local
        IUnitOfWork _unitOfWork;
        readonly IMixedCaseBoxingRecordRepository _mixedCaseBoxingRecordRepository;
        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="unitOfWork"></param>
        /// <param name="mixedCaseBoxingRecordRepository"></param>
        public MixedCaseBoxingRecordService(IUnitOfWork unitOfWork, IMixedCaseBoxingRecordRepository mixedCaseBoxingRecordRepository)
            : base(unitOfWork, mixedCaseBoxingRecordRepository)
        {
            _unitOfWork = unitOfWork;
            _mixedCaseBoxingRecordRepository = mixedCaseBoxingRecordRepository;
        }

        #endregion

        #region Get By Id
        /// <summary>
        /// Get By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public tMixedCaseBoxingRecord GetById(long id)
        {
            return _mixedCaseBoxingRecordRepository.GetById(id);
        }
        #endregion
    }
}