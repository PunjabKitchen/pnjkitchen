﻿using PunjabKitchenBLL.Common;
using PunjabKitchenBLL.Interfaces;
using PunjabKitchenDAL.Interfaces;
using PunjabKitchenEntities.EntityModel;
using PunjabKitchenEntities.Interfaces;

namespace PunjabKitchenBLL.Services
{
    public class PackingRecord2DetailService : EntityService<tPackingRecord2Detail>, IPackingRecord2DetailService
    {
        #region Data Members
        readonly IPackingRecord2DetailRepository _packingRecord2DetailRepository;
        #endregion

        #region Constructor
        /// <summary>
        ///  Constructor
        /// </summary>
        /// <param name="unitOfWork"></param>
        /// <param name="packingRecord2DetailRepository"></param>
        public PackingRecord2DetailService(IUnitOfWork unitOfWork, IPackingRecord2DetailRepository packingRecord2DetailRepository)
            : base(unitOfWork, packingRecord2DetailRepository)
        {
            _packingRecord2DetailRepository = packingRecord2DetailRepository;
        }
        #endregion

        #region Get By Id
        /// <summary>
        /// Get By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public tPackingRecord2Detail GetById(long id)
        {
            return _packingRecord2DetailRepository.GetById(id);
        }
        #endregion
    }
}