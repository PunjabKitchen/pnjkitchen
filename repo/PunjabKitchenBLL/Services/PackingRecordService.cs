﻿using System;
using System.Collections.Generic;
using Audit.Core;
using PunjabKitchenBLL.Common;
using PunjabKitchenBLL.Interfaces;
using PunjabKitchenDAL.Interfaces;
using PunjabKitchenEntities.EntityModel;
using PunjabKitchenEntities.Interfaces;

namespace PunjabKitchenBLL.Services
{
    public class PackingRecordService : EntityService<tPackingRecord>, IPackingRecordService
    {
        #region Data Members
        // ReSharper disable once ArrangeTypeMemberModifiers
        readonly IPackingRecordRepository _packingRecordRepository;
        // ReSharper disable once ArrangeTypeMemberModifiers
        readonly IPackingRecordDetailRepository _packingRecordDetailRepository;
        #endregion

        #region Constructor
        /// <summary>
        /// 
        /// </summary>
        /// <param name="unitOfWork"></param>
        /// <param name="packingRecordRepository"></param>
        /// <param name="packingRecordDetailRepository"></param>
        public PackingRecordService(IUnitOfWork unitOfWork, IPackingRecordRepository packingRecordRepository, IPackingRecordDetailRepository packingRecordDetailRepository)
            : base(unitOfWork, packingRecordRepository)
        {
            _packingRecordRepository = packingRecordRepository;
            _packingRecordDetailRepository = packingRecordDetailRepository;
        }
        #endregion

        #region Get By Id
        /// <summary>
        /// Get By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public tPackingRecord GetById(long id)
        {
            return _packingRecordRepository.GetById(id);
        }
        #endregion

        #region Create Packing Record
        /// <summary>
        /// Create Packing Record
        /// </summary>
        /// <param name="record"></param>
        /// <param name="usersList"></param>
        /// <returns></returns>
        public int CreateRecord(tPackingRecord record, List<string> usersList)
        {
            // ReSharper disable once RedundantAssignment
            var savedRecord = 0;

            try
            {
                _packingRecordRepository.Add(record);
                _packingRecordRepository.Save();
                savedRecord = record.iPackingRecordId;

                foreach (var user in usersList)
                {
                    _packingRecordDetailRepository.Add(new tPackingRecordDetail { UserId = user, iPackingRecordId = record.iPackingRecordId, Sys_CreatedBy = record.Sys_CreatedBy, Sys_CreatedDateTime = record.Sys_CreatedDateTime });
                }
                _packingRecordDetailRepository.Save();
            }
            catch (Exception ex)
            {
                // ReSharper disable once PossibleIntendedRethrow
                throw ex;
            }

            return savedRecord;
        }
        #endregion

        #region Create User Detail Record 
        /// <summary>
        /// Create User Detail Record 
        /// </summary>
        /// <param name="record"></param>
        /// <param name="userId"></param>
        public void CreatePackerRecord(tPackingRecord record, string userId)
        {
            try
            {
                _packingRecordDetailRepository.Add(new tPackingRecordDetail
                {
                    iPackingRecordId = record.iPackingRecordId,
                    UserId = userId,
                    Sys_CreatedBy = record.Sys_CreatedBy,
                    Sys_CreatedDateTime = record.Sys_CreatedDateTime
                });
                _packingRecordDetailRepository.Save();
            }
            catch (Exception ex)
            {
                AuditScope.CreateAndSave("Error saving Packer record for Packing Record.", new { Exception = ex.Message });
            }
        }
        #endregion
    }
}