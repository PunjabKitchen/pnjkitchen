﻿using PunjabKitchenBLL.Common;
using PunjabKitchenBLL.Interfaces;
using PunjabKitchenDAL.Interfaces;
using PunjabKitchenEntities.EntityModel;
using PunjabKitchenEntities.Interfaces;

namespace PunjabKitchenBLL.Services
{
    class RiceCoolingTempRecordService : EntityService<tRiceCoolingTempRecord>, IRiceCoolingTempRecordService
    {
        #region Data Members
        // ReSharper disable once NotAccessedField.Local
        IUnitOfWork _unitOfWork;
        IRiceCoolingTempRecordRepository _riceCoolingTempRecordRepository;
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="unitOfWork"></param>
        /// <param name="riceCoolingTempRecordRepository"></param>
        public RiceCoolingTempRecordService(IUnitOfWork unitOfWork, IRiceCoolingTempRecordRepository riceCoolingTempRecordRepository)
            : base(unitOfWork, riceCoolingTempRecordRepository)
        {
            _unitOfWork = unitOfWork;
            _riceCoolingTempRecordRepository = riceCoolingTempRecordRepository;
        }
        #endregion

        #region Get By Id
        /// <summary>
        /// Get By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public tRiceCoolingTempRecord GetById(long id)
        {
            return _riceCoolingTempRecordRepository.GetById(id);
        }
        #endregion
    }
}