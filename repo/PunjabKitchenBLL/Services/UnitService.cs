﻿using PunjabKitchenBLL.Common;
using PunjabKitchenDAL.Interfaces;
using PunjabKitchenBLL.Interfaces;
using PunjabKitchenEntities.EntityModel;
using PunjabKitchenEntities.Interfaces;

namespace PunjabKitchenBLL.Services
{

    public class UnitService : EntityService<tUnit>, IUnitService
    {
        #region Data Members
        // ReSharper disable once NotAccessedField.Local
        IUnitOfWork _unitOfWork;
        IUnitRepository _unitRepository;
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="unitOfWork"></param>
        /// <param name="unitRepository"></param>
        public UnitService(IUnitOfWork unitOfWork, IUnitRepository unitRepository)
            : base(unitOfWork, unitRepository)
        {
            _unitOfWork = unitOfWork;
            _unitRepository = unitRepository;
        }
        #endregion

        #region Get By Id
        /// <summary>
        /// Get By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public tUnit GetById(long id)
        {
            return _unitRepository.GetById(id);
        }
        #endregion
    }
}