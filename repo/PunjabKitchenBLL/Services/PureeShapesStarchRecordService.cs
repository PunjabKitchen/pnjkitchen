﻿using System;
using System.Collections.Generic;
using Audit.Core;
using PunjabKitchenBLL.Common;
using PunjabKitchenDAL.Interfaces;
using PunjabKitchenBLL.Interfaces;
using PunjabKitchenEntities.EntityModel;
using PunjabKitchenEntities.Interfaces;

namespace PunjabKitchenBLL.Services
{
    public class PureeShapesStarchRecordService : EntityService<tPureeShapesStarchRecord>, IPureeShapesStarchRecordService
    {
        #region Data Members
        readonly IPureeShapesStarchRecordRepository _pureeShapesStarchRecordRepository;
        readonly IPureeShapesStarchRecordDetailRepository _pureeShapesStarchRecordDetailRepository;
        readonly IPureeShapeStarchAdditionRecordRepository _pureeShapeStarchAdditionRecordRepository;
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="unitOfWork"></param>
        /// <param name="pureeShapesStarchRecordRepository"></param>
        /// <param name="pureeShapesStarchRecordDetailRepository"></param>
        /// <param name="pureeShapeStarchAdditionRecordRepository"></param>
        public PureeShapesStarchRecordService(IUnitOfWork unitOfWork, IPureeShapesStarchRecordRepository pureeShapesStarchRecordRepository, IPureeShapesStarchRecordDetailRepository pureeShapesStarchRecordDetailRepository, IPureeShapeStarchAdditionRecordRepository pureeShapeStarchAdditionRecordRepository)
            : base(unitOfWork, pureeShapesStarchRecordRepository)
        {
            _pureeShapesStarchRecordRepository = pureeShapesStarchRecordRepository;
            _pureeShapesStarchRecordDetailRepository = pureeShapesStarchRecordDetailRepository;
            _pureeShapeStarchAdditionRecordRepository = pureeShapeStarchAdditionRecordRepository;
        }
        #endregion

        #region Get By Id
        /// <summary>
        /// Get By Id
        /// </summary>
        /// <param name="id"></param>
        public tPureeShapesStarchRecord GetById(long id)
        {
            return _pureeShapesStarchRecordRepository.GetById(id);
        }
        #endregion

        #region Create Record with its Detail
        /// <summary>
        /// Create Record with its Detail
        /// </summary>
        /// <param name="record"></param>
        /// <param name="batchCodesList"></param>
        /// <param name="additionRecord"></param>
        /// <returns></returns>
        public int CreateRecord(tPureeShapesStarchRecord record, List<string> batchCodesList, tPureeShapesStarchAdditionRecord additionRecord)
        {
            try
            {
                // ReSharper disable once RedundantAssignment
                var savedRecord = 0;

                //For addition in Parent's table only for the first entry.
                if (!(record.iPureeShapeStarchRecordId > 0))
                {
                    _pureeShapesStarchRecordRepository.Add(record);
                    _pureeShapesStarchRecordRepository.Save();
                    savedRecord = record.iPureeShapeStarchRecordId;

                    foreach (var batchCode in batchCodesList)
                    {
                        _pureeShapesStarchRecordDetailRepository.Add(new tPureeShapesStarchRecordDetail
                        {
                            vBatchCode = batchCode,
                            iPureeShapeStarchRecordId = record.iPureeShapeStarchRecordId,
                            Sys_CreatedBy = record.Sys_CreatedBy,
                            Sys_CreatedDateTime = record.Sys_CreatedDateTime
                        });
                        _pureeShapesStarchRecordDetailRepository.Save();
                    }
                }
                else
                {
                    additionRecord.iPureeShapeStarchRecordId = record.iPureeShapeStarchRecordId;
                    _pureeShapeStarchAdditionRecordRepository.Add(additionRecord);
                    _pureeShapeStarchAdditionRecordRepository.Save();
                    savedRecord = additionRecord.iPureeShapeStarchRecordAdditionId;
                }

                return savedRecord;
            }
            catch (Exception ex)
            {
                AuditScope.CreateAndSave("Error saving record: ", new { Exception = ex.Message });
                return -1;
            }
        }
        #endregion

        #region Create Batch Detail Record 
        /// <summary>
        /// Create Batch Detail Record 
        /// </summary>
        /// <param name="record"></param>
        /// <param name="batchCode"></param>
        public void CreateBatchRecord(tPureeShapesStarchRecord record, string batchCode)
        {
            try
            {
                _pureeShapesStarchRecordDetailRepository.Add(new tPureeShapesStarchRecordDetail
                {
                    vBatchCode = batchCode,
                    iPureeShapeStarchRecordId = record.iPureeShapeStarchRecordId,
                    Sys_CreatedBy = record.Sys_CreatedBy,
                    Sys_CreatedDateTime = record.Sys_CreatedDateTime
                });
                _pureeShapesStarchRecordDetailRepository.Save();
            }
            catch (Exception ex)
            {
                AuditScope.CreateAndSave("Error saving record: ", new { Exception = ex.Message });
            }
        }
        #endregion

        #region Delete Addition Item
        /// <summary>
        /// Create Record with its Detail
        /// </summary>
        /// <param name="iPureeShapeStarchRecordAdditionId"></param>
        /// <returns></returns>
        public int DeleteAdditionItem(int iPureeShapeStarchRecordAdditionId)
        {
            // ReSharper disable once InconsistentNaming
            var deletedRecordID = 0;

            try
            {
                var record = _pureeShapeStarchAdditionRecordRepository.GetById(iPureeShapeStarchRecordAdditionId);
                deletedRecordID = record.iPureeShapeStarchRecordAdditionId;
                _pureeShapeStarchAdditionRecordRepository.Delete(record);
                _pureeShapeStarchAdditionRecordRepository.Save();
                return deletedRecordID;
            }
            // ReSharper disable once UnusedVariable
            catch (Exception ex)
            {
                return deletedRecordID;
            }
        }
        #endregion
    }
}