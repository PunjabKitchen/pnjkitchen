﻿using PunjabKitchenBLL.Common;
using PunjabKitchenDAL.Interfaces;
using PunjabKitchenBLL.Interfaces;
using PunjabKitchenEntities.EntityModel;
using PunjabKitchenEntities.Interfaces;

namespace PunjabKitchenBLL.Services
{
    public class BatchChangePureeRecordService : EntityService<tBatchChangeChkPureeRecrd>, IBatchChangePureeRecordService
    {
        #region Data Members
        readonly IBatchChangePureeRecordRepository _batchChangePureeRecordRepository;
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="unitOfWork"></param>
        /// <param name="batchChangePureeRecordRepository"></param>
        public BatchChangePureeRecordService(IUnitOfWork unitOfWork, IBatchChangePureeRecordRepository batchChangePureeRecordRepository)
            : base(unitOfWork, batchChangePureeRecordRepository)
        {
            _batchChangePureeRecordRepository = batchChangePureeRecordRepository;
        }
        #endregion

        #region Get By Id
        /// <summary>
        /// Get By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public tBatchChangeChkPureeRecrd GetById(long id)
        {
            return _batchChangePureeRecordRepository.GetById(id);
        }
        #endregion
    }
}