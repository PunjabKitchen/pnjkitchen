﻿using PunjabKitchenBLL.Common;
using PunjabKitchenDAL.Interfaces;
using PunjabKitchenBLL.Interfaces;
using PunjabKitchenEntities.EntityModel;
using PunjabKitchenEntities.Interfaces;

namespace PunjabKitchenBLL.Services
{
    public class MixedCaseBoxingRecordDetailService : EntityService<tMixedCaseBoxingRecordDetail>, IMixedCaseBoxingRecordDetailService
    {
        #region Data Members
        // ReSharper disable once NotAccessedField.Local
        // ReSharper disable once ArrangeTypeMemberModifiers
        IUnitOfWork _unitOfWork;
        // ReSharper disable once ArrangeTypeMemberModifiers
        readonly IMixedCaseBoxingRecordDetailRepository _mixedCaseBoxingRecordDetailRepository;
        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="unitOfWork"></param>
        /// <param name="mixedCaseBoxingRecordDetailRepository"></param>
        public MixedCaseBoxingRecordDetailService(IUnitOfWork unitOfWork, IMixedCaseBoxingRecordDetailRepository mixedCaseBoxingRecordDetailRepository)
            : base(unitOfWork, mixedCaseBoxingRecordDetailRepository)
        {
            _unitOfWork = unitOfWork;
            _mixedCaseBoxingRecordDetailRepository = mixedCaseBoxingRecordDetailRepository;
        }

        #endregion

        #region Get By Id
        /// <summary>
        /// Get By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public tMixedCaseBoxingRecordDetail GetById(long id)
        {
            return _mixedCaseBoxingRecordDetailRepository.GetById(id);
        }
        #endregion
    }
}