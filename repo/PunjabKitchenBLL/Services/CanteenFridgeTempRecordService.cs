﻿using System;
using PunjabKitchenDAL.Interfaces;
using PunjabKitchenBLL.Interfaces;
using System.Collections.Generic;
using PunjabKitchenBLL.Common;
using PunjabKitchenEntities.EntityModel;
using PunjabKitchenEntities.Interfaces;

namespace PunjabKitchenBLL.Services
{
    public class CanteenFridgeTempRecordService : EntityService<tCanteenFridgeTempRecordDaily>, ICanteenFridgeTempRecordService
    {
        #region Data Members
        // ReSharper disable once NotAccessedField.Local
        IUnitOfWork _unitOfWork;
        // ReSharper disable once FieldCanBeMadeReadOnly.Local
        ICanteenFridgeTempRecordRepository _canteenFridgeTempRecordRepository;
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="unitOfWork"></param>
        /// <param name="canteenFridgeTempRecordRepository"></param>
        public CanteenFridgeTempRecordService(IUnitOfWork unitOfWork, ICanteenFridgeTempRecordRepository canteenFridgeTempRecordRepository)
            : base(unitOfWork, canteenFridgeTempRecordRepository)
        {
            _unitOfWork = unitOfWork;
            _canteenFridgeTempRecordRepository = canteenFridgeTempRecordRepository;
        }
        #endregion

        #region Get By Id
        /// <summary>
        /// Get By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public tCanteenFridgeTempRecordDaily GetById(long id)
        {
            return _canteenFridgeTempRecordRepository.GetById(id);
        }
        #endregion

        #region Get Canteen Fridge Temperature History
        /// <summary>
        /// Get Canteen Fridge Temperature History
        /// </summary>
        /// <param name="startDateTime"></param>
        /// <param name="endDateTime"></param>
        /// <returns></returns>
        public IEnumerable<tCanteenFridgeTempRecordDaily> GetCanteenFridgeTemperatureHistory(DateTime startDateTime, DateTime endDateTime)
        {
            return _canteenFridgeTempRecordRepository.GetCanteenFridgeTemperatureHistory(startDateTime, endDateTime);
        }
        #endregion

        #region Get Canteen Fridge Temperature History By ID
        /// <summary>
        /// Get Canteen Fridge Temperature History By ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public tCanteenFridgeTempRecordDaily GetCanteenFridgeChecklistHistoryById(long id)
        {
            return _canteenFridgeTempRecordRepository.GetCanteenFridgeChecklistHistoryById(id);
        }
        #endregion
    }
}