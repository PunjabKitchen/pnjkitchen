﻿using System;
using System.Collections.Generic;
using PunjabKitchenBLL.Common;
using PunjabKitchenDAL.Interfaces;
using PunjabKitchenBLL.Interfaces;
using PunjabKitchenEntities.EntityModel;
using PunjabKitchenEntities.Interfaces;

namespace PunjabKitchenBLL.Services
{
    public class AttendanceService : EntityService<tEmployeeAttendance>, IAttendanceService
    {
        #region Data Members
        // ReSharper disable once NotAccessedField.Local
        IUnitOfWork _unitOfWork;
        readonly IAttendanceRepository _attendanceRepository;
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="unitOfWork"></param>
        /// <param name="attendanceRepository"></param>
        public AttendanceService(IUnitOfWork unitOfWork, IAttendanceRepository attendanceRepository)
            : base(unitOfWork, attendanceRepository)
        {
            _unitOfWork = unitOfWork;
            _attendanceRepository = attendanceRepository;
        }
        #endregion

        #region Get By Id
        /// <summary>
        /// Get By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public tEmployeeAttendance GetById(string id)
        {
            return _attendanceRepository.GetById(id);
        }
        #endregion

        #region Get All Records
        /// <summary>
        /// Get All Records By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public IEnumerable<tEmployeeAttendance> GetAllRecordsById(string id)
        {
            return _attendanceRepository.GetAllRecordsById(id);
        }

        /// <summary>
        /// Get Todays Record By User Id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="dateTime"></param>
        /// <returns></returns>
        public tEmployeeAttendance GetTodaysRecordByUserId(string id, DateTime dateTime)
        {
            return _attendanceRepository.GetTodaysRecordByUserId(id, dateTime);
        }
        #endregion

        #region Get Record for Editing
        /// <summary>
        /// Get Record for Editing
        /// </summary>
        /// <param name="iEmployeeId"></param>
        /// <param name="Sys_CreatedDateTime"></param>
        /// <returns></returns>
        // ReSharper disable once InconsistentNaming
        public tEmployeeAttendance GetRecordForEditing(string iEmployeeId, DateTime Sys_CreatedDateTime)
        {
            return _attendanceRepository.GetRecordForEditing(iEmployeeId, Sys_CreatedDateTime);
        }
        #endregion
    }
}