﻿using PunjabKitchenBLL.Common;
using PunjabKitchenBLL.Interfaces;
using PunjabKitchenDAL.Interfaces;
using PunjabKitchenEntities.EntityModel;
using PunjabKitchenEntities.Interfaces;

namespace PunjabKitchenBLL.Services
{
    public class PortionWeightsCheckRecordDetailService : EntityService<tPortionWeightCheckDetail>, IPortionWeightsCheckRecordDetailService
    {
        #region Data Members
        readonly IPortionWeightsCheckRecordDetailRepository _portionCheckRecordDetailRepository;
        #endregion

        #region Constructor
        /// <summary>
        /// 
        /// </summary>
        /// <param name="unitOfWork"></param>
        /// <param name="portionCheckRecordDetailRepository"></param>
        public PortionWeightsCheckRecordDetailService(IUnitOfWork unitOfWork, IPortionWeightsCheckRecordDetailRepository portionCheckRecordDetailRepository)
            : base(unitOfWork, portionCheckRecordDetailRepository)
        {
            _portionCheckRecordDetailRepository = portionCheckRecordDetailRepository;
        }
        #endregion

        #region Get By Id
        /// <summary>
        /// Get By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public tPortionWeightCheckDetail GetById(long id)
        {
            return _portionCheckRecordDetailRepository.GetById(id);
        }
        #endregion
    }
}