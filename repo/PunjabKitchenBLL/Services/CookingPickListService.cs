﻿using PunjabKitchenBLL.Common;
using PunjabKitchenBLL.Interfaces;
using PunjabKitchenDAL.Interfaces;
using PunjabKitchenEntities.EntityModel;
using PunjabKitchenEntities.Interfaces;

namespace PunjabKitchenBLL.Services
{
    class CookingPickListService : EntityService<tPickList>, ICookingPickListService
    {
        #region Data Members
        // ReSharper disable once NotAccessedField.Local
        IUnitOfWork _unitOfWork;
        // ReSharper disable once FieldCanBeMadeReadOnly.Local
        ICookingPickListRepository _cookingPickListRepository;
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="unitOfWork"></param>
        /// <param name="cookingPickListRepository"></param>
        public CookingPickListService(IUnitOfWork unitOfWork, ICookingPickListRepository cookingPickListRepository)
            : base(unitOfWork, cookingPickListRepository)
        {
            _unitOfWork = unitOfWork;
            _cookingPickListRepository = cookingPickListRepository;
        }
        #endregion

        #region Get By Id
        /// <summary>
        /// Get By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public tPickList GetById(long id)
        {
            return _cookingPickListRepository.GetById(id);
        }
        #endregion
    }
}