﻿using System;
using System.Collections.Generic;
using Audit.Core;
using PunjabKitchenBLL.Common;
using PunjabKitchenBLL.Interfaces;
using PunjabKitchenDAL.Interfaces;
using PunjabKitchenEntities.EntityModel;
using PunjabKitchenEntities.Interfaces;

namespace PunjabKitchenBLL.Services
{
    public class VegetarianPackingRecordService : EntityService<tVegetarianPackingRecord>, IVegetarianPackingRecordService
    {
        #region Data Members
        readonly IVegetarianPackingRecordRepository _vegetarianRecordRepository;
        readonly IVegetarianPackingRecordDetailRepository _vegetarianRecordDetailRepository;
        #endregion

        #region Constructor
        /// <summary>
        /// 
        /// </summary>
        /// <param name="unitOfWork"></param>
        /// <param name="vegetarianRecordRepository"></param>
        /// <param name="vegetarianRecordDetailRepository"></param>
        public VegetarianPackingRecordService(IUnitOfWork unitOfWork, IVegetarianPackingRecordRepository vegetarianRecordRepository, IVegetarianPackingRecordDetailRepository vegetarianRecordDetailRepository)
            : base(unitOfWork, vegetarianRecordRepository)
        {
            _vegetarianRecordRepository = vegetarianRecordRepository;
            _vegetarianRecordDetailRepository = vegetarianRecordDetailRepository;
        }
        #endregion

        #region Get By Id
        /// <summary>
        /// Get By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public tVegetarianPackingRecord GetById(long id)
        {
            return _vegetarianRecordRepository.GetById(id);
        }
        #endregion

        #region Create Packing Record
        /// <summary>
        /// Create Packing Record
        /// </summary>
        /// <param name="record"></param>
        /// <param name="usersList"></param>
        /// <returns></returns>
        public int CreateRecord(tVegetarianPackingRecord record, List<string> usersList)
        {
            // ReSharper disable once RedundantAssignment
            var savedRecord = 0;

            try
            {
                _vegetarianRecordRepository.Add(record);
                _vegetarianRecordRepository.Save();
                savedRecord = record.iVegeRecordId;

                foreach (var user in usersList)
                {
                    _vegetarianRecordDetailRepository.Add(new tVegetarianPackingRecordDetail { UserId = user, iVegeRecordId = record.iVegeRecordId, Sys_CreatedBy = record.Sys_CreatedBy, Sys_CreatedDateTime = record.Sys_CreatedDateTime });
                }
                _vegetarianRecordDetailRepository.Save();
            }
            catch (Exception ex)
            {
                // ReSharper disable once PossibleIntendedRethrow
                throw ex;
            }

            return savedRecord;
        }
        #endregion

        #region Create User Detail Record 
        /// <summary>
        /// Create User Detail Record 
        /// </summary>
        /// <param name="record"></param>
        /// <param name="userId"></param>
        public void CreatePackerRecord(tVegetarianPackingRecord record, string userId)
        {
            try
            {
                _vegetarianRecordDetailRepository.Add(new tVegetarianPackingRecordDetail
                {
                    iVegeRecordId = record.iVegeRecordId,
                    UserId = userId,
                    Sys_CreatedBy = record.Sys_CreatedBy,
                    Sys_CreatedDateTime = record.Sys_CreatedDateTime
                });
                _vegetarianRecordDetailRepository.Save();
            }
            catch (Exception ex)
            {
                AuditScope.CreateAndSave("Error saving Packer record for Vegetarian Record.", new { Exception = ex.Message });
            }
        }
        #endregion
    }
}
