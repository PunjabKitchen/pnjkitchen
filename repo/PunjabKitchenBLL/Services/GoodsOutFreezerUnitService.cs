﻿using System;
using System.Collections.Generic;
using PunjabKitchenBLL.Common;
using PunjabKitchenDAL.Interfaces;
using PunjabKitchenBLL.Interfaces;
using PunjabKitchenEntities.EntityModel;
using PunjabKitchenEntities.Interfaces;

namespace PunjabKitchenBLL.Services
{
    public class GoodsOutFreezerUnitService : EntityService<tGoodsOutFreezerUnit>, IGoodsOutFreezerUnitService
    {
        #region Data Members
        // ReSharper disable once NotAccessedField.Local
        IUnitOfWork _unitOfWork;
        // ReSharper disable once FieldCanBeMadeReadOnly.Local
        IGoodsOutFreezerUnitRepository _goodsOutFreezerUnit;
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="unitOfWork"></param>
        /// <param name="goodsOutFreezerUnit"></param>
        public GoodsOutFreezerUnitService(IUnitOfWork unitOfWork, IGoodsOutFreezerUnitRepository goodsOutFreezerUnit)
            : base(unitOfWork, goodsOutFreezerUnit)
        {
            _unitOfWork = unitOfWork;
            _goodsOutFreezerUnit = goodsOutFreezerUnit;
        }
        #endregion

        #region Get By Id
        /// <summary>
        /// Get By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public tGoodsOutFreezerUnit GetById(long id)
        {
            return _goodsOutFreezerUnit.GetById(id);
        }
        #endregion

        #region Get Goods Out Freezer Unit History
        /// <summary>
        /// Get Goods Out Freezer Unit History
        /// </summary>
        /// <param name="startDateTime"></param>
        /// <param name="endDateTime"></param>
        /// <returns></returns>
        public IEnumerable<tGoodsOutFreezerUnit> GetGoodsOutFreezerUnitHistory(DateTime startDateTime, DateTime endDateTime)
        {
            return _goodsOutFreezerUnit.GetGoodsOutFreezerUnitHistory(startDateTime, endDateTime);
        }
        #endregion

        #region Get Goods Out Freezer Unit History By ID
        /// <summary>
        /// Get Goods Out Freezer Unit History By ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public tGoodsOutFreezerUnit GetGoodsOutFreezerUnitHistoryById(long id)
        {
            return _goodsOutFreezerUnit.GetGoodsOutFreezerUnitHistoryById(id);
        }
        #endregion
    }
}