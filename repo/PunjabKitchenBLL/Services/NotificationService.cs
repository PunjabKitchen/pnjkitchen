﻿using System;
using System.Collections.Generic;
using PunjabKitchenBLL.Common;
using PunjabKitchenBLL.Interfaces;
using PunjabKitchenDAL.Interfaces;
using PunjabKitchenEntities.EntityModel;
using PunjabKitchenEntities.Interfaces;

namespace PunjabKitchenBLL.Services
{
    public class NotificationService : EntityService<tNotification>, INotificationService
    {
        #region Data Members
        // ReSharper disable once NotAccessedField.Local
        IUnitOfWork _unitOfWork;
        readonly INotificationRepository _notificationRepository;
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="unitOfWork"></param>
        /// <param name="notificationRepository"></param>
        public NotificationService(IUnitOfWork unitOfWork, INotificationRepository notificationRepository)
            : base(unitOfWork, notificationRepository)
        {
            _unitOfWork = unitOfWork;
            _notificationRepository = notificationRepository;
        }
        #endregion

        #region Method Get Notification By Id
        /// <summary>
        /// Method Get Notification By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public tNotification GetById(long id)
        {
            return _notificationRepository.GetById(id);
        }
        #endregion

        #region Get Notifications Count
        /// <summary>
        /// Get Notifications Count
        /// </summary>
        /// <returns></returns>
        public int GetNotificationsCount()
        {
            return _notificationRepository.GetNotificationsCount();
        }
        #endregion

        #region Can Assig nNotification To User
        /// <summary>
        /// Can Assig nNotification To User
        /// </summary>
        /// <param name="notificationId"></param>
        /// <returns></returns>
        public bool CanAssignNotificationToUser(int notificationId)
        {
            return _notificationRepository.CanAssignNotificationToUser(notificationId);
        }
        #endregion

        #region Get Notification By Date
        /// <summary>
        /// Get Notification By Date
        /// </summary>
        /// <param name="dateTime"></param>
        /// <returns></returns>
        public IEnumerable<tNotification> GetNotificationByDate(DateTime dateTime)
        {
            return _notificationRepository.GetNotificationByDate(dateTime);
        }
        #endregion

        #region Get Latest Notification By NotificationTypeID

        /// <summary>
        /// Get Latest Notification By NotificationTypeID
        /// </summary>
        /// <param name="notificationTypeID"></param>
        /// <returns></returns>
        // ReSharper disable once InconsistentNaming
        public tNotification GetLatestNotificationByType(int notificationTypeID)
        {
            return _notificationRepository.GetLatestNotificationByType(notificationTypeID);
        }
        #endregion
    }
}