﻿using System.Collections.Generic;
using PunjabKitchenBLL.Common;
using PunjabKitchenDAL.Interfaces;
using PunjabKitchenBLL.Interfaces;
using PunjabKitchenEntities.EntityModel;
using PunjabKitchenEntities.Interfaces;

namespace PunjabKitchenBLL.Services
{
    public class RecipeService : EntityService<tRecipe>, IRecipeService
    {
        #region Data Members
        readonly IRecipeRepository _recipeRepository;
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="unitOfWork"></param>
        /// <param name="repository"></param>
        /// <param name="recipeRepository"></param>
        public RecipeService(IUnitOfWork unitOfWork, IGenericRepository<tRecipe> repository, IRecipeRepository recipeRepository) : base(unitOfWork, repository)
        {
            _recipeRepository = recipeRepository;
        }
        #endregion

        #region Get By Id
        /// <summary>
        /// Get By Id
        /// </summary>
        /// <param name="id"></param>
        public tRecipe GetById(long id)
        {
            return _recipeRepository.GetById(id);
        }
        #endregion

        #region Get By Code
        /// <summary>
        /// Get By Code
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public IEnumerable<tRecipe> GetByCode(string code)
        {
            return _recipeRepository.GetByCode(code);
        }
        #endregion
    }
}