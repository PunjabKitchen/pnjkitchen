﻿using PunjabKitchenBLL.Common;
using PunjabKitchenDAL.Interfaces;
using PunjabKitchenBLL.Interfaces;
using PunjabKitchenEntities.EntityModel;
using PunjabKitchenEntities.Interfaces;

namespace PunjabKitchenBLL.Services
{
    public class NotificationTypeService : EntityService<tNotificationType>, INotificationTypeService
    {
        #region Data Members
        // ReSharper disable once NotAccessedField.Local
        IUnitOfWork _unitOfWork;
        // ReSharper disable once NotAccessedField.Local
        INotificationTypeRepository _notificationTypeRepository;
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="unitOfWork"></param>
        /// <param name="notificationTypeRepository"></param>
        public NotificationTypeService(IUnitOfWork unitOfWork, INotificationTypeRepository notificationTypeRepository) : base(unitOfWork, notificationTypeRepository)
        {
            _unitOfWork = unitOfWork;
            _notificationTypeRepository = notificationTypeRepository;
        }
        #endregion
    }
}