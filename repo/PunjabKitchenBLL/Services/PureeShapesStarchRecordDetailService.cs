﻿using PunjabKitchenBLL.Common;
using PunjabKitchenDAL.Interfaces;
using PunjabKitchenBLL.Interfaces;
using PunjabKitchenEntities.EntityModel;
using PunjabKitchenEntities.Interfaces;

namespace PunjabKitchenBLL.Services
{
    public class PureeShapesStarchRecordDetailService : EntityService<tPureeShapesStarchRecordDetail>, IPureeShapesStarchRecordDetailService
    {
        #region Data Members
        readonly IPureeShapesStarchRecordDetailRepository _pureeShapesStarchRecordDetailRepository;
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="unitOfWork"></param>
        /// <param name="pureeShapesStarchRecordDetailRepository"></param>
        public PureeShapesStarchRecordDetailService(IUnitOfWork unitOfWork, IPureeShapesStarchRecordDetailRepository pureeShapesStarchRecordDetailRepository)
            : base(unitOfWork, pureeShapesStarchRecordDetailRepository)
        {
            _pureeShapesStarchRecordDetailRepository = pureeShapesStarchRecordDetailRepository;
        }
        #endregion

        #region Get By Id
        /// <summary>
        /// Get By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public tPureeShapesStarchRecordDetail GetById(long id)
        {
            return _pureeShapesStarchRecordDetailRepository.GetById(id);
        }
        #endregion
    }
}