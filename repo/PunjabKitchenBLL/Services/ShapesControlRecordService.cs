﻿using System;
using PunjabKitchenBLL.Common;
using PunjabKitchenBLL.Interfaces;
using PunjabKitchenDAL.Interfaces;
using PunjabKitchenEntities.EntityModel;
using PunjabKitchenEntities.Interfaces;
using System.Collections.Generic;
using Audit.Core;

namespace PunjabKitchenBLL.Services
{
    public class ShapesControlRecordService : EntityService<tShapeControlRecord>, IShapesControlRecordService
    {
        #region Data Members
        readonly IShapesControlRecordRepository _shapesControlRecordRepository;
        readonly IShapesControlRecordDetailRepository _shapesControlRecordDetailRepository;
        readonly IShapesControlRecordBatchDetailRepository _shapesControlRecordBatchDetailRepository;
        #endregion

        #region Constructor
        /// <summary>
        /// 
        /// </summary>
        /// <param name="unitOfWork"></param>
        /// <param name="shapesControlRecordRepository"></param>
        /// <param name="shapesControlRecordDetailRepository"></param>
        /// <param name="shapesControlRecordBatchDetailRepository"></param>
        public ShapesControlRecordService(IUnitOfWork unitOfWork, IShapesControlRecordRepository shapesControlRecordRepository,
            IShapesControlRecordDetailRepository shapesControlRecordDetailRepository, IShapesControlRecordBatchDetailRepository shapesControlRecordBatchDetailRepository)
            : base(unitOfWork, shapesControlRecordRepository)
        {
            _shapesControlRecordRepository = shapesControlRecordRepository;
            _shapesControlRecordDetailRepository = shapesControlRecordDetailRepository;
            _shapesControlRecordBatchDetailRepository = shapesControlRecordBatchDetailRepository;
        }
        #endregion

        #region Get By Id
        /// <summary>
        /// Get By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public tShapeControlRecord GetById(long id)
        {
            return _shapesControlRecordRepository.GetById(id);
        }
        #endregion

        #region Create Shapes Records
        /// <summary>
        /// Create Shapes Records
        /// </summary>
        /// <param name="record"></param>
        /// <param name="usersList"></param>
        /// <param name="batchList"></param>
        /// <returns></returns>
        public int CreateRecord(tShapeControlRecord record, List<string> usersList, List<string> batchList)
        {
            // ReSharper disable once RedundantAssignment
            var savedRecord = 0;

            try
            {
                _shapesControlRecordRepository.Add(record);
                _shapesControlRecordRepository.Save();
                savedRecord = record.iShapeControlRecordId;

                foreach (var user in usersList)
                {
                    _shapesControlRecordDetailRepository.Add(new tShapeControlRecordDetail { UserId = user, iShapeControlRecordId = record.iShapeControlRecordId, Sys_CreatedBy = record.Sys_CreatedBy, Sys_CreatedDateTime = record.Sys_CreatedDateTime });
                }
                _shapesControlRecordDetailRepository.Save();

                foreach (var batchCode in batchList)
                {
                    _shapesControlRecordBatchDetailRepository.Add(new tShapesControlRecordBatchDetail { vBatchCode = batchCode, iShapeControlRecordId = record.iShapeControlRecordId, Sys_CreatedBy = record.Sys_CreatedBy, Sys_CreatedDateTime = record.Sys_CreatedDateTime });
                }
                _shapesControlRecordBatchDetailRepository.Save();
            }
            catch (Exception ex)
            {
                // ReSharper disable once PossibleIntendedRethrow
                throw ex;
            }

            return savedRecord;
        }
        #endregion

        #region Create Batch Detail Record 
        /// <summary>
        /// Create Batch Detail Record 
        /// </summary>
        /// <param name="record"></param>
        /// <param name="batchCode"></param>
        public void CreateBatchRecord(tShapeControlRecord record, string batchCode)
        {
            try
            {
                _shapesControlRecordBatchDetailRepository.Add(new tShapesControlRecordBatchDetail
                {
                    vBatchCode = batchCode,
                    iShapeControlRecordId = record.iShapeControlRecordId,
                    Sys_CreatedBy = record.Sys_CreatedBy,
                    Sys_CreatedDateTime = record.Sys_CreatedDateTime
                });
                _shapesControlRecordBatchDetailRepository.Save();
            }
            catch (Exception ex)
            {
                AuditScope.CreateAndSave("Error saving Batch record for Shape Control Record.", new { Exception = ex.Message });
            }
        }
        #endregion

        #region Create Packer Detail Record 
        /// <summary>
        /// Create Batch Detail Record 
        /// </summary>
        /// <param name="record"></param>
        /// <param name="packerId"></param>
        public void CreatePackerRecord(tShapeControlRecord record, string packerId)
        {
            try
            {
                _shapesControlRecordDetailRepository.Add(new tShapeControlRecordDetail
                {
                    UserId = packerId,
                    iShapeControlRecordId = record.iShapeControlRecordId,
                    Sys_CreatedBy = record.Sys_CreatedBy,
                    Sys_CreatedDateTime = record.Sys_CreatedDateTime
                });
                _shapesControlRecordDetailRepository.Save();
            }
            catch (Exception ex)
            {
                AuditScope.CreateAndSave("Error saving Packer record for Shape Control Record.", new { Exception = ex.Message });
            }
        }
        #endregion
    }
}