﻿using PunjabKitchenBLL.Common;
using PunjabKitchenBLL.Interfaces;
using PunjabKitchenDAL.Interfaces;
using PunjabKitchenEntities.EntityModel;
using PunjabKitchenEntities.Interfaces;

namespace PunjabKitchenBLL.Services
{
    class MeatMincingRecordService : EntityService<tMeatMincingRecord>, IMeatMincingRecordService
    {
        #region Data Members
        // ReSharper disable once NotAccessedField.Local
        IUnitOfWork _unitOfWork;
        // ReSharper disable once FieldCanBeMadeReadOnly.Local
        // ReSharper disable once ArrangeTypeMemberModifiers
        IMeatMincingRecordRepository _meatMincingRecordRepository;
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="unitOfWork"></param>
        /// <param name="meatMincingRecordRepository"></param>
        public MeatMincingRecordService(IUnitOfWork unitOfWork, IMeatMincingRecordRepository meatMincingRecordRepository)
            : base(unitOfWork, meatMincingRecordRepository)
        {
            _unitOfWork = unitOfWork;
            _meatMincingRecordRepository = meatMincingRecordRepository;
        }
        #endregion

        #region Get By Id
        /// <summary>
        /// Get By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public tMeatMincingRecord GetById(long id)
        {
            return _meatMincingRecordRepository.GetById(id);
        }
        #endregion
    }
}