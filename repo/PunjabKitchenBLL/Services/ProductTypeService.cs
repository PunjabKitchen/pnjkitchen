﻿using PunjabKitchenBLL.Common;
using PunjabKitchenDAL.Interfaces;
using PunjabKitchenBLL.Interfaces;
using PunjabKitchenEntities.EntityModel;
using PunjabKitchenEntities.Interfaces;

namespace PunjabKitchenBLL.Services
{

    public class ProductTypeService : EntityService<tProductType>, IProductTypeService
    {
        #region Data Members
        // ReSharper disable once NotAccessedField.Local
        IUnitOfWork _unitOfWork;
        // ReSharper disable once FieldCanBeMadeReadOnly.Local
        IProductTypeRepository _productTypeRepository;
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="unitOfWork"></param>
        /// <param name="productTypeRepository"></param>
        public ProductTypeService(IUnitOfWork unitOfWork, IProductTypeRepository productTypeRepository)
            : base(unitOfWork, productTypeRepository)
        {
            _unitOfWork = unitOfWork;
            _productTypeRepository = productTypeRepository;
        }
        #endregion

        #region Get By Id
        /// <summary>
        /// Get By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public tProductType GetById(long id)
        {
            return _productTypeRepository.GetById(id);
        }
        #endregion
    }
}
