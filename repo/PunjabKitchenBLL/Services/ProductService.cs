﻿using PunjabKitchenDAL.Interfaces;
using PunjabKitchenBLL.Interfaces;
using System.Collections.Generic;
using PunjabKitchenBLL.Common;
using PunjabKitchenEntities.EntityModel;
using PunjabKitchenEntities.Interfaces;

namespace PunjabKitchenBLL.Services
{
    public class ProductService : EntityService<tProduct>, IProductService
    {
        #region Data Members
        // ReSharper disable once NotAccessedField.Local
        IUnitOfWork _unitOfWork;
        readonly IProductRepository _productRepository;
        readonly IProductTypeRepository _productTypeRepository;
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="unitOfWork"></param>
        /// <param name="productRepository"></param>
        /// <param name="productTypeRepository"></param>
        public ProductService(IUnitOfWork unitOfWork, IProductRepository productRepository, IProductTypeRepository productTypeRepository)
            : base(unitOfWork, productRepository)
        {
            _unitOfWork = unitOfWork;
            _productRepository = productRepository;
            _productTypeRepository = productTypeRepository;
        }
        #endregion

        #region Get By Id
        /// <summary>
        /// Get By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public tProduct GetById(long id)
        {
            return _productRepository.GetById(id);
        }
        #endregion

        #region Get By Code
        /// <summary>
        /// Get By Code
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public IEnumerable<tProduct> GetByCode(string code)
        {
            return _productRepository.GetByCode(code);
        }
        #endregion

        #region Get Product Code
        /// <summary>
        /// Get Product Code
        /// </summary>
        /// <param name="productTypeId"></param>
        /// <returns></returns>
        public string GetProductCode(int productTypeId)
        {
            var prefixCode = _productTypeRepository.GetPrefixById(productTypeId);
            return _productRepository.GetProductCode(productTypeId, prefixCode);
        }
        #endregion

        #region Validate Code
        /// <summary>
        /// Validate Code
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public bool ValidateCode(string code)
        {
            return _productRepository.ValidateCode(code);
        }
        #endregion
    }
}