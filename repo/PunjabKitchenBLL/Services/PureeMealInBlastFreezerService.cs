﻿using System;
using PunjabKitchenBLL.Common;
using PunjabKitchenDAL.Interfaces;
using PunjabKitchenBLL.Interfaces;
using PunjabKitchenEntities.EntityModel;
using PunjabKitchenEntities.Interfaces;

namespace PunjabKitchenBLL.Services
{
    public class PureeMealInBlastFreezerService : EntityService<tPureeMealInBlastFreezer>, IPureeMealInBlastService
    {
        #region Data Members
        readonly IPureeMealInBlastFreezerRepository _pureeMealInBlastFreezerRepository;
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="unitOfWork"></param>
        /// <param name="pureeMealInBlastFreezerRepository"></param>
        public PureeMealInBlastFreezerService(IUnitOfWork unitOfWork, IPureeMealInBlastFreezerRepository pureeMealInBlastFreezerRepository)
            : base(unitOfWork, pureeMealInBlastFreezerRepository)
        {
            _pureeMealInBlastFreezerRepository = pureeMealInBlastFreezerRepository;
        }
        #endregion

        #region Get By Id
        /// <summary>
        /// Get By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>tPureeMeanBatchAssembly</returns>
        public tPureeMealInBlastFreezer GetById(long id)
        {
            return _pureeMealInBlastFreezerRepository.GetById(id);
        }
        #endregion

        #region Create Record
        /// <summary>
        /// Create Packing Record
        /// </summary>
        /// <param name="record"></param>
        /// <returns></returns>
        public int CreateRecord(tPureeMealInBlastFreezer record)
        {
            // ReSharper disable once RedundantAssignment
            var savedRecord = 0;

            try
            {
                _pureeMealInBlastFreezerRepository.Add(record);
                _pureeMealInBlastFreezerRepository.Save();
                savedRecord = record.iPureeMealInBlastFreezerId;
            }
            catch (Exception ex)
            {
                // ReSharper disable once PossibleIntendedRethrow
                throw ex;
            }

            return savedRecord;
        }
        #endregion

        #region Delete Freezer Record
        /// <summary>
        /// Create Freezer Record
        /// </summary>
        /// <param name="iPureeMealInBlastFreezerId"></param>
        /// <returns></returns>
        public int DeleteFreezerRecord(int iPureeMealInBlastFreezerId)
        {
            // ReSharper disable once InconsistentNaming
            var deletedRecordID = 0;

            try
            {
                var record = _pureeMealInBlastFreezerRepository.GetById(iPureeMealInBlastFreezerId);
                deletedRecordID = record.iPureeMealInBlastFreezerId;
                _pureeMealInBlastFreezerRepository.Delete(record);
                _pureeMealInBlastFreezerRepository.Save();
                return deletedRecordID;
            }
            // ReSharper disable once UnusedVariable
            catch (Exception ex)
            {
                return deletedRecordID;
            }
        }
        #endregion
    }
}