﻿using PunjabKitchenBLL.Common;
using PunjabKitchenDAL.Interfaces;
using PunjabKitchenBLL.Interfaces;
using PunjabKitchenEntities.EntityModel;
using PunjabKitchenEntities.Interfaces;

namespace PunjabKitchenBLL.Services
{
    public class GoodsInService : EntityService<tGoodsIn>, IGoodsInService
    {
        #region Data Members
        // ReSharper disable once NotAccessedField.Local
        IUnitOfWork _unitOfWork;
        readonly IGoodsInRepository _goodsInRepository;
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="unitOfWork"></param>
        /// <param name="goodsInRepository"></param>
        public GoodsInService(IUnitOfWork unitOfWork, IGoodsInRepository goodsInRepository)
            : base(unitOfWork, goodsInRepository)
        {
            _unitOfWork = unitOfWork;
            _goodsInRepository = goodsInRepository;
        }
        #endregion

        #region Get By Id
        /// <summary>
        /// Get By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public tGoodsIn GetById(long id)
        {
            return _goodsInRepository.GetById(id);
        }
        #endregion
    }
}