﻿using System;
using PunjabKitchenDAL.Interfaces;
using PunjabKitchenBLL.Interfaces;
using System.Collections.Generic;
using PunjabKitchenBLL.Common;
using PunjabKitchenEntities.EntityModel;
using PunjabKitchenEntities.Interfaces;

namespace PunjabKitchenBLL.Services
{
    public class StartUpcheckDailyService : EntityService<tStartUpcheckDaily>, IStartUpcheckDailyService
    {
        #region Data Members
        // ReSharper disable once NotAccessedField.Local
        IUnitOfWork _unitOfWork;
        IStartUpcheckDailyRepository _startUpcheckDailyRepository;
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="unitOfWork"></param>
        /// <param name="startUpcheckDailyRepository"></param>
        public StartUpcheckDailyService(IUnitOfWork unitOfWork, IStartUpcheckDailyRepository startUpcheckDailyRepository)
            : base(unitOfWork, startUpcheckDailyRepository)
        {
            _unitOfWork = unitOfWork;
            _startUpcheckDailyRepository = startUpcheckDailyRepository;
        }
        #endregion

        #region Get By Id
        /// <summary>
        /// Get By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public tStartUpcheckDaily GetById(long id)
        {
            return _startUpcheckDailyRepository.GetById(id);
        }
        #endregion

        #region Get Startup Checklist - LR Cooking Room History
        /// <summary>
        /// Get Startup Checklist - LR Cooking Room History
        /// </summary>
        /// <param name="startDateTime"></param>
        /// <param name="endDateTime"></param>
        /// <returns></returns>
        public IEnumerable<tStartUpcheckDaily> GetStartupChecklistHistory(DateTime startDateTime, DateTime endDateTime)
        {
            return _startUpcheckDailyRepository.GetStartupChecklistHistory(startDateTime, endDateTime);
        }
        #endregion

        #region Get Startup Checklist History By Id
        /// <summary>
        /// Get Startup Checklist History By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public IEnumerable<tStartUpcheckDaily> GetStartupChecklistHistoryById(long id)
        {
            return _startUpcheckDailyRepository.GetStartupChecklistHistoryById(id);
        }
        #endregion
    }
}