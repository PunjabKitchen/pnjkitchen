﻿using PunjabKitchenBLL.Common;
using PunjabKitchenDAL.Interfaces;
using PunjabKitchenBLL.Interfaces;
using PunjabKitchenEntities.EntityModel;
using PunjabKitchenEntities.Interfaces;

namespace PunjabKitchenBLL.Services
{
    public class AreaService : EntityService<tArea>, IAreaService
    {
        #region Data Members
        // ReSharper disable once NotAccessedField.Local
        IUnitOfWork _unitOfWork;
        readonly IAreaRepository _areaRepository;
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="unitOfWork"></param>
        /// <param name="areaRepository"></param>
        public AreaService(IUnitOfWork unitOfWork, IAreaRepository areaRepository)
            : base(unitOfWork, areaRepository)
        {
            _unitOfWork = unitOfWork;
            _areaRepository = areaRepository;
        }
        #endregion

        #region Get By Id
        /// <summary>
        /// Get By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public tArea GetById(long id)
        {
            return _areaRepository.GetById(id);
        }
        #endregion
    }
}