﻿using PunjabKitchenBLL.Common;
using PunjabKitchenDAL.Interfaces;
using PunjabKitchenBLL.Interfaces;
using PunjabKitchenEntities.EntityModel;
using PunjabKitchenEntities.Interfaces;
using System;
using System.Linq;

namespace PunjabKitchenBLL.Services
{
    public class GoodsOutPlanService : EntityService<tGoodsOutPlan>, IGoodsOutPlanService
    {
        #region Data Members
        // ReSharper disable once NotAccessedField.Local
        // ReSharper disable once ArrangeTypeMemberModifiers
        IUnitOfWork _unitOfWork;
        // ReSharper disable once ArrangeTypeMemberModifiers
        readonly IGoodsOutPlanRepository _goodsOutPlanRepository;
        // ReSharper disable once ArrangeTypeMemberModifiers
        readonly IGoodsOutPlanDetailRepository _goodsOutPlanDetailRepository;
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="unitOfWork"></param>
        /// <param name="goodsOutPlanRepository"></param>
        /// <param name="goodsOutPlanDetailRepository"></param>
        public GoodsOutPlanService(IUnitOfWork unitOfWork, IGoodsOutPlanRepository goodsOutPlanRepository, IGoodsOutPlanDetailRepository goodsOutPlanDetailRepository)
            : base(unitOfWork, goodsOutPlanRepository)
        {
            _unitOfWork = unitOfWork;
            _goodsOutPlanRepository = goodsOutPlanRepository;
            _goodsOutPlanDetailRepository = goodsOutPlanDetailRepository;
        }
        #endregion

        #region Get By Id
        /// <summary>
        /// Get By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public tGoodsOutPlan GetById(long id)
        {
            return _goodsOutPlanRepository.GetById(id);
        }
        #endregion

        #region Get By Category And Date
        /// <summary>
        /// Get By Category And Date
        /// </summary>
        /// <param name="category"></param>
        /// <param name="date"></param>
        /// <returns></returns>

        public tGoodsOutPlan GetByCategoryAndDate(string category, DateTime date)
        {
            return _goodsOutPlanRepository.GetByCategoryAndDate(category, date);
        }
        #endregion

        #region Get Batch Code
        /// <summary>
        /// Get Batch Code
        /// </summary>
        /// <returns></returns>
        public string GetBatchCode()
        {
            return _goodsOutPlanRepository.GetBatchCode();
        }
        #endregion

        #region Validate Batch Code
        /// <summary>
        /// Validate Batch Code
        /// </summary>
        /// <param name="batchCode"></param>
        /// <returns></returns>
        public bool ValidateBatchCode(string batchCode)
        {
            return _goodsOutPlanRepository.ValidateBatchCode(batchCode);
        }
        #endregion

        #region Add/Edit Record
        /// <summary>
        /// Add/Edit Record
        /// </summary>
        /// <param name="record"></param>
        /// <returns></returns>

        public tGoodsOutPlan UpdateRecord(tGoodsOutPlan record)
        {
            tGoodsOutPlan updatedRecord = null;

            try
            {
                // ReSharper disable once PossibleNullReferenceException
                record.tGoodsOutPlanDetails.FirstOrDefault().tProduct = null;
                // ReSharper disable once PossibleNullReferenceException
                record.tGoodsOutPlanDetails.FirstOrDefault().iGoodsOutPlanId = record.iGoodsOutPlanId;

                _goodsOutPlanRepository.Edit(record);
                _goodsOutPlanRepository.Save();

                _goodsOutPlanDetailRepository.Edit(record.tGoodsOutPlanDetails.FirstOrDefault());
                _goodsOutPlanDetailRepository.Save();
                // ReSharper disable once ExpressionIsAlwaysNull
                return updatedRecord;
            }
            catch (Exception ex)
            {
                // ReSharper disable once PossibleIntendedRethrow
                throw ex;
            }
        }
        #endregion

        #region Create Record
        /// <summary>
        ///Create Record
        /// </summary>
        /// <param name="record"></param>
        /// <returns></returns>
        public tGoodsOutPlan CreateRecord(tGoodsOutPlan record)
        {
            // ReSharper disable once RedundantAssignment
            tGoodsOutPlan savedRecord = null;

            try
            {
                var existingBatch = _goodsOutPlanRepository.GetByCategoryAndDate(record.vCategory, Convert.ToDateTime(record.dDate));
                if (existingBatch != null)
                {
                    var detailRecord = record.tGoodsOutPlanDetails.FirstOrDefault();
                    // ReSharper disable once PossibleNullReferenceException
                    detailRecord.tProduct = null;
                    detailRecord.iGoodsOutPlanId = existingBatch.iGoodsOutPlanId;
                    _goodsOutPlanDetailRepository.Add(detailRecord);
                    _goodsOutPlanDetailRepository.Save();
                    savedRecord = existingBatch;
                }
                else
                {
                    // ReSharper disable once PossibleNullReferenceException
                    record.tGoodsOutPlanDetails.FirstOrDefault().tProduct = null;
                    savedRecord = _goodsOutPlanRepository.Add(record);
                    _goodsOutPlanRepository.Save();
                }
            }
            catch (Exception ex)
            {
                // ReSharper disable once PossibleIntendedRethrow
                throw ex;
            }

            return savedRecord;
        }
        #endregion
    }
}
