﻿using PunjabKitchenBLL.Common;
using PunjabKitchenDAL.Interfaces;
using PunjabKitchenBLL.Interfaces;
using PunjabKitchenEntities.EntityModel;
using PunjabKitchenEntities.Interfaces;

namespace PunjabKitchenBLL.Services
{
    public class HotPackingRercordService : EntityService<tHotPackingRercord>, IHotPackingRercordService
    {
        #region Data Members
        readonly IHotPackingRercordRepository _hotPackingRercordRepository;
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="unitOfWork"></param>
        /// <param name="hotPackingRercordRepository"></param>
        public HotPackingRercordService(IUnitOfWork unitOfWork, IHotPackingRercordRepository hotPackingRercordRepository)
            : base(unitOfWork, hotPackingRercordRepository)
        {
            _hotPackingRercordRepository = hotPackingRercordRepository;
        }
        #endregion

        #region Get By Id
        /// <summary>
        /// Get By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public tHotPackingRercord GetById(long id)
        {
            return _hotPackingRercordRepository.GetById(id);
        }
        #endregion
    }
}