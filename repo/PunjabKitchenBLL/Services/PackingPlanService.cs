﻿using System;
using PunjabKitchenBLL.Common;
using PunjabKitchenDAL.Interfaces;
using PunjabKitchenBLL.Interfaces;
using PunjabKitchenEntities.EntityModel;
using PunjabKitchenEntities.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace PunjabKitchenBLL.Services
{
    public class PackingPlanService : EntityService<tPackingProductionPlan>, IPackingPlanService
    {
        #region Data Members
        readonly IPackingPlanRepository _packingPlanRepository;
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="unitOfWork"></param>
        /// <param name="packingPlanRepository"></param>
        public PackingPlanService(IUnitOfWork unitOfWork, IPackingPlanRepository packingPlanRepository)
            : base(unitOfWork, packingPlanRepository)
        {
            _packingPlanRepository = packingPlanRepository;
        }
        #endregion

        #region Get By Id
        /// <summary>
        /// Get By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public tPackingProductionPlan GetById(long id)
        {
            return _packingPlanRepository.GetById(id);
        }
        #endregion

        #region Get Packing Plans By LineId
        /// <summary>
        /// Get Packing Plans By LineId
        /// </summary>
        /// <param name="LineId"></param>
        /// <returns></returns>
        // ReSharper disable once InconsistentNaming
        public List<tPackingProductionPlan> GetPackingPlansByLineId(int LineId)
        {
            return _packingPlanRepository.GetPackingPlansByLineId(LineId);
        }
        #endregion

        #region Create Packing Plan
        /// <summary>
        /// Create Packing Plan
        /// </summary>
        /// <param name="packingPlan"></param>
        public tPackingProductionPlan CreatePackingPlan(tPackingProductionPlan packingPlan)
        {
            var result = _packingPlanRepository.Add(packingPlan);
            _packingPlanRepository.Save();
            return result;
        }
        #endregion

        #region Update Packing Plan
        /// <summary>
        ///Update Packing Plan
        /// </summary>
        /// <param name="packingPlan"></param>
        public tPackingProductionPlan UpdatePackingPlan(tPackingProductionPlan packingPlan)
        {
            _packingPlanRepository.Edit(packingPlan);
            return packingPlan;
        }
        #endregion

        #region Get By Code
        /// <summary>
        /// Get By Code
        /// </summary>
        /// <param name="code"></param>
        public IEnumerable<tPackingProductionPlan> GetByCode(string code)
        {
            return _packingPlanRepository.GetByCode(code);
        }
        #endregion

        #region Get Packing Plans By LineId
        /// <summary>
        /// Get Packing Plans By LineId
        /// </summary>
        /// <param name="isLine1"></param>
        /// <param name="isLine2"></param>
        /// <param name="isLine3"></param>
        public List<tPackingProductionPlan> GetPackingPlansByLineId(bool isLine1, bool isLine2, bool isLine3)
        {
            return _packingPlanRepository.GetPackingPlansByLineId(isLine1, isLine2, isLine3);
        }
        #endregion

        #region Get Batch Code Unique
        /// <summary>
        /// Get Batch Code Unique
        /// </summary>
        /// <returns></returns>
        public string GetBatchCode()
        {
            return _packingPlanRepository.GetBatchCode();
        }
        #endregion

        #region Validate Code
        /// <summary>
        /// Validate Code
        /// </summary>
        /// <returns></returns>
        public bool ValidateCode(string code)
        {
            return _packingPlanRepository.ValidateCode(code);
        }
        #endregion

        #region Get Today's Time Slot Availability
        /// <summary>
        /// Get Today's Time Slot Availability
        /// </summary>
        /// <param name="iLineNo"></param>
        /// <param name="startDateTime"></param>
        /// <param name="finishDateTime"></param>
        /// <returns></returns>
        public string GetTodaysTimeSlotAvailability(int iLineNo, DateTime startDateTime, DateTime finishDateTime)
        {
            var todaysDate = DateTime.Now;
            // ReSharper disable once PossibleInvalidOperationException
            var packinggRecords = _packingPlanRepository.GetAll().Where(m => (m.iLineNo == iLineNo) && (m.dPackingPlan.Value.ToString("d/M/yyyy") == todaysDate.ToString("d/M/yyyy"))).ToList();

            if (packinggRecords.Count != 0)
            {
                // ReSharper disable once LoopCanBeConvertedToQuery
                foreach (var record in packinggRecords)
                {
                    if (((startDateTime >= record.tPlannedStart) && (startDateTime <= record.tPlannedFinish)) || ((finishDateTime >= record.tPlannedStart) && (finishDateTime <= record.tPlannedFinish)))
                    {
                        return "Time slot already filled.";
                    }
                }
            }

            return "Time slot available.";
        }
        #endregion

        #region Check Finishing Sequence
        /// <summary>
        /// Check Finishing Sequence
        /// </summary>
        /// <param name="iPackingProductionPlanId"></param>
        public string CheckFinishingSequence(int iPackingProductionPlanId)
        {
            var todaysDate = DateTime.Now;
            // ReSharper disable once PossibleInvalidOperationException
            var packingRecord = _packingPlanRepository.GetAll().FirstOrDefault(m => (m.tActualStart == null) 
                                                                                    && (m.tActualFinish == null));

            //var packingRecord = _packingPlanRepository.GetAll().FirstOrDefault(m => (m.tActualStart == null)
            //                                                                        && (m.tActualFinish == null)
            //                                                                        && (m.dPackingPlan.Value.ToString("d/M/yyyy") == todaysDate.ToString("d/M/yyyy")));

            // ReSharper disable once PossibleNullReferenceException
            // ReSharper disable once ConvertIfStatementToReturnStatement
            if (packingRecord.iPackingProductionPlanId == iPackingProductionPlanId)
            {
                return "Valid Sequence";
            }

            return "Please finish the plans in sequence.";
        }
        #endregion
    }
}