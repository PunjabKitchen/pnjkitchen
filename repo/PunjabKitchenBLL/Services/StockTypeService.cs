﻿using PunjabKitchenBLL.Common;
using PunjabKitchenDAL.Interfaces;
using PunjabKitchenBLL.Interfaces;
using PunjabKitchenEntities.EntityModel;
using PunjabKitchenEntities.Interfaces;

namespace PunjabKitchenBLL.Services
{
    public class StockTypeService : EntityService<tStockType>, IStockTypeService
    {
        #region Data Members
        // ReSharper disable once NotAccessedField.Local
        IUnitOfWork _unitOfWork;
        // ReSharper disable once NotAccessedField.Local
        IStockTypeRepository _stockTypeRepository;
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="unitOfWork"></param>
        /// <param name="stockTypeRepository"></param>
        public StockTypeService(IUnitOfWork unitOfWork, IStockTypeRepository stockTypeRepository)
            : base(unitOfWork, stockTypeRepository)
        {
            _unitOfWork = unitOfWork;
            _stockTypeRepository = stockTypeRepository;
        }
        #endregion
    }
}