﻿using PunjabKitchenDAL.Interfaces;
using PunjabKitchenBLL.Interfaces;
using System.Collections.Generic;
using PunjabKitchenBLL.Common;
using PunjabKitchenEntities.EntityModel;
using PunjabKitchenEntities.Interfaces;

namespace PunjabKitchenBLL.Services
{
    public class StockService : EntityService<tStock>, IStockService
    {
        #region Data Members
        // ReSharper disable once NotAccessedField.Local
        IUnitOfWork _unitOfWork;
        IStockRepository _stockRepository;
        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="unitOfWork"></param>
        /// <param name="stockRepository"></param>
        public StockService(IUnitOfWork unitOfWork, IStockRepository stockRepository)
            : base(unitOfWork, stockRepository)
        {
            _unitOfWork = unitOfWork;
            _stockRepository = stockRepository;
        }

        #endregion

        #region

        /// <summary>
        /// Get By ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public tStock GetById(long id)
        {
            return _stockRepository.GetById(id);
        }

        #endregion

        #region Get All Distinct Stock Locations

        /// <summary>
        /// Get All Distinct Stock Locations
        /// </summary>
        /// <returns></returns>
        public List<tStock> GetAllDistinctStockLocations()
        {
            return _stockRepository.GetAllDistinctStockLocations();
        }

        #endregion

        #region Get All Distinct Stock Points

        /// <summary>
        /// Get All Distinct Stock Points
        /// </summary>
        /// <param name="stockLoc"></param>
        /// <returns></returns>
        public List<tStock> GetAllDistinctStockPoints(string stockLoc)
        {
            return _stockRepository.GetAllDistinctStockPoints(stockLoc);
        }

        #endregion

        #region Validate Stock Location and Stock Point
        /// <summary>
        /// Validate Stock Location and Stock Point To Avoid Duplication
        /// </summary>
        /// <param name="stock"></param>
        /// <returns></returns>
        public bool validateStockPoint(tStock stock)
        {
            return _stockRepository.validateStockPoint(stock);
        }
        #endregion

        #region Get Allowed Stock Quanity
        /// <summary>
        /// Get Allowed Stock Quanity
        /// </summary>
        /// <param name="productCode"></param>
        /// <returns></returns>
        public int GetAllowedStockQuanity(string productCode)
        {
            return _stockRepository.GetAllowedStockQuanity(productCode);
        }
        #endregion
    }
}