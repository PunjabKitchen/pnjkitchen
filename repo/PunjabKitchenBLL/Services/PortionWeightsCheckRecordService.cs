﻿using System;
using Audit.Core;
using PunjabKitchenBLL.Common;
using PunjabKitchenBLL.Interfaces;
using PunjabKitchenDAL.Interfaces;
using PunjabKitchenEntities.EntityModel;
using PunjabKitchenEntities.Interfaces;

namespace PunjabKitchenBLL.Services
{
    public class PortionWeightsCheckRecordService : EntityService<tPortionWeightCheck>, IPortionWeightsCheckRecordService
    {
        #region Data Members
        readonly IPortionWeightsCheckRecordRepository _portionCheckRecordRepository;
        readonly IPortionWeightsCheckRecordDetailRepository _portionCheckRecordDetailRepository;
        #endregion

        #region Constructor
        /// <summary>
        /// 
        /// </summary>
        /// <param name="unitOfWork"></param>
        /// <param name="portionCheckRecordRepository"></param>
        /// <param name="portionCheckRecordDetailRepository"></param>
        public PortionWeightsCheckRecordService(IUnitOfWork unitOfWork, IPortionWeightsCheckRecordRepository portionCheckRecordRepository, IPortionWeightsCheckRecordDetailRepository portionCheckRecordDetailRepository)
            : base(unitOfWork, portionCheckRecordRepository)
        {
            _portionCheckRecordRepository = portionCheckRecordRepository;
            _portionCheckRecordDetailRepository = portionCheckRecordDetailRepository;
        }
        #endregion

        #region Get By Id
        /// <summary>
        /// Get By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public tPortionWeightCheck GetById(long id)
        {
            return _portionCheckRecordRepository.GetById(id);
        }
        #endregion

        #region Create Record with its Detail
        /// <summary>
        /// Create Record with its Detail
        /// </summary>
        /// <param name="record"></param>
        /// <param name="detailRecord"></param>
        /// <returns></returns>
        public tPortionWeightCheck CreateRecord(tPortionWeightCheck record, tPortionWeightCheckDetail detailRecord)
        {
            try
            {
                var savedRecord = record;

                //For addition in Parent's table only for the first entry.
                if (!(record.iPortionWeightCheckId > 0))
                {
                    savedRecord = _portionCheckRecordRepository.Add(record);
                }

                detailRecord.iPortionWeightCheckId = record.iPortionWeightCheckId;
                detailRecord.Sys_CreatedBy = record.Sys_CreatedBy;
                detailRecord.Sys_CreatedDateTime = record.Sys_CreatedDateTime;
                _portionCheckRecordDetailRepository.Add(detailRecord);
                _portionCheckRecordDetailRepository.Save();

                return savedRecord;
            }
            catch (Exception ex)
            {
                AuditScope.CreateAndSave("Error saving record: ", new { Exception = ex.Message });
                return null;
            }
        }
        #endregion
    }
}
