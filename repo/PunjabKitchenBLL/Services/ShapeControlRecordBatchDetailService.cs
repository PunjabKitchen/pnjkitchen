﻿using PunjabKitchenBLL.Common;
using PunjabKitchenBLL.Interfaces;
using PunjabKitchenDAL.Interfaces;
using PunjabKitchenEntities.EntityModel;
using PunjabKitchenEntities.Interfaces;

namespace PunjabKitchenBLL.Services
{
    public class ShapesControlRecordBatchDetailService : EntityService<tShapesControlRecordBatchDetail>, IShapesControlRecordBatchDetailService
    {
        #region Data Members
        readonly IShapesControlRecordBatchDetailRepository _shapesControlRecordBatchDetailRepository;
        #endregion

        #region Constructor
        /// <summary>
        /// 
        /// </summary>
        /// <param name="unitOfWork"></param>
        /// <param name="shapesControlRecordBatchDetailRepository"></param>
        public ShapesControlRecordBatchDetailService(IUnitOfWork unitOfWork, IShapesControlRecordBatchDetailRepository shapesControlRecordBatchDetailRepository)
            : base(unitOfWork, shapesControlRecordBatchDetailRepository)
        {
            _shapesControlRecordBatchDetailRepository = shapesControlRecordBatchDetailRepository;
        }
        #endregion

        #region Get By Id
        /// <summary>
        /// Get By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public tShapesControlRecordBatchDetail GetById(long id)
        {
            return _shapesControlRecordBatchDetailRepository.GetById(id);
        }
        #endregion
    }
}