﻿using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchenBLL.Interfaces
{
    public interface IPackingRecord2DetailService : IEntityService<tPackingRecord2Detail>
    {
        #region Functions Declarations
        tPackingRecord2Detail GetById(long id);
        #endregion
    }
}