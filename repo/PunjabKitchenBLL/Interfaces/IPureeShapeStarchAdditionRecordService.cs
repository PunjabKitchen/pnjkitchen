﻿using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchenBLL.Interfaces
{
    public interface IPureeShapesStarchAdditionRecordService : IEntityService<tPureeShapesStarchAdditionRecord>
    {
        #region Functions Declarations
        tPureeShapesStarchAdditionRecord GetById(long id);
        #endregion
    }
}