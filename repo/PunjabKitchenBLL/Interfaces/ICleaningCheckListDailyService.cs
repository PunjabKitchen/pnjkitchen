﻿using System;
using System.Collections.Generic;
using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchenBLL.Interfaces
{
    public interface ICleaningCheckListDailyService : IEntityService<CleaningChecklistDaily>
    {
        #region Functions Declarations
        CleaningChecklistDaily GetById(long id);
        IEnumerable<CleaningChecklistDaily> GetDistinctDailyCheck(DateTime currentDateTime);
        IEnumerable<CleaningChecklistDaily> GetDailyCleaningChecklistHistory(DateTime startDateTime, DateTime endDateTime);
        IEnumerable<CleaningChecklistDaily> GetDailyCleaningChecklistHistoryById(long id, long areaId);
        #endregion
    }
}
