﻿using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchenBLL.Interfaces
{
    public interface IMeatMincingRecordService : IEntityService<tMeatMincingRecord>
    {
        #region Functions Declarations
        tMeatMincingRecord GetById(long id);
        #endregion
    }
}