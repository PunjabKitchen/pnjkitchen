﻿using PunjabKitchenEntities.EntityModel;
using System.Collections.Generic;

namespace PunjabKitchenBLL.Interfaces
{
    public interface IShapesControlRecordService : IEntityService<tShapeControlRecord>
    {
        #region Functions Declarations
        tShapeControlRecord GetById(long id);
        int CreateRecord(tShapeControlRecord record, List<string> usersList, List<string> batchList);
        void CreateBatchRecord(tShapeControlRecord record, string batchCode);
        void CreatePackerRecord(tShapeControlRecord record, string packerId);
        #endregion
    }
}