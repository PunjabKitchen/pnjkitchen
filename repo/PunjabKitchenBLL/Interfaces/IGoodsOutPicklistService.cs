﻿using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchenBLL.Interfaces
{
    public interface IGoodsOutPicklistService : IEntityService<tGoodsOutPicklist>
    {
        #region Functions Declarations
        tGoodsOutPicklist GetById(long id);
        #endregion
    }
}