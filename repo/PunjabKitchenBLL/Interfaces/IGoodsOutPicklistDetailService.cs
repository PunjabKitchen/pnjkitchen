﻿using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchenBLL.Interfaces
{
    public interface IGoodsOutPicklistDetailService : IEntityService<tGoodsOutPicklistDetail>
    {
        #region Functions Declarations
        tGoodsOutPicklistDetail GetById(long id);
        #endregion
    }
}