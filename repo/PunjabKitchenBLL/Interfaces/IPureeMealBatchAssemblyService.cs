﻿using System.Collections.Generic;
using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchenBLL.Interfaces
{
    public interface IPureeMealBatchAssemblyService : IEntityService<tPureeMeanBatchAssembly>
    {
        #region Functions Declarations
        tPureeMeanBatchAssembly GetById(long id);
        int CreateRecord(tPureeMeanBatchAssembly record, List<string> usersList);
        void CreatePackerRecord(tPureeMeanBatchAssembly record, string packersList);
        #endregion
    }
}