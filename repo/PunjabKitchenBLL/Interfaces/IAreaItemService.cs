﻿using System.Collections.Generic;
using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchenBLL.Interfaces
{
    public interface IAreaItemService : IEntityService<tAreaItem>
    {
        #region Functions Declarations
        tAreaItem GetById(long id);
        List<tAreaItem> GetItemByAreaId(long id);
        #endregion
    }
}
