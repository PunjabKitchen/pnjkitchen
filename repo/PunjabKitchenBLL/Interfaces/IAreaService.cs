﻿using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchenBLL.Interfaces
{
    public interface IAreaService : IEntityService<tArea>
    {
        #region Functions Declarations
        tArea GetById(long id);
        #endregion
    }
}
