﻿using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchenBLL.Interfaces
{
    public interface IStartupCheckListService : IEntityService<tStartUpCheckList>
    {
        #region Functions Declarations
        tStartUpCheckList GetById(long id);
        #endregion
    }
}