﻿using System.Collections.Generic;
using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchenBLL.Interfaces
{
    public interface IStockService : IEntityService<tStock>
    {
        #region Functions Declarations
        tStock GetById(long id);
        List<tStock> GetAllDistinctStockLocations();
        List<tStock> GetAllDistinctStockPoints(string stockLoc);
        // ReSharper disable once InconsistentNaming
        bool validateStockPoint(tStock stock);
        int GetAllowedStockQuanity(string productCode);
        #endregion
    }
}