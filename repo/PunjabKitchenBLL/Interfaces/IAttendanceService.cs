﻿using System;
using System.Collections.Generic;
using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchenBLL.Interfaces
{
    public interface IAttendanceService : IEntityService<tEmployeeAttendance>
    {
        #region Functions Declarations
        tEmployeeAttendance GetById(string id);
        IEnumerable<tEmployeeAttendance> GetAllRecordsById(string id);
        tEmployeeAttendance GetTodaysRecordByUserId(string id, DateTime dateTime);
        // ReSharper disable once InconsistentNaming
        tEmployeeAttendance GetRecordForEditing(string iEmployeeId, DateTime Sys_CreatedDateTime);

        #endregion
    }
}
