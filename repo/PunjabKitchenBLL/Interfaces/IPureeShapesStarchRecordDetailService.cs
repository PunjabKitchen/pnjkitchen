﻿using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchenBLL.Interfaces
{
    public interface IPureeShapesStarchRecordDetailService : IEntityService<tPureeShapesStarchRecordDetail>
    {
        #region Functions Declarations
        tPureeShapesStarchRecordDetail GetById(long id);
        #endregion
    }
}