﻿using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchenBLL.Interfaces
{
    public interface IPackingRecordDetailService : IEntityService<tPackingRecordDetail>
    {
        #region Functions Declarations
        tPackingRecordDetail GetById(long id);
        #endregion
    }
}