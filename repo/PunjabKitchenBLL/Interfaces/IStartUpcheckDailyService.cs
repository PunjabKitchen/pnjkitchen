﻿using System;
using System.Collections.Generic;
using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchenBLL.Interfaces
{
    public interface IStartUpcheckDailyService : IEntityService<tStartUpcheckDaily>
    {
        #region Functions Declarations
        tStartUpcheckDaily GetById(long id);
        IEnumerable<tStartUpcheckDaily> GetStartupChecklistHistory(DateTime startDateTime, DateTime endDateTime);
        IEnumerable<tStartUpcheckDaily> GetStartupChecklistHistoryById(long id);
        #endregion
    }
}