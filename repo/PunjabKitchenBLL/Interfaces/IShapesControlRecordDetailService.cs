﻿using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchenBLL.Interfaces
{
    public interface IShapesControlRecordDetailService : IEntityService<tShapeControlRecordDetail>
    {
        #region Functions Declarations
        tShapeControlRecordDetail GetById(long id);
        #endregion
    }
}