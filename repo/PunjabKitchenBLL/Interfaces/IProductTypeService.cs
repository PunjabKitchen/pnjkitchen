﻿using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchenBLL.Interfaces
{
    public interface IProductTypeService : IEntityService<tProductType>
    {
        #region Functions Declarations
        tProductType GetById(long id);
        #endregion
    }
}