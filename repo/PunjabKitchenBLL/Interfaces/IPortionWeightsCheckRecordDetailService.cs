﻿using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchenBLL.Interfaces
{
    public interface IPortionWeightsCheckRecordDetailService : IEntityService<tPortionWeightCheckDetail>
    {
        #region Functions Declarations
        tPortionWeightCheckDetail GetById(long id);
        #endregion
    }
}