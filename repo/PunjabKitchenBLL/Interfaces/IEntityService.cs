﻿using System.Collections.Generic;
using PunjabKitchenEntities.Common;
using System.Linq.Expressions;
using System;

namespace PunjabKitchenBLL.Interfaces
{
    public interface IEntityService<T> : IService
     where T : BaseEntity
    {
        #region Functions Declarations
        T Create(T entity);
        void Delete(T entity);
        IEnumerable<T> GetAll();
        IEnumerable<T> FindBy(Expression<Func<T, bool>> predicate);
        void Update(T entity);
        #endregion
    }
}
