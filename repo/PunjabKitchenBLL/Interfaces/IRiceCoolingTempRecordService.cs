﻿using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchenBLL.Interfaces
{
    public interface IRiceCoolingTempRecordService : IEntityService<tRiceCoolingTempRecord>
    {
        #region Functions Declarations
        tRiceCoolingTempRecord GetById(long id);
        #endregion
    }
}