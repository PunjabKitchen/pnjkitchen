﻿using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchenBLL.Interfaces
{
    public interface IUnitService : IEntityService<tUnit>
    {
        #region Functions Declarations
        tUnit GetById(long id);
        #endregion
    }
}