﻿using System.Collections.Generic;
using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchenBLL.Interfaces
{
    public interface IPureeShapesStarchRecordService : IEntityService<tPureeShapesStarchRecord>
    {
        #region Functions Declarations
        tPureeShapesStarchRecord GetById(long id);
        int CreateRecord(tPureeShapesStarchRecord record, List<string> batchCodesList, tPureeShapesStarchAdditionRecord additionRecord);
        int DeleteAdditionItem(int id);
        void CreateBatchRecord(tPureeShapesStarchRecord record, string batchCode);
        #endregion
    }
}