﻿using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchenBLL.Interfaces
{
    public interface IMixedCaseBoxingRecordDetailService : IEntityService<tMixedCaseBoxingRecordDetail>
    {
        #region Functions Declarations
        tMixedCaseBoxingRecordDetail GetById(long id);
        #endregion
    }
}