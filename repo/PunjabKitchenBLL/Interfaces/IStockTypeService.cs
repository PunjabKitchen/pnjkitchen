﻿using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchenBLL.Interfaces
{
    public interface IStockTypeService : IEntityService<tStockType>
    {
    }
}