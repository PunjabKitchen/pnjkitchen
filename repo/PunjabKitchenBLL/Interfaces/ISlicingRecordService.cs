﻿using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchenBLL.Interfaces
{
    public interface ISlicingRecordService : IEntityService<tSlicingRecord>
    {
        #region Functions Declarations
        tSlicingRecord GetById(long id);
        #endregion
    }
}