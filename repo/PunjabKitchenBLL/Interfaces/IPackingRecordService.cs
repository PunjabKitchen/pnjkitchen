﻿using System.Collections.Generic;
using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchenBLL.Interfaces
{
    public interface IPackingRecordService : IEntityService<tPackingRecord>
    {
        #region Functions Declarations
        tPackingRecord GetById(long id);
        int CreateRecord(tPackingRecord record, List<string> usersList);
        void CreatePackerRecord(tPackingRecord record, string userId);
        #endregion
    }
}