﻿using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchenBLL.Interfaces
{
    public interface IPortionWeightsCheckRecordService : IEntityService<tPortionWeightCheck>
    {
        #region Functions Declarations
        tPortionWeightCheck GetById(long id);
        tPortionWeightCheck CreateRecord(tPortionWeightCheck record, tPortionWeightCheckDetail detailRecord);
        #endregion
    }
}