﻿using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchenBLL.Interfaces
{
    public interface IPureeMealInBlastService : IEntityService<tPureeMealInBlastFreezer>
    {
        #region Functions Declarations
        tPureeMealInBlastFreezer GetById(long id);
        int CreateRecord(tPureeMealInBlastFreezer record);
        int DeleteFreezerRecord(int iPureeMealInBlastFreezerId);
        #endregion
    }
}