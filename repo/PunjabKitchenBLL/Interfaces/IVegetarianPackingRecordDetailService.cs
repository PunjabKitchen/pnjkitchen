﻿using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchenBLL.Interfaces
{
    public interface IVegetarianPackingRecordDetailService : IEntityService<tVegetarianPackingRecordDetail>
    {
        #region Functions Declarations
        tVegetarianPackingRecordDetail GetById(long id);
        #endregion
    }
}