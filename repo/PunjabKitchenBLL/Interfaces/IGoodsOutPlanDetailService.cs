﻿using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchenBLL.Interfaces
{
    public interface IGoodsOutPlanDetailService : IEntityService<tGoodsOutPlanDetail>
    {
        #region Functions Declarations
        tGoodsOutPlanDetail GetById(long id);
        #endregion
    }
}