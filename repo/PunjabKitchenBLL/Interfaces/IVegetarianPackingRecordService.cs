﻿using System.Collections.Generic;
using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchenBLL.Interfaces
{
    public interface IVegetarianPackingRecordService : IEntityService<tVegetarianPackingRecord>
    {
        #region Functions Declarations
        tVegetarianPackingRecord GetById(long id);
        int CreateRecord(tVegetarianPackingRecord record, List<string> usersList);
        void CreatePackerRecord(tVegetarianPackingRecord record, string userId);
        #endregion
    }
}