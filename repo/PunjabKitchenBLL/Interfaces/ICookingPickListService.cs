﻿using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchenBLL.Interfaces
{
    public interface ICookingPickListService : IEntityService<tPickList>
    {
        #region Functions Declarations
        tPickList GetById(long id);
        #endregion
    }
}

