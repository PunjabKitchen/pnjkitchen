﻿using System;
using System.Collections.Generic;
using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchenBLL.Interfaces
{
    public interface IShoeCleaningCheckService : IEntityService<tShoeCleaningRecrdWeekly>
    {
        #region Functions Declarations
        tShoeCleaningRecrdWeekly GetById(long id);
        bool CheckWeeklyRecord(DateTime startDate, string userId);
        IEnumerable<tShoeCleaningRecrdWeekly> GetShoesCleaningHistory(DateTime startDateTime, DateTime endDateTime);
        bool IsCheckPerformed(DateTime startDateTime, DateTime endDateTime, string userId);
        tShoeCleaningRecrdWeekly GetShoeCleaningChecklistHistoryById(long id);
        #endregion
    }
}