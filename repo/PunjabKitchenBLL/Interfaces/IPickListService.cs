﻿using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchenBLL.Interfaces
{
    public interface IPickListService : IEntityService<tPickList>
    {
        #region Functions Declarations
        tPickList AddPickList(tPickList pickListItem);
        #endregion
    }
}