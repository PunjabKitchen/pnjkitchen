﻿using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchenBLL.Interfaces
{
    public interface IPureeMealBatchAssemblyDetailService : IEntityService<tPureeMealBatchAssemblyDetail>
    {
        #region Functions Declarations
        tPureeMealBatchAssemblyDetail GetById(long id);
        #endregion
    }
}