﻿using System;
using System.Collections.Generic;
using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchenBLL.Interfaces
{
    public interface INotificationService : IEntityService<tNotification>
    {
        #region Functions Declarations
        tNotification GetById(long id);
        int GetNotificationsCount();
        bool CanAssignNotificationToUser(int notificationId);
        IEnumerable<tNotification> GetNotificationByDate(DateTime dateTime);
        // ReSharper disable once InconsistentNaming
        tNotification GetLatestNotificationByType(int notificationTypeID);
        #endregion
    }
}