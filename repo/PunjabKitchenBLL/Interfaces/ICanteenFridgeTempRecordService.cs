﻿using System;
using System.Collections.Generic;
using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchenBLL.Interfaces
{
    public interface ICanteenFridgeTempRecordService : IEntityService<tCanteenFridgeTempRecordDaily>
    {
        #region Functions Declarations
        tCanteenFridgeTempRecordDaily GetById(long id);
        IEnumerable<tCanteenFridgeTempRecordDaily> GetCanteenFridgeTemperatureHistory(DateTime startDateTime, DateTime endDateTime);
        tCanteenFridgeTempRecordDaily GetCanteenFridgeChecklistHistoryById(long id);
        #endregion
    }
}
