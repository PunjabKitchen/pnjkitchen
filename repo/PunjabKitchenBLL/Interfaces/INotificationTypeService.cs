﻿using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchenBLL.Interfaces
{
    public interface INotificationTypeService : IEntityService<tNotificationType>
    {
    }
}
