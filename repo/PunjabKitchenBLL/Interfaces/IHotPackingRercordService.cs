﻿using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchenBLL.Interfaces
{
    public interface IHotPackingRercordService : IEntityService<tHotPackingRercord>
    {
        #region Functions Declarations
        tHotPackingRercord GetById(long id);
        #endregion
    }
}