﻿using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchenBLL.Interfaces
{
    public interface IGoodsOutPlanService : IEntityService<tGoodsOutPlan>
    {
        #region Functions Declarations
        tGoodsOutPlan GetById(long id);
        tGoodsOutPlan CreateRecord(tGoodsOutPlan record);
        tGoodsOutPlan UpdateRecord(tGoodsOutPlan record);
        string GetBatchCode();
        bool ValidateBatchCode(string batchCode);
        #endregion
    }
}