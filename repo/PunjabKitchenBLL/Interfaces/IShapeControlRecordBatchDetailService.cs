﻿using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchenBLL.Interfaces
{
    public interface IShapesControlRecordBatchDetailService : IEntityService<tShapesControlRecordBatchDetail>
    {
        #region Functions Declarations
        tShapesControlRecordBatchDetail GetById(long id);
        #endregion
    }
}