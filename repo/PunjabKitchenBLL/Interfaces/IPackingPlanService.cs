﻿using System;
using System.Collections.Generic;
using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchenBLL.Interfaces
{
    public interface IPackingPlanService : IEntityService<tPackingProductionPlan>
    {
        #region Functions Declarations
        List<tPackingProductionPlan> GetPackingPlansByLineId(int lineId);
        tPackingProductionPlan CreatePackingPlan(tPackingProductionPlan packingPlan);
        IEnumerable<tPackingProductionPlan> GetByCode(string code);
        List<tPackingProductionPlan> GetPackingPlansByLineId(bool isLine1, bool isLine2, bool isLine3);
        tPackingProductionPlan GetById(long id);
        string GetBatchCode();
        bool ValidateCode(string code);
        string GetTodaysTimeSlotAvailability(int iLineNo, DateTime startDateTime, DateTime finishDateTime);
        string CheckFinishingSequence(int iPackingProductionPlanId);
        #endregion
    }
}