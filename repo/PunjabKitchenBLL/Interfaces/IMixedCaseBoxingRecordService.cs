﻿using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchenBLL.Interfaces
{
    public interface IMixedCaseBoxingRecordService : IEntityService<tMixedCaseBoxingRecord>
    {
        #region Functions Declarations
        tMixedCaseBoxingRecord GetById(long id);
        #endregion
    }
}