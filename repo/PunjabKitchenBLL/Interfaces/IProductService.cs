﻿using System.Collections.Generic;
using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchenBLL.Interfaces
{
    public interface IProductService : IEntityService<tProduct>
    {
        #region Functions Declarations
        tProduct GetById(long id);
        IEnumerable<tProduct> GetByCode(string code);
        string GetProductCode(int productTypeId);
        bool ValidateCode(string code);
        #endregion
    }
}