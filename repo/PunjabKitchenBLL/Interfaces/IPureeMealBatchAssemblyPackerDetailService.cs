﻿using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchenBLL.Interfaces
{
    public interface IPureeMealBatchAssemblyPackerDetailService : IEntityService<tPureeMealBatchAssemblyPackerDetail>
    {
        #region Functions Declarations
        tPureeMealBatchAssemblyPackerDetail GetById(long id);
        #endregion
    }
}