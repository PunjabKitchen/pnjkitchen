﻿using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchenBLL.Interfaces
{
    public interface IBoxingRecordService : IEntityService<tBoxingRecord>
    {
        #region Functions Declarations
        tBoxingRecord GetById(long id);
        string GetBatchCode();
        bool ValidateBatchCode(string batchCode);
        #endregion
    }
}

