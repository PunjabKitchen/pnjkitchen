﻿using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchenBLL.Interfaces
{
    public interface IFreezerRecordService : IEntityService<tFreezerRecord>
    {
        #region Functions Declarations
        tFreezerRecord GetById(long id);
        #endregion
    }
}
