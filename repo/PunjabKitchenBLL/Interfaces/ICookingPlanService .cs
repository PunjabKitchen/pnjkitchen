﻿using System;
using System.Collections.Generic;
using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchenBLL.Interfaces
{
    public interface ICookingPlanService : IEntityService<tCookingPlan>
    {
        #region Functions Declarations
        tCookingPlan GetById(long id);
        List<tCookingPlan> GetCookingPlansByCookpotId(int cookPotId);
        tCookingPlan CreateCookingPlan(tCookingPlan cookingPlan);
        tCookingPlan UpdateCookingPlan(tCookingPlan cookingPlan);
        IEnumerable<tCookingPlan> GetByCode(string code);
        List<tCookingPlan> GetCookingPlansByCookpotId(bool isCookPot1, bool isCookPot2, bool isCookPot3);
        string GetBatchCode();
        bool ValidateBatchCode(string batchCode);
        string GetTodaysTimeSlotAvailability(int iCookPotNo, DateTime startDateTime, DateTime finishDateTime);
        string CheckFinishingSequence(int iCookingPlanId);
        #endregion
    }
}
