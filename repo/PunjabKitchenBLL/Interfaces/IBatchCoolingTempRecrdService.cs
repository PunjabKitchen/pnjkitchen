﻿using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchenBLL.Interfaces
{
    public interface IBatchCoolingTempRecrdService : IEntityService<tBatchCoolingTempRecrd>
    {
        #region Functions Declarations
        tBatchCoolingTempRecrd GetById(long id);
        #endregion
    }
}