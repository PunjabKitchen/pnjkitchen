﻿using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchenBLL.Interfaces
{
    public interface IBatchChangePureeRecordService : IEntityService<tBatchChangeChkPureeRecrd>
    {
        #region Functions Declarations
        tBatchChangeChkPureeRecrd GetById(long id);
        #endregion
    }
}
