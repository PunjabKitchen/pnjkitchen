﻿using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchenBLL.Interfaces
{
    public interface IGoodsInService : IEntityService<tGoodsIn>
    {
        #region Functions Declarations
        tGoodsIn GetById(long id);
        #endregion
    }
}
