﻿using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchenBLL.Interfaces
{
    // ReSharper disable once InconsistentNaming
    public interface IFMDetectionAndPackWeightRecordService : IEntityService<tFMDetectionAndPackWeightTest>
    {
        #region Functions Declarations
        tFMDetectionAndPackWeightTest GetById(long id);
        #endregion
    }
}
