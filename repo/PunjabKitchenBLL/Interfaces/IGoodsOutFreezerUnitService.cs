﻿using System;
using System.Collections.Generic;
using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchenBLL.Interfaces
{
    public interface IGoodsOutFreezerUnitService : IEntityService<tGoodsOutFreezerUnit>
    {
        #region Functions Declarations
        tGoodsOutFreezerUnit GetById(long id);
        IEnumerable<tGoodsOutFreezerUnit> GetGoodsOutFreezerUnitHistory(DateTime startDateTime, DateTime endDateTime);
        tGoodsOutFreezerUnit GetGoodsOutFreezerUnitHistoryById(long id);
        #endregion
    }
}
