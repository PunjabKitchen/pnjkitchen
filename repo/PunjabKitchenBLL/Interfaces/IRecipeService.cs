﻿using System.Collections.Generic;
using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchenBLL.Interfaces
{
    public interface IRecipeService : IEntityService<tRecipe>
    {
        #region Functions Declarations
        tRecipe GetById(long id);
        IEnumerable<tRecipe> GetByCode(string code);
        #endregion
    }
}