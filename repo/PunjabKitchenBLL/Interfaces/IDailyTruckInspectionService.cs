﻿using System;
using System.Collections.Generic;
using PunjabKitchenEntities.EntityModel;


namespace PunjabKitchenBLL.Interfaces
{
    public interface IDailyTruckInspectionService : IEntityService<tDailyTruckInspection>
    {
        #region Functions Declarations
        tDailyTruckInspection GetById(long id);
        IEnumerable<tDailyTruckInspection> GetDailyTruckInspectionHistory(DateTime startDateTime, DateTime endDateTime);
        tDailyTruckInspection GetDailyTruckInspectionHistoryById(long id);
        #endregion
    }
}
