﻿using System.Collections.Generic;
using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchenBLL.Interfaces
{
    public interface IPackingRecord2Service : IEntityService<tPackingRecord2>
    {
        #region Functions Declarations
        tPackingRecord2 GetById(long id);
        int CreateRecord(tPackingRecord2 record, List<string> usersList);
        void CreatePackerRecord(tPackingRecord2 record, string userId);
        #endregion
    }
}