﻿using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchenBLL.Interfaces
{
    public interface IColdDessertPackingRecordService : IEntityService<tColdDesertPackingRecord>
    {
        #region Functions Declarations
        tColdDesertPackingRecord GetById(long id);
        #endregion
    }
}
