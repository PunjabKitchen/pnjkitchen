﻿using System;
using System.Collections.Generic;
using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchenBLL.Interfaces
{
    public interface IStartupCheckEPGoodsOutService : IEntityService<tStartupChecksEPGoodsOut>
    {
        #region Functions Declarations
        tStartupChecksEPGoodsOut GetById(long id);
        IEnumerable<tStartupChecksEPGoodsOut> GetStartupChecksEpGoodsOutHistory(DateTime startDateTime, DateTime endDateTime);
        tStartupChecksEPGoodsOut GetStartupChecksEpGoodsOutHistoryById(long id);
        #endregion
    }
}
