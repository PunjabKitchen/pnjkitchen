﻿using Audit.SqlServer.Providers;
using Autofac;
using Autofac.Integration.Mvc;
using AutoMapper;
using PunjabKitchen.Models;
using System;
using System.Data.Entity;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchen
{
    // Note: For instructions on enabling IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=301868
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            initializeMapping();

            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            //Autofac Configuration
            var builder = new Autofac.ContainerBuilder();

            builder.RegisterControllers(typeof(MvcApplication).Assembly).PropertiesAutowired();

            builder.RegisterModule(new RepositoryModule());
            builder.RegisterModule(new ServiceModule());
            builder.RegisterModule(new EFModule());

            var container = builder.Build();

            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));

            #region Audit 
            //        Audit.Core.Configuration.Setup()
            //.UseFileLogProvider(config => config
            //    .DirectoryBuilder(_ => $@"C:\Logs\{DateTime.Now:yyyy-MM-dd}")
            //    .FilenameBuilder(auditEvent => $"{auditEvent.Environment.UserName}_{DateTime.Now.Ticks}.json"));

            var cs = System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            Audit.Core.Configuration.DataProvider = new SqlDataProvider()
            {
                ConnectionString =cs,
                Schema = "dbo",
                TableName = "Event",
                IdColumnName = "EventId",
                JsonColumnName = "Data",
                LastUpdatedDateColumnName = "LastUpdatedDate"
            };
            #endregion
        }

        private void initializeMapping()
        {
            Mapper.Initialize(cfg =>
            {
                #region Server to Client Mappings
                cfg.CreateMap<tStock, Stock>();
                cfg.CreateMap<tProduct, Product>();
                cfg.CreateMap<tStockType, StockType>();
                cfg.CreateMap<tNotification, Notification>();
                cfg.CreateMap<tGoodsIn, GoodsIn>();
                cfg.CreateMap<tProductType, ProductType>();
                cfg.CreateMap<tUnit, Unit>();
                cfg.CreateMap<tStock, StockLocationSelectList>();
                cfg.CreateMap<tStock, StockPointSelectList>();
                cfg.CreateMap <tStartUpCheckList, StartupCheckList>();
                cfg.CreateMap<tStartUpcheckDaily, StartupCheckDaily>();
                cfg.CreateMap<tCanteenFridgeTempRecordDaily, CanteenFridgeTempRecord>();
                cfg.CreateMap<tArea, Area>();
                cfg.CreateMap<tAreaItem, CleaningCheckListDaily>();
                cfg.CreateMap<CleaningChecklistDaily,CleaningCheckListDaily >();
                cfg.CreateMap<tShoeCleaningRecrdWeekly, ShoeCleaningCheck>();
                cfg.CreateMap<tCookingPlan, CookingPlan>();
                cfg.CreateMap<tFreezerRecord, FreezerRecord>();
                cfg.CreateMap<tArea, CleaningAreas>();
                cfg.CreateMap<tBatchCoolingTempRecrd, BatchCoolingTempRecrd>();
                cfg.CreateMap<tRecipe, Recipe>();
                cfg.CreateMap<tSlicingRecord, SlicingRecord>();
                cfg.CreateMap<tRiceCoolingTempRecord, RiceCoolingTempRecord>();
                cfg.CreateMap<tMeatMincingRecord, MeatMincingRecord>();
                cfg.CreateMap<tHotPackingRercord, HotPackingRercord>();
                cfg.CreateMap<tPackingProductionPlan, PackingProductionPlan>();
                cfg.CreateMap<tColdDesertPackingRecord, ColdDesertPackingRecord>();
                cfg.CreateMap<tPickList, PickList>();
                cfg.CreateMap<tPortionWeightCheck, PortionWeightCheck>();
                cfg.CreateMap<tShapeControlRecord, ShapesControlRecord>();
                cfg.CreateMap<tShapeControlRecordDetail, ShapesControlRecordDetail>();
                cfg.CreateMap<tShapesControlRecordBatchDetail, ShapesControlRecordBatchDetail>();
                cfg.CreateMap<tPackingRecord, PackingRecord>();
                cfg.CreateMap<tPackingRecord2, PackingRecord2>();
                cfg.CreateMap<tPackingRecord2Detail, PackingRecord2Detail>();
                cfg.CreateMap<tBatchChangeChkPureeRecrd, BatchChangeCheckPureeRecord>();
                cfg.CreateMap<tFMDetectionAndPackWeightTest, FMDetectionAndPackWeightTest>();
                cfg.CreateMap<tPortionWeightCheck, PortionWeightCheck>();
                cfg.CreateMap<tPortionWeightCheckDetail, PortionWeightCheckDetail>();
                cfg.CreateMap<tPureeShapesStarchRecord, PureeShapeStarchRecord>();
                cfg.CreateMap<tPureeShapesStarchRecordDetail, PureeShapesStarchRecordDetail>();
                cfg.CreateMap<tPureeShapesStarchAdditionRecord, PureeShapesStarchAdditionRecord>();
                cfg.CreateMap<tBoxingRecord, BoxingRecord>();
                cfg.CreateMap<tMixedCaseBoxingRecord, MixedCaseBoxingRecord>();
                cfg.CreateMap<tMixedCaseBoxingRecordDetail, MixedCaseBoxingRecordDetail>();
                cfg.CreateMap<tVegetarianPackingRecord, VegetarianPackingRecord>();
                cfg.CreateMap<tVegetarianPackingRecordDetail, VegetarianPackingRecordDetail>();
                cfg.CreateMap<tPureeMeanBatchAssembly, PureeMealBatchAssembly>();
                cfg.CreateMap<tPureeMealBatchAssemblyDetail, PureeMealBatchAssemblyDetail>();
                cfg.CreateMap<tPureeMealBatchAssemblyPackerDetail, PureeMealBatchAssemblyPackerDetail>();
                cfg.CreateMap<tPureeMealInBlastFreezer, PureeMealInBlastFreezer>();
                cfg.CreateMap<tGoodsOutPlan, GoodsOutPlan>();
                cfg.CreateMap<tGoodsOutPlanDetail, GoodsOutPlanDetail>();
                cfg.CreateMap<tGoodsOutPicklist, GoodsOutPicklist>();
                cfg.CreateMap<tGoodsOutPicklistDetail, GoodsOutPicklistDetail>();
                cfg.CreateMap<tEmployeeAttendance, EmployeeAttendance>();
                cfg.CreateMap<Event, EventModel>();
                cfg.CreateMap<tDailyTruckInspection, DailyTruckInspection>();
                cfg.CreateMap<tGoodsOutFreezerUnit, GoodsOutFreezerUnit>();
                cfg.CreateMap<tStartupChecksEPGoodsOut, StartupChecksEPGoodsOut>();

                #endregion

                #region Client to Server Mappings
                cfg.CreateMap<Stock, tStock>();
                cfg.CreateMap<Product, tProduct>();
                cfg.CreateMap<ProductType, tProductType>();
                cfg.CreateMap<StockType, tStockType>();
                cfg.CreateMap<Notification, tNotification>();
                cfg.CreateMap<GoodsIn, tGoodsIn>();
                cfg.CreateMap<Unit, tUnit>();
                cfg.CreateMap<StartupCheckList, tStartUpCheckList>();
                cfg.CreateMap<StartupCheckDaily, tStartUpcheckDaily>();
                cfg.CreateMap<CanteenFridgeTempRecord, tCanteenFridgeTempRecordDaily>();
                cfg.CreateMap<Area, tArea>();
                cfg.CreateMap<CleaningCheckListDaily, tAreaItem>();
                cfg.CreateMap<CleaningCheckListDaily, CleaningChecklistDaily>();
                cfg.CreateMap<ShoeCleaningCheck, tShoeCleaningRecrdWeekly>();
                cfg.CreateMap<CookingPlan, tCookingPlan>();
                cfg.CreateMap<FreezerRecord, tFreezerRecord>();
                cfg.CreateMap<SlicingRecord, tSlicingRecord>();
                cfg.CreateMap<RiceCoolingTempRecord, tRiceCoolingTempRecord>();
                cfg.CreateMap<MeatMincingRecord, tMeatMincingRecord>();
                cfg.CreateMap<Recipe, tRecipe>();
                cfg.CreateMap<BatchCoolingTempRecrd,tBatchCoolingTempRecrd>();
                cfg.CreateMap<PickList,tPickList>();
                cfg.CreateMap<PackingProductionPlan, tPackingProductionPlan>();
                cfg.CreateMap<HotPackingRercord, tHotPackingRercord>();
                cfg.CreateMap<ColdDesertPackingRecord, tColdDesertPackingRecord>();
                cfg.CreateMap<PortionWeightCheck, tPortionWeightCheck>();
                cfg.CreateMap<ShapesControlRecord, tShapeControlRecord>();
                cfg.CreateMap<ShapesControlRecordDetail, tShapeControlRecordDetail>();
                cfg.CreateMap<ShapesControlRecordBatchDetail, tShapesControlRecordBatchDetail>();
                cfg.CreateMap<PackingRecord, tPackingRecord>();
                cfg.CreateMap<PackingRecord2, tPackingRecord2>();
                cfg.CreateMap<PackingRecord2Detail, tPackingRecord2Detail>();
                cfg.CreateMap<BatchChangeCheckPureeRecord, tBatchChangeChkPureeRecrd>();
                cfg.CreateMap<FMDetectionAndPackWeightTest, tFMDetectionAndPackWeightTest>();
                cfg.CreateMap<PortionWeightCheck, tPortionWeightCheck>();
                cfg.CreateMap<PortionWeightCheckDetail, tPortionWeightCheckDetail>();
                cfg.CreateMap<PureeShapeStarchRecord, tPureeShapesStarchRecord>();
                cfg.CreateMap<PureeShapesStarchRecordDetail, tPureeShapesStarchRecordDetail>();
                cfg.CreateMap<PureeShapesStarchAdditionRecord, tPureeShapesStarchAdditionRecord>();
                cfg.CreateMap<BoxingRecord, tBoxingRecord>();
                cfg.CreateMap<MixedCaseBoxingRecord, tMixedCaseBoxingRecord>();
                cfg.CreateMap<MixedCaseBoxingRecordDetail, tMixedCaseBoxingRecordDetail>();
                cfg.CreateMap<VegetarianPackingRecord, tVegetarianPackingRecord>();
                cfg.CreateMap<VegetarianPackingRecordDetail, tVegetarianPackingRecordDetail>();
                cfg.CreateMap<PureeMealBatchAssembly, tPureeMeanBatchAssembly>();
                cfg.CreateMap<PureeMealBatchAssemblyDetail, tPureeMealBatchAssemblyDetail>();
                cfg.CreateMap<PureeMealBatchAssemblyPackerDetail, tPureeMealBatchAssemblyPackerDetail>();
                cfg.CreateMap<PureeMealInBlastFreezer, tPureeMealInBlastFreezer>();
                cfg.CreateMap<GoodsOutPlan, tGoodsOutPlan>();
                cfg.CreateMap<GoodsOutPlanDetail, tGoodsOutPlanDetail>();
                cfg.CreateMap<GoodsOutPicklist, tGoodsOutPicklist>();
                cfg.CreateMap<GoodsOutPicklistDetail, tGoodsOutPicklistDetail>();
                cfg.CreateMap<EmployeeAttendance, tEmployeeAttendance>();
                cfg.CreateMap<EventModel, Event>();
                cfg.CreateMap<DailyTruckInspection, tDailyTruckInspection>();
                cfg.CreateMap<GoodsOutFreezerUnit, tGoodsOutFreezerUnit>();
                cfg.CreateMap<StartupChecksEPGoodsOut, tStartupChecksEPGoodsOut>();
                #endregion
            });
        }
    }
}
