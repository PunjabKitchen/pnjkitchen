﻿using System.Data.Entity;
using Autofac;
using PunjabKitchenEntities;
using PunjabKitchenEntities.Common;
using PunjabKitchenEntities.Interfaces;

namespace PunjabKitchen
{
    public class EFModule : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterModule(new RepositoryModule());
            builder.RegisterType(typeof(PunjabKitchenContext)).As(typeof(DbContext)).InstancePerLifetimeScope();
            builder.RegisterType(typeof(UnitOfWork)).As(typeof(IUnitOfWork)).InstancePerRequest();         
        }
    }
}