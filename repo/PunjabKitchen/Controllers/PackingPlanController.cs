﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Audit.Core;
using AutoMapper;
using Microsoft.AspNet.Identity;
using PunjabKitchen.Models;
using PunjabKitchenBLL.Interfaces;
using PunjabKitchenEntities.EntityModel;
using System.Collections;
using PagedList;

namespace PunjabKitchen.Controllers
{
    [Authorize(Roles = "Admin,Packing  Staff,Packing Manager")]
    public partial class PackingPlanController : BaseController
    {
        #region Data Members
        readonly IPackingPlanService _packingPlanService;
        readonly ICookingPlanService _cookingPlanService;
        #endregion

        #region Contructor
        /// <summary>
        /// Contructor
        /// </summary>
        /// <param name="packingPlanService"></param>
        /// <param name="cookingPlanService"></param>
        /// <returns></returns>
        public PackingPlanController(IPackingPlanService packingPlanService, ICookingPlanService cookingPlanService)
        {
            _packingPlanService = packingPlanService;
            _cookingPlanService = cookingPlanService;
        }
        #endregion

        #region GET: PackingPlan
        /// <summary>
        /// GET: PackingPlan
        /// </summary>
        /// <param name="lineId"></param>
        /// <param name="page"></param>
        /// <returns></returns>
        public virtual ActionResult Index(int? lineId, int? page = 1)
        {
            var newLineId = lineId ?? (int)PackingLines.Line_1;

            if (lineId == null)
            { ViewBag.Line1 = true; }

            var packingPlansList = GetPackingPlans(newLineId).OrderByDescending(x => x.iPackingProductionPlanId);
            var pageNumber = page ?? 1;
            ViewBag.PackingLinesList = PackingLinesEnum.GetPackingLines();
            return View(Views.Index, packingPlansList.ToPagedList(pageNumber, GetPageSize()));
        }
        #endregion

        #region GET: Create New packingPlan
        /// <summary>
        /// GET: Create New packingPlan
        /// </summary>
        public virtual ActionResult Create()
        {
            ViewBag.PackingLines = new SelectList(PackingLinesEnum.GetPackingLines(), "value", "text");
            return PartialView(Views.Create, new PackingProductionPlan());
        }
        #endregion

        #region Edit PackingPlan
        /// <summary>
        /// Edit PackingPlan
        /// </summary>
        [Authorize(Roles = "Admin,Packing Manager")]
        public virtual ActionResult Edit(long id)
        {
            ViewBag.PackingLines = new SelectList(PackingLinesEnum.GetPackingLines(), "value", "text");
            var packingPlan = Mapper.Map<tPackingProductionPlan, PackingProductionPlan>(_packingPlanService.GetById(id));
            return PartialView(Views.Create, packingPlan);
        }
        #endregion

        #region Edit PackingPlan
        /// <summary>
        /// Cancel PackingPlan
        /// </summary>
        /// <param name="id"></param>
        public virtual ActionResult CancelPlan(long id)
        {
            var packingPlan = _packingPlanService.GetById(id);
            packingPlan.bDeleted = true;
            _packingPlanService.Update(packingPlan);
            // ReSharper disable once Mvc.InvalidModelType
            return View(Views.Index, GetPackingPlans(1));
        }
        #endregion

        #region Post: Packing Plan Record
        /// <summary>
        /// Post: Packing Plan Record
        /// </summary>
        /// <param name="packingPlan"></param>
        public virtual ActionResult Post(PackingProductionPlan packingPlan)
        {
            try
            {
                //Edit
                if (packingPlan.iPackingProductionPlanId > 0)
                {
                    var dbPackingPlan = _packingPlanService.GetById(packingPlan.iPackingProductionPlanId);
                    dbPackingPlan.bFinishedPlan = true;
                    dbPackingPlan.iQuantityCompleted = packingPlan.iQuantityCompleted;
                    dbPackingPlan.iPackers = packingPlan.iPackers;
                    dbPackingPlan.tBreakStart = packingPlan.tBreakStart;
                    dbPackingPlan.tBreakFinish = packingPlan.tBreakFinish;
                    dbPackingPlan.tActualStart = packingPlan.tActualStart;
                    dbPackingPlan.tActualFinish = packingPlan.tActualFinish;
                    dbPackingPlan.Sys_ModifyBy = User.Identity.GetUserId();
                    dbPackingPlan.Sys_ModifyDateTime = DateTime.Now;
                    EditPackingPlan(dbPackingPlan);
                    AuditScope.CreateAndSave("Editing Packing Plan", new { User = User.Identity.GetUserId(), CookingPlanid = dbPackingPlan.iPackingProductionPlanId });
                }
                //Add
                else
                {
                    packingPlan.bFinishedPlan = false;
                    packingPlan.Sys_CreatedBy = User.Identity.GetUserId();
                    packingPlan.Sys_CreatedDateTime = DateTime.Now;
                    packingPlan.tCookingPlan = null;
                    var savedPackingPlan = AddPackingPlan(packingPlan);
                    packingPlan.iPackingProductionPlanId = savedPackingPlan.iPackingProductionPlanId;

                    var cookingPlanRecord = _cookingPlanService.GetAll().FirstOrDefault(m => m.vBatchNo == packingPlan.vCookingBatchNo);
                    // ReSharper disable once PossibleNullReferenceException
                    cookingPlanRecord.bPackingConsumptionCheck = true;
                    _cookingPlanService.Update(cookingPlanRecord);

                    AuditScope.CreateAndSave("Saving Packing Plan", new { User = User.Identity.GetUserId(), CookingPlanid = packingPlan.iPackingProductionPlanId });
                }

                return RedirectToAction(MVC.PackingPlan.Index());
            }
            catch (Exception ex)
            {
                AuditScope.CreateAndSave("Error Saving Packing Plan Record: ", new { User = User.Identity.GetUserId(), Exception = ex.Message });
                return RedirectToAction(MVC.Home.Index());
            }
        }
        #endregion

        #region Edit Packing Plan Record
        /// <summary>
        /// Edit Packing Plan Record
        /// </summary>
        /// <param name="packingPlan"></param>
        private void EditPackingPlan(tPackingProductionPlan packingPlan)
        {
            try
            {
                _packingPlanService.Update(packingPlan);
            }
            catch (Exception ex)
            {
                AuditScope.CreateAndSave("Error editing packing plan record: ", new { User = User.Identity.GetUserId(), Exception = ex.Message });
                throw;
            }
        }
        #endregion

        #region Add Packing Plan Record
        /// <summary>
        /// Add Packing Plan Record
        /// </summary>
        /// <param name="packingPlan"></param>
        private tPackingProductionPlan AddPackingPlan(PackingProductionPlan packingPlan)
        {
            try
            {
                //Validate Product Code
                var codeAlreadyAssigned = _packingPlanService.ValidateCode(packingPlan.vBatchNo);
                if (codeAlreadyAssigned)
                {
                    packingPlan.vBatchNo = _packingPlanService.GetBatchCode();
                }
                return _packingPlanService.Create(Mapper.Map<PackingProductionPlan, tPackingProductionPlan>(packingPlan));
            }
            catch (Exception ex)
            {
                AuditScope.CreateAndSave("Error adding packing record: ", new { User = User.Identity.GetUserId(), Exception = ex.Message });
                throw;
            }
        }
        #endregion

        #region Private: GetPackingPlans
        /// <summary>
        /// Private: GetPackingPlans
        /// </summary>
        /// <param name="LineId"></param>
        // ReSharper disable once InconsistentNaming
        private List<PackingProductionPlan> GetPackingPlans(int LineId)
        {
            try
            {
                ViewBag.hdnLineId = LineId;
                var packingPlanList = _packingPlanService.GetPackingPlansByLineId(LineId).ToList();
                return packingPlanList.Select(Mapper.Map<tPackingProductionPlan, PackingProductionPlan>).ToList();
            }
            catch (Exception ex)
            {
                AuditScope.CreateAndSave("Error getting packing plan record: ", new { User = User.Identity.GetUserId(), Exception = ex.Message });
                throw;
            }
        }
        #endregion

        #region Get Batch By Code
        /// <summary>
        /// Get Batch By Code
        /// </summary>
        /// <param name="code"></param>
        [HttpPost]
        public virtual JsonResult GetBatchByCode(string code)
        {
            try
            {
                var batchList = _packingPlanService.GetByCode(code);
                var batchesList = new ArrayList();
                foreach (var batch in batchList)
                {
                    // ReSharper disable once PossibleInvalidOperationException
                    if (batch.bIsTask.Value == false)
                    {
                        batchesList.Add(new { id = batch.tCookingPlan.iCookingPlanId, name = batch.tCookingPlan.tRecipe.vRecipeName, description = batch.tCookingPlan.tRecipe.vResipeDescription, code = batch.vCookingBatchNo, weight = batch.iWeight, lineNo = batch.iLineNo, recipeCode = batch.tCookingPlan.tRecipe.vRecipeCode });
                    }
                }
                return Json(batchesList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                AuditScope.CreateAndSave("Error getting batch code record in packing plan: ", new { User = User.Identity.GetUserId(), Exception = ex.Message });
                throw;
            }
        }
        #endregion

        #region Get Today's Time Slot Availability
        /// <summary>
        /// Get Today's Time Slot Availability
        /// </summary>
        /// <param name="iLineNo"></param>
        /// <param name="startDateTime"></param>
        /// <param name="finishDateTime"></param>
        public virtual JsonResult GetTodaysTimeSlotAvailability(int iLineNo, DateTime startDateTime, DateTime finishDateTime)
        {
            try
            {
                string slotAvailability = _packingPlanService.GetTodaysTimeSlotAvailability(iLineNo, startDateTime, finishDateTime);
                return Json(new { slot = slotAvailability }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                AuditScope.CreateAndSave("Error finiding time slot for Packing Plan.", new { User = User.Identity.GetUserId(), Exception = ex.Message });
                return Json(new { slot = ex }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Check Finishing Sequence
        /// <summary>
        /// Get Today's Time Slot Availability
        /// </summary>
        /// <param name="iPackingProductionPlanId"></param>
        public virtual JsonResult CheckFinishingSequence(int iPackingProductionPlanId)
        {
            try
            {
                string result = _packingPlanService.CheckFinishingSequence(iPackingProductionPlanId);
                return Json(new { validity = result }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                AuditScope.CreateAndSave("Error checking validit sequence for Packing Plan.", new { User = User.Identity.GetUserId(), Exception = ex.Message });
                return Json(new { validity = ex }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Print Record as Report

        /// <summary>
        /// Print Record
        /// </summary>
        /// <param name="pId">Id of the record to print</param>
        /// <returns>RDLC Report View</returns>
        public virtual ActionResult PrintRecord(Int32 pId)
        {
            try
            {
                return View("~/Views/Report/ReportPreview.aspx", new Report { ReportName = PkReportNames.PackingPlanRecord, Id = pId, Service = _packingPlanService });
            }
            catch (Exception ex)
            {
                AuditScope.CreateAndSave("Error getting report of Packing Plan", new { User = User.Identity.GetUserId(), Exception = ex.Message });
                // ReSharper disable once PossibleIntendedRethrow
                throw ex;
            }
        }
        #endregion
    }
}