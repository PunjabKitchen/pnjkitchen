﻿using System;using System.Collections.Generic;using System.Diagnostics.CodeAnalysis;using System.Linq;using System.Web.Mvc;using System.Web.Script.Serialization;using PunjabKitchen.Models;using PunjabKitchenBLL.Interfaces;
// ReSharper disable All

namespace PunjabKitchen.Controllers
{
    [Authorize(Roles = "Admin")]
    public partial class AuditLogController : BaseController
    {
        #region Data Members
        readonly IEventService _eventService;
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        ///<param name="eventService"></param>
        public AuditLogController(IEventService eventService)
        {
            _eventService = eventService;
        }
        #endregion

        #region Get Audit Filters View
        /// <summary>
        /// Get Audit Log Filters View
        /// </summary>
        [HttpGet]
        public virtual ActionResult Index()        {
            ViewBag.Users = new SelectList(new ApplicationDbContext().Users.Select(s => new { Id = s.Id, Name = s.FirstName + " " + s.LastName }).ToList(), "Id", "Name");
            ViewBag.Modules = new SelectList(GetModulesList(), "Item1", "Item1");

            return View(Views.Index);
        }
        #endregion

        #region Get Audit Log List With Filters
        /// <summary>
        /// Get Audit Log List With Filters
        /// </summary>
        [HttpGet]        [SuppressMessage("ReSharper", "InconsistentNaming")]        [SuppressMessage("ReSharper", "ParameterHidesMember")]        public virtual ActionResult GetAuditLogByFilter(string Module, string User, DateTime StartDateTime, DateTime EndDateTime)        {            var eventsList = _eventService.GetAll();            var auditEventsList = new List<AuditEventModel>();            if (eventsList != null)
            {
                // ReSharper disable once LoopCanBeConvertedToQuery
                var filteredList = eventsList.Where(w => w.Data.ToLower().Replace(" ", "").Contains(User.ToLower()));
                if (filteredList != null)
                {
                    foreach (var record in filteredList)
                    {
                        JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
                        AuditEventModel auditEventObject = jsonSerializer.Deserialize<AuditEventModel>(record.Data);
                        auditEventObject.EventId = Convert.ToInt32(record.EventId);

                        var moduleName = GetModuleForController(auditEventObject.Environment.CallingMethodName.Split('.')[2]);

                        if (moduleName.Equals(Module))                        {                            if (auditEventObject.User != null)                            {                                if (auditEventObject.User.ToLower().Equals(User.ToLower()) &&
                                                  (auditEventObject.StartDate.Date >= StartDateTime.Date) &&
                               (auditEventObject.StartDate.Date <= EndDateTime.Date))
                                {
                                    auditEventsList.Add(auditEventObject);
                                }                            }
                        }
                    }
                }
            }

            return PartialView(Views.IndexTable, auditEventsList);
        }
        #endregion

        #region Private: Get Modules List
        /// <summary>
        /// Private: Get Modules List
        /// </summary>
        private List<Tuple<string>> GetModulesList()        {            var result = new List<Tuple<string>>();            result.Add(new Tuple<string>("Account"));
            result.Add(new Tuple<string>("Checks"));
            result.Add(new Tuple<string>("Attendance"));
            result.Add(new Tuple<string>("GoodsIn"));
            result.Add(new Tuple<string>("Planning"));
            result.Add(new Tuple<string>("Cooking"));
            result.Add(new Tuple<string>("Packing"));
            result.Add(new Tuple<string>("GoodsOut"));
            result.Add(new Tuple<string>("Boxing"));

            return result;        }
        #endregion

        #region Private: Get Module Name for Controller
        /// <summary>
        /// Private: Get Module Name for Controller
        /// </summary>
        private string GetModuleForController(string controllerName)        {            if (controllerName.Contains("AccountController"))            {                return "Account";            }

            switch (controllerName)
            {
                case "AccountController":                case "RolesAdminController":                case "UserAdminController":                case "HomeController":                case "ManageController":
                case "BaseController":                    return "Account";
                case "ChecksController":                case "NotificationsController":                    return "Checks";
                case "AttendanceController":                    return "Attendance";
                case "GoodsInController":                case "PickListController":                    return "GoodsIn";
                case "BatchCoolingTempRecrdController":                case "ColdDessertPackingController":                case "CookingPickListController":                case "CookingPlanController":                case "FreezerRecordController":                case "HotPackingRecordController":                case "MeatMincingRecordController":                case "RiceCoolingTempRecordController":                case "SlicingRecordController":                case "RecipeController":                case "ProductController":                    return "Cooking";
                case "BatchChangeCheckPureeController":                case "FerrousDetectionAndPackWeightController":                case "MetalDetectionAndPackWeightController":                case "PackingPlanController":                case "PackingRecordController":                case "PackingRecord2Controller":                case "PortionWeightsCheckPackingController":                case "PureeMealBatchAssemblyController":                case "PureeShapeStarchRecordController":                case "ShapesControlPackingController":                case "SpotWeightAndMetalCheckController":                case "VegetarianPackingRecordController":                    return "Packing";
                case "PlanningController":                    return "Planning";
                case "GoodsOutPicklistController":                case "GoodsOutPlanController":                    return "GoodsOut";
                case "BoxingRecordController":                    return "Boxing";
                default:                    return "";            }        }
        #endregion
    }
}