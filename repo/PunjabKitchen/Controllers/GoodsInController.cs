﻿using Audit.Core;
using AutoMapper;
using Microsoft.AspNet.Identity;
using PunjabKitchen.Models;
using PunjabKitchenBLL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using PunjabKitchenEntities.EntityModel;
using PagedList;
using PunjabKitchen.Models.RequestModels;

namespace PunjabKitchen.Controllers
{
    [Authorize(Roles = "Admin,Goods In Staff,Goods In Manager")]
    public partial class GoodsInController : BaseController
    {
        #region Data Member
        readonly IGoodsInService _goodsInService;
        readonly IProductService _productService;
        readonly IProductTypeService _productTypeService;
        readonly IUnitService _unitService;
        readonly IStockService _stockService;
        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="productService"></param>
        /// <param name="stockService"></param>
        /// <param name="goodsInService"></param>
        /// <param name="productTypeService"></param>
        /// <param name="unitService"></param>
        public GoodsInController(IGoodsInService goodsInService, IProductTypeService productTypeService,
             IUnitService unitService, IProductService productService, IStockService stockService)
        {
            _goodsInService = goodsInService;
            _productTypeService = productTypeService;
            _unitService = unitService;
            _productService = productService;
            _stockService = stockService;
        }
        #endregion

        #region Get Deliveries List 
        /// <summary>
        /// Get Deliveries List 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public virtual ActionResult Deliveries(int? page = 1)
        {
            var deliveriesPlansList = GetDeliveries().OrderByDescending(x => x.tGoodsInId);
            var pageNumber = page ?? 1;
            return View(Views.Deliveries.Index, deliveriesPlansList.ToPagedList(pageNumber, GetPageSize()));
        }
        #endregion

        #region Create New Delivery
        /// <summary>
        /// Create New Delivery
        /// </summary>
        /// <returns></returns>
        public virtual ActionResult Create()
        {
            //Getting Product Types
            ViewBag.ProductTypes = new SelectList(GetProductTypes(), "iProductTypeId", "vName");
            return View(Views.Deliveries.Create, new GoodsIn());
        }
        #endregion

        #region Edit Goods In Record
        /// <summary>
        /// Edit Goods In Record
        /// </summary>
        /// <returns></returns>
        public virtual ActionResult Edit(long id)
        {
            var goodsIn = Mapper.Map<tGoodsIn, GoodsIn>(_goodsInService.GetById(id));
            //checking if goods intake already finished
            if (goodsIn.bGoodsIntakeFinished == true)
            {
                return RedirectToAction(MVC.GoodsIn.GoodsToStock(id));
            }

            //Getting Product Types
            ViewBag.ProductTypes = new SelectList(GetProductTypes(), "iProductTypeId", "vName");

            return View(Views.Deliveries.Create, goodsIn);
        }
        #endregion

        #region Post Goods In Record
        /// <summary>
        /// Post Goods In Record
        /// </summary>
        /// <param name="goodsIn"></param>
        /// <returns></returns>
        public virtual ActionResult Post(GoodsIn goodsIn)
        {
            try
            {
                //Edit
                if (goodsIn.tGoodsInId > 0)
                {
                    goodsIn.Sys_ModifyBy = User.Identity.GetUserId();
                    goodsIn.Sys_ModifyDateTime = DateTime.Now;
                    EditGoodsIn(goodsIn);
                    AuditScope.CreateAndSave("Editing Goods In", new { User = User.Identity.GetUserId(), GoodsInId = goodsIn.tGoodsInId });
                }
                //Add
                else
                {
                    goodsIn.Sys_CreatedBy = User.Identity.GetUserId();
                    goodsIn.Sys_CreatedDateTime = DateTime.Now;
                    var savedGoodsIn = AddGoodsIn(goodsIn);
                    goodsIn.tGoodsInId = savedGoodsIn.tGoodsInId;
                    AuditScope.CreateAndSave("Saving Goods In", new { User = User.Identity.GetUserId(), GoodsInId = goodsIn.tGoodsInId });
                }
                //return RedirectToAction("Create");
                ViewBag.ProductTypes = new SelectList(GetProductTypes(), "iProductTypeId", "vName");
                return RedirectToAction(MVC.GoodsIn.Edit(goodsIn.tGoodsInId));
            }
            catch (Exception ex)
            {
                AuditScope.CreateAndSave("Error saving Goods In Record", new { User = User.Identity.GetUserId(), Exception = ex.Message });
                throw;
            }
        }
        #endregion

        #region Add Edit Product
        /// <summary>
        /// Add Edit Product
        /// </summary>
        /// <param name="product"></param>
        /// <returns></returns>
        public virtual ActionResult AddEditProduct(Product product)
        {
            var savingProduct = SaveProduct(product);
            return RedirectToAction(MVC.GoodsIn.Edit((long)savingProduct.tGoodsInId));
        }
        #endregion

        #region Add Edit Product To GoodsIn
        /// <summary>
        /// Add Edit Product To GoodsIn
        /// </summary>
        /// <param name="product"></param>
        /// <returns></returns>
        public virtual JsonResult AddEditProductToGoodsIn(Product product)
        {
            var result = SaveProduct(product);
            return Json(result.tGoodsInId, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Goods To Stock
        /// <summary>
        /// Goods To Stock
        /// </summary>
        /// <param name="goodsInId"></param>
        /// <returns></returns>
        public virtual ActionResult GoodsToStock(long goodsInId)
        {
            var productsList = GetProducts(goodsInId);
            ViewBag.StockLocationList = new SelectList(GetStockLocationSelectList(), "vStorageLocation", "vStorageLocation");
            return View(Views.Deliveries._GoodsToStock, productsList);
        }
        public virtual JsonResult GetStockPoints(string stockLocation)
        {
            var list = GetAllDistinctStockPoints(stockLocation);
            return Json(list, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Allocate Stock Location
        /// <summary>
        /// Allocate Stock Location
        /// </summary>
        /// <param name="productId"></param>
        /// <param name="stockLocId"></param>
        /// <returns></returns>
        public virtual JsonResult AllocateStockLocation(int productId, int stockLocId)
        {
            try
            {
                var product = _productService.GetById(productId);
                product.iStockId = stockLocId;
                _productService.Update(product);

                //Update stock location
                var stock = _stockService.GetById(stockLocId);
                stock.iProductId = product.iProductId;
                stock.iQuantity = product.iQuantity;
                stock.iUnitId = product.iUnitId;
                _stockService.Update(stock);
                AuditScope.CreateAndSave("Allocating Stock Location", new { User = User.Identity.GetUserId() });
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                AuditScope.CreateAndSave("Error Allocating Stock Location: ", new { User = User.Identity.GetUserId(), Exception = ex.Message });
                throw;
            }
        }
        #endregion

        #region Finish Goods In
        /// <summary>
        /// Finish Goods In
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public virtual JsonResult FinishGoodsIn(int id)
        {
            try
            {
                var goodsIn = _goodsInService.GetById(id);
                if (goodsIn.tProducts.Count == 0)
                {
                    return Json(new { result = false }, JsonRequestBehavior.AllowGet);
                }
                goodsIn.bGoodsIntakeFinished = true;
                _goodsInService.Update(goodsIn);
                AuditScope.CreateAndSave("Finishing Goods In", new { User = User.Identity.GetUserId() });
                return Json(new { result = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                AuditScope.CreateAndSave("Error Finishing Goods In: ", new { User = User.Identity.GetUserId(), Exception = ex.Message });
                return Json(new { result = false }, JsonRequestBehavior.AllowGet);
                throw;
            }
        }
        #endregion

        #region Return Add Goods In View
        /// <summary>
        /// Return Add Goods In View
        /// </summary>
        /// <returns></returns>
        public virtual ActionResult CreateGoodsIn(int id)
        {
            var newProduct = new Product();
            newProduct.tGoodsInId = id;
            ViewBag.units = new SelectList(GetUnits(), "iUnitId", "vName");
            return PartialView(Views.Deliveries._CreateGoodsIn, newProduct);
        }
        #endregion

        #region Private: Deliveries, Products, Stock Location
        private List<GoodsIn> GetDeliveries()
        {
            var deliveriesList = _goodsInService.GetAll();
            return deliveriesList.Select(Mapper.Map<tGoodsIn, GoodsIn>).ToList();
        }
        private List<ProductType> GetProductTypes()
        {
            var productTypes = new List<ProductType>();
            var list = _productTypeService.GetAll();
            // ReSharper disable once LoopCanBeConvertedToQuery
            foreach (var item in list)
            {
                productTypes.Add(Mapper.Map<tProductType, ProductType>(item));
            }
            return productTypes;
        }
        private List<StockLocationSelectList> GetStockLocationSelectList()
        {
            var stockLocations = new List<StockLocationSelectList>();
            var list = _stockService.GetAllDistinctStockLocations();
            // ReSharper disable once LoopCanBeConvertedToQuery
            foreach (var item in list)
            {
                stockLocations.Add(Mapper.Map<tStock, StockLocationSelectList>(item));
            }
            return stockLocations;
        }

        private List<StockPointSelectList> GetAllDistinctStockPoints(string stockLocation)
        {

            var stockLocations = new List<StockPointSelectList>();
            var stockPointslist = _stockService.GetAllDistinctStockPoints(stockLocation);
            // ReSharper disable once LoopCanBeConvertedToQuery
            foreach (var item in stockPointslist)
            {
                stockLocations.Add(Mapper.Map<tStock, StockPointSelectList>(item));
            }
            return stockLocations;
        }

        private List<Unit> GetUnits()
        {
            var units = new List<Unit>();
            var list = _unitService.GetAll();
            // ReSharper disable once LoopCanBeConvertedToQuery
            foreach (var item in list)
            {
                units.Add(Mapper.Map<tUnit, Unit>(item));
            }
            return units;
        }

        private void EditGoodsIn(GoodsIn goodsIn)
        {
            _goodsInService.Update(Mapper.Map<GoodsIn, tGoodsIn>(goodsIn));
        }
        private tGoodsIn AddGoodsIn(GoodsIn goodsIn)
        {
            return _goodsInService.Create(Mapper.Map<GoodsIn, tGoodsIn>(goodsIn));
        }
        private tProduct SaveProduct(Product product)
        {
            try
            {
                tProduct savingProduct;
                ////Updating Batch Code as Lot Number
                //product.vBatchCode = product.vLotNumber;
                //add
                if (product.iProductId == 0)
                {
                    savingProduct = Mapper.Map<Product, tProduct>(product);
                    savingProduct.Sys_CreatedBy = User.Identity.GetUserId();
                    savingProduct.Sys_CreatedDateTime = DateTime.Now;
                    //Validate Product Code
                    var codeAlreadyAssigned = _productService.ValidateCode(savingProduct.vProductCode);
                    if (codeAlreadyAssigned)
                    {
                        if (savingProduct.iProductTypeId != null)
                            savingProduct.vProductCode = _productService.GetProductCode((int)savingProduct.iProductTypeId);
                    }
                    _productService.Create(savingProduct);
                    AuditScope.CreateAndSave("Adding Product for Goods In", new { User = User.Identity.GetUserId() });
                }
                //update
                else
                {
                    savingProduct = Mapper.Map<Product, tProduct>(product);
                    savingProduct.Sys_ModifyBy = User.Identity.GetUserId();
                    savingProduct.Sys_CreatedDateTime = DateTime.Now;
                    _productService.Update(savingProduct);
                    AuditScope.CreateAndSave("Editing Product for Goods In", new { User = User.Identity.GetUserId() });
                }
                return savingProduct;
            }
            catch (Exception ex)
            {
                AuditScope.CreateAndSave("Error saving product in Goods In: ", new { User = User.Identity.GetUserId(), Exception = ex.Message });
                throw;
            }
        }
        private List<Product> GetProducts(long goodsInId)
        {
            List<Product> productsList = new List<Product>();
            //Get Products by Goods In Id
            var goodsInItem = _goodsInService.GetById(goodsInId);
            // ReSharper disable once LoopCanBeConvertedToQuery
            foreach (var item in goodsInItem.tProducts)
            {
                if (item.iStockId == null)
                {
                    productsList.Add(Mapper.Map<tProduct, Product>(item));
                }
            }
            return productsList;
        }
        private List<Product> GetProductsToSetThreshold(long goodsInId)
        {
            //Get Products by Goods In Id
            var goodsInItem = _goodsInService.GetById(goodsInId);
            return goodsInItem.tProducts.Select(item => Mapper.Map<tProduct, Product>(item)).ToList();
        }
        #endregion

        #region Print Record as Report

        /// <summary>
        /// Print Record
        /// </summary>
        /// <param name="pId">Id of the record to print</param>
        /// <returns>RDLC Report View</returns>
        public virtual ActionResult PrintRecord(Int32 pId)
        {
            try
            {
                return View("~/Views/Report/ReportPreview.aspx", new Report { ReportName = PkReportNames.DeliveryRecord, Id = pId, Service = _goodsInService });
            }
            catch (Exception ex)
            {
                AuditScope.CreateAndSave("Error getting report of Goods In", new { User = User.Identity.GetUserId() });
                // ReSharper disable once PossibleIntendedRethrow
                throw ex;
            }
        }
        #endregion

        #region Set Threshold
        /// <summary>
        /// Admin Set Threshold of products
        /// </summary>
        /// <param name="goodsInId"></param>
        /// <returns></returns>
        public virtual ActionResult SetThreshold(long goodsInId)
        {
            var productsList = GetProductsToSetThreshold(goodsInId);
            ViewBag.StockLocationList = new SelectList(GetStockLocationSelectList(), "vStorageLocation", "vStorageLocation");
            return View(Views.Deliveries._ProductThreshold, productsList);
        }

        public virtual JsonResult PostProductThreshold(IEnumerable<ProductThresholdModel> products)
        {
            foreach (var product in products)
            {
                var productToUpdate = _productService.GetById(product.iProductId);
                productToUpdate.iThreshold = product.iThreshold;
                _productService.Update(productToUpdate);
                AuditScope.CreateAndSave("Saving Product Threshold", new { User = User.Identity.GetUserId() });
            }
            return Json(true, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}