﻿using System;
using System.Collections.Generic;
using PunjabKitchen.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.IO;
using Audit.Core;
using PunjabKitchen.Models.RequestModels;
using PunjabKitchenBLL.Interfaces;
using PunjabKitchenEntities.EntityModel;
using PagedList;

namespace PunjabKitchen.Controllers
{
    [Authorize(Roles = "Admin")]
    public partial class UsersAdminController : BaseController
    {
        #region Data Members
        readonly IShoeCleaningCheckService _shoeCleaningCheckService;
        #endregion

        #region Constructor
        public UsersAdminController(IShoeCleaningCheckService shoeCleaningCheckService)
        {
            _shoeCleaningCheckService = shoeCleaningCheckService;
        }

        public UsersAdminController(ApplicationUserManager userManager, ApplicationRoleManager roleManager, IShoeCleaningCheckService shoeCleaningCheckService)
        {
            UserManager = userManager;
            RoleManager = roleManager;
            _shoeCleaningCheckService = shoeCleaningCheckService;
        }
        #endregion

        #region User Manager
        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        private ApplicationRoleManager _roleManager;
        public ApplicationRoleManager RoleManager
        {
            get
            {
                return _roleManager ?? HttpContext.GetOwinContext().Get<ApplicationRoleManager>();
            }
            private set
            {
                _roleManager = value;
            }
        }
        #endregion

        #region User Management Functions
        //
        // GET: /Users/
        public virtual async Task<ActionResult> Index(int? page = 1)
        {
            var usersData = await UserManager.Users.ToListAsync();
            var usersList = usersData.OrderByDescending(x => x.Id);
            ViewBag.RoleManager = RoleManager;
            var pageNumber = page ?? 1;
            return View("~/Views/UsersAdmin/Index.cshtml", usersList.ToPagedList(pageNumber, GetPageSize()));
        }

        //
        // GET: /Users/Details/5
        public virtual async Task<ActionResult> Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var user = await UserManager.FindByIdAsync(id);

            ViewBag.RoleNames = await UserManager.GetRolesAsync(user.Id);

            return View(user);
        }

        //
        // GET: /Users/Create
        public virtual async Task<ActionResult> Create()
        {
            //Get the list of Roles
            ViewBag.RoleId = new SelectList(await RoleManager.Roles.ToListAsync(), "Name", "Name");
            return View();
        }

        //
        // POST: /Users/Create
        [HttpPost]
        public virtual async Task<ActionResult> Create([Bind(Exclude = "UserPhoto")]RegisterViewModel userViewModel, params string[] selectedRoles)
        {
            if (ModelState.IsValid)
            {
                // To convert the user uploaded Photo as Byte Array before save to DB
                byte[] imageData = null;
                if (Request.Files.Count > 0)
                {
                    HttpPostedFileBase poImgFile = Request.Files["UserPhoto"];

                    if (poImgFile != null)
                        using (var binary = new BinaryReader(poImgFile.InputStream))
                        {
                            imageData = binary.ReadBytes(poImgFile.ContentLength);
                        }
                }
                var user = new ApplicationUser
                {
                    UserName = userViewModel.Email,
                    Email =
                    userViewModel.Email,
                    // Add the Address Info:
                    Address = userViewModel.Address,
                    City = userViewModel.City,
                    State = userViewModel.State,
                    PostalCode = userViewModel.PostalCode,
                    FirstName = userViewModel.FirstName,
                    LastName = userViewModel.LastName,
                    UserPhoto = imageData
                };

                // Add the Address Info:
                user.Address = userViewModel.Address;
                user.City = userViewModel.City;
                user.State = userViewModel.State;
                user.PostalCode = userViewModel.PostalCode;

                // Then create:
                var adminresult = await UserManager.CreateAsync(user, userViewModel.Password);
                AuditScope.CreateAndSave("Saving New User: " + user.FirstName + " " + user.LastName, new { User = User.Identity.GetUserId() });

                //Add User to the selected Roles 
                if (adminresult.Succeeded)
                {
                    if (selectedRoles != null)
                    {
                        var result = await UserManager.AddToRolesAsync(user.Id, selectedRoles);
                        if (!result.Succeeded)
                        {
                            ModelState.AddModelError("", result.Errors.First());
                            ViewBag.RoleId = new SelectList(await RoleManager.Roles.ToListAsync(), "Name", "Name");
                            return View();
                        }
                    }
                }
                else
                {
                    ModelState.AddModelError("", adminresult.Errors.First());
                    ViewBag.RoleId = new SelectList(RoleManager.Roles, "Name", "Name");
                    return View();

                }
                return RedirectToAction("Index");
            }
            ViewBag.RoleId = new SelectList(RoleManager.Roles, "Name", "Name");
            return View();
        }

        //
        // GET: /Users/Edit/1
        public virtual async Task<ActionResult> Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var user = await UserManager.FindByIdAsync(id);
            if (user == null)
            {
                return HttpNotFound();
            }

            var userRoles = await UserManager.GetRolesAsync(user.Id);

            return View(new EditUserViewModel()
            {
                Id = user.Id,
                Email = user.Email,
                // Include the Addresss info:
                Address = user.Address,
                City = user.City,
                State = user.State,
                PostalCode = user.PostalCode,
                FirstName = user.FirstName,
                LastName = user.LastName,
                UserPhoto = user.UserPhoto,
                RolesList = RoleManager.Roles.ToList().Select(x => new SelectListItem()
                {
                    Selected = userRoles.Contains(x.Name),
                    Text = x.Name,
                    Value = x.Name
                })
            });
        }

        //
        // POST: /Users/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual async Task<ActionResult> Edit([Bind(Include = "Email,Id,Address,City,State,PostalCode,FirstName,LastName,UserPhoto", Exclude = "")] EditUserViewModel editUser, params string[] selectedRole)
        {
            if (ModelState.IsValid)
            {
                HttpPostedFileBase poImgFile = Request.Files["UserPhoto"];

                var user = await UserManager.FindByIdAsync(editUser.Id);
                if (user == null)
                {
                    return HttpNotFound();
                }
                // To convert the user uploaded Photo as Byte Array before save to DB
                // ReSharper disable once PossibleNullReferenceException
                if (poImgFile.ContentLength > 0)
                {

                    // ReSharper disable once ConditionIsAlwaysTrueOrFalse
                    if (poImgFile != null)
                        using (var binary = new BinaryReader(poImgFile.InputStream))
                        {
                            var imageData = binary.ReadBytes(poImgFile.ContentLength);
                            user.UserPhoto = imageData;
                            AuditScope.CreateAndSave("Updating User Photo", new { User = User.Identity.GetUserId() });
                        }
                }

                user.UserName = editUser.Email;
                user.Email = editUser.Email;
                user.Address = editUser.Address;
                user.City = editUser.City;
                user.State = editUser.State;
                user.PostalCode = editUser.PostalCode;
                user.FirstName = editUser.FirstName;
                user.LastName = editUser.LastName;

                var userRoles = await UserManager.GetRolesAsync(user.Id);

                selectedRole = selectedRole ?? new string[] { };

                var result = await UserManager.AddToRolesAsync(user.Id, selectedRole.Except(userRoles).ToArray());

                if (!result.Succeeded)
                {
                    ModelState.AddModelError("", result.Errors.First());
                    return View();
                }
                result = await UserManager.RemoveFromRolesAsync(user.Id, userRoles.Except(selectedRole).ToArray());

                if (!result.Succeeded)
                {
                    ModelState.AddModelError("", result.Errors.First());
                    return View();
                }
                AuditScope.CreateAndSave("User Updated: " + user.FirstName + " " + user.LastName, new { User = User.Identity.GetUserId() });
                return RedirectToAction("Index");
            }
            AuditScope.CreateAndSave("User Editing Failed", new { User = User.Identity.GetUserId() });
            // ReSharper disable once LocalizableElement
            ModelState.AddModelError("", "Something failed.");
            return View();
        }

        //
        // GET: /Users/Delete/5
        public virtual async Task<ActionResult> Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var user = await UserManager.FindByIdAsync(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        //
        // POST: /Users/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public virtual async Task<ActionResult> DeleteConfirmed(string id)
        {
            if (ModelState.IsValid)
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }

                var user = await UserManager.FindByIdAsync(id);
                if (user == null)
                {
                    return HttpNotFound();
                }
                var result = await UserManager.DeleteAsync(user);
                if (!result.Succeeded)
                {
                    ModelState.AddModelError("", result.Errors.First());
                    return View();
                }
                return RedirectToAction("Index");
            }
            return View();
        }

        #region Checking Email Address Validity
        /// <summary>
        /// Checking Email Address Validity
        /// </summary>
        /// <param name="emailAddress"></param>
        public virtual JsonResult CheckEmailValidity(string emailAddress)
        {
            try
            {
                var check = UserManager.FindByEmail(emailAddress.ToLower()) != null ? "Invalid" : "Valid" ;
                return Json(new { emailValidity = check }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                AuditScope.CreateAndSave("Error Validating Email Address.", new { User = User.Identity.GetUserId(), Exception = ex.Message });
                return Json(new { check = ex }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #endregion

        #region GetUserShoesCleaningHistory
        public virtual ActionResult GetUserShoesCleaningHistory(ChecksHistoryRequestModel requestModel)
        {
            var usersList = UserManager.Users.ToList();
            var shoesCleanedInPeriod = _shoeCleaningCheckService.GetShoesCleaningHistory(requestModel.StartDateTime, requestModel.EndDateTime);
            var list = new List<ChecksHistoryModel>();
            foreach (var user in usersList)
            {
                // ReSharper disable once RedundantAssignment
                var foundRecord = new tShoeCleaningRecrdWeekly();
                //if (shoesCleanedInPeriod != null)
                //{

                var roles = UserManager.GetRoles(user.Id);
                // ReSharper disable once InvertIf
                if (roles.Any(x => x.Contains("Cooking")))
                {
                    // ReSharper disable once PossibleMultipleEnumeration
                    // ReSharper disable once ReplaceWithSingleCallToFirstOrDefault
                    foundRecord = shoesCleanedInPeriod.Where(x => x.iUserId == user.Id &&
                                                                           // ReSharper disable once PossibleInvalidOperationException
                                                                           x.dCheckPerformDate.Value.Date >= requestModel.StartDateTime.Date &&
                                                                            x.dCheckPerformDate.Value.Date <= requestModel.EndDateTime.Date).FirstOrDefault();
                    list.Add(new ChecksHistoryModel
                    {
                        CreatedBy = user.FirstName + " " + user.LastName,
                        CreatedDate = foundRecord?.dCheckPerformDate,
                        IsPerformed = foundRecord != null
                    });
                }
            }
            return PartialView("~/Views/Checks/Daily/ShoeCleaningCheck.cshtml", list);

        }
        #endregion
    }
}
