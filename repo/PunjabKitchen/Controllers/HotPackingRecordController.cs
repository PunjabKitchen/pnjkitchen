﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Audit.Core;
using AutoMapper;
using Microsoft.AspNet.Identity;
using PunjabKitchen.Models;
using PunjabKitchenBLL.Interfaces;
using PunjabKitchenEntities.EntityModel;
using PagedList;

namespace PunjabKitchen.Controllers
{
    [Authorize(Roles = "Admin,Packing Manager,Packing  Staff")]
    public partial class HotPackingRecordController : BaseController
    {
        #region Data Members
        readonly IHotPackingRercordService _hotPackingRecordService;
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="hotPackingRecordService"></param>
        public HotPackingRecordController(IHotPackingRercordService hotPackingRecordService)
        {
            _hotPackingRecordService = hotPackingRecordService;
        }
        #endregion

        #region Create New Hot Packing Record
        /// <summary>
        /// Create New Hot Packing Record
        /// </summary>
        /// <returns></returns>
        public virtual ActionResult CreateHotPacking()
        {
            return PartialView(Views.Create, new HotPackingRercord());
        }
        #endregion

        #region Get Hot Packing Record List 
        /// <summary>
        /// Get Hot Packing Record List 
        /// </summary>
        /// <param name="page"></param>
        /// <returns></returns>
        [HttpGet]
        public virtual ActionResult HotPackingRecords(int? page = 1)
        {
            var recordsList = GetHotPackingRecord().OrderByDescending(x => x.iHotPackingRercordId);
            var pageNumber = page ?? 1;
            return View(Views.Index, recordsList.ToPagedList(pageNumber, GetPageSize()));
        }
        #endregion

        #region Return Hot Packing Record Create New View
        /// <summary>
        /// Return Hot Packing Record Create New View
        /// </summary>
        /// <returns></returns>
        public virtual ActionResult CreateHotPackingRecord()
        {
            return PartialView(Views.Create);
        }
        #endregion

        #region Return Hot Packing Record Edit View
        /// <summary>
        /// Return Hot Packing Record Edit View
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize(Roles = "Admin,Packing Manager")]
        public virtual ActionResult EditHotPackingRecord(long id)
        {
            HotPackingRercord hotPackingRec = Mapper.Map<tHotPackingRercord, HotPackingRercord>(_hotPackingRecordService.GetById(id));
            return PartialView(Views.Create, hotPackingRec);
        }
        #endregion

        #region  Get All Hot Packing Record
        /// <summary>
        /// Get All Hot Packing Record
        /// </summary>
        /// <returns></returns>
        private List<HotPackingRercord> GetHotPackingRecord()
        {
            var hotPackingRecordList = _hotPackingRecordService.GetAll();
            return hotPackingRecordList.Select(Mapper.Map<tHotPackingRercord, HotPackingRercord>).ToList();
        }
        #endregion

        #region Add/Edit
        /// <summary>
        /// Update Hot Packing record if found else create new
        /// </summary>
        /// <param name="hotPackingRecordModel"></param>
        /// <returns></returns>
        public virtual ActionResult PostHotPacking(HotPackingRercord hotPackingRecordModel)
        {
            try
            {
                //Edit
                if (hotPackingRecordModel.iHotPackingRercordId > 0)
                {
                    var savedihotPackingRecord = Mapper.Map<HotPackingRercord, tHotPackingRercord>(hotPackingRecordModel);
                    savedihotPackingRecord.Sys_ModifyBy = User.Identity.GetUserId();
                    savedihotPackingRecord.Sys_ModifyDateTime = DateTime.Now;
                    _hotPackingRecordService.Update(savedihotPackingRecord);
                    AuditScope.CreateAndSave("Editing Hot Packing Record", new { User = User.Identity.GetUserId() });
                }
                //Add
                else
                {
                    hotPackingRecordModel.Sys_CreatedBy = User.Identity.GetUserId();
                    hotPackingRecordModel.Sys_CreatedDateTime = DateTime.Now;
                    try
                    {
                        _hotPackingRecordService.Create(Mapper.Map<HotPackingRercord, tHotPackingRercord>(hotPackingRecordModel));
                        AuditScope.CreateAndSave("Adding Hot Packing Record", new { User = User.Identity.GetUserId() });
                    }
                    catch (Exception ex)
                    {

                        AuditScope.CreateAndSave("Error Saving Hot Packing record: ", new { User = User.Identity.GetUserId(), Exception = ex.Message });
                        throw;
                    }
                }
                return RedirectToAction(MVC.HotPackingRecord.HotPackingRecords());
            }
            catch (Exception ex)
            {
                AuditScope.CreateAndSave("Error saving hot packing record: ", new { User = User.Identity.GetUserId(), Exception = ex.Message });
                throw;
            }
        }
        #endregion

        #region Print Record as Report

        /// <summary>
        /// Print Record
        /// </summary>
        /// <param name="pId">Id of the record to print</param>
        /// <returns>RDLC Report View</returns>
        public virtual ActionResult PrintRecord(Int32 pId)
        {
            try
            {
                return View("~/Views/Report/ReportPreview.aspx", new Report { ReportName = PkReportNames.HotPackingRecord, Id = pId, Service = _hotPackingRecordService });
            }
            catch (Exception ex)
            {
                AuditScope.CreateAndSave("Error getting report of Hot Packing Record", new { User = User.Identity.GetUserId() });
                // ReSharper disable once PossibleIntendedRethrow
                throw ex;
            }
        }
        #endregion
    }
}