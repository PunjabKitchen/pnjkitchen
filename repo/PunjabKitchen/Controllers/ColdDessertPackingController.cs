﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Audit.Core;
using AutoMapper;
using Microsoft.AspNet.Identity;
using PunjabKitchen.Models;
using PunjabKitchenBLL.Interfaces;
using PunjabKitchenEntities.EntityModel;
using PagedList;

namespace PunjabKitchen.Controllers
{
    [Authorize(Roles = "Admin,Packing Manager,Packing  Staff")]
    public partial class ColdDessertPackingController : BaseController
    {
        #region Data Members
        readonly IColdDessertPackingRecordService _coldDessertPackingRecordService;
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="coldDessertPackingRecordService"></param>
        public ColdDessertPackingController(IColdDessertPackingRecordService coldDessertPackingRecordService)
        {
            _coldDessertPackingRecordService = coldDessertPackingRecordService;
        }
        #endregion

        #region Create New Cold Dessert Packing Record
        /// <summary>
        /// Create New Cold Dessert Packing Record
        /// </summary>
        /// <returns></returns>
        public virtual ActionResult CreateColdDesserts()
        {
            return PartialView(Views.Create, new ColdDesertPackingRecord());
        }
        #endregion

        #region Get Cold Dessert Packing Record List 
        /// <summary>
        /// Get Cold Dessert Packing Record List
        /// </summary>
        /// <param name="page"></param>
        /// <returns></returns>
        [HttpGet]
        public virtual ActionResult ColdDessertPackingRecords(int? page = 1)
        {
            var recordsList = GetColdDessertPackingRecord().OrderByDescending(x => x.iColdDesertPackingRecordId);
            var pageNumber = page ?? 1;
            return View(Views.Index, recordsList.ToPagedList(pageNumber, GetPageSize()));
        }
        #endregion

        #region Return Cold Dessert Packing Record Create New View
        /// <summary>
        /// Return Cold Dessert Packing Record Create New View
        /// </summary>
        /// <returns></returns>
        public virtual ActionResult CreateColdDessertPackingRecord()
        {
            return PartialView(Views.Create);
        }
        #endregion

        #region Return Cold Dessert Packing Record Edit View
        /// <summary>
        /// Return Cold Dessert Packing Record Edit View
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize(Roles = "Admin,Packing Manager")]
        public virtual ActionResult EditColdDessertPackingRecord(long id)
        {
            ColdDesertPackingRecord coldDesertPackingRec = Mapper.Map<tColdDesertPackingRecord, ColdDesertPackingRecord>(_coldDessertPackingRecordService.GetById(id));
            return PartialView(Views.Create, coldDesertPackingRec);
        }
        #endregion

        #region Get All Cold Dessert Packing Record
        /// <summary>
        /// Get All Cold Dessert Packing Record
        /// </summary>
        /// <returns></returns>
        private List<ColdDesertPackingRecord> GetColdDessertPackingRecord()
        {
            var coldDesertPackingRecordList = _coldDessertPackingRecordService.GetAll();
            return coldDesertPackingRecordList.Select(Mapper.Map<tColdDesertPackingRecord, ColdDesertPackingRecord>).ToList();
        }
        #endregion

        #region Add/Edit
        /// <summary>
        /// Update Cold Dessert Packing record if found else create new
        /// </summary>
        /// <param name="coldDessertPackingRecordModel"></param>
        /// <returns></returns>
        public virtual ActionResult PostColdDesserts(ColdDesertPackingRecord coldDessertPackingRecordModel)
        {
            try
            {
                //Edit
                if (coldDessertPackingRecordModel.iColdDesertPackingRecordId > 0)
                {
                    var savedicoldDessertPackingRecord = Mapper.Map<ColdDesertPackingRecord, tColdDesertPackingRecord>(coldDessertPackingRecordModel);
                    savedicoldDessertPackingRecord.Sys_ModifyBy = User.Identity.GetUserId();
                    savedicoldDessertPackingRecord.Sys_ModifyDateTime = DateTime.Now;
                    _coldDessertPackingRecordService.Update(savedicoldDessertPackingRecord);
                    AuditScope.CreateAndSave("Editing Cold Dessert Packing", new { User = User.Identity.GetUserId() });
                }
                //Add
                else
                {
                    coldDessertPackingRecordModel.Sys_CreatedBy = User.Identity.GetUserId();
                    coldDessertPackingRecordModel.Sys_CreatedDateTime = DateTime.Now;
                    try
                    {
                        _coldDessertPackingRecordService.Create(Mapper.Map<ColdDesertPackingRecord, tColdDesertPackingRecord>(coldDessertPackingRecordModel));
                        AuditScope.CreateAndSave("Adding Cold Dessert Packing Record", new { User = User.Identity.GetUserId() });
                    }
                    catch (Exception ex)
                    {
                        AuditScope.CreateAndSave("Error Saving Cold Dessert Record: ", new { User = User.Identity.GetUserId(), Exception = ex.Message });
                        throw;
                    }
                }
                return RedirectToAction(MVC.ColdDessertPacking.ColdDessertPackingRecords());
            }
            catch (Exception ex)
            {
                AuditScope.CreateAndSave("Error Saving Cold Dessert Record: ", new { User = User.Identity.GetUserId(), Exception = ex.Message });
                throw;
            }
        }
        #endregion

        #region Print Record as Report

        /// <summary>
        /// Print Record
        /// </summary>
        /// <param name="pId">Id of the record to print</param>
        /// <returns>RDLC Report View</returns>
        public virtual ActionResult PrintRecord(Int32 pId)
        {
            try
            {
                return View("~/Views/Report/ReportPreview.aspx", new Report { ReportName = PkReportNames.ColdDessertPackingRecord, Id = pId, Service = _coldDessertPackingRecordService });
            }
            catch (Exception ex)
            {
                AuditScope.CreateAndSave("Error getting report of Cold Dessert Packing Record", new { User = User.Identity.GetUserId() });
                // ReSharper disable once PossibleIntendedRethrow
                throw ex;
            }
        }
        #endregion
    }
}