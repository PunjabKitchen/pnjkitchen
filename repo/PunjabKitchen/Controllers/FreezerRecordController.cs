﻿using Audit.Core;
using AutoMapper;
using Microsoft.AspNet.Identity;
using PunjabKitchen.Models;
using PunjabKitchenBLL.Interfaces;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using PunjabKitchenEntities.EntityModel;
using PagedList;

namespace PunjabKitchen.Controllers
{
    [Authorize(Roles = "Admin,Cooking Manager,Cooking Staff")]
    public partial class FreezerRecordController : BaseController
    {
        #region Data Member
        readonly IFreezerRecordService _freezerRecordService;
        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="freezerRecordService"></param>
        public FreezerRecordController(IFreezerRecordService freezerRecordService)
        {
            _freezerRecordService = freezerRecordService;
        }
        #endregion

        #region Create New Freezer Record
        /// <summary>
        /// Create New Freezer Record
        /// </summary>
        /// <returns></returns>
        public virtual ActionResult Create()
        {
            return PartialView(Views.CreateFreezerRecord, new FreezerRecord());
        }
        #endregion

        #region Get Freezer Record List 
        /// <summary>
        /// GET: Freezer Record
        /// </summary>
        /// <param name="page"></param>
        /// <returns></returns>
        [HttpGet]
        public virtual ActionResult FreezerRecords(int? page = 1)
        {
            var recordsList = GetFreezerRecord().OrderByDescending(x => x.iFreezerRecordId);
            var pageNumber = page ?? 1;
            return View(Views.FreezerRecord, recordsList.ToPagedList(pageNumber, GetPageSize()));
        }
        #endregion

        #region Return Freezer Record Create New View
        /// <summary>
        /// Return Freezer Record Create New View
        /// </summary>
        /// <returns></returns>
        public virtual ActionResult CreateFreezerRecord()
        {
            return PartialView(Views.CreateFreezerRecord);
        }
        #endregion

        #region Return Freezer Record Edit View
        /// <summary>
        /// Return Freezer RecordEdit View
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize(Roles = "Admin,Cooking Manager")]
        public virtual ActionResult EditFreezerRecord(long id)
        {
            FreezerRecord freezerRec = Mapper.Map<tFreezerRecord, FreezerRecord>(_freezerRecordService.GetById(id));
            return PartialView(Views.CreateFreezerRecord, freezerRec);
        }
        #endregion

        #region  Get All Freezer Record
        /// <summary>
        /// Get All Freezer Record
        /// </summary>
        /// <returns></returns>
        private List<FreezerRecord> GetFreezerRecord()
        {
            var freezerRecordList = _freezerRecordService.GetAll();
            return freezerRecordList.Select(Mapper.Map<tFreezerRecord, FreezerRecord>).ToList();
        }
        #endregion

        #region Add/Edit
        /// <summary>
        /// Update freezer record if found else create new
        /// </summary>
        /// <param name="freezerRecordModel"></param>
        /// <returns></returns>
        public virtual ActionResult Post(FreezerRecord freezerRecordModel)
        {
            try
            {
                // ReSharper disable once InconsistentNaming
                string AMPM = DateTime.Now.ToString("tt", CultureInfo.InvariantCulture);
                //Edit
                if (freezerRecordModel.iFreezerRecordId > 0)
                {
                    var savediFreezerRecord = _freezerRecordService.GetById(freezerRecordModel.iFreezerRecordId);
                    if (AMPM == "PM")
                    {
                        if (freezerRecordModel.vGoodsInFreezerTempAm == null)
                        {
                            savediFreezerRecord.vGoodsInFreezerTempPm = freezerRecordModel.vGoodsInFreezerTempPm;
                            savediFreezerRecord.vShapesFreezerTempPm = freezerRecordModel.vShapesFreezerTempPm;
                            savediFreezerRecord.vCarParkFreezerTempPm = freezerRecordModel.vCarParkFreezerTempPm;
                        }
                        else
                        {
                            savediFreezerRecord.vGoodsInFreezerTempPm = freezerRecordModel.vGoodsInFreezerTempAm;
                            savediFreezerRecord.vShapesFreezerTempPm = freezerRecordModel.vShapesFreezerTempAm;
                            savediFreezerRecord.vCarParkFreezerTempPm = freezerRecordModel.vCarParkFreezerTempAm;
                            savediFreezerRecord.vGoodsInFreezerTempAm = null;
                            savediFreezerRecord.vShapesFreezerTempAm = null;
                            savediFreezerRecord.vCarParkFreezerTempAm = null;
                        }
                        savediFreezerRecord.tGoodsInFreezerTimePm = DateTime.Now;
                        savediFreezerRecord.tShapesFreezerTimePm = DateTime.Now;
                        savediFreezerRecord.tCarParkFreezerTimePm = DateTime.Now;
                    }
                    else
                    {
                        if (freezerRecordModel.vGoodsInFreezerTempPm == null)
                        {
                            savediFreezerRecord.vGoodsInFreezerTempAm = freezerRecordModel.vGoodsInFreezerTempAm;
                            savediFreezerRecord.vShapesFreezerTempAm = freezerRecordModel.vShapesFreezerTempAm;
                            savediFreezerRecord.vCarParkFreezerTempAm = freezerRecordModel.vCarParkFreezerTempAm;
                        }
                        else
                        {
                            savediFreezerRecord.vGoodsInFreezerTempAm = freezerRecordModel.vGoodsInFreezerTempPm;
                            savediFreezerRecord.vShapesFreezerTempAm = freezerRecordModel.vShapesFreezerTempPm;
                            savediFreezerRecord.vCarParkFreezerTempAm = freezerRecordModel.vCarParkFreezerTempPm;
                            savediFreezerRecord.vGoodsInFreezerTempPm = null;
                            savediFreezerRecord.vShapesFreezerTempPm = null;
                            savediFreezerRecord.vCarParkFreezerTempPm = null;
                        }
                        savediFreezerRecord.tGoodsInFreezerTimeAm = DateTime.Now;
                        savediFreezerRecord.tShapesFreezerTimeAm = DateTime.Now;
                        savediFreezerRecord.tCarParkFreezerTimeAm = DateTime.Now;
                    }
                    savediFreezerRecord.vComment = freezerRecordModel.vComment;
                    savediFreezerRecord.Sys_ModifyBy = User.Identity.GetUserId();
                    savediFreezerRecord.Sys_ModifyDateTime = DateTime.Now;
                    _freezerRecordService.Update(savediFreezerRecord);
                    AuditScope.CreateAndSave("Editing Freezer Record", new { User = User.Identity.GetUserId() });
                }
                //Add
                else
                {
                    freezerRecordModel.Sys_CreatedBy = User.Identity.GetUserId();
                    freezerRecordModel.Sys_CreatedDateTime = DateTime.Now;
                    if (AMPM == "PM")
                    {
                        freezerRecordModel.tGoodsInFreezerTimePm = DateTime.Now;
                        freezerRecordModel.tShapesFreezerTimePm = DateTime.Now;
                        freezerRecordModel.tCarParkFreezerTimePm = DateTime.Now;
                    }
                    else
                    {
                        freezerRecordModel.tGoodsInFreezerTimeAm = DateTime.Now;
                        freezerRecordModel.tShapesFreezerTimeAm = DateTime.Now;
                        freezerRecordModel.tCarParkFreezerTimeAm = DateTime.Now;
                    }

                    _freezerRecordService.Create(Mapper.Map<FreezerRecord, tFreezerRecord>(freezerRecordModel));
                    AuditScope.CreateAndSave("Adding Freezer Record", new { User = User.Identity.GetUserId() });
                }
                return RedirectToAction(MVC.FreezerRecord.FreezerRecords());
            }
            catch (Exception ex)
            {
                AuditScope.CreateAndSave("Error Saving Freezer Record", new { User = User.Identity.GetUserId(), Exception = ex.Message });
                return RedirectToAction(MVC.Home.Index());
            }
        }
        #endregion

        #region Print Record as Report

        /// <summary>
        /// Print Record
        /// </summary>
        /// <param name="pId">Id of the record to print</param>
        /// <returns>RDLC Report View</returns>
        public virtual ActionResult PrintRecord(Int32 pId)
        {
            try
            {
                return View("~/Views/Report/ReportPreview.aspx", new Report { ReportName = PkReportNames.FreezerRecord, Id = pId, Service = _freezerRecordService });
            }
            catch (Exception ex)
            {
                AuditScope.CreateAndSave("Error getting report of Freezer Record", new { User = User.Identity.GetUserId() });
                // ReSharper disable once PossibleIntendedRethrow
                throw ex;
            }
        }
        #endregion
    }
}