﻿using AutoMapper;
using Microsoft.AspNet.Identity;
using PunjabKitchen.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web.Mvc;
using Audit.Core;
using PunjabKitchenBLL.Interfaces;
using PunjabKitchenEntities.EntityModel;
using System.Security.Principal;
using PunjabKitchenEntities.Common;

namespace PunjabKitchen.Controllers
{
    [Authorize]
    public partial class NotificationsController : BaseController
    {
        #region Data Members
        readonly INotificationService _notificationService;
        readonly IShoeCleaningCheckService _shoeCleaningCheckService;
        private readonly IGoodsOutFreezerUnitService _goodsOutFreezerUnit;
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="notificationService"></param>
        /// <param name="shoeCleaningCheckService"></param>
        /// <param name="goodsOutFreezerUnit"></param>
        public NotificationsController(INotificationService notificationService, IShoeCleaningCheckService shoeCleaningCheckService, IGoodsOutFreezerUnitService goodsOutFreezerUnit)
        {
            _notificationService = notificationService;
            _shoeCleaningCheckService = shoeCleaningCheckService;
            _goodsOutFreezerUnit = goodsOutFreezerUnit;
        }

        #endregion

        #region Get Notifications List 
        /// <summary>
        /// GET: Notifications
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public virtual ActionResult Index()
        {
            return View(GetUserNotifications());
        }
        #endregion

        #region Create Notification
        /// <summary>
        /// Create New Notification 
        /// </summary>
        /// <returns></returns>
        public virtual ActionResult Create()
        {
            return PartialView();
        }
        #endregion

        #region Save New Notification
        /// <summary>
        /// Save Notification
        /// </summary>
        /// <param name="notificationModel"></param>
        /// <returns></returns>
        [HttpGet]
        public virtual JsonResult CreateNotification(Notification notificationModel)
        {
            try
            {
                var config = new MapperConfiguration(cfg =>
                {
                    cfg.CreateMap<Notification, tNotification>();
                });
                IMapper mapper = config.CreateMapper();
                tNotification notification = mapper.Map<Notification, tNotification>(notificationModel);

                if (notification.Sys_CreatedBy == null)
                {
                    notification.Sys_CreatedBy = User.Identity.GetUserId();
                    notification.Sys_CreatedDateTime = DateTime.Now;
                }
                notification.Sys_ModifyBy = User.Identity.GetUserId();
                notification.Sys_ModifyDateTime = DateTime.Now;
                notification.bEnabled = true;
                //Setting Notification type
                if (User.IsInRole("Admin"))
                {
                    notification.iNotificationTypeId = GetNotifiationTypeId(notificationModel.vProcess);
                }
                _notificationService.Create(notification);
                AuditScope.CreateAndSave("Saving Notification", new { User = User.Identity.GetUserId() });
            }
            // ReSharper disable once UnusedVariable
            catch (Exception ex)
            {
                AuditScope.CreateAndSave("Saving Notification Error", new { User = User.Identity.GetUserId() });
                throw;
            }

            if (User.IsInRole("Admin"))
            {
                return Json(new { role = "Admin" }, JsonRequestBehavior.AllowGet);
            }

            //if the Notification is created from the Picklist.
            return Json(new { role = "Staff" }, JsonRequestBehavior.AllowGet);
        }

        [NonAction]
        public bool CreateNotification(Notification notificationModel, IPrincipal pUser)
        {
            try
            {
                var config = new MapperConfiguration(cfg =>
                {
                    cfg.CreateMap<Notification, tNotification>();
                });
                IMapper mapper = config.CreateMapper();
                tNotification notification = mapper.Map<Notification, tNotification>(notificationModel);

                if (notification.Sys_CreatedBy == null)
                {
                    notification.Sys_CreatedBy = pUser.Identity.GetUserId();
                    notification.Sys_CreatedDateTime = DateTime.Now;
                }
                notification.Sys_ModifyBy = pUser.Identity.GetUserId();
                notification.Sys_ModifyDateTime = DateTime.Now;
                notification.bEnabled = true;
                //Setting Notification type
                if (pUser.IsInRole("Admin"))
                {
                    notification.iNotificationTypeId = GetNotifiationTypeId(notificationModel.vProcess);
                }
                var context = new PunjabKitchenContext();
                context.Notifications.Add(notification);
                context.SaveChanges();
                AuditScope.CreateAndSave("Saving Notification", new { User = pUser.Identity.GetUserId() });
                return true;
            }
            // ReSharper disable once UnusedVariable
            catch (Exception ex)
            {
                AuditScope.CreateAndSave("Saving Notification Error", new { User = pUser.Identity.GetUserId() });
            }
            return false;
        }

        private int GetNotifiationTypeId(string processName)
        {
            switch (processName.ToLower())
            {
                case "boxing":
                    return 11;
                case "goods in":
                    return 6;
                case "stock take":
                    return 7;
                case "cooking":
                    return 8;
                case "goods out":
                    return 9;
                case "packing":
                    return 10;
                default:
                    return 1;
            }
        }

        #endregion

        #region Delete Notification
        /// <summary>
        /// Delete Notification
        /// </summary>
        /// <param name="notificationId"></param>
        /// <returns></returns>
        public virtual ActionResult Delete(int notificationId)
        {
            try
            {
                tNotification notification = _notificationService.GetById(notificationId);
                notification.Sys_ModifyBy = User.Identity.GetUserId();
                notification.Sys_ModifyDateTime = DateTime.Now;
                notification.bEnabled = true;
                notification.bDeleted = true;

                _notificationService.Delete(notification);

                AuditScope.CreateAndSave("Deleting Notification", new { User = User.Identity.GetUserId() });
            }
            // ReSharper disable once UnusedVariable
            catch (Exception ex)
            {
                AuditScope.CreateAndSave("Error Deleting Notification", new { User = User.Identity.GetUserId() });
                throw;
            }

            return RedirectToAction(MVC.Notifications.Index());
        }
        #endregion

        #region Get Notifications Count
        /// <summary>
        /// Get Notifications Count
        /// </summary>
        /// <returns>Int Notification Count</returns>
        [HttpGet]
        public virtual JsonResult GetNotificationsCount()
        {
            var notifications = GetUserNotifications();
            return Json(new
                            {
                                count = notifications.Count,
                                userid = User.Identity.GetUserId(),
                                ids = notifications.Select(s => s.iNotificationId)
                            }, 
                            JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Private: Get Users Notifications
        /// <summary>
        /// Method to get Notifications according to current user and user's roles
        /// </summary>
        /// <returns>List of Notifications</returns>
        private List<Notification> GetUserNotifications()
        {
            //System.Media.SoundPlayer player = new System.Media.SoundPlayer(@"c:\mywavfile.wav");
            //player.Play();
            //todo replace with user session
            ApplicationDbContext mycontext = new ApplicationDbContext();
            Microsoft.AspNet.Identity.EntityFramework.UserStore<ApplicationUser> mystore = new Microsoft.AspNet.Identity.EntityFramework.UserStore<ApplicationUser>(mycontext);
            ApplicationUserManager userMgr = new ApplicationUserManager(mystore);
            var roles = ((ClaimsIdentity)User.Identity).Claims
                 .Where(c => c.Type == ClaimTypes.Role)
                 .Select(c => c.Value).ToList();

            var list = _notificationService.GetAll().ToList();
            //todo : Optimize to get count from db and remove this loop

            var notificationList = new List<Notification>();
            foreach (var item in list)
            {
                if (roles.Any(x => x.Contains(item.vProcess))
                    && item.vSubject.Equals("Goods Out Freezer Unit Check") && CanPerformGoodsOutFreezerUnitCheck())
                {
                    if (item.Sys_CreatedBy.ToLower() != "system")
                    {
                        var user = userMgr.FindById(item.Sys_CreatedBy);
                        var fullName = user.FirstName + " " + user.LastName;
                        item.Sys_CreatedBy = fullName;
                    }
                    else
                    {
                        item.Sys_CreatedBy = "System";
                    }
                    Notification model = Mapper.Map<tNotification, Notification>(item);
                    notificationList.Add(model);
                }
                else if (roles.Any(x => x.Contains(item.vProcess))
                    && item.iWorkStarterId == null
                    && (((item.iNotificationTypeId == 2 || item.iNotificationTypeId == 3 || item.iNotificationTypeId == 4 || item.iNotificationTypeId == 11 || item.iNotificationTypeId == 12 || item.iNotificationTypeId == 13)
                        && DateTime.Parse(item.Sys_CreatedDateTime.ToString()).Date == DateTime.Now.Date)//Checking Daily Checks
                    || (item.iNotificationTypeId == 5 && DateTime.Parse(item.Sys_CreatedDateTime.ToString()).Date.AddDays(7) > DateTime.Now.Date)//Checking Weekly Checks
                    || (item.iNotificationTypeId != 2 && item.iNotificationTypeId != 3 && item.iNotificationTypeId != 4 && item.iNotificationTypeId != 5 && item.iNotificationTypeId != 11 && item.iNotificationTypeId != 12 && item.iNotificationTypeId != 13))
                    )
                {
                    if (item.Sys_CreatedBy.ToLower() != "system")
                    {
                        var user = userMgr.FindById(item.Sys_CreatedBy);
                        var fullName = user.FirstName + " " + user.LastName;
                        item.Sys_CreatedBy = fullName;
                    }
                    else
                    {
                        item.Sys_CreatedBy = "System";
                    }
                    Notification model = Mapper.Map<tNotification, Notification>(item);
                    //check if current record is shoe cleaning record and current user already not checked this
                    if (item.iNotificationTypeId != 5)
                    {
                        notificationList.Add(model);
                    }
                    else
                    {
                        // ReSharper disable once EmptyStatement
                        if (!CheckWeeklyRecord(DateTime.Parse(item.Sys_CreatedDateTime.ToString()).Date, User.Identity.GetUserId())) { notificationList.Add(model); };
                    }
                }
            }            

            return notificationList;
        }
        #endregion

        #region Check Weekly Shoe Cleaning Record

        /// <summary>
        /// Check Weekly Shoe Cleaning Record
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        private bool CheckWeeklyRecord(DateTime startDate, string userId)
        {
            return _shoeCleaningCheckService.CheckWeeklyRecord(startDate, userId);
        }

        #endregion

        #region  Assign Notification

        /// <summary>
        /// Assign Notification
        /// </summary>
        /// <param name="notificationId"></param>
        /// <returns></returns>
        [HttpPost]
        public virtual JsonResult AssignNotification(int notificationId)
        {
            if (!_notificationService.CanAssignNotificationToUser(notificationId)) return Json(new { result = false }, JsonRequestBehavior.AllowGet);
            AssignNotificationToUser(notificationId);
            return Json(new { result = true }, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Private: Assign Notification To User

        /// <summary>
        /// Private: Assign Notification To User
        /// </summary>
        /// <param name="notificationId"></param>
        private void AssignNotificationToUser(int notificationId)
        {
            var dbNotification = _notificationService.GetById(notificationId);
            try
            {
                AuditScope.CreateAndSave("Assigning User Notification to User",
                    new { User = User.Identity.GetUserId(), NotifiactionId = notificationId });
                var userId = User.Identity.GetUserId();
                dbNotification.iWorkStarterId = userId;
                dbNotification.Sys_ModifyBy = userId;
                dbNotification.Sys_ModifyDateTime = DateTime.Now;
                _notificationService.Update(dbNotification);
                AuditScope.CreateAndSave("Successfully Assigned User Notification to User",
                    new { User = User.Identity.GetUserId(), NotifiactionId = notificationId });
            }
            catch (Exception)
            {
                AuditScope.CreateAndSave("Error in Assigning User Notification to User",
                    new { User = User.Identity.GetUserId(), NotifiactionId = notificationId });
            }
        }

        #endregion

        #region Get Latest Notification ID
        /// <summary>
        /// Get Latest Notification ID
        /// </summary>
        /// <param name="notificationTypeID"></param>
        // ReSharper disable once InconsistentNaming
        public virtual JsonResult GetLatestNotificationId(int notificationTypeID)
        {
            //Getting the latest notification record.
            var notificationRecord = _notificationService.GetLatestNotificationByType(notificationTypeID);

            //If no such notification record exists
            if (notificationRecord == null || (notificationTypeID == 5 && !CanPerformShoeCleaningCheck()))
            {
                return Json(0, JsonRequestBehavior.AllowGet);
            }

            return Json(notificationRecord.iNotificationId, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Check Shoes Cleaning Already Performed
        //Function to check either user already performed shoes cleaning check or not
        public virtual JsonResult CanPerformShoesCleaningCheck()
        {
            var userId = User.Identity.GetUserId();
            DateTime weekStartDate = DateTime.Now.StartOfWeek(DayOfWeek.Monday);
            return Json(new { result = _shoeCleaningCheckService.IsCheckPerformed(weekStartDate, weekStartDate.AddDays(7), userId) }, JsonRequestBehavior.AllowGet);
        }
        public virtual bool CanPerformShoeCleaningCheck()
        {
            var userId = User.Identity.GetUserId();
            DateTime weekStartDate = DateTime.Now.StartOfWeek(DayOfWeek.Monday);
            return _shoeCleaningCheckService.IsCheckPerformed(weekStartDate, weekStartDate.AddDays(7), userId);
        }
        #endregion

        #region Can Perform Goods Out Freezer Unit Check
        /// <summary>
        /// Can Perform Goods Out Freezer Unit Check
        /// </summary>
        /// <returns>bool</returns>
        public virtual bool CanPerformGoodsOutFreezerUnitCheck()
        {
            var result = true;
            var record = _goodsOutFreezerUnit.GetAll().OrderByDescending(x => x.Sys_CreatedDateTime).FirstOrDefault();

            // ReSharper disable once PossibleInvalidOperationException
            if ((record != null) && (record.Sys_CreatedDateTime.Value.ToString("d/M/yyyy") == DateTime.Now.ToString("d/M/yyyy")))
            {
                //Checking if the check has already been filled.
                if (((GetTimePeriod() == ClockInOutPeriod.Morning) && (record.Sys_CreatedDateTime.Value.Hour < 12)) ||
                    ((GetTimePeriod() == ClockInOutPeriod.Lunch) && (record.Sys_CreatedDateTime.Value.Hour > 12)))
                {
                    result = false;
                }
            }
            else if ((record != null) && (record.Sys_CreatedDateTime.Value.ToString("d/M/yyyy") != DateTime.Now.ToString("d/M/yyyy")))
            {
                result = false;
            }

            return result;
        }
        #endregion

        #region Check Time Period for Goods Out Freezer Unit Check
        /// <summary>
        /// Check Time Period for Goods Out Freezer Unit Check
        /// </summary>
        /// <returns>bool</returns>
        private ClockInOutPeriod GetTimePeriod()
        {
            var isPm = (DateTime.Now.Hour >= 12);
            return isPm ? ClockInOutPeriod.Lunch : ClockInOutPeriod.Morning;
        }
        #endregion
    }
}