﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Audit.Core;
using AutoMapper;
using Microsoft.AspNet.Identity;
using PunjabKitchen.Models;
using PunjabKitchenBLL.Interfaces;
using PunjabKitchenEntities.EntityModel;
using PagedList;

namespace PunjabKitchen.Controllers
{
    [Authorize(Roles = "Admin,Packing Manager,Packing  Staff")]
    public partial class MetalDetectionAndPackWeightController : BaseController
    {
        #region Data Members
        readonly IFMDetectionAndPackWeightRecordService _fMDetectionAndPackingWeightService;
        readonly ICookingPlanService _cookingPlanService;
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="fMDetectionAndPackingWeightService"></param>
        /// <param name="cookingPlanService"></param>
        public MetalDetectionAndPackWeightController(IFMDetectionAndPackWeightRecordService fMDetectionAndPackingWeightService, ICookingPlanService cookingPlanService)
        {
            _fMDetectionAndPackingWeightService = fMDetectionAndPackingWeightService;
            _cookingPlanService = cookingPlanService;
        }
        #endregion

        #region Create New Metal Detection And Pack Weight Record
        /// <summary>
        /// Create New Metal Detection And Pack Weight Record
        /// </summary>
        /// <returns></returns>
        public virtual ActionResult CreateMetalDetectionAndPackWeightRecord()
        {
            return View(Views.Create, new FMDetectionAndPackWeightTest());
        }
        #endregion

        #region Get Metal Detection And Pack Weight Records List 
        /// <summary>
        /// Get Metal Detection And Pack Weight Records List 
        /// </summary>
        /// <param name="page"></param>
        /// <returns></returns>
        [HttpGet]
        public virtual ActionResult MetalDetectionAndPackWeightRecords(int? page = 1)
        {
            var recordsList = GetMetalDetectionAndPackWeightRecords().OrderByDescending(x => x.iDetectionPackWeightTestId);
            var pageNumber = page ?? 1;
            return View(Views.Index, recordsList.ToPagedList(pageNumber, GetPageSize()));
        }
        #endregion

        #region Return Metal Detection And Pack Weight Records Create New View
        /// <summary>
        /// Return Metal Detection And Pack Weight Records Create New View
        /// </summary>
        /// <returns></returns>
        public virtual ActionResult CreateMetalDetectionAndPackWeightRecordCreateView()
        {
            return PartialView(Views.Create);
        }
        #endregion

        #region Return Metal Detection And Pack Weight Record Edit View
        /// <summary>
        /// Return Metal Detection And Pack Weight Record Edit View
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize(Roles = "Admin,Packing Manager")]
        public virtual ActionResult EditMetalDetectionAndPackWeightRecord(long id)
        {
            FMDetectionAndPackWeightTest metalDetectionAndPackWeightRec = Mapper.Map<tFMDetectionAndPackWeightTest, FMDetectionAndPackWeightTest>(_fMDetectionAndPackingWeightService.GetById(id));
            return View(Views.Create, metalDetectionAndPackWeightRec);
        }
        #endregion

        #region  Get All Metal Detection And Pack Weight Records
        /// <summary>
        /// Get All Metal Detection And Pack Weight Records
        /// </summary>
        /// <returns></returns>
        private List<FMDetectionAndPackWeightTest> GetMetalDetectionAndPackWeightRecords()
        {
            var metalDetectionAndPackWeightRecordList = _fMDetectionAndPackingWeightService.GetAll().Where(m => m.vTestType == "Metal Detection Test and Pack Weight Check");
            return metalDetectionAndPackWeightRecordList.Select(Mapper.Map<tFMDetectionAndPackWeightTest, FMDetectionAndPackWeightTest>).ToList();
        }
        #endregion

        #region Add/Edit
        /// <summary>
        /// Update Metal Detection And Pack Weight Record if found else create new
        /// </summary>
        /// <param name="metalDetectionAndPackWeightRecord"></param>
        /// <returns></returns>
        public virtual ActionResult PostMetalDetectionAndPackWeightRecord(FMDetectionAndPackWeightTest metalDetectionAndPackWeightRecord)
        {
            try
            {
                //Edit
                if (metalDetectionAndPackWeightRecord.iDetectionPackWeightTestId > 0)
                {
                    var savedMetalDetectionAndPackWeightRecord = Mapper.Map<FMDetectionAndPackWeightTest, tFMDetectionAndPackWeightTest>(metalDetectionAndPackWeightRecord);
                    savedMetalDetectionAndPackWeightRecord.Sys_ModifyBy = User.Identity.GetUserId();
                    savedMetalDetectionAndPackWeightRecord.Sys_ModifyDateTime = DateTime.Now;
                    // ReSharper disable once PossibleInvalidOperationException
                    savedMetalDetectionAndPackWeightRecord.tCookingPlan = _cookingPlanService.GetById((long)metalDetectionAndPackWeightRecord.iCookingPlanId);
                    savedMetalDetectionAndPackWeightRecord.vTestType = "Metal Detection Test and Pack Weight Check";
                    _fMDetectionAndPackingWeightService.Update(savedMetalDetectionAndPackWeightRecord);
                    AuditScope.CreateAndSave("Editing Metal Detection And Pack Weight Record", new { User = User.Identity.GetUserId() });
                }
                //Add
                else
                {
                    metalDetectionAndPackWeightRecord.Sys_CreatedBy = User.Identity.GetUserId();
                    metalDetectionAndPackWeightRecord.Sys_CreatedDateTime = DateTime.Now;
                    metalDetectionAndPackWeightRecord.tCookingPlan = null;
                    metalDetectionAndPackWeightRecord.vTestType = "Metal Detection Test and Pack Weight Check";
                    try
                    {
                        _fMDetectionAndPackingWeightService.Create(Mapper.Map<FMDetectionAndPackWeightTest, tFMDetectionAndPackWeightTest>(metalDetectionAndPackWeightRecord));
                        AuditScope.CreateAndSave("Adding Metal Detection And Pack Weight Record", new { User = User.Identity.GetUserId() });
                    }
                    catch (Exception ex)
                    {
                        AuditScope.CreateAndSave("Error Saving Metal Detection And Pack Weight Record", new { User = User.Identity.GetUserId(), Exception = ex.Message });
                    }
                }
                return RedirectToAction(MVC.MetalDetectionAndPackWeight.MetalDetectionAndPackWeightRecords());
            }
            catch (Exception ex)
            {
                AuditScope.CreateAndSave("Error Saving Metal Detection And Pack Weight Record", new { User = User.Identity.GetUserId(), Exception = ex.Message });
                return RedirectToAction(MVC.Home.Index());
            }
        }
        #endregion

        #region Print Record as Report

        /// <summary>
        /// Print Record
        /// </summary>
        /// <param name="pId">Id of the record to print</param>
        /// <returns>RDLC Report View</returns>
        public virtual ActionResult PrintRecord(Int32 pId)
        {
            try
            {
                return View("~/Views/Report/ReportPreview.aspx", new Report { ReportName = PkReportNames.MetalDetectionAndPackWeightRecord, Id = pId, Service = _fMDetectionAndPackingWeightService });
            }
            catch (Exception ex)
            {
                AuditScope.CreateAndSave("Error getting report of Metal Detection And Pack Weight Record", new { User = User.Identity.GetUserId() });
                // ReSharper disable once PossibleIntendedRethrow
                throw ex;
            }
        }
        #endregion
    }
}