﻿using Audit.Core;
using AutoMapper;
using Microsoft.AspNet.Identity;
using PunjabKitchen.Models;
using PunjabKitchenBLL.Interfaces;
using PunjabKitchenEntities.EntityModel;
using System;
using System.Collections.Generic;
using System.Linq;
using PagedList;
using System.Web.Mvc;

namespace PunjabKitchen.Controllers
{
    [Authorize(Roles = "Admin,Goods Out Staff,Goods Out Manager")]
    public partial class GoodsOutPlanController : BaseController
    {
        #region Data Members
        readonly IGoodsOutPlanService _goodsOutPlanService;
        readonly IGoodsOutPlanDetailService _goodsOutPlanDetailService;
        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="goodsOutPlanService"></param>
        /// <param name="goodsOutPlanDetailService"></param>
        public GoodsOutPlanController(IGoodsOutPlanService goodsOutPlanService, IGoodsOutPlanDetailService goodsOutPlanDetailService)
        {
            _goodsOutPlanService = goodsOutPlanService;
            _goodsOutPlanDetailService = goodsOutPlanDetailService;
        }

        #endregion

        #region Get Goods Out List 
        /// <summary>
        /// GET: Deliveries
        /// </summary>
        /// <param name="page"></param>
        /// <returns></returns>
        [HttpGet]
        public virtual ActionResult Index(int? page)
        {
            var goodsOutPlans = _goodsOutPlanService.GetAll().OrderByDescending(x => x.iGoodsOutPlanId);
            var pageNumber = page ?? 1;
            var goodsOutPlansList = goodsOutPlans.Select(item => Mapper.Map<tGoodsOutPlan, GoodsOutPlan>(item)).ToList();
            return View(Views.Index, goodsOutPlansList.ToPagedList(pageNumber, GetPageSize()));
        }

        #endregion

        #region Create New Plan
        /// <summary>
        /// New Goods Out Plan
        /// </summary>
        /// <returns></returns>
        public virtual ActionResult Create()
        {
            var newPlan = new GoodsOutPlan();
            newPlan.tGoodsOutPlanDetails = new List<GoodsOutPlanDetail>();
            newPlan.tGoodsOutPlanDetails.Add(new GoodsOutPlanDetail());
            return PartialView(Views._Create, newPlan);
        }

        #endregion

        #region Edit Goods Out Plan
        /// <summary>
        /// Edit Goods Out Plan
        /// </summary>
        /// <returns></returns>
        public virtual ActionResult Edit(long id)
        {
            var goodsOutPlan = Mapper.Map<tGoodsOutPlan, GoodsOutPlan>(_goodsOutPlanService.GetById(id));

            return PartialView(Views._Create, goodsOutPlan);
        }

        #endregion

        #region Post: Goods Out Plan Record
        /// <summary>
        /// Post: Goods Out Plan Record
        /// </summary>
        /// <returns></returns>
        public virtual ActionResult Post(GoodsOutPlan goodsOutPlan)
        {
            try
            {
                //Edit
                if (goodsOutPlan.iGoodsOutPlanId > 0)
                {
                    goodsOutPlan.Sys_ModifyBy = User.Identity.GetUserId();
                    goodsOutPlan.Sys_ModifyDateTime = DateTime.Now;

                    EditGoodsOutPlan(Mapper.Map<GoodsOutPlan, tGoodsOutPlan>(goodsOutPlan));
                    AuditScope.CreateAndSave("Editing Goods Out Plan", new { User = User.Identity.GetUserId(), GoodsOutPlanId = goodsOutPlan.iGoodsOutPlanId });
                }
                //Add
                else
                {
                    //Check if batch code already assigned to any plan
                    var batchCodeAlreadyAssigned = _goodsOutPlanService.ValidateBatchCode(goodsOutPlan.vBatchCode);
                    if (batchCodeAlreadyAssigned)
                    {
                        goodsOutPlan.vBatchCode = _goodsOutPlanService.GetBatchCode();
                    }
                    goodsOutPlan.Sys_CreatedBy = User.Identity.GetUserId();
                    goodsOutPlan.Sys_CreatedDateTime = DateTime.Now;

                    var detail = goodsOutPlan.tGoodsOutPlanDetails.FirstOrDefault();
                    if (detail != null)
                    {
                        detail.Sys_CreatedBy = User.Identity.GetUserId();
                        detail.Sys_CreatedDateTime = DateTime.Now;
                    }

                    var savedGoodsOutPlan = AddGoodsOutPlan(Mapper.Map<GoodsOutPlan, tGoodsOutPlan>(goodsOutPlan));
                    goodsOutPlan.iGoodsOutPlanId = savedGoodsOutPlan.iGoodsOutPlanId;
                    AuditScope.CreateAndSave("Saving Goods Out Plan", new { User = User.Identity.GetUserId(), GoodsInId = goodsOutPlan.iGoodsOutPlanId });
                }

                return RedirectToAction(MVC.GoodsOutPlan.Index());
            }
            catch (Exception ex)
            {
                AuditScope.CreateAndSave("Error saving goods out plan record: ", new { User = User.Identity.GetUserId(), Exception = ex.Message });
                throw;
            }
        }

        #endregion

        #region Delete Detail Item
        /// <summary>
        /// Delete Addition Item
        /// </summary>
        /// <param name="iGoodsOutPlanDetailId"></param>
        /// <param name="iRemainingRecords"></param>
        /// <returns></returns>
        public virtual JsonResult DeleteDetailItem(int iGoodsOutPlanDetailId, int iRemainingRecords)
        {
            var deletedRecordID = 0;

            try
            {
                var recordtoDelete = _goodsOutPlanDetailService.GetById(iGoodsOutPlanDetailId);

                tGoodsOutPlan parentRecord = null;
                if (iRemainingRecords <= 2)
                    // ReSharper disable once PossibleInvalidOperationException
                    parentRecord = _goodsOutPlanService.GetById((long)recordtoDelete.iGoodsOutPlanId);

                if (recordtoDelete != null)
                {
                    _goodsOutPlanDetailService.Delete(recordtoDelete);

                    if (parentRecord != null)
                        _goodsOutPlanService.Delete(parentRecord);

                    AuditScope.CreateAndSave("Deleting Goods Out Plan Detail.", new { User = User.Identity.GetUserId() });
                    return Json(new { status = "Success", iGoodsOutPlanDetailId }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                AuditScope.CreateAndSave("Error deleting detail item in goods out plan: ", new { User = User.Identity.GetUserId(), Exception = ex.Message });
                throw;
            }
#pragma warning disable 162
            return Json(new { status = "Failed", deletedRecordID }, JsonRequestBehavior.AllowGet);
#pragma warning restore 162
        }
        #endregion

        #region Privates

        private void EditGoodsOutPlan(tGoodsOutPlan goodsOutPlan)
        {
            try
            {
                _goodsOutPlanService.Update(goodsOutPlan);
            }
            catch (Exception ex)
            {
                AuditScope.CreateAndSave("Error saving goods out plan record: ", new { User = User.Identity.GetUserId(), Exception = ex.Message });
                throw;
            }
        }

        private tGoodsOutPlan AddGoodsOutPlan(tGoodsOutPlan goodsOutPlan)
        {
            try
            {
                return _goodsOutPlanService.CreateRecord(goodsOutPlan);
            }
            catch (Exception ex)
            {
                AuditScope.CreateAndSave("Error saving goods out plan record: ", new { User = User.Identity.GetUserId(), Exception = ex.Message });
                throw;
            }
        }

        // ReSharper disable once UnusedMember.Local
        private List<GoodsOutPlan> GetAllGoodsOutPlans()
        {
            var plansList = _goodsOutPlanService.GetAll();
            return plansList.Select(Mapper.Map<tGoodsOutPlan, GoodsOutPlan>).ToList();
        }

        #endregion

        #region Print Record as Report

        /// <summary>
        /// Print Record
        /// </summary>
        /// <param name="pId">Id of the record to print</param>
        /// <returns>RDLC Report View</returns>
        public virtual ActionResult PrintRecord(Int32 pId)
        {
            try
            {
                return View("~/Views/Report/ReportPreview.aspx", new Report { ReportName = PkReportNames.GoodsOutPlanRecord, Id = pId, Service = _goodsOutPlanService });
            }
            catch (Exception ex)
            {
                AuditScope.CreateAndSave("Error getting report of Goods Out Plan", new { User = User.Identity.GetUserId() });
                // ReSharper disable once PossibleIntendedRethrow
                throw ex;
            }
        }
        #endregion
    }
}