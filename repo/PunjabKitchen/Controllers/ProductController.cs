﻿using System;
using PunjabKitchenBLL.Interfaces;
using System.Collections;
using System.Linq;
using System.Web.Mvc;

namespace PunjabKitchen.Controllers
{
    [Authorize]
    public partial class ProductController : BaseController
    {
        #region Data Members
        readonly IProductService _productService;
        #endregion

        #region Contructor
        /// <summary>
        /// Contructor
        /// </summary>
        /// <param name="productService"></param>
        /// <returns></returns>
        public ProductController(IProductService productService)
        {
            _productService = productService;
        }
        #endregion

        #region Get Product By Code
        /// <summary>
        /// Get Product By Code
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        [HttpPost]
        public virtual JsonResult GetProductByCode(string code)
        {
            var productList = _productService.GetByCode(code);
            productList = productList.Where(w => w.dCCP1aBestBefore != null);
            var productsList = new ArrayList();
            foreach (var product in productList)
            {
                //Checking if the product is older than 6 months.
                if ((DateTime.Now - product.dCCP1aBestBefore.Value).TotalDays <= 185)
                {
                    productsList.Add(new { id = product.iProductId, name = product.vName, description = product.vDescription, code = product.vProductCode });
                }
            }
            return Json(productsList, JsonRequestBehavior.AllowGet);
        }

        #endregion
    }
}