﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Web.Mvc;
using System.IO;
using System.Linq;
using System.Web;
using PunjabKitchen.Models;

namespace PunjabKitchen.Controllers
{
    public partial class HomeController : BaseController
    {
        #region Login Page
        /// <summary>
        /// Login Page
        /// </summary>
        /// <returns></returns>
        public virtual ActionResult Index()
        {
            if (User.Identity.IsAuthenticated)
            {
                return View();
            }
            return RedirectToAction(MVC.Account.Login());
        }
        #endregion

        #region About Page
        /// <summary>
        /// About Page
        /// </summary>
        /// <returns></returns>
        [Authorize]
        public virtual ActionResult About()
        {
            ViewBag.Message = "Your app description page.";

            return View();
        }
        #endregion

        #region Contact Page
        /// <summary>
        /// Contact Page
        /// </summary>
        /// <returns></returns>
        public virtual ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        #endregion

        #region User Photo
        /// <summary>
        /// User Photo
        /// </summary>
        /// <returns></returns>
        public virtual FileContentResult UserPhotos()
        {
            if (User.Identity.IsAuthenticated)
            {
                String userId = User.Identity.GetUserId();

                if (userId == null)
                {
                    string fileName = HttpContext.Server.MapPath(@"~/Images/noImg.png");

                    byte[] imageData = null;
                    FileInfo fileInfo = new FileInfo(fileName);
                    long imageFileLength = fileInfo.Length;
                    FileStream fs = new FileStream(fileName, FileMode.Open, FileAccess.Read);
                    BinaryReader br = new BinaryReader(fs);
                    imageData = br.ReadBytes((int)imageFileLength);

                    return File(imageData, "image/png");

                }
                // to get the user details to load user Image
                var bdUsers = HttpContext.GetOwinContext().Get<ApplicationDbContext>();
                var userImage = bdUsers.Users.FirstOrDefault(x => x.Id == userId);

                if (userImage != null && userImage.UserPhoto.Length > 0)
                {
                    return new FileContentResult(userImage.UserPhoto, "image/jpeg");
                }
                return UserDefaultPhoto();
            }
            return UserDefaultPhoto();
        }

        public virtual FileContentResult UserDefaultPhoto()
        {
            string fileName = HttpContext.Server.MapPath(@"~/Images/noImg.png");

            byte[] imageData = null;
            FileInfo fileInfo = new FileInfo(fileName);
            long imageFileLength = fileInfo.Length;
            FileStream fs = new FileStream(fileName, FileMode.Open, FileAccess.Read);
            BinaryReader br = new BinaryReader(fs);
            imageData = br.ReadBytes((int)imageFileLength);
            return File(imageData, "image/png");
        }
        #endregion
    }
}
