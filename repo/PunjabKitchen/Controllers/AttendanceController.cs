﻿using System;
using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using Microsoft.AspNet.Identity;
using PunjabKitchen.Models;
using PunjabKitchenBLL.Interfaces;
using PunjabKitchenEntities.EntityModel;
using PagedList;
using System.Collections.Generic;
using System.Web;
using Audit.Core;
using Microsoft.AspNet.Identity.Owin;
using PunjabKitchen.Models.RequestModels;
using PunjabKitchen.Models.ResponseModels;
// ReSharper disable All

namespace PunjabKitchen.Controllers
{
    #region Time Periods Enums
    enum ClockInOutPeriod
    {
        Morning,
        Lunch,
    };
    #endregion

    public partial class AttendanceController : BaseController
    {
        #region Data Members
        readonly IAttendanceService _attendanceService;
        #endregion

        #region User Manager
        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            // ReSharper disable once UnusedMember.Local
            private set
            {
                _userManager = value;
            }
        }

        private ApplicationRoleManager _roleManager;
        private bool _b;

        public ApplicationRoleManager RoleManager
        {
            get
            {
                return _roleManager ?? HttpContext.GetOwinContext().Get<ApplicationRoleManager>();
            }
            // ReSharper disable once UnusedMember.Local
            private set
            {
                _roleManager = value;
            }
        }
        #endregion

        #region Constructor
        public AttendanceController(IAttendanceService attendanceService)
        {
            _attendanceService = attendanceService;
        }
        #endregion

        #region Private: Generate Employee Attendances
        private IEnumerable<tEmployeeAttendance> GeneratEmployeeAttendances(PagedClientViewModel viewModel)
        {
            var dbList = _attendanceService.GetAllRecordsById(User.Identity.GetUserId()).ToList();
            var daysCount = (viewModel.ToDate - viewModel.FromDate).TotalDays;
            var currentDate = viewModel.FromDate;
            for (int i = 0; i < daysCount; i++)
            {
                if (!dbList.Any(x => x.dDateTime.Date == currentDate.Date))
                {
                    dbList.Add(new tEmployeeAttendance { dDateTime = currentDate });
                }
                currentDate = currentDate.AddDays(1);
            }
            return dbList.OrderByDescending(x => x.dDateTime);
        }
        #endregion

        #region Get Employee Attendances List
        public virtual ActionResult Index(PagedClientViewModel pagedClientViewModel)
        {
            if (pagedClientViewModel.FromDate == DateTime.MinValue)
            {
                //pagedClientViewModel.FromDate = DateTime.Now.AddMonths(-1);
                pagedClientViewModel.FromDate = DateTime.Now.AddDays(-15);
                pagedClientViewModel.ToDate = DateTime.Now;
            }
            //var attendanceRecords = _attendanceService.GetAllRecordsById(User.Identity.GetUserId()).ToList().OrderByDescending(x => x.dDateTime);
            var attendanceRecords = GeneratEmployeeAttendances(pagedClientViewModel);

            var pageNumber = pagedClientViewModel.Page ?? 1;

            var attendanceRecordsList = attendanceRecords.Select(item => Mapper.Map<tEmployeeAttendance, EmployeeAttendance>(item)).ToList();

            pagedClientViewModel.Attendances =
                (PagedList<EmployeeAttendance>)attendanceRecordsList.ToPagedList(pageNumber, GetPageSize());

            return View(Views.Index, pagedClientViewModel);
        }
        #endregion

        #region Get Staff Attendance List
        public virtual ActionResult StaffAttendance(PagedClientViewModel viewModel)
        {
            if (viewModel.FromDate == DateTime.MinValue)
            {
                //viewModel.FromDate = DateTime.Now.AddMonths(-1);
                viewModel.FromDate = DateTime.Now.AddDays(-15);
                viewModel.ToDate = DateTime.Now;
            }

            var dbList = _attendanceService.GetAll().ToList();
            var daysCount = (viewModel.ToDate - viewModel.FromDate).TotalDays;
            //var daysCount = 1;
            var currentDate = viewModel.FromDate;
            //All users List
            var context = new ApplicationDbContext();
            var allUsers = context.Users.ToList();

            for (int i = 0; i <= daysCount; i++)
            {
                for (int j = 0; j < allUsers.Count; j++)
                {
                    if (!dbList.Any(x => x.dDateTime.Date == currentDate.Date && x.iEmployeeId == allUsers[j].Id))
                    {
                        var user = UserManager.FindById(allUsers[j].Id);
                        dbList.Add(new tEmployeeAttendance
                        {
                            dDateTime = currentDate,
                            iEmployeeId = user.Id,
                            Sys_CreatedDateTime = currentDate,
                            AspNetUser = new AspNetUser { FirstName = user.FirstName, LastName = user.LastName }
                        });
                    }
                }

                currentDate = currentDate.AddDays(1);
            }

            var attendanceRecordsList = dbList.Select(item => Mapper.Map<tEmployeeAttendance, EmployeeAttendance>(item)).ToList().OrderByDescending(x => x.dDateTime);

            var pageNumber = viewModel.Page ?? 1;

            viewModel.Attendances =
                (PagedList<EmployeeAttendance>)attendanceRecordsList.ToPagedList(pageNumber, GetPageSize());

            return View(Views.StaffAttendance, viewModel);
        }
        #endregion

        #region Get Today's Attendance Details
        public virtual JsonResult GetTodaysAttendanceStatus()
        {
            var todaysAttendanceRecord = _attendanceService.GetTodaysRecordByUserId(User.Identity.GetUserId(), DateTime.Now);

            AttendanceStatus result;

            if ((todaysAttendanceRecord != null) && (todaysAttendanceRecord.dClockInTime == null) && (todaysAttendanceRecord.dLunchClockOutTime == null))
            {
                result = todaysAttendanceRecord != null
                    ? new AttendanceStatus { IsAlreadyCheckedIn = todaysAttendanceRecord.dClockInTime != null, IsAlreadyCheckedOut = todaysAttendanceRecord.dClockOutTime != null }
                    : new AttendanceStatus { IsAlreadyCheckedIn = false, IsAlreadyCheckedOut = false };
            }
            else
            {
                var dayPeriod = GetClockInOutPeriod();

                if (dayPeriod == ClockInOutPeriod.Morning)
                {
                    result = todaysAttendanceRecord != null
                        ? new AttendanceStatus { IsAlreadyCheckedIn = todaysAttendanceRecord.dClockInTime != null, IsAlreadyCheckedOut = todaysAttendanceRecord.dClockOutTime != null }
                        : new AttendanceStatus { IsAlreadyCheckedIn = false, IsAlreadyCheckedOut = false };
                }
                else
                {
                    result = todaysAttendanceRecord != null
                        ? new AttendanceStatus { IsAlreadyCheckedIn = todaysAttendanceRecord.dLunchClockInTime != null, IsAlreadyCheckedOut = todaysAttendanceRecord.dLunchClockOutTime != null }
                        : new AttendanceStatus { IsAlreadyCheckedIn = false, IsAlreadyCheckedOut = false };
                }
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region ClockIn
        public virtual JsonResult ClockIn()
        {
            var objToCreateUpdate = _attendanceService.GetTodaysRecordByUserId(User.Identity.GetUserId(), DateTime.Now);
            bool isNewObject = objToCreateUpdate == null;

            if (GetClockInOutPeriod() == ClockInOutPeriod.Morning)
            {
                if (isNewObject)
                {
                    _attendanceService.Create(GenerateAttendanceForMorning(true, null));
                }
                else
                {
                    _attendanceService.Update(GenerateAttendanceForMorning(true, objToCreateUpdate));
                }
            }
            else
            {
                if (isNewObject)
                {
                    _attendanceService.Create(GenerateAttendanceForLunch(true, null));
                }
                else if ((objToCreateUpdate.dClockInTime == null) && (objToCreateUpdate.dLunchClockOutTime == null))
                {
                    _attendanceService.Update(GenerateAttendanceForMorning(true, objToCreateUpdate));
                }
                else
                {
                    _attendanceService.Update(GenerateAttendanceForLunch(true, objToCreateUpdate));
                }
            }
            return Json(new { result = true }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region ClockOut
        public virtual JsonResult ClockOut()
        {
            var objToCreateUpdate = _attendanceService.GetTodaysRecordByUserId(User.Identity.GetUserId(), DateTime.Now);
            bool isNewObject = objToCreateUpdate == null;

            if (GetClockInOutPeriod() == ClockInOutPeriod.Morning)
            {
                if (isNewObject)
                {
                    _attendanceService.Create(GenerateAttendanceForMorning(false, null));
                }
                else
                {
                    _attendanceService.Update(GenerateAttendanceForMorning(false, objToCreateUpdate));
                }
            }
            else
            {
                if (isNewObject)
                {
                    _attendanceService.Create(GenerateAttendanceForLunch(false, null));
                }
                else
                {
                    _attendanceService.Update(GenerateAttendanceForLunch(false, objToCreateUpdate));
                }
            }
            return Json(new { result = true }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Generate Attendance for Morning
        private tEmployeeAttendance GenerateAttendanceForMorning(bool isClockIn, tEmployeeAttendance objToCreateUpdateParam)
        {
            var objToCreateUpdate = GetObjToAddUpdate(objToCreateUpdateParam);

            if (isClockIn)
            {
                objToCreateUpdate.dClockInTime = DateTime.Now;
            }
            else
            {
                objToCreateUpdate.dClockOutTime = DateTime.Now;
            }
            //Returning Object
            return objToCreateUpdate;
        }

        private tEmployeeAttendance GenerateAttendanceForLunch(bool isClockIn, tEmployeeAttendance objToCreateUpdateParam)
        {
            var objToCreateUpdate = GetObjToAddUpdate(objToCreateUpdateParam);

            if (isClockIn)
            {
                objToCreateUpdate.dLunchClockInTime = DateTime.Now;
            }
            else
            {
                objToCreateUpdate.dLunchClockOutTime = DateTime.Now;
            }
            //Returning Object
            return objToCreateUpdate;
        }
        #endregion

        #region Get: Edit View
        /// <summary>
        /// Get: Edit View
        /// </summary>
        /// <returns></returns>
        public virtual ActionResult EditAttendanceRecord(string iEmployeeId, DateTime Sys_CreatedDateTime)
        {
            var record = _attendanceService.GetRecordForEditing(iEmployeeId, Sys_CreatedDateTime);
            var attendanceRecord = Mapper.Map<tEmployeeAttendance, EmployeeAttendance>(record);
            var user = UserManager.Users.FirstOrDefault(x => x.Id == iEmployeeId);
            string username = user.FirstName + " " + user.LastName;
            attendanceRecord.EmployeeName = username;
            attendanceRecord.dDateTime = Sys_CreatedDateTime;
            return PartialView(Views.EditStaffAttendance, attendanceRecord);
        }
        #endregion

        #region Edit Attendance Record
        /// <summary>
        /// Edit Attendance Record
        /// </summary>
        /// <returns></returns>
        public virtual ActionResult SubmitAttendanceRecord(EmployeeAttendance employeeAttendance)
        {
            try
            {
                //var record = _attendanceService.GetRecordForEditing(employeeAttendance.iEmployeeId, employeeAttendance.dDateTime.Value);
                var record = _attendanceService.GetTodaysRecordByUserId(employeeAttendance.iEmployeeId, employeeAttendance.dDateTime);
                var isNew = record == null;
                record = record ?? new tEmployeeAttendance();

                record.dDateTime = employeeAttendance.dDateTime;
                record.dLunchClockInTime = employeeAttendance.dLunchClockInTime;
                record.dLunchClockOutTime = employeeAttendance.dLunchClockOutTime;
                record.dClockInTime = employeeAttendance.dClockInTime;
                record.dClockOutTime = employeeAttendance.dClockOutTime;
                if (isNew)
                {
                    record.iEmployeeId = employeeAttendance.iEmployeeId;
                    record.Sys_CreatedBy = User.Identity.GetUserId();
                    record.Sys_CreatedDateTime = DateTime.Now;
                    _attendanceService.Create(record);
                    AuditScope.CreateAndSave("Saving Attendance Record", new { User = User.Identity.GetUserId() });
                }
                else
                {
                    record.Sys_ModifyBy = User.Identity.GetUserId();
                    record.Sys_ModifyDateTime = DateTime.Now;
                    _attendanceService.Update(record);
                    AuditScope.CreateAndSave("Editing Attendance Record", new { User = User.Identity.GetUserId() });
                }
                
                return RedirectToAction(Views.StaffAttendance);
            }
            catch (Exception ex)
            {
                AuditScope.CreateAndSave("Error Modifying Attendance record: ", new { User = User.Identity.GetUserId(), Exception = ex.Message });
                return RedirectToAction(MVC.Home.Index());
            }
        }
        #endregion

        #region Get Time Period
        private ClockInOutPeriod GetClockInOutPeriod()
        {
            var isPm = (DateTime.Now.Hour >= 12);
            return isPm ? ClockInOutPeriod.Lunch : ClockInOutPeriod.Morning;
        }
        #endregion

        #region Get Attendance Record to update
        private tEmployeeAttendance GetObjToAddUpdate(tEmployeeAttendance objToCreateUpdate)
        {
            var isNewObject = objToCreateUpdate == null;
            objToCreateUpdate = objToCreateUpdate ?? new tEmployeeAttendance();

            if (isNewObject)
            {
                objToCreateUpdate.dDateTime = DateTime.Now;
                objToCreateUpdate.iEmployeeId = User.Identity.GetUserId();
                objToCreateUpdate.Sys_CreatedDateTime = DateTime.Now;
                objToCreateUpdate.Sys_CreatedBy = User.Identity.GetUserId();
            }
            else
            {
                objToCreateUpdate.Sys_ModifyDateTime = DateTime.Now;
                objToCreateUpdate.Sys_ModifyBy = User.Identity.GetUserId();
            }
            //Returning Object
            return objToCreateUpdate;
        }
        #endregion
    }
}