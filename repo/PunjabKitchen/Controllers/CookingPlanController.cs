﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Audit.Core;
using AutoMapper;
using Microsoft.AspNet.Identity;
using PunjabKitchen.Models;
using PunjabKitchenBLL.Interfaces;
using PunjabKitchenEntities.EntityModel;
using System.Collections;
using System.Data.Entity.Infrastructure;
using PagedList;
// ReSharper disable All

namespace PunjabKitchen.Controllers
{   

    [Authorize(Roles = "Admin,Cooking Staff,Cooking Manager,Goods In Staff,Goods In Manager")]
    public partial class CookingPlanController : BaseController
    {
        #region Data Members

        readonly ICookingPlanService _cookingPlanService;
        readonly IStockService _stockService;

        #endregion

        #region Constructor

        /// <summary>
        /// Construcotr
        /// </summary>
        /// <param name="cookingPlanService"></param>
        /// <param name="stockService"></param>
        public CookingPlanController(ICookingPlanService cookingPlanService, IStockService stockService)
        {
            _cookingPlanService = cookingPlanService;
            _stockService = stockService;
        }

        #endregion

        #region GET: CookingPlan

        /// <summary>
        /// GET: CookingPlan
        /// </summary>
        /// <param name="cookpotId"></param>
        /// <param name="page"></param>
        /// <returns></returns>
        public virtual ActionResult Index(int? cookpotId, int? page = 1)
        {
            var newCookPotId = cookpotId ?? (int)CookingKitchens.Cookpot_1;

            if (cookpotId == null)
            {
                ViewBag.Cookpot1 = true;
            }

            var cookingPlansList = GetCookingPlans(newCookPotId).OrderByDescending(x => x.iCookingPlanId);
            var pageNumber = page ?? 1;
            ViewBag.CookingKitchensList = CookingKitchensEnum.GetCookingKitchens();
            return View(Views.Index, cookingPlansList.ToPagedList(pageNumber, GetPageSize()));
        }

        #endregion

        #region GET: Create New Cooking Plan

        /// <summary>
        /// GET: Create New Cooking Plan
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "Admin")]
        public virtual ActionResult Create()
        {
            ViewBag.CookingKitchens = new SelectList(CookingKitchensEnum.GetCookingKitchens(), "value", "text");
            return PartialView(Views._Create, new CookingPlan());
        }

        #endregion

        #region Get: Edit Cooking Plan

        /// <summary>
        /// Get: Edit Cooking Plan
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize(Roles = "Admin,Cooking Manager")]
        public virtual ActionResult Edit(long id)
        {
            ViewBag.CookingKitchens = new SelectList(CookingKitchensEnum.GetCookingKitchens(), "value", "text");
            var cookingPlan = Mapper.Map<tCookingPlan, CookingPlan>(_cookingPlanService.GetById(id));
            return PartialView(Views._Create, cookingPlan);
        }

        #endregion

        #region Cancel Plan
        /// <summary>
        /// CancelPlan
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public virtual ActionResult CancelPlan(long id)
        {
            var cookingPlan = _cookingPlanService.GetById(id);
            cookingPlan.bIsCancelled = true;
            _cookingPlanService.Update(cookingPlan);
            // ReSharper disable once Mvc.InvalidModelType
            return View(Views.Index, GetCookingPlans(1));
        }

        #endregion

        #region Post: Cooking Plan Record
        /// <summary>
        /// Post
        /// </summary>
        /// <param name="cookingPlan"></param>
        /// <returns></returns>
        public virtual ActionResult Post(CookingPlan cookingPlan)
        {
            try
            {
                //Edit
                if (cookingPlan.iCookingPlanId > 0)
                {
                    var dbCookingPlan = _cookingPlanService.GetById(cookingPlan.iCookingPlanId);
                    dbCookingPlan.iCookPotNo = cookingPlan.iCookPotNo;
                    dbCookingPlan.bFinishedPlan = true;
                    dbCookingPlan.tActualStartTime = cookingPlan.tActualStartTime;
                    dbCookingPlan.tActualFinishedTime = cookingPlan.tActualFinishedTime;
                    dbCookingPlan.iQuantityCompleted = cookingPlan.iQuantityCompleted;
                    dbCookingPlan.Sys_ModifyBy = User.Identity.GetUserId();
                    dbCookingPlan.Sys_ModifyDateTime = DateTime.Now;
                    EditCookingPlan(dbCookingPlan);
                    AuditScope.CreateAndSave("Editing Cooking Plan", new { User = User.Identity.GetUserId(), CookingPlanid = dbCookingPlan.iCookingPlanId });
                }
                //Add
                else
                {
                    //Check if batch code already assigned to any plan
                    var batchCodeAlreadyAssigned = _cookingPlanService.ValidateBatchCode(cookingPlan.vBatchNo);
                    if (batchCodeAlreadyAssigned)
                    {
                        cookingPlan.vBatchNo = _cookingPlanService.GetBatchCode();
                    }
                    cookingPlan.bFinishedPlan = false;
                    cookingPlan.Sys_CreatedBy = User.Identity.GetUserId();
                    cookingPlan.Sys_CreatedDateTime = DateTime.Now;

                    var savedCookingPlan = AddCookingPlan(cookingPlan);
                    cookingPlan.iCookingPlanId = savedCookingPlan.iCookingPlanId;
                    AuditScope.CreateAndSave("Saving Cooking Plan", new { User = User.Identity.GetUserId(), CookingPlanid = cookingPlan.iCookingPlanId });
                }

                return RedirectToAction(MVC.CookingPlan.Index());
            }
            catch (Exception ex)
            {
                AuditScope.CreateAndSave("Error Saving Cooking Plan", new { User = User.Identity.GetUserId(), Exception = ex.Message });
                throw;
            }
        }
        #endregion

        #region Private: Edit Cooking Plan

        /// <summary>
        /// Edit Cooking Plan
        /// </summary>
        /// <param name="cookingPlan"></param>
        private void EditCookingPlan(tCookingPlan cookingPlan)
        {
            _cookingPlanService.Update(cookingPlan);
        }

        #endregion

        #region Private: Add Cooking Plan

        /// <summary>
        /// Add Cooking Plan
        /// </summary>
        /// <param name="cookingPlan"></param>
        /// <returns></returns>
        private tCookingPlan AddCookingPlan(CookingPlan cookingPlan)
        {
            return _cookingPlanService.Create(Mapper.Map<CookingPlan, tCookingPlan>(cookingPlan));
        }

        #endregion

        #region Get Allowed Stock Quanity

        /// <summary>
        /// Get Allowed Stock Quanity
        /// </summary>
        /// <param name="productCode"></param>
        /// <returns></returns>
        public virtual JsonResult GetAllowedStockQuanity(string productCode)
        {
            return Json(new { result = _stockService.GetAllowedStockQuanity(productCode) }, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Private: Get Cooking Plans
        /// <summary>
        /// Get Cooking Plans
        /// </summary>
        /// <param name="cookpotId"></param>
        /// <returns></returns>
        private List<CookingPlan> GetCookingPlans(int cookpotId)
        {
            ViewBag.hdnCookpotId = cookpotId;
            var cookingPlanList = _cookingPlanService.GetCookingPlansByCookpotId(cookpotId).ToList();
            return cookingPlanList.Select(Mapper.Map<tCookingPlan, CookingPlan>).ToList();
        }
        #endregion

        #region JSON: Get By Code
        /// <summary>
        /// Get By Code
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        [HttpPost]
        public virtual JsonResult GetByCode(string code)
        {
            try
            {
                var batchList = _cookingPlanService.GetByCode(code);

                var batchsList = new ArrayList();
                foreach (var batch in batchList)
                {

                    batchsList.Add(new
                    {
                        id = batch.iCookingPlanId,
                        name = batch.tRecipe.vRecipeName,
                        description = batch.tRecipe.vResipeDescription,
                        code = batch.vBatchNo,
                        recipeCode = batch.tRecipe.vRecipeCode,
                        recipeId = batch.iRecipeId,
                        weight = batch.iQuantity,
                        // ReSharper disable once PossibleInvalidOperationException
                        // ReSharper disable once MergeConditionalExpression
                        bestBefore = batch.tRecipe != null ? (batch.tRecipe.dBestBeforeDate != null ? batch.tRecipe.dBestBeforeDate.Value.ToString("yyyy-MM-dd") : null) : null
                    });
                }
                return Json(batchsList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                AuditScope.CreateAndSave("Error Getting Record By Code in Cooking Plan: ", new { User = User.Identity.GetUserId(), Exception = ex.Message });
                throw;
            }
        }

        #endregion

        #region JSON: Get By Code For Packing Plan
        /// <summary>
        /// Get By Code For Packing Plan
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        [HttpPost]
        public virtual JsonResult GetByCodeForPacking(string code)
        {
            try
            {
                var batchList = _cookingPlanService.GetByCode(code);

                var batchsList = new ArrayList();
                foreach (var batch in batchList)
                {
                    // ReSharper disable once SimplifyConditionalTernaryExpression
                    var bPackingConsumptionCheck = (batch.bPackingConsumptionCheck != null || batch.bPackingConsumptionCheck == false || batch.tActualStartTime == null || batch.tActualFinishedTime == null || batch.bIsTask == true) ? false : true;

                    if (bPackingConsumptionCheck)
                    {
                        batchsList.Add(new
                        {
                            id = batch.iCookingPlanId,
                            name = batch.tRecipe.vRecipeName,
                            description = batch.tRecipe.vResipeDescription,
                            code = batch.vBatchNo,
                            recipeCode = batch.tRecipe.vRecipeCode,
                            recipeId = batch.iRecipeId
                        });
                    }
                }
                return Json(batchsList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                AuditScope.CreateAndSave("Error getting cooking record for packing in CookingPlanController: ", new { User = User.Identity.GetUserId(), Exception = ex.Message });
                throw;
            }
        }

        #endregion

        #region Delete Cooking Plan Record
        /// <summary>
        /// Delete Cooking Plan Record
        /// </summary>
        /// <param name="iCookingPlanId"></param>
        /// <returns></returns>
        public virtual JsonResult DeleteCookingRecord(int iCookingPlanId)
        {
            try
            {
                var cookingPlanRecord = _cookingPlanService.GetById(iCookingPlanId);
                _cookingPlanService.Delete(cookingPlanRecord);
                AuditScope.CreateAndSave("Deleting Cooking Plan Record.", new { User = User.Identity.GetUserId() });
                return Json(new { status = "Success" }, JsonRequestBehavior.AllowGet);
            }
            catch (DbUpdateException ex)
            {
                AuditScope.CreateAndSave("Error Deleting Cooking Plan", new { User = User.Identity.GetUserId() });
                return Json(new { status = "Reference Error" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                AuditScope.CreateAndSave("Error Deleting Cooking Plan", new { User = User.Identity.GetUserId() });
                return Json(new { status = ex }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Get Today's Time Slot Availability
        /// <summary>
        /// Get Today's Time Slot Availability
        /// </summary>
        /// <param name="iCookPotNo"></param>
        /// <param name="startDateTime"></param>
        /// <param name="finishDateTime"></param>
        /// <returns></returns>
        public virtual JsonResult GetTodaysTimeSlotAvailability(int iCookPotNo, DateTime startDateTime, DateTime finishDateTime)
        {
            try
            {
                string slotAvailability = _cookingPlanService.GetTodaysTimeSlotAvailability(iCookPotNo, startDateTime, finishDateTime);
                return Json(new { slot = slotAvailability }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                AuditScope.CreateAndSave("Error finiding time slot for Cooking Plan.", new { User = User.Identity.GetUserId(), Exception = ex.Message });
                return Json(new { slot = ex }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Check Finishing Sequence
        /// <summary>
        /// Get Today's Time Slot Availability
        /// </summary>
        /// <param name="iCookingPlanId"></param>
        /// <returns></returns>
        public virtual JsonResult CheckFinishingSequence(int iCookingPlanId)
        {
            try
            {
                string result = _cookingPlanService.CheckFinishingSequence(iCookingPlanId);
                return Json(new { validity = result }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                AuditScope.CreateAndSave("Error checking validit finishing sequence for Cooking Plan.", new { User = User.Identity.GetUserId(), Exception = ex.Message });
                return Json(new { validity = ex }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Check If Any Cooking Plan exists

        /// <summary>
        /// Check if any cooking plan exists in system
        /// </summary>
        /// <returns>true if exists, false if not exists</returns>
        public virtual JsonResult IsCookingPlanExists(bool isPackingConsumptionCheck = true)
        {
           var existingCookingPlans = _cookingPlanService.GetAll();
            if (existingCookingPlans != null)
            {
                bool result;

                if (isPackingConsumptionCheck)
                    result = existingCookingPlans.Any(w => w.bPackingConsumptionCheck == null || w.bPackingConsumptionCheck == false);
                else
                    result = existingCookingPlans.Any();

                return Json(new { canCreate = result }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { canCreate = false }, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Check Batch Code's Availability
        /// <summary>
        /// Check Batch Code's Availability
        /// </summary>
        /// <param name="vBatchNo"></param>
        /// <returns>Json</returns>
        public virtual JsonResult CheckBatchCodeAvailability(string iBatchNo)
        {
            try
            {
                var batchCodeAlreadyAssigned = _cookingPlanService.ValidateBatchCode(iBatchNo);
                return Json(new { availability = batchCodeAlreadyAssigned }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                AuditScope.CreateAndSave("Error finiding Batch Code availability for Cooking Plan.", new { User = User.Identity.GetUserId(), Exception = ex.Message });
                return Json(new { slot = ex }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Print Record as Report

        /// <summary>
        /// Print Record
        /// </summary>
        /// <param name="pId">Id of the record to print</param>
        /// <returns>RDLC Report View</returns>
        public virtual ActionResult PrintRecord(Int32 pId)
        {
            try
            {
                return View("~/Views/Report/ReportPreview.aspx", new Report { ReportName = PkReportNames.CookingPlanRecord, Id = pId, Service = _cookingPlanService });
            }
            catch (Exception ex)
            {
                AuditScope.CreateAndSave("Error getting report of Cooking Plan", new { User = User.Identity.GetUserId() });
                throw;
            }
        }
        #endregion      
    }
}