﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Audit.Core;
using AutoMapper;
using Microsoft.AspNet.Identity;
using PunjabKitchen.Models;
using PunjabKitchenBLL.Interfaces;
using PunjabKitchenEntities.EntityModel;
using PagedList;

namespace PunjabKitchen.Controllers
{
    [Authorize(Roles = "Admin,Packing Manager,Packing  Staff")]
    public partial class BatchChangeCheckPureeController : BaseController
    {
        #region Data Members
        readonly IBatchChangePureeRecordService _batchChangeCheckPureePackingRecordService;
        readonly IRecipeService _recipeService;
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="batchChangeCheckPureePackingRecordService"></param>
        /// <param name="recipeService"></param>
        public BatchChangeCheckPureeController(IBatchChangePureeRecordService batchChangeCheckPureePackingRecordService, IRecipeService recipeService)
        {
            _batchChangeCheckPureePackingRecordService = batchChangeCheckPureePackingRecordService;
            _recipeService = recipeService;
        }
        #endregion

        #region Create New Batch Change Check Puree Packing Record
        /// <summary>
        /// Create New Batch Change Check Puree Packing Record
        /// </summary>
        /// <returns></returns>
        public virtual ActionResult CreateBatchChangeCheckPureePackingRecord()
        {
            return View(Views.Create, new BatchChangeCheckPureeRecord());
        }
        #endregion

        #region Get Batch Change Check Puree Packing Records List 
        /// <summary>
        /// Get Batch Change Check Puree Packing Records List 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public virtual ActionResult BatchChangeCheckPureePackingRecords(int? page = 1)
        {
            var recordsList = GetBatchChangeCheckPureePackingRecords().OrderByDescending(x => x.iBatchChangeChkPureeRecrdId);
            var pageNumber = page ?? 1;
            return View(Views.Index, recordsList.ToPagedList(pageNumber, GetPageSize()));
        }
        #endregion

        #region Return Batch Change Check Puree Packing Record Edit View
        /// <summary>
        /// Return Batch Change Check Puree Packing Record Edit View
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize(Roles = "Admin,Packing Manager")]
        public virtual ActionResult EditBatchChangeCheckPureePackingRecord(long id)
        {
            BatchChangeCheckPureeRecord batchChangeCheckPureePackingRec = Mapper.Map<tBatchChangeChkPureeRecrd, BatchChangeCheckPureeRecord>(_batchChangeCheckPureePackingRecordService.GetById(id));
            return View(Views.Create, batchChangeCheckPureePackingRec);
        }
        #endregion

        #region  Get All Batch Change Check Puree Packing Records
        /// <summary>
        /// Get All Batch Change Check Puree Packing Records
        /// </summary>
        /// <returns></returns>
        private List<BatchChangeCheckPureeRecord> GetBatchChangeCheckPureePackingRecords()
        {
            var batchChangeCheckPureeRecordsList = _batchChangeCheckPureePackingRecordService.GetAll();
            return batchChangeCheckPureeRecordsList.Select(Mapper.Map<tBatchChangeChkPureeRecrd, BatchChangeCheckPureeRecord>).ToList();
        }
        #endregion

        #region Add/Edit
        /// <summary>
        /// Update Batch Change Check Puree Packing record if found else create new
        /// </summary>
        /// <param name="batchChangeCheckPureeRecordModel"></param>
        /// <returns></returns>
        public virtual ActionResult PostBatchChangeCheckPureePackingRecord(BatchChangeCheckPureeRecord batchChangeCheckPureeRecordModel)
        {
            try
            {
                //Edit
                if (batchChangeCheckPureeRecordModel.iBatchChangeChkPureeRecrdId > 0)
                {
                    var savedibatchChangeCheckPureeRecord = Mapper.Map<BatchChangeCheckPureeRecord, tBatchChangeChkPureeRecrd>(batchChangeCheckPureeRecordModel);

                    savedibatchChangeCheckPureeRecord.Sys_ModifyBy = User.Identity.GetUserId();
                    savedibatchChangeCheckPureeRecord.Sys_ModifyDateTime = DateTime.Now;
                    // ReSharper disable once PossibleInvalidOperationException
                    savedibatchChangeCheckPureeRecord.tRecipe = _recipeService.GetById((long)batchChangeCheckPureeRecordModel.iRecipeId);
                    _batchChangeCheckPureePackingRecordService.Update(savedibatchChangeCheckPureeRecord);
                    AuditScope.CreateAndSave("Editing Batch Change Check Puree Packing Record", new { User = User.Identity.GetUserId() });
                }
                //Add
                else
                {
                    batchChangeCheckPureeRecordModel.Sys_CreatedBy = User.Identity.GetUserId();
                    batchChangeCheckPureeRecordModel.Sys_CreatedDateTime = DateTime.Now;
                    batchChangeCheckPureeRecordModel.tRecipe = null;
                    try
                    {
                        _batchChangeCheckPureePackingRecordService.Create(Mapper.Map<BatchChangeCheckPureeRecord, tBatchChangeChkPureeRecrd>(batchChangeCheckPureeRecordModel));
                        AuditScope.CreateAndSave("Adding Batch Change Check Puree Packing Record", new { User = User.Identity.GetUserId() });
                    }
                    catch (Exception ex)
                    {
                        AuditScope.CreateAndSave("Error saving Batch Change Check Puree Packing: ", new { User = User.Identity.GetUserId(), Exception = ex.Message });
                        throw;
                    }
                }
                return RedirectToAction(MVC.BatchChangeCheckPuree.BatchChangeCheckPureePackingRecords());
            }
            catch (Exception ex)
            {
                AuditScope.CreateAndSave("Error saving Batch Change Check Puree Packing record: ", new { User = User.Identity.GetUserId(), Exception = ex.Message });
                return RedirectToAction(MVC.Home.Index());
            }
        }
        #endregion

        #region Print Record as Report

        /// <summary>
        /// Print Record
        /// </summary>
        /// <param name="pId">Id of the record to print</param>
        /// <returns>RDLC Report View</returns>
        public virtual ActionResult PrintRecord(Int32 pId)
        {
            try
            {
                return View("~/Views/Report/ReportPreview.aspx", new Report { ReportName = PkReportNames.BatchChangeCheckPureeRecord, Id = pId, Service = _batchChangeCheckPureePackingRecordService });
            }
            catch (Exception ex)
            {
                AuditScope.CreateAndSave("Error getting Report of Batch Change Check Puree", new { User = User.Identity.GetUserId() });
                // ReSharper disable once PossibleIntendedRethrow
                throw ex;
            }
        }
        #endregion
    }
}