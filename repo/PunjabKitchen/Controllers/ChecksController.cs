﻿using AutoMapper;
using Microsoft.AspNet.Identity;
using PunjabKitchen.Models;
using PunjabKitchenBLL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Audit.Core;
using PunjabKitchen.Models.RequestModels;
using PunjabKitchenEntities.EntityModel;
using static System.Boolean;
using PagedList;
// ReSharper disable All

namespace PunjabKitchen.Controllers
{
    [Authorize(Roles = "Admin,Cooking Manager,Cooking Staff,Goods Out Manager,Goods Out Staff")]
    public partial class ChecksController : BaseController
    {
        #region Data Members
        readonly IStartupCheckListService _startupCheckListService;
        readonly IStartUpcheckDailyService _startupCheckService;
        readonly INotificationService _notificationService;
        readonly ICanteenFridgeTempRecordService _canteenTempService;
        readonly IAreaService _areaService;
        readonly IAreaItemService _areaItemService;
        readonly ICleaningCheckListDailyService _cleaningCheckListDailyService;
        readonly IShoeCleaningCheckService _shoeCleaningCheckService;
        readonly INotificationTypeService _notificationTypeService;
        readonly IDailyTruckInspectionService _dailyTruckInspectionService;
        readonly IGoodsOutFreezerUnitService _goodsOutFreezerUnit;
        readonly IStartupCheckEPGoodsOutService _startupCheckEpGoodsOutService;
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="startupCheckListService"></param>
        /// <param name="startupCheckService"></param>
        /// <param name="notificationService"></param>
        /// <param name="canteenTempService"></param>
        /// <param name="areaService"></param>
        /// <param name="areaItemService"></param>
        /// <param name="cleaningCheckListDailyService"></param>
        /// <param name="shoeCleaningCheckService"></param>
        /// <param name="notificationTypeService"></param>
        /// <param name="dailyTruckInspectionService"></param>
        /// <param name="goodsOutFreezerUnit"></param>
        /// <param name="startupCheckEpGoodsOutService"></param>
        public ChecksController(IStartupCheckListService startupCheckListService, IStartUpcheckDailyService startupCheckService, INotificationService notificationService, ICanteenFridgeTempRecordService canteenTempService, IAreaService areaService, IAreaItemService areaItemService, ICleaningCheckListDailyService cleaningCheckListDailyService, IShoeCleaningCheckService shoeCleaningCheckService, INotificationTypeService notificationTypeService, IDailyTruckInspectionService dailyTruckInspectionService, IGoodsOutFreezerUnitService goodsOutFreezerUnit, IStartupCheckEPGoodsOutService startupCheckEpGoodsOutService)
        {
            _startupCheckListService = startupCheckListService;
            _startupCheckService = startupCheckService;
            _notificationService = notificationService;
            _canteenTempService = canteenTempService;
            _areaService = areaService;
            _areaItemService = areaItemService;
            _cleaningCheckListDailyService = cleaningCheckListDailyService;
            _shoeCleaningCheckService = shoeCleaningCheckService;
            _notificationTypeService = notificationTypeService;
            _dailyTruckInspectionService = dailyTruckInspectionService;
            _goodsOutFreezerUnit = goodsOutFreezerUnit;
            _startupCheckEpGoodsOutService = startupCheckEpGoodsOutService;
        }

        #endregion

        #region GetNotificationModel
        /// <summary>
        /// get check screen based upon notification type id
        /// </summary>
        /// <param name="notificationTypeId"></param>
        /// <param name="areaId"></param>
        /// <returns></returns>
        public virtual ActionResult GetNotificationModel(int notificationTypeId, int areaId)
        {
            //2 - Start Up Check - LR Cooking Room
            //3- Daily Cleaning Checklist
            //4- Canteen Fridge temperature Check
            //5- Shoe Cleaning Checklist
            switch (notificationTypeId)
            {
                case 2:
                    return CreateDailyCheck();
                case 3:
                    return CleaningCheckListDaily(areaId);
                case 4:
                    return CanteenFridgeTempDaily();
                case 5:
                    return ShoeCleaningCheck();
                case 11:
                    return CreateDailyTruckInspectionCheck();
                case 12:
                    return CreateGoodsOutFreezerUnitCheck();
                case 13:
                    return CreateStartupEPGoodsOutCheck();
                default:
                    return RedirectToAction(MVC.Notifications.Index());
            }

        }
        #endregion

        #region Create Daily Check
        /// <summary>
        /// Create Daily Check
        /// </summary>
        /// <returns></returns>
        public virtual ActionResult CreateDailyCheck()
        {
            return PartialView(Views.Daily.CookingStartup, GetDailyStartupCheckList());
        }

        /// <summary>
        /// Create Daily Check (Canteen fridge temp)
        /// </summary>
        /// <returns></returns>
        public virtual ActionResult CanteenFridgeTempDaily()
        {
            return PartialView(Views.Daily.CanteenFridgeTempRecord);
        }

        /// <summary>
        /// Daily Cleaning Check List
        /// </summary>
        /// <param name="areaId"></param>
        /// <returns></returns>
        public virtual ActionResult CleaningCheckListDaily(int areaId)
        {
            return GetAreaItems(areaId);
        }

        /// <summary>
        /// Shoe Cleaning Check List
        /// </summary>
        /// <returns></returns>
        public virtual ActionResult ShoeCleaningCheck()
        {
            // ReSharper disable once Mvc.InvalidModelType
            return PartialView(Views.Daily.ShoeCleaningCheck, new List<ShoeCleaningCheck>());
        }

        /// <summary>
        /// Create Daily Truck Inspection Check
        /// </summary>
        /// <returns></returns>
        public virtual ActionResult CreateDailyTruckInspectionCheck()
        {
            return PartialView(Views.Daily.GoodsOut.DailyTruckInspection, new DailyTruckInspection());
        }

        /// <summary>
        /// Create Goods Out Freezer Unit Check
        /// </summary>
        /// <returns></returns>
        public virtual ActionResult CreateGoodsOutFreezerUnitCheck()
        {
            return PartialView(Views.Daily.GoodsOut.GoodsOutFreezerUnit, new GoodsOutFreezerUnit());
        }

        /// <summary>
        /// Create Startup EP Goods Out Check
        /// </summary>
        /// <returns></returns>
        public virtual ActionResult CreateStartupEPGoodsOutCheck()
        {
            return PartialView(Views.Daily.GoodsOut.StartupCheckEPGoodsOut, new StartupChecksEPGoodsOut());
        }
        #endregion

        #region Cancel Check
        /// <summary>
        /// cancel daily checks
        /// </summary>        
        [HttpPost]
        public virtual JsonResult Cancelcheck(int notificationId)
        {
            try
            {
                CancelCheck(notificationId);
            }
            catch (Exception ex)
            {
                AuditScope.CreateAndSave("Error cancelling check: ", new { User = User.Identity.GetUserId(), Exception = ex.Message });
                throw;
            }

            return Json(notificationId, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region save canteen fridge temperature
        /// <summary>
        /// Save Canteen Fridge Temperature
        /// </summary>
        /// <param name="objFridgeTemp"></param>
        /// <returns></returns>
        [HttpPost]
        public virtual JsonResult SaveCanteenFridgeTempDailyCheck(CanteenFridgeTempRecord objFridgeTemp)
        {
            try
            {
                CreateAuditLog("Save Canteen Fridge Temperature Start of Date " + DateTime.Now.Date, User.Identity.GetUserId());
                objFridgeTemp.dDateTime = DateTime.Now;
                objFridgeTemp.Sys_CreatedBy = User.Identity.GetUserId();
                objFridgeTemp.Sys_CreatedDateTime = DateTime.Now;
                _canteenTempService.Create(Mapper.Map<CanteenFridgeTempRecord, tCanteenFridgeTempRecordDaily>(objFridgeTemp));
                AuditScope.CreateAndSave("Saving Canteen Fridge Temp Daily Check", new { User = User.Identity.GetUserId() });
                return Json(new { result = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                CreateAuditLog("Error in Saving Canteen Fridge Temperature Check", new { exception = ex.Message });
                return Json(new { result = false }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region save daily cleaning check
        /// <summary>
        /// save dailycleaning check
        /// </summary>
        /// <param name="valueArray"></param>
        [HttpPost]
        public virtual JsonResult SaveCleaningChecklisDaily(List<string> valueArray)
        {
            try
            {
                foreach (string rowValue in valueArray)
                {
                    string[] arrayRow = rowValue.Split(',');
                    if (arrayRow[0] == "undefined") continue;

                    int areaId;
                    if (!int.TryParse(arrayRow[0], out areaId)) continue;

                    int areaItemId;
                    if (!int.TryParse(arrayRow[1], out areaItemId)) continue;

                    bool opischecked;
                    if (!TryParse(arrayRow[2], out opischecked)) continue;

                    bool rcischecked;
                    if (!TryParse(arrayRow[3], out rcischecked)) continue;


                    InsertDailyCleaningCheck(areaId, areaItemId, opischecked, rcischecked, arrayRow[4], arrayRow[5]);

                    UpdateNotificationOnDailyCleaningCheck();
                    AuditScope.CreateAndSave("Saving Daily Cleaning Check", new { User = User.Identity.GetUserId() });
                }
                return Json(new { result = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //Logging exception
                AuditScope.CreateAndSave("Error in saving Daily Cleaning Check", new { User = User.Identity.GetUserId() });
                return Json(new { result = false }, JsonRequestBehavior.AllowGet);
            }

        }
        #endregion

        #region Helper Methods

        /// <summary>
        /// Get daily check list
        /// </summary>
        /// <returns></returns>
        private List<StartupCheckList> GetDailyStartupCheckList()
        {
            var list = new List<StartupCheckList>();
            try
            {
                var checkList = _startupCheckListService.GetAll();
                // ReSharper disable once LoopCanBeConvertedToQuery
                foreach (var tStartUpCheckList in checkList)
                {
                    list.Add(Mapper.Map<tStartUpCheckList, StartupCheckList>(tStartUpCheckList));
                }
            }
            catch (Exception ex)
            {
                list = null;
                AuditScope.CreateAndSave("Get Daily Checklist Error", new { User = User.Identity.GetUserId() });
            }

            return list;
        }

        /// <summary>
        /// save daily checks
        /// </summary>        
        [HttpPost]
        public virtual JsonResult SaveDailyCheck(List<string> valueArray)
        {
            try
            {
                // ReSharper disable once LoopCanBePartlyConvertedToQuery
                foreach (string rowValue in valueArray)
                {
                    string[] arrayRow = rowValue.Split(',');
                    if (arrayRow[0] == "undefined") continue;

                    bool isBoolean;
                    if (!TryParse(arrayRow[2], out isBoolean)) continue;

                    var value = arrayRow[0];

                    int checkListId;
                    if (!int.TryParse(arrayRow[1], out checkListId)) continue;

                    InsertDailyCheck(value, checkListId, isBoolean);
                    AuditScope.CreateAndSave("Saving Daily Check", new { User = User.Identity.GetUserId() });
                }
                return Json(new { result = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { result = false }, JsonRequestBehavior.AllowGet);
            }

        }

        /// <summary>
        /// save daily checks
        /// </summary>
        /// <param name="value"></param>
        /// <param name="checkListId">check list id</param>
        /// <param name="isBoolean"></param>        
        private void InsertDailyCheck(string value, int checkListId, bool isBoolean)
        {
            try
            {
                StartupCheckDaily objStartUpCheckDaily = new StartupCheckDaily
                {
                    bIsChecked = isBoolean ? Parse(value) : (bool?)null,
                    vValue = !isBoolean ? value : null,
                    iStartUpCheckListId = checkListId,
                    dDateTime = DateTime.Now,
                    Sys_CreatedBy = User.Identity.GetUserId(),
                    Sys_CreatedDateTime = DateTime.Now
                };
                _startupCheckService.Create(Mapper.Map<StartupCheckDaily, tStartUpcheckDaily>(objStartUpCheckDaily));
                AuditScope.CreateAndSave("Saving Startup Check", new { User = User.Identity.GetUserId() });
            }
            catch (Exception ex)
            {
                AuditScope.CreateAndSave("Error saving Startup Check", new { User = User.Identity.GetUserId(), Exception = ex.Message });
                throw;
            }
        }

        /// <summary>
        /// cancel daily checks
        /// </summary>     
        private void CancelCheck(int notificationId)
        {
            try
            {
                var notification = _notificationService.GetById(notificationId);
                notification.bNotificationReceived = false;
                notification.iWorkStarterId = null;
                notification.Sys_ModifyBy = User.Identity.GetUserId();
                notification.Sys_ModifyDateTime = DateTime.Now;
                _notificationService.Update(notification);
                AuditScope.CreateAndSave("Cancelling Notification", new { User = User.Identity.GetUserId() });
            }
            catch (Exception ex)
            {
                AuditScope.CreateAndSave("Error cancelling notification: ", new { User = User.Identity.GetUserId(), Exception = ex.Message });
                throw;
            }
        }

        /// <summary>
        /// Get list of areas
        /// </summary>
        /// <returns></returns>
        private List<Area> GetAreas()
        {
            var list = _areaService.GetAll();
            if (list == null) throw new ArgumentNullException(nameof(list));
            return list.Select(item => Mapper.Map<tArea, Area>(item)).ToList();
        }

        /// <summary>
        /// Getitems by area id
        /// </summary>
        /// <param name="areaId">area id</param>
        /// <returns></returns>
        [HttpPost]
        public virtual ActionResult GetAreaItems(int areaId)
        {
            try
            {
                var items = new List<CleaningCheckListDaily>();
                var list = _areaItemService.GetItemByAreaId(areaId);
                // ReSharper disable once LoopCanBeConvertedToQuery
                foreach (var item in list)
                {
                    //TODO: apply mapper
                    items.Add(Mapper.Map<tAreaItem, CleaningCheckListDaily>(item));
                }
                ViewBag.areas = new SelectList(GetAreas(), "iAreaId", "vAreaName");
                return PartialView("~/Views/Checks/Daily/CleaningCheckList.cshtml", items);

            }
            catch (Exception ex)
            {
                //Logging exception
                CreateAuditLog("Create Cooking Daily Check Error", new { exception = ex.Message });
            }
            // ReSharper disable once Mvc.ViewNotResolved
            return View("");
        }

        /// <summary>
        /// save dailycleaning check
        /// </summary>
        private void InsertDailyCleaningCheck(int areaId, int areaItemId, bool opischecked, bool rcischecked, string issue, string action)
        {
            try
            {
                var objStartUpCheckDaily = new CleaningCheckListDaily
                {
                    iAreaId = areaId,
                    iAreaItemId = areaItemId,
                    bOP = opischecked,
                    bRC = rcischecked,
                    vIssues = issue,
                    vAction = action,
                    dDateTime = DateTime.Now,
                    Sys_CreatedBy = User.Identity.GetUserId(),
                    Sys_CreatedDateTime = DateTime.Now
                };
                _cleaningCheckListDailyService.Create(Mapper.Map<CleaningCheckListDaily, CleaningChecklistDaily>(objStartUpCheckDaily));
                AuditScope.CreateAndSave("Inserting Daily Cleaning Check", new { User = User.Identity.GetUserId() });
            }
            catch (Exception ex)
            {
                AuditScope.CreateAndSave("Error inserting daily cleaning check", new { User = User.Identity.GetUserId(), Exception = ex.Message });
                throw;
            }
        }

        #endregion

        #region UpdateNotificationOnDailyCleaningCheck
        /// <summary>
        /// Method to update Notification Record if all daily cleaning areas are performed
        /// </summary>
        private void UpdateNotificationOnDailyCleaningCheck()
        {
            try
            {
                var areas = _areaService.GetAll();
                var distinctPerformedChecksList = _cleaningCheckListDailyService.GetDistinctDailyCheck(DateTime.Now).ToList();
                if (areas.Count() == distinctPerformedChecksList.Count)
                {
                    var notifications = _notificationService.GetNotificationByDate(DateTime.Now);
                    var dbNotification = notifications.FirstOrDefault(x => x.iNotificationTypeId == 3);

                    CreateAuditLog("Updating notification - all daily cleaning areas check performed" + DateTime.Now.Date, User.Identity.GetUserId());
                    if (dbNotification != null)
                    {
                        AuditScope.CreateAndSave("Assigning User Notification to User",
                            new { User = User.Identity.GetUserId(), NotifiactionId = dbNotification.iNotificationId });
                        var userId = User.Identity.GetUserId();
                        dbNotification.iWorkStarterId = userId;
                        dbNotification.Sys_ModifyBy = userId;
                        dbNotification.Sys_ModifyDateTime = DateTime.Now;
                        _notificationService.Update(dbNotification);
                        AuditScope.CreateAndSave("Successfully Assigned User Notification to User",
                            new { User = User.Identity.GetUserId(), NotifiactionId = dbNotification.iNotificationId });
                    }
                }
            }
            catch (Exception ex)
            {
                AuditScope.CreateAndSave("Error Occured while Assigning User to Notification",
                           new { User = User.Identity.GetUserId(), ExceptionMsg = ex.Message });
                throw;
            }


        }
        #endregion

        #region save shoe Cleaning Check Weekly
        /// <summary>
        /// save shoe Cleaning Check Weekly
        /// </summary>
        [HttpPost]
        public virtual JsonResult SaveShoeCleaningCheckWeekly()
        {
            try
            {
                var shoeCleaning = new ShoeCleaningCheck
                {
                    iUserId = User.Identity.GetUserId(),
                    dCheckPerformDate = DateTime.Now,
                    Sys_CreatedBy = User.Identity.GetUserId(),
                    Sys_CreatedDateTime = DateTime.Now
                };
                _shoeCleaningCheckService.Create(Mapper.Map<ShoeCleaningCheck, tShoeCleaningRecrdWeekly>(shoeCleaning));
                AuditScope.CreateAndSave("Saving Show Cleaning Check", new { User = User.Identity.GetUserId() });
                return Json(new { result = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //Logging exception
                CreateAuditLog("Error saving Shoe Cleaning Check Weekly", new { exception = ex.Message });
                return Json(new { result = false }, JsonRequestBehavior.AllowGet);
            }

        }
        #endregion

        #region Return Cleaning Checks Areas
        /// <summary>
        /// Return Cleaning Checks Areas
        /// </summary>
        /// <returns></returns>
        public virtual ActionResult CleaningCheckAreas()
        {
            var areasList = _areaService.GetAll();
            var mappedAreasList = areasList.Select(Mapper.Map<tArea, CleaningAreas>).ToList();
            var distinctPerformedChecksList = _cleaningCheckListDailyService.GetDistinctDailyCheck(DateTime.Now).ToList();

            foreach (var item in mappedAreasList)
            {
                var foundElem = distinctPerformedChecksList.FirstOrDefault(x => x.iAreaId == item.iAreaId);
                if (foundElem != null)
                {
                    item.IsPerformed = true;
                    item.PerformedBy = foundElem.AspNetUser.FirstName + " " + foundElem.AspNetUser.LastName;
                    if (foundElem.Sys_CreatedDateTime != null)
                        item.PerformedDateTime = (DateTime)foundElem.Sys_CreatedDateTime;

                    if (foundElem.Sys_ModifyDateTime != null)
                    {
                        item.ModifiedBy = foundElem.AspNetUser1.FirstName + " " + foundElem.AspNetUser1.LastName;
                        item.ModifiedTime = (DateTime)foundElem.Sys_ModifyDateTime;
                    }
                }
            }
            return PartialView("~/Views/Notifications/CleaningAreas.cshtml", mappedAreasList);
        }
        #endregion

        #region Daily/Weekly Checks
        /// <summary>
        /// Daily/Weekly Checks
        /// </summary>
        [Authorize(Roles = "Admin,Cooking Manager")]
        [HttpGet]
        public virtual ActionResult DailyWeeklyChecks()
        {
            //List containing the desired filters.
            string[] filterList = { "Start up Check - LR Cooking Room", "Daily Cleaning Checklist", "Canteen Fridge Temperature Records   ", "Shoe Cleaning Check" };
            ViewBag.Filters = new SelectList(_notificationTypeService.GetAll().Where(model => filterList.Contains(model.vType)), "iNotificationTypeId", "vType");

            return View(Views.Daily.DailyWeeklyChecks);
        }
        #endregion

        #region Goods Out Daily Checks
        /// <summary>
        /// Goods Out Daily Checks
        /// </summary>
        [Authorize(Roles = "Admin,Goods Out Manager")]
        [HttpGet]
        public virtual ActionResult GoodsOutDailyChecks()
        {
            //List containing the desired filters.
            string[] filterList = { "Daily Truck Inspection", "Goods Out Freezer Unit Check", "Start Up EP Goods Out" };
            ViewBag.Filters = new SelectList(_notificationTypeService.GetAll().Where(model => filterList.Contains(model.vType)), "iNotificationTypeId", "vType");

            return View(Views.Daily.GoodsOut.GoodsOutChecksHistory);
        }
        #endregion

        #region Get Daily/Weekly Check
        /// <summary>
        /// Get Daily/Weekly Check
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public virtual ActionResult GetDailyWeeklyCheck(ChecksHistoryRequestModel requestModel)
        {
            switch (requestModel.Id)
            {
                //Start up Check - LR Cooking Room
                case 2:
                    return StartupChecklistHistory(requestModel);
                //Daily Cleaning Checklist
                case 3:
                    return DailyCleaningChecklistHistory(requestModel);
                //Canteen Fridge Temperature Records   
                case 4:
                    return CanteenFridgeTemperatureHistory(requestModel);
                //Shoe Cleaning Check
                case 5:
                    return ShoeCleaningHistory(requestModel);
            }
            return RedirectToAction(MVC.Checks.DailyWeeklyChecks());
        }
        #endregion

        #region Get Goods Out Daily Check
        /// <summary>
        /// Get Goods Out Daily Check
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public virtual ActionResult GetGoodsOutDailyChecksHistory(ChecksHistoryRequestModel requestModel)
        {
            switch (requestModel.Id)
            {
                //Daily Truck Inspection History
                case 11:
                    return DailyTruckInspectionHistory(requestModel);
                //Goods Out Freezer Unit Check
                case 12:
                    return GoodsOutFreezerUnitHistory(requestModel);
                //Start Up EP Goods Out
                case 13:
                    return StartupEPGoodsOutHistory(requestModel);
            }
            return RedirectToAction(MVC.Checks.GoodsOutDailyChecks());
        }
        #endregion

        #region Method to get Daily Truck Inspection Historic Data
        /// <summary>
        /// Method to get Daily Truck Inspection Historic Data
        /// </summary>
        [HttpGet]
        public virtual ActionResult DailyTruckInspectionHistory(ChecksHistoryRequestModel requestModel)
        {
            try
            {
                var list = new List<ChecksHistoryModel>();
                var dailyTruckInspectionList = _dailyTruckInspectionService.GetDailyTruckInspectionHistory(requestModel.StartDateTime, requestModel.EndDateTime)?.ToList();
                if (dailyTruckInspectionList != null)
                {
                    var daysCount = (requestModel.EndDateTime - requestModel.StartDateTime).TotalDays + 1;

                    for (var i = 0; i < daysCount; i++)
                    {
                        var nextDate = requestModel.StartDateTime.AddDays(i);
                        var exist =
                            dailyTruckInspectionList.Any(
                                x => x.Sys_CreatedDateTime != null && x.Sys_CreatedDateTime.Value.Date == nextDate.Date);

                        //Enter Missing Date
                        if (!exist)
                        {
                            list.Add(new ChecksHistoryModel
                            {
                                CreatedDate = nextDate,
                                IsPerformed = false
                            });
                        }
                        //Enter Found Records
                        else
                        {
                            try
                            {
                                var filteredList =
                                    dailyTruckInspectionList.Where(
                                        x =>
                                            x.Sys_CreatedDateTime != null &&
                                            x.Sys_CreatedDateTime.Value.Date == nextDate.Date);
                                list.AddRange(filteredList.Select(item => new ChecksHistoryModel
                                {
                                    Id = item.iTruckInspectionId,
                                    IsPerformed = true,
                                    CreatedDate = item.Sys_CreatedDateTime,
                                    CreatedBy =
                                        item.AspNetUser != null
                                            ? item.AspNetUser.FirstName + " " + item.AspNetUser.LastName
                                            : string.Empty,
                                    ModifiedBy =
                                        item.AspNetUser1 != null
                                            ? item.AspNetUser1.FirstName + " " + item.AspNetUser1.LastName
                                            : string.Empty,
                                    vTruckType = item.vTruckType != null ? item.vTruckType : "",
                                    vTruckMake = item.vTruckMake != null ? item.vTruckMake : "",
                                    iMaxLoad = item.iMaxLoad != null ? item.iMaxLoad.Value : 0,
                                    iPropulsion = item.iPropulsion != null ? item.iPropulsion : "",
                                    vChanges = item.vChanges != null ? item.vChanges : ""
                                }));
                            }
                            catch (Exception ex)
                            {
                                AuditScope.CreateAndSave("Error Getting Daily Truck Inspection Record: ",
                                    new { User = User.Identity.GetUserId(), Exception = ex.Message });
                                throw;
                            }
                        }
                    }
                }
                var recordsList = list.OrderByDescending(x => x.CreatedDate);
                var pageNumber = requestModel.page ?? 1;
                return PartialView(Views.ChecksHistoricViews.DailyTruckInspectionHistory, recordsList.ToPagedList(pageNumber, GetPageSize()));
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region Method to get Goods Out Freezer Unit Historic Data
        /// <summary>
        /// Method to get Goods Out Freezer Unit Historic Data
        /// </summary>
        [HttpGet]
        public virtual ActionResult GoodsOutFreezerUnitHistory(ChecksHistoryRequestModel requestModel)
        {
            var list = new List<ChecksHistoryModel>();
            var goodsOutFreezerList = _goodsOutFreezerUnit.GetGoodsOutFreezerUnitHistory(requestModel.StartDateTime, requestModel.EndDateTime).ToList();
            var daysCount = (requestModel.EndDateTime - requestModel.StartDateTime).TotalDays + 1;

            for (var i = 0; i < daysCount; i++)
            {
                var nextDate = requestModel.StartDateTime.AddDays(i);
                var exist = goodsOutFreezerList.Any(x => x.Sys_CreatedDateTime != null && x.Sys_CreatedDateTime.Value.Date == nextDate.Date);

                //Enter Missing Date
                if (!exist)
                {
                    list.Add(new ChecksHistoryModel
                    {
                        CreatedDate = nextDate,
                        IsPerformed = false
                    });
                }
                //Enter Found Records
                else
                {
                    try
                    {
                        var filteredList = goodsOutFreezerList.Where(x => x.Sys_CreatedDateTime != null && x.Sys_CreatedDateTime.Value.Date == nextDate.Date);
                        list.AddRange(filteredList.Select(item => new ChecksHistoryModel
                        {
                            Id = item.iGoodsOutFreezerUnit,
                            IsPerformed = true,
                            CreatedDate = item.dTime,
                            CreatedBy = item.AspNetUser != null ? item.AspNetUser.FirstName + " " + item.AspNetUser.LastName : string.Empty,
                            ModifiedBy = item.AspNetUser1 != null ? item.AspNetUser1.FirstName + " " + item.AspNetUser1.LastName : string.Empty,
                            iFreezerTemp1 = item.iFreezerTemp1 != null ? item.iFreezerTemp1.Value : 0,
                            iFreezerTemp2 = item.iFreezerTemp2 != null ? item.iFreezerTemp2.Value : 0,
                            iFreezerTemp3 = item.iFreezerTemp3 != null ? item.iFreezerTemp3.Value : 0,
                            iFreezerTemp4 = item.iFreezerTemp4 != null ? item.iFreezerTemp4.Value : 0,
                            iFreezerTemp5 = item.iFreezerTemp5 != null ? item.iFreezerTemp5.Value : 0,
                            vComments = item.vComments != null ? item.vComments : "",
                        }));
                    }
                    catch (Exception ex)
                    {
                        AuditScope.CreateAndSave("Error Getting Goods Out Freezer Unit Record: ", new { User = User.Identity.GetUserId(), Exception = ex.Message });
                        throw;
                    }
                }
            }

            var recordsList = list.OrderByDescending(x => x.CreatedDate);
            var pageNumber = requestModel.page ?? 1;
            return PartialView(Views.ChecksHistoricViews.GoodsOutFreezerUnitHistory, recordsList.ToPagedList(pageNumber, GetPageSize()));
        }
        #endregion

        #region Method to get Startup EP Goods Out Historic Data
        /// <summary>
        /// Method to get Startup EP Goods Out Historic Data
        /// </summary>
        [HttpGet]
        public virtual ActionResult StartupEPGoodsOutHistory(ChecksHistoryRequestModel requestModel)
        {
            var list = new List<ChecksHistoryModel>();
            var startupEPGoodsOutList = _startupCheckEpGoodsOutService.GetStartupChecksEpGoodsOutHistory(requestModel.StartDateTime, requestModel.EndDateTime).ToList();
            var daysCount = (requestModel.EndDateTime - requestModel.StartDateTime).TotalDays + 1;

            for (var i = 0; i < daysCount; i++)
            {
                var nextDate = requestModel.StartDateTime.AddDays(i);
                var exist = startupEPGoodsOutList.Any(x => x.Sys_CreatedDateTime != null && x.Sys_CreatedDateTime.Value.Date == nextDate.Date);

                //Enter Missing Date
                if (!exist)
                {
                    list.Add(new ChecksHistoryModel
                    {
                        CreatedDate = nextDate,
                        IsPerformed = false
                    });
                }
                //Enter Found Records
                else
                {
                    try
                    {
                        var filteredList = startupEPGoodsOutList.Where(x => x.Sys_CreatedDateTime != null && x.Sys_CreatedDateTime.Value.Date == nextDate.Date);
                        list.AddRange(filteredList.Select(item => new ChecksHistoryModel
                        {
                            Id = item.iStartupChecksEPGoodsOut,
                            IsPerformed = true,
                            CreatedDate = item.Sys_CreatedDateTime,
                            CreatedBy = item.AspNetUser != null ? item.AspNetUser.FirstName + " " + item.AspNetUser.LastName : string.Empty,
                            ModifiedBy = item.AspNetUser1 != null ? item.AspNetUser1.FirstName + " " + item.AspNetUser1.LastName : string.Empty
                        }));
                    }
                    catch (Exception ex)
                    {
                        AuditScope.CreateAndSave("Error Getting Startup EP Goods Out Record: ", new { User = User.Identity.GetUserId(), Exception = ex.Message });
                        throw;
                    }
                }
            }

            var recordsList = list.OrderByDescending(x => x.CreatedDate);
            var pageNumber = requestModel.page ?? 1;
            return PartialView(Views.ChecksHistoricViews.StartupEPGoodsOutHistory, recordsList.ToPagedList(pageNumber, GetPageSize()));
        }
        #endregion

        #region Get Shoes Cleaning Record Historic Data
        /// <summary>
        /// Get Shoes Cleaning Record Historic Data
        /// </summary>
        [HttpGet]
        public virtual ActionResult ShoeCleaningHistory(ChecksHistoryRequestModel requestModel)
        {
            var list = new List<ChecksHistoryModel>();
            var daysCount = (requestModel.EndDateTime - requestModel.StartDateTime).TotalDays;
            var weeksCount = getWeeksCount(requestModel.StartDateTime, requestModel.EndDateTime);
 
            for (int i = 0; i < weeksCount; i++)
            {
                DateTime weekStartDate;
                DateTime weekEndDate;
                //Base start condition
                switch (i)
                {
                    case 0:
                        weekStartDate = requestModel.StartDateTime;
                        weekEndDate = requestModel.StartDateTime.StartOfWeek(DayOfWeek.Monday).AddDays(6);
                        break;
                    default:
                        // ReSharper disable once CompareOfFloatsByEqualityOperator
                        if (i + 1 == weeksCount)
                        {
                            weekStartDate = list[i - 1].WeekEndDate.AddDays(1);
                            weekEndDate = requestModel.EndDateTime;
                        }
                        else
                        {
                            weekStartDate = list[i - 1].WeekEndDate.AddDays(1);
                            weekEndDate = list[i - 1].WeekEndDate.AddDays(7);
                        }
                        break;
                }
                list.Add(new ChecksHistoryModel
                {
                    WeekStartDate = weekStartDate,
                    WeekEndDate = weekEndDate
                });
            }

            var recordsList = list.OrderByDescending(x => x.CreatedDate);
            var pageNumber = requestModel.page ?? 1;
            return PartialView(Views.ChecksHistoricViews.ShoesCleaningHistory, recordsList.ToPagedList(pageNumber, GetPageSize()));
        }

        private int getWeeksCount(DateTime periodStart, DateTime periodEnd)
        {
            const DayOfWeek FIRST_DAY_OF_WEEK = DayOfWeek.Monday;
            const DayOfWeek LAST_DAY_OF_WEEK = DayOfWeek.Sunday;
            const int DAYS_IN_WEEK = 7;

            DateTime firstDayOfWeekBeforeStartDate;
            int daysBetweenStartDateAndPreviousFirstDayOfWeek = (int)periodStart.DayOfWeek - (int)FIRST_DAY_OF_WEEK;
            if (daysBetweenStartDateAndPreviousFirstDayOfWeek >= 0)
            {
                firstDayOfWeekBeforeStartDate = periodStart.AddDays(-daysBetweenStartDateAndPreviousFirstDayOfWeek);
            }
            else
            {
                firstDayOfWeekBeforeStartDate = periodStart.AddDays(-(daysBetweenStartDateAndPreviousFirstDayOfWeek + DAYS_IN_WEEK));
            }

            DateTime lastDayOfWeekAfterEndDate;
            int daysBetweenEndDateAndFollowingLastDayOfWeek = (int)LAST_DAY_OF_WEEK - (int)periodEnd.DayOfWeek;
            if (daysBetweenEndDateAndFollowingLastDayOfWeek >= 0)
            {
                lastDayOfWeekAfterEndDate = periodEnd.AddDays(daysBetweenEndDateAndFollowingLastDayOfWeek);
            }
            else
            {
                lastDayOfWeekAfterEndDate = periodEnd.AddDays(daysBetweenEndDateAndFollowingLastDayOfWeek + DAYS_IN_WEEK);
            }

            var weeksCount = 1 + (int)((lastDayOfWeekAfterEndDate - firstDayOfWeekBeforeStartDate).TotalDays / DAYS_IN_WEEK);

            return weeksCount;
        }
        #endregion

        #region Method to get Canteen Fridge Temperature Record Historic Data
        /// <summary>
        /// Method to get Canteen Fridge Temperatue Historic Data
        /// </summary>
        [HttpGet]
        public virtual ActionResult CanteenFridgeTemperatureHistory(ChecksHistoryRequestModel requestModel)
        {
            var list = new List<ChecksHistoryModel>();
            var canteenFridgeTemperatureList = _canteenTempService.GetCanteenFridgeTemperatureHistory(requestModel.StartDateTime, requestModel.EndDateTime).ToList();
            var daysCount = (requestModel.EndDateTime - requestModel.StartDateTime).TotalDays + 1;

            for (var i = 0; i < daysCount; i++)
            {
                var nextDate = requestModel.StartDateTime.AddDays(i);
                var exist = canteenFridgeTemperatureList.Any(x => x.Sys_CreatedDateTime != null && x.Sys_CreatedDateTime.Value.Date == nextDate.Date);

                //Enter Missing Date
                if (!exist)
                {
                    list.Add(new ChecksHistoryModel
                    {
                        CreatedDate = nextDate,
                        IsPerformed = false
                    });
                }
                //Enter Found Records
                else
                {
                    try
                    {
                        var filteredList = canteenFridgeTemperatureList.Where(x => x.Sys_CreatedDateTime != null && x.Sys_CreatedDateTime.Value.Date == nextDate.Date);
                        list.AddRange(filteredList.Select(item => new ChecksHistoryModel
                        {
                            Id = item.iCanteenFridgeTempRecordId,
                            IsPerformed = true,
                            CreatedDate = item.Sys_CreatedDateTime,
                            CreatedBy = item.AspNetUser1 != null ? item.AspNetUser1.FirstName + " " + item.AspNetUser1.LastName : string.Empty,
                            ModifiedBy = item.AspNetUser != null ? item.AspNetUser.FirstName + " " + item.AspNetUser.LastName : string.Empty,
                            Fridge1Temp = item.Fridge1Temp,
                            Fridge2Temp = item.Fridge2Temp,
                            Fridge3Temp = item.Fridge3Temp,
                            Fridge1Action = item.vFridge1Action,
                            Fridge2Action = item.vFridge2Action,
                            Fridge3Action = item.vFridge3Action
                        }));
                    }
                    catch (Exception ex)
                    {
                        AuditScope.CreateAndSave("Error Getting Canteen Fridge Record: ", new { User = User.Identity.GetUserId(), Exception = ex.Message });
                        throw;
                    }
                }
            }

            var recordsList = list.OrderByDescending(x => x.CreatedDate);
            var pageNumber = requestModel.page ?? 1;
            return PartialView(Views.ChecksHistoricViews.CanteenFridgeTemperatureRecordHistory, recordsList.ToPagedList(pageNumber, GetPageSize()));
        }
        #endregion

        #region Method to get Daily Cleaning Checklist Historic Data
        /// <summary>
        /// Method to get Daily Cleaning Checklist Historic Data
        /// </summary>
        [HttpGet]
        public virtual ActionResult DailyCleaningChecklistHistory(ChecksHistoryRequestModel requestModel)
        {
            var list = new List<ChecksHistoryModel>();
            var dailyCleaningChecklistList = _cleaningCheckListDailyService.GetDailyCleaningChecklistHistory(requestModel.StartDateTime, requestModel.EndDateTime).ToList();
            var daysCount = (requestModel.EndDateTime - requestModel.StartDateTime).TotalDays + 1;
            var areas = _areaService.GetAll().ToList();

            for (var i = 0; i < daysCount; i++)
            {
                var nextDate = requestModel.StartDateTime.AddDays(i);
                var exist = dailyCleaningChecklistList.Any(x => x.Sys_CreatedDateTime != null && x.Sys_CreatedDateTime.Value.Date == nextDate.Date);
                //Enter Missing Date
                if (!exist)
                {
                    //Entering Mising Date Records For All Areas.
                    // ReSharper disable once LoopCanBeConvertedToQuery
                    foreach (var area in areas)
                    {
                        list.Add(new ChecksHistoryModel
                        {
                            CreatedDate = nextDate,
                            IsPerformed = false,
                            Area = area.vAreaName
                        });
                    }
                }
                //Enter Found Records
                else
                {
                    foreach (var area in areas)
                    {
                        var areaDatedRecord = dailyCleaningChecklistList.FirstOrDefault(d => (d.iAreaId == area.iAreaId) && d.Sys_CreatedDateTime != null && d.Sys_CreatedDateTime.Value.Date == nextDate.Date);

                        if (areaDatedRecord != null)
                        {
                            try
                            {
                                list.Add(new ChecksHistoryModel
                                {
                                    Id = areaDatedRecord.iCleaningChecklistDailyId,
                                    IsPerformed = true,
                                    // ReSharper disable once PossibleInvalidOperationException
                                    CreatedDate = (DateTime)areaDatedRecord.Sys_CreatedDateTime,
                                    CreatedBy = areaDatedRecord.AspNetUser != null ? areaDatedRecord.AspNetUser.FirstName + " " + areaDatedRecord.AspNetUser.LastName : "",
                                    ModifiedBy = areaDatedRecord.AspNetUser1 != null ? areaDatedRecord.AspNetUser1.FirstName + " " + areaDatedRecord.AspNetUser1.LastName : "",
                                    Area = areaDatedRecord.tArea.vAreaName,
                                    AreaId = areaDatedRecord.tArea.iAreaId
                                });
                            }
                            catch (Exception ex)
                            {
                                AuditScope.CreateAndSave("Error getting daily check history: ", new { User = User.Identity.GetUserId(), Exception = ex.Message });
                                throw;
                            }
                        }
                        else
                        {
                            try
                            {
                                list.Add(new ChecksHistoryModel
                                {
                                    CreatedDate = nextDate,
                                    IsPerformed = false,
                                    Area = area.vAreaName
                                });
                            }
                            catch (Exception ex)
                            {
                                AuditScope.CreateAndSave("Error getting daily check history: ", new { User = User.Identity.GetUserId(), Exception = ex.Message });
                                throw;
                            }
                        }
                    }
                }
            }

            var recordsList = list.OrderByDescending(x => x.CreatedDate);
            var pageNumber = requestModel.page ?? 1;
            return PartialView(Views.ChecksHistoricViews.DailyCleaningChecklistHistory, recordsList.ToPagedList(pageNumber, GetPageSize()));
        }
        #endregion

        #region Method to get Startup Checklist - LR Cooking Room Historic Data
        /// <summary>
        /// Method to get Startup Checklist - LR Cooking Room Historic Data
        /// </summary>
        [HttpGet]
        public virtual ActionResult StartupChecklistHistory(ChecksHistoryRequestModel requestModel)
        {
            var list = new List<ChecksHistoryModel>();
            var startupChecklistList = _startupCheckService.GetStartupChecklistHistory(requestModel.StartDateTime, requestModel.EndDateTime).ToList();
            var daysCount = (requestModel.EndDateTime - requestModel.StartDateTime).TotalDays + 1;

            for (var i = 0; i < daysCount; i++)
            {
                var nextDate = requestModel.StartDateTime.AddDays(i).Date;
                var exist = startupChecklistList.Any(x => x.Sys_CreatedDateTime != null && x.Sys_CreatedDateTime.Value.Date == nextDate.Date);

                //Enter Missing Date
                if (!exist)
                {
                    list.Add(new ChecksHistoryModel { CreatedDate = nextDate, IsPerformed = false });
                }
                //Enter Found Records
                else
                {
                    try
                    {
                        var filteredList =
                        startupChecklistList.Where(
                            x => x.Sys_CreatedDateTime != null && x.Sys_CreatedDateTime.Value.Date == nextDate.Date).GroupBy(y => y.Sys_CreatedDateTime != null && y.Sys_CreatedDateTime.Value.Date == nextDate.Date).Select(z => z.FirstOrDefault());
                        list.AddRange(filteredList.Select(item => new ChecksHistoryModel
                        {
                            Id = item.iStartUpcheckDailyId,
                            IsPerformed = true,
                            CreatedDate = item.Sys_CreatedDateTime,
                            CreatedBy = item.AspNetUser != null ? item.AspNetUser.FirstName + " " + item.AspNetUser.LastName : string.Empty,
                            ModifiedBy = item.AspNetUser1 != null ? item.AspNetUser1.FirstName + " " + item.AspNetUser1.LastName : string.Empty,
                            vCheck = item.tStartUpCheckList.vCheck
                        }));
                    }
                    catch (Exception ex)
                    {
                        AuditScope.CreateAndSave("Error getting startup check history: ", new { User = User.Identity.GetUserId(), Exception = ex.Message });
                        throw;
                    }
                }
            }

            var recordsList = list.OrderByDescending(x => x.CreatedDate);
            var pageNumber = requestModel.page ?? 1;
            return PartialView(Views.ChecksHistoricViews.StartupChecklistHistory, recordsList.ToPagedList(pageNumber, GetPageSize()));
        }
        #endregion

        #region Get Startup Checklist History By Id
        /// <summary>
        /// GetStartupChecklistHistoryById
        /// </summary>
        /// <param name="id"></param>
        public virtual JsonResult GetStartupChecklistHistoryById(long id)
        {
            List<StartupCheckDaily> list = new List<StartupCheckDaily>();
            var startUpDailyList = _startupCheckService.GetStartupChecklistHistoryById(id);
            // ReSharper disable once LoopCanBeConvertedToQuery
            foreach (var tStartUpcheckDailyItem in startUpDailyList)
            {
                var item = Mapper.Map<tStartUpcheckDaily, StartupCheckDaily>(tStartUpcheckDailyItem);
                list.Add(item);
            }
            return Json(list, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Get Daily Checks History By Id
        /// <summary>
        /// Get Daily Checks History By Id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="areaId"></param>
        public virtual JsonResult GetDailyChecksHistoryById(long id, long areaId)
        {
            List<CleaningCheckListDaily> list = new List<CleaningCheckListDaily>();
            var checksList = _cleaningCheckListDailyService.GetDailyCleaningChecklistHistoryById(id, areaId);
            // ReSharper disable once LoopCanBeConvertedToQuery
            foreach (var item in checksList)
            {
                var mappedItem = Mapper.Map<CleaningChecklistDaily, CleaningCheckListDaily>(item);
                list.Add(mappedItem);
            }
            return Json(list, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Get Canteen Fridge Checklist History By Id
        /// <summary>
        /// Get Canteen Fridge Checklist History By Id
        /// </summary>
        /// <param name="id"></param>
        public virtual JsonResult GetCanteenFridgeChecklistHistoryById(long id)
        {
            var canteenFridgeRecord = _canteenTempService.GetCanteenFridgeChecklistHistoryById(id);
            var item = Mapper.Map<tCanteenFridgeTempRecordDaily, CanteenFridgeTempRecord>(canteenFridgeRecord);

            return Json(item, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Get Daily Truck Inspection History By Id
        /// <summary>
        /// Get Daily Truck Inspection History By Id
        /// </summary>
        /// <param name="id"></param>
        public virtual JsonResult GetDailyTruckInspectionHistoryById(long id)
        {
            var dailyTruckInspectionRecord = _dailyTruckInspectionService.GetDailyTruckInspectionHistoryById(id);

            tDailyTruckInspection item = new tDailyTruckInspection()
            {
                iTruckInspectionId = dailyTruckInspectionRecord.iTruckInspectionId,
                dDateTime = dailyTruckInspectionRecord.dDateTime,
                vTruckType = dailyTruckInspectionRecord.vTruckType,
                vTruckNumber = dailyTruckInspectionRecord.vTruckNumber,
                vTruckMake = dailyTruckInspectionRecord.vTruckMake,
                iMaxLoad = dailyTruckInspectionRecord.iMaxLoad,
                iPropulsion = dailyTruckInspectionRecord.iPropulsion,
                vChanges = dailyTruckInspectionRecord.vChanges,
                bForkArms = dailyTruckInspectionRecord.bForkArms,
                bMast = dailyTruckInspectionRecord.bMast,
                bCarriagePlate = dailyTruckInspectionRecord.bCarriagePlate,
                bLiftChainAndLubrication = dailyTruckInspectionRecord.bLiftChainAndLubrication,
                bWheelAndTyres = dailyTruckInspectionRecord.bWheelAndTyres,
                bBackRest = dailyTruckInspectionRecord.bBackRest,
                bSeatAndSeatBelt = dailyTruckInspectionRecord.bSeatAndSeatBelt,
                bSteering = dailyTruckInspectionRecord.bSteering,
                bLightAndIndicators = dailyTruckInspectionRecord.bLightAndIndicators,
                bHornAndBeeper = dailyTruckInspectionRecord.bHornAndBeeper,
                bMastController = dailyTruckInspectionRecord.bMastController,
                bHandAndParkingBreak = dailyTruckInspectionRecord.bHandAndParkingBreak,
                bDrivingAndServiceBreak = dailyTruckInspectionRecord.bDrivingAndServiceBreak,
                bHydraulics = dailyTruckInspectionRecord.bHydraulics,
                bOilLevel = dailyTruckInspectionRecord.bOilLevel,
                bFuelAndPower = dailyTruckInspectionRecord.bFuelAndPower,
                bBatteries = dailyTruckInspectionRecord.bBatteries,
                bLPG = dailyTruckInspectionRecord.bLPG,
                vActionTaken = dailyTruckInspectionRecord.vActionTaken,
                Sys_CreatedDateTime = dailyTruckInspectionRecord.Sys_CreatedDateTime,
                Sys_CreatedBy = dailyTruckInspectionRecord.Sys_CreatedBy,
                Sys_ModifyDateTime = dailyTruckInspectionRecord.Sys_ModifyDateTime,
                Sys_ModifyBy = dailyTruckInspectionRecord.Sys_ModifyBy

            };

            return Json(item, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Get Goods Out Freezer Unit History By Id
        /// <summary>
        /// Get Goods Out Freezer Unit History By Id
        /// </summary>
        /// <param name="id"></param>
        public virtual JsonResult GetGoodsOutFreezerUnitHistoryById(long id)
        {
            var goodsOutFreezerUnitRecord = _goodsOutFreezerUnit.GetGoodsOutFreezerUnitHistoryById(id);
            tGoodsOutFreezerUnit item = new tGoodsOutFreezerUnit()
            {
                iGoodsOutFreezerUnit = goodsOutFreezerUnitRecord.iGoodsOutFreezerUnit,
                dTime = goodsOutFreezerUnitRecord.dTime,
                iFreezerTemp1 = goodsOutFreezerUnitRecord.iFreezerTemp1,
                iFreezerTemp2 = goodsOutFreezerUnitRecord.iFreezerTemp2,
                iFreezerTemp3 = goodsOutFreezerUnitRecord.iFreezerTemp3,
                iFreezerTemp4 = goodsOutFreezerUnitRecord.iFreezerTemp4,
                iFreezerTemp5 = goodsOutFreezerUnitRecord.iFreezerTemp5,
                vComments = goodsOutFreezerUnitRecord.vComments
            };

            return Json(item, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Get Startup Checks EP Goods Out History By Id
        /// <summary>
        /// Get Startup Checks EP Goods Out History By Id
        /// </summary>
        /// <param name="id"></param>
        public virtual JsonResult GetStartupChecksEPGoodsOutHistoryById(long id)
        {
            var startupChecksEPGoodsOutRecord = _startupCheckEpGoodsOutService.GetStartupChecksEpGoodsOutHistoryById(id);

            tStartupChecksEPGoodsOut item = new tStartupChecksEPGoodsOut()
            {
                iStartupChecksEPGoodsOut = startupChecksEPGoodsOutRecord.iStartupChecksEPGoodsOut,
                dDateTime = startupChecksEPGoodsOutRecord.dDateTime,
                bSwitchLightsOn = startupChecksEPGoodsOutRecord.bSwitchLightsOn,
                bCheckElectric = startupChecksEPGoodsOutRecord.bCheckElectric,
                bSwitchOnComputer = startupChecksEPGoodsOutRecord.bSwitchOnComputer,
                bCheckBlade = startupChecksEPGoodsOutRecord.bCheckBlade,
                bCheckHoldingFreezers = startupChecksEPGoodsOutRecord.bCheckHoldingFreezers,
                bCheckDisplayTemperature = startupChecksEPGoodsOutRecord.bCheckDisplayTemperature,
                bRingOffice = startupChecksEPGoodsOutRecord.bRingOffice,
                bCollectOrder = startupChecksEPGoodsOutRecord.bCollectOrder,
                bCheckAllStaffComply = startupChecksEPGoodsOutRecord.bCheckAllStaffComply,
                bCheckAllStaffSuitably = startupChecksEPGoodsOutRecord.bCheckAllStaffSuitably

            };

            return Json(item, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Get Shoe Cleaning Checklist History By Id
        /// <summary>
        /// Get Shoe Cleaning Checklist History By Id
        /// </summary>
        /// <param name="id"></param>
        public virtual JsonResult GetShoeCleaningChecklistHistoryById(long id)
        {
            var shoeCleaningRecord = _shoeCleaningCheckService.GetShoeCleaningChecklistHistoryById(id);
            var item = Mapper.Map<tShoeCleaningRecrdWeekly, ShoeCleaningCheck>(shoeCleaningRecord);

            return Json(item, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Submit Daily Truck Inspection Check
        /// <summary>
        /// Submit Daily Truck Inspection Check
        /// </summary>
        /// <param name="dailyTruckInspectionModel"></param>
        /// <returns></returns>
        [HttpPost]
        public virtual JsonResult SubmitDailyTruckInspectionCheck(DailyTruckInspection dailyTruckInspectionModel)
        {
            try
            {
                dailyTruckInspectionModel.dDateTime = DateTime.Now;
                dailyTruckInspectionModel.Sys_CreatedBy = User.Identity.GetUserId();
                dailyTruckInspectionModel.Sys_CreatedDateTime = DateTime.Now;
                _dailyTruckInspectionService.Create(Mapper.Map<DailyTruckInspection, tDailyTruckInspection>(dailyTruckInspectionModel));
                AuditScope.CreateAndSave("Checks: Saving Daily Truck Inspection Check", new { User = User.Identity.GetUserId() });
                return Json(new { result = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                CreateAuditLog("Error: Saving Daily Truck Inspection Check", new { exception = ex.Message });
                return Json(new { result = false }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Can Perform Goods Out Freezer Unit Check
        /// <summary>
        /// Can Perform Goods Out Freezer Unit Check
        /// </summary>
        /// <returns>bool</returns>
        public virtual JsonResult CanPerformGoodsOutFreezerUnitCheck()
        {
            var result = true;
            var record = _goodsOutFreezerUnit.GetAll().OrderByDescending(x => x.Sys_CreatedDateTime).FirstOrDefault();

            // ReSharper disable once PossibleInvalidOperationException
            if ((record != null) && (record.Sys_CreatedDateTime.Value.ToString("d/M/yyyy") == DateTime.Now.ToString("d/M/yyyy")))
            {
                //Checking if the check has already been filled.
                if (((GetTimePeriod() == ClockInOutPeriod.Morning) && (record.Sys_CreatedDateTime.Value.Hour < 12)) ||
                    ((GetTimePeriod() == ClockInOutPeriod.Lunch) && (record.Sys_CreatedDateTime.Value.Hour > 12)))
                {
                    result = false;
                }
            }

            return Json(new { value = result }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Check Time Period for Goods Out Freezer Unit Check
        /// <summary>
        /// Check Time Period for Goods Out Freezer Unit Check
        /// </summary>
        /// <returns>bool</returns>
        private ClockInOutPeriod GetTimePeriod()
        {
            var isPm = (DateTime.Now.Hour >= 12);
            return isPm ? ClockInOutPeriod.Lunch : ClockInOutPeriod.Morning;
        }
        #endregion

        #region Submit Goods Out Freezer Unit Check
        /// <summary>
        /// Submit Startup EP Goods Out Check
        /// </summary>
        /// <param name="goodsOutFreezerUnit"></param>
        /// <returns></returns>
        [HttpPost]
        public virtual JsonResult SubmitGoodsOutFreezerUnitCheck(GoodsOutFreezerUnit goodsOutFreezerUnit)
        {
            try
            {
                goodsOutFreezerUnit.dDateTime = DateTime.Now;
                goodsOutFreezerUnit.Sys_CreatedBy = User.Identity.GetUserId();
                goodsOutFreezerUnit.Sys_CreatedDateTime = DateTime.Now;
                _goodsOutFreezerUnit.Create(Mapper.Map<GoodsOutFreezerUnit, tGoodsOutFreezerUnit>(goodsOutFreezerUnit));
                AuditScope.CreateAndSave("Checks: Saving Goods Out Freezer Unit Check", new { User = User.Identity.GetUserId() });
                return Json(new { result = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                CreateAuditLog("Error in Saving Goods Out Freezer Unit Check", new { exception = ex.Message });
                return Json(new { result = false }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Submit Startup EP Goods Out Check
        /// <summary>
        /// Submit Startup EP Goods Out Check
        /// </summary>
        /// <param name="startupChecksEpGoodsOut"></param>
        /// <returns></returns>
        [HttpPost]
        // ReSharper disable once InconsistentNaming
        public virtual JsonResult SubmitStartupEPGoodsOutCheck(StartupChecksEPGoodsOut startupChecksEpGoodsOut)
        {
            try
            {
                startupChecksEpGoodsOut.dDateTime = DateTime.Now;
                startupChecksEpGoodsOut.Sys_CreatedBy = User.Identity.GetUserId();
                startupChecksEpGoodsOut.Sys_CreatedDateTime = DateTime.Now;
                _startupCheckEpGoodsOutService.Create(Mapper.Map<StartupChecksEPGoodsOut, tStartupChecksEPGoodsOut>(startupChecksEpGoodsOut));
                AuditScope.CreateAndSave("Saving Startup EP Goods Out Check", new { User = User.Identity.GetUserId() });
                return Json(new { result = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                CreateAuditLog("Error in Saving Startup EP Goods Out Check", new { exception = ex.Message });
                return Json(new { result = false }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
    }
}