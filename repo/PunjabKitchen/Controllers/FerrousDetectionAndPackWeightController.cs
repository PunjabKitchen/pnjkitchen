﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Audit.Core;
using AutoMapper;
using Microsoft.AspNet.Identity;
using PunjabKitchen.Models;
using PunjabKitchenBLL.Interfaces;
using PunjabKitchenEntities.EntityModel;
using PagedList;

namespace PunjabKitchen.Controllers
{
    [Authorize(Roles = "Admin,Packing Manager,Packing  Staff")]
    public partial class FerrousDetectionAndPackWeightController : BaseController
    {
        #region Data Members
        readonly IFMDetectionAndPackWeightRecordService _fMDetectionAndPackingWeightService;
        readonly ICookingPlanService _cookingPlanService;
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="fMDetectionAndPackingWeightService"></param>
        /// <param name="cookingPlanService"></param>
        public FerrousDetectionAndPackWeightController(IFMDetectionAndPackWeightRecordService fMDetectionAndPackingWeightService, ICookingPlanService cookingPlanService)
        {
            _fMDetectionAndPackingWeightService = fMDetectionAndPackingWeightService;
            _cookingPlanService = cookingPlanService;
        }
        #endregion

        #region Create New Ferrous Detection And Pack Weight Record
        /// <summary>
        /// Create New Ferrous Detection And Pack Weight Record
        /// </summary>
        /// <returns></returns>
        public virtual ActionResult CreateFerrousDetectionAndPackWeightRecord()
        {
            return View(Views.Create, new FMDetectionAndPackWeightTest());
        }
        #endregion

        #region Get Ferrous Detection And Pack Weight Records List 
        /// <summary>
        /// Get Ferrous Detection And Pack Weight Records List 
        /// </summary>
        /// <param name="page"></param>
        /// <returns></returns>
        [HttpGet]
        public virtual ActionResult FerrousDetectionAndPackWeightRecords(int? page = 1)
        {
            var recordsList = GetFerrousDetectionAndPackWeightRecords().OrderByDescending(x => x.iDetectionPackWeightTestId);
            var pageNumber = page ?? 1;
            return View(Views.Index, recordsList.ToPagedList(pageNumber, GetPageSize()));
        }
        #endregion

        #region Return Ferrous Detection And Pack Weight Records Create New View
        /// <summary>
        /// Return Ferrous Detection And Pack Weight Records Create New View
        /// </summary>
        /// <returns></returns>
        public virtual ActionResult CreateFerrousDetectionAndPackWeightRecordCreateView()
        {
            return PartialView(Views.Create);
        }
        #endregion

        #region Return Ferrous Detection And Pack Weight Record Edit View
        /// <summary>
        /// Return Ferrous Detection And Pack Weight Record Edit View
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize(Roles = "Admin,Packing Manager")]
        public virtual ActionResult EditFerrousDetectionAndPackWeightRecord(long id)
        {
            FMDetectionAndPackWeightTest ferrousDetectionAndPackWeightRec = Mapper.Map<tFMDetectionAndPackWeightTest, FMDetectionAndPackWeightTest>(_fMDetectionAndPackingWeightService.GetById(id));
            return View(Views.Create, ferrousDetectionAndPackWeightRec);
        }
        #endregion

        #region  Get All Ferrous Detection And Pack Weight Records
        /// <summary>
        /// Get All Ferrous Detection And Pack Weight Records
        /// </summary>
        /// <returns></returns>
        private List<FMDetectionAndPackWeightTest> GetFerrousDetectionAndPackWeightRecords()
        {
            var ferrousDetectionAndPackWeightRecordList = _fMDetectionAndPackingWeightService.GetAll().Where(m => m.vTestType == "Ferrous Detection Test and Pack Weight Check");
            return ferrousDetectionAndPackWeightRecordList.Select(Mapper.Map<tFMDetectionAndPackWeightTest, FMDetectionAndPackWeightTest>).ToList();
        }
        #endregion

        #region Add/Edit
        /// <summary>
        /// Update Ferrous Detection And Pack Weight Record if found else create new
        /// </summary>
        /// <param name="ferrousDetectionAndPackWeightRecord"></param>
        /// <returns></returns>
        public virtual ActionResult PostFerrousDetectionAndPackWeightRecord(FMDetectionAndPackWeightTest ferrousDetectionAndPackWeightRecord)
        {
            try
            {
                //Edit
                if (ferrousDetectionAndPackWeightRecord.iDetectionPackWeightTestId > 0)
                {
                    var savedFerrousDetectionAndPackWeightRecord = Mapper.Map<FMDetectionAndPackWeightTest, tFMDetectionAndPackWeightTest>(ferrousDetectionAndPackWeightRecord);

                    savedFerrousDetectionAndPackWeightRecord.Sys_ModifyBy = User.Identity.GetUserId();
                    savedFerrousDetectionAndPackWeightRecord.Sys_ModifyDateTime = DateTime.Now;
                    // ReSharper disable once PossibleInvalidOperationException
                    savedFerrousDetectionAndPackWeightRecord.tCookingPlan = _cookingPlanService.GetById((long)ferrousDetectionAndPackWeightRecord.iCookingPlanId);
                    savedFerrousDetectionAndPackWeightRecord.vTestType = "Ferrous Detection Test and Pack Weight Check";
                    _fMDetectionAndPackingWeightService.Update(savedFerrousDetectionAndPackWeightRecord);
                    AuditScope.CreateAndSave("Editing Ferrous Detection And Pack Weight Record", new { User = User.Identity.GetUserId() });
                }
                //Add
                else
                {
                    ferrousDetectionAndPackWeightRecord.Sys_CreatedBy = User.Identity.GetUserId();
                    ferrousDetectionAndPackWeightRecord.Sys_CreatedDateTime = DateTime.Now;
                    ferrousDetectionAndPackWeightRecord.tCookingPlan = null;
                    ferrousDetectionAndPackWeightRecord.vTestType = "Ferrous Detection Test and Pack Weight Check";
                    try
                    {
                        _fMDetectionAndPackingWeightService.Create(Mapper.Map<FMDetectionAndPackWeightTest, tFMDetectionAndPackWeightTest>(ferrousDetectionAndPackWeightRecord));
                        AuditScope.CreateAndSave("Adding Ferrous Detection And Pack Weight Record", new { User = User.Identity.GetUserId() });
                    }
                    catch (Exception ex)
                    {
                        AuditScope.CreateAndSave("Error Ferrous Detection And Pack Weight Record: ", new { User = User.Identity.GetUserId(), Exception = ex.Message });
                        return RedirectToAction(MVC.Home.Index());
                    }
                }
                return RedirectToAction(MVC.FerrousDetectionAndPackWeight.FerrousDetectionAndPackWeightRecords());
            }
            catch (Exception ex)
            {
                AuditScope.CreateAndSave("Error Saving Ferrous Detection And Pack Weight Record: ", new { User = User.Identity.GetUserId(), Exception = ex.Message });
                return RedirectToAction(MVC.Home.Index());
            }
        }
        #endregion

        #region Print Record as Report

        /// <summary>
        /// Print Record
        /// </summary>
        /// <param name="pId">Id of the record to print</param>
        /// <returns>RDLC Report View</returns>
        public virtual ActionResult PrintRecord(Int32 pId)
        {
            try
            {
                return View("~/Views/Report/ReportPreview.aspx", new Report { ReportName = PkReportNames.FerrousDetectionAndPackWeightRecord, Id = pId, Service = _fMDetectionAndPackingWeightService });
            }
            catch (Exception ex)
            {
                AuditScope.CreateAndSave("Error getting report of Ferrous Detection And Pack Weight Record", new { User = User.Identity.GetUserId() });
                // ReSharper disable once PossibleIntendedRethrow
                throw ex;
            }
        }
        #endregion
    }
}