﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Audit.Core;
using AutoMapper;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using PunjabKitchen.Models;
using PunjabKitchen.Properties;
using PunjabKitchenBLL.Interfaces;
using PunjabKitchenEntities.EntityModel;
using PagedList;

namespace PunjabKitchen.Controllers
{
    [Authorize(Roles = "Admin,Packing Manager,Packing  Staff")]
    public partial class PureeMealBatchAssemblyController : BaseController
    {
        #region Data Members
        readonly IPureeMealBatchAssemblyService _pureeMealBatchAssemblyService;
        readonly IPureeMealBatchAssemblyDetailService _pureeMealBatchAssemblyDetailService;
        readonly IPureeMealBatchAssemblyPackerDetailService _pureeMealBatchAssemblyPackerDetailService;
        readonly IPureeMealInBlastService _pureeMealInBlastService;
        readonly IUnitService _unitService;
        readonly ICookingPlanService _cookingPlanService;
        #endregion

        #region User Manager
        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            // ReSharper disable once UnusedMember.Local
            private set
            {
                _userManager = value;
            }
        }

        private ApplicationRoleManager _roleManager;
        public ApplicationRoleManager RoleManager
        {
            get
            {
                return _roleManager ?? HttpContext.GetOwinContext().Get<ApplicationRoleManager>();
            }
            // ReSharper disable once UnusedMember.Local
            private set
            {
                _roleManager = value;
            }
        }
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="pureeMealBatchAssemblyService"></param>
        /// <param name="pureeMealBatchAssemblyDetailService"></param>
        /// <param name="pureeMealBatchAssemblyPackerDetailService"></param>
        /// <param name="pureeMealInBlastService"></param>
        /// <param name="unitService"></param>
        /// <param name="cookingPlanService"></param>
        public PureeMealBatchAssemblyController(IPureeMealBatchAssemblyService pureeMealBatchAssemblyService, IPureeMealBatchAssemblyDetailService pureeMealBatchAssemblyDetailService, IPureeMealBatchAssemblyPackerDetailService pureeMealBatchAssemblyPackerDetailService, IPureeMealInBlastService pureeMealInBlastService, IUnitService unitService, ICookingPlanService cookingPlanService)
        {
            _pureeMealBatchAssemblyService = pureeMealBatchAssemblyService;
            _pureeMealBatchAssemblyDetailService = pureeMealBatchAssemblyDetailService;
            _pureeMealBatchAssemblyPackerDetailService = pureeMealBatchAssemblyPackerDetailService;
            _pureeMealInBlastService = pureeMealInBlastService;
            _unitService = unitService;
            _cookingPlanService = cookingPlanService;
        }
        #endregion

        #region Create New Puree Meal Batch Assembly Record
        /// <summary>
        /// Create New Puree Meal Batch Assembly Record
        /// </summary>
        /// <returns></returns>
        public virtual async Task<ActionResult> CreatePureeMealBatchAssemblyRecord()
        {
            var users = UserManager.Users.ToList();
            // ReSharper disable once InconsistentNaming
            var Roles = await RoleManager.Roles.ToListAsync();
            // ReSharper disable once PossibleNullReferenceException
            var roleId = Roles.FirstOrDefault(m => m.Name == PKRoles.PackingStaff).Id;
            var list = users.Where(x => x.Roles.Any(y => y.RoleId == roleId)).ToList();

            var userslist = new List<Data>();

            // ReSharper disable once LoopCanBeConvertedToQuery
            foreach (var item in list)
            {
                userslist.Add(new Data(item.Id, item.FirstName + " " + item.LastName));
            }

            ViewBag.PackersList = new SelectList(userslist, "IntegerData", "StringData");
            return View(Views.Create, new PureeMealBatchAssemblyCombinedModel());
        }
        #endregion

        #region Show Records List
        /// <summary>
        /// Show Records List
        /// </summary>
        /// <returns></returns>
        public virtual ActionResult ShowRecordsList(int iPureeMeanBatchAssemblyId, string mealComponent)
        {
            ViewBag.MealComponent = mealComponent;
            // ReSharper disable once RedundantAssignment
            var recordsList = new List<tPureeMealBatchAssemblyDetail>();

            if (mealComponent == "Shape")
            { recordsList = _pureeMealBatchAssemblyDetailService.GetAll().Where(m => (m.iPureeMeanBatchAssemblyId == iPureeMeanBatchAssemblyId) && (m.vShape != null)).ToList(); }
            else if (mealComponent == "Sauce")
            { recordsList = _pureeMealBatchAssemblyDetailService.GetAll().Where(m => (m.iPureeMeanBatchAssemblyId == iPureeMeanBatchAssemblyId) && (m.vSauce != null)).ToList(); }
            else
            { recordsList = _pureeMealBatchAssemblyDetailService.GetAll().Where(m => (m.iPureeMeanBatchAssemblyId == iPureeMeanBatchAssemblyId) && (m.vMash != null)).ToList(); }

            return PartialView(Views.RecordsList, recordsList.Select(Mapper.Map<tPureeMealBatchAssemblyDetail, PureeMealBatchAssemblyDetail>));
        }
        #endregion

        #region Get Puree Meal Batch Assembly Records List
        /// <summary>
        /// Get Puree Meal Batch Assembly Records List
        /// </summary>
        /// <param name="page"></param>
        /// <returns></returns>
        [HttpGet]
        public virtual ActionResult PureeMealBatchAssemblyRecords(int? page = 1)
        {
            var recordsList = GetPureeMealBatchAssemblyRecords().OrderByDescending(x => x.iPureeMeanBatchAssemblyId);
            var pageNumber = page ?? 1;
            return View(Views.Index, recordsList.ToPagedList(pageNumber, GetPageSize()));
        }
        #endregion

        #region Return Puree Meal Batch Assembly Record Edit View
        /// <summary>
        /// Return Puree Meal Batch Assembly Record Edit View
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize(Roles = "Admin,Packing Manager")]
        public virtual async Task<ActionResult> EditPureeMealBatchAssemblyRecord(long id)
        {
            var users = UserManager.Users.ToList();
            // ReSharper disable once InconsistentNaming
            var Roles = await RoleManager.Roles.ToListAsync();
            // ReSharper disable once PossibleNullReferenceException
            var roleId = Roles.FirstOrDefault(m => m.Name == PKRoles.PackingStaff).Id;
            var list = users.Where(x => x.Roles.Any(y => y.RoleId == roleId)).ToList();

            var userslist = new List<Data>();

            // ReSharper disable once LoopCanBeConvertedToQuery
            foreach (var item in list)
            {
                userslist.Add(new Data(item.Id, item.FirstName + " " + item.LastName));
            }

            PureeMealBatchAssemblyCombinedModel pureeCombinedModel = new PureeMealBatchAssemblyCombinedModel();

            ViewBag.PackersList = new SelectList(userslist, "IntegerData", "StringData");
            // ReSharper disable once SuggestVarOrType_SimpleTypes
            PureeMealBatchAssembly pureeMealBatchRec = Mapper.Map<tPureeMeanBatchAssembly, PureeMealBatchAssembly>(_pureeMealBatchAssemblyService.GetById(id));
            var pureeMealBatchBlastRec = _pureeMealInBlastService.GetAll().Where(m => m.iPureeMeanBatchAssemblyId == pureeMealBatchRec.iPureeMeanBatchAssemblyId).ToList();
            pureeCombinedModel.BlastFreezersList = pureeMealBatchBlastRec.Select(Mapper.Map<tPureeMealInBlastFreezer, PureeMealInBlastFreezer>);
            var packersList = _pureeMealBatchAssemblyPackerDetailService.GetAll().Where(m => m.iPureeMeanBatchAssemblyId == pureeMealBatchRec.iPureeMeanBatchAssemblyId).ToList();

            pureeCombinedModel.packersList = packersList[0].UserId;
            // ReSharper disable once SuggestVarOrType_BuiltInTypes
            for (int i = 1; i < packersList.Count; i++)
            {
                pureeCombinedModel.packersList += "," + packersList[i].UserId;
            }

            pureeCombinedModel.PureeMealBatchAssembly = pureeMealBatchRec;

            return View(Views.Create, pureeCombinedModel);
        }
        #endregion

        #region  Get All Puree Meal Batch Assembly Records
        /// <summary>
        /// Get All Puree Meal Batch Assembly Records
        /// </summary>
        /// <returns></returns>
        // ReSharper disable once ReturnTypeCanBeEnumerable.Local
        private List<PureeMealBatchAssembly> GetPureeMealBatchAssemblyRecords()
        {
            var pureeMealBatchAssemblyRecordsList = _pureeMealBatchAssemblyService.GetAll();
            return pureeMealBatchAssemblyRecordsList.Select(Mapper.Map<tPureeMeanBatchAssembly, PureeMealBatchAssembly>).ToList();
        }
        #endregion

        #region Add/Edit
        /// <summary>
        /// Update Puree Meal Batch Assembly record if found else create new
        /// </summary>
        /// <param name="pureeMealCombinedModel"></param>
        /// <returns></returns>
        public virtual JsonResult PostPureeMealBatchAssemblyRecord(PureeMealBatchAssemblyCombinedModel pureeMealCombinedModel)
        {
            // ReSharper disable once InconsistentNaming
            // ReSharper disable once RedundantAssignment
            var savedRecordID = 0;
            // ReSharper disable once SuggestVarOrType_Elsewhere
            string[] packersName = pureeMealCombinedModel.packersList.Split(',');

            //Edit
            if (pureeMealCombinedModel.PureeMealBatchAssembly.iPureeMeanBatchAssemblyId > 0)
            {
                try
                {
                    savedRecordID = pureeMealCombinedModel.PureeMealBatchAssembly.iPureeMeanBatchAssemblyId;
                    var savediPureeMealBatchRecord = Mapper.Map<PureeMealBatchAssembly, tPureeMeanBatchAssembly>(pureeMealCombinedModel.PureeMealBatchAssembly);

                    savediPureeMealBatchRecord.Sys_ModifyBy = User.Identity.GetUserId();
                    savediPureeMealBatchRecord.Sys_ModifyDateTime = DateTime.Now;
                    // ReSharper disable once PossibleInvalidOperationException
                    savediPureeMealBatchRecord.tCookingPlan = _cookingPlanService.GetById((long)pureeMealCombinedModel.PureeMealBatchAssembly.iCookingPlanId);
                    _pureeMealBatchAssemblyService.Update(savediPureeMealBatchRecord);
                    ValidatePackerRecords(packersName, pureeMealCombinedModel.PureeMealBatchAssembly);
                    AuditScope.CreateAndSave("Editing Puree Meal Batch Assembly Record", new { User = User.Identity.GetUserId() });
                    return Json(new { status = "Edited", savedRecordID }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    AuditScope.CreateAndSave("Error Editing Puree Meal Batch Assmebly Record: ", new { User = User.Identity.GetUserId(), Exception = ex.Message });
                    throw;
                }
            }
            //Add
            else
            {
                try
                {
                    pureeMealCombinedModel.PureeMealBatchAssembly.Sys_CreatedBy = User.Identity.GetUserId();
                    pureeMealCombinedModel.PureeMealBatchAssembly.Sys_CreatedDateTime = DateTime.Now;
                    pureeMealCombinedModel.PureeMealBatchAssembly.tCookingPlan = null;
                    try
                    {
                        savedRecordID = _pureeMealBatchAssemblyService.CreateRecord(Mapper.Map<PureeMealBatchAssembly, tPureeMeanBatchAssembly>(pureeMealCombinedModel.PureeMealBatchAssembly), packersName.ToList());
                        AuditScope.CreateAndSave("Adding Puree Meal Batch Assembly Record.", new { User = User.Identity.GetUserId() });
                    }
                    catch (Exception ex)
                    {
                        AuditScope.CreateAndSave("Error Adding puree Meal Batch Assembly Record: ", new { User = User.Identity.GetUserId(), Exception = ex.Message });
                        throw;
                    }

                    return Json(new { status = "Success", savedRecordID }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    AuditScope.CreateAndSave("Error Adding Puree Meal Batch Assembly Record: ", new { User = User.Identity.GetUserId(), Exception = ex.Message });
                    throw;
                }
            }
        }
        #endregion

        #region Add Shape Detail
        /// <summary>
        /// Add Shape Detail
        /// </summary>
        /// <param name="pureeMealCombinedModel"></param>
        /// <returns></returns>
        public virtual JsonResult AddShapeDetail(PureeMealBatchAssemblyCombinedModel pureeMealCombinedModel)
        {
            // ReSharper disable once UnusedVariable
            PureeMealInBlastFreezer pureeMealInBlast = new PureeMealInBlastFreezer();

            pureeMealCombinedModel.PureeMealBatchAssemblyDetail.Sys_CreatedBy = User.Identity.GetUserId();
            pureeMealCombinedModel.PureeMealBatchAssemblyDetail.Sys_CreatedDateTime = DateTime.Now;
            pureeMealCombinedModel.PureeMealBatchAssemblyDetail.vSauce = null;
            pureeMealCombinedModel.PureeMealBatchAssemblyDetail.vSauceBatchNo = null;
            pureeMealCombinedModel.PureeMealBatchAssemblyDetail.vSauceTemp = null;
            pureeMealCombinedModel.PureeMealBatchAssemblyDetail.tSauceTimeOfTransferFromPrep = null;
            pureeMealCombinedModel.PureeMealBatchAssemblyDetail.vMash = null;
            pureeMealCombinedModel.PureeMealBatchAssemblyDetail.vMashBatchNo = null;
            pureeMealCombinedModel.PureeMealBatchAssemblyDetail.vMashTemp = null;
            pureeMealCombinedModel.PureeMealBatchAssemblyDetail.tMashTimeOfTransferFromPrep = null;
            try
            {
                pureeMealCombinedModel.PureeMealBatchAssemblyDetail.iPureeMeanBatchAssemblyId = pureeMealCombinedModel.PureeMealBatchAssembly.iPureeMeanBatchAssemblyId;
                _pureeMealBatchAssemblyDetailService.Create(Mapper.Map<PureeMealBatchAssemblyDetail, tPureeMealBatchAssemblyDetail>(pureeMealCombinedModel.PureeMealBatchAssemblyDetail));

                AuditScope.CreateAndSave("Adding Shape Detail for Puree Meal Batch Assembly's Record.", new { User = User.Identity.GetUserId() });
                return Json(new { status = "Success" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { status = ex }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Add Sauce Detail
        /// <summary>
        /// Add Sauce Detail
        /// </summary>
        /// <param name="pureeMealCombinedModel"></param>
        /// <returns></returns>
        public virtual JsonResult AddSauceDetail(PureeMealBatchAssemblyCombinedModel pureeMealCombinedModel)
        {
            pureeMealCombinedModel.PureeMealBatchAssemblyDetail.Sys_CreatedBy = User.Identity.GetUserId();
            pureeMealCombinedModel.PureeMealBatchAssemblyDetail.Sys_CreatedDateTime = DateTime.Now;
            pureeMealCombinedModel.PureeMealBatchAssemblyDetail.vShape = null;
            pureeMealCombinedModel.PureeMealBatchAssemblyDetail.vShapeBatchNo = null;
            pureeMealCombinedModel.PureeMealBatchAssemblyDetail.vShapeTemp = null;
            pureeMealCombinedModel.PureeMealBatchAssemblyDetail.dTime = null;
            pureeMealCombinedModel.PureeMealBatchAssemblyDetail.vMash = null;
            pureeMealCombinedModel.PureeMealBatchAssemblyDetail.vMashBatchNo = null;
            pureeMealCombinedModel.PureeMealBatchAssemblyDetail.vMashTemp = null;
            pureeMealCombinedModel.PureeMealBatchAssemblyDetail.tMashTimeOfTransferFromPrep = null;
            try
            {
                pureeMealCombinedModel.PureeMealBatchAssemblyDetail.iPureeMeanBatchAssemblyId = pureeMealCombinedModel.PureeMealBatchAssembly.iPureeMeanBatchAssemblyId;
                _pureeMealBatchAssemblyDetailService.Create(Mapper.Map<PureeMealBatchAssemblyDetail, tPureeMealBatchAssemblyDetail>(pureeMealCombinedModel.PureeMealBatchAssemblyDetail));

                AuditScope.CreateAndSave("Adding Sauce Detail Puree Meal Batch Assembly's Record.", new { User = User.Identity.GetUserId() });
                return Json(new { status = "Success" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { status = ex }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Add Mash Detail
        /// <summary>
        /// Add Mash Detail
        /// </summary>
        /// <param name="pureeMealCombinedModel"></param>
        /// <returns></returns>
        public virtual JsonResult AddMashDetail(PureeMealBatchAssemblyCombinedModel pureeMealCombinedModel)
        {
            var savedRecord = 0;
            // ReSharper disable once SuggestVarOrType_SimpleTypes
            PureeMealInBlastFreezer pureeMealInBlast = new PureeMealInBlastFreezer();

            pureeMealCombinedModel.PureeMealBatchAssemblyDetail.Sys_CreatedBy = User.Identity.GetUserId();
            pureeMealCombinedModel.PureeMealBatchAssemblyDetail.Sys_CreatedDateTime = DateTime.Now;
            pureeMealCombinedModel.PureeMealBatchAssemblyDetail.vShape = null;
            pureeMealCombinedModel.PureeMealBatchAssemblyDetail.vShapeBatchNo = null;
            pureeMealCombinedModel.PureeMealBatchAssemblyDetail.vShapeTemp = null;
            pureeMealCombinedModel.PureeMealBatchAssemblyDetail.dTime = null;
            pureeMealCombinedModel.PureeMealBatchAssemblyDetail.vSauce = null;
            pureeMealCombinedModel.PureeMealBatchAssemblyDetail.vSauceBatchNo = null;
            pureeMealCombinedModel.PureeMealBatchAssemblyDetail.vSauceTemp = null;
            pureeMealCombinedModel.PureeMealBatchAssemblyDetail.tSauceTimeOfTransferFromPrep = null;
            try
            {
                pureeMealCombinedModel.PureeMealBatchAssemblyDetail.iPureeMeanBatchAssemblyId = pureeMealCombinedModel.PureeMealBatchAssembly.iPureeMeanBatchAssemblyId;
                _pureeMealBatchAssemblyDetailService.Create(Mapper.Map<PureeMealBatchAssemblyDetail, tPureeMealBatchAssemblyDetail>(pureeMealCombinedModel.PureeMealBatchAssemblyDetail));

                pureeMealInBlast.iPureeMeanBatchAssemblyId = pureeMealCombinedModel.PureeMealBatchAssembly.iPureeMeanBatchAssemblyId;
                pureeMealInBlast.tStartTime = pureeMealCombinedModel.PureeMealBatchAssemblyDetail.tMashTimeOfTransferFromPrep;
                pureeMealInBlast.tLastPackTimeInFreezer = pureeMealCombinedModel.PureeMealBatchAssemblyDetail.tMashTimeOfTransferFromPrep;
                pureeMealInBlast.iNumberOfPacks = 0;
                pureeMealInBlast.Sys_CreatedBy = User.Identity.GetUserId();
                pureeMealInBlast.Sys_CreatedDateTime = DateTime.Now;
                pureeMealCombinedModel.PureeMealInBlastFreezer = pureeMealInBlast;
                savedRecord = _pureeMealInBlastService.CreateRecord(Mapper.Map<PureeMealInBlastFreezer, tPureeMealInBlastFreezer>(pureeMealCombinedModel.PureeMealInBlastFreezer));

                AuditScope.CreateAndSave("Adding Mash & Blast Freezer Detail Puree Meal Batch Assembly's Record.", new { User = User.Identity.GetUserId() });
                return Json(new { status = "Success", savedRecord }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { status = ex, savedRecord }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Edit Number of Packs
        /// <summary>
        /// Edit Number of Packs
        /// </summary>
        /// <param name="iPureeMealInBlastFreezerId"></param>
        /// /// <param name="iNumberOfPacks"></param>
        /// <returns></returns>
        public virtual JsonResult EditNumberOfPacks(int iPureeMealInBlastFreezerId, int iNumberOfPacks)
        {
            try
            {
                var blastFreezerRecModified = _pureeMealInBlastService.GetById(iPureeMealInBlastFreezerId);
                blastFreezerRecModified.iNumberOfPacks = iNumberOfPacks;
                blastFreezerRecModified.Sys_ModifyBy = User.Identity.GetUserId();
                blastFreezerRecModified.Sys_ModifyDateTime = DateTime.Now;
                _pureeMealInBlastService.Update(blastFreezerRecModified);

                AuditScope.CreateAndSave("Editing Blast Freezer Detail for Puree Meal Batch Assembly's Record.", new { User = User.Identity.GetUserId() });
                return Json(new { status = "Success" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { status = ex }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Edit Time of Last Pack into Blast Freezer
        /// <summary>
        /// Edit Time of Last Pack into Blast Freezer
        /// </summary>
        /// <param name="iPureeMealInBlastFreezerId"></param>
        /// <param name="tLastPackTimeInFreezer"></param>
        /// <returns></returns>
        public virtual JsonResult EditLastPackIntoFreezer(int iPureeMealInBlastFreezerId, DateTime tLastPackTimeInFreezer)
        {
            try
            {
                var blastFreezerRecModified = _pureeMealInBlastService.GetById(iPureeMealInBlastFreezerId);
                blastFreezerRecModified.tLastPackTimeInFreezer = tLastPackTimeInFreezer;
                blastFreezerRecModified.Sys_ModifyBy = User.Identity.GetUserId();
                blastFreezerRecModified.Sys_ModifyDateTime = DateTime.Now;
                _pureeMealInBlastService.Update(blastFreezerRecModified);

                AuditScope.CreateAndSave("Editing Blast Freezer Detail for Puree Meal Batch Assembly's Record.", new { User = User.Identity.GetUserId() });
                return Json(new { status = "Success" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { status = ex }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Delete Blast Freezer Record
        /// <summary>
        /// Delete Blast Freezer Item
        /// </summary>
        /// <param name="iPureeMealInBlastFreezerId"></param>
        /// <returns></returns>
        public virtual JsonResult DeleteFreezerRecord(int iPureeMealInBlastFreezerId)
        {
            // ReSharper disable once InconsistentNaming
            var deletedRecordID = 0;

            try
            {
                deletedRecordID = _pureeMealInBlastService.DeleteFreezerRecord(iPureeMealInBlastFreezerId);
                AuditScope.CreateAndSave("Deleting Freezer Record for Puree Meal Batch Assembly's Record.", new { User = User.Identity.GetUserId() });
                return Json(new { status = "Success", deletedRecordID }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { status = ex, deletedRecordID }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Get Units From DB
        /// <summary>
        /// Get Units From DB
        /// </summary>
        public List<Unit> GetUnits()
        {
            var units = new List<Unit>();
            var list = _unitService.GetAll();
            // ReSharper disable once LoopCanBeConvertedToQuery
            foreach (var item in list)
            {
                units.Add(Mapper.Map<tUnit, Unit>(item));
            }
            return units;
        }
        #endregion

        #region Private: Validate Packer Records
        // ReSharper disable once SuggestBaseTypeForParameter
        private void ValidatePackerRecords(string[] packerIDs, PureeMealBatchAssembly pureeMealBatchAssemblyRecord)
        {
            var packerRecords = _pureeMealBatchAssemblyPackerDetailService.GetAll().Where(m => m.iPureeMeanBatchAssemblyId == pureeMealBatchAssemblyRecord.iPureeMeanBatchAssemblyId).ToList();

            // ReSharper disable once LoopCanBePartlyConvertedToQuery
            foreach (var packer in packerRecords)
            {
                if (!packerIDs.Contains(packer.UserId))
                {
                    _pureeMealBatchAssemblyPackerDetailService.Delete(packer);
                }
            }

            // ReSharper disable once ForCanBeConvertedToForeach
            for (int i = 0; i < packerIDs.Length; i++)
            {
                var existing = false;
                var packerId = packerIDs[i];

                // ReSharper disable once LoopCanBePartlyConvertedToQuery
                foreach (var packer in packerRecords)
                {
                    if (packer.UserId == packerId)
                    {
                        existing = true;
                    }
                }

                if (!existing)
                {
                    _pureeMealBatchAssemblyService.CreatePackerRecord(Mapper.Map<PureeMealBatchAssembly, tPureeMeanBatchAssembly>(pureeMealBatchAssemblyRecord), packerId);
                    AuditScope.CreateAndSave("Creating new Packer Record for Puree Meal Batch Assembly Record.", new { User = User.Identity.GetUserId() });
                }

                // ReSharper disable once RedundantAssignment
                existing = false;
            }
        }
        #endregion

        #region Data Struct
        /// <summary>
        /// Data Struct for Batch Codes
        /// </summary>
        /// <returns></returns>
        public struct Data
        {
            public Data(string id, string name)
            {
                IntegerData = id;
                StringData = name;
            }

            public string IntegerData { get; private set; }
            public string StringData { get; private set; }
        }
        #endregion

        #region Print Record as Report

        /// <summary>
        /// Print Record
        /// </summary>
        /// <param name="pId">Id of the record to print</param>
        /// <returns>RDLC Report View</returns>
        public virtual ActionResult PrintRecord(Int32 pId)
        {
            try
            {
                return View("~/Views/Report/ReportPreview.aspx", new Report { ReportName = PkReportNames.PureeMealBatchAssemblyRecord, Id = pId, Service = _pureeMealBatchAssemblyService });
            }
            catch (Exception ex)
            {
                AuditScope.CreateAndSave("Error getting report of Puree Meal Batch Assembly", new { User = User.Identity.GetUserId() });
                // ReSharper disable once PossibleIntendedRethrow
                throw ex;
            }
        }
        #endregion
    }
}