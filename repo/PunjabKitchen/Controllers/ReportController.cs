﻿using PunjabKitchen.Models;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
// ReSharper disable All

namespace PunjabKitchen.Controllers
{
    public partial class ReportController : BaseController
    {
        #region Index
        /// <summary>
        /// Method to show index page for reporting
        /// </summary>
        /// <returns></returns>
        public virtual ActionResult Index()
        {
            try
            {
                ViewBag.ReportsList = FillReportsList();
                return View(Views.Index, new Report());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Post
        /// <summary>
        /// Method to receive report params and assign appropriate action to generate report
        /// </summary>
        /// <param name="report"></param>
        /// <returns>JsonResult</returns>
        public virtual JsonResult Post(Report report)
        {
            try
            {
                string pathPrefix = "Report/";

                TempData["ReportData"] = report;
                TempData.Keep();

                return Json(pathPrefix + nameof(ShowReport), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                // ReSharper disable once PossibleIntendedRethrow
                throw ex;
            }
        }
        #endregion

        #region Show Report Action
        /// <summary>
        /// Method to set Action of iFrame to generate report
        /// </summary>
        /// <returns></returns>
        public virtual ActionResult ShowReport()
        {
            try
            {
                return View(Views.ReportPreview, SetTempData());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Reports List
        /// <summary>
        /// Method to fill report names in dropdown
        /// </summary>
        /// <returns>SelectList</returns>
        private SelectList FillReportsList()
        {
            try
            {
                List<Tuple<string, string>> reportNames = new List<Tuple<string, string>>
            {
                new Tuple<string, string>(PkReportNames.GoodsIn.ToString(), PkReportNames.GoodsIn.DescriptionAttr()),
                new Tuple<string, string>(PkReportNames.CookingPlan.ToString(), PkReportNames.CookingPlan.DescriptionAttr()),
                new Tuple<string, string>(PkReportNames.PackingPlan.ToString(), PkReportNames.PackingPlan.DescriptionAttr()),
                new Tuple<string, string>(PkReportNames.Boxing.ToString(), PkReportNames.Boxing.DescriptionAttr()),
                new Tuple<string, string>(PkReportNames.GoodsOut.ToString(), PkReportNames.GoodsOut.DescriptionAttr()),
                new Tuple<string, string>(PkReportNames.StockCount.ToString(), PkReportNames.StockCount.DescriptionAttr())
            };

                return new SelectList(reportNames);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Set Report Params into TempData
        /// <summary>
        /// Set Report params in MVC TempDate
        /// </summary>
        /// <returns>Report</returns>
        private Report SetTempData()
        {
            try
            {
                var report = (Report)TempData["ReportData"];
                TempData.Keep();

                return report;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}