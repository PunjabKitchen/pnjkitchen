﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Audit.Core;
using AutoMapper;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using PunjabKitchen.Models;
using PunjabKitchen.Properties;
using PunjabKitchenBLL.Interfaces;
using PunjabKitchenEntities.EntityModel;
using PagedList;

namespace PunjabKitchen.Controllers
{
    [Authorize(Roles = "Admin,Packing Manager,Packing  Staff")]
    public partial class ShapesControlPackingController : BaseController
    {
        #region Data Members
        readonly IShapesControlRecordService _shapesControlPackingRecordService;
        readonly IShapesControlRecordDetailService _shapesControlPackingRecordDetailService;
        readonly IShapesControlRecordBatchDetailService _shapesControlPackingRecordBatchDetailService;
        readonly IUnitService _unitService;
        readonly ICookingPlanService _cookingPlanService;
        #endregion

        #region User Manager
        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            // ReSharper disable once UnusedMember.Local
            private set
            {
                _userManager = value;
            }
        }

        private ApplicationRoleManager _roleManager;
        public ApplicationRoleManager RoleManager
        {
            get
            {
                return _roleManager ?? HttpContext.GetOwinContext().Get<ApplicationRoleManager>();
            }
            // ReSharper disable once UnusedMember.Local
            private set
            {
                _roleManager = value;
            }
        }
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="shapesControlPackingRecordService"></param>
        /// <param name="shapesControlPackingRecordDetailService"></param>
        /// <param name="shapesControlPackingRecordBatchDetailService"></param>
        /// <param name="unitService"></param>
        /// <param name="cookingPlanService"></param>
        public ShapesControlPackingController(IShapesControlRecordService shapesControlPackingRecordService, IShapesControlRecordDetailService shapesControlPackingRecordDetailService, IShapesControlRecordBatchDetailService shapesControlPackingRecordBatchDetailService, IUnitService unitService, ICookingPlanService cookingPlanService)
        {
            _shapesControlPackingRecordService = shapesControlPackingRecordService;
            _unitService = unitService;
            _shapesControlPackingRecordDetailService = shapesControlPackingRecordDetailService;
            _shapesControlPackingRecordBatchDetailService = shapesControlPackingRecordBatchDetailService;
            _cookingPlanService = cookingPlanService;
        }
        #endregion

        #region Create New Shapes Control Packing Record
        /// <summary>
        /// Create New Shapes Control Packing Record
        /// </summary>
        /// <returns></returns>
        public virtual async Task<ActionResult> CreateShapesControlPacking()
        {
            var users = UserManager.Users.ToList();
            // ReSharper disable once InconsistentNaming
            var Roles = await RoleManager.Roles.ToListAsync();
            // ReSharper disable once PossibleNullReferenceException
            var roleId = Roles.FirstOrDefault(m => m.Name == PKRoles.PackingStaff).Id;
            var list = users.Where(x => x.Roles.Any(y => y.RoleId == roleId)).ToList();

            var userslist = new List<Data>();

            // ReSharper disable once LoopCanBeConvertedToQuery
            foreach (var item in list)
            {
                userslist.Add(new Data(item.Id, item.FirstName + " " + item.LastName));
            }

            ViewBag.Unit = new SelectList(GetUnits(), "iUnitId", "vName");
            ViewBag.BatchCodesList = new SelectList(GetCookingBatchCodes(), "IntegerData", "StringData");
            ViewBag.PackersList = new SelectList(userslist, "IntegerData", "StringData");
            return PartialView(Views.Create, new ShapesControlRecord());
        }
        #endregion

        #region Get Shapes Control Packing Record List 
        /// <summary>
        /// Get Shapes Control Packing Record List
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public virtual ActionResult ShapesControlPackingRecords(int? page = 1)
        {
            var recordsList = GetShapesControlPackingRecord().OrderByDescending(x => x.iShapeControlRecordId);
            var pageNumber = page ?? 1;
            return View(Views.Index, recordsList.ToPagedList(pageNumber, GetPageSize()));
        }
        #endregion

        #region Return Shapes Control Packing Record Edit View
        /// <summary>
        /// Return Shapes Control Packing Record Edit View
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize(Roles = "Admin,Packing Manager")]
        public virtual async Task<ActionResult> EditShapesControlPackingRecord(long id)
        {
            try
            {
                ViewBag.BatchCodesList = new SelectList(GetCookingBatchCodes(), "IntegerData", "StringData");

                var users = UserManager.Users.ToList();
                // ReSharper disable once InconsistentNaming
                var Roles = await RoleManager.Roles.ToListAsync();
                // ReSharper disable once PossibleNullReferenceException
                var roleId = Roles.FirstOrDefault(m => m.Name == PKRoles.PackingStaff).Id;
                var list = users.Where(x => x.Roles.Any(y => y.RoleId == roleId)).ToList();

                var userslist = new List<Data>();

                // ReSharper disable once LoopCanBeConvertedToQuery
                foreach (var item in list)
                {
                    userslist.Add(new Data(item.Id, item.FirstName + " " + item.LastName));
                }
                ViewBag.PackersList = new SelectList(userslist, "IntegerData", "StringData");

                ViewBag.Unit = new SelectList(GetUnits(), "iUnitId", "vName");

                // ReSharper disable once SuggestVarOrType_SimpleTypes
                ShapesControlRecord shapesControlRecordModel = Mapper.Map<tShapeControlRecord, ShapesControlRecord>(_shapesControlPackingRecordService.GetById(id));
                var packersList = _shapesControlPackingRecordDetailService.GetAll().Where(m => m.iShapeControlRecordId == shapesControlRecordModel.iShapeControlRecordId).ToList();

                shapesControlRecordModel.packersList = packersList[0].UserId;
                // ReSharper disable once SuggestVarOrType_BuiltInTypes
                for (int i = 1; i < packersList.Count; i++)
                {
                    shapesControlRecordModel.packersList += "," + packersList[i].UserId;
                }

                var batchCodesList = _shapesControlPackingRecordBatchDetailService.GetAll().Where(m => m.iShapeControlRecordId == shapesControlRecordModel.iShapeControlRecordId).ToList();

                shapesControlRecordModel.batchCodesList = batchCodesList[0].vBatchCode;
                // ReSharper disable once SuggestVarOrType_BuiltInTypes
                for (int i = 1; i < batchCodesList.Count; i++)
                {
                    shapesControlRecordModel.batchCodesList += "," + batchCodesList[i].vBatchCode;
                }

                return PartialView(Views.Create, shapesControlRecordModel);
            }
            catch (Exception ex)
            {
                AuditScope.CreateAndSave("Error getting shapes control edit view: ", new { User = User.Identity.GetUserId(), Exception = ex.Message });
                throw;
            }

        }
        #endregion

        #region  Get All Shapes Control Packing Records
        /// <summary>
        /// Get All Shapes Control Packing Records
        /// </summary>
        /// <returns></returns>
        // ReSharper disable once ReturnTypeCanBeEnumerable.Local
        private List<ShapesControlRecord> GetShapesControlPackingRecord()
        {
            try
            {
                var shapesControlPackingRecordList = _shapesControlPackingRecordService.GetAll();
                return shapesControlPackingRecordList.Select(Mapper.Map<tShapeControlRecord, ShapesControlRecord>).ToList();
            }
            catch (Exception ex)
            {
                AuditScope.CreateAndSave("Error getting shapes control packing record: ", new { User = User.Identity.GetUserId(), Exception = ex.Message });
                throw;
            }
        }
        #endregion

        #region Add/Edit
        /// <summary>
        /// Update Shapes Control Packing record if found else create new
        /// </summary>
        /// <param name="shapesControlRecordModel"></param>
        /// <returns></returns>
        public virtual JsonResult PostShapesControlRecord(ShapesControlRecord shapesControlRecordModel)
        {
            // ReSharper disable once InconsistentNaming
            // ReSharper disable once RedundantAssignment
            int savedRecordID = 0;
            // ReSharper disable once SuggestVarOrType_Elsewhere
            string[] packersName = shapesControlRecordModel.packersList.Split(',');
            // ReSharper disable once SuggestVarOrType_Elsewhere
            string[] batchCodesList = shapesControlRecordModel.batchCodesList.Split(',');

            var result = validateProducts(batchCodesList);
            if (result == -1)
            {
                return Json(new { status = "Failed", savedRecordID = -1 }, JsonRequestBehavior.AllowGet);
            }

            // ReSharper disable once SuggestVarOrType_Elsewhere
            string[] batchCodes = new string[batchCodesList.Length];
            // ReSharper disable once SuggestVarOrType_BuiltInTypes
            for (int i = 0; i < batchCodesList.Length; i++)
            { batchCodes[i] = batchCodesList[i].Split(' ')[0]; }

            //Edit
            if (shapesControlRecordModel.iShapeControlRecordId > 0)
            {
                try
                {
                    var shapesControlRecordModified = Mapper.Map<ShapesControlRecord, tShapeControlRecord>(shapesControlRecordModel);
                    shapesControlRecordModified.Sys_ModifyBy = User.Identity.GetUserId();
                    shapesControlRecordModified.Sys_ModifyDateTime = DateTime.Now;
                    _shapesControlPackingRecordService.Update(shapesControlRecordModified);
                    savedRecordID = shapesControlRecordModified.iShapeControlRecordId;
                    AuditScope.CreateAndSave("Editing Shapes Control Packing Record.", new { User = User.Identity.GetUserId() });

                    ValidateBatchRecords(batchCodes, shapesControlRecordModel);
                    ValidatePackerRecords(packersName, shapesControlRecordModel);

                    return Json(new { status = "Success", savedRecordID }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    AuditScope.CreateAndSave("Error Editing Shapes Control Packing Record: ", new { User = User.Identity.GetUserId(), Exception = ex.Message });
                    throw;
                }
            }
            //Add
            else
            {
                shapesControlRecordModel.Sys_CreatedBy = User.Identity.GetUserId();
                shapesControlRecordModel.Sys_CreatedDateTime = DateTime.Now;

                try
                {
                    savedRecordID = _shapesControlPackingRecordService.CreateRecord(Mapper.Map<ShapesControlRecord, tShapeControlRecord>(shapesControlRecordModel), packersName.ToList(), batchCodes.ToList());
                    AuditScope.CreateAndSave("Saving Shapes Control Packing Record", new { User = User.Identity.GetUserId() });
                    return Json(new { status = "Success", savedRecordID }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    AuditScope.CreateAndSave("Error Saving Shapes Control Packing Record: ", new { User = User.Identity.GetUserId(), Exception = ex.Message });
                    throw;
                }
            }
        }
        #endregion     

        #region Get Units From DB
        /// <summary>
        /// Get Units From DB
        /// </summary>
        public List<Unit> GetUnits()
        {
            var units = new List<Unit>();
            var list = _unitService.GetAll();
            // ReSharper disable once LoopCanBeConvertedToQuery
            foreach (var item in list)
            {
                units.Add(Mapper.Map<tUnit, Unit>(item));
            }
            return units;
        }
        #endregion

        #region Get Cooking Batch Codes From DB
        /// <summary>
        /// Get Cooking Batch Codes From DB
        /// </summary>
        // ReSharper disable once ReturnTypeCanBeEnumerable.Local
        private List<Data> GetCookingBatchCodes()
        {
            var batchCodesList = new List<Data>();
            var list = _cookingPlanService.GetAll();

            // ReSharper disable once LoopCanBeConvertedToQuery
            foreach (var item in list)
            {
                if (item.tRecipe != null)
                {
                    batchCodesList.Add(new Data(item.vBatchNo + " - " + item.tRecipe.vRecipeName, item.vBatchNo + " - " + item.tRecipe.vRecipeName));
                }
            }

            return batchCodesList;
        }
        #endregion

        #region Data Struct for Batch Codes
        /// <summary>
        /// Data Struct for Batch Codes
        /// </summary>
        /// <returns></returns>
        public struct Data
        {
            public Data(string id, string name)
            {
                IntegerData = id;
                StringData = name;
            }

            public string IntegerData { get; private set; }
            public string StringData { get; private set; }
        }
        #endregion

        #region Private: Validate Conflicting Products
        private int validateProducts(string[] batchList)
        {
            // ReSharper disable once SuggestVarOrType_Elsewhere
            string[] products = new string[batchList.Length];
            for (var i = 0; i < batchList.Length; i++)
            { products[i] = batchList[i].Split('-')[1]; }

            var matchProduct = products[0];
            // ReSharper disable once ForCanBeConvertedToForeach
            // ReSharper disable once LoopCanBeConvertedToQuery
            for (var i = 0; i < products.Length; i++)
            {
                if (!products[i].Equals(matchProduct))
                {
                    return -1;
                }
            }
            return 1;
        }
        #endregion

        #region Private: Validate Batch Records
        private void ValidateBatchRecords(string[] batchCodes, ShapesControlRecord shapesControlRecord)
        {
            var batchRecords = _shapesControlPackingRecordBatchDetailService.GetAll().Where(m => m.iShapeControlRecordId == shapesControlRecord.iShapeControlRecordId).ToList();

            // ReSharper disable once LoopCanBePartlyConvertedToQuery
            foreach (var batch in batchRecords)
            {
                if (!batchCodes.Contains(batch.vBatchCode))
                {
                    _shapesControlPackingRecordBatchDetailService.Delete(batch);
                }
            }

            // ReSharper disable once ForCanBeConvertedToForeach
            for (var i = 0; i < batchCodes.Length; i++)
            {
                var existing = false;
                var batchCode = batchCodes[i];

                // ReSharper disable once LoopCanBePartlyConvertedToQuery
                foreach (var batch in batchRecords)
                {
                    if (batch.vBatchCode == batchCode)
                    {
                        existing = true;
                    }
                }

                if (!existing)
                {
                    _shapesControlPackingRecordService.CreateBatchRecord(Mapper.Map<ShapesControlRecord, tShapeControlRecord>(shapesControlRecord), batchCode);
                    AuditScope.CreateAndSave("Creating new Batch Record for Shapes Control Record.", new { User = User.Identity.GetUserId() });
                }

                // ReSharper disable once RedundantAssignment
                existing = false;
            }
        }
        #endregion

        #region Private: Validate Packer Records
        private void ValidatePackerRecords(string[] packerIDs, ShapesControlRecord shapesControlRecord)
        {
            var packerRecords = _shapesControlPackingRecordDetailService.GetAll().Where(m => m.iShapeControlRecordId == shapesControlRecord.iShapeControlRecordId).ToList();

            foreach (var packer in packerRecords)
            {
                if (!packerIDs.Contains(packer.UserId))
                {
                    _shapesControlPackingRecordDetailService.Delete(packer);
                }
            }

            // ReSharper disable once ForCanBeConvertedToForeach
            for (var i = 0; i < packerIDs.Length; i++)
            {
                var existing = false;
                var packerId = packerIDs[i];

                // ReSharper disable once LoopCanBePartlyConvertedToQuery
                foreach (var packer in packerRecords)
                {
                    if (packer.UserId == packerId)
                    {
                        existing = true;
                    }
                }

                if (!existing)
                {
                    _shapesControlPackingRecordService.CreatePackerRecord(Mapper.Map<ShapesControlRecord, tShapeControlRecord>(shapesControlRecord), packerId);
                    AuditScope.CreateAndSave("Creating new Packer Record for Shapes Control Record.", new { User = User.Identity.GetUserId() });
                }

                // ReSharper disable once RedundantAssignment
                existing = false;
            }
        }
        #endregion

        #region Print Record as Report

        /// <summary>
        /// Print Record
        /// </summary>
        /// <param name="pId">Id of the record to print</param>
        /// <returns>RDLC Report View</returns>
        public virtual ActionResult PrintRecord(Int32 pId)
        {
            try
            {
                return View("~/Views/Report/ReportPreview.aspx", new Report { ReportName = PkReportNames.ShapesControlPackingRecord, Id = pId, Service = _shapesControlPackingRecordService });
            }
            catch (Exception ex)
            {
                AuditScope.CreateAndSave("Error getting report of Shapes Control Packing", new { User = User.Identity.GetUserId() });
                // ReSharper disable once PossibleIntendedRethrow
                throw ex;
            }
        }
        #endregion
    }
}