﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Audit.Core;
using AutoMapper;
using Microsoft.AspNet.Identity;
using PunjabKitchen.Models;
using PunjabKitchenBLL.Interfaces;
using PunjabKitchenEntities.EntityModel;
using PagedList;

namespace PunjabKitchen.Controllers
{
    [Authorize(Roles = "Admin,Cooking Manager,Cooking Staff")]
    public partial class SlicingRecordController : BaseController
    {
        #region Data Members
        readonly ISlicingRecordService _slicingRecordService;
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        ///<param name="slicingRecordService"></param>
        public SlicingRecordController(ISlicingRecordService slicingRecordService)
        {
            _slicingRecordService = slicingRecordService;
        }
        #endregion

        #region Get Slicing Records List
        /// <summary>
        /// Get Slicing Records List
        /// </summary>
        [HttpGet]
        public virtual ActionResult Index(int? page = 1)
        {
            var slicingRecordsList = GetSlicingRecords().OrderByDescending(x => x.iSlicingRecordId);
            var pageNumber = page ?? 1;
            return View(Views.SlicingRecord, slicingRecordsList.ToPagedList(pageNumber, GetPageSize()));
        }
        #endregion

        #region Create New Slicing Record
        /// <summary>
        /// Create New Slicing Record
        /// </summary>
        /// <returns></returns>
        public virtual ActionResult Create()
        {
            return PartialView(Views.CreateSlicingRecord, new SlicingRecord());
        }
        #endregion

        #region Edit Slicing Record
        /// <summary>
        /// Edit Slicing Record
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "Admin,Cooking Manager")]
        public virtual ActionResult Edit(long id)
        {
            var slicingRecord = Mapper.Map<tSlicingRecord, SlicingRecord>(_slicingRecordService.GetById(id));
            return PartialView(Views.CreateSlicingRecord, slicingRecord);
        }
        #endregion

        #region Add/Edit Slicing Record
        /// <summary>
        /// Add/Edit Slicing Record
        /// </summary>
        /// <param name="slicingRecordModel"></param>
        /// <returns></returns>
        public virtual ActionResult Post(SlicingRecord slicingRecordModel)
        {
            try
            {
                //Edit
                if (slicingRecordModel.iSlicingRecordId > 0)
                {
                    var slicingRecModified = Mapper.Map<SlicingRecord, tSlicingRecord>(slicingRecordModel);
                    slicingRecModified.Sys_ModifyBy = User.Identity.GetUserId();
                    slicingRecModified.Sys_ModifyDateTime = DateTime.Now;
                    _slicingRecordService.Update(slicingRecModified);
                    AuditScope.CreateAndSave("Editing Slicing Record", new { User = User.Identity.GetUserId() });
                }
                //Add
                else
                {
                    slicingRecordModel.Sys_CreatedBy = User.Identity.GetUserId();
                    slicingRecordModel.Sys_CreatedDateTime = DateTime.Now;
                    _slicingRecordService.Create(Mapper.Map<SlicingRecord, tSlicingRecord>(slicingRecordModel));
                    AuditScope.CreateAndSave("Adding Slicing Record", new { User = User.Identity.GetUserId() });
                }
                return RedirectToAction(MVC.SlicingRecord.Index());
            }
            catch (Exception ex)
            {
                AuditScope.CreateAndSave("Error saving Slicing Record: ", new { User = User.Identity.GetUserId(), Exception = ex.Message });
                return RedirectToAction(MVC.Home.Index());
            }
        }
        #endregion

        #region Private: Get Slicing Records
        /// <summary>
        /// Get Slicing Records
        /// </summary>
        private List<SlicingRecord> GetSlicingRecords()
        {
            var slicingRecordsList = _slicingRecordService.GetAll();
            return slicingRecordsList.Select(Mapper.Map<tSlicingRecord, SlicingRecord>).ToList();
        }
        #endregion

        #region Print Record as Report

        /// <summary>
        /// Print Record
        /// </summary>
        /// <param name="pId">Id of the record to print</param>
        /// <returns>RDLC Report View</returns>
        public virtual ActionResult PrintRecord(Int32 pId)
        {
            try
            {
                return View("~/Views/Report/ReportPreview.aspx", new Report { ReportName = PkReportNames.SlicingRecord, Id = pId, Service = _slicingRecordService });
            }
            catch (Exception ex)
            {
                AuditScope.CreateAndSave("Error Printing Report of Slicing Record", new { User = User.Identity.GetUserId() });
                // ReSharper disable once PossibleIntendedRethrow
                throw ex;
            }
        }
        #endregion
    }
}