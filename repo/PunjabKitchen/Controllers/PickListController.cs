﻿using Audit.Core;
using AutoMapper;
using Microsoft.AspNet.Identity;
using PunjabKitchen.Models;
using PunjabKitchenBLL.Interfaces;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using PunjabKitchenEntities.EntityModel;
using System.Threading.Tasks;
using PunjabKitchenEntities.Common;
using System.Linq;
using System.Security.Principal;

namespace PunjabKitchen.Controllers
{
    public partial class PickListController : BaseController
    {
        #region Data Members
        readonly IUnitService _unitService;
        readonly IPickListService _pickListService;
        readonly IProductService _productService;
        readonly INotificationService _notificationService;
        readonly IShoeCleaningCheckService _shoeCleaningCheckService;
        private readonly IGoodsOutFreezerUnitService _goodsOutFreezerUnit;
        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="unitService"></param>
        /// <param name="pickListService"></param>
        public PickListController(
             IUnitService unitService, IPickListService pickListService, IProductService productService,
             INotificationService notificationService, IShoeCleaningCheckService shoeCleaningCheckService, IGoodsOutFreezerUnitService goodsOutFreezerUnit)
        {
            _unitService = unitService;
            _pickListService = pickListService;
            _productService = productService;

            _notificationService = notificationService;
            _shoeCleaningCheckService = shoeCleaningCheckService;
            _goodsOutFreezerUnit = goodsOutFreezerUnit;
        }

        #endregion

        #region GET: PickList Modal
        /// <summary>
        /// GET: PickList Modal
        /// </summary>
        /// <returns></returns>
        public virtual PartialViewResult StockPickList()
        {
            ViewBag.units = new SelectList(GetUnits(), "iUnitId", "vName");
            return PartialView(Views.PickListModal, new PickList());
        }
        #endregion

        #region GET: Units
        /// <summary>
        /// GET: PickList Modal
        /// </summary>
        /// <returns></returns>
        private List<Unit> GetUnits()
        {
            var units = new List<Unit>();
            var list = _unitService.GetAll();
            // ReSharper disable once LoopCanBeConvertedToQuery
            foreach (var item in list)
            {
                units.Add(Mapper.Map<tUnit, Unit>(item));
            }
            return units;
        }
        #endregion

        #region Post: Picklist Record
        /// <summary>
        /// Post
        /// </summary>
        /// <param name="pickList"></param>
        /// <returns></returns>
        public virtual JsonResult Post(PickList pickList)
        {
            try
            {
                pickList.Sys_CreatedBy = User.Identity.GetUserId();
                pickList.Sys_CreatedDateTime = DateTime.Now;
                //Pick List service pick product
                var pickListSaved = _pickListService.AddPickList(Mapper.Map<PickList, tPickList>(pickList));
                AuditScope.CreateAndSave("Saving Pick List item", new { User = User.Identity.GetUserId() });
                CheckThreshold(pickList.iProductId, User);
                return Json(Mapper.Map<tPickList, PickList>(pickListSaved));
            }
            catch (Exception ex)
            {
                AuditScope.CreateAndSave("Error Saving Picklist Record: ", new { User = User.Identity.GetUserId(), Exception = ex.Message });
                return null;
            }
        }
        #endregion

        #region Check Threshold
        /// <summary>
        /// Check Threshold
        /// </summary>
        /// <param name="pProductId"></param>
        /// <param name="pUser"></param>
        /// <returns></returns>
        private async Task CheckThreshold(int? pProductId, IPrincipal pUser)
        {
            try
            {
                await Task.Run(() =>
                {
                    var context = new PunjabKitchenContext();
                    var product = context.Products.FirstOrDefault(f => f.iProductId == (int)pProductId);

                    if (product != null)
                    {
                        if (product.tStock.iQuantity <= product.iThreshold)
                        {
                            new NotificationsController(_notificationService, _shoeCleaningCheckService, _goodsOutFreezerUnit).CreateNotification(new Notification()
                            {
                                bEnabled = true,
                                vProcess = "Admin",
                                iRefrenceId = pProductId,
                                vDescription = "Product has meet the threshold.",
                                iNotificationTypeId = 1,
                                vSubject = "Threshold Alert"
                            }, pUser);
                        }
                    }
                });
            }
            catch (Exception ex)
            {
                AuditScope.CreateAndSave("Error while checking threshold: ", new { User = User.Identity.GetUserId(), Exception = ex.Message });
            }
        }
        #endregion
    }
}