﻿using System;
using System.Linq;
using System.Web.Mvc;
using Audit.Core;
using AutoMapper;
using Microsoft.AspNet.Identity;
using PunjabKitchen.Models;
using PunjabKitchenBLL.Interfaces;
using PunjabKitchenEntities.EntityModel;
using PagedList;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNet.Identity.Owin;
using System.Data.Entity;
using PunjabKitchen.Properties;
using System.Collections.Generic;

namespace PunjabKitchen.Controllers
{
    [Authorize(Roles = "Admin,Boxing Manager,Boxing Staff")]
    public partial class BoxingRecordController : BaseController
    {
        #region Data Members

        readonly IBoxingRecordService _boxingRecordService;
        readonly IMixedCaseBoxingRecordService _mixedCaseBoxingRecordService;
        readonly IMixedCaseBoxingRecordDetailService _mixedCaseBoxingRecordDetailService;
        readonly ICookingPlanService _cookingPlanService;

        #endregion

        #region Constructor

        public BoxingRecordController(IBoxingRecordService boxingRecordService,
            IMixedCaseBoxingRecordService mixedCaseBoxingRecordService, 
            IMixedCaseBoxingRecordDetailService mixedCaseBoxingRecordDetailService,
            ICookingPlanService cookingPlanService)
        {
            _boxingRecordService = boxingRecordService;
            _mixedCaseBoxingRecordService = mixedCaseBoxingRecordService;
            _mixedCaseBoxingRecordDetailService = mixedCaseBoxingRecordDetailService;
            _cookingPlanService = cookingPlanService;
        }

        #endregion

        #region  GET: Boxing Record
        /// <summary>
        /// GET: Boxing Record
        /// </summary>
        /// <param name="page"></param>
        /// <returns></returns>
        public virtual ActionResult Index(int? page = 1)
        {
            var boxingRecords = _boxingRecordService.GetAll().OrderByDescending(x => x.iBoxingRecordId);
            var pageNumber = page ?? 1;
            var boxingRecordsList = boxingRecords.Select(item => Mapper.Map<tBoxingRecord, BoxingRecord>(item)).ToList();
            return View(Views.Index, boxingRecordsList.ToPagedList(pageNumber, GetPageSize()));
        }
        #endregion

        #region Return Boxing Record
        /// <summary>
        /// Return Boxing Record
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public virtual ActionResult CreateBoxingRecord(int? id)
        {
            
            var boxingRecord = new BoxingRecord();
            if (id != null)
            {
                boxingRecord = Mapper.Map<tBoxingRecord, BoxingRecord>(_boxingRecordService.GetById((long)id));
            }
            
            return View(Views.Create, boxingRecord);
        }
        #endregion

        #region Return Mixed Case Boxing Record
        /// <summary>
        /// Return Boxing Record
        /// </summary>
        /// <param name="page"></param>
        /// <returns></returns>
        public virtual ActionResult MixedCaseBoxingRecordList(int? page = 1)
        {
            var mixedCaseBoxingRecords = _mixedCaseBoxingRecordService.GetAll().OrderByDescending(x => x.iMixedCaseBoxingRecordId);
            var pageNumber = page ?? 1;
            var mixedCaseBoxingRecordsList = mixedCaseBoxingRecords.Select(item => Mapper.Map<tMixedCaseBoxingRecord, MixedCaseBoxingRecord>(item)).ToList();
            return View(Views.MixedCaseBoxingRecord, mixedCaseBoxingRecordsList.ToPagedList(pageNumber, GetPageSize()));
        }
        #endregion

        #region Return Mixed Case Boxing Record Partial View
        /// <summary>
        ///  Return Mixed Case Boxing Record Partial View
        /// </summary>
        /// <returns></returns>
        public virtual async Task<ActionResult> CreateMixedCaseBoxingRecord()
        {
            await FetchPackersList();
            return PartialView(Views.AddEditMixedCaseBoxing);
        }
        #endregion

        #region POST BOXING RECORD
        /// <summary>
        /// Post
        /// </summary>
        /// <param name="boxingRecord"></param>
        /// <returns></returns>
        public virtual ActionResult Post(BoxingRecord boxingRecord)
        {
            try
            {
                //Edit
                if (boxingRecord.iBoxingRecordId > 0)
                {
                    boxingRecord.Sys_ModifyBy = User.Identity.GetUserId();
                    boxingRecord.Sys_ModifyDateTime = DateTime.Now;
                    if (boxingRecord.iCookingPlanId != null)
                        boxingRecord.tCookingPlan = _cookingPlanService.GetById((long)boxingRecord.iCookingPlanId);
                    EditBoxingRecord(Mapper.Map<BoxingRecord, tBoxingRecord>(boxingRecord));
                    AuditScope.CreateAndSave("Editing Boxing Record", new { User = User.Identity.GetUserId(), BoxingRecordId = boxingRecord.iBoxingRecordId });
                }
                //Add
                else
                {
                    boxingRecord.Sys_CreatedBy = User.Identity.GetUserId();
                    boxingRecord.Sys_CreatedDateTime = DateTime.Now;
                    boxingRecord.tCookingPlan.tRecipe = null;
                    boxingRecord.tCookingPlan = null;
                    var savedBoxingRecord = AddBoxingRecord(boxingRecord);
                    boxingRecord.iBoxingRecordId = savedBoxingRecord.iBoxingRecordId;
                    AuditScope.CreateAndSave("Add Boxing Record", new { User = User.Identity.GetUserId(), BoxingRecordId = boxingRecord.iBoxingRecordId });
                }

                return RedirectToAction(MVC.BoxingRecord.Index());
            }
            catch (Exception ex)
            {
                AuditScope.CreateAndSave("Error saving Boxing Record", new { User = User.Identity.GetUserId(), Exception = ex.Message });
                throw;
            }
        }

        #region Private: Edit Boxing Record

        /// <summary>
        /// Edit Boxing Record
        /// </summary>
        /// <param name="boxingRecord"></param>
        private void EditBoxingRecord(tBoxingRecord boxingRecord)
        {
            _boxingRecordService.Update(boxingRecord);
        }

        #endregion

        #region Private: Add Boxing Record

        /// <summary>
        /// Add Boxing Record
        /// </summary>
        /// <param name="boxingRecord"></param>
        /// <returns></returns>
        private tBoxingRecord AddBoxingRecord(BoxingRecord boxingRecord)
        {
            return _boxingRecordService.Create(Mapper.Map<BoxingRecord, tBoxingRecord>(boxingRecord));
        }

        #endregion
        #endregion

        #region MIXED CASE BOXING RECORD
        /// <summary>
        /// Post MIXED CASE BOXING RECORD
        /// </summary>
        /// <param name="mixedCaseBoxingRecord"></param>
        /// <returns></returns>
        public virtual JsonResult PostMixedCase(MixedCaseBoxingRecord mixedCaseBoxingRecord)
        {
            //Edit
            if (mixedCaseBoxingRecord.iMixedCaseBoxingRecordId > 0)
            {
                var mixedRecord = Mapper.Map<MixedCaseBoxingRecord, tMixedCaseBoxingRecord>(mixedCaseBoxingRecord);
                mixedRecord.Sys_ModifyBy = User.Identity.GetUserId();
                mixedRecord.Sys_ModifyDateTime = DateTime.Now;
                _mixedCaseBoxingRecordService.Update(mixedRecord);

                if ((mixedCaseBoxingRecord.vBatchCode != null) && !mixedCaseBoxingRecord.vBatchCode.Equals(""))
                {
                    SaveMixedCaseDetailRecord(mixedCaseBoxingRecord);
                }

            }
            //Add
            else
            {
                //var mixedCaseBoxingRecordCopy = mixedCaseBoxingRecord.DeepClone();
                mixedCaseBoxingRecord.Sys_CreatedBy = User.Identity.GetUserId();
                mixedCaseBoxingRecord.Sys_CreatedDateTime = DateTime.Now;
                var savedBoxingRecord = AddMixedCaseBoxingRecord(mixedCaseBoxingRecord);
                //Update Copy Obj Id 
                mixedCaseBoxingRecord.iMixedCaseBoxingRecordId = savedBoxingRecord.iMixedCaseBoxingRecordId;
                //Save Mixed Case Boxing Record Detail 
                if ((mixedCaseBoxingRecord.vBatchCode != null) && !mixedCaseBoxingRecord.vBatchCode.Equals(""))
                {
                    SaveMixedCaseDetailRecord(mixedCaseBoxingRecord);
                }
                AuditScope.CreateAndSave("Add Mixed Boxing Record", new { User = User.Identity.GetUserId(), MixedCaseBoxingRecordId = mixedCaseBoxingRecord.iMixedCaseBoxingRecordId });

            }
            
            return Json(new { result = mixedCaseBoxingRecord.iMixedCaseBoxingRecordId }, JsonRequestBehavior.AllowGet);
        }

        #region Save Mixed Case Detail Record
        /// <summary>
        /// Save Mixed Case Detail Record
        /// </summary>
        /// <param name="mixedCaseBoxingRecord"></param>
        /// <returns></returns>
        private void SaveMixedCaseDetailRecord(MixedCaseBoxingRecord mixedCaseBoxingRecord)
        {
            try
            {
                var mixedCaseDetailObject = new MixedCaseBoxingRecordDetail
                {
                    iMixedCaseBoxingRecordId = mixedCaseBoxingRecord.iMixedCaseBoxingRecordId,
                    vNumber = mixedCaseBoxingRecord.vNumber,
                    vBatchCode = mixedCaseBoxingRecord.vBatchCode,
                    vNameOfPack = mixedCaseBoxingRecord.vNameOfPack
                };
                var savedMixedCaseDetRec = _mixedCaseBoxingRecordDetailService.Create(
                    Mapper.Map<MixedCaseBoxingRecordDetail, tMixedCaseBoxingRecordDetail>(mixedCaseDetailObject));
                AuditScope.CreateAndSave("Editing Boxing Record Detail", new
                {
                    User = User.Identity.GetUserId(),
                    MixedCaseBoxingRecordId = mixedCaseBoxingRecord.iMixedCaseBoxingRecordId,
                    MixedCaseBoxingRecordDetailId = savedMixedCaseDetRec.iMixedCaseBoxingRecordDetailId
                });
            }
            catch (Exception ex)
            {
                AuditScope.CreateAndSave("Error saving mixed boxing record detail: ", new { User = User.Identity.GetUserId(), Exception = ex.Message });
                throw;
            }  
        }
        #endregion

        #region Private: Edit Mixed Case Boxing Record
        /// <summary>
        ///  Private: Edit Mixed Case Boxing Record
        /// </summary>
        /// <param name="id"></param>
        public virtual async Task<ActionResult> EditMixedBoxingRecord(long id)
        {
            var mixedBoxingRecord = Mapper.Map<tMixedCaseBoxingRecord, MixedCaseBoxingRecord>(_mixedCaseBoxingRecordService.GetById(id));
            await FetchPackersList();
            return PartialView(Views.AddEditMixedCaseBoxing, mixedBoxingRecord);
        }

        #endregion

        #region Private: Add Mixed Case Boxing Record

        /// <summary>
        /// Private: Add Mixed Case Boxing Record
        /// </summary>
        /// <param name="mixedCaseBoxingRecord"></param>
        /// <returns></returns>
        private MixedCaseBoxingRecord AddMixedCaseBoxingRecord(MixedCaseBoxingRecord mixedCaseBoxingRecord)
        {
            var savedRec= _mixedCaseBoxingRecordService.Create(Mapper.Map<MixedCaseBoxingRecord, tMixedCaseBoxingRecord>(mixedCaseBoxingRecord));
            return Mapper.Map<tMixedCaseBoxingRecord, MixedCaseBoxingRecord>(savedRec);
        }

        #endregion

        /// <summary>
        /// Fetch Packers List
        /// </summary>
        /// <returns></returns>
        public virtual async Task FetchPackersList()
        {
            var users = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>().Users.ToList();
            // ReSharper disable once InconsistentNaming
            var Roles = await HttpContext.GetOwinContext().Get<ApplicationRoleManager>().Roles.ToListAsync();
            // ReSharper disable once PossibleNullReferenceException
            var roleId = Roles.FirstOrDefault(m => m.Name == PKRoles.PackingStaff).Id;
            var list = users.Where(x => x.Roles.Any(y => y.RoleId == roleId)).ToList();

            var userslist = new List<Data>();

            // ReSharper disable once LoopCanBeConvertedToQuery
            foreach (var item in list)
            {
                userslist.Add(new Data(item.Id, item.FirstName + " " + item.LastName));
            }

            ViewBag.PackersList = new SelectList(userslist, "IntegerData", "StringData");
        }
        #endregion

        #region Data Struct
        /// <summary>
        /// Data Struct for Batch Codes
        /// </summary>
        /// <returns></returns>
        public struct Data
        {
            public Data(string id, string name)
            {
                IntegerData = id;
                StringData = name;
            }

            public string IntegerData { get; private set; }
            public string StringData { get; private set; }
        }
        #endregion

        #region Print Record as Report

        /// <summary>
        /// Print Record
        /// </summary>
        /// <param name="pId">Id of the record to print</param>
        /// <returns>RDLC Report View</returns>
        public virtual ActionResult PrintRecord(Int32 pId)
        {
            try
            {
                return View("~/Views/Report/ReportPreview.aspx", new Report { ReportName = PkReportNames.BoxingRecord, Id = pId, Service = _boxingRecordService });
            }
            catch (Exception ex)
            {
                AuditScope.CreateAndSave("Error getting report of Boxing Record", new { User = User.Identity.GetUserId() });
                // ReSharper disable once PossibleIntendedRethrow
                throw ex;
            }
        }
        #endregion

        #region Print Mixed Case Record as Report

        /// <summary>
        /// Print Record
        /// </summary>
        /// <param name="pId">Id of the record to print</param>
        /// <returns>RDLC Report View</returns>
        public virtual ActionResult PrintMixedCaseRecord(Int32 pId)
        {
            try
            {
                return View("~/Views/Report/ReportPreview.aspx", new Report { ReportName = PkReportNames.MixedCaseBoxingRecord, Id = pId, Service = _mixedCaseBoxingRecordService});
            }
            catch (Exception ex)
            {
                AuditScope.CreateAndSave("Error getting report of Mixed Boxing Record", new { User = User.Identity.GetUserId() });
                // ReSharper disable once PossibleIntendedRethrow
                throw ex;
            }
        }
        #endregion
    }
}