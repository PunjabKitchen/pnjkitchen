﻿using System.Web.Mvc;
using PunjabKitchenBLL.Interfaces;

namespace PunjabKitchen.Controllers
{
    public partial class UniqueCodeController : BaseController
    {
        #region Data Members
        readonly IProductService _productService;
        readonly ICookingPlanService _cookingPlanService;
        readonly IPackingPlanService _packingPlanService;
        readonly IBoxingRecordService _boxingRecordService;
        readonly IGoodsOutPlanService _goodsOutPlanService;
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="productService"></param>
        /// <param name="cookingPlanService"></param>
        /// <param name="packingPlanService"></param>
        /// <param name="boxingRecordService"></param>
        /// <param name="goodsOutPlanService"></param>
        public UniqueCodeController(IProductService productService, ICookingPlanService cookingPlanService, IPackingPlanService packingPlanService, IBoxingRecordService boxingRecordService, IGoodsOutPlanService goodsOutPlanService)
        {
            _productService = productService;
            _cookingPlanService = cookingPlanService;
            _packingPlanService = packingPlanService;
            _boxingRecordService = boxingRecordService;
            _goodsOutPlanService = goodsOutPlanService;
        }
        #endregion

        #region Get Unique Code By Id
        /// <summary>
        /// Function To Get Unique Codes 
        /// If Type = 1 => Get product code by using productTypeId recieved in param
        /// If Type = 2 => Get Cooking Batch Code
        /// If Type = 3 => Get Packing Batch Code
        /// If Type = 4 => Get Boxing Batch Code
        /// If Type = 5 => Get Goods Out Batch Code
        /// </summary>
        /// <param name="id"></param>
        /// <param name="productTypeId"></param>
        /// <returns></returns>
        public virtual JsonResult GetUniqueCodeId(int id, int productTypeId)
        {
            switch (id)
            {
                case 1:
                    return Json(new { result = _productService.GetProductCode(productTypeId) }, JsonRequestBehavior.AllowGet);
                case 2:
                    return Json(new { result = _cookingPlanService.GetBatchCode() }, JsonRequestBehavior.AllowGet);
                case 3:
                    return Json(new { result = _packingPlanService.GetBatchCode() }, JsonRequestBehavior.AllowGet);
                case 4:
                    return Json(new { result = _boxingRecordService.GetBatchCode() }, JsonRequestBehavior.AllowGet);
                case 5:
                    return Json(new { result = _goodsOutPlanService.GetBatchCode() }, JsonRequestBehavior.AllowGet);
                default:
                    return Json(new { result = false }, JsonRequestBehavior.AllowGet);
        }
    }
        #endregion
    }
}