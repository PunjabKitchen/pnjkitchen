﻿using Audit.Core;
using AutoMapper;
using Microsoft.AspNet.Identity;
using PunjabKitchen.Models;
using PunjabKitchenBLL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using PunjabKitchenEntities.EntityModel;
using PagedList;

namespace PunjabKitchen.Controllers
{
    [Authorize(Roles = "Admin,Cooking Manager,Cooking Staff")]
    public partial class BatchCoolingTempRecrdController : BaseController
    {
        #region Data Members
        readonly IBatchCoolingTempRecrdService _batchCoolingTempRecrdService;
        readonly IUnitService _unitService;
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="batchCoolingTempRecrdService"></param>
        /// <param name="unitService"></param>
        public BatchCoolingTempRecrdController(IBatchCoolingTempRecrdService batchCoolingTempRecrdService, IUnitService unitService)
        {
            _batchCoolingTempRecrdService = batchCoolingTempRecrdService;
            _unitService = unitService;
        }
        #endregion

        #region Create New Batch Cooling Temp
        /// <summary>
        /// Create New Batch Cooling Temp
        /// </summary>
        /// <returns></returns>
        public virtual ActionResult Create()
        {
            ViewBag.Unit = new SelectList(GetUnits(), "iUnitId", "vName");

            return PartialView(Views.Create, new BatchCoolingTempRecrd());
        }
        #endregion

        #region Get Units From DB
        /// <summary>
        /// Get Units From DB
        /// </summary>
        private List<Unit> GetUnits()
        {
            var units = new List<Unit>();
            var list = _unitService.GetAll();
            // ReSharper disable once LoopCanBeConvertedToQuery
            foreach (var item in list)
            {
                units.Add(Mapper.Map<tUnit, Unit>(item));
            }
            return units;
        }
        #endregion

        #region Get Batch Cooling Temp Record List 
        /// <summary>
        /// Get Batch Cooling Temp Record List 
        /// </summary>
        /// <param name="page"></param>
        /// <returns></returns>
        [HttpGet]
        public virtual ActionResult BatchCoolingTempRecords(int? page = 1)
        {
            var recordsList = GetBatchCoolingTempRecord().OrderByDescending(x => x.iBatchCoolingTempRecrdId);
            var pageNumber = page ?? 1;
            return View(Views.Index, recordsList.ToPagedList(pageNumber, GetPageSize()));
        }
        #endregion

        #region  Get All Batch Cooling Temp Record
        /// <summary>
        /// Get All Batch Cooling Temp Record
        /// </summary>
        /// <returns></returns>
        private List<BatchCoolingTempRecrd> GetBatchCoolingTempRecord()
        {
            try
            {
                var batchCoolinTempRecordList = _batchCoolingTempRecrdService.GetAll();
                return batchCoolinTempRecordList.Select(Mapper.Map<tBatchCoolingTempRecrd, BatchCoolingTempRecrd>).ToList();
            }
            catch (Exception ex)
            {
                AuditScope.CreateAndSave("Error Getting Batch Cooling Temp Record", new { User = User.Identity.GetUserId(), Exception = ex.Message });
                throw;
            }
        }
        #endregion

        #region Add/Edit
        /// <summary>
        /// Update batch cooling temp record if found else create new
        /// </summary>
        /// <param name="batchCoolingtempRecordModel"></param>
        /// <returns></returns>
        public virtual ActionResult Post(BatchCoolingTempRecrd batchCoolingtempRecordModel)
        {
            try
            {
                //Edit
                if (batchCoolingtempRecordModel.iBatchCoolingTempRecrdId > 0)
                {
                    var batchCoolingRecModified = Mapper.Map<BatchCoolingTempRecrd, tBatchCoolingTempRecrd>(batchCoolingtempRecordModel);
                    batchCoolingRecModified.Sys_ModifyBy = User.Identity.GetUserId();
                    batchCoolingRecModified.Sys_ModifyDateTime = DateTime.Now;
                    _batchCoolingTempRecrdService.Update(batchCoolingRecModified);
                    AuditScope.CreateAndSave("Editing Batch Cooling temp Record", new { User = User.Identity.GetUserId() });
                }
                //Add
                else
                {
                    batchCoolingtempRecordModel.Sys_CreatedBy = User.Identity.GetUserId();
                    batchCoolingtempRecordModel.Sys_CreatedDateTime = DateTime.Now;
                    try
                    {
                        _batchCoolingTempRecrdService.Create(Mapper.Map<BatchCoolingTempRecrd, tBatchCoolingTempRecrd>(batchCoolingtempRecordModel));
                        AuditScope.CreateAndSave("Adding Batch Cooling Temp Record", new { User = User.Identity.GetUserId() });
                    }
                    catch (Exception ex)
                    {
                        AuditScope.CreateAndSave("Error saving Batch Cooling temp record ", new { User = User.Identity.GetUserId(), Exception = ex.Message });
                        throw;
                    }
                }
                return RedirectToAction(MVC.BatchCoolingTempRecrd.BatchCoolingTempRecords());
            }
            catch (Exception ex)
            {
                AuditScope.CreateAndSave("Error saving Batch Cooling temp record", new { User = User.Identity.GetUserId(), Exception = ex.Message });
                return RedirectToAction(MVC.Home.Index());
            }
        }
        #endregion

        #region Return Batch Cooling Temp Record Edit View
        /// <summary>
        /// Return Batch Cooling Temp Record Edit View
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize(Roles = "Admin,Cooking Manager")]
        public virtual ActionResult EditBatchCoolingTempRecord(long id)
        {
            ViewBag.Unit = new SelectList(GetUnits(), "iUnitId", "vName");
            BatchCoolingTempRecrd batchCoolingTempRec = Mapper.Map<tBatchCoolingTempRecrd, BatchCoolingTempRecrd>(_batchCoolingTempRecrdService.GetById(id));
            return PartialView(Views.Create, batchCoolingTempRec);
        }
        #endregion

        #region Return Batch Cooling Temp Record Create New View
        /// <summary>
        /// Return Batch Cooling Temp Create New View
        /// </summary>
        /// <returns></returns>
        public virtual ActionResult CreateBatchCoolingTempRecord()
        {
            return PartialView(Views.Create);
        }
        #endregion

        #region Print Record as Report

        /// <summary>
        /// Print Record
        /// </summary>
        /// <param name="pId">Id of the record to print</param>
        /// <returns>RDLC Report View</returns>
        public virtual ActionResult PrintRecord(Int32 pId)
        {
            try
            {
                return View("~/Views/Report/ReportPreview.aspx", new Report { ReportName = PkReportNames.BatchCoolingTempRecord, Id = pId, Service = _batchCoolingTempRecrdService });
            }
            catch (Exception ex)
            {
                AuditScope.CreateAndSave("Error getting report of Batch Cooling Temp Record", new { User = User.Identity.GetUserId() });
                // ReSharper disable once PossibleIntendedRethrow
                throw ex;
            }
        }
        #endregion
    }
}