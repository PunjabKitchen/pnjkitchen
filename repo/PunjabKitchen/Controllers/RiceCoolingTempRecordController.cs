﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Audit.Core;
using AutoMapper;
using Microsoft.AspNet.Identity;
using PunjabKitchen.Models;
using PunjabKitchenBLL.Interfaces;
using PunjabKitchenEntities.EntityModel;
using PagedList;

namespace PunjabKitchen.Controllers
{
    [Authorize(Roles = "Admin,Cooking Manager,Cooking Staff")]
    public partial class RiceCoolingTempRecordController : BaseController
    {
        #region Data Members
        readonly IRiceCoolingTempRecordService _riceCoolingTempRecordService;
        readonly IUnitService _unitService;
#endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        ///<param name="riceCoolingTempRecordService"></param>
        /// <param name="unitService"></param>
        public RiceCoolingTempRecordController(IRiceCoolingTempRecordService riceCoolingTempRecordService, IUnitService unitService)
        {
            _riceCoolingTempRecordService = riceCoolingTempRecordService;
            _unitService = unitService;
        }
        #endregion

        #region Private: Get Units
        private List<Unit> GetUnits()
        {
            var units = new List<Unit>();
            var list = _unitService.GetAll();
            // ReSharper disable once LoopCanBeConvertedToQuery
            foreach (var item in list)
            {
                units.Add(Mapper.Map<tUnit, Unit>(item));
            }
            return units;
        }
        #endregion

        #region Get Rice Cooling Temp Records List
        /// <summary>
        /// Get Rice Cooling Temp Records List
        /// </summary>
        [HttpGet]
        public virtual ActionResult Index(int? page = 1)
        {
            var recordsList = GetRiceCoolingTempRecords().OrderByDescending(x => x.iRiceCoolingTempRecordId);
            var pageNumber = page ?? 1;
            return View(Views.RiceCoolingTempRecord, recordsList.ToPagedList(pageNumber, GetPageSize()));
        }
        #endregion

        #region Create New Rice Cooling Temp Record
        /// <summary>
        /// Create New Rice Cooling Temp Record
        /// </summary>
        /// <returns></returns>
        public virtual ActionResult Create()
        {
            ViewBag.Unit = new SelectList(GetUnits(), "iUnitId", "vName");
            return PartialView(Views.CreateRiceCoolingTempRecord, new RiceCoolingTempRecord());
        }
        #endregion

        #region Edit Rice Cooling Temp Record
        /// <summary>
        /// Edit Rice Cooling Temp Record
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "Admin,Cooking Manager")]
        public virtual ActionResult Edit(long id)
        {
            ViewBag.Unit = new SelectList(GetUnits(), "iUnitId", "vName");
            var riceCoolingTempRecord = Mapper.Map<tRiceCoolingTempRecord, RiceCoolingTempRecord>(_riceCoolingTempRecordService.GetById(id));
            return PartialView(Views.CreateRiceCoolingTempRecord, riceCoolingTempRecord);
        }
        #endregion

        #region Add/Edit Rice Cooling Temp Record
        /// <summary>
        /// Add/Edit Rice Cooling Temp Record
        /// </summary>
        /// <param name="riceCoolingTempRecordModel"></param>
        /// <returns></returns>
        public virtual ActionResult Post(RiceCoolingTempRecord riceCoolingTempRecordModel)
        {
            try
            {
                //Edit
                if (riceCoolingTempRecordModel.iRiceCoolingTempRecordId > 0)
                {
                    var riceCoolingRecModified = Mapper.Map<RiceCoolingTempRecord, tRiceCoolingTempRecord>(riceCoolingTempRecordModel);
                    riceCoolingRecModified.Sys_ModifyBy = User.Identity.GetUserId();
                    riceCoolingRecModified.Sys_ModifyDateTime = DateTime.Now;
                    _riceCoolingTempRecordService.Update(riceCoolingRecModified);
                    AuditScope.CreateAndSave("Editing Rice Cooling Temp Record", new { User = User.Identity.GetUserId() });
                }
                //Add
                else
                {
                    riceCoolingTempRecordModel.Sys_CreatedBy = User.Identity.GetUserId();
                    riceCoolingTempRecordModel.Sys_CreatedDateTime = DateTime.Now;
                    _riceCoolingTempRecordService.Create(Mapper.Map<RiceCoolingTempRecord, tRiceCoolingTempRecord>(riceCoolingTempRecordModel));
                    AuditScope.CreateAndSave("Saving Rice Cooling Temp Record", new { User = User.Identity.GetUserId() });
                }
                return RedirectToAction(MVC.RiceCoolingTempRecord.Index());
            }
            catch (Exception ex)
            {
                AuditScope.CreateAndSave("Error Saving Rice Cooling Temp Record: ", new { User = User.Identity.GetUserId(), Exception = ex.Message });
                return RedirectToAction(MVC.Home.Index());
            }
        }
        #endregion

        #region Private: Get Rice Cooling Records
        /// <summary>
        /// Get Rice Cooling Records
        /// </summary>
        private List<RiceCoolingTempRecord> GetRiceCoolingTempRecords()
        {
            var riceCoolingTempRecordsList = _riceCoolingTempRecordService.GetAll();
            return riceCoolingTempRecordsList.Select(Mapper.Map<tRiceCoolingTempRecord, RiceCoolingTempRecord>).ToList();
        }
        #endregion

        #region Print Record as Report

        /// <summary>
        /// Print Record
        /// </summary>
        /// <param name="pId">Id of the record to print</param>
        /// <returns>RDLC Report View</returns>
        public virtual ActionResult PrintRecord(Int32 pId)
        {
            try
            {
                return View("~/Views/Report/ReportPreview.aspx", new Report { ReportName = PkReportNames.RiceCoolingTempRecord, Id = pId, Service = _riceCoolingTempRecordService });
            }
            catch (Exception ex)
            {
                AuditScope.CreateAndSave("Error getting report of Rice Cooling Temp Record", new { User = User.Identity.GetUserId() });
                // ReSharper disable once PossibleIntendedRethrow
                throw ex;
            }
        }
        #endregion
    }
}