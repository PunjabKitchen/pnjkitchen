﻿using System;
using PunjabKitchenBLL.Interfaces;
using System.Collections;
using System.Web.Mvc;
using Audit.Core;
using Microsoft.AspNet.Identity;

namespace PunjabKitchen.Controllers
{
    [Authorize]
    public partial class RecipeController : BaseController
    {
        #region Data Members
        readonly IRecipeService _recipeService;
        #endregion

        #region Constructor
        public RecipeController(IRecipeService recipeService)
        {
            _recipeService = recipeService;
        }
        #endregion

        #region Get Recipe By Code
        /// <summary>
        /// Get Recipe By Code
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        [HttpPost]
        public virtual JsonResult GetRecipeByCode(string code)
        {
            try
            {
                var recipeList = _recipeService.GetByCode(code);
                var recipesList = new ArrayList();
                foreach (var recipe in recipeList)
                {
                    recipesList.Add(new { id = recipe.iRecipeId, name = recipe.vRecipeName, description = recipe.vResipeDescription, code = recipe.vRecipeCode });
                }
                return Json(recipesList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                AuditScope.CreateAndSave("Error getting recipe by code", new { User = User.Identity.GetUserId(), Exception = ex.Message });
                throw;
            }
        }
        #endregion
    }
}