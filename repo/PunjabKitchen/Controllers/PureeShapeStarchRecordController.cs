﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Audit.Core;
using AutoMapper;
using Microsoft.AspNet.Identity;
using PunjabKitchen.Models;
using PunjabKitchenBLL.Interfaces;
using PunjabKitchenEntities.EntityModel;
using PagedList;

namespace PunjabKitchen.Controllers
{
    [Authorize(Roles = "Admin,Packing Manager,Packing  Staff")]
    public partial class PureeShapeStarchRecordController : BaseController
    {
        #region Data Members
        readonly IPureeShapesStarchRecordService _pureeShapesStarchRecordService;
        readonly IPureeShapesStarchRecordDetailService _pureeShapesStarchRecordDetailService;
        readonly IPureeShapesStarchAdditionRecordService _pureeShapesStarchAdditionRecordService;
        readonly IUnitService _unitService;
        readonly ICookingPlanService _cookingPlanService;
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="unitService"></param>
        /// <param name="pureeShapesStarchRecordService"></param>
        /// <param name="pureeShapesStarchRecordDetailService"></param>
        /// <param name="pureeShapesStarchAdditionRecordService"></param>
        /// <param name="cookingPlanService"></param>
        public PureeShapeStarchRecordController(IPureeShapesStarchRecordService pureeShapesStarchRecordService, IUnitService unitService, IPureeShapesStarchRecordDetailService pureeShapesStarchRecordDetailService, IPureeShapesStarchAdditionRecordService pureeShapesStarchAdditionRecordService, ICookingPlanService cookingPlanService)
        {
            _pureeShapesStarchRecordService = pureeShapesStarchRecordService;
            _unitService = unitService;
            _pureeShapesStarchRecordDetailService = pureeShapesStarchRecordDetailService;
            _pureeShapesStarchAdditionRecordService = pureeShapesStarchAdditionRecordService;
            _cookingPlanService = cookingPlanService;
        }
        #endregion

        #region Create New Puree Shape starch Record
        /// <summary>
        /// Create New Puree Shape starch Record
        /// </summary>
        /// <returns></returns>
        public virtual ActionResult CreatePureeShapesStarch()
        {
            ViewBag.BatchCodesList = new SelectList(GetCookingBatchCodes(), "IntegerData", "StringData");
            ViewBag.Unit = new SelectList(GetUnits(), "iUnitId", "vName");
            return PartialView(Views.Create, new PureeShapesRecordCombinedModel());
        }
        #endregion

        #region Get Units From DB
        /// <summary>
        /// Get Units From DB
        /// </summary>
        private List<Unit> GetUnits()
        {
            var units = new List<Unit>();
            var list = _unitService.GetAll();
            // ReSharper disable once LoopCanBeConvertedToQuery
            foreach (var item in list)
            {
                units.Add(Mapper.Map<tUnit, Unit>(item));
            }
            return units;
        }
        #endregion

        #region Get Cooking Batch Codes From DB
        /// <summary>
        /// Get Cooking Batch Codes From DB
        /// </summary>
        // ReSharper disable once ReturnTypeCanBeEnumerable.Local
        private List<Data> GetCookingBatchCodes()
        {
            var batchCodesList = new List<Data>();
            var list = _cookingPlanService.GetAll();

            // ReSharper disable once LoopCanBeConvertedToQuery
            foreach (var item in list)
            {
                if (item.tRecipe != null)
                {
                    batchCodesList.Add(new Data(item.vBatchNo + " - " + item.tRecipe.vRecipeName, item.vBatchNo + " - " + item.tRecipe.vRecipeName));
                }
            }

            return batchCodesList;
        }
        #endregion

        #region Get Puree Shape starch Record List 
        /// <summary>
        /// Get Puree Shape starch Record List
        /// </summary>
        /// <param name="page"></param>
        /// <returns></returns>
        [HttpGet]
        public virtual ActionResult PureeShapesStarchRecords(int? page = 1)
        {
            var recordsList = GetPureeShapesStarchRecord().OrderByDescending(x => x.iPureeShapeStarchRecordId);
            var pageNumber = page ?? 1;
            return View(Views.Index, recordsList.ToPagedList(pageNumber, GetPageSize()));
        }
        #endregion

        #region Return Puree Shape starch Record Edit View
        /// <summary>
        /// Return Puree Shape starch Record Edit View
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize(Roles = "Admin,Packing Manager")]
        public virtual ActionResult EditPureeShapesStarchRecord(long id)
        {
            ViewBag.BatchCodesList = new SelectList(GetCookingBatchCodes(), "IntegerData", "StringData");
            ViewBag.Unit = new SelectList(GetUnits(), "iUnitId", "vName");

            // ReSharper disable once SuggestVarOrType_SimpleTypes
            PureeShapesRecordCombinedModel pureeShapesRecordCombinedModel = new PureeShapesRecordCombinedModel();
            pureeShapesRecordCombinedModel.PureeShapeStarchRecord = Mapper.Map<tPureeShapesStarchRecord, PureeShapeStarchRecord>(_pureeShapesStarchRecordService.GetById(id));
            var batchCodesList = _pureeShapesStarchRecordDetailService.GetAll().Where(m => m.iPureeShapeStarchRecordId == pureeShapesRecordCombinedModel.PureeShapeStarchRecord.iPureeShapeStarchRecordId).ToList();

            pureeShapesRecordCombinedModel.BatchCodesList = batchCodesList[0].vBatchCode;
            // ReSharper disable once SuggestVarOrType_BuiltInTypes
            for (int i = 1; i < batchCodesList.Count; i++)
            {
                pureeShapesRecordCombinedModel.BatchCodesList += "," + batchCodesList[i].vBatchCode;
            }

            pureeShapesRecordCombinedModel.PureeShapesStarchAdditionRecordsList = _pureeShapesStarchAdditionRecordService.GetAll().Where(m => m.iPureeShapeStarchRecordId == pureeShapesRecordCombinedModel.PureeShapeStarchRecord.iPureeShapeStarchRecordId).Select(Mapper.Map<tPureeShapesStarchAdditionRecord, PureeShapesStarchAdditionRecord>).ToList();

            return PartialView(Views.Create, pureeShapesRecordCombinedModel);
        }
        #endregion

        #region  Get All Puree Shape starch Records
        /// <summary>
        /// Get All Puree Shape starch Record
        /// </summary>
        /// <returns></returns>
        // ReSharper disable once ReturnTypeCanBeEnumerable.Local
        private List<PureeShapeStarchRecord> GetPureeShapesStarchRecord()
        {
            var pureeShapesStarchRecordList = _pureeShapesStarchRecordService.GetAll();
            return pureeShapesStarchRecordList.Select(Mapper.Map<tPureeShapesStarchRecord, PureeShapeStarchRecord>).ToList();
        }
        #endregion

        #region Add/Edit
        /// <summary>
        /// Update Puree Shape starch Record if found else create new
        /// </summary>
        /// <param name="pureeShapesRecordCombinedModel"></param>
        /// <returns></returns>
        public virtual JsonResult Post(PureeShapesRecordCombinedModel pureeShapesRecordCombinedModel)
        {
            if (pureeShapesRecordCombinedModel.PureeShapeStarchRecord.iPureeShapeStarchRecordId > 0)
            {
                // ReSharper disable once SuggestVarOrType_Elsewhere
                string[] batchList = pureeShapesRecordCombinedModel.BatchCodesList.Split(',');

                var result = validateProducts(batchList);
                if (result == -1)
                {
                    return Json(new { status = "Failed", savedRecordID = -1 }, JsonRequestBehavior.AllowGet);
                }

                string[] batchCodes = new string[batchList.Length];
                // ReSharper disable once SuggestVarOrType_BuiltInTypes
                for (int i = 0; i < batchList.Length; i++)
                { batchCodes[i] = batchList[i].Split(' ')[0]; }

                // ReSharper disable once SuggestVarOrType_BuiltInTypes
                try
                {
                    // ReSharper disable once InconsistentNaming
                    // ReSharper disable once RedundantAssignment
                    var savedRecordID = 0;
                    if (pureeShapesRecordCombinedModel.PureeShapeStarchRecord.Sys_CreatedBy != null)
                    {
                        var pureeMasterRecModified = Mapper.Map<PureeShapeStarchRecord, tPureeShapesStarchRecord>(pureeShapesRecordCombinedModel.PureeShapeStarchRecord);
                        pureeShapesRecordCombinedModel.PureeShapeStarchRecord.Sys_ModifyBy = User.Identity.GetUserId();
                        pureeShapesRecordCombinedModel.PureeShapeStarchRecord.Sys_ModifyDateTime = DateTime.Now;
                        _pureeShapesStarchRecordService.Update(pureeMasterRecModified);
                        savedRecordID = pureeShapesRecordCombinedModel.PureeShapeStarchRecord.iPureeShapeStarchRecordId;
                        AuditScope.CreateAndSave("Updating Puree Shape Starch Record's Master Record With Batch Detail.", new { User = User.Identity.GetUserId() });

                        validateBatchRecords(batchCodes, pureeShapesRecordCombinedModel.PureeShapeStarchRecord);

                        return Json(new { status = "Success", savedRecordID }, JsonRequestBehavior.AllowGet);
                    }

                    //Checking if total weight has updated
                    var existingPureeRecord = _pureeShapesStarchRecordService.GetById(pureeShapesRecordCombinedModel.PureeShapeStarchRecord.iPureeShapeStarchRecordId);
                    if (existingPureeRecord.vTotal != pureeShapesRecordCombinedModel.PureeShapeStarchRecord.vTotal)
                    {
                        existingPureeRecord.vTotal = pureeShapesRecordCombinedModel.PureeShapeStarchRecord.vTotal;
                        existingPureeRecord.Sys_ModifyBy = User.Identity.GetUserId();
                        existingPureeRecord.Sys_ModifyDateTime = DateTime.Now;
                        _pureeShapesStarchRecordService.Update(existingPureeRecord);
                        AuditScope.CreateAndSave("Updating Puree Shape Starch Record's Master Record.", new { User = User.Identity.GetUserId() });
                    }

                    pureeShapesRecordCombinedModel.PureeShapesStarchAdditionRecord.Sys_CreatedBy = User.Identity.GetUserId();
                    pureeShapesRecordCombinedModel.PureeShapesStarchAdditionRecord.Sys_CreatedDateTime = DateTime.Now;
                    savedRecordID = _pureeShapesStarchRecordService.CreateRecord(Mapper.Map<PureeShapeStarchRecord, tPureeShapesStarchRecord>(pureeShapesRecordCombinedModel.PureeShapeStarchRecord), batchCodes.ToList(), Mapper.Map<PureeShapesStarchAdditionRecord, tPureeShapesStarchAdditionRecord>(pureeShapesRecordCombinedModel.PureeShapesStarchAdditionRecord));
                    AuditScope.CreateAndSave("Adding Puree Shape Starch Record's Addition.", new { User = User.Identity.GetUserId() });
                    return Json(new { status = "Success", savedRecordID }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    AuditScope.CreateAndSave("Error editing puree shape starch record: ", new { User = User.Identity.GetUserId(), Exception = ex.Message });
                    throw;
                }
            }
            else
            {
                // ReSharper disable once InconsistentNaming
                // ReSharper disable once TooWideLocalVariableScope
                int savedRecordID;
                // ReSharper disable once SuggestVarOrType_Elsewhere
                string[] batchList = pureeShapesRecordCombinedModel.BatchCodesList.Split(',');

                var result = validateProducts(batchList);
                if (result == -1)
                {
                    return Json(new { status = "Failed", savedRecordID = -1 }, JsonRequestBehavior.AllowGet);
                }

                // ReSharper disable once SuggestVarOrType_Elsewhere
                string[] batchCodes = new string[batchList.Length];
                // ReSharper disable once SuggestVarOrType_BuiltInTypes
                for (int i = 0; i < batchList.Length; i++)
                {
                    batchCodes[i] = batchList[i].Split(' ')[0];
                }

                pureeShapesRecordCombinedModel.PureeShapeStarchRecord.Sys_CreatedBy = User.Identity.GetUserId();
                pureeShapesRecordCombinedModel.PureeShapeStarchRecord.Sys_CreatedDateTime = DateTime.Now;

                try
                {
                    savedRecordID = _pureeShapesStarchRecordService.CreateRecord(Mapper.Map<PureeShapeStarchRecord, tPureeShapesStarchRecord>(pureeShapesRecordCombinedModel.PureeShapeStarchRecord), batchCodes.ToList(), null);
                    AuditScope.CreateAndSave("Adding Puree Shape Starch Record", new { User = User.Identity.GetUserId() });
                    return Json(new { status = "Success", savedRecordID }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    AuditScope.CreateAndSave("Error adding puree shape starch record: ", new { User = User.Identity.GetUserId(), Exception = ex.Message });
                    throw;
                }
            }
        }
        #endregion

        #region Add Addition After Master Table Modification
        /// <summary>
        /// Add Addition After Master Table Modification
        /// </summary>
        /// <param name="pureeShapesRecordCombinedModel"></param>
        /// <returns></returns>
        public virtual JsonResult AddAdditionAfterMasterModification(PureeShapesRecordCombinedModel pureeShapesRecordCombinedModel)
        {
            // ReSharper disable once InconsistentNaming
            // ReSharper disable once TooWideLocalVariableScope
            int savedRecordID;
            if (pureeShapesRecordCombinedModel.PureeShapeStarchRecord.iPureeShapeStarchRecordId > 0)
            {
                // ReSharper disable once InconsistentNaming
                // ReSharper disable once TooWideLocalVariableScope
                try
                {
                    //Checking if total weight has updated
                    var existingPureeRecord = _pureeShapesStarchRecordService.GetById(pureeShapesRecordCombinedModel.PureeShapeStarchRecord.iPureeShapeStarchRecordId);
                    if (existingPureeRecord.vTotal != pureeShapesRecordCombinedModel.PureeShapeStarchRecord.vTotal)
                    {
                        existingPureeRecord.vTotal = pureeShapesRecordCombinedModel.PureeShapeStarchRecord.vTotal;
                        existingPureeRecord.Sys_ModifyBy = User.Identity.GetUserId();
                        existingPureeRecord.Sys_ModifyDateTime = DateTime.Now;
                        _pureeShapesStarchRecordService.Update(existingPureeRecord);
                        AuditScope.CreateAndSave("Updating Puree Shape Starch Record's Master Table.", new { User = User.Identity.GetUserId() });
                    }

                    pureeShapesRecordCombinedModel.PureeShapesStarchAdditionRecord.Sys_CreatedBy = User.Identity.GetUserId();
                    pureeShapesRecordCombinedModel.PureeShapesStarchAdditionRecord.Sys_CreatedDateTime = DateTime.Now;
                    savedRecordID = _pureeShapesStarchRecordService.CreateRecord(Mapper.Map<PureeShapeStarchRecord, tPureeShapesStarchRecord>(pureeShapesRecordCombinedModel.PureeShapeStarchRecord), null, Mapper.Map<PureeShapesStarchAdditionRecord, tPureeShapesStarchAdditionRecord>(pureeShapesRecordCombinedModel.PureeShapesStarchAdditionRecord));
                    AuditScope.CreateAndSave("Adding Puree Shape Starch Record's Addition.", new { User = User.Identity.GetUserId() });
                    return Json(new { status = "Success", savedRecordID }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    AuditScope.CreateAndSave("Error adding addition in puree shape starch record: ", new { User = User.Identity.GetUserId(), Exception = ex.Message });
                    throw;
                }
            }
            return Json(new { status = "Failed", savedRecordID = 0 }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Private: Validate Batch Records
        // ReSharper disable once InconsistentNaming
        // ReSharper disable once SuggestBaseTypeForParameter
        private void validateBatchRecords(string[] batchCodes, PureeShapeStarchRecord pureeShapeStarchRecord)
        {
            var batchRecords = _pureeShapesStarchRecordDetailService.GetAll().Where(m => m.iPureeShapeStarchRecordId == pureeShapeStarchRecord.iPureeShapeStarchRecordId).ToList();

            // ReSharper disable once LoopCanBePartlyConvertedToQuery
            foreach (var batch in batchRecords)
            {
                if (!batchCodes.Contains(batch.vBatchCode))
                {
                    _pureeShapesStarchRecordDetailService.Delete(batch);
                }
            }

            // ReSharper disable once ForCanBeConvertedToForeach
            // ReSharper disable once SuggestVarOrType_BuiltInTypes
            for (int i = 0; i < batchCodes.Length; i++)
            {
                var existing = false;
                var batchCode = batchCodes[i];

                // ReSharper disable once LoopCanBePartlyConvertedToQuery
                foreach (var batch in batchRecords)
                {
                    if (batch.vBatchCode == batchCode)
                    {
                        existing = true;
                    }
                }

                if (!existing)
                {
                    _pureeShapesStarchRecordService.CreateBatchRecord(Mapper.Map<PureeShapeStarchRecord, tPureeShapesStarchRecord>(pureeShapeStarchRecord), batchCode);
                    AuditScope.CreateAndSave("Creating new Batch Record.", new { User = User.Identity.GetUserId() });
                }

                // ReSharper disable once RedundantAssignment
                existing = false;
            }
        }
        #endregion

        #region Private: Validate Conflicting Products
        // ReSharper disable once InconsistentNaming
        private int validateProducts(string[] batchList)
        {
            // ReSharper disable once SuggestVarOrType_Elsewhere
            string[] products = new string[batchList.Length];
            for (int i = 0; i < batchList.Length; i++)
            { products[i] = batchList[i].Split('-')[1]; }

            var matchProduct = products[0];
            // ReSharper disable once ForCanBeConvertedToForeach
            // ReSharper disable once LoopCanBeConvertedToQuery
            for (int i = 0; i < products.Length; i++)
            {
                if (!products[i].Equals(matchProduct))
                {
                    return -1;
                }
            }
            return 1;
        }
        #endregion

        #region Delete Addition Item
        /// <summary>
        /// Delete Addition Item
        /// </summary>
        /// <param name="iPureeShapeStarchRecordAdditionId"></param>
        /// <returns></returns>
        public virtual JsonResult DeleteAdditionItem(int iPureeShapeStarchRecordAdditionId)
        {
            // ReSharper disable once InconsistentNaming
            // ReSharper disable once TooWideLocalVariableScope
            int deletedRecordID;
            // ReSharper disable once TooWideLocalVariableScope
            int deletedAdditionWeight;

            try
            {
                // ReSharper disable once PossibleInvalidOperationException
                deletedAdditionWeight = (int)_pureeShapesStarchAdditionRecordService.GetById(iPureeShapeStarchRecordAdditionId).vAdditionAmount;
                deletedRecordID = _pureeShapesStarchRecordService.DeleteAdditionItem(iPureeShapeStarchRecordAdditionId);
                AuditScope.CreateAndSave("Deleting Addition Record in Puree Shape Starch Record.", new { User = User.Identity.GetUserId() });
                return Json(new { status = "Success", deletedRecordID, deletedAdditionWeight }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                AuditScope.CreateAndSave("Error Deleting Addition Record in Puree Shape Starch Record", new { User = User.Identity.GetUserId(), Exception = ex.Message });
                throw;
            }
        }
        #endregion

        #region Validate Total Weight
        /// <summary>
        /// Validate Total Weight
        /// </summary>
        /// <param name="iPureeShapeStarchRecordAdditionId"></param>
        /// <param name="totalWeight"></param>
        /// <returns></returns>
        public virtual JsonResult ValidateTotalWeight(int iPureeShapeStarchRecordAdditionId, string totalWeight)
        {
            //Checking if total weight has updated
            var existingPureeRecord = _pureeShapesStarchRecordService.GetById(iPureeShapeStarchRecordAdditionId);
            // ReSharper disable once InvertIf
            if (existingPureeRecord.vTotal != totalWeight)
            {
                existingPureeRecord.vTotal = totalWeight;
                existingPureeRecord.Sys_ModifyBy = User.Identity.GetUserId();
                existingPureeRecord.Sys_ModifyDateTime = DateTime.Now;
                _pureeShapesStarchRecordService.Update(existingPureeRecord);
                AuditScope.CreateAndSave("Updating Puree Shape Starch Record's Master Record.", new { User = User.Identity.GetUserId() });
            }
            return Json(new { result = true }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Data Struct for Batch Codes
        /// <summary>
        /// Data Struct for Batch Codes
        /// </summary>
        /// <returns></returns>
        public struct Data
        {
            public Data(string id, string name)
            {
                IntegerData = id;
                StringData = name;
            }

            public string IntegerData { get; private set; }
            public string StringData { get; private set; }
        }
        #endregion

        #region Print Record as Report

        /// <summary>
        /// Print Record
        /// </summary>
        /// <param name="pId">Id of the record to print</param>
        /// <returns>RDLC Report View</returns>
        public virtual ActionResult PrintRecord(Int32 pId)
        {
            try
            {
                return View("~/Views/Report/ReportPreview.aspx", new Report { ReportName = PkReportNames.PureeShapeStarchRecord, Id = pId, Service = _pureeShapesStarchRecordService });
            }
            catch (Exception ex)
            {
                AuditScope.CreateAndSave("Error getting report of Puree Shape Starch Record", new { User = User.Identity.GetUserId() });
                // ReSharper disable once PossibleIntendedRethrow
                throw ex;
            }
        }
        #endregion
    }
}