﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Audit.Core;
using AutoMapper;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using PunjabKitchen.Models;
using PunjabKitchen.Properties;
using PunjabKitchenBLL.Interfaces;
using PunjabKitchenEntities.EntityModel;
using PagedList;

namespace PunjabKitchen.Controllers
{
    [Authorize(Roles = "Admin,Packing Manager,Packing  Staff")]
    public partial class PackingRecordController : BaseController
    {
        #region Data Members
        readonly IPackingRecordService _packingRecordService;
        readonly IPackingRecordDetailService _packingRecordDetailService;
        readonly IUnitService _unitService;
        #endregion

        #region User Manager
        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            // ReSharper disable once UnusedMember.Local
            private set
            {
                _userManager = value;
            }
        }

        private ApplicationRoleManager _roleManager;
        public ApplicationRoleManager RoleManager
        {
            get
            {
                return _roleManager ?? HttpContext.GetOwinContext().Get<ApplicationRoleManager>();
            }
            // ReSharper disable once UnusedMember.Local
            private set
            {
                _roleManager = value;
            }
        }
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="packingRecordService"></param>
        /// <param name="packingRecordDetailService"></param>
        /// <param name="unitService"></param>
        public PackingRecordController(IPackingRecordService packingRecordService, IPackingRecordDetailService packingRecordDetailService, IUnitService unitService)
        {
            _packingRecordService = packingRecordService;
            _packingRecordDetailService = packingRecordDetailService;
            _unitService = unitService;
        }
        #endregion

        #region Create New Packing Record
        /// <summary>
        /// Create New Packing Record
        /// </summary>
        /// <returns></returns>
        public virtual async Task<ActionResult> CreatePackingRecord()
        {
            var users = UserManager.Users.ToList();
            // ReSharper disable once InconsistentNaming
            var Roles = await RoleManager.Roles.ToListAsync();
            // ReSharper disable once PossibleNullReferenceException
            var roleId = Roles.FirstOrDefault(m => m.Name == PKRoles.PackingStaff).Id;
            var list = users.Where(x => x.Roles.Any(y => y.RoleId == roleId)).ToList();

            var userslist = new List<Data>();

            // ReSharper disable once LoopCanBeConvertedToQuery
            foreach (var item in list)
            {
                userslist.Add(new Data(item.Id, item.FirstName + " " + item.LastName));
            }

            ViewBag.Unit = new SelectList(GetUnits(), "iUnitId", "vName");
            ViewBag.PackersList = new SelectList(userslist, "IntegerData", "StringData");
            return View(Views.Create, new PackingRecord());
        }
        #endregion

        #region Get Packing Records List 
        /// <summary>
        /// Get Packing Records List
        /// </summary>
        /// <param name="page"></param>
        /// <returns></returns>
        [HttpGet]
        public virtual ActionResult PackingRecords(int? page = 1)
        {
            var recordsList = GetPackingRecords().OrderByDescending(x => x.iPackingRecordId);
            var pageNumber = page ?? 1;
            return View(Views.Index, recordsList.ToPagedList(pageNumber, GetPageSize()));
        }
        #endregion

        #region Return Packing Record Edit View
        /// <summary>
        /// Return Packing Record Edit View
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize(Roles = "Admin,Packing Manager")]
        public virtual async Task<ActionResult> EditPackingRecord(long id)
        {
            var users = UserManager.Users.ToList();
            // ReSharper disable once InconsistentNaming
            var Roles = await RoleManager.Roles.ToListAsync();
            // ReSharper disable once PossibleNullReferenceException
            var roleId = Roles.FirstOrDefault(m => m.Name == PKRoles.PackingStaff).Id;
            var list = users.Where(x => x.Roles.Any(y => y.RoleId == roleId)).ToList();

            var userslist = new List<Data>();

            // ReSharper disable once LoopCanBeConvertedToQuery
            foreach (var item in list)
            {
                userslist.Add(new Data(item.Id, item.FirstName + " " + item.LastName));
            }

            ViewBag.Unit = new SelectList(GetUnits(), "iUnitId", "vName");
            ViewBag.PackersList = new SelectList(userslist, "IntegerData", "StringData");
            // ReSharper disable once SuggestVarOrType_SimpleTypes
            PackingRecord packingRec = Mapper.Map<tPackingRecord, PackingRecord>(_packingRecordService.GetById(id));
            var packersList = _packingRecordDetailService.GetAll().Where(m => m.iPackingRecordId == packingRec.iPackingRecordId).ToList();

            packingRec.packersList = packersList[0].UserId;
            // ReSharper disable once SuggestVarOrType_BuiltInTypes
            for (int i = 1; i < packersList.Count; i++)
            {
                packingRec.packersList += "," + packersList[i].UserId;
            }

            return View(Views.Create, packingRec);
        }
        #endregion

        #region  Get All Packing Records
        /// <summary>
        /// Get All Packing Records
        /// </summary>
        /// <returns></returns>
        // ReSharper disable once ReturnTypeCanBeEnumerable.Local
        private List<PackingRecord> GetPackingRecords()
        {
            var packingRecordsList = _packingRecordService.GetAll();
            return packingRecordsList.Select(Mapper.Map<tPackingRecord, PackingRecord>).ToList();
        }
        #endregion

        #region Add/Edit
        /// <summary>
        /// Update Packing record if found else create new
        /// </summary>
        /// <param name="packingRecordModel"></param>
        /// <returns></returns>
        public virtual ActionResult PostPackingRecord(PackingRecord packingRecordModel)
        {
            try
            {
                string[] packersName = packingRecordModel.packersList.Split(',');

                //Edit
                if (packingRecordModel.iPackingRecordId > 0)
                {
                    var savediPackingRecord = Mapper.Map<PackingRecord, tPackingRecord>(packingRecordModel);

                    savediPackingRecord.Sys_ModifyBy = User.Identity.GetUserId();
                    savediPackingRecord.Sys_ModifyDateTime = DateTime.Now;
                    _packingRecordService.Update(savediPackingRecord);
                    ValidatePackerRecords(packersName, packingRecordModel);
                    AuditScope.CreateAndSave("Editing Packing Record", new { User = User.Identity.GetUserId() });
                }
                //Add
                else
                {
                    packingRecordModel.Sys_CreatedBy = User.Identity.GetUserId();
                    packingRecordModel.Sys_CreatedDateTime = DateTime.Now;
                    try
                    {
                        _packingRecordService.CreateRecord(Mapper.Map<PackingRecord, tPackingRecord>(packingRecordModel), packersName.ToList());
                        AuditScope.CreateAndSave("Saving Packing Record", new { User = User.Identity.GetUserId() });
                    }
                    catch (Exception ex)
                    {
                        AuditScope.CreateAndSave("Error Saving Packing Record: ", new { User = User.Identity.GetUserId(), Exception = ex.Message });
                        return RedirectToAction("Index", "Home");
                    }
                }
                return RedirectToAction(MVC.PackingRecord.PackingRecords());
            }
            catch (Exception ex)
            {
                AuditScope.CreateAndSave("Error Saving Packing Record: ", new { User = User.Identity.GetUserId(), Exception = ex.Message });
                return RedirectToAction(MVC.Home.Index());
            }
        }
        #endregion

        #region Get Units From DB
        /// <summary>
        /// Get Units From DB
        /// </summary>
        public List<Unit> GetUnits()
        {
            var units = new List<Unit>();
            var list = _unitService.GetAll();
            // ReSharper disable once LoopCanBeConvertedToQuery
            foreach (var item in list)
            {
                units.Add(Mapper.Map<tUnit, Unit>(item));
            }
            return units;
        }
        #endregion

        #region Private: Validate Packer Records
        /// <summary>
        /// Update Packing record if found else create new
        /// </summary>
        /// <param name="packerIDs"></param>
        /// <param name="packingRecord"></param>
        /// <returns></returns>
        private void ValidatePackerRecords(string[] packerIDs, PackingRecord packingRecord)
        {
            var packerRecords = _packingRecordDetailService.GetAll().Where(m => m.iPackingRecordId == packingRecord.iPackingRecordId).ToList();

            foreach (var packer in packerRecords)
            {
                if (!packerIDs.Contains(packer.UserId))
                {
                    _packingRecordDetailService.Delete(packer);
                }
            }

            for (int i = 0; i < packerIDs.Length; i++)
            {
                var existing = false;
                var packerId = packerIDs[i];

                foreach (var packer in packerRecords)
                {
                    if (packer.UserId == packerId)
                    {
                        existing = true;
                    }
                }

                if (!existing)
                {
                    _packingRecordService.CreatePackerRecord(Mapper.Map<PackingRecord, tPackingRecord>(packingRecord), packerId);
                    AuditScope.CreateAndSave("Creating new Packer Record for Packing Record.", new { User = User.Identity.GetUserId() });
                }

                // ReSharper disable once RedundantAssignment
                existing = false;
            }
        }
        #endregion

        #region Data Struct
        /// <summary>
        /// Data Struct for Batch Codes
        /// </summary>
        /// <returns></returns>
        public struct Data
        {
            public Data(string id, string name)
            {
                IntegerData = id;
                StringData = name;
            }

            public string IntegerData { get; private set; }
            public string StringData { get; private set; }
        }
        #endregion

        #region Print Record as Report

        /// <summary>
        /// Print Record
        /// </summary>
        /// <param name="pId">Id of the record to print</param>
        /// <returns>RDLC Report View</returns>
        public virtual ActionResult PrintRecord(Int32 pId)
        {
            try
            {
                return View("~/Views/Report/ReportPreview.aspx", new Report { ReportName = PkReportNames.PackingRecord, Id = pId, Service = _packingRecordService });
            }
            catch (Exception ex)
            {
                AuditScope.CreateAndSave("Error getting report of Packing Record", new { User = User.Identity.GetUserId() });
                // ReSharper disable once PossibleIntendedRethrow
                throw ex;
            }
        }
        #endregion
    }
}