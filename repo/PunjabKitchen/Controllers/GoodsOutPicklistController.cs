﻿using Audit.Core;
using AutoMapper;
using Microsoft.AspNet.Identity;
using PunjabKitchen.Models;
using PunjabKitchenBLL.Interfaces;
using PunjabKitchenEntities.EntityModel;
using System;
using System.Collections.Generic;
using System.Linq;
using PagedList;
using System.Web.Mvc;
using System.Collections;
using System.Web;
using Microsoft.AspNet.Identity.Owin;

namespace PunjabKitchen.Controllers
{
    [Authorize(Roles = "Admin,Goods Out Staff,Goods Out Manager")]
    public partial class GoodsOutPicklistController : BaseController
    {
        #region Data Members
        readonly IGoodsOutPicklistService _goodsOutPicklistService;
        readonly IGoodsOutPicklistDetailService _goodsOutPicklistDetailService;
        readonly IStockService _stockService;
        readonly IUnitService _unitService;
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="goodsOutPicklistService"></param>
        /// <param name="goodsOutPicklistDetailService"></param>
        /// <param name="stockService"></param>
        /// <param name="unitService"></param>
        public GoodsOutPicklistController(IGoodsOutPicklistService goodsOutPicklistService
                                        , IGoodsOutPicklistDetailService goodsOutPicklistDetailService
                                        , IStockService stockService
                                        , IUnitService unitService)
        {
            _goodsOutPicklistService = goodsOutPicklistService;
            _goodsOutPicklistDetailService = goodsOutPicklistDetailService;
            _stockService = stockService;
            _unitService = unitService;
        }

        #endregion

        #region Get Goods Out List 
        /// <summary>
        /// Get Goods Out List 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public virtual ActionResult Index(int? page)
        {
            var goodsOutPicklists = _goodsOutPicklistService.GetAll().OrderByDescending(x => x.iGoodsOutPickListId);
            var pageNumber = page ?? 1;
            var goodsOutPicklistsList = goodsOutPicklists.Select(item => Mapper.Map<tGoodsOutPicklist, GoodsOutPicklist>(item)).ToList();
            return View(Views.Index, goodsOutPicklistsList.ToPagedList(pageNumber, GetPageSize()));
        }

        #endregion

        #region Create New Plan
        /// <summary>
        /// Create New Plan
        /// </summary>
        /// <returns></returns>
        public virtual ActionResult Create()
        {
            var newPlan = new GoodsOutPicklist();
            newPlan.tGoodsOutPicklistDetails = new List<tGoodsOutPicklistDetail>();

            ViewBag.StockLocationList = new SelectList(GetStockLocationSelectList(), "iStockId", "vStorageLocation");
            ViewBag.UnitsList = new SelectList(GetUnits(), "iUnitId", "vName");

            return PartialView(Views.Create, newPlan);
        }

        #endregion

        #region Edit Goods Out Picklist
        /// <summary>
        ///  Edit Goods Out Picklist
        /// </summary>
        /// <returns></returns>
        public virtual ActionResult Edit(long id)
        {
            var goodsOutPicklist = Mapper.Map<tGoodsOutPicklist, GoodsOutPicklist>(_goodsOutPicklistService.GetById(id));

            ViewBag.StockLocationList = new SelectList(GetStockLocationSelectList(), "iStockId", "vStorageLocation");
            ViewBag.UnitsList = new SelectList(GetUnits(), "iUnitId", "vName");

            var lastModifiedByUser = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>().Users.FirstOrDefault(f => f.Id == goodsOutPicklist.Sys_ModifyBy);
            if (lastModifiedByUser != null)
                goodsOutPicklist.Sys_ModifyBy_Name = lastModifiedByUser.FirstName + " " + lastModifiedByUser.LastName;

            return PartialView(Views.Create, goodsOutPicklist);
        }

        #endregion

        #region Post: Goods Out Picklist Record
        /// <summary>
        /// Post: Goods Out Picklist Record
        /// </summary>
        /// <param name="goodsOutPicklist"></param>
        /// <returns></returns>
        public virtual JsonResult Post(GoodsOutPicklist goodsOutPicklist)
        {
            try
            {
                //Edit
                if (goodsOutPicklist.iGoodsOutPickListId > 0)
                {
                    goodsOutPicklist.Sys_ModifyBy = User.Identity.GetUserId();
                    goodsOutPicklist.Sys_ModifyDateTime = DateTime.Now;

                    EditGoodsOutPicklist(Mapper.Map<GoodsOutPicklist, tGoodsOutPicklist>(goodsOutPicklist));

                    if (goodsOutPicklist.detail_iRecipeId > 0)
                    {
                        goodsOutPicklist.tGoodsOutPicklistDetails = new List<tGoodsOutPicklistDetail>() { PopulateDetailRecord(goodsOutPicklist) };

                        if (goodsOutPicklist.detail_iGoodsOutPickListDetailId > 0)
                            EditGoodsOutPicklistDetail(goodsOutPicklist.tGoodsOutPicklistDetails.FirstOrDefault());
                        else
                            AddGoodsOutPicklistDetail(goodsOutPicklist.tGoodsOutPicklistDetails.FirstOrDefault());

                        // ReSharper disable once PossibleNullReferenceException
                        goodsOutPicklist.detail_iGoodsOutPickListDetailId = goodsOutPicklist.tGoodsOutPicklistDetails.FirstOrDefault().iGoodsOutPickListDetailId;
                    }
                    AuditScope.CreateAndSave("Editing Goods Out Picklist", new { User = User.Identity.GetUserId(), GoodsOutPicklistId = goodsOutPicklist.iGoodsOutPickListId });
                }
                //Add
                else
                {
                    goodsOutPicklist.Sys_CreatedBy = User.Identity.GetUserId();
                    goodsOutPicklist.Sys_CreatedDateTime = DateTime.Now;

                    if (goodsOutPicklist.detail_iRecipeId > 0)
                    {
                        goodsOutPicklist.tGoodsOutPicklistDetails = new List<tGoodsOutPicklistDetail>()
                        {
                            new tGoodsOutPicklistDetail()
                            {
                                bOldLabel = goodsOutPicklist.detail_bOldLabel,
                                iGoodsOutPickListDetailId = 0,
                                iGoodsOutPickListId = goodsOutPicklist.iGoodsOutPickListId,
                                iQuantity = goodsOutPicklist.detail_iQuantity,
                                iRecipeId = goodsOutPicklist.detail_iRecipeId,
                                iToFollow = goodsOutPicklist.detail_iToFollow,
                                iUnitId = goodsOutPicklist.detail_iUnitId,
                                Sys_CreatedBy = User.Identity.GetUserId(),
                                Sys_CreatedDateTime = DateTime.Now,
                                vBatchNo = goodsOutPicklist.detail_vBatchNo,
                                vShelf = goodsOutPicklist.detail_vShelf
                            }
                        };
                    }

                    var savedGoodsOutPicklist = AddGoodsOutPicklist(Mapper.Map<GoodsOutPicklist, tGoodsOutPicklist>(goodsOutPicklist));

                    goodsOutPicklist.iGoodsOutPickListId = savedGoodsOutPicklist.iGoodsOutPickListId;

                    if (goodsOutPicklist.detail_iRecipeId > 0)
                    {
                        // ReSharper disable once PossibleNullReferenceException
                        goodsOutPicklist.detail_iGoodsOutPickListDetailId = savedGoodsOutPicklist.tGoodsOutPicklistDetails.FirstOrDefault().iGoodsOutPickListDetailId;
                    }

                    AuditScope.CreateAndSave("Saving Goods Out Picklist", new { User = User.Identity.GetUserId(), GoodsInId = goodsOutPicklist.iGoodsOutPickListId });

                }
                return Json(ResponseToJson(goodsOutPicklist), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                AuditScope.CreateAndSave("Error Saving Goods Out Picklist Record: ", new { User = User.Identity.GetUserId(), Exception = ex.Message });
                throw;
            }
        }

        private static ArrayList ResponseToJson(GoodsOutPicklist goodsOutPicklist)
        {
            var jsonResult = new ArrayList();
            jsonResult.Add(new
            {
                // ReSharper disable once RedundantAnonymousTypePropertyName
                iGoodsOutPickListId = goodsOutPicklist.iGoodsOutPickListId,
                // ReSharper disable once RedundantAnonymousTypePropertyName
                detail_iGoodsOutPickListDetailId = goodsOutPicklist.detail_iGoodsOutPickListDetailId,
                // ReSharper disable once RedundantAnonymousTypePropertyName
                detail_vShelf = goodsOutPicklist.detail_vShelf,
                // ReSharper disable once RedundantAnonymousTypePropertyName
                detail_vItemCode = goodsOutPicklist.detail_vItemCode,
                // ReSharper disable once RedundantAnonymousTypePropertyName
                detail_vItemName = goodsOutPicklist.detail_vItemName,
                // ReSharper disable once RedundantAnonymousTypePropertyName
                detail_iQuantity = goodsOutPicklist.detail_iQuantity,
                // ReSharper disable once RedundantAnonymousTypePropertyName
                detail_vBatchNo = goodsOutPicklist.detail_vBatchNo,
                // ReSharper disable once RedundantAnonymousTypePropertyName
                detail_bOldLabel = goodsOutPicklist.detail_bOldLabel,
                // ReSharper disable once RedundantAnonymousTypePropertyName
                detail_iToFollow = goodsOutPicklist.detail_iToFollow,
                // ReSharper disable once RedundantAnonymousTypePropertyName
                detail_vUnitName = goodsOutPicklist.detail_vUnitName
            });
            return jsonResult;
        }

        #endregion

        #region Delete Detail Item
        /// <summary>
        /// Delete Addition Item
        /// </summary>
        /// <param name="iGoodsOutPicklistDetailId"></param>
        /// <param name="iRemainingRecords"></param>
        /// <returns></returns>
        public virtual JsonResult DeleteDetailItem(int iGoodsOutPicklistDetailId, int iRemainingRecords)
        {
            var deletedRecordID = 0;

            try
            {
                var recordtoDelete = _goodsOutPicklistDetailService.GetById(iGoodsOutPicklistDetailId);

                tGoodsOutPicklist parentRecord = null;
                if (iRemainingRecords <= 2)
                    // ReSharper disable once PossibleInvalidOperationException
                    parentRecord = _goodsOutPicklistService.GetById((long)recordtoDelete.iGoodsOutPickListId);

                if (recordtoDelete != null)
                {
                    _goodsOutPicklistDetailService.Delete(recordtoDelete);

                    if (parentRecord != null)
                        _goodsOutPicklistService.Delete(parentRecord);

                    AuditScope.CreateAndSave("Deleting detail Item in Goods In", new { User = User.Identity.GetUserId() });
                    return Json(new { status = "Success", iGoodsOutPicklistDetailId }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                AuditScope.CreateAndSave("Error deleting detail item in Goods Out Picklist: ", new { User = User.Identity.GetUserId(), Exception = ex.Message });
                throw;
            }
#pragma warning disable 162
            return Json(new { status = "Failed", deletedRecordID }, JsonRequestBehavior.AllowGet);
#pragma warning restore 162
        }
        #endregion

        #region Private: Goods Out Picklist & its Detail Creating, Editing
        /// <returns></returns>
        private tGoodsOutPicklistDetail PopulateDetailRecord(GoodsOutPicklist goodsOutPicklist)
        {
            return new tGoodsOutPicklistDetail()
            {
                bOldLabel = goodsOutPicklist.detail_bOldLabel,
                iGoodsOutPickListDetailId = 0,
                iGoodsOutPickListId = goodsOutPicklist.iGoodsOutPickListId,
                iQuantity = goodsOutPicklist.detail_iQuantity,
                iRecipeId = goodsOutPicklist.detail_iRecipeId,
                iToFollow = goodsOutPicklist.detail_iToFollow,
                iUnitId = goodsOutPicklist.detail_iUnitId,
                Sys_CreatedBy = User.Identity.GetUserId(),
                Sys_CreatedDateTime = DateTime.Now,
                vBatchNo = goodsOutPicklist.detail_vBatchNo,
                vShelf = goodsOutPicklist.detail_vShelf
            };
        }

        private void EditGoodsOutPicklist(tGoodsOutPicklist goodsOutPicklist)
        {
            _goodsOutPicklistService.Update(goodsOutPicklist);
        }

        private tGoodsOutPicklist AddGoodsOutPicklist(tGoodsOutPicklist goodsOutPicklist)
        {
            try
            {
                return _goodsOutPicklistService.Create(goodsOutPicklist);
            }
            catch (Exception ex)
            {
                AuditScope.CreateAndSave("Error saving goods out picklist record: ", new { User = User.Identity.GetUserId(), Exception = ex.Message });
                throw;
            }
        }

        // ReSharper disable once UnusedMethodReturnValue.Local
        private tGoodsOutPicklistDetail AddGoodsOutPicklistDetail(tGoodsOutPicklistDetail goodsOutPicklistDetail)
        {
            try
            {
                return _goodsOutPicklistDetailService.Create(goodsOutPicklistDetail);
            }
            catch (Exception ex)
            {
                AuditScope.CreateAndSave("Error saving goods out picklist detail record: ", new { User = User.Identity.GetUserId(), Exception = ex.Message });
                throw;
            }
        }

        private void EditGoodsOutPicklistDetail(tGoodsOutPicklistDetail goodsOutPicklistDetail)
        {
            try
            {
                _goodsOutPicklistDetailService.Update(goodsOutPicklistDetail);
            }
            catch (Exception ex)
            {
                AuditScope.CreateAndSave("Error editing goods out picklist detail record: ", new { User = User.Identity.GetUserId(), Exception = ex.Message });
                throw;
            }
        }

        // ReSharper disable once UnusedMember.Local
        private List<GoodsOutPicklist> GetAllGoodsOutPicklists()
        {
            var plansList = _goodsOutPicklistService.GetAll();
            return plansList.Select(Mapper.Map<tGoodsOutPicklist, GoodsOutPicklist>).ToList();
        }

        private List<StockLocationSelectList> GetStockLocationSelectList()
        {
            var stockLocations = new List<StockLocationSelectList>();
            var list = _stockService.GetAllDistinctStockLocations();
            // ReSharper disable once LoopCanBeConvertedToQuery
            foreach (var item in list)
            {
                stockLocations.Add(Mapper.Map<tStock, StockLocationSelectList>(item));
            }
            return stockLocations;
        }

        // ReSharper disable once UnusedMember.Local
        private List<StockPointSelectList> GetAllDistinctStockPoints(string stockLocation)
        {

            var stockLocations = new List<StockPointSelectList>();
            var stockPointslist = _stockService.GetAllDistinctStockPoints(stockLocation);
            // ReSharper disable once LoopCanBeConvertedToQuery
            foreach (var item in stockPointslist)
            {
                stockLocations.Add(Mapper.Map<tStock, StockPointSelectList>(item));
            }
            return stockLocations;
        }

        private List<Unit> GetUnits()
        {
            var units = new List<Unit>();
            var list = _unitService.GetAll();
            // ReSharper disable once LoopCanBeConvertedToQuery
            foreach (var item in list)
            {
                units.Add(Mapper.Map<tUnit, Unit>(item));
            }
            return units;
        }
        #endregion

        #region Print Record as Report

        /// <summary>
        /// Print Record
        /// </summary>
        /// <param name="pId">Id of the record to print</param>
        /// <returns>RDLC Report View</returns>
        public virtual ActionResult PrintRecord(Int32 pId)
        {
            try
            {
                return View("~/Views/Report/ReportPreview.aspx", new Report { ReportName = PkReportNames.GoodsOutPickList, Id = pId, Service = _goodsOutPicklistService });
            }
            catch (Exception ex)
            {
                AuditScope.CreateAndSave("Error getting report of Goods Out Picklist", new { User = User.Identity.GetUserId() });
                // ReSharper disable once PossibleIntendedRethrow
                throw ex;
            }
        }
        #endregion
    }
}