﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Audit.Core;
using AutoMapper;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using PagedList;
using PunjabKitchen.Models;
using PunjabKitchen.Properties;
using PunjabKitchenBLL.Interfaces;
using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchen.Controllers
{
    [Authorize(Roles = "Admin,Packing Manager,Packing  Staff")]
    public partial class PackingRecord2Controller : BaseController
    {
        #region Data Members
        readonly IPackingRecord2Service _packingRecord2Service;
        readonly IPackingRecord2DetailService _packingRecord2DetailService;
        readonly IUnitService _unitService;
        readonly ICookingPlanService _cookingPlanService;
        #endregion

        #region User Manager
        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            // ReSharper disable once UnusedMember.Local
            private set
            {
                _userManager = value;
            }
        }

        private ApplicationRoleManager _roleManager;
        public ApplicationRoleManager RoleManager
        {
            get
            {
                return _roleManager ?? HttpContext.GetOwinContext().Get<ApplicationRoleManager>();
            }
            // ReSharper disable once UnusedMember.Local
            private set
            {
                _roleManager = value;
            }
        }
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="packingRecord2Service"></param>
        /// <param name="packingRecord2DetailService"></param>
        /// <param name="unitService"></param>
        /// <param name="cookingPlanService"></param>
        public PackingRecord2Controller(IPackingRecord2Service packingRecord2Service, IPackingRecord2DetailService packingRecord2DetailService, IUnitService unitService, ICookingPlanService cookingPlanService)
        {
            _packingRecord2Service = packingRecord2Service;
            _packingRecord2DetailService = packingRecord2DetailService;
            _unitService = unitService;
            _cookingPlanService = cookingPlanService;
        }
        #endregion

        #region Create New Packing Record 2
        /// <summary>
        /// Create New Packing Record 2
        /// </summary>
        /// <returns></returns>
        public virtual async Task<ActionResult> CreatePackingRecord2()
        {
            var users = UserManager.Users.ToList();
            // ReSharper disable once InconsistentNaming
            var Roles = await RoleManager.Roles.ToListAsync();
            // ReSharper disable once PossibleNullReferenceException
            var roleId = Roles.FirstOrDefault(m => m.Name == PKRoles.PackingStaff).Id;
            var list = users.Where(x => x.Roles.Any(y => y.RoleId == roleId)).ToList();

            var userslist = new List<ShapesControlPackingController.Data>();

            // ReSharper disable once LoopCanBeConvertedToQuery
            foreach (var item in list)
            {
                userslist.Add(new ShapesControlPackingController.Data(item.Id, item.FirstName + " " + item.LastName));
            }

            ViewBag.Unit = new SelectList(GetUnits(), "iUnitId", "vName");
            ViewBag.PackersList = new SelectList(userslist, "IntegerData", "StringData");
            return PartialView(Views.Create, new PackingRecord2());
        }
        #endregion

        #region Get Packing Records 2 List 
        /// <summary>
        /// Get Packing Records 2 List
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public virtual ActionResult PackingRecords2(int? page = 1)
        {
            var recordsList = GetPackingRecords2().OrderByDescending(x => x.iPackingRecordId);
            var pageNumber = page ?? 1;
            return View(Views.Index, recordsList.ToPagedList(pageNumber, GetPageSize()));
        }
        #endregion

        #region Return Packing Record 2 Edit View
        /// <summary>
        /// Return Packing Record 2 Edit View
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize(Roles = "Admin,Packing Manager")]
        public virtual async Task<ActionResult> EditPackingRecord2(long id)
        {
            var users = UserManager.Users.ToList();
            // ReSharper disable once InconsistentNaming
            var Roles = await RoleManager.Roles.ToListAsync();
            // ReSharper disable once PossibleNullReferenceException
            var roleId = Roles.FirstOrDefault(m => m.Name == PKRoles.PackingStaff).Id;
            var list = users.Where(x => x.Roles.Any(y => y.RoleId == roleId)).ToList();

            var userslist = new List<ShapesControlPackingController.Data>();

            // ReSharper disable once LoopCanBeConvertedToQuery
            foreach (var item in list)
            {
                userslist.Add(new ShapesControlPackingController.Data(item.Id, item.FirstName + " " + item.LastName));
            }

            ViewBag.Unit = new SelectList(GetUnits(), "iUnitId", "vName");
            ViewBag.PackersList = new SelectList(userslist, "IntegerData", "StringData");

            PackingRecord2 packingRec2 = Mapper.Map<tPackingRecord2, PackingRecord2>(_packingRecord2Service.GetById(id));
            var packersList = _packingRecord2DetailService.GetAll().Where(m => m.iPackingRecordId == packingRec2.iPackingRecordId).ToList();

            packingRec2.packersList = packersList[0].UserId;
            // ReSharper disable once SuggestVarOrType_BuiltInTypes
            for (int i = 1; i < packersList.Count; i++)
            {
                packingRec2.packersList += "," + packersList[i].UserId;
            }

            return PartialView(Views.Create, packingRec2);
        }
        #endregion

        #region  Get All Packing Records 2
        /// <summary>
        /// Get All Packing Records 2
        /// </summary>
        /// <returns></returns>
        private List<PackingRecord2> GetPackingRecords2()
        {
            var packingRecords2List = _packingRecord2Service.GetAll();
            return packingRecords2List.Select(Mapper.Map<tPackingRecord2, PackingRecord2>).ToList();
        }
        #endregion

        #region Add/Edit
        /// <summary>
        /// Update Packing Record 2 if found else create new
        /// </summary>
        /// <param name="packingRecord2Model"></param>
        /// <returns></returns>
        public virtual ActionResult PostPackingRecord2(PackingRecord2 packingRecord2Model)
        {
            try
            {
                string[] packersName = packingRecord2Model.packersList.Split(',');

                //Edit
                if (packingRecord2Model.iPackingRecordId > 0)
                {
                    var packingRecModified = Mapper.Map<PackingRecord2, tPackingRecord2>(packingRecord2Model);
                    packingRecModified.Sys_ModifyBy = User.Identity.GetUserId();
                    packingRecModified.Sys_ModifyDateTime = DateTime.Now;
                    // ReSharper disable once PossibleInvalidOperationException
                    packingRecModified.tCookingPlan = _cookingPlanService.GetById((long)packingRecord2Model.iCookingPlanId);
                    _packingRecord2Service.Update(packingRecModified);
                    ValidatePackerRecords(packersName, packingRecord2Model);
                    AuditScope.CreateAndSave("Editing Packing Room Record.", new { User = User.Identity.GetUserId() });
                }
                //Add
                else
                {
                    packingRecord2Model.Sys_CreatedBy = User.Identity.GetUserId();
                    packingRecord2Model.Sys_CreatedDateTime = DateTime.Now;
                    packingRecord2Model.tCookingPlan = null;
                    try
                    {
                        _packingRecord2Service.CreateRecord(Mapper.Map<PackingRecord2, tPackingRecord2>(packingRecord2Model), packersName.ToList());
                        AuditScope.CreateAndSave("Adding Packing Room Record.", new { User = User.Identity.GetUserId() });
                    }
                    catch (Exception ex)
                    {
                        AuditScope.CreateAndSave("Error Saving Packing Room Record: ", new { User = User.Identity.GetUserId(), Exception = ex.Message });
                        throw;
                    }
                }
                return RedirectToAction(MVC.PackingRecord2.PackingRecords2());
            }
            catch (Exception ex)
            {
                AuditScope.CreateAndSave("Error Saving Packing Room Record: ", new { User = User.Identity.GetUserId(), Exception = ex.Message });
                throw;
            }
        }
        #endregion

        #region Get Units From DB
        /// <summary>
        /// Get Units From DB
        /// </summary>
        public List<Unit> GetUnits()
        {
            var units = new List<Unit>();
            var list = _unitService.GetAll();
            // ReSharper disable once LoopCanBeConvertedToQuery
            foreach (var item in list)
            {
                units.Add(Mapper.Map<tUnit, Unit>(item));
            }
            return units;
        }
        #endregion

        #region Private: Validate Packer Records
        /// <summary>
        /// Private: Validate Packer Records
        /// </summary>
        /// <param name="packerIDs"></param>
        /// <param name="packingRecord2"></param>
        /// <returns></returns>
        private void ValidatePackerRecords(string[] packerIDs, PackingRecord2 packingRecord2)
        {
            var packerRecords = _packingRecord2DetailService.GetAll().Where(m => m.iPackingRecordId == packingRecord2.iPackingRecordId).ToList();

            // ReSharper disable once LoopCanBePartlyConvertedToQuery
            foreach (var packer in packerRecords)
            {
                if (!packerIDs.Contains(packer.UserId))
                {
                    _packingRecord2DetailService.Delete(packer);
                }
            }

            // ReSharper disable once ForCanBeConvertedToForeach
            for (int i = 0; i < packerIDs.Length; i++)
            {
                var existing = false;
                var packerId = packerIDs[i];

                // ReSharper disable once LoopCanBePartlyConvertedToQuery
                foreach (var packer in packerRecords)
                {
                    if (packer.UserId == packerId)
                    {
                        existing = true;
                    }
                }

                if (!existing)
                {
                    _packingRecord2Service.CreatePackerRecord(Mapper.Map<PackingRecord2, tPackingRecord2>(packingRecord2), packerId);
                    AuditScope.CreateAndSave("Creating new Packer Record for Packing Room Record.", new { User = User.Identity.GetUserId() });
                }

                // ReSharper disable once RedundantAssignment
                existing = false;
            }
        }
        #endregion

        #region Print Record as Report

        /// <summary>
        /// Print Record
        /// </summary>
        /// <param name="pId">Id of the record to print</param>
        /// <returns>RDLC Report View</returns>
        public virtual ActionResult PrintRecord(Int32 pId)
        {
            try
            {
                return View("~/Views/Report/ReportPreview.aspx", new Report { ReportName = PkReportNames.PackingRoomRecord, Id = pId, Service = _packingRecord2Service });
            }
            catch (Exception ex)
            {
                AuditScope.CreateAndSave("Error getting report of Packing Room Record", new { User = User.Identity.GetUserId() });
                // ReSharper disable once PossibleIntendedRethrow
                throw ex;
            }
        }
        #endregion
    }
}