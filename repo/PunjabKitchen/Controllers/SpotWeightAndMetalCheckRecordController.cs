﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Audit.Core;
using AutoMapper;
using Microsoft.AspNet.Identity;
using PunjabKitchen.Models;
using PunjabKitchenBLL.Interfaces;
using PunjabKitchenEntities.EntityModel;
using PagedList;

namespace PunjabKitchen.Controllers
{
    [Authorize(Roles = "Admin,Packing Manager,Packing  Staff")]
    public partial class SpotWeightAndMetalCheckRecordController : BaseController
    {
        #region Data Members
        readonly IFMDetectionAndPackWeightRecordService _fMDetectionAndPackingWeightService;
        // ReSharper disable once ArrangeTypeMemberModifiers
        readonly ICookingPlanService _cookingPlanService;
#endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="fMDetectionAndPackingWeightService"></param>
        /// <param name="cookingPlanService"></param>
        public SpotWeightAndMetalCheckRecordController(IFMDetectionAndPackWeightRecordService fMDetectionAndPackingWeightService, ICookingPlanService cookingPlanService)
        {
            _fMDetectionAndPackingWeightService = fMDetectionAndPackingWeightService;
            _cookingPlanService = cookingPlanService;
        }
        #endregion

        #region Create New Spot Weight And Metal Check Record
        /// <summary>
        /// Create New Spot Weight And Metal Check Record
        /// </summary>
        /// <returns></returns>
        public virtual ActionResult CreateSpotWeightAndMetalCheckRecord()
        {
            return View(Views.Create, new FMDetectionAndPackWeightTest());
        }
        #endregion

        #region Get Spot Weight And Metal Check Records List 
        /// <summary>
        /// Get Spot Weight And Metal Check Records List 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public virtual ActionResult SpotWeightAndMetalCheckRecords(int? page = 1)
        {
            var recordsList = GetSpotWeightAndMetalCheckRecords().OrderByDescending(x => x.iDetectionPackWeightTestId);
            var pageNumber = page ?? 1;
            return View(Views.Index, recordsList.ToPagedList(pageNumber, GetPageSize()));
        }
        #endregion

        #region Return Spot Weight And Metal Check Records Create New View
        /// <summary>
        /// Return Spot Weight And Metal Check Records Create New View
        /// </summary>
        /// <returns></returns>
        public virtual ActionResult CreateSpotWeightAndMetalCheckRecordCreateView()
        {
            return PartialView(Views.Create);
        }
        #endregion

        #region Return Spot Weight And Metal Check Record Edit View
        /// <summary>
        /// Return Spot Weight And Metal Check Record Edit View
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize(Roles = "Admin,Packing Manager")]
        public virtual ActionResult EditSpotWeightAndMetalCheckRecord(long id)
        {
            // ReSharper disable once SuggestVarOrType_SimpleTypes
            FMDetectionAndPackWeightTest spotWeightAndMetalCheckRec = Mapper.Map<tFMDetectionAndPackWeightTest, FMDetectionAndPackWeightTest>(_fMDetectionAndPackingWeightService.GetById(id));
            return View(Views.Create, spotWeightAndMetalCheckRec);
        }
        #endregion

        #region  Get All Spot Weight And Metal Check Records
        /// <summary>
        /// Get All Spot Weight And Metal Check Records
        /// </summary>
        /// <returns></returns>
        // ReSharper disable once ReturnTypeCanBeEnumerable.Local
        private List<FMDetectionAndPackWeightTest> GetSpotWeightAndMetalCheckRecords()
        {
            var spotWeightAndMetalCheckRecordList = _fMDetectionAndPackingWeightService.GetAll().Where(m => m.vTestType == "Spot Weight And Metal Check Record");
            return spotWeightAndMetalCheckRecordList.Select(Mapper.Map<tFMDetectionAndPackWeightTest, FMDetectionAndPackWeightTest>).ToList();
        }
        #endregion

        #region Add/Edit
        /// <summary>
        /// Update Spot Weight And Metal Check Record if found else create new
        /// </summary>
        /// <param name="spotWeightAndMetalCheckRecord"></param>
        /// <returns></returns>
        public virtual ActionResult PostSpotWeightAndMetalCheckRecord(FMDetectionAndPackWeightTest spotWeightAndMetalCheckRecord)
        {
            try
            {
                //Edit
                if (spotWeightAndMetalCheckRecord.iDetectionPackWeightTestId > 0)
                {
                    var savedSpotWeightAndMetalCheckRecord = Mapper.Map<FMDetectionAndPackWeightTest, tFMDetectionAndPackWeightTest>(spotWeightAndMetalCheckRecord);
                    savedSpotWeightAndMetalCheckRecord.Sys_ModifyBy = User.Identity.GetUserId();
                    savedSpotWeightAndMetalCheckRecord.Sys_ModifyDateTime = DateTime.Now;
                    // ReSharper disable once PossibleInvalidOperationException
                    savedSpotWeightAndMetalCheckRecord.tCookingPlan = _cookingPlanService.GetById((long)spotWeightAndMetalCheckRecord.iCookingPlanId);
                    savedSpotWeightAndMetalCheckRecord.vTestType = "Spot Weight And Metal Check Record";
                    _fMDetectionAndPackingWeightService.Update(savedSpotWeightAndMetalCheckRecord);
                    AuditScope.CreateAndSave("Editing Spot Weight And Metal Check Record", new { User = User.Identity.GetUserId() });
                }
                //Add
                else
                {
                    spotWeightAndMetalCheckRecord.Sys_CreatedBy = User.Identity.GetUserId();
                    spotWeightAndMetalCheckRecord.Sys_CreatedDateTime = DateTime.Now;
                    spotWeightAndMetalCheckRecord.tCookingPlan = null;
                    spotWeightAndMetalCheckRecord.vTestType = "Spot Weight And Metal Check Record";
                    try
                    {
                        _fMDetectionAndPackingWeightService.Create(Mapper.Map<FMDetectionAndPackWeightTest, tFMDetectionAndPackWeightTest>(spotWeightAndMetalCheckRecord));
                        AuditScope.CreateAndSave("Saving Spot Weight Record", new { User = User.Identity.GetUserId() });
                    }
                    catch (Exception ex)
                    {
                        AuditScope.CreateAndSave("Error Saving Spot Weight Record: ", new { User = User.Identity.GetUserId(), Exception = ex.Message });
                        return RedirectToAction(MVC.Home.Index());
                    }
                }
                return RedirectToAction(MVC.SpotWeightAndMetalCheckRecord.SpotWeightAndMetalCheckRecords());
            }
            catch (Exception ex)
            {
                AuditScope.CreateAndSave("Error saving spot weight record: ", new { User = User.Identity.GetUserId(), Exception = ex.Message });
                return RedirectToAction(MVC.Home.Index());
            }
        }
        #endregion

        #region Print Record as Report

        /// <summary>
        /// Print Record
        /// </summary>
        /// <param name="pId">Id of the record to print</param>
        /// <returns>RDLC Report View</returns>
        public virtual ActionResult PrintRecord(Int32 pId)
        {
            try
            {
                return View("~/Views/Report/ReportPreview.aspx", new Report { ReportName = PkReportNames.SpotWeightAndMetalCheckRecord, Id = pId, Service = _fMDetectionAndPackingWeightService });
            }
            catch (Exception ex)
            {
                AuditScope.CreateAndSave("Error getting report of Spot Weight Record", new { User = User.Identity.GetUserId() });
                // ReSharper disable once PossibleIntendedRethrow
                throw ex;
            }
        }
        #endregion
    }
}