﻿using Audit.Core;
using AutoMapper;
using Microsoft.AspNet.Identity;
using PunjabKitchen.Models;
using PunjabKitchenBLL.Interfaces;
using PunjabKitchenEntities.EntityModel;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace PunjabKitchen.Controllers
{
    [Authorize]
    public partial class CookingPickListController : BaseController
    {
        #region Data Members
        readonly ICookingPickListService _cookingPickListService;
        readonly IUnitService _unitService;
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="cookingPickListService"></param>
        /// <param name="unitService"></param>
        public CookingPickListController(ICookingPickListService cookingPickListService, IUnitService unitService)
        {
            _cookingPickListService = cookingPickListService;
            _unitService = unitService;
        }
        #endregion

        #region Private: Get Units
        /// <summary>
        /// Private: Get Units
        /// </summary>
        /// <returns></returns>
        private List<Unit> GetUnits()
        {
            var units = new List<Unit>();
            var list = _unitService.GetAll();
            // ReSharper disable once LoopCanBeConvertedToQuery
            foreach (var item in list)
            {
                units.Add(Mapper.Map<tUnit, Unit>(item));
            }
            return units;
        }
        #endregion

        #region Create New Cooking Pick List Record
        /// <summary>
        /// Create New Cooking Pick List Record
        /// </summary>
        /// <returns></returns>
        public virtual ActionResult Create()
        {
            ViewBag.Unit = new SelectList(GetUnits(), "iUnitId", "vName");
            return PartialView(Views.CookingPickListModal, new PickList());
        }
        #endregion

        #region Home View 
        /// <summary>
        /// Home View 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public virtual ActionResult HomeView()
        {
            return View("~/Views/Home/Index.cshtml");
        }
        #endregion

        #region Add/Edit
        /// <summary>
        /// Update Cooking Pick List record if found otherwise create a new record
        /// </summary>
        /// <param name="pickListModel"></param>
        /// <returns></returns>
        public virtual ActionResult Post(PickList pickListModel)
        {
            try
            {
                //Edit
                if (pickListModel.iPickListId > 0)
                {
                    var savediPickListRecord = _cookingPickListService.GetById(pickListModel.iPickListId);

                    savediPickListRecord.Sys_ModifyBy = User.Identity.GetUserId();
                    savediPickListRecord.Sys_ModifyDateTime = DateTime.Now;
                    _cookingPickListService.Update(savediPickListRecord);
                    AuditScope.CreateAndSave("Editing Cooking Pick List", new { User = User.Identity.GetUserId() });
                    //added comments
                }
                //Add
                else
                {
                    pickListModel.Sys_CreatedBy = User.Identity.GetUserId();
                    pickListModel.Sys_CreatedDateTime = DateTime.Now;
                    try
                    {
                        _cookingPickListService.Create(Mapper.Map<PickList, tPickList>(pickListModel));
                        AuditScope.CreateAndSave("Adding Cooking Pick List", new { User = User.Identity.GetUserId() });
                    }
                    catch (Exception ex)
                    {
                        AuditScope.CreateAndSave("Error saving cooking picklist record: ", new { User = User.Identity.GetUserId(), Exception = ex.Message });
                        throw;
                    }
                }
                return RedirectToAction(MVC.CookingPickList.HomeView());
            }
            catch (Exception ex)
            {
                AuditScope.CreateAndSave("Error saving cooking picklist record: ", new { User = User.Identity.GetUserId(), Exception = ex.Message });
                throw;
            }
        }
        #endregion
    }
}