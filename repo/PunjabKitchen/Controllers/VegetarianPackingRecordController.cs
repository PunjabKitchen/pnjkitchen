﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Audit.Core;
using AutoMapper;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using PunjabKitchen.Models;
using PunjabKitchen.Properties;
using PunjabKitchenBLL.Interfaces;
using PunjabKitchenEntities.EntityModel;
using PagedList;

namespace PunjabKitchen.Controllers
{
    [Authorize(Roles = "Admin,Packing Manager,Packing  Staff")]
    public partial class VegetarianPackingRecordController : BaseController
    {
        #region Data Members
        readonly IVegetarianPackingRecordService _vegetarianRecordService;
        readonly IVegetarianPackingRecordDetailService _vegetarianRecordDetailService;
        readonly IUnitService _unitService;
        #endregion

        #region User Manager
        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            // ReSharper disable once UnusedMember.Local
            private set
            {
                _userManager = value;
            }
        }

        private ApplicationRoleManager _roleManager;
        public ApplicationRoleManager RoleManager
        {
            get
            {
                return _roleManager ?? HttpContext.GetOwinContext().Get<ApplicationRoleManager>();
            }
            // ReSharper disable once UnusedMember.Local
            private set
            {
                _roleManager = value;
            }
        }
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="vegetarianRecordService"></param>
        /// <param name="vegetarianRecordDetailService"></param>
        /// <param name="unitService"></param>
        public VegetarianPackingRecordController(IVegetarianPackingRecordService vegetarianRecordService, IVegetarianPackingRecordDetailService vegetarianRecordDetailService, IUnitService unitService)
        {
            _vegetarianRecordService = vegetarianRecordService;
            _vegetarianRecordDetailService = vegetarianRecordDetailService;
            _unitService = unitService;
        }
        #endregion

        #region Create New Vegetarian Record
        /// <summary>
        /// Create New Vegetarian Record
        /// </summary>
        /// <returns></returns>
        public virtual async Task<ActionResult> CreateVegetarianRecord()
        {
            var users = UserManager.Users.ToList();
            // ReSharper disable once InconsistentNaming
            var Roles = await RoleManager.Roles.ToListAsync();
            // ReSharper disable once PossibleNullReferenceException
            var roleId = Roles.FirstOrDefault(m => m.Name == PKRoles.PackingStaff).Id;
            var list = users.Where(x => x.Roles.Any(y => y.RoleId == roleId)).ToList();

            var userslist = new List<Data>();

            // ReSharper disable once LoopCanBeConvertedToQuery
            foreach (var item in list)
            {
                userslist.Add(new Data(item.Id, item.FirstName + " " + item.LastName));
            }

            ViewBag.Unit = new SelectList(GetUnits(), "iUnitId", "vName");
            ViewBag.PackersList = new SelectList(userslist, "IntegerData", "StringData");
            return View(Views.Create, new VegetarianPackingRecord());
        }
        #endregion

        #region Get Vegetarians Records List 
        /// <summary>
        /// Get Vegetarians Records List
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public virtual ActionResult VegetarianRecords(int? page = 1)
        {
            var recordsList = GetVegetarianRecords().OrderByDescending(x => x.iVegeRecordId);
            var pageNumber = page ?? 1;
            return View(Views.Index, recordsList.ToPagedList(pageNumber, GetPageSize()));
        }
        #endregion

        #region Return Vegetarian Record Edit View
        /// <summary>
        /// Return Vegetarian Record Edit View
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize(Roles = "Admin,Packing Manager")]
        public virtual async Task<ActionResult> EditVegetarianRecord(long id)
        {
            var users = UserManager.Users.ToList();
            // ReSharper disable once InconsistentNaming
            var Roles = await RoleManager.Roles.ToListAsync();
            // ReSharper disable once PossibleNullReferenceException
            var roleId = Roles.FirstOrDefault(m => m.Name == PKRoles.PackingStaff).Id;
            var list = users.Where(x => x.Roles.Any(y => y.RoleId == roleId)).ToList();

            var userslist = new List<Data>();

            // ReSharper disable once LoopCanBeConvertedToQuery
            foreach (var item in list)
            {
                userslist.Add(new Data(item.Id, item.FirstName + " " + item.LastName));
            }

            ViewBag.Unit = new SelectList(GetUnits(), "iUnitId", "vName");
            ViewBag.PackersList = new SelectList(userslist, "IntegerData", "StringData");
            // ReSharper disable once SuggestVarOrType_SimpleTypes
            VegetarianPackingRecord vegeRec = Mapper.Map<tVegetarianPackingRecord, VegetarianPackingRecord>(_vegetarianRecordService.GetById(id));
            var packersList = _vegetarianRecordDetailService.GetAll().Where(m => m.iVegeRecordId == vegeRec.iVegeRecordId).ToList();

            vegeRec.packersList = packersList[0].UserId;
            for (var i = 1; i < packersList.Count; i++)
            {
                vegeRec.packersList += "," + packersList[i].UserId;
            }

            return View(Views.Create, vegeRec);
        }
        #endregion

        #region  Get All Vegetarian Records
        /// <summary>
        /// Get All Vegetarian Records
        /// </summary>
        /// <returns></returns>
        // ReSharper disable once ReturnTypeCanBeEnumerable.Local
        private List<VegetarianPackingRecord> GetVegetarianRecords()
        {
            var vegetarianRecordsList = _vegetarianRecordService.GetAll();
            return vegetarianRecordsList.Select(Mapper.Map<tVegetarianPackingRecord, VegetarianPackingRecord>).ToList();
        }
        #endregion

        #region Add/Edit
        /// <summary>
        /// Update Vegetarian record if found else create new
        /// </summary>
        /// <param name="vegetarianPackingRecordModel"></param>
        /// <returns></returns>
        public virtual ActionResult PostVegetarianRecord(VegetarianPackingRecord vegetarianPackingRecordModel)
        {
            try
            {
                // ReSharper disable once SuggestVarOrType_Elsewhere
                string[] packersName = vegetarianPackingRecordModel.packersList.Split(',');

                //Edit
                if (vegetarianPackingRecordModel.iVegeRecordId > 0)
                {
                    var savediVegetarianRecord = Mapper.Map<VegetarianPackingRecord, tVegetarianPackingRecord>(vegetarianPackingRecordModel);

                    savediVegetarianRecord.Sys_ModifyBy = User.Identity.GetUserId();
                    savediVegetarianRecord.Sys_ModifyDateTime = DateTime.Now;
                    _vegetarianRecordService.Update(savediVegetarianRecord);
                    ValidatePackerRecords(packersName, vegetarianPackingRecordModel);
                    AuditScope.CreateAndSave("Editing Vegetarian Record", new { User = User.Identity.GetUserId() });
                }
                //Add
                else
                {
                    vegetarianPackingRecordModel.Sys_CreatedBy = User.Identity.GetUserId();
                    vegetarianPackingRecordModel.Sys_CreatedDateTime = DateTime.Now;
                    try
                    {
                        _vegetarianRecordService.CreateRecord(Mapper.Map<VegetarianPackingRecord, tVegetarianPackingRecord>(vegetarianPackingRecordModel), packersName.ToList());
                        AuditScope.CreateAndSave("Saving Vegetarian Record", new { User = User.Identity.GetUserId() });
                    }
                    catch (Exception ex)
                    {
                        AuditScope.CreateAndSave("Error Saving Vegetarian Packing Record: ", new { User = User.Identity.GetUserId(), Exception = ex.Message });
                        return RedirectToAction(MVC.Home.Index());
                    }
                }
                return RedirectToAction(MVC.VegetarianPackingRecord.VegetarianRecords());
            }
            catch (Exception ex)
            {
                AuditScope.CreateAndSave("Error Saving Vegetarian Packing Record: ", new { User = User.Identity.GetUserId(), Exception = ex.Message });
                return RedirectToAction(MVC.Home.Index());
            }
        }
        #endregion

        #region Get Units From DB
        /// <summary>
        /// Get Units From DB
        /// </summary>
        public List<Unit> GetUnits()
        {
            var units = new List<Unit>();
            var list = _unitService.GetAll();
            // ReSharper disable once LoopCanBeConvertedToQuery
            foreach (var item in list)
            {
                units.Add(Mapper.Map<tUnit, Unit>(item));
            }
            return units;
        }
        #endregion

        #region Private: Validate Packer Records
        // ReSharper disable once SuggestBaseTypeForParameter
        private void ValidatePackerRecords(string[] packerIDs, VegetarianPackingRecord vegetarianPackingRecord)
        {
            var packerRecords = _vegetarianRecordDetailService.GetAll().Where(m => m.iVegeRecordId == vegetarianPackingRecord.iVegeRecordId).ToList();

            // ReSharper disable once LoopCanBePartlyConvertedToQuery
            foreach (var packer in packerRecords)
            {
                if (!packerIDs.Contains(packer.UserId))
                {
                    _vegetarianRecordDetailService.Delete(packer);
                }
            }

            // ReSharper disable once ForCanBeConvertedToForeach
            for (var i = 0; i < packerIDs.Length; i++)
            {
                var existing = false;
                var packerId = packerIDs[i];

                // ReSharper disable once LoopCanBePartlyConvertedToQuery
                foreach (var packer in packerRecords)
                {
                    if (packer.UserId == packerId)
                    {
                        existing = true;
                    }
                }

                if (!existing)
                {
                    _vegetarianRecordService.CreatePackerRecord(Mapper.Map<VegetarianPackingRecord, tVegetarianPackingRecord>(vegetarianPackingRecord), packerId);
                    AuditScope.CreateAndSave("Creating new Packer Record for Vegetarian Record.", new { User = User.Identity.GetUserId() });
                }

                // ReSharper disable once RedundantAssignment
                existing = false;
            }
        }
        #endregion

        #region Data Struct
        /// <summary>
        /// Data Struct for Batch Codes
        /// </summary>
        /// <returns></returns>
        public struct Data
        {
            public Data(string id, string name)
            {
                IntegerData = id;
                StringData = name;
            }

            public string IntegerData { get; private set; }
            public string StringData { get; private set; }
        }
        #endregion

        #region Print Record as Report

        /// <summary>
        /// Print Record
        /// </summary>
        /// <param name="pId">Id of the record to print</param>
        /// <returns>RDLC Report View</returns>
        public virtual ActionResult PrintRecord(Int32 pId)
        {
            try
            {
                return View("~/Views/Report/ReportPreview.aspx", new Report { ReportName = PkReportNames.VegetarianPackingRecord, Id = pId, Service = _vegetarianRecordService });
            }
            catch (Exception ex)
            {
                AuditScope.CreateAndSave("Error getting report of Vegetarian Packing Record", new { User = User.Identity.GetUserId() });
                // ReSharper disable once PossibleIntendedRethrow
                throw ex;
            }
        }
        #endregion
    }
}