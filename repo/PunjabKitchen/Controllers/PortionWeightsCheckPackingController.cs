﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Audit.Core;
using AutoMapper;
using Microsoft.AspNet.Identity;
using PunjabKitchen.Models;
using PunjabKitchenBLL.Interfaces;
using PunjabKitchenEntities.EntityModel;
using PagedList;

namespace PunjabKitchen.Controllers
{
    [Authorize(Roles = "Admin,Packing Manager,Packing  Staff")]
    public partial class PortionWeightsCheckPackingController : BaseController
    {
        #region Data Members
        readonly IPortionWeightsCheckRecordService _portionWeightsCheckPackingRecordService;
        readonly IPortionWeightsCheckRecordDetailService _portionWeightsCheckPackingRecordDetailService;
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="portionWeightsCheckPackingRecordService"></param>
        /// <param name="portionWeightsCheckPackingRecordDetailService"></param>
        public PortionWeightsCheckPackingController(IPortionWeightsCheckRecordService portionWeightsCheckPackingRecordService, IPortionWeightsCheckRecordDetailService portionWeightsCheckPackingRecordDetailService)
        {
            _portionWeightsCheckPackingRecordService = portionWeightsCheckPackingRecordService;
            _portionWeightsCheckPackingRecordDetailService = portionWeightsCheckPackingRecordDetailService;
        }
        #endregion

        #region Create New Portion Weight Check Packing Record
        /// <summary>
        /// Create New Portion Weight Check Packing Record
        /// </summary>
        /// <returns></returns>
        public virtual ActionResult CreatePortionWeightCheckPacking()
        {
            var portionWeightWithDetail = new PortionWeightRecordWithDetail();
            return PartialView(Views.Create, portionWeightWithDetail);
        }
        #endregion

        #region Get Portion Weight Check Packing Record List 
        /// <summary>
        /// GET: Hot Packing Record
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public virtual ActionResult PortionWeightCheckPackingRecords(int? page = 1)
        {
            var recordsList = GetPortionWeightCheckPackingRecordDetails().OrderByDescending(x => x.iPortionWeightCheckDetailId);
            var pageNumber = page ?? 1;
            return View(Views.Index, recordsList.ToPagedList(pageNumber, GetPageSize()));
        }
        #endregion

        #region Return Portion Weight Check Packing Record Create New View
        /// <summary>
        /// Return Portion Weight Check Packing Record Create New View
        /// </summary>
        /// <returns></returns>
        public virtual ActionResult CreatePortionWeightCheckPackingRecord()
        {
            return PartialView(Views.Create);
        }
        #endregion

        #region Return Portion Weight Check Packing Record Edit View
        /// <summary>
        /// Return Portion Weight Check Packing Record Edit View
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize(Roles = "Admin,Packing Manager")]
        public virtual ActionResult EditPortionWeightCheckPackingRecord(long id)
        {
            // ReSharper disable once SuggestVarOrType_SimpleTypes
            // ReSharper disable once UseObjectOrCollectionInitializer
            PortionWeightRecordWithDetail portionWeightWithDetailRec = new PortionWeightRecordWithDetail();
            portionWeightWithDetailRec.PortionWeightCheckDetail = Mapper.Map<tPortionWeightCheckDetail, PortionWeightCheckDetail>(_portionWeightsCheckPackingRecordDetailService.GetById(id));
            portionWeightWithDetailRec.PortionWeightCheck = Mapper.Map<tPortionWeightCheck, PortionWeightCheck>(portionWeightWithDetailRec.PortionWeightCheckDetail.tPortionWeightCheck);

            return PartialView(Views.Create, portionWeightWithDetailRec);
        }
        #endregion

        #region  Get All Portion Weight Check Packing Record
        /// <summary>
        /// Get All Portion Weight Check Packing Record
        /// </summary>
        /// <returns></returns>
        // ReSharper disable once ReturnTypeCanBeEnumerable.Local
        private List<PortionWeightCheckDetail> GetPortionWeightCheckPackingRecordDetails()
        {
            var portionWeightCheckPackingRecordDetailsList = _portionWeightsCheckPackingRecordDetailService.GetAll();
            return portionWeightCheckPackingRecordDetailsList.Select(Mapper.Map<tPortionWeightCheckDetail, PortionWeightCheckDetail>).ToList();
        }
        #endregion

        #region Add/Edit
        /// <summary>
        /// Update Portion Weight Check Packing record if found else create new
        /// </summary>
        /// <param name="portionWeightCheckPackingRecordModel"></param>
        /// <returns></returns>
        public virtual JsonResult PostPortionWeightCheckPacking(PortionWeightRecordWithDetail portionWeightCheckPackingRecordModel)
        {
            //Edit
            if ((portionWeightCheckPackingRecordModel.PortionWeightCheck.iPortionWeightCheckId > 0) && (portionWeightCheckPackingRecordModel.PortionWeightCheck.Sys_CreatedBy != null))
            {
                try
                {
                    var savediportionWeightCheckDetailRecord = Mapper.Map<PortionWeightCheckDetail, tPortionWeightCheckDetail>(portionWeightCheckPackingRecordModel.PortionWeightCheckDetail);

                    savediportionWeightCheckDetailRecord.iPortionWeightCheckId = portionWeightCheckPackingRecordModel.PortionWeightCheck.iPortionWeightCheckId;
                    savediportionWeightCheckDetailRecord.Sys_ModifyBy = User.Identity.GetUserId();
                    savediportionWeightCheckDetailRecord.Sys_ModifyDateTime = DateTime.Now;
                    _portionWeightsCheckPackingRecordDetailService.Update(savediportionWeightCheckDetailRecord);
                    AuditScope.CreateAndSave("Editing Portion Weights Check Detail Record", new { User = User.Identity.GetUserId() });
                    return Json(new { status = "Edited", savedRecordID = 0 }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    AuditScope.CreateAndSave("Error Editing Portion Weights Record: ", new { User = User.Identity.GetUserId(), Exception = ex.Message });
                    throw;
                }
            }
            //Add
            else
            {
                try
                {
                    portionWeightCheckPackingRecordModel.PortionWeightCheck.Sys_CreatedBy = User.Identity.GetUserId();
                    portionWeightCheckPackingRecordModel.PortionWeightCheck.Sys_CreatedDateTime = DateTime.Now;
                    // ReSharper disable once InconsistentNaming
                    int savedRecordID;
                    try
                    {
                        savedRecordID = _portionWeightsCheckPackingRecordService.CreateRecord(Mapper.Map<PortionWeightCheck, tPortionWeightCheck>(portionWeightCheckPackingRecordModel.PortionWeightCheck), Mapper.Map<PortionWeightCheckDetail, tPortionWeightCheckDetail>(portionWeightCheckPackingRecordModel.PortionWeightCheckDetail)).iPortionWeightCheckId;
                    }
                    catch (Exception ex)
                    {
                        AuditScope.CreateAndSave("Error Adding Portion Weights Record: ", new { User = User.Identity.GetUserId(), Exception = ex.Message });
                        throw;
                    }

                    AuditScope.CreateAndSave("Adding Portion Weights Check Detail Record", new { User = User.Identity.GetUserId() });

                    return Json(new { status = "Success", savedRecordID }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    AuditScope.CreateAndSave("Error Adding Portion Weights Record: ", new { User = User.Identity.GetUserId(), Exception = ex.Message });
                    throw;
                }
            }
        }
        #endregion

        #region Print Record as Report

        /// <summary>
        /// Print Record
        /// </summary>
        /// <param name="pId">Id of the record to print</param>
        /// <returns>RDLC Report View</returns>
        public virtual ActionResult PrintRecord(Int32 pId)
        {
            try
            {
                return View("~/Views/Report/ReportPreview.aspx", new Report { ReportName = PkReportNames.PortionWeightsCheckPackingRecord, Id = pId, Service = _portionWeightsCheckPackingRecordService });
            }
            catch (Exception ex)
            {
                AuditScope.CreateAndSave("Error getting report in Portion Weights Check Packing Record", new { User = User.Identity.GetUserId() });
                // ReSharper disable once PossibleIntendedRethrow
                throw ex;
            }
        }
        #endregion
    }
}