﻿using Audit.Core;
using AutoMapper;
using Microsoft.AspNet.Identity;
using PunjabKitchen.Models;
using PunjabKitchenBLL.Interfaces;
using System;
using System.Linq;
using System.Web.Mvc;
using PagedList;
using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchen.Controllers
{
    [Authorize(Roles = "Admin")]
    public partial class PlanningController : BaseController
    {
        #region Data Members
        readonly IStockService _stockService;
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="stockService"></param>
        public PlanningController(IStockService stockService)
        {
            _stockService = stockService;
        }
        #endregion

        #region Return Stock and Storage List View
        /// <summary>
        /// Return Stock and Storage List View
        /// </summary>
        /// <returns></returns>
        public virtual ActionResult StockAndStorage(int? page)
        {
            var stocks = _stockService.GetAll().OrderByDescending(x => x.iStockId);
            var pageNumber = page ?? 1;
            var stockList = stocks.Select(item => Mapper.Map<tStock, Stock>(item)).ToList();
            return View(Views.StockAndStorage.Index, stockList.ToPagedList(pageNumber, GetPageSize()));
        }
        #endregion

        #region Return Stock and Storage Create New View
        /// <summary>
        /// Return Stock and Storage Create New View
        /// </summary>
        /// <returns></returns>
        public virtual ActionResult CreateStockAndStorage()
        {
            return PartialView(Views.StockAndStorage.Create);
        }
        #endregion

        #region Return Stock and Storage Edit View
        /// <summary>
        /// Return Stock and Storage Edit View
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public virtual ActionResult EditStock(long id)
        {
            // ReSharper disable once SuggestVarOrType_SimpleTypes
            Stock stock = Mapper.Map<tStock, Stock>(_stockService.GetById(id));
            return PartialView(Views.StockAndStorage.Create, stock);
        }
        #endregion

        #region Add/Edit
        /// <summary>
        /// Update record if found else create new
        /// </summary>
        /// <param name="stockModel"></param>
        /// <returns></returns>
        public virtual ActionResult Post(Stock stockModel)
        {
            try
            {
                //Edit
                if (stockModel.iStockId > 0)
                {
                    var savedStock = _stockService.GetById(stockModel.iStockId);
                    savedStock.vStorageLocation = stockModel.vStorageLocation;
                    savedStock.vStoragePoint = stockModel.vStoragePoint;
                    savedStock.iInspectedQuantity = stockModel.iInspectedQuantity;
                    savedStock.vComments = stockModel.vComments;
                    savedStock.Sys_ModifyBy = User.Identity.GetUserId();
                    savedStock.bQuantityShiftCheck = stockModel.bQuantityShiftCheck;
                    savedStock.dLastChecked = DateTime.Now;

                    // ReSharper disable once PossibleInvalidOperationException
                    if (stockModel.bQuantityShiftCheck.Value)
                    {
                        savedStock.iQuantity = savedStock.iInspectedQuantity;
                    }

                    savedStock.Sys_ModifyDateTime = DateTime.Now;
                    _stockService.Update(savedStock);
                    AuditScope.CreateAndSave("Editing Stock", new { User = User.Identity.GetUserId() });
                }
                //Add
                else
                {
                    stockModel.Sys_CreatedBy = User.Identity.GetUserId();
                    stockModel.Sys_CreatedDateTime = DateTime.Now;

                    // ReSharper disable once PossibleInvalidOperationException
                    if (stockModel.bQuantityShiftCheck.Value)
                    {
                        stockModel.iQuantity = stockModel.iInspectedQuantity;
                    }

                    _stockService.Create(Mapper.Map<Stock, tStock>(stockModel));
                    AuditScope.CreateAndSave("Saving Stock", new { User = User.Identity.GetUserId() });
                }
                return RedirectToAction(MVC.Planning.StockAndStorage());
            }
            catch (Exception ex)
            {
                AuditScope.CreateAndSave("Error Saving Stock Record in PlanningController: ", new { User = User.Identity.GetUserId(), Exception = ex.Message });
                throw;
            }
        }
        #endregion

        #region Validate Stock Location and Stock Point
        /// <summary>
        /// Validate Stock Location and Stock Point
        /// </summary>
        /// <param name="stock"></param>
        /// <returns></returns>
        public virtual JsonResult ValidateStockPoint(Stock stock)
        {
            if (_stockService.validateStockPoint(Mapper.Map<Stock, tStock>(stock)))
            {
                return Json(new { result = true }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { result = false }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Print Record as Report

        /// <summary>
        /// Print Record
        /// </summary>
        /// <param name="pId">Id of the record to print</param>
        /// <returns>RDLC Report View</returns>
        public virtual ActionResult PrintRecord(Int32 pId)
        {
            try
            {
                return View("~/Views/Report/ReportPreview.aspx", new Report { ReportName = PkReportNames.StockAndStorageRecord, Id = pId, Service = _stockService });
            }
            catch (Exception ex)
            {
                AuditScope.CreateAndSave("Error getting report in Stock Planning", new { User = User.Identity.GetUserId() });
                // ReSharper disable once PossibleIntendedRethrow
                throw ex;
            }
        }
        #endregion
    }
}