﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Audit.Core;
using AutoMapper;
using Microsoft.AspNet.Identity;
using PunjabKitchen.Models;
using PunjabKitchenBLL.Interfaces;
using PunjabKitchenEntities.EntityModel;
using PagedList;

namespace PunjabKitchen.Controllers
{
    [Authorize(Roles = "Admin,Cooking Manager,Cooking Staff")]
    public partial class MeatMincingRecordController : BaseController
    {
        #region Data Members
        readonly IMeatMincingRecordService _meatMincingRecordService;
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="meatMincingRecordService"></param>
        public MeatMincingRecordController(IMeatMincingRecordService meatMincingRecordService)
        {
            _meatMincingRecordService = meatMincingRecordService;
        }
        #endregion

        #region Get Meat Mincing Records List
        /// <summary>
        /// Get Meat Mincing Records List
        /// </summary>
        /// <param name="page"></param>
        [HttpGet]
        public virtual ActionResult Index(int? page = 1)
        {
            var recordsList = GetMeatMincingRecords().OrderByDescending(x => x.iMeatMincingRecordId);
            var pageNumber = page ?? 1;
            return View(Views.MeatMincingRecord, recordsList.ToPagedList(pageNumber, GetPageSize()));
        }
        #endregion

        #region Create New Meat Mincing Record
        /// <summary>
        /// Create New Meat Mincing Record
        /// </summary>
        /// <returns></returns>
        public virtual ActionResult Create()
        {
            return PartialView(Views.CreateMeatMincingRecord, new MeatMincingRecord());
        }
        #endregion

        #region Edit Meat Mincing Record
        /// <summary>
        /// Edit Meat Mincing Record
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize(Roles = "Admin,Cooking Manager")]
        public virtual ActionResult Edit(long id)
        {
            var meatMincingRecord = Mapper.Map<tMeatMincingRecord, MeatMincingRecord>(_meatMincingRecordService.GetById(id));
            return PartialView(Views.CreateMeatMincingRecord, meatMincingRecord);
        }
        #endregion

        #region Add/Edit Meat Mincing Record

        /// <summary>
        /// Add/Edit Meat Mincing Record
        /// </summary>
        /// <param name="meatMincingRecordModel"></param>
        /// <returns></returns>
        public virtual ActionResult Post(MeatMincingRecord meatMincingRecordModel)
        {
            try
            {
                //Edit
                if (meatMincingRecordModel.iMeatMincingRecordId > 0)
                {
                    var meatMincingRecModified = Mapper.Map<MeatMincingRecord, tMeatMincingRecord>(meatMincingRecordModel);
                    meatMincingRecModified.Sys_ModifyBy = User.Identity.GetUserId();
                    meatMincingRecModified.Sys_ModifyDateTime = DateTime.Now;
                    meatMincingRecModified.vPreUseCleanlinessCheckSign = meatMincingRecordModel.vPreUseCleanlinessCheckSign != "true" ? "unchecked" : "checked";
                    _meatMincingRecordService.Update(meatMincingRecModified);
                    AuditScope.CreateAndSave("Editing Meat Mincing Record", new { User = User.Identity.GetUserId() });
                }
                //Add
                else
                {
                    meatMincingRecordModel.vPreUseCleanlinessCheckSign = meatMincingRecordModel.vPreUseCleanlinessCheckSign != "true" ? "unchecked" : "checked";
                    meatMincingRecordModel.Sys_CreatedBy = User.Identity.GetUserId();
                    meatMincingRecordModel.Sys_CreatedDateTime = DateTime.Now;
                    _meatMincingRecordService.Create(Mapper.Map<MeatMincingRecord, tMeatMincingRecord>(meatMincingRecordModel));
                    AuditScope.CreateAndSave("Saving Meat Mincing Record", new { User = User.Identity.GetUserId() });
                }
                return RedirectToAction(MVC.MeatMincingRecord.Index());
            }
            catch (Exception ex)
            {
                AuditScope.CreateAndSave("Error saving Meat Mincing Record ", new { User = User.Identity.GetUserId(), Exception = ex.Message });
                return RedirectToAction(MVC.Home.Index());
            }
        }
        #endregion

        #region Private: Get Meat Mincing Records
        /// <summary>
        /// Get Meat Mincing Records
        /// </summary>
        private List<MeatMincingRecord> GetMeatMincingRecords()
        {
            var meatMincingRecordsList = _meatMincingRecordService.GetAll();
            return meatMincingRecordsList.Select(Mapper.Map<tMeatMincingRecord, MeatMincingRecord>).ToList();
        }
        #endregion

        #region Print Record as Report

        /// <summary>
        /// Print Record
        /// </summary>
        /// <param name="pId">Id of the record to print</param>
        /// <returns>RDLC Report View</returns>
        public virtual ActionResult PrintRecord(Int32 pId)
        {
            try
            {
                return View("~/Views/Report/ReportPreview.aspx", new Report { ReportName = PkReportNames.MeatMincingRecord, Id = pId, Service = _meatMincingRecordService });
            }
            catch (Exception ex)
            {
                AuditScope.CreateAndSave("Error getting report of Meat Mincing Record", new { User = User.Identity.GetUserId() });
                // ReSharper disable once PossibleIntendedRethrow
                throw ex;
            }
        }
        #endregion
    }
}