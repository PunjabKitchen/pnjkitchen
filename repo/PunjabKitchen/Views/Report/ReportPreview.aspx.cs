﻿using Microsoft.Reporting.WebForms;
using PunjabKitchen.Bl;
using PunjabKitchen.Models;
using PunjabKitchenBLL.Interfaces;
using System;

namespace PunjabKitchen.Views.Report
{
    public partial class ReportPreview : System.Web.Mvc.ViewPage<dynamic>
    {
        private Models.Report _reportParams;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Model is Models.Report)
                {
                    _reportParams = (Model as Models.Report);

                    if (_reportParams != null)
                    {
                        #region GoodsIn

                        if (_reportParams.ReportName == PkReportNames.GoodsIn)
                            ShowGoodsInReport(_reportParams);
                        else if (_reportParams.ReportName == PkReportNames.StockAndStorageRecord)
                            ShowStockAndStorageRecordReport(_reportParams);
                        else if (_reportParams.ReportName == PkReportNames.StockCount)
                            ShowStockCountReport(_reportParams);
                        else if (_reportParams.ReportName == PkReportNames.DeliveryRecord)
                            DeliveryRecordReport(_reportParams);

                        #endregion

                        #region Cooking

                        else if (_reportParams.ReportName == PkReportNames.CookingPlan)
                            ShowCookingPlanReport(_reportParams);
                        else if (_reportParams.ReportName == PkReportNames.CookingPlanRecord)
                            ShowCookingPlanRecordReport(_reportParams);
                        else if (_reportParams.ReportName == PkReportNames.SlicingRecord)
                            ShowCookingSlicingRecordReport(_reportParams);
                        else if (_reportParams.ReportName == PkReportNames.RiceCoolingTempRecord)
                            ShowRiceCoolingTempRecordReport(_reportParams);
                        else if (_reportParams.ReportName == PkReportNames.MeatMincingRecord)
                            ShowMeatMincingRecordReport(_reportParams);
                        else if (_reportParams.ReportName == PkReportNames.BatchCoolingTempRecord)
                            ShowBatchCoolingTempRecordReport(_reportParams);
                        else if (_reportParams.ReportName == PkReportNames.FreezerRecord)
                            ShowFreezerRecordReport(_reportParams);

                        #endregion

                        #region Packing

                        else if (_reportParams.ReportName == PkReportNames.PackingPlan)
                            ShowPackingPlanReport(_reportParams);
                        else if (_reportParams.ReportName == PkReportNames.PackingPlanRecord)
                            ShowPackingPlanRecordReport(_reportParams);
                        else if (_reportParams.ReportName == PkReportNames.Boxing)
                            ShowBoxingReport(_reportParams);
                        else if (_reportParams.ReportName == PkReportNames.BoxingRecord)
                            ShowBoxingRecordReport(_reportParams);
                        else if (_reportParams.ReportName == PkReportNames.MixedCaseBoxingRecord)
                            ShowMixedCaseBoxingRecordReport(_reportParams);

                        else if (_reportParams.ReportName == PkReportNames.ColdDessertPackingRecord)
                            ShowColdDessertPackingRecordReport(_reportParams);
                        else if (_reportParams.ReportName == PkReportNames.HotPackingRecord)
                            ShowHotPackingRecordReport(_reportParams);
                        else if (_reportParams.ReportName == PkReportNames.ShapesControlPackingRecord)
                            ShowShapesControlPackingRecordReport(_reportParams);
                        else if (_reportParams.ReportName == PkReportNames.BatchChangeCheckPureeRecord)
                            ShowBatchChangeCheckPureeRecordReport(_reportParams);
                        else if (_reportParams.ReportName == PkReportNames.MetalDetectionAndPackWeightRecord)
                            ShowMetalDetectionAndPackWeightRecordReport(_reportParams);
                        else if (_reportParams.ReportName == PkReportNames.FerrousDetectionAndPackWeightRecord)
                            ShowFerrousDetectionAndPackWeightRecordReport(_reportParams);
                        else if (_reportParams.ReportName == PkReportNames.SpotWeightAndMetalCheckRecord)
                            ShowSpotWeightAndMetalCheckRecordReport(_reportParams);
                        else if (_reportParams.ReportName == PkReportNames.PortionWeightsCheckPackingRecord)
                            ShowPortionWeightsCheckPackingRecordReport(_reportParams);
                        else if (_reportParams.ReportName == PkReportNames.PureeShapeStarchRecord)
                            ShowPureeShapeStarchRecordReport(_reportParams);
                        else if (_reportParams.ReportName == PkReportNames.PackingRecord)
                            ShowPackingRecordReport(_reportParams);
                        else if (_reportParams.ReportName == PkReportNames.PackingRoomRecord)
                            ShowPackingRoomRecordReport(_reportParams);
                        else if (_reportParams.ReportName == PkReportNames.VegetarianPackingRecord)
                            ShowVegetarianPackingRecordReport(_reportParams);
                        else if (_reportParams.ReportName == PkReportNames.PureeMealBatchAssemblyRecord)
                            ShowPureeMealBatchAssemblyRecordReport(_reportParams);


                        #endregion

                        #region GoodsOut

                        else if (_reportParams.ReportName == PkReportNames.GoodsOut)
                            ShowGoodsOutReport(_reportParams);
                        else if (_reportParams.ReportName == PkReportNames.GoodsOutPlanRecord)
                            ShowGoodsOutPlanRecordReport(_reportParams);
                        else if (_reportParams.ReportName == PkReportNames.GoodsOutPickList)
                            ShowGoodsOutPickListReport(_reportParams);

                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region GoodsIn

        private void ShowGoodsInReport(Models.Report pParam)
        {
            try
            {
                rptViewer.LocalReport.ReportPath = @"Reports\GoodsInCount.rdlc";
                rptViewer.LocalReport.DataSources.Clear();
                rptViewer.LocalReport.DataSources.Add(new ReportDataSource("GoodsInDataSet", PKReporting.GetAllGoodsInCount(pParam.FromDate, pParam.ToDate)));
                rptViewer.LocalReport.Refresh();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void ShowStockAndStorageRecordReport(Models.Report pParam)
        {
            try
            {
                rptViewer.LocalReport.ReportPath = @"Reports\Templates\GoodsIn\StockAndStorage.rdlc";
                rptViewer.LocalReport.DataSources.Clear();
                rptViewer.LocalReport.DataSources.Add(new ReportDataSource("dsReport", Reports.DataSets.GoodsIn.dsStockAndStorageReport.GetStockAndStorage(pParam.Service as IStockService, pParam.Id)));
                rptViewer.LocalReport.Refresh();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void ShowStockCountReport(Models.Report pParam)
        {
            try
            {
                rptViewer.LocalReport.ReportPath = @"Reports\StockCount.rdlc";
                rptViewer.LocalReport.DataSources.Clear();
                rptViewer.LocalReport.DataSources.Add(new ReportDataSource("DataSet1", PKReporting.GetAllStockCount(pParam.FromDate, pParam.ToDate)));
                rptViewer.LocalReport.Refresh();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void DeliveryRecordReport(Models.Report pParam)
        {
            try
            {
                rptViewer.LocalReport.ReportPath = @"Reports\Templates\GoodsIn\Delivery.rdlc";
                rptViewer.LocalReport.DataSources.Clear();
                rptViewer.LocalReport.DataSources.Add(new ReportDataSource("dsReport", Reports.DataSets.GoodsIn.dsDeliveryReport.GetDelivery(pParam.Service as IGoodsInService, pParam.Id)));
                rptViewer.LocalReport.DataSources.Add(new ReportDataSource("dsReportItems", Reports.DataSets.GoodsIn.dsDeliveryReport.GetDeliveryItems(pParam.Service as IGoodsInService, pParam.Id)));
                rptViewer.LocalReport.Refresh();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        #endregion

        #region Cooking

        private void ShowCookingPlanRecordReport(Models.Report pParam)
        {
            try
            {
                rptViewer.LocalReport.ReportPath = @"Reports\Templates\Cooking\CookingPlan.rdlc";
                rptViewer.LocalReport.DataSources.Clear();
                rptViewer.LocalReport.DataSources.Add(new ReportDataSource("dsReport", Reports.DataSets.Cooking.dsCookingPlanReport.GetCookingPlan(pParam.Service as ICookingPlanService, pParam.Id)));
                rptViewer.LocalReport.Refresh();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void ShowCookingPlanReport(Models.Report pParam)
        {
            try
            {
                rptViewer.LocalReport.ReportPath = @"Reports\CookingPlanCount.rdlc";
                rptViewer.LocalReport.DataSources.Clear();
                rptViewer.LocalReport.DataSources.Add(new ReportDataSource("CookingPlanDataSet",
                    PKReporting.GetAllCookingPlanCount(pParam.FromDate, pParam.ToDate)));
                rptViewer.ZoomPercent = 80;
                rptViewer.LocalReport.Refresh();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void ShowCookingSlicingRecordReport(Models.Report pParam)
        {
            try
            {
                rptViewer.LocalReport.ReportPath = @"Reports\Templates\Cooking\SlicingRecord.rdlc";
                rptViewer.LocalReport.DataSources.Clear();
                rptViewer.LocalReport.DataSources.Add(new ReportDataSource("dsReport", Reports.DataSets.Cooking.dsSlicingRecordReport.GetSlicingRecord(pParam.Service as ISlicingRecordService, pParam.Id)));
                rptViewer.LocalReport.Refresh();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void ShowRiceCoolingTempRecordReport(Models.Report pParam)
        {
            try
            {
                rptViewer.LocalReport.ReportPath = @"Reports\Templates\Cooking\RiceCoolingTempRecord.rdlc";
                rptViewer.LocalReport.DataSources.Clear();
                rptViewer.LocalReport.DataSources.Add(new ReportDataSource("dsReport", Reports.DataSets.Cooking.dsRiceCoolingTempRecordReport.GetRiceCoolingTempRecord(pParam.Service as IRiceCoolingTempRecordService, pParam.Id)));
                rptViewer.LocalReport.Refresh();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void ShowMeatMincingRecordReport(Models.Report pParam)
        {
            try
            {
                rptViewer.LocalReport.ReportPath = @"Reports\Templates\Cooking\MeatMincingRecord.rdlc";
                rptViewer.LocalReport.DataSources.Clear();
                rptViewer.LocalReport.DataSources.Add(new ReportDataSource("dsReport", Reports.DataSets.Cooking.dsMeatMincingRecordReport.GetMeatMincingRecord(pParam.Service as IMeatMincingRecordService, pParam.Id)));
                rptViewer.LocalReport.Refresh();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void ShowBatchCoolingTempRecordReport(Models.Report pParam)
        {
            try
            {
                rptViewer.LocalReport.ReportPath = @"Reports\Templates\Cooking\BatchCoolingTempRecord.rdlc";
                rptViewer.LocalReport.DataSources.Clear();
                rptViewer.LocalReport.DataSources.Add(new ReportDataSource("dsReport", Reports.DataSets.Cooking.dsBatchCoolingTempRecordReport.GetBatchCoolingTempRecord(pParam.Service as IBatchCoolingTempRecrdService, pParam.Id)));
                rptViewer.LocalReport.Refresh();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void ShowFreezerRecordReport(Models.Report pParam)
        {
            try
            {
                rptViewer.LocalReport.ReportPath = @"Reports\Templates\Cooking\FreezerRecord.rdlc";
                rptViewer.LocalReport.DataSources.Clear();
                rptViewer.LocalReport.DataSources.Add(new ReportDataSource("dsReport", Reports.DataSets.Cooking.dsFreezerRecordReport.GetFreezerRecord(pParam.Service as IFreezerRecordService, pParam.Id)));
                rptViewer.LocalReport.Refresh();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Packing

        private void ShowPackingPlanRecordReport(Models.Report pParam)
        {
            try
            {
                rptViewer.LocalReport.ReportPath = @"Reports\Templates\Packing\PackingPlan.rdlc";
                rptViewer.LocalReport.DataSources.Clear();
                rptViewer.LocalReport.DataSources.Add(new ReportDataSource("dsReport", Reports.DataSets.Packing.dsPackingPlanReport.GetPackingPlan(pParam.Service as IPackingPlanService, pParam.Id)));
                rptViewer.LocalReport.Refresh();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void ShowPackingPlanReport(Models.Report pParam)
        {
            try
            {
                rptViewer.LocalReport.ReportPath = @"Reports\PackingCount.rdlc";
                rptViewer.LocalReport.DataSources.Clear();
                rptViewer.LocalReport.DataSources.Add(new ReportDataSource("PackingDataSet", PKReporting.GetAllPackingCount(pParam.FromDate, pParam.ToDate)));
                rptViewer.ZoomPercent = 80;
                rptViewer.LocalReport.Refresh();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void ShowBoxingReport(Models.Report pParam)
        {
            try
            {
                rptViewer.LocalReport.ReportPath = @"Reports\BoxingCount.rdlc";
                rptViewer.LocalReport.DataSources.Clear();
                rptViewer.LocalReport.DataSources.Add(new ReportDataSource("BoxingDataSet", PKReporting.GetAllBoxingCount(pParam.FromDate, pParam.ToDate)));
                rptViewer.ZoomPercent = 80;
                rptViewer.LocalReport.Refresh();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void ShowBoxingRecordReport(Models.Report pParam)
        {
            try
            {
                rptViewer.LocalReport.ReportPath = @"Reports\Templates\Packing\BoxingRecord.rdlc";
                rptViewer.LocalReport.DataSources.Clear();
                rptViewer.LocalReport.DataSources.Add(new ReportDataSource("dsReport", Reports.DataSets.Packing.dsBoxingRecordReport.GetBoxingRecord(pParam.Service as IBoxingRecordService, pParam.Id)));
                rptViewer.LocalReport.Refresh();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void ShowMixedCaseBoxingRecordReport(Models.Report pParam)
        {
            try
            {
                rptViewer.LocalReport.ReportPath = @"Reports\Templates\Packing\MixedCaseBoxingRecord.rdlc";
                rptViewer.LocalReport.DataSources.Clear();
                rptViewer.LocalReport.DataSources.Add(new ReportDataSource("dsReport", Reports.DataSets.Packing.dsMixedCaseBoxingRecordReport.GetMixedCaseBoxingRecord(pParam.Service as IMixedCaseBoxingRecordService, pParam.Id)));
                rptViewer.LocalReport.Refresh();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void ShowColdDessertPackingRecordReport(Models.Report pParam)
        {
            try
            {
                rptViewer.LocalReport.ReportPath = @"Reports\Templates\Packing\ColdDessertPackingRecord.rdlc";
                rptViewer.LocalReport.DataSources.Clear();
                rptViewer.LocalReport.DataSources.Add(new ReportDataSource("dsReport", Reports.DataSets.Packing.dsColdDessertPackingRecordReport.GetColdDessertPackingRecord(pParam.Service as IColdDessertPackingRecordService, pParam.Id)));
                rptViewer.LocalReport.Refresh();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void ShowHotPackingRecordReport(Models.Report pParam)
        {
            try
            {
                rptViewer.LocalReport.ReportPath = @"Reports\Templates\Packing\HotPackingRecord.rdlc";
                rptViewer.LocalReport.DataSources.Clear();
                rptViewer.LocalReport.DataSources.Add(new ReportDataSource("dsReport", Reports.DataSets.Packing.dsHotPackingRecordReport.GetHotPackingRecord(pParam.Service as IHotPackingRercordService, pParam.Id)));
                rptViewer.LocalReport.Refresh();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void ShowShapesControlPackingRecordReport(Models.Report pParam)
        {
            try
            {
                rptViewer.LocalReport.ReportPath = @"Reports\Templates\Packing\ShapesControlPackingRecord.rdlc";
                rptViewer.LocalReport.DataSources.Clear();
                rptViewer.LocalReport.DataSources.Add(new ReportDataSource("dsReport", Reports.DataSets.Packing.dsShapesControlPackingRecordReport.GetShapesControlPackingRecord(pParam.Service as IShapesControlRecordService, pParam.Id)));
                rptViewer.LocalReport.Refresh();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void ShowBatchChangeCheckPureeRecordReport(Models.Report pParam)
        {
            try
            {
                rptViewer.LocalReport.ReportPath = @"Reports\Templates\Packing\BatchChangeCheckPureeRecord.rdlc";
                rptViewer.LocalReport.DataSources.Clear();
                rptViewer.LocalReport.DataSources.Add(new ReportDataSource("dsReport", Reports.DataSets.Packing.dsBatchChangeCheckPureeRecordReport.GetBatchChangeCheckPureeRecord(pParam.Service as IBatchChangePureeRecordService, pParam.Id)));
                rptViewer.LocalReport.Refresh();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void ShowMetalDetectionAndPackWeightRecordReport(Models.Report pParam)
        {
            try
            {
                rptViewer.LocalReport.ReportPath = @"Reports\Templates\Packing\MetalDetectionAndPackWeightRecord.rdlc";
                rptViewer.LocalReport.DataSources.Clear();
                rptViewer.LocalReport.DataSources.Add(new ReportDataSource("dsReport", Reports.DataSets.Packing.dsMetalDetectionAndPackWeightRecordReport.GetMetalDetectionAndPackWeightRecord(pParam.Service as IFMDetectionAndPackWeightRecordService, pParam.Id)));
                rptViewer.LocalReport.Refresh();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void ShowFerrousDetectionAndPackWeightRecordReport(Models.Report pParam)
        {
            try
            {
                rptViewer.LocalReport.ReportPath = @"Reports\Templates\Packing\FerrousDetectionAndPackWeightRecord.rdlc";
                rptViewer.LocalReport.DataSources.Clear();
                rptViewer.LocalReport.DataSources.Add(new ReportDataSource("dsReport", Reports.DataSets.Packing.dsFerrousDetectionAndPackWeightRecordReport.GetFerrousDetectionAndPackWeightRecord(pParam.Service as IFMDetectionAndPackWeightRecordService, pParam.Id)));
                rptViewer.LocalReport.Refresh();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void ShowSpotWeightAndMetalCheckRecordReport(Models.Report pParam)
        {
            try
            {
                rptViewer.LocalReport.ReportPath = @"Reports\Templates\Packing\SpotWeightAndMetalCheckRecord.rdlc";
                rptViewer.LocalReport.DataSources.Clear();
                rptViewer.LocalReport.DataSources.Add(new ReportDataSource("dsReport", Reports.DataSets.Packing.dsSpotWeightAndMetalCheckRecordReport.GetSpotWeightAndMetalCheckRecord(pParam.Service as IFMDetectionAndPackWeightRecordService, pParam.Id)));
                rptViewer.LocalReport.Refresh();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void ShowPortionWeightsCheckPackingRecordReport(Models.Report pParam)
        {
            try
            {
                rptViewer.LocalReport.ReportPath = @"Reports\Templates\Packing\PortionWeightsCheckPackingRecord.rdlc";
                rptViewer.LocalReport.DataSources.Clear();
                rptViewer.LocalReport.DataSources.Add(new ReportDataSource("dsReport", Reports.DataSets.Packing.dsPortionWeightsCheckPackingRecordReport.GetPortionWeightsCheckPackingRecord(pParam.Service as IPortionWeightsCheckRecordService, pParam.Id)));
                rptViewer.LocalReport.Refresh();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void ShowPureeShapeStarchRecordReport(Models.Report pParam)
        {
            try
            {
                rptViewer.LocalReport.ReportPath = @"Reports\Templates\Packing\PureeShapeStarchRecord.rdlc";
                rptViewer.LocalReport.DataSources.Clear();
                rptViewer.LocalReport.DataSources.Add(new ReportDataSource("dsReport", Reports.DataSets.Packing.dsPureeShapeStarchRecordReport.GetPureeShapeStarchRecord(pParam.Service as IPureeShapesStarchRecordService, pParam.Id)));
                rptViewer.LocalReport.Refresh();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void ShowPackingRecordReport(Models.Report pParam)
        {
            try
            {
                rptViewer.LocalReport.ReportPath = @"Reports\Templates\Packing\PackingRecord.rdlc";
                rptViewer.LocalReport.DataSources.Clear();
                rptViewer.LocalReport.DataSources.Add(new ReportDataSource("dsReport", Reports.DataSets.Packing.dsPackingRecordReport.GetPackingRecord(pParam.Service as IPackingRecordService, pParam.Id)));
                rptViewer.LocalReport.Refresh();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void ShowPackingRoomRecordReport(Models.Report pParam)
        {
            try
            {
                rptViewer.LocalReport.ReportPath = @"Reports\Templates\Packing\PackingRoomRecord.rdlc";
                rptViewer.LocalReport.DataSources.Clear();
                rptViewer.LocalReport.DataSources.Add(new ReportDataSource("dsReport", Reports.DataSets.Packing.dsPackingRoomRecordReport.GetPackingRoomRecord(pParam.Service as IPackingRecord2Service, pParam.Id)));
                rptViewer.LocalReport.Refresh();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void ShowVegetarianPackingRecordReport(Models.Report pParam)
        {
            try
            {
                rptViewer.LocalReport.ReportPath = @"Reports\Templates\Packing\VegetarianPackingRecord.rdlc";
                rptViewer.LocalReport.DataSources.Clear();
                rptViewer.LocalReport.DataSources.Add(new ReportDataSource("dsReport", Reports.DataSets.Packing.dsVegetarianPackingRecordReport.GetVegetarianPackingRecord(pParam.Service as IVegetarianPackingRecordService, pParam.Id)));
                rptViewer.LocalReport.Refresh();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void ShowPureeMealBatchAssemblyRecordReport(Models.Report pParam)
        {
            try
            {
                rptViewer.LocalReport.ReportPath = @"Reports\Templates\Packing\PureeMealBatchAssemblyRecord.rdlc";
                rptViewer.LocalReport.DataSources.Clear();
                rptViewer.LocalReport.DataSources.Add(new ReportDataSource("dsReportMain", Reports.DataSets.Packing.dsPureeMealBatchAssemblyRecordReport.GetPureeMealBatchAssemblyRecord(pParam.Service as IPureeMealBatchAssemblyService, pParam.Id)));
                rptViewer.LocalReport.DataSources.Add(new ReportDataSource("dsReportMeal", Reports.DataSets.Packing.dsPureeMealBatchAssemblyRecordReport.GetPureeMealBatchAssemblyMealRecord(pParam.Service as IPureeMealBatchAssemblyService, pParam.Id)));
                rptViewer.LocalReport.DataSources.Add(new ReportDataSource("dsReportSauce", Reports.DataSets.Packing.dsPureeMealBatchAssemblyRecordReport.GetPureeMealBatchAssemblySauceRecord(pParam.Service as IPureeMealBatchAssemblyService, pParam.Id)));
                rptViewer.LocalReport.DataSources.Add(new ReportDataSource("dsReportMash", Reports.DataSets.Packing.dsPureeMealBatchAssemblyRecordReport.GetPureeMealBatchAssemblyMashRecord(pParam.Service as IPureeMealBatchAssemblyService, pParam.Id)));
                rptViewer.LocalReport.DataSources.Add(new ReportDataSource("dsReportBlastFreezer", Reports.DataSets.Packing.dsPureeMealBatchAssemblyRecordReport.GetPureeMealBatchAssemblyBlastFreezerRecord(pParam.Service as IPureeMealBatchAssemblyService, pParam.Id)));
                rptViewer.LocalReport.Refresh();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region GoodsOut

        private void ShowGoodsOutReport(Models.Report pParam)
        {
            try
            {
                rptViewer.LocalReport.ReportPath = @"Reports\GoodsOutCount.rdlc";
                rptViewer.LocalReport.DataSources.Clear();
                rptViewer.LocalReport.DataSources.Add(new ReportDataSource("GoodsOutDataSet", PKReporting.GetAllGoodsOutCount(pParam.FromDate, pParam.ToDate)));
                rptViewer.LocalReport.Refresh();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void ShowGoodsOutPlanRecordReport(Models.Report pParam)
        {
            try
            {
                rptViewer.LocalReport.ReportPath = @"Reports\Templates\GoodsOut\GoodsOutPlan.rdlc";
                rptViewer.LocalReport.DataSources.Clear();
                rptViewer.LocalReport.DataSources.Add(new ReportDataSource("dsReport", Reports.DataSets.GoodsOut.dsGoodsOutPlanReport.GetGoodsOutPlan(pParam.Service as IGoodsOutPlanService, pParam.Id)));
                rptViewer.LocalReport.Refresh();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void ShowGoodsOutPickListReport(Models.Report pParam)
        {
            try
            {
                rptViewer.LocalReport.ReportPath = @"Reports\Templates\GoodsOut\GoodsOutPickList.rdlc";
                rptViewer.LocalReport.DataSources.Clear();
                rptViewer.LocalReport.DataSources.Add(new ReportDataSource("dsReport", Reports.DataSets.GoodsOut.dsGoodsOutPickListReport.GetGoodsOutPickList(pParam.Service as IGoodsOutPicklistService, pParam.Id)));
                rptViewer.LocalReport.Refresh();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion
    }
}