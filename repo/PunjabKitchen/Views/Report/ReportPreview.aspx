﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ReportPreview.aspx.cs" Inherits="PunjabKitchen.Views.Report.ReportPreview" %>
<%@ Register TagPrefix="rsweb" Namespace="Microsoft.Reporting.WebForms" Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Print</title>
    <link rel="icon" href="../../Images/favicon.png" type="image/x-icon" />
    <link rel="shortcut icon" href="../../Images/favicon.png" type="image/x-icon" />
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <div>
            <table style="width: 100%; height: 100%" id="tblReport">
                <tr>
                    <td>
                        <asp:Panel runat="server" ID="pnl_ErrorPanel" Visible="false">
                            <asp:Label runat="server" ID="lbl_ErrorMessage" ForeColor="Red" Font-Names="Verdana"
                                Font-Size="9pt"></asp:Label>
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td>
                        <rsweb:ReportViewer ID="rptViewer" runat="server" Font-Names="Verdana" Font-Size="8pt"
                            WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt" AsyncRendering="false"
                            Width="100%" Height="100%" InteractiveDeviceInfos="(Collection)" SizeToReportContent="True">
                        </rsweb:ReportViewer>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
