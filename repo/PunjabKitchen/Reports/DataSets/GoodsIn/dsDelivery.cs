﻿using PunjabKitchenBLL.Interfaces;
using PunjabKitchenEntities.EntityModel;
using System;
using System.Collections.Generic;
using System.Linq;

namespace PunjabKitchen.Reports.DataSets.GoodsIn
{
    public class dsDeliveryReport
    {
        public class dsDelivery
        {
            public string SupplierName { get; set; }
            public string ProductType { get; set; }
            public string VehicleCheck { get; set; }
            public string InTakeCheck { get; set; }
            public string QACheck { get; set; }
            public string InternalStorageArea { get; set; }
            public string VehicleInternalCheckComment { get; set; }
            public string RawMaterialStorage { get; set; }
            public string RawMaterialComment { get; set; }
            public string CCP1aStatus { get; set; }
            public int CCP1aTemp { get; set; }
            public string CCP1aComment { get; set; }
        }

        public class dsDeliveryItems
        {
            public string ProductCode { get; set; }
            public string ProductName { get; set; }
            public string Location { get; set; }
            public string LocationPoint { get; set; }
            public int Quantity { get; set; }
            public string Unit { get; set; }
        }

        public static IEnumerable<dsDelivery> GetDelivery(IGoodsInService goodsInService, int pId)
        {
            var data = goodsInService.FindBy(filter => filter.tGoodsInId == pId);

            return
            data.Select(row => new dsDelivery()
            {
                SupplierName = row.vSupplier,
                ProductType = row.tProductType.vName,
                VehicleCheck = Convert.ToBoolean(row.bVehicleChecksAccepted) ? "Yes" : "No",
                InTakeCheck = Convert.ToBoolean(row.bGoodsIntakeFinished) ? "Yes" : "No",
                QACheck = (row.tProducts == null || row.tProducts.Count == 0) ? "No" : !row.tProducts.Any(x => x.bQualityApproved == false) ? "Yes" : "No",
                InternalStorageArea = Convert.ToBoolean(row.bVehicleInternalCheck) ? "Good" : "Bad",
                VehicleInternalCheckComment = row.vVehicleInternalCheckComment,
                RawMaterialStorage = Convert.ToBoolean(row.bRawMaterial) ? "Good" : "Bad",
                RawMaterialComment = row.vRawMaterialComment,
                CCP1aStatus = Convert.ToBoolean(row.ccp1a) ? "Good" : "Bad",
                CCP1aTemp = Convert.ToInt32(row.ccp1a_temp),
                CCP1aComment = row.ccp1a_comment
            });
        }

        public static IEnumerable<dsDeliveryItems> GetDeliveryItems(IGoodsInService goodsInService, int pGoodsInId)
        {
            var data = goodsInService.FindBy(filter => filter.tGoodsInId == pGoodsInId).FirstOrDefault().tProducts;

            return
            data.Select(row => new dsDeliveryItems()
            {
               ProductCode = row.vProductCode,
               ProductName = row.vName,
               Location = row.tStock?.vStorageLocation,
               LocationPoint  = row.tStock?.vStoragePoint,
               Quantity = Convert.ToInt32(row.iQuantity),
               Unit = row.tUnit?.vName
            });
        }
    }
}