﻿using PunjabKitchenBLL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace PunjabKitchen.Reports.DataSets.GoodsIn
{
    public class dsStockAndStorageReport
    {
        public class dsStockAndStorage
        {
            public string Location { get; set; }
            public string LocationPoint { get; set; }
            public int ActualQuantity { get; set; }
            public int InspectedQuantity { get; set; }
            public string Comments { get; set; }
            public bool UseInspectedQty { get; set; }
        }

        public static IEnumerable<dsStockAndStorage> GetStockAndStorage(IStockService stockService, int pId)
        {
            var data = stockService.FindBy(filter => filter.iStockId == pId);

            return
            data.Select(row => new dsStockAndStorage()
            {
                Location = row.vStorageLocation,
                LocationPoint = row.vStoragePoint,
                ActualQuantity = Convert.ToInt32(row.iQuantity),
                InspectedQuantity = Convert.ToInt32(row.iInspectedQuantity),
                Comments = row.vComments,
                UseInspectedQty = Convert.ToBoolean(row.bQuantityShiftCheck)
            });
        }
    }
}