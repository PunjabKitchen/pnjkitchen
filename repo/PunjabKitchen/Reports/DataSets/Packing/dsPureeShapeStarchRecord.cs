﻿using PunjabKitchenBLL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PunjabKitchen.Reports.DataSets.Packing
{
    public class dsPureeShapeStarchRecordReport
    {
        public class dsPureeShapeStarchRecord
        {
            public string BatchCode { get; set; }
            public string Product { get; set; }
            public int Amount { get; set; }
            public double Total { get; set; }
            public int UltraTex2 { get; set; }
            public string UltraTex2BatchNo { get; set; }
            public int Metalose { get; set; }
            public string MetaloseBatchCode { get; set; }
            public int Purity { get; set; }
            public string PurityBatchCode { get; set; }
        }

        public static IEnumerable<dsPureeShapeStarchRecord> GetPureeShapeStarchRecord(IPureeShapesStarchRecordService PureeShapeStarchRecordService, int pId)
        {
            var data = PureeShapeStarchRecordService.FindBy(filter => filter.iPureeShapeStarchRecordId == pId).FirstOrDefault().tPureeShapesStarchRecordDetails;

            return
            data.Select(row => new dsPureeShapeStarchRecord()
            {
                BatchCode = row.tPureeShapesStarchRecord.tCookingPlan?.vBatchNo,
                Product = row.tPureeShapesStarchRecord.vProductName,
                Amount = Convert.ToInt32(row.tPureeShapesStarchRecord.iProductAmount),
                Total = Convert.ToDouble(row.tPureeShapesStarchRecord.vTotal),
                UltraTex2 = Convert.ToInt32(row.tPureeShapesStarchRecord.iUltraTex2),
                UltraTex2BatchNo = row.tPureeShapesStarchRecord.vUltraTex2BatchNo,
                Metalose = Convert.ToInt32(row.tPureeShapesStarchRecord.iMetalose),
                MetaloseBatchCode = row.tPureeShapesStarchRecord.vMetaloseBatchNo,
                Purity = Convert.ToInt32(row.tPureeShapesStarchRecord.iPurityW),
                PurityBatchCode = row.tPureeShapesStarchRecord.vPurityWBatchNo
            });
        }
    }
}