﻿using PunjabKitchenBLL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PunjabKitchen.Reports.DataSets.Packing
{
    public class dsPackingRoomRecordReport
    {
        public class dsPackingRoomRecord
        {
            public string BatchNo { get; set; }
            public string ProductCode { get; set; }
            public string ProductName { get; set; }
            public string LineNo { get; set; }
            public int QuantityMade { get; set; }
            public string Unit { get; set; }
            public DateTime StartTime { get; set; }
            public DateTime FinishTime { get; set; }
            public string Packers { get; set; }
        }

        public static IEnumerable<dsPackingRoomRecord> GetPackingRoomRecord(IPackingRecord2Service PackingRoomRecordService, int pId)
        {
            var data = PackingRoomRecordService.FindBy(filter => filter.iPackingRecordId == pId);

            return
            data.Select(row => new dsPackingRoomRecord()
            {
                BatchNo = row.tCookingPlan?.vBatchNo,
                ProductCode = row.tCookingPlan?.tProduct?.vProductCode,
                ProductName = row.tCookingPlan?.tProduct?.vName,
                LineNo = "Line No. " + row.iLineNo,
                QuantityMade = Convert.ToInt32(row.iQuantityMade),
                Unit = row.tUnit?.vName,
                StartTime = Convert.ToDateTime(row.tStartTime),
                FinishTime = Convert.ToDateTime(row.tFinishTime),
                Packers = row.iPackerUserId.ToString()
            });
        }
    }
}