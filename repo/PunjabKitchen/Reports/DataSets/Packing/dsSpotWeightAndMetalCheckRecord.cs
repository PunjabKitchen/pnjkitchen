﻿using PunjabKitchenBLL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PunjabKitchen.Reports.DataSets.Packing
{
    public class dsSpotWeightAndMetalCheckRecordReport
    {
        public class dsSpotWeightAndMetalCheckRecord
        {
            public string Location { get; set; }
            public string BatchCode { get; set; }
            public string Product { get; set; }
            public int ProductWeight { get; set; }

            public int Pack1Weight { get; set; }
            public int Pack2Weight { get; set; }
            public int Pack3Weight { get; set; }
            public int Pack4Weight { get; set; }
            public int Pack5Weight { get; set; }
            public int Pack6Weight { get; set; }
            public int Pack7Weight { get; set; }
            public int Pack8Weight { get; set; }
            public int Pack9Weight { get; set; }
            public int Pack10Weight { get; set; }

            public string Ferrous18mm { get; set; }
            public string NonFerrous3mm { get; set; }
            public string RejectMechanism { get; set; }
            public string StainlessSteel4mm { get; set; }
        }

        public static IEnumerable<dsSpotWeightAndMetalCheckRecord> GetSpotWeightAndMetalCheckRecord(IFMDetectionAndPackWeightRecordService SpotWeightAndMetalCheckRecordService, int pId)
        {
            var data = SpotWeightAndMetalCheckRecordService.FindBy(filter => filter.iDetectionPackWeightTestId == pId);

            return
            data.Select(row => new dsSpotWeightAndMetalCheckRecord()
            {
                Location = row.vLocation,
                BatchCode = row.vBatchNo,
                Product = row.tCookingPlan?.tRecipe?.vRecipeCode + " - " + row.tCookingPlan?.tRecipe?.vRecipeName,
                ProductWeight = Convert.ToInt32(row.iProductWeight),

                Pack1Weight = Convert.ToInt32(row.iWeightOfPacks1),
                Pack2Weight = Convert.ToInt32(row.iWeightOfPacks2),
                Pack3Weight = Convert.ToInt32(row.iWeightOfPacks3),
                Pack4Weight = Convert.ToInt32(row.iWeightOfPacks4),
                Pack5Weight = Convert.ToInt32(row.iWeightOfPacks5),
                Pack6Weight = Convert.ToInt32(row.iWeightOfPacks6),
                Pack7Weight = Convert.ToInt32(row.iWeightOfPacks7),
                Pack8Weight = Convert.ToInt32(row.iWeightOfPacks8),
                Pack9Weight = Convert.ToInt32(row.iWeightOfPacks9),
                Pack10Weight = Convert.ToInt32(row.iWeightOfPacks10),

                Ferrous18mm = Convert.ToBoolean(row.bFerrous1_8mmPass) ? "Yes" : "No",
                NonFerrous3mm = Convert.ToBoolean(row.bNonFerrous3_0mmPass) ? "Yes" : "No",
                RejectMechanism = Convert.ToBoolean(row.bRejectMechnismWorking) ? "Yes" : "No",
                StainlessSteel4mm = Convert.ToBoolean(row.bStainlessSteel4_0mmPass) ? "Yes" : "No"
            });
        }
    }
}