﻿using PunjabKitchenBLL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PunjabKitchen.Reports.DataSets.Packing
{
    public class dsFerrousDetectionAndPackWeightRecordReport
    {
        public class dsFerrousDetectionAndPackWeightRecord
        {
            public string Location { get; set; }
            public string BatchCode { get; set; }
            public string Product { get; set; }
            public int ProductWeight { get; set; }

            public int Pack1Weight { get; set; }
            public int Pack2Weight { get; set; }
            public int Pack3Weight { get; set; }
            public int Pack4Weight { get; set; }
            public int Pack5Weight { get; set; }
            public int Pack6Weight { get; set; }
            public int Pack7Weight { get; set; }
            public int Pack8Weight { get; set; }
            public int Pack9Weight { get; set; }
            public int Pack10Weight { get; set; }

            public string Ferrous28mm { get; set; }
            public string BeltStopWorking { get; set; }
        }

        public static IEnumerable<dsFerrousDetectionAndPackWeightRecord> GetFerrousDetectionAndPackWeightRecord(IFMDetectionAndPackWeightRecordService FerrousDetectionAndPackWeightRecordService, int pId)
        {
            var data = FerrousDetectionAndPackWeightRecordService.FindBy(filter => filter.iDetectionPackWeightTestId == pId);

            return
            data.Select(row => new dsFerrousDetectionAndPackWeightRecord()
            {
                Location = row.vLocation,
                BatchCode = row.vBatchNo,
                Product = row.tCookingPlan?.tRecipe?.vRecipeCode + " - " + row.tCookingPlan?.tRecipe?.vRecipeName,
                ProductWeight = Convert.ToInt32(row.iProductWeight),

                Pack1Weight = Convert.ToInt32(row.iWeightOfPacks1),
                Pack2Weight = Convert.ToInt32(row.iWeightOfPacks2),
                Pack3Weight = Convert.ToInt32(row.iWeightOfPacks3),
                Pack4Weight = Convert.ToInt32(row.iWeightOfPacks4),
                Pack5Weight = Convert.ToInt32(row.iWeightOfPacks5),
                Pack6Weight = Convert.ToInt32(row.iWeightOfPacks6),
                Pack7Weight = Convert.ToInt32(row.iWeightOfPacks7),
                Pack8Weight = Convert.ToInt32(row.iWeightOfPacks8),
                Pack9Weight = Convert.ToInt32(row.iWeightOfPacks9),
                Pack10Weight = Convert.ToInt32(row.iWeightOfPacks10),

                Ferrous28mm = Convert.ToBoolean(row.bFerrous2_8mmPass) ? "Yes" : "No",
                BeltStopWorking = Convert.ToBoolean(row.bBeltStopWorking) ? "Yes" : "No"
            });
        }
    }
}