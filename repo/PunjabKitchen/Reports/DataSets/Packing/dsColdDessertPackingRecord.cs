﻿using PunjabKitchenBLL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PunjabKitchen.Reports.DataSets.Packing
{
    public class dsColdDessertPackingRecordReport
    {
        public class dsColdDessertPackingRecord
        {
            public string BatchNo { get; set; }
            public string Product { get; set; }
            public DateTime TimeExMixing { get; set; }
            public DateTime TimeOfLastTrayIntoBlastFreezer { get; set; }
            public DateTime TimeTaken { get; set; }
        }

        public static IEnumerable<dsColdDessertPackingRecord> GetColdDessertPackingRecord(IColdDessertPackingRecordService ColdDessertPackingRecordService, int pId)
        {
            var data = ColdDessertPackingRecordService.FindBy(filter => filter.iColdDesertPackingRecordId == pId);

            return
            data.Select(row => new dsColdDessertPackingRecord()
            {
                BatchNo = row.tCookingPlan?.vBatchNo,
                Product = row.tCookingPlan?.tRecipe?.vRecipeCode + " - " + row.tCookingPlan?.tRecipe?.vRecipeName,
                TimeExMixing = Convert.ToDateTime(row.tTimeExMixing),
                TimeOfLastTrayIntoBlastFreezer = Convert.ToDateTime(row.tTimeOfLastTrayInBlastFreezer),
                TimeTaken = Convert.ToDateTime(row.tTimeTaken)
            });
        }
    }
}