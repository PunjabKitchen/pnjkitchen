﻿using PunjabKitchenBLL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PunjabKitchen.Reports.DataSets.Packing
{
    public class dsVegetarianPackingRecordReport
    {
        public class dsVegetarianPackingRecord
        {
            public string BatchCode { get; set; }
            public string Product { get; set; }
            public int Weight { get; set; }
            public string ProductType { get; set; }
            public DateTime StartTime { get; set; }
            public string Packers { get; set; }

            public string MealBatchNo { get; set; }
            public string MealStartTemp { get; set; }
            public string MealFinishTemp { get; set; }
            public string SauceBatchNo { get; set; }
            public string SauceStartTemp { get; set; }
            public string SauceFinishTemp { get; set; }

            public string RiceVegName { get; set; }
            public string RiceVegBatchNo { get; set; }
            public string RiceVegStartTemp { get; set; }
            public string RiceVegFinishTemp { get; set; }

            public DateTime BatchChangeCheckTime { get; set; }
            public string PackingType { get; set; }
            public int PackingSize { get; set; }
            public string Unit { get; set; }
            public string PackagingBatchNo { get; set; }
            public string LidFilmBatchNo { get; set; }

            public string EquipmentVisualHygiene { get; set; }
            public string CheckStart { get; set; }
            public string CheckIfChange { get; set; }
            public string OldPackagingLabelsCleared { get; set; }
            public string CheckDuring { get; set; }
            public string CheckEnd { get; set; }

            public string LabelProductCode { get; set; }
            public string LabelType { get; set; }
            public int LabelQuantity { get; set; }
            public string LabelUnit { get; set; }
            public string LabelBatchNo { get; set; }
            public DateTime LabelBestBeforeEnd { get; set; }

            public string LabelCheckStart { get; set; }
            public string LabelCheckIfChange { get; set; }
            public string LabelAllergen { get; set; }
            public string LabelCheckDuring { get; set; }
            public string LabelCheckEnd { get; set; }

            public DateTime FinishTime { get; set; }
            public int NoOfPacksMade { get; set; }
            public DateTime TimeToFreezer { get; set; }
        }

        public static IEnumerable<dsVegetarianPackingRecord> GetVegetarianPackingRecord(IVegetarianPackingRecordService VegetarianPackingRecordService, int pId)
        {
            var data = VegetarianPackingRecordService.FindBy(filter => filter.iVegeRecordId == pId);

            return
            data.Select(row => new dsVegetarianPackingRecord()
            {
                BatchCode = row.tCookingPlan?.vBatchNo,
                Product = row.tCookingPlan?.tRecipe?.vRecipeCode + " - " + row.tCookingPlan?.tRecipe?.vRecipeName,
                Weight = Convert.ToInt32(row.iProductWeight),
                ProductType = row.vProductType,
                StartTime = Convert.ToDateTime(row.tStartTime),
                Packers = row.vPackers,
                MealBatchNo = row.vMealBatchNo,
                MealStartTemp = row.vMealStartTemp,
                MealFinishTemp = row.vMealFinishTemp,
                SauceBatchNo = row.vSauceBatchNo,
                SauceStartTemp = row.vSauceStartTemp,
                SauceFinishTemp = row.vSauceFinishTemp,
                RiceVegBatchNo = row.vVeg_RiceBatchNo,
                RiceVegStartTemp = row.vVeg_RiceStartTemp,
                RiceVegFinishTemp = row.vVeg_RiceFinishTemp,
                RiceVegName = row.vVeg_RiceName,

                BatchChangeCheckTime = Convert.ToDateTime(row.tBatchChangeCheckTime),
                PackingType = row.vPackagingType,
                PackingSize = Convert.ToInt32(row.iPackagingSize),
                Unit = row.tUnit?.vName,
                PackagingBatchNo = row.vPackagingBatchNo,
                LidFilmBatchNo = row.Lid_FilmBatchNo,

                EquipmentVisualHygiene = Convert.ToBoolean(row.bEquipVisualHygieneCheck) ? "Yes" : "No",
                CheckStart = Convert.ToBoolean(row.bCheckStart) ? "Yes" : "No",
                CheckIfChange = Convert.ToBoolean(row.bCheckIfChange) ? "Yes" : "No",
                OldPackagingLabelsCleared = Convert.ToBoolean(row.bOldPackaging_LabelsCleared) ? "Yes" : "No",
                CheckDuring = Convert.ToBoolean(row.bCheckDuring) ? "Yes" : "No",
                CheckEnd = Convert.ToBoolean(row.bCheckEnd) ? "Yes" : "No",

                LabelProductCode = row.vLabelProductCode,
                LabelType = row.vLabelType,
                LabelQuantity = Convert.ToInt32(row.iLabelQuantity),
                LabelBatchNo = row.vLabel_BatchNo,
                LabelBestBeforeEnd = Convert.ToDateTime(row.dLabelBestBefore),

                LabelCheckStart = Convert.ToBoolean(row.bCheckLabelStart) ? "Yes" : "No",
                LabelCheckIfChange = Convert.ToBoolean(row.bCheckLabelIfChange) ? "Yes" : "No",
                LabelAllergen = Convert.ToBoolean(row.bAllergen) ? "Yes" : "No",
                LabelCheckDuring = Convert.ToBoolean(row.bCheckLabelDuring) ? "Yes" : "No",
                LabelCheckEnd = Convert.ToBoolean(row.bCheckLabelEnd) ? "Yes" : "No",

                FinishTime = Convert.ToDateTime(row.tFinishTime),
                NoOfPacksMade = Convert.ToInt32(row.iNumberOfPacksMade),
                TimeToFreezer = Convert.ToDateTime(row.tTimeToFreezer)
            });
        }
    }
}