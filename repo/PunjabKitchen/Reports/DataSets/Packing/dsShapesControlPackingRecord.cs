﻿using PunjabKitchenBLL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PunjabKitchen.Reports.DataSets.Packing
{
    public class dsShapesControlPackingRecordReport
    {
        public class dsShapesControlPackingRecord
        {
            public string BatchNo { get; set; }
            public string Product { get; set; }
            public DateTime CookingFinishTime { get; set; }
            public DateTime PureeStartTime { get; set; }
            public int QuantityMade { get; set; }
            public string Unit { get; set; }
            public DateTime TimeIntoBlastFreezerLastDay { get; set; }
            public DateTime TimeExBlastFreezer { get; set; }
            public DateTime TimeExBlastFreezerBelow { get; set; }
            public DateTime TimeToStorageFreezer { get; set; }
            public string Packers { get; set; }
        }

        public static IEnumerable<dsShapesControlPackingRecord> GetShapesControlPackingRecord(IShapesControlRecordService ShapesControlPackingRecordService, int pId)
        {
            var data = ShapesControlPackingRecordService.FindBy(filter => filter.iShapeControlRecordId == pId);

            return
            data.Select(row => new dsShapesControlPackingRecord()
            {
                BatchNo = row.tCookingPlan?.vBatchNo,
                Product = row.tCookingPlan?.tRecipe?.vRecipeCode + " - " + row.tCookingPlan?.tRecipe?.vRecipeName,
                CookingFinishTime = Convert.ToDateTime(row.tCookingFinishTime),
                PureeStartTime = Convert.ToDateTime(row.tPureeStartTime),
                QuantityMade = Convert.ToInt32(row.iQuantityMade),
                Unit = row.tUnit?.vName,
                TimeIntoBlastFreezerLastDay = Convert.ToDateTime(row.tTimeIntoBlastFreezer),
                TimeExBlastFreezer = Convert.ToDateTime(row.tTimeExInBlastFreezer),
                TimeExBlastFreezerBelow = Convert.ToDateTime(row.tTimeIntoBlastFreezer),
                TimeToStorageFreezer = Convert.ToDateTime(row.tTimeToStorageFreezer),
                Packers = row.iPackerUserId.ToString()
            });
        }
    }
}