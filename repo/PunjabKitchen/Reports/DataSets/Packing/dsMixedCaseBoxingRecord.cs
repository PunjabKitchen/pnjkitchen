﻿using PunjabKitchenBLL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PunjabKitchen.Reports.DataSets.Packing
{
    public class dsMixedCaseBoxingRecordReport
    {
        public class dsMixedCaseBoxingRecord
        {
            public DateTime DateCaseMade { get; set; }
            public string CustomerName { get; set; }
            public int NoOfCases { get; set; }
            public DateTime DayOfBuild { get; set; }
            public string MixedBoxBatchNo { get; set; }
            public string CaseName { get; set; }
            public DateTime DateCaseGoingOut { get; set; }
            public DateTime LabelBestBefore { get; set; }
            public string LabelChecked { get; set; }
            public string CopyOnBack { get; set; }
            public string PackedBy { get; set; }

            public string ProductCode { get; set; }
            public string NameOfPack { get; set; }
            public string BatchCode { get; set; }
        }

        public static IEnumerable<dsMixedCaseBoxingRecord> GetMixedCaseBoxingRecord(IMixedCaseBoxingRecordService MixedCaseBoxingRecordService, int pId)
        {
            var data = MixedCaseBoxingRecordService.FindBy(filter => filter.iMixedCaseBoxingRecordId == pId).FirstOrDefault().tMixedCaseBoxingRecordDetails;

            return
            data.Select(row => new dsMixedCaseBoxingRecord()
            {
               DateCaseMade = Convert.ToDateTime(row.tMixedCaseBoxingRecord.dDateCaseMade),
               CustomerName = row.tMixedCaseBoxingRecord.vCustomer,
               NoOfCases = Convert.ToInt32(row.tMixedCaseBoxingRecord.iNoOfCases),
               DayOfBuild = Convert.ToDateTime(row.tMixedCaseBoxingRecord.dDayOfBuild),
               MixedBoxBatchNo = row.tMixedCaseBoxingRecord.vBatchNo,
               CaseName = row.tMixedCaseBoxingRecord.vCaseName,
               DateCaseGoingOut = Convert.ToDateTime(row.tMixedCaseBoxingRecord.vDateCaseGoingOut),
               LabelBestBefore = Convert.ToDateTime(row.tMixedCaseBoxingRecord.dLabelBestBefore),
               CopyOnBack = Convert.ToBoolean(row.tMixedCaseBoxingRecord.bCopyOnBack) ? "Yes" : "No",
               LabelChecked = Convert.ToBoolean(row.tMixedCaseBoxingRecord.bLabelChecked) ? "Yes" : "No",
               PackedBy = row.tMixedCaseBoxingRecord.AspNetUser2.FirstName + " " + row.tMixedCaseBoxingRecord.AspNetUser2.LastName,

               ProductCode = row.tProduct?.vName,
               NameOfPack = row.vNameOfPack,
               BatchCode = row.vBatchCode
            });
        }
    }
}