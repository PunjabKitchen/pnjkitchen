﻿using PunjabKitchenBLL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PunjabKitchen.Reports.DataSets.Packing
{
    public class dsPortionWeightsCheckPackingRecordReport
    {
        public class dsPortionWeightsCheckPackingRecord
        {
            public string Location { get; set; }
            public string BatchNo { get; set; }
            public string Product { get; set; }
            public int ProductWeight { get; set; }
            public string PortionName { get; set; }
            public int PortionWeight { get; set; }
            public int CheckWeight { get; set; }
        }

        public static IEnumerable<dsPortionWeightsCheckPackingRecord> GetPortionWeightsCheckPackingRecord(IPortionWeightsCheckRecordService PortionWeightsCheckPackingRecordService, int pId)
        {
            var data = PortionWeightsCheckPackingRecordService.FindBy(filter => filter.iPortionWeightCheckId == pId).FirstOrDefault().tPortionWeightCheckDetails;

            return
            data.Select(row => new dsPortionWeightsCheckPackingRecord()
            {
                Location = row.tPortionWeightCheck.vLocation,
                BatchNo = row.tPortionWeightCheck.vBatchNo,
                Product = row.tPortionWeightCheck.tCookingPlan?.tProduct?.vProductCode + " " + row.tPortionWeightCheck.tCookingPlan?.tProduct?.vName,
                ProductWeight = Convert.ToInt32(row.tPortionWeightCheck.iProductWeight),
                PortionName = row.vPortionName,
                PortionWeight = Convert.ToInt32(row.iPortionWeight),
                CheckWeight = Convert.ToInt32(row.iPortionCheckWeight)
            });
        }
    }
}