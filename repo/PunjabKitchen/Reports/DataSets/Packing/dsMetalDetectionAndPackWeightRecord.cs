﻿using PunjabKitchenBLL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PunjabKitchen.Reports.DataSets.Packing
{
    public class dsMetalDetectionAndPackWeightRecordReport
    {
        public class dsMetalDetectionAndPackWeightRecord
        {
            public string Location { get; set; }
            public string BatchCode { get; set; }
            public string Product { get; set; }
            public int ProductWeight { get; set; }

            public int Pack1Weight { get; set; }
            public int Pack2Weight { get; set; }
            public int Pack3Weight { get; set; }
            public int Pack4Weight { get; set; }
            public int Pack5Weight { get; set; }
            public int Pack6Weight { get; set; }
            public int Pack7Weight { get; set; }
            public int Pack8Weight { get; set; }
            public int Pack9Weight { get; set; }
            public int Pack10Weight { get; set; }

            public string Ferrous2mm { get; set; }
            public string NonFerrous25mm { get; set; }
            public string RejectMechanismWorking { get; set; }
            public string StainlessSteel45mm { get; set; }
        }

        public static IEnumerable<dsMetalDetectionAndPackWeightRecord> GetMetalDetectionAndPackWeightRecord(IFMDetectionAndPackWeightRecordService MetalDetectionAndPackWeightRecordService, int pId)
        {
            var data = MetalDetectionAndPackWeightRecordService.FindBy(filter => filter.iDetectionPackWeightTestId == pId);

            return
            data.Select(row => new dsMetalDetectionAndPackWeightRecord()
            {
                Location = row.vLocation,
                BatchCode = row.vBatchNo,
                Product = row.tCookingPlan?.tRecipe?.vRecipeCode + " - " + row.tCookingPlan?.tRecipe?.vRecipeName,
                ProductWeight = Convert.ToInt32(row.iProductWeight),

                Pack1Weight = Convert.ToInt32(row.iWeightOfPacks1),
                Pack2Weight = Convert.ToInt32(row.iWeightOfPacks2),
                Pack3Weight = Convert.ToInt32(row.iWeightOfPacks3),
                Pack4Weight = Convert.ToInt32(row.iWeightOfPacks4),
                Pack5Weight = Convert.ToInt32(row.iWeightOfPacks5),
                Pack6Weight = Convert.ToInt32(row.iWeightOfPacks6),
                Pack7Weight = Convert.ToInt32(row.iWeightOfPacks7),
                Pack8Weight = Convert.ToInt32(row.iWeightOfPacks8),
                Pack9Weight = Convert.ToInt32(row.iWeightOfPacks9),
                Pack10Weight = Convert.ToInt32(row.iWeightOfPacks10),

                Ferrous2mm = Convert.ToBoolean(row.bFerrous2_0mmPass) ? "Yes" : "No",
                NonFerrous25mm = Convert.ToBoolean(row.bNonFerrous2_5mmPass) ? "Yes" : "No",
                RejectMechanismWorking = Convert.ToBoolean(row.bRejectMechnismWorking) ? "Yes" : "No",
                StainlessSteel45mm = Convert.ToBoolean(row.bStainlessSteel4_5mmPass) ? "Yes" : "No"
            });
        }
    }
}