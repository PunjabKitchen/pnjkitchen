﻿using PunjabKitchenBLL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PunjabKitchen.Reports.DataSets.Packing
{
    public class dsBoxingRecordReport
    {
        public class dsBoxingRecord
        {
            public string ProductCode { get; set; }
            public string ProductName { get; set; }
            public string BatchNo { get; set; }
            public DateTime BestBefore { get; set; }
            public int Weight { get; set; }
            public string BoxType { get; set; }
            public string BoxBatchCode { get; set; }
            public Nullable<decimal> ProductTemp { get; set; }
            public int TotalCases { get; set; }
            public DateTime StartTime { get; set; }
            public DateTime FinishTime { get; set; }

            public string EquipVisualHygiene { get; set; }
            public string BoxLabelBatchNo { get; set; }
            public string BoxTypeAndBatchNo { get; set; }
            public string ChecksIfChange { get; set; }
            public string OldBoxesClearedAway { get; set; }
            public string BoxLabelBestBefore { get; set; }
            public string ChecksAtStart { get; set; }
            public string ChecksAtEnd { get; set; }
            public string BoxLabelProductCode { get; set; }
            public string BoxLabelQuantity { get; set; }
            public string ChecksDuring { get; set; }
        }

        public static IEnumerable<dsBoxingRecord> GetBoxingRecord(IBoxingRecordService BoxingRecordService, int pId)
        {
            var data = BoxingRecordService.FindBy(filter => filter.iBoxingRecordId == pId);

            return
            data.Select(row => new dsBoxingRecord()
            {
                ProductCode = row.tCookingPlan?.tRecipe?.vRecipeCode,
                ProductName = row.tCookingPlan?.tRecipe?.vRecipeName,
                BatchNo = row.vBatchNo,
                BestBefore = Convert.ToDateTime(row.dBestBefore),
                Weight = Convert.ToInt32(row.iQuatity),
                BoxType = row.vBoxType,
                BoxBatchCode = row.vBoxBatchCode,
                ProductTemp = row.vProductTemp,
                TotalCases = Convert.ToInt32(row.iTotalCases_Trays),
                StartTime = Convert.ToDateTime(row.tStartTime),
                FinishTime = Convert.ToDateTime(row.tFinishTime),
                EquipVisualHygiene = Convert.ToBoolean(row.bEquipVisualHygiene) ? "Yes" : "No",
                BoxLabelBatchNo = Convert.ToBoolean(row.bBoxLabelBatchNumber) ? "Yes" : "No",
                BoxLabelBestBefore = Convert.ToBoolean(row.bBoxLabelBestBefore) ? "Yes" : "No",
                BoxLabelProductCode = Convert.ToBoolean(row.bBoxLabelProductCode) ? "Yes" : "No",
                BoxLabelQuantity = Convert.ToBoolean(row.bBoxLabelQuatity) ? "Yes" : "No",
                BoxTypeAndBatchNo = Convert.ToBoolean(row.bBoxTypeAndBatchNo) ? "Yes" : "No",
                ChecksAtEnd = Convert.ToBoolean(row.bChecksAtEnd) ? "Yes" : "No",
                ChecksAtStart = Convert.ToBoolean(row.bChecksAtStart) ? "Yes" : "No",
                ChecksDuring = Convert.ToBoolean(row.bChecksDuring) ? "Yes" : "No",
                ChecksIfChange = Convert.ToBoolean(row.bChecksIfChange) ? "Yes" : "No",
                OldBoxesClearedAway = Convert.ToBoolean(row.bOldBoxesAndLabelsCleared) ? "Yes" : "No"
            });
        }
    }
}