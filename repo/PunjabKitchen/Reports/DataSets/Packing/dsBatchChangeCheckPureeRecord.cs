﻿using PunjabKitchenBLL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PunjabKitchen.Reports.DataSets.Packing
{
    public class dsBatchChangeCheckPureeRecordReport
    {
        public class dsBatchChangeCheckPureeRecord
        {
            public string ProductCode { get; set; }
            public string ProductName { get; set; }
            public string PackagingType { get; set; }
            public int PackagingSize { get; set; }
            public string PackagingBatchNo { get; set; }
            public string LidFilmBatchNo { get; set; }

            public string EquipmentVisualHygiene { get; set; }
            public string CheckStart { get; set; }
            public string CheckIfChange { get; set; }
            public string OldPackagingLabelsCleared { get; set; }
            public string CheckDuring { get; set; }
            public string CheckEnd { get; set; }

            public string LabelProductCode { get; set; }
            public string LabelType { get; set; }
            public int LabelWeight { get; set; }
            public string LabelBatchNo { get; set; }
            public DateTime LabelBestBeforeEnd { get; set; }

            public string LabelCheckPrintPosition { get; set; }
            public string LabelCheckStart { get; set; }
            public string LabelCheckIfChange { get; set; }
            public string LabelAllergen { get; set; }
            public string LabelCheckDuring { get; set; }
            public string LabelCheckEnd { get; set; }
        }

        public static IEnumerable<dsBatchChangeCheckPureeRecord> GetBatchChangeCheckPureeRecord(IBatchChangePureeRecordService BatchChangeCheckPureeRecordService, int pId)
        {
            var data = BatchChangeCheckPureeRecordService.FindBy(filter => filter.iBatchChangeChkPureeRecrdId == pId);

            return
            data.Select(row => new dsBatchChangeCheckPureeRecord()
            {
                ProductCode = row.tRecipe?.tProduct?.vProductCode,
                ProductName = row.tRecipe?.tProduct?.vName,
                PackagingType = row.vPackageType,
                PackagingSize = Convert.ToInt32(row.iPackagingSize),
                PackagingBatchNo = row.vPackagingBatchNo,
                LidFilmBatchNo = row.vLid_FilmBatchNo,

                EquipmentVisualHygiene = Convert.ToBoolean(row.bEquipVisualHygenChk) ? "Yes" : "No",
                CheckStart = Convert.ToBoolean(row.bCheckStart) ? "Yes" : "No",
                CheckIfChange = Convert.ToBoolean(row.bCheckIfChange) ? "Yes" : "No",
                OldPackagingLabelsCleared = Convert.ToBoolean(row.bOldPackagesLablesCleared) ? "Yes" : "No",
                CheckDuring = Convert.ToBoolean(row.bCheckDuring) ? "Yes" : "No",
                CheckEnd = Convert.ToBoolean(row.bCheckEnd) ? "Yes" : "No",

                LabelProductCode = row.vProductCode,
                LabelType = row.vLabelType,
                LabelWeight = Convert.ToInt32(row.iLabelWeight),
                LabelBatchNo = row.vLabelBatchNo,
                LabelBestBeforeEnd = Convert.ToDateTime(row.dLabelBestBeforeEnd),

                LabelCheckPrintPosition = Convert.ToBoolean(row.bCheckPrintPositionlegibility) ? "Yes" : "No",
                LabelCheckStart = Convert.ToBoolean(row.bCheckLabelStart) ? "Yes" : "No",
                LabelCheckIfChange = Convert.ToBoolean(row.bCheckLabelIfChange) ? "Yes" : "No",
                LabelAllergen = Convert.ToBoolean(row.bAllergin) ? "Yes" : "No",
                LabelCheckDuring = Convert.ToBoolean(row.bCheckLabelDuring) ? "Yes" : "No",
                LabelCheckEnd = Convert.ToBoolean(row.bCheckLabelEnd) ? "Yes" : "No"
            });
        }
    }
}