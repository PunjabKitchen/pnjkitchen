﻿using PunjabKitchenBLL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PunjabKitchen.Reports.DataSets.Packing
{
    public class dsHotPackingRecordReport
    {
        public class dsHotPackingRecord
        {
            public string BatchNo { get; set; }
            public string Product { get; set; }
            public DateTime TimeExCook { get; set; }
            public DateTime TimeOfLastTrayIntoBlastFreezer { get; set; }
            public DateTime TimeTaken { get; set; }
        }

        public static IEnumerable<dsHotPackingRecord> GetHotPackingRecord(IHotPackingRercordService HotPackingRecordService, int pId)
        {
            var data = HotPackingRecordService.FindBy(filter => filter.iHotPackingRercordId == pId);

            return
            data.Select(row => new dsHotPackingRecord()
            {
                BatchNo = row.tCookingPlan?.vBatchNo,
                Product = row.tCookingPlan?.tRecipe?.vRecipeCode + " - " + row.tCookingPlan?.tRecipe?.vRecipeName,
                TimeExCook = Convert.ToDateTime(row.tTimeExCook),
                TimeOfLastTrayIntoBlastFreezer = Convert.ToDateTime(row.tTimeOfLastTrayInBlastFreezer),
                TimeTaken = Convert.ToDateTime(row.tTimeTaken)
            });
        }
    }
}