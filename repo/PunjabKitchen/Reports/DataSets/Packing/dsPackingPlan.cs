﻿using PunjabKitchenBLL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PunjabKitchen.Reports.DataSets.Packing
{
    public class dsPackingPlanReport
    {
        public class dsPackingPlan
        {
            public DateTime PlanDate { get; set; }
            public string PackingLineNo { get; set; }
            public string Type { get; set; }
            public string BatchCode { get; set; }
            public string ProductName { get; set; }
            public int Weight { get; set; }
            public int QuantityRequired { get; set; }
            public DateTime RunTime { get; set; }
            public DateTime PlanStart { get; set; }
            public DateTime PlanFinish { get; set; }
            public DateTime Duration { get; set; }
            public int PacksPerMin { get; set; }
        }

        public static IEnumerable<dsPackingPlan> GetPackingPlan(IPackingPlanService packingPlanService, int pId)
        {
            var data = packingPlanService.FindBy(filter => filter.iPackingProductionPlanId == pId);

            return
            data.Select(row => new dsPackingPlan()
            {
               PlanDate = Convert.ToDateTime(row.dPackingPlan),
               PackingLineNo = "Line " + row.iLineNo,
               Type = Convert.ToBoolean(row.bIsTask) ? "Task" : "Product",
               BatchCode = row.vCookingBatchNo,
               ProductName = row.tCookingPlan?.tRecipe?.vRecipeName,
               Weight = Convert.ToInt32(row.iWeight),
               QuantityRequired = Convert.ToInt32(row.iQuantityRequired),
               RunTime = Convert.ToDateTime(row.tRunTime),
               PlanStart = Convert.ToDateTime(row.tPlannedStart),
               PlanFinish = Convert.ToDateTime(row.tPlannedFinish),
               Duration = new DateTime((Convert.ToDateTime(row.tPlannedFinish) - Convert.ToDateTime(row.tPlannedStart)).Ticks),
               PacksPerMin = Convert.ToInt32(row.iPacksPerMin)
            });
        }
    }
}