﻿using PunjabKitchenBLL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
// ReSharper disable All

namespace PunjabKitchen.Reports.DataSets.Packing
{
    public class dsPureeMealBatchAssemblyRecordReport
    {
        public class dsPureeMealBatchAssemblyRecord
        {
            public string BatchNo { get; set; }
            public string Product { get; set; }
            public DateTime Date { get; set; }
            public string AssembledBy { get; set; }

            public string ProductReleased { get; set; }
            public string ProductMeetsQAChecks { get; set; }
            public string ProductMeatPackagingQuality { get; set; }
            public string ProductMeetsSpecification { get; set; }
            public string Comments { get; set; }
        }

        public class dsPureeMealBatchAssemblyMealRecord
        {
            public string MealShape { get; set; }
            public string MealBatchNo { get; set; }
            public string MealTemp { get; set; }
            public DateTime MealTime { get; set; }
        }

        public class dsPureeMealBatchAssemblySauceRecord
        {
            public string Sauce { get; set; }
            public string SauceBatchNo { get; set; }
            public string SauceTemp { get; set; }
            public DateTime SauceTimeOfTransfer { get; set; }
        }

        public class dsPureeMealBatchAssemblyMashRecord
        {
            public string Mash { get; set; }
            public string MashBatchNo { get; set; }
            public string MashTemp { get; set; }
            public DateTime MashTimeOfTransfer { get; set; }
        }

        public class dsPureeMealBatchAssemblyBlastFreezerRecord
        {
            public DateTime TimeStart { get; set; }
            public DateTime TimeOfLastPackInFreezer { get; set; }
            public int NoOfPacks { get; set; }
        }

        public static IEnumerable<dsPureeMealBatchAssemblyRecord> GetPureeMealBatchAssemblyRecord(IPureeMealBatchAssemblyService  PureeMealBatchAssemblyRecordService, int pId)
        {
            var data = PureeMealBatchAssemblyRecordService.FindBy(filter => filter.iPureeMeanBatchAssemblyId == pId);

            return
            data.Select(row => new dsPureeMealBatchAssemblyRecord()
            {
                BatchNo = row.tCookingPlan?.vBatchNo,
                Product = row.tCookingPlan?.tRecipe?.vRecipeCode + " - " + row.tCookingPlan?.tRecipe?.vRecipeName,
                Date = Convert.ToDateTime(row.dDate),
                AssembledBy = string.Join(", ", row.tPureeMealBatchAssemblyPackerDetails.ToList().Select(e => e.AspNetUser.FirstName + " " + e.AspNetUser.LastName)),
                Comments = row.vComments,
                ProductMeatPackagingQuality = Convert.ToBoolean(row.bProductMeatsPackingQuality) ? "Yes" : "No",
                ProductMeetsQAChecks = Convert.ToBoolean(row.bProductMeatsQA) ? "Yes" : "No",
                ProductMeetsSpecification = Convert.ToBoolean(row.bProductMeatsSpecs) ? "Yes" : "No",
                ProductReleased = Convert.ToBoolean(row.bProductReleased) ? "Yes" : "No"
            });
        }

        public static IEnumerable<dsPureeMealBatchAssemblyMealRecord> GetPureeMealBatchAssemblyMealRecord(IPureeMealBatchAssemblyService PureeMealBatchAssemblyRecordService, int pId)
        {
            var data = PureeMealBatchAssemblyRecordService.FindBy(filter => filter.iPureeMeanBatchAssemblyId == pId).FirstOrDefault().tPureeMealBatchAssemblyDetails;

            return
            data.Where(w => !String.IsNullOrEmpty(w.vShape) && !String.IsNullOrEmpty(w.vShapeTemp) && !String.IsNullOrEmpty(w.vShapeBatchNo)).Select(row => new dsPureeMealBatchAssemblyMealRecord()
            {
                MealBatchNo = row.vShapeBatchNo,
                MealShape = row.vShape,
                MealTemp = row.vShapeTemp,
                MealTime = Convert.ToDateTime(row.dTime)
            });
        }

        public static IEnumerable<dsPureeMealBatchAssemblySauceRecord> GetPureeMealBatchAssemblySauceRecord(IPureeMealBatchAssemblyService PureeMealBatchAssemblyRecordService, int pId)
        {
            var data = PureeMealBatchAssemblyRecordService.FindBy(filter => filter.iPureeMeanBatchAssemblyId == pId).FirstOrDefault().tPureeMealBatchAssemblyDetails;

            return
            data.Where(w => !String.IsNullOrEmpty(w.vSauce) && !String.IsNullOrEmpty(w.vSauceTemp) && !String.IsNullOrEmpty(w.vSauceBatchNo)).Select(row => new dsPureeMealBatchAssemblySauceRecord()
            {
                SauceBatchNo = row.vSauceBatchNo,
                Sauce = row.vSauce,
                SauceTemp = row.vSauceTemp,
                SauceTimeOfTransfer = Convert.ToDateTime(row.tSauceTimeOfTransferFromPrep)
            });
        }

        public static IEnumerable<dsPureeMealBatchAssemblyMashRecord> GetPureeMealBatchAssemblyMashRecord(IPureeMealBatchAssemblyService PureeMealBatchAssemblyRecordService, int pId)
        {
            var data = PureeMealBatchAssemblyRecordService.FindBy(filter => filter.iPureeMeanBatchAssemblyId == pId).FirstOrDefault().tPureeMealBatchAssemblyDetails;

            return
            data.Where(w => !String.IsNullOrEmpty(w.vMash) && !String.IsNullOrEmpty(w.vMashTemp) && !String.IsNullOrEmpty(w.vMashBatchNo)).Select(row => new dsPureeMealBatchAssemblyMashRecord()
            {
                MashBatchNo = row.vMashBatchNo,
                Mash = row.vMash,
                MashTemp = row.vMashTemp,
                MashTimeOfTransfer = Convert.ToDateTime(row.tMashTimeOfTransferFromPrep)
            });
        }

        public static IEnumerable<dsPureeMealBatchAssemblyBlastFreezerRecord> GetPureeMealBatchAssemblyBlastFreezerRecord(IPureeMealBatchAssemblyService PureeMealBatchAssemblyRecordService, int pId)
        {
            var data = PureeMealBatchAssemblyRecordService.FindBy(filter => filter.iPureeMeanBatchAssemblyId == pId).FirstOrDefault().tPureeMealInBlastFreezers;

            return
            data.Select(row => new dsPureeMealBatchAssemblyBlastFreezerRecord()
            {
                TimeStart = Convert.ToDateTime(row.tStartTime),
                TimeOfLastPackInFreezer = Convert.ToDateTime(row.tLastPackTimeInFreezer),
                NoOfPacks = Convert.ToInt16(row.iNumberOfPacks)
            });
        }
    }
}