﻿using PunjabKitchenBLL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace PunjabKitchen.Reports.DataSets.Cooking
{
    public class dsSlicingRecordReport
    {
        public class dsSlicingRecord
        {
            public string BatchNo { get; set; }
            public string Product { get; set; }
            public int TrolleyNo { get; set; }
            public DateTime StartTime { get; set; }
            public string PreSlicingTemp { get; set; }
            public DateTime FinishTime { get; set; }
            public string PostSlicingTemp { get; set; }
        }

        public static IEnumerable<dsSlicingRecord> GetSlicingRecord(ISlicingRecordService SlicingRecordService, int pId)
        {
            var data = SlicingRecordService.FindBy(filter =>
                                                filter.iSlicingRecordId == pId
                                            );

            return 
            data.Select(row => new dsSlicingRecord()
            {
                BatchNo = row.tCookingPlan?.vBatchNo,
                Product = row.tCookingPlan?.tRecipe?.vRecipeCode + " - " + row.tCookingPlan?.tRecipe?.vRecipeName,
                TrolleyNo = Convert.ToInt32(row.iTrolleyNo),
                StartTime = Convert.ToDateTime(row.tStartTime),
                PreSlicingTemp = row.vTempProductMaxStart,
                FinishTime = Convert.ToDateTime(row.tFinishTime),
                PostSlicingTemp = row.vTempProductMaxEnd
            });
        }
    }
}