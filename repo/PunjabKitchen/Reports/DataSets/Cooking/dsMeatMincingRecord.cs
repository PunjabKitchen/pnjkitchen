﻿using PunjabKitchenBLL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace PunjabKitchen.Reports.DataSets.Cooking
{
    public class dsMeatMincingRecordReport
    {
        public class dsMeatMincingRecord
        {
            public DateTime StartTime { get; set; }
            public string BatchNo { get; set; }
            public DateTime BestBefore { get; set; }
            public string PreUseCleanlinessCheck { get; set; }
            public DateTime FinishTime { get; set; }
            public string Meat { get; set; }
            public string Purpose { get; set; }
        }

        public static IEnumerable<dsMeatMincingRecord> GetMeatMincingRecord(IMeatMincingRecordService MeatMincingRecordService, int pId)
        {
            var data = MeatMincingRecordService.FindBy(filter =>
                                                filter.iMeatMincingRecordId == pId
                                            );

            return 
            data.Select(row => new dsMeatMincingRecord()
            {
                BatchNo = row.tCookingPlan?.vBatchNo,                
                StartTime = Convert.ToDateTime(row.tStartTime),
                FinishTime = Convert.ToDateTime(row.tFinishTime),
                BestBefore = Convert.ToDateTime(row.dBestBefore),
                Meat = row.vMeat,
                PreUseCleanlinessCheck = row.vPreUseCleanlinessCheckSign
            });
        }
    }
}