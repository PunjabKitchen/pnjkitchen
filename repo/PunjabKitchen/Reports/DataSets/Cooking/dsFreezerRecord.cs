﻿using PunjabKitchenBLL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace PunjabKitchen.Reports.DataSets.Cooking
{
    public class dsFreezerRecordReport
    {
        public class dsFreezerRecord
        {
            public string GoodsInFreezerTemp { get; set; }
            public string ShapesFreezerTemp { get; set; }
            public string CarParkFreezer { get; set; }
            public string Comments { get; set; }
        }

        public static IEnumerable<dsFreezerRecord> GetFreezerRecord(IFreezerRecordService FreezerRecordService, int pId)
        {
            var data = FreezerRecordService.FindBy(filter =>
                                                filter.iFreezerRecordId == pId
                                            );

            return 
            data.Select(row => new dsFreezerRecord()
            {
                GoodsInFreezerTemp = row.vGoodsInFreezerTempPm,
                ShapesFreezerTemp = row.vShapesFreezerTempPm,
                CarParkFreezer = row.vCarParkFreezerTempPm,
                Comments = row.vComment
            });
        }
    }
}