﻿using PunjabKitchenBLL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace PunjabKitchen.Reports.DataSets.Cooking
{
    public class dsBatchCoolingTempRecordReport
    {
        public class dsBatchCoolingTempRecord
        {
            public string BatchNo { get; set; }
            public string Product { get; set; }
            public int Quantity { get; set; }
            public string Unit { get; set; }
            public DateTime TimeExCook { get; set; }
            public string TempInBlastChiller { get; set; }
            public DateTime TimeIntoBlastChiller { get; set; }
            public string TempExBlastChiller { get; set; }
            public DateTime TimeToWIPChiller { get; set; }
            public string Comments { get; set; }
        }

        public static IEnumerable<dsBatchCoolingTempRecord> GetBatchCoolingTempRecord(IBatchCoolingTempRecrdService BatchCoolingTempRecordService, int pId)
        {
            var data = BatchCoolingTempRecordService.FindBy(filter =>
                                                filter.iBatchCoolingTempRecrdId == pId
                                            );

            return 
            data.Select(row => new dsBatchCoolingTempRecord()
            {
                BatchNo = row.tCookingPlan?.vBatchNo,
                Product = row.tCookingPlan?.tRecipe?.vRecipeCode + " - " + row.tCookingPlan?.tRecipe?.vRecipeName,
                Quantity = Convert.ToInt32(row.iQuantity),
                TimeExCook = Convert.ToDateTime(row.tTimeExCook),
                TempInBlastChiller = row.vTempInBlastChiller,
                TimeIntoBlastChiller = Convert.ToDateTime(row.tTimeInBlastChiller),
                TimeToWIPChiller = Convert.ToDateTime(row.tTimeToWipChiller),
                Comments = row.vComments,
                TempExBlastChiller = row.vTempExInBlastChiller,
                Unit = row.tUnit?.vName
            });
        }
    }
}