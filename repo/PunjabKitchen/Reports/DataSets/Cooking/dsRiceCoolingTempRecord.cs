﻿using PunjabKitchenBLL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace PunjabKitchen.Reports.DataSets.Cooking
{
    public class dsRiceCoolingTempRecordReport
    {
        public class dsRiceCoolingTempRecord
        {
            public string BatchNo { get; set; }
            public string Product { get; set; }
            public int Quantity { get; set; }
            public string Unit { get; set; }
            public DateTime TimeExCook { get; set; }
            public string TempAfterWaterRinse { get; set; }
            public DateTime TimeToWIPChiller { get; set; }
            public string Comments { get; set; }
        }

        public static IEnumerable<dsRiceCoolingTempRecord> GetRiceCoolingTempRecord(IRiceCoolingTempRecordService RiceCoolingTempRecordService, int pId)
        {
            var data = RiceCoolingTempRecordService.FindBy(filter =>
                                                filter.iRiceCoolingTempRecordId == pId
                                            );

            return 
            data.Select(row => new dsRiceCoolingTempRecord()
            {
                BatchNo = row.tCookingPlan?.vBatchNo,
                Product = row.tCookingPlan?.tRecipe?.vRecipeCode + " - " + row.tCookingPlan?.tRecipe?.vRecipeName,
                Quantity = Convert.ToInt32(row.iQuantity),
                Unit = row.tUnit.vName,
                TimeExCook = Convert.ToDateTime(row.tTimeExCook),
                TempAfterWaterRinse = row.vTempAfterWaterRinse,
                TimeToWIPChiller = Convert.ToDateTime(row.tTimeToWIPChiller),
                Comments = row.vComments
            });
        }
    }
}