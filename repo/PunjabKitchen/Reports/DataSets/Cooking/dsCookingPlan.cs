﻿using PunjabKitchenBLL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace PunjabKitchen.Reports.DataSets.Cooking
{
    public class dsCookingPlanReport
    {
        public class dsCookingPlan
        {
            public int CookingPotNo { get; set; }
            public DateTime CookingPlanDate { get; set; }
            public string Type { get; set; }
            public string ProductCode { get; set; }
            public string ProductName { get; set; }
            public int QuantityRequired { get; set; }
            public int EstimatedMeals { get; set; }
            public string BatchNo { get; set; }
            public DateTime StartTime { get; set; }
            public DateTime Durantion { get; set; }
            public DateTime FinishTime { get; set; }
        }

        public static IEnumerable<dsCookingPlan> GetCookingPlan(ICookingPlanService cookingPlanService, int pId)
        {
            var data = cookingPlanService.FindBy(filter =>
                                                filter.iCookingPlanId == pId
                                            );

            return 
            data.Select(row => new dsCookingPlan()
            {
                CookingPotNo = Convert.ToInt32(row.iCookPotNo),
                CookingPlanDate = Convert.ToDateTime(row.tCookDate),
                Type = Convert.ToBoolean(row.bIsTask) ? "Task" : "Product",
                ProductCode = row.tRecipe?.vRecipeCode,
                ProductName = row.tRecipe?.vRecipeName,
                QuantityRequired = Convert.ToInt32(row.iQuantity),
                EstimatedMeals = Convert.ToInt32(row.iEstMeals),
                BatchNo = row.vBatchNo,
                StartTime = Convert.ToDateTime(row.tActualStartTime),
                FinishTime = Convert.ToDateTime(row.tActualFinishTime),
                Durantion = new DateTime((Convert.ToDateTime(row.tActualFinishTime) - Convert.ToDateTime(row.tActualStartTime)).Ticks)
            });
        }
    }
}