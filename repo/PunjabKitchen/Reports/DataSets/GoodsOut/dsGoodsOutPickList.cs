﻿using PunjabKitchenBLL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace PunjabKitchen.Reports.DataSets.GoodsOut
{
    public class dsGoodsOutPickListReport
    {
        public class dsGoodsOutPickList
        {
            public string Location { get; set; }
            public string ShelfRack { get; set; }
            public string Customer { get; set; }
            public int OrderNo { get; set; }
            public char Priority { get; set; }
            public DateTime DeliveryDate { get; set; }
            public string LastAmendedBy { get; set; }
            public string DeliverTo { get; set; }

            public string Shelf { get; set; }
            public string ItemCode { get; set; }
            public string ItemName { get; set; }
            public int Quantity { get; set; }
            public string BatchNo { get; set; }
            public string OldLabel { get; set; }
            public int ToFollow { get; set; }
            public string Unit { get; set; }
        }

        public static IEnumerable<dsGoodsOutPickList> GetGoodsOutPickList(IGoodsOutPicklistService GoodsOutPickListService, int pId)
        {
            var data = GoodsOutPickListService.FindBy(filter => filter.iGoodsOutPickListId == pId).FirstOrDefault().tGoodsOutPicklistDetails;

            return
            data.Select(row => new dsGoodsOutPickList()
            {
                Location = row.tGoodsOutPicklist.tStock?.vStorageLocation,
                ShelfRack = row.tGoodsOutPicklist.tStock?.vStoragePoint,
                Customer = (string.IsNullOrEmpty(row.tGoodsOutPicklist.vCustomerCode) ? "" : row.tGoodsOutPicklist.vCustomerCode) + " " + row.tGoodsOutPicklist.vCustomer,
                OrderNo = Convert.ToInt32(row.tGoodsOutPicklist.iOrderNumber),
                Priority = Convert.ToChar(row.tGoodsOutPicklist.cPriority),
                DeliveryDate = Convert.ToDateTime(row.tGoodsOutPicklist.dDeliveryDate),
                LastAmendedBy = row.tGoodsOutPicklist.Sys_ModifyBy?.ToString(),
                DeliverTo = row.tGoodsOutPicklist.vDeliverTo,

                Shelf = row.vShelf,
                ItemCode = row.tRecipe?.tProduct?.vProductCode,
                ItemName = row.tRecipe?.tProduct?.vName,
                Quantity = Convert.ToInt32(row.iQuantity),
                BatchNo = row.vBatchNo,
                OldLabel = Convert.ToBoolean(row.bOldLabel) ? "Yes" : "No",
                ToFollow = Convert.ToInt32(row.iToFollow),
                Unit = row.tUnit?.vName
            });
        }
    }
}