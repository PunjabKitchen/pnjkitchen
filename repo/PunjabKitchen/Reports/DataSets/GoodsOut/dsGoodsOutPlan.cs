﻿using PunjabKitchenBLL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace PunjabKitchen.Reports.DataSets.GoodsOut
{
    public class dsGoodsOutPlanReport
    {
        public class dsGoodsOutPlan
        {
            public DateTime PlanDate { get; set; }
            public string Category { get; set; }

            public string ProductCode { get; set; }
            public string ProductName { get; set; }
            public string CategoryDescription { get; set; }
            public int Quantity { get; set; }
            public int NoOfMeals { get; set; }
        }

        public static IEnumerable<dsGoodsOutPlan> GetGoodsOutPlan(IGoodsOutPlanService GoodsOutPlanService, int pId)
        {
            var data = GoodsOutPlanService.FindBy(filter => filter.iGoodsOutPlanId == pId).FirstOrDefault().tGoodsOutPlanDetails;

            var result = new List<dsGoodsOutPlan>();

            foreach (var row in data)
            {
                dsGoodsOutPlan _row = new dsGoodsOutPlan
                {
                    PlanDate = Convert.ToDateTime(row.tGoodsOutPlan.dDate),
                    Category = row.tGoodsOutPlan.vCategory,
                    ProductCode = row.tProduct.vProductCode,
                    ProductName = row.tProduct.vName,
                    CategoryDescription = row.vCategoryDescription,
                    Quantity = Convert.ToInt32(row.vQuantity),
                    NoOfMeals = Convert.ToInt32(row.iNumberOfMeals)
                };

                result.Add(_row);
            }

            return result;
        }
    }
}