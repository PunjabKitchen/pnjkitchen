﻿using System;
using System.Collections.Generic;
using System.Linq;
using PunjabKitchenEntities.Common;
using System.Data.Entity;

namespace PunjabKitchen.Bl
{
    public class StockReport
    {
        public string Id { get; set; }
        public string StockItemName { get; set; }
        public string Quantity { get; set; }
        public string DateAdded { get; set; }
        public string UserAdded { get; set; }
        public string StockType { get; set; }
        public string BatchNo { get; set; }
    }

    public class GoodsInReport
    {
        public string Id { get; set; }
        public string Supplier { get; set; }
        public string Product { get; set; }
        public string ProductType { get; set; }
        public string VehicleInternalCheck { get; set; }
        public string RawMaterial { get; set; }
        public string VehicleTemp { get; set; }
        public string UserAdded { get; set; }
        public string DateAdded { get; set; }
    }

    public class CookingPlanReport
    {
        public string Id { get; set; }
        public string CookPot { get; set; }
        public string Quantity { get; set; }
        public string EstimatedMeals { get; set; }
        public string QuantityCompleted { get; set; }
        public string CookDate { get; set; }
        public string isTask { get; set; }
        public string Recipe { get; set; }
        public string UserAdded { get; set; }
        public string PlannedStartTime { get; set; }
        public string PlannedFinishTime { get; set; }
        public string ActualStartTime { get; set; }
        public string ActualFinishTime { get; set; }
    }

    public class GoodsOutReport
    {
        public string Id { get; set; }
        public string BatchCode { get; set; }
        public string DateAdded { get; set; }
        public string UserAdded { get; set; }
        public string Category { get; set; }
        public string CategoryDescription { get; set; }
        public string Product { get; set; }
        public string NumberOfMeals { get; set; }
        public string Quantity { get; set; }
    }

    public class PackingReport
    {
        public string Id { get; set; }
        public DateTime PlanDate { get; set; }
        public string CookingLine { get; set; }
        public string PlanType { get; set; }
        public string ProductCode { get; set; }
        public string ProductDesc { get; set; }
        public int Weight { get; set; }
        public int QtyRequired { get; set; }
        public DateTime Runtime { get; set; }
        public DateTime PlannedStartTime { get; set; }
        public DateTime Durantion { get; set; }
        public DateTime PlannedFinish { get; set; }
        public int PacksPerMin { get; set; }
        public string BatchNo { get; set; }
        public string UserAdded { get; set; }
        public DateTime DateAdded { get; set; }
    }

    public class BoxingReport
    {
        public string Id { get; set; }
        public string ProductCode { get; set; }
        public string ProductName { get; set; }
        public string BatchNo { get; set; }
        public DateTime BestBefore { get; set; }
        public int Quantity { get; set; }
        public string BoxType { get; set; }
        public string BoxBatchCode { get; set; }
        public DateTime StartTime { get; set; }
        public Nullable<decimal> Temprature { get; set; }
        public int TotalCasesOrTrays { get; set; }
        public DateTime FinishTime { get; set; }
        public string UserAdded { get; set; }
        public DateTime DateAdded { get; set; }
    }

    public class PKReporting
    {
        private static List<StockReport> _listStockReport;
        private static List<GoodsInReport> _listGoodsInReport;
        private static List<PackingReport> _listPackingReport;
        private static List<BoxingReport> _listBoxingReport;
        private static List<CookingPlanReport> _listCookingPlanReport;
        private static List<GoodsOutReport> _listGoodsOutReport;

        public static List<StockReport> GetAllStockCount(DateTime fromDate, DateTime toDate)
        {
            _listStockReport = new List<StockReport>();

            var context = new PunjabKitchenContext();
            var paramFromDate = Convert.ToDateTime(fromDate);
            var paramToDate = Convert.ToDateTime(toDate);
            var list = context.Stocks.Where(x => x.Sys_CreatedDateTime >= paramFromDate && x.Sys_CreatedDateTime <= paramToDate).ToList();

            foreach (var item in list)
            {
                _listStockReport.Add(new StockReport
                {
                    BatchNo = item.vBatchNo ?? "",
                    StockItemName = item.tProduct != null ? item.tProduct.vName : "",
                    DateAdded = item.Sys_CreatedDateTime?.ToString() ?? "",
                    Quantity = item.iQuantity?.ToString() ?? "",
                    UserAdded = item.AspNetUser != null ? item.AspNetUser.FirstName + " " + item.AspNetUser.LastName : "",
                    Id = item.iStockId.ToString(),
                    StockType = item.tStockType != null ? item.tStockType.vName : ""
                });
            }

            return _listStockReport;
        }

        public static List<GoodsInReport> GetAllGoodsInCount(DateTime fromDate, DateTime toDate)
        {
            _listGoodsInReport = new List<GoodsInReport>();

            var context = new PunjabKitchenContext();
            var paramFromDate = Convert.ToDateTime(fromDate);
            var paramToDate = Convert.ToDateTime(toDate);
            var list = context.Products.Where(x => x.Sys_CreatedDateTime >= paramFromDate && x.Sys_CreatedDateTime <= paramToDate).ToList();

            foreach (var item in list)
            {
                _listGoodsInReport.Add(new GoodsInReport
                {
                    Id = item.tGoodsIn != null ? item.tGoodsIn.tGoodsInId.ToString() : "",
                    Supplier = item.tGoodsIn != null ? item.tGoodsIn.vSupplier : "",
                    Product = item.vName ?? "",
                    ProductType = item.tGoodsIn != null ? (item.tGoodsIn.tProductType != null ? item.tGoodsIn.tProductType.vName : "") : "",
                    VehicleInternalCheck = item.tGoodsIn != null ? (item.tGoodsIn.bVehicleInternalCheck.ToString()) : "",
                    RawMaterial = item.tGoodsIn != null ? (item.tGoodsIn.bRawMaterial.ToString()) : "",
                    VehicleTemp = item.tGoodsIn != null ? (item.tGoodsIn.bVehicleTemp != null ? item.tGoodsIn.bVehicleTemp.ToString() : "") : "",
                    UserAdded = item.AspNetUser != null ? item.AspNetUser.FirstName + " " + item.AspNetUser.LastName : "",
                    DateAdded = item.Sys_CreatedDateTime?.ToString() ?? ""
                });
            }

            return _listGoodsInReport;
        }

        public static List<PackingReport> GetAllPackingCount(DateTime fromDate, DateTime toDate)
        {
            _listPackingReport = new List<PackingReport>();

            var context = new PunjabKitchenContext();
            var paramFromDate = Convert.ToDateTime(fromDate);
            var paramToDate = Convert.ToDateTime(toDate);
            var list = context.TPackingProductionPlans.Where(x => DbFunctions.TruncateTime(x.Sys_CreatedDateTime) >= paramFromDate.Date && DbFunctions.TruncateTime(x.Sys_CreatedDateTime) <= paramToDate.Date).ToList();

            foreach (var item in list)
            {
                var packingReport = new PackingReport();

                packingReport.Id = item.iPackingProductionPlanId.ToString();
                packingReport.CookingLine = "Line " + item.iLineNo.ToString();
                packingReport.PlanDate = Convert.ToDateTime(item.dPackingPlan);
                packingReport.ProductCode = Convert.ToBoolean(item.bIsTask) ? "" : item.tCookingPlan?.tProduct?.vProductCode;
                packingReport.ProductDesc = Convert.ToBoolean(item.bIsTask) ? item.vTaskDescription : item.tCookingPlan?.tProduct?.vName;
                packingReport.Weight = Convert.ToInt32(item.iWeight);
                packingReport.QtyRequired = Convert.ToInt32(item.iQuantityRequired);
                packingReport.BatchNo = Convert.ToBoolean(item.bIsTask) ? "" : item.tCookingPlan?.vBatchNo;
                packingReport.PacksPerMin = Convert.ToInt32(item.iPacksPerMin);
                packingReport.Runtime = Convert.ToDateTime(item.tRunTime);
                packingReport.PlannedStartTime = Convert.ToDateTime(item.tPlannedStart);
                packingReport.PlannedFinish = Convert.ToDateTime(item.tPlannedFinish);
                var duration = (packingReport.PlannedFinish - packingReport.PlannedStartTime);
                packingReport.Durantion = new DateTime(duration.Ticks);
                packingReport.UserAdded = item.AspNetUser != null ? item.AspNetUser.FirstName + " " + item.AspNetUser.LastName : "";
                packingReport.DateAdded = Convert.ToDateTime(item.Sys_CreatedDateTime);

                _listPackingReport.Add(packingReport);
            }

            return _listPackingReport;
        }

        public static List<BoxingReport> GetAllBoxingCount(DateTime fromDate, DateTime toDate)
        {
            _listBoxingReport = new List<BoxingReport>();

            var context = new PunjabKitchenContext();
            var paramFromDate = Convert.ToDateTime(fromDate);
            var paramToDate = Convert.ToDateTime(toDate);
            var list = context.tBoxingRecord.Where(x => DbFunctions.TruncateTime(x.Sys_CreatedDateTime) >= paramFromDate.Date && DbFunctions.TruncateTime(x.Sys_CreatedDateTime) <= paramToDate.Date).ToList();

            foreach (var item in list)
            {
                var boxingReport = new BoxingReport();

                boxingReport.Id = item.iBoxingRecordId.ToString();
                boxingReport.ProductCode = item.tCookingPlan?.tProduct?.vProductCode;
                boxingReport.ProductName = item.tCookingPlan?.tProduct?.vName;
                boxingReport.BatchNo = item.tCookingPlan?.vBatchNo;
                boxingReport.BestBefore = Convert.ToDateTime(item.dBestBefore);
                boxingReport.Quantity = Convert.ToInt32(item.iQuatity);
                boxingReport.BoxType = item.vBoxType;
                boxingReport.BoxBatchCode = item.vBoxBatchCode;
                boxingReport.StartTime = Convert.ToDateTime(item.tStartTime);
                boxingReport.Temprature = item.vProductTemp;
                boxingReport.TotalCasesOrTrays = Convert.ToInt32(item.iTotalCases_Trays);
                boxingReport.FinishTime = Convert.ToDateTime(item.tFinishTime);
                boxingReport.UserAdded = item.AspNetUser != null ? item.AspNetUser.FirstName + " " + item.AspNetUser.LastName : "";
                boxingReport.DateAdded = Convert.ToDateTime(item.Sys_CreatedDateTime);

                _listBoxingReport.Add(boxingReport);
            }

            return _listBoxingReport;
        }

        public static List<CookingPlanReport> GetAllCookingPlanCount(DateTime fromDate, DateTime toDate)
        {
            _listCookingPlanReport = new List<CookingPlanReport>();

            var context = new PunjabKitchenContext();
            var paramFromDate = Convert.ToDateTime(fromDate);
            var paramToDate = Convert.ToDateTime(toDate);
            var list = context.tCookingPlan.Where(x => DbFunctions.TruncateTime(x.tCookDate) >= paramFromDate.Date && DbFunctions.TruncateTime(x.tCookDate) <= paramToDate.Date).ToList();

            foreach (var item in list)
            {
                _listCookingPlanReport.Add(new CookingPlanReport
                {
                    Id = item.iCookingPlanId.ToString(),
                    isTask = item.bIsTask != false ? "Yes" : "No",
                    CookPot = item.iCookPotNo != null ? item.iCookPotNo.ToString() : "",
                    Recipe = item.tRecipe != null ? (item.tRecipe.vRecipeName != null ? item.tRecipe.vRecipeName : "") : "",
                    EstimatedMeals = item.iEstMeals != null ? item.iEstMeals.ToString() : "",
                    Quantity = item.iQuantity != null ? item.iQuantity.ToString() : "",
                    QuantityCompleted = item.iQuantityCompleted != null ? item.iQuantityCompleted.ToString() : "",
                    PlannedStartTime = item.tPlannedStartTime != null ? item.tPlannedStartTime.Value.ToString("HH:mm") : "",
                    PlannedFinishTime = item.tActualFinishTime != null ? item.tActualFinishTime.Value.ToString("HH:mm") : "",
                    ActualStartTime = item.tActualStartTime != null ? item.tActualStartTime.Value.ToString("HH:mm") : "",
                    ActualFinishTime = item.tActualFinishedTime != null ? item.tActualFinishedTime.Value.ToString("HH:mm") : "",
                    UserAdded = item.AspNetUser != null ? item.AspNetUser.FirstName + " " + item.AspNetUser.LastName : "",
                    CookDate = item.Sys_CreatedDateTime?.ToString() ?? ""
                });
            }

            return _listCookingPlanReport;
        }

        public static List<GoodsOutReport> GetAllGoodsOutCount(DateTime fromDate, DateTime toDate)
        {
            _listGoodsOutReport = new List<GoodsOutReport>();

            var context = new PunjabKitchenContext();
            var paramFromDate = Convert.ToDateTime(fromDate);
            var paramToDate = Convert.ToDateTime(toDate);
            var list = context.tGoodsOutPlanDetail.Where(x => DbFunctions.TruncateTime(x.Sys_CreatedDateTime) >= paramFromDate.Date && DbFunctions.TruncateTime(x.Sys_CreatedDateTime) <= paramToDate.Date).ToList();

            foreach (var item in list)
            {
                _listGoodsOutReport.Add(new GoodsOutReport
                {
                    Id = item.iGoodsOutPlanDetailId.ToString(),
                    BatchCode = item.tGoodsOutPlan != null ? (item.tGoodsOutPlan.vBatchCode != null ? item.tGoodsOutPlan.vBatchCode : "") : "",
                    Category = item.tGoodsOutPlan != null ? (item.tGoodsOutPlan.vCategory != null ? item.tGoodsOutPlan.vCategory : "") : "",
                    CategoryDescription = item.vCategoryDescription != null ? item.vCategoryDescription : "",
                    Product = item.tProduct != null ? (item.tProduct.vName != null ? item.tProduct.vName : "") : "",
                    Quantity = item.vQuantity != null ? item.vQuantity : "",
                    NumberOfMeals = item.iNumberOfMeals != null ? item.iNumberOfMeals.ToString() : "",
                    UserAdded = item.AspNetUser != null ? item.AspNetUser.FirstName + " " + item.AspNetUser.LastName : "",
                    DateAdded = item.Sys_CreatedDateTime?.ToString() ?? ""
                });
            }

            return _listGoodsOutReport;
        }
    }
}