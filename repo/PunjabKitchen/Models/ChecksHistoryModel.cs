﻿using System;

namespace PunjabKitchen.Models
{
    public class ChecksHistoryModel
    {
        public int Id { get; set; }
        public bool IsPerformed{ get; set; }
        public string CreatedBy{ get; set; }
        public DateTime? CreatedDate{ get; set; }
        public string ModifiedBy{ get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string Fridge1Temp { get; set; }
        public string Fridge2Temp { get; set; }
        public string Fridge3Temp { get; set; }
        public string Fridge1Action { get; set; }
        public string Fridge2Action { get; set; }
        public string Fridge3Action { get; set; }
        public string Area { get; set; }
        public string vCheck { get; set; }
        public string vLocation { get; set; }
        public int AreaId { get; set; }
        public DateTime WeekStartDate { get; set; }
        public DateTime WeekEndDate { get; set; }
        public string vTruckType { get; set; }
        public string vTruckMake { get; set; }
        public double iMaxLoad { get; set; }
        public string iPropulsion { get; set; }
        public string vChanges { get; set; }
        public decimal iFreezerTemp1 { get; set; }
        public decimal iFreezerTemp2 { get; set; }
        public decimal iFreezerTemp3 { get; set; }
        public decimal iFreezerTemp4 { get; set; }
        public decimal iFreezerTemp5 { get; set; }
        public string vComments { get; set; }
    }
}