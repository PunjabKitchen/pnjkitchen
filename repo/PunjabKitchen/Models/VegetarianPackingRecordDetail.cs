﻿using System;
using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchen.Models
{
    public class VegetarianPackingRecordDetail
    {
        public int iVegeRecordDetailId { get; set; }
        public Nullable<int> iVegeRecordId { get; set; }
        public string UserId { get; set; }
        public Nullable<System.DateTime> Sys_CreatedDateTime { get; set; }
        public string Sys_CreatedBy { get; set; }
        public Nullable<System.DateTime> Sys_ModifyDateTime { get; set; }
        public string Sys_ModifyBy { get; set; }

        public virtual tVegetarianPackingRecord tVegetarianPackingRecord { get; set; }
    }
}