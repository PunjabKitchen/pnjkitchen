﻿using System;
using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchen.Models
{
    public class PackingRecord2Detail
    {
        public int iPackingRecord2Detail { get; set; }
        public string UserId { get; set; }
        public Nullable<int> iPackingRecordId { get; set; }
        public Nullable<System.DateTime> Sys_CreatedDateTime { get; set; }
        public string Sys_CreatedBy { get; set; }
        public Nullable<System.DateTime> Sys_ModifyDateTime { get; set; }
        public string Sys_ModifyBy { get; set; }

        public virtual tPackingRecord2 tPackingRecord2 { get; set; }
    }
}