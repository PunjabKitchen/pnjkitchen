﻿using PunjabKitchenEntities.EntityModel;
using System;

namespace PunjabKitchen.Models
{
    public class GoodsOutPicklistDetail
    {
        public int iGoodsOutPickListDetailId { get; set; }
        public int? iGoodsOutPickListId { get; set; }
        public string vShelf { get; set; }
        public int? iRecipeId { get; set; }
        public decimal? iQuantity { get; set; }
        public string vBatchNo { get; set; }
        public bool? bOldLabel { get; set; }
        public decimal? iToFollow { get; set; }
        public int? iUnitId { get; set; }
        public DateTime? Sys_CreatedDateTime { get; set; }
        public string Sys_CreatedBy { get; set; }
        public DateTime? Sys_ModifyDateTime { get; set; }
        public string Sys_ModifyBy { get; set; }

        public virtual tRecipe tRecipe { get; set; }
        public virtual tUnit tUnit { get; set; }
    }
}