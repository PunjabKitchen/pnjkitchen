﻿using System;
using System.Collections.Generic;
using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchen.Models
{
    public class PureeMealBatchAssembly
    {
        public int iPureeMeanBatchAssemblyId { get; set; }
        public Nullable<System.DateTime> dDate { get; set; }
        public Nullable<int> iCookingPlanId { get; set; }
        public string vProductName { get; set; }
        public Nullable<System.DateTime> tLastPackTimeInFreezer { get; set; }
        public string vAssembledBy { get; set; }
        public Nullable<bool> bProductReleased { get; set; }
        public Nullable<bool> bProductMeatsQA { get; set; }
        public Nullable<bool> bProductMeatsPackingQuality { get; set; }
        public Nullable<bool> bProductMeatsSpecs { get; set; }
        public Nullable<int> iProductReleaseAuthBy { get; set; }
        public Nullable<int> iNumberOfPacksInBlastFreezer { get; set; }
        public string vComments { get; set; }
        public Nullable<bool> bEnabled { get; set; }
        public Nullable<bool> bDeleted { get; set; }
        public Nullable<System.DateTime> Sys_CreatedDateTime { get; set; }
        public string Sys_CreatedBy { get; set; }
        public Nullable<System.DateTime> Sys_ModifyDateTime { get; set; }
        public string Sys_ModifyBy { get; set; }

        public virtual tCookingPlan tCookingPlan { get; set; }
        public string packersList { get; set; }

        public virtual ICollection<tPureeMealBatchAssemblyPackerDetail> tPureeMealBatchAssemblyPackerDetails { get; set; }
        public virtual ICollection<tPureeMealInBlastFreezer> tPureeMealInBlastFreezers { get; set; }
    }
}