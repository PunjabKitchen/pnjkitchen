﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PunjabKitchen.Models
{
    public class StockPointSelectList
    {
        public int iStockId { get; set; }
        public string vStoragePoint { get; set; }
    }
}