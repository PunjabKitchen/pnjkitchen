﻿using System;

namespace PunjabKitchen.Models
{
    public class EventModel
    {
        public long EventId { get; set; }
        public DateTime InsertedDate { get; set; }
        public DateTime? LastUpdatedDate { get; set; }
        public string Data { get; set; }
    }
}