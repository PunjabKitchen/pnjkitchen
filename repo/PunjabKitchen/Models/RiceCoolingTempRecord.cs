﻿using PunjabKitchenEntities.EntityModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PunjabKitchen.Models
{
    public class RiceCoolingTempRecord
    {
        public int iRiceCoolingTempRecordId { get; set; }
        public Nullable<int> iProductId { get; set; }
        public Nullable<int> iQuantity { get; set; }
        public string vBatchNo { get; set; }
        public Nullable<System.DateTime> tTimeExCook { get; set; }
        public string vTempAfterWaterRinse { get; set; }
        public Nullable<System.DateTime> tTimeToWIPChiller { get; set; }
        public string vComments { get; set; }
        public string vSign { get; set; }
        public string vManagerSign { get; set; }
        public Nullable<bool> bEnabled { get; set; }
        public Nullable<bool> bDeleted { get; set; }
        public Nullable<int> iCookingPlanId { get; set; }
        public Nullable<int> iUnitId { get; set; }
        public Nullable<System.DateTime> Sys_CreatedDateTime { get; set; }
        public string Sys_CreatedBy { get; set; }
        public Nullable<System.DateTime> Sys_ModifyDateTime { get; set; }
        public string Sys_ModifyBy { get; set; }
        public Nullable<bool> bCriticalCheck { get; set; }

        public virtual AspNetUser AspNetUser { get; set; }
        public virtual AspNetUser AspNetUser1 { get; set; }
        public virtual tCookingPlan tCookingPlan { get; set; }
        public virtual tUnit tUnit { get; set; }
    }
}