﻿using System;
using System.Collections.Generic;
using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchen.Models
{
    public class PortionWeightCheck
    {
        public int iPortionWeightCheckId { get; set; }
        public Nullable<System.DateTime> dDate { get; set; }
        public string vLocation { get; set; }
        public Nullable<System.DateTime> tCheckTime { get; set; }
        public Nullable<int> iCookingPlanId { get; set; }
        public Nullable<int> iProductWeight { get; set; }
        public string vSign { get; set; }
        public string vManagementSign { get; set; }
        public Nullable<bool> bEnabled { get; set; }
        public Nullable<bool> bDeleted { get; set; }
        public Nullable<System.DateTime> Sys_CreatedDateTime { get; set; }
        public string Sys_CreatedBy { get; set; }
        public Nullable<System.DateTime> Sys_ModifyDateTime { get; set; }
        public string Sys_ModifyBy { get; set; }
        public string vBatchNo { get; set; }

        public virtual AspNetUser AspNetUser { get; set; }
        public virtual AspNetUser AspNetUser1 { get; set; }
        public virtual tCookingPlan tCookingPlan { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tPortionWeightCheckDetail> tPortionWeightCheckDetails { get; set; }
    }
}