﻿using System;

namespace PunjabKitchen.Models
{
    public class StartupCheckList
    {
        public int iStartUpCheckListId { get; set; }        
        public string vCheck { get; set; }
        public bool? bIsBool { get; set; }
        public DateTime? Sys_CreatedDateTime { get; set; }
        public string Sys_CreatedBy { get; set; }
        public DateTime? Sys_ModifyDateTime { get; set; }
        public string Sys_ModifyBy { get; set; } 
    }
}