﻿using System;
using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchen.Models
{
    public class StartupChecksEPGoodsOut
    {
        public int iStartupChecksEPGoodsOut { get; set; }
        public Nullable<System.DateTime> dDateTime { get; set; }
        public Nullable<bool> bSwitchLightsOn { get; set; }
        public Nullable<bool> bCheckElectric { get; set; }
        public Nullable<bool> bSwitchOnComputer { get; set; }
        public Nullable<bool> bCheckBlade { get; set; }
        public Nullable<bool> bCheckHoldingFreezers { get; set; }
        public Nullable<bool> bCheckDisplayTemperature { get; set; }
        public Nullable<bool> bRingOffice { get; set; }
        public Nullable<bool> bCollectOrder { get; set; }
        public Nullable<bool> bCheckAllStaffComply { get; set; }
        public Nullable<bool> bCheckAllStaffSuitably { get; set; }
        public Nullable<System.DateTime> Sys_CreatedDateTime { get; set; }
        public string Sys_CreatedBy { get; set; }
        public Nullable<System.DateTime> Sys_ModifyDateTime { get; set; }
        public string Sys_ModifyBy { get; set; }

        public virtual AspNetUser AspNetUser { get; set; }
        public virtual AspNetUser AspNetUser1 { get; set; }
    }
}