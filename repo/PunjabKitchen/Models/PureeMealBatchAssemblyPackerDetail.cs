﻿using System;
using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchen.Models
{
    public class PureeMealBatchAssemblyPackerDetail
    {
        public int iPureeMealBatchAssemblyPackerDetailId { get; set; }
        public Nullable<int> iPureeMeanBatchAssemblyId { get; set; }
        public string UserId { get; set; }
        public Nullable<System.DateTime> Sys_CreatedDateTime { get; set; }
        public string Sys_CreatedBy { get; set; }
        public Nullable<System.DateTime> Sys_ModifyDateTime { get; set; }
        public string Sys_ModifyBy { get; set; }

        public virtual tPureeMeanBatchAssembly tPureeMeanBatchAssembly { get; set; }
    }
}