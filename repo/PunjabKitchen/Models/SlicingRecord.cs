﻿using PunjabKitchenEntities.EntityModel;
using System;

namespace PunjabKitchen.Models
{
    public class SlicingRecord
    {

        public int iSlicingRecordId { get; set; }
        public Nullable<int> iProductId { get; set; }
        public string vBatchNo { get; set; }
        public Nullable<int> iTrolleyNo { get; set; }
        public Nullable<System.DateTime> tStartTime { get; set; }
        public string vTempProductMaxStart { get; set; }
        public Nullable<System.DateTime> tFinishTime { get; set; }
        public string vTempProductMaxEnd { get; set; }
        public string vSign { get; set; }
        public string vManagerSign { get; set; }
        public Nullable<int> iCookingPlanId { get; set; }
        public Nullable<System.DateTime> Sys_CreatedDateTime { get; set; }
        public string Sys_CreatedBy { get; set; }
        public Nullable<System.DateTime> Sys_ModifyDateTime { get; set; }
        public string Sys_ModifyBy { get; set; }
        public Nullable<bool> bCriticalCheck { get; set; }

        public virtual AspNetUser AspNetUser { get; set; }
        public virtual AspNetUser AspNetUser1 { get; set; }
        public virtual tCookingPlan tCookingPlan { get; set; }
    }
}