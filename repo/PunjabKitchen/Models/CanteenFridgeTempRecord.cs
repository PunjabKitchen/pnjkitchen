﻿using System;

namespace PunjabKitchen.Models
{
    public class CanteenFridgeTempRecord
    {
        public int iCanteenFridgeTempRecordId { get; set; }
        public DateTime? dDateTime { get; set; }
        public string Fridge1Temp { get; set; }
        public string Fridge2Temp { get; set; }
        public string Fridge3Temp { get; set; }
        public string vFridge1Action { get; set; }
        public string vFridge2Action { get; set; }
        public string vFridge3Action { get; set; }        
        public DateTime? Sys_CreatedDateTime { get; set; }
        public string Sys_CreatedBy { get; set; }
        public DateTime? Sys_ModifyDateTime { get; set; }
        public string Sys_ModifyBy { get; set; } 
    }
}