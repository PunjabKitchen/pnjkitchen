﻿using System;
using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchen.Models
{
    public class PureeMealInBlastFreezer
    {
        public int iPureeMealInBlastFreezerId { get; set; }
        public Nullable<int> iPureeMeanBatchAssemblyId { get; set; }
        public Nullable<System.DateTime> tStartTime { get; set; }
        public Nullable<System.DateTime> tLastPackTimeInFreezer { get; set; }
        public Nullable<int> iNumberOfPacks { get; set; }
        public Nullable<bool> bEnabled { get; set; }
        public Nullable<bool> bDeleted { get; set; }
        public Nullable<System.DateTime> Sys_CreatedDateTime { get; set; }
        public string Sys_CreatedBy { get; set; }
        public Nullable<System.DateTime> Sys_ModifyDateTime { get; set; }
        public string Sys_ModifyBy { get; set; }

        public virtual tPureeMeanBatchAssembly tPureeMeanBatchAssembly { get; set; }
    }
}