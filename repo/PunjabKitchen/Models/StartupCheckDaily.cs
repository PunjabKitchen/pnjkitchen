﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PunjabKitchen.Models
{
    public class StartupCheckDaily
    {
        public int iStartUpcheckDailyId { get; set; }
        public int iStartUpCheckListId { get; set; }
        public DateTime? dDateTime { get; set; }
        public Nullable<bool> bIsChecked { get; set; }
        public DateTime? Sys_CreatedDateTime { get; set; }
        public string Sys_CreatedBy { get; set; }
        public DateTime? Sys_ModifyDateTime { get; set; }
        public string Sys_ModifyBy { get; set; }
        public string vValue { get; set; }
    }
}