﻿using PunjabKitchenEntities.EntityModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PunjabKitchen.Models
{
    public class MeatMincingRecord
    {
        public int iMeatMincingRecordId { get; set; }
        public Nullable<System.DateTime> tStartTime { get; set; }
        public string vPreUseCleanlinessCheckSign { get; set; }
        public string vBatchNo { get; set; }
        public Nullable<int> iUseByUserId { get; set; }
        public Nullable<System.DateTime> dBestBefore { get; set; }
        public string vMeat { get; set; }
        public string vPurpose { get; set; }
        public Nullable<System.DateTime> tFinishTime { get; set; }
        public Nullable<int> iMincirCleanedByUserId { get; set; }
        public string vPostCleanCheckSupervisorSign { get; set; }
        public string vManagerSign { get; set; }
        public Nullable<int> iCookingPlanId { get; set; }
        public Nullable<System.DateTime> Sys_CreatedDateTime { get; set; }
        public string Sys_CreatedBy { get; set; }
        public Nullable<System.DateTime> Sys_ModifyDateTime { get; set; }
        public string Sys_ModifyBy { get; set; }

        public virtual AspNetUser AspNetUser { get; set; }
        public virtual AspNetUser AspNetUser1 { get; set; }
        public virtual tCookingPlan tCookingPlan { get; set; }
    }
}