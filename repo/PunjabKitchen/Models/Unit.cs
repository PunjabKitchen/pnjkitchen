﻿using System;

namespace PunjabKitchen.Models
{
    public class Unit
    {
        public int iUnitId { get; set; }
        public string vName { get; set; }
        public string vDescription { get; set; }
        public DateTime? Sys_CreatedDateTime { get; set; }
        public string Sys_CreatedBy { get; set; }
        public DateTime? Sys_ModifyDateTime { get; set; }
        public string Sys_ModifyBy { get; set; }
    }
}