﻿using System;
using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchen.Models
{
    public class MixedCaseBoxingRecordDetail
    {
        public int iMixedCaseBoxingRecordDetailId { get; set; }
        public Nullable<int> iMixedCaseBoxingRecordId { get; set; }
        public Nullable<int> iProductId { get; set; }
        public string vBatchCode { get; set; }
        public string vNameOfPack { get; set; }
        public string vNumber { get; set; }
        public Nullable<bool> bEnabled { get; set; }
        public Nullable<bool> bDeleted { get; set; }
        public Nullable<System.DateTime> Sys_CreatedDateTime { get; set; }
        public string Sys_CreatedBy { get; set; }
        public Nullable<System.DateTime> Sys_ModifyDateTime { get; set; }
        public string Sys_ModifyBy { get; set; }

        public virtual tMixedCaseBoxingRecord tMixedCaseBoxingRecord { get; set; }
        public virtual tProduct tProduct { get; set; }
    }
}