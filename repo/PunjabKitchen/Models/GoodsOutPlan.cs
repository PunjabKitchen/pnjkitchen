﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchen.Models
{
    public class GoodsOutPlan
    {
        public int iGoodsOutPlanId { get; set; }
        public string vBatchCode { get; set; }
        public DateTime? dDate { get; set; }
        public string vCategory { get; set; }
        public DateTime? Sys_CreatedDateTime { get; set; }
        public string Sys_CreatedBy { get; set; }
        public DateTime? Sys_ModifyDateTime { get; set; }
        public string Sys_ModifyBy { get; set; }

        public virtual List<GoodsOutPlanDetail> tGoodsOutPlanDetails { get; set; }
    }
}