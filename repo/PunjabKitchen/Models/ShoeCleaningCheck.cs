﻿using System;
 

namespace PunjabKitchen.Models
{
    public class ShoeCleaningCheck
    {
        
        public int iShoeCleaningRecordId { get; set; }
        public string iUserId { get; set; }
        public DateTime? dCheckPerformDate { get; set; }
        public DateTime? Sys_CreatedDateTime { get; set; }
        public string Sys_CreatedBy { get; set; }
        public DateTime? Sys_ModifyDateTime { get; set; }
        public string Sys_ModifyBy { get; set; } 
    }
}