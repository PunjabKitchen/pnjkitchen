﻿using System;

namespace PunjabKitchen.Models
{
    public class PureeShapeStarchRecord
    {
        public int iPureeShapeStarchRecordId { get; set; }
        public string vUltraTex2BatchNo { get; set; }
        public string vMetaloseBatchNo { get; set; }
        public string vPurityWBatchNo { get; set; }
        public Nullable<int> iProductAmount { get; set; }
        public Nullable<int> iUltraTex2 { get; set; }
        public Nullable<int> iMetalose { get; set; }
        public Nullable<int> iPurityW { get; set; }
        public string vTotal { get; set; }
        public Nullable<bool> bEnabled { get; set; }
        public Nullable<bool> bDeleted { get; set; }
        public Nullable<System.DateTime> Sys_CreatedDateTime { get; set; }
        public string Sys_CreatedBy { get; set; }
        public Nullable<System.DateTime> Sys_ModifyDateTime { get; set; }
        public string Sys_ModifyBy { get; set; }
        public string vComments { get; set; }
        public string vManagerSign { get; set; }
        public string vSign { get; set; }
        public Nullable<System.DateTime> dDate { get; set; }
        public string vProductName { get; set; }
    }
}