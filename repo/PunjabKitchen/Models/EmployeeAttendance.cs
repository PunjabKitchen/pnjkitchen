﻿using System;
using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchen.Models
{
    public class EmployeeAttendance
    {
        public string iEmployeeId { get; set; }
        public DateTime? dClockInTime { get; set; }
        public DateTime? dClockOutTime { get; set; }
        public DateTime? Sys_CreatedDateTime { get; set; }
        public string Sys_CreatedBy { get; set; }
        public DateTime? Sys_ModifyDateTime { get; set; }
        public string Sys_ModifyBy { get; set; }
        public System.DateTime dDateTime { get; set; }
        public Nullable<System.DateTime> dLunchClockInTime { get; set; }
        public Nullable<System.DateTime> dLunchClockOutTime { get; set; }

        public string EmployeeName { get; set; }
        public virtual AspNetUser AspNetUser { get; set; }
        public virtual AspNetUser AspNetUser1 { get; set; }
        public virtual AspNetUser AspNetUser2 { get; set; }
    }
}