﻿using System;

namespace PunjabKitchen.Models
{
    public class Stock
    {
        public int iStockId { get; set; }
        public Nullable<int> iStockTypeId { get; set; }
        public string vStorageLocation { get; set; }
        public string vStoragePoint { get; set; }
        public Nullable<int> iProductId { get; set; }
        public string vBatchNo { get; set; }
        public Nullable<int> iQuantity { get; set; }
        public Nullable<int> iInspectedQuantity { get; set; }
        public Nullable<bool> bQuantityShiftCheck { get; set; }
        public Nullable<int> iUnitId { get; set; }
        public string vProcess { get; set; }
        public Nullable<int> iRefrenceId { get; set; }
        public Nullable<System.DateTime> dLastChecked { get; set; }
        public string vComments { get; set; }
        public Nullable<System.DateTime> Sys_CreatedDateTime { get; set; }
        public string Sys_CreatedBy { get; set; }
        public Nullable<System.DateTime> Sys_ModifyDateTime { get; set; }
        public string Sys_ModifyBy { get; set; }

        public virtual Product tProduct { get; set; }
        public virtual StockType tStockType { get; set; }
        //public virtual tUnit tUnit { get; set; }
    }
}