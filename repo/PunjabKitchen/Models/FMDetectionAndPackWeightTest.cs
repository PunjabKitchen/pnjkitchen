﻿using System;
using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchen.Models
{
    public class FMDetectionAndPackWeightTest
    {
        public int iDetectionPackWeightTestId { get; set; }
        public Nullable<System.DateTime> dDate { get; set; }
        public Nullable<int> iCookingPlanId { get; set; }
        public Nullable<System.DateTime> tTime { get; set; }
        public Nullable<bool> bFerrous2_0mmPass { get; set; }
        public Nullable<bool> bNonFerrous2_5mmPass { get; set; }
        public Nullable<bool> bStainlessSteel4_5mmPass { get; set; }
        public Nullable<bool> bRejectMechnismWorking { get; set; }
        public string vTestBySign { get; set; }
        public Nullable<decimal> iAveragePackWeight { get; set; }
        public Nullable<int> iMinimumpackWeight { get; set; }
        public Nullable<int> iNominalPackWeight { get; set; }
        public string vSign { get; set; }
        public string vManagerSign { get; set; }
        public string vTestType { get; set; }
        public Nullable<bool> bFerrous2_8mmPass { get; set; }
        public Nullable<bool> bBeltStopWorking { get; set; }
        public Nullable<int> iWeightOfPacks1 { get; set; }
        public Nullable<int> iWeightOfPacks2 { get; set; }
        public Nullable<int> iWeightOfPacks3 { get; set; }
        public Nullable<int> iWeightOfPacks4 { get; set; }
        public Nullable<int> iWeightOfPacks5 { get; set; }
        public Nullable<int> iWeightOfPacks6 { get; set; }
        public Nullable<int> iWeightOfPacks7 { get; set; }
        public Nullable<int> iWeightOfPacks8 { get; set; }
        public Nullable<int> iWeightOfPacks9 { get; set; }
        public Nullable<int> iWeightOfPacks10 { get; set; }
        public Nullable<System.DateTime> Sys_CreatedDateTime { get; set; }
        public string Sys_CreatedBy { get; set; }
        public Nullable<System.DateTime> Sys_ModifyDateTime { get; set; }
        public string Sys_ModifyBy { get; set; }
        public string vLocation { get; set; }
        public Nullable<int> iProductWeight { get; set; }
        public string vBatchNo { get; set; }
        public Nullable<bool> bFerrous1_8mmPass { get; set; }
        public Nullable<bool> bNonFerrous3_0mmPass { get; set; }
        public Nullable<bool> bStainlessSteel4_0mmPass { get; set; }

        public virtual tCookingPlan tCookingPlan { get; set; }
    }
}