﻿using System;
using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchen.Models
{
    public class ShapesControlRecordBatchDetail
    {
        public int iShapesControlRecordBatchDetailId { get; set; }
        public Nullable<int> iShapeControlRecordId { get; set; }
        public string vBatchCode { get; set; }
        public Nullable<System.DateTime> Sys_CreatedDateTime { get; set; }
        public string Sys_CreatedBy { get; set; }
        public Nullable<System.DateTime> Sys_ModifyDateTime { get; set; }
        public string Sys_ModifyBy { get; set; }

        public virtual tShapeControlRecord tShapeControlRecord { get; set; }
    }
}