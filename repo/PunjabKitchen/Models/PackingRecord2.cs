﻿using System;
using System.Collections.Generic;
using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchen.Models
{
    public class PackingRecord2
    {
        public int iPackingRecordId { get; set; }
        public Nullable<System.DateTime> dDate { get; set; }
        public Nullable<int> iCookingPlanId { get; set; }
        public string vBatchNo { get; set; }
        public Nullable<int> iLineNo { get; set; }
        public Nullable<System.DateTime> tStartTime { get; set; }
        public Nullable<int> iQuantityMade { get; set; }
        public Nullable<System.DateTime> tFinishTime { get; set; }
        public Nullable<bool> bEnabled { get; set; }
        public Nullable<bool> bDeleted { get; set; }
        public Nullable<System.DateTime> Sys_CreatedDateTime { get; set; }
        public string Sys_CreatedBy { get; set; }
        public Nullable<System.DateTime> Sys_ModifyDateTime { get; set; }
        public string Sys_ModifyBy { get; set; }
        public Nullable<int> iUnitId { get; set; }
        public Nullable<int> iPackerUserId { get; set; }
        public string packersList { set; get; }

        public virtual tCookingPlan tCookingPlan { get; set; }
        public virtual tUnit tUnit { get; set; }
        public virtual ICollection<tPackingRecord2Detail> tPackingRecord2Detail { get; set; }
    }
}