﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PunjabKitchen.Models
{
    public class CleaningAreas
    {
        public int iAreaId { get; set; }
        public string vAreaName { get; set; }
        public DateTime? Sys_CreatedDateTime { get; set; }
        public string Sys_CreatedBy { get; set; }
        public DateTime? Sys_ModifyDateTime { get; set; }
        public string Sys_ModifyBy { get; set; }
        //Screen Specific Properties
        public bool IsPerformed { get; set; }
        public string PerformedBy { get; set; }
        public DateTime PerformedDateTime { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedTime { get; set; }
    }
}