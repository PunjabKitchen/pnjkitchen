﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PunjabKitchen.Models
{
    public class ShapesControlRecordWithDetail
    {
        public ShapesControlRecord ShapesControl { get; set; }
        public ShapesControlRecordDetail ShapesControlDetail { get; set; }
    }
}