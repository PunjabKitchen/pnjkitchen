﻿using PunjabKitchenEntities.EntityModel;
using System;
using System.Collections.Generic;

namespace PunjabKitchen.Models
{
    public class GoodsOutPicklist
    {
        public int iGoodsOutPickListId { get; set; }
        public int? iStockId { get; set; }
        public string vCustomerCode { get; set; }
        public string vCustomer { get; set; }
        public int? iOrderNumber { get; set; }
        public string cPriority { get; set; }
        public DateTime? dDeliveryDate { get; set; }
        public string vDeliverTo { get; set; }
        public DateTime? Sys_CreatedDateTime { get; set; }
        public string Sys_CreatedBy { get; set; }
        public DateTime? Sys_ModifyDateTime { get; set; }
        public string Sys_ModifyBy { get; set; }

        public string Sys_ModifyBy_Name { get; set; }

        public virtual List<tGoodsOutPicklistDetail> tGoodsOutPicklistDetails { get; set; }
        public virtual tStock tStock { get; set; }

        //Detail
        public int detail_iGoodsOutPickListDetailId { get; set; }
        public string detail_vShelf { get; set; }
        public Nullable<int> detail_iRecipeId { get; set; }
        public string detail_vItemCode { get; set; }
        public string detail_vItemName { get; set; }
        public Nullable<decimal> detail_iQuantity { get; set; }
        public string detail_vBatchNo { get; set; }
        public Nullable<bool> detail_bOldLabel { get; set; }
        public Nullable<decimal> detail_iToFollow { get; set; }
        public Nullable<int> detail_iUnitId { get; set; }
        public string detail_vUnitName { get; set; }
    }
}