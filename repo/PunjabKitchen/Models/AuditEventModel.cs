﻿using System;
using System.Collections.Generic;
using Audit.Core;

namespace PunjabKitchen.Models
{
    public class AuditEventModel
    {
        public int EventId { get; set; }
        public AuditEventEnvironment Environment { get; set; }
        public string EventType { get; set; }
        public Dictionary<string, object> CustomFields { get; set; }
        public AuditTarget Target { get; set; }
        public List<string> Comments { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int Duration { get; set; }
        public string User { get; set; }
    }
}