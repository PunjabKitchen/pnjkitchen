﻿using System;
using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchen.Models
{
    public class FreezerRecord
    {
        public int iFreezerRecordId { get; set; }
        public DateTime? dDate { get; set; }
        public string vGoodsInFreezerTempAm { get; set; }
        public string vGoodsInFreezerTempPm { get; set; }
        public DateTime? tGoodsInFreezerTimeAm { get; set; }
        public DateTime? tGoodsInFreezerTimePm { get; set; }
        public string vShapesFreezerTempAm { get; set; }
        public string vShapesFreezerTempPm { get; set; }
        public DateTime? tShapesFreezerTimeAm { get; set; }
        public DateTime? tShapesFreezerTimePm { get; set; }
        public string vCarParkFreezerTempAm { get; set; }
        public string vCarParkFreezerTempPm { get; set; }
        public DateTime? tCarParkFreezerTimeAm { get; set; }
        public DateTime? tCarParkFreezerTimePm { get; set; }
        public DateTime? Sys_CreatedDateTime { get; set; }
        public string Sys_CreatedBy { get; set; }
        public DateTime? Sys_ModifyDateTime { get; set; }
        public string Sys_ModifyBy { get; set; }
        public string vComment { get; set; }
        public virtual AspNetUser AspNetUser { get; set; }
        public virtual AspNetUser AspNetUser1 { get; set; }
    }
}