﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Web.Mvc;
// ReSharper disable All

namespace PunjabKitchen.Models
{
    #region Packing Lines
    public enum PackingLines
    {
        None,
        [Description("Line 1")]
        Line_1,
        [Description("Line 2")]
        Line_2,
        [Description("Line 3")]
        Line_3
    };
    #endregion

    public static class PackingLinesEnum
    {
        #region Public: Get Packing Lines List
        /// <summary>
        /// Public: Get Packing Lines List
        /// </summary>
        /// <returns>List<SelectListItem></returns>
        public static List<SelectListItem> GetPackingLines()
        {
            var packingLinesEnumsList = Enum.GetValues(typeof(PackingLines)).Cast<PackingLines>().ToList();

            List<SelectListItem> packingLinesList = new List<SelectListItem>();

            for (int i = 1; i < packingLinesEnumsList.Count; i++)
            {
                packingLinesList.Add(new SelectListItem() { Text = EnumHelper.GetDescription((PackingLines)i), Value = i.ToString() });
            }

            return packingLinesList;
        }

        private class EnumHelper
        {
            public static string GetDescription(Enum @enum)
            {
                if (@enum == null)
                    return null;

                string description = @enum.ToString();

                try
                {
                    FieldInfo fi = @enum.GetType().GetField(@enum.ToString());

                    DescriptionAttribute[] attributes = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);

                    if (attributes.Length > 0)
                        description = attributes[0].Description;
                }
                // ReSharper disable once EmptyGeneralCatchClause
                catch
                {
                }

                return description;
            }
        }
        #endregion
    }
}