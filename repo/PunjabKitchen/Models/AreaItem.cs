﻿using System;
 

namespace PunjabKitchen.Models
{
    public class AreaItem
    {
        public int iAreaItemId { get; set; }
        public int iAreaId { get; set; }
        public string vItem { get; set; }
        public string vProcRef { get; set; }
        public DateTime? Sys_CreatedDateTime { get; set; }
        public string Sys_CreatedBy { get; set; }
        public DateTime? Sys_ModifyDateTime { get; set; }
        public string Sys_ModifyBy { get; set; } 
    }
}