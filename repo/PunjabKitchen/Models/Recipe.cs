﻿using System;
using System.Collections.Generic;
using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchen.Models
{
    public class Recipe
    {
        public Recipe()
        {
            this.tBatchChangeChkPureeRecrds = new HashSet<tBatchChangeChkPureeRecrd>();
            this.tBatchCoolingTempRecrds = new HashSet<tBatchCoolingTempRecrd>();
            this.tBoxingRecords = new HashSet<tBoxingRecord>();
            this.tColdDesertPackingRecords = new HashSet<tColdDesertPackingRecord>();
            this.tCookingPlans = new HashSet<tCookingPlan>();
            this.tFMDetectionAndPackWeightTests = new HashSet<tFMDetectionAndPackWeightTest>();
            this.tHotPackingRercords = new HashSet<tHotPackingRercord>();
            this.tIngredients = new HashSet<tIngredient>();
            this.tPackingProductionPlans = new HashSet<tPackingProductionPlan>();
            this.tPackingRecord2 = new HashSet<tPackingRecord2>();
            this.tPortionWeightChecks = new HashSet<tPortionWeightCheck>();
            this.tProductBatchAssemblyRecords = new HashSet<tProductBatchAssemblyRecord>();
        }

        public int iRecipeId { get; set; }
        public Nullable<int> iProductId { get; set; }
        public Nullable<int> iIngredientsId { get; set; }
        public string vInstructions { get; set; }
        public string vRecipeCode { get; set; }
        public string vRecipeName { get; set; }
        public string vResipeDescription { get; set; }
        public Nullable<System.DateTime> Sys_CreatedDateTime { get; set; }
        public string Sys_CreatedBy { get; set; }
        public Nullable<System.DateTime> Sys_ModifyDateTime { get; set; }
        public string Sys_ModifyBy { get; set; }
        public Nullable<System.DateTime> dBestBeforeDate { get; set; }

        public virtual AspNetUser AspNetUser { get; set; }
        public virtual AspNetUser AspNetUser1 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tBatchChangeChkPureeRecrd> tBatchChangeChkPureeRecrds { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tBatchCoolingTempRecrd> tBatchCoolingTempRecrds { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tBoxingRecord> tBoxingRecords { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tColdDesertPackingRecord> tColdDesertPackingRecords { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tCookingPlan> tCookingPlans { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tFMDetectionAndPackWeightTest> tFMDetectionAndPackWeightTests { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tHotPackingRercord> tHotPackingRercords { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tIngredient> tIngredients { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tPackingProductionPlan> tPackingProductionPlans { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tPackingRecord2> tPackingRecord2 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tPortionWeightCheck> tPortionWeightChecks { get; set; }
        public virtual tProduct tProduct { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tProductBatchAssemblyRecord> tProductBatchAssemblyRecords { get; set; }


    }
}