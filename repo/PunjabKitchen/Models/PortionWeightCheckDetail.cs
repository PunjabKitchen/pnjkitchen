﻿using System;
using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchen.Models
{
    public class PortionWeightCheckDetail
    {
        public int iPortionWeightCheckDetailId { get; set; }
        public int iPortionWeightCheckId { get; set; }
        public string vPortionName { get; set; }
        public Nullable<int> iPortionWeight { get; set; }
        public Nullable<int> iPortionCheckWeight { get; set; }
        public Nullable<System.DateTime> Sys_CreatedDateTime { get; set; }
        public string Sys_CreatedBy { get; set; }
        public Nullable<System.DateTime> Sys_ModifyDateTime { get; set; }
        public string Sys_ModifyBy { get; set; }

        public virtual AspNetUser AspNetUser { get; set; }
        public virtual AspNetUser AspNetUser1 { get; set; }
        public virtual tPortionWeightCheck tPortionWeightCheck { get; set; }
    }
}