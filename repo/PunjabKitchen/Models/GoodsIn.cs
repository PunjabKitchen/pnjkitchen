﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchen.Models
{
    public class GoodsIn
    {
        public int tGoodsInId { get; set; }
        public int? iOrderId { get; set; }
        public string vSignature { get; set; }
        public bool? bEnabled { get; set; }
        public bool? bDeleted { get; set; }
        public DateTime? Sys_CreatedDateTime { get; set; }
        public string Sys_CreatedBy { get; set; }
        public DateTime? Sys_ModifyDateTime { get; set; }
        public string Sys_ModifyBy { get; set; }
        public int? iProductTypeId { get; set; }
        public bool bVehicleInternalCheck { get; set; }
        public bool bRawMaterial { get; set; }
        public int? bVehicleTemp { get; set; }
        public DateTime? dCheckDateTime { get; set; }
        [Required]
        [Display(Name = "Supplier Name")]
        public string vSupplier { get; set; }
        public string vVehicleInternalCheckComment { get; set; }
        public string vRawMaterialComment { get; set; }
        public Nullable<decimal> ccp1a_temp { get; set; }
        public string ccp1a_comment { get; set; }
        public Nullable<decimal> ccp1b_temp { get; set; }
        public string ccp1b_comment { get; set; }
        public Nullable<bool> ccp1a { get; set; }
        public Nullable<bool> ccp1b { get; set; }
        public Nullable<bool> bVehicleChecksAccepted { get; set; }
        public Nullable<bool> bGoodsIntakeFinished { get; set; }

        public virtual ProductType tProductType { get; set; }
        public virtual ICollection<Product> tProducts { get; set; }
        public virtual AspNetUser AspNetUser { get; set; }
    }
}