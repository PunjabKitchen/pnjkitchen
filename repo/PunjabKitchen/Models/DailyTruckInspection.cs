﻿using System;
using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchen.Models
{
    public class DailyTruckInspection
    {
        public int iTruckInspectionId { get; set; }
        public DateTime? dDateTime { get; set; }
        public string vTruckType { get; set; }
        public string vTruckNumber { get; set; }
        public string vTruckMake { get; set; }
        public int? iMaxLoad { get; set; }
        public string iPropulsion { get; set; }
        public string vChanges { get; set; }
        public bool bForkArms { get; set; }
        public bool? bMast { get; set; }
        public bool? bCarriagePlate { get; set; }
        public bool? bLiftChainAndLubrication { get; set; }
        public bool? bWheelAndTyres { get; set; }
        public bool? bBackRest { get; set; }
        public bool? bSeatAndSeatBelt { get; set; }
        public bool? bSteering { get; set; }
        public bool? bLightAndIndicators { get; set; }
        public bool? bHornAndBeeper { get; set; }
        public bool? bMastController { get; set; }
        public bool? bHandAndParkingBreak { get; set; }
        public bool? bDrivingAndServiceBreak { get; set; }
        public bool? bHydraulics { get; set; }
        public bool? bOilLevel { get; set; }
        public bool? bFuelAndPower { get; set; }
        public bool? bBatteries { get; set; }
        public bool? bLPG { get; set; }
        public string vActionTaken { get; set; }
        public DateTime? Sys_CreatedDateTime { get; set; }
        public string Sys_CreatedBy { get; set; }
        public DateTime? Sys_ModifyDateTime { get; set; }
        public string Sys_ModifyBy { get; set; }

        public virtual AspNetUser AspNetUser { get; set; }
        public virtual AspNetUser AspNetUser1 { get; set; }
    }
}