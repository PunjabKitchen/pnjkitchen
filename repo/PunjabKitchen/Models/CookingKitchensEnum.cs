﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Web.Mvc;
// ReSharper disable All

namespace PunjabKitchen.Models
{

    #region Cooking Kitchens
    public enum CookingKitchens
    {
        None,
        [Description("Cookpot 1")]
        Cookpot_1,
        [Description("Cookpot 2")]
        Cookpot_2,
        [Description("Cookpot 3")]
        Cookpot_3
    };
    #endregion

    public static class CookingKitchensEnum
    {
        #region Public: Get Cooking Kitchens List
        /// <summary>
        /// Public: Get Cooking Kitchens List
        /// </summary>
        /// <returns>List<SelectListItem></returns>
        public static List<SelectListItem> GetCookingKitchens()
        {
            var cookingKitchensEnumsList = Enum.GetValues(typeof(CookingKitchens)).Cast<CookingKitchens>().ToList();

            List<SelectListItem> cookingKitchensList = new List<SelectListItem>();

            for (int i = 1; i < cookingKitchensEnumsList.Count; i++)
            {
                cookingKitchensList.Add(new SelectListItem() { Text = EnumHelper.GetDescription((CookingKitchens)i), Value = i.ToString() });
            }

            return cookingKitchensList;
        }

        private class EnumHelper
        {
            public static string GetDescription(Enum @enum)
            {
                if (@enum == null)
                    return null;

                string description = @enum.ToString();

                try
                {
                    FieldInfo fi = @enum.GetType().GetField(@enum.ToString());

                    DescriptionAttribute[] attributes = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);

                    if (attributes.Length > 0)
                        description = attributes[0].Description;
                }
                    // ReSharper disable once EmptyGeneralCatchClause
                catch
                {
                }

                return description;
            }
        }
        #endregion
    }
}