﻿using System;
 

namespace PunjabKitchen.Models
{
    public class CleaningCheckListDaily
    {
        public int iCleaningChecklistDailyId { get; set; }
        public int? iAreaId { get; set; }
        public DateTime? dDateTime { get; set; }
        public bool bOP { get; set; }
        public bool bRC { get; set; }
        public string vIssues { get; set; }
        public string vAction { get; set; }
        public DateTime? Sys_CreatedDateTime { get; set; }
        public string Sys_CreatedBy { get; set; }
        public DateTime? Sys_ModifyDateTime { get; set; }
        public string Sys_ModifyBy { get; set; }

        public int iAreaItemId { get; set; }
        public string vItem { get; set; }
        public string vProcRef { get; set; }

    }
}