﻿namespace PunjabKitchen.Models.RequestModels
{
    public class BaseRequestModel
    {
        public string SearchString { get; set; }
        public int FromRow { get; set; }
        public int ToRow { get; set; }
    }
}