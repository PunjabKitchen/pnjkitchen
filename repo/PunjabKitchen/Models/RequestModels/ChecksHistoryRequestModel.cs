﻿using System;

namespace PunjabKitchen.Models.RequestModels
{
    public class ChecksHistoryRequestModel : BaseRequestModel
    {
        public int Id{ get; set; }
        public DateTime StartDateTime { get; set; }
        public DateTime EndDateTime { get; set; }

        public int? page { get; set; }
    }
}