﻿using System;

namespace PunjabKitchen.Models.RequestModels
{
    public class CookingRequestModel : BaseRequestModel
    {
        public bool isCookpot1Selected { get; set; }
        public bool isCookpot2Selected { get; set; }
        public bool isCookpot3Selected { get; set; }
    }
}