﻿using System;

namespace PunjabKitchen.Models.RequestModels
{
    public class PagedClientViewModel
    {
        public int? Page { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public PagedList.PagedList<EmployeeAttendance> Attendances { get; set; }
    }
}