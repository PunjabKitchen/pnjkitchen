﻿namespace PunjabKitchen.Models.RequestModels
{
    public class ProductThresholdModel
    {
        public int iProductId { get; set; }
        public int? iThreshold { get; set; }
    }
}