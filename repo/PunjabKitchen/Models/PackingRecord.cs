﻿using System;
using System.Collections.Generic;
using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchen.Models
{
    public class PackingRecord
    {
        public int iPackingRecordId { get; set; }
        public Nullable<int> iCookingPlanId { get; set; }
        public string vBatchNo { get; set; }
        public string vProductName { get; set; }
        public Nullable<int> iProductWeight { get; set; }
        public string vProductType { get; set; }
        public Nullable<System.DateTime> tStartTime { get; set; }
        public string vPackers { get; set; }
        public string vMealBatchNo { get; set; }
        public string vMealStartTemp { get; set; }
        public string vMealFinishTemp { get; set; }
        public string vSauceBatchNo { get; set; }
        public string vSauceStartTemp { get; set; }
        public string vSauceFinishTemp { get; set; }
        public string vVeg_RiceName { get; set; }
        public string vVeg_RiceBatchNo { get; set; }
        public string vVeg_RiceStartTemp { get; set; }
        public string vVeg_RiceFinishTemp { get; set; }
        public Nullable<System.DateTime> tFinishTime { get; set; }
        public Nullable<int> iNumberOfPacksMade { get; set; }
        public Nullable<System.DateTime> tTimeToFreezer { get; set; }
        public string vSign { get; set; }
        public Nullable<System.DateTime> tBatchChangeCheckTime { get; set; }
        public Nullable<bool> bEquipVisualHygieneCheck { get; set; }
        public Nullable<bool> bOldPackaging_LabelsCleared { get; set; }
        public string vPackagingType { get; set; }
        public Nullable<int> iPackagingSize { get; set; }
        public Nullable<int> iPackagingUnit { get; set; }
        public string vPackagingBatchNo { get; set; }
        public string Lid_FilmBatchNo { get; set; }
        public Nullable<bool> bCheckStart { get; set; }
        public Nullable<bool> bCheckDuring { get; set; }
        public Nullable<bool> bCheckIfChange { get; set; }
        public Nullable<bool> bCheckEnd { get; set; }
        public string vLabelProductCode { get; set; }
        public string vLabelType { get; set; }
        public Nullable<int> iLabelQuantity { get; set; }
        public Nullable<int> iLabelUnit { get; set; }
        public Nullable<int> iLabelQuantityUnitId { get; set; }
        public string vLabel_BatchNo { get; set; }
        public Nullable<System.DateTime> dLabelBestBefore { get; set; }
        public Nullable<bool> bAllergen { get; set; }
        public Nullable<bool> bCheckLabelStart { get; set; }
        public Nullable<bool> bCheckLabelDuring { get; set; }
        public Nullable<bool> bCheckLabelIfChange { get; set; }
        public Nullable<bool> bCheckLabelEnd { get; set; }
        public string vBatchChangeCheckBySign { get; set; }
        public string vProductReleaseAuthBy { get; set; }
        public Nullable<System.DateTime> dDate { get; set; }
        public Nullable<bool> bEnabled { get; set; }
        public Nullable<bool> bDeleted { get; set; }
        public Nullable<System.DateTime> Sys_CreatedDateTime { get; set; }
        public string Sys_CreatedBy { get; set; }
        public Nullable<System.DateTime> Sys_ModifyDateTime { get; set; }
        public string Sys_ModifyBy { get; set; }
        public Nullable<bool> vMealStartTempCriticalCheck { get; set; }
        public Nullable<bool> vMealFinishTempCriticalCheck { get; set; }
        public Nullable<bool> vSauceStartTempCriticalCheck { get; set; }
        public Nullable<bool> vSauceFinishTempCriticalCheck { get; set; }
        public Nullable<bool> vVeg_RiceStartTempCriticalCheck { get; set; }
        public Nullable<bool> vVeg_RiceFinishTempCriticalCheck { get; set; }

        public virtual tCookingPlan tCookingPlan { get; set; }
        public string packersList { get; set; }

        public virtual tUnit tUnit { get; set; }
        public virtual tUnit tUnit1 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tPackingRecordDetail> tPackingRecordDetails { get; set; }
    }
}