﻿using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchen.Models
{
    public class PortionWeightRecordWithDetail
    {
        public PortionWeightCheck PortionWeightCheck { get; set; }
        public PortionWeightCheckDetail PortionWeightCheckDetail { get; set; }
    }
}