﻿using System;
using PunjabKitchenEntities.EntityModel;
// ReSharper disable All

namespace PunjabKitchen.Models
{
    public class GoodsOutFreezerUnit
    {
        public int iGoodsOutFreezerUnit { get; set; }
        public DateTime? dDateTime { get; set; }
        public DateTime? dTime { get; set; }
        public decimal? iFreezerTemp1 { get; set; }
        public decimal? iFreezerTemp2 { get; set; }
        public decimal? iFreezerTemp3 { get; set; }
        public decimal? iFreezerTemp4 { get; set; }
        public decimal? iFreezerTemp5 { get; set; }
        public string vComments { get; set; }
        public DateTime? Sys_CreatedDateTime { get; set; }
        public string Sys_CreatedBy { get; set; }
        public DateTime? Sys_ModifyDateTime { get; set; }
        public string Sys_ModifyBy { get; set; }

        public virtual AspNetUser AspNetUser { get; set; }
        public virtual AspNetUser AspNetUser1 { get; set; }
    }
}