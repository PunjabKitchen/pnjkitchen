﻿using System;
using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchen.Models
{
    public class PureeShapesStarchRecordDetail
    {
        public int iPureeShapeStarchRecordDetailId { get; set; }
        public Nullable<int> iPureeShapeStarchRecordId { get; set; }
        public string vBatchCode { get; set; }
        public Nullable<System.DateTime> Sys_CreatedDateTime { get; set; }
        public string Sys_CreatedBy { get; set; }
        public Nullable<System.DateTime> Sys_ModifyDateTime { get; set; }
        public string Sys_ModifyBy { get; set; }

        public virtual tPureeShapesStarchRecord tPureeShapesStarchRecord { get; set; }
    }
}