﻿namespace PunjabKitchen.Models.ResponseModels
{
    public class AttendanceStatus
    {
        public bool IsAlreadyCheckedIn { get; set; }
        public bool IsAlreadyCheckedOut { get; set; }
    }
}