﻿using PunjabKitchenEntities.EntityModel;
using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace PunjabKitchen.Models
{
    public class HotPackingRercord
    {
        public int iHotPackingRercordId { get; set; }
        public Nullable<System.DateTime> dDate { get; set; }
        public string vBatchNo { get; set; }
        public Nullable<int> iCookingPlanId { get; set; }
        public Nullable<System.DateTime> tTimeExCook { get; set; }
        public Nullable<System.DateTime> tTimeOfLastTrayInBlastFreezer { get; set; }
        public Nullable<System.DateTime> tTimeTaken { get; set; }
        public string vOperativeSign { get; set; }
        public string vSupervisorSign { get; set; }
        public string vManagerSign { get; set; }
        public Nullable<bool> bEnabled { get; set; }
        public Nullable<bool> bDeleted { get; set; }
        public Nullable<System.DateTime> Sys_CreatedDateTime { get; set; }
        public string Sys_CreatedBy { get; set; }
        public Nullable<System.DateTime> Sys_ModifyDateTime { get; set; }
        public string Sys_ModifyBy { get; set; }
        public Nullable<bool> bCriticalCheck { get; set; }

        public virtual tCookingPlan tCookingPlan { get; set; }
    }
}