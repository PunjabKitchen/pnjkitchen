﻿using System;
// ReSharper disable All


namespace PunjabKitchen.Models
{
    public class CookingPlan
    {
        public int iCookingPlanId { get; set; }
        public int? iCookPotNo { get; set; }
        public int? iPBARId { get; set; }
        public int? iQuantity { get; set; }
        public int? iEstMeals { get; set; }
        public int? iQuantityCompleted { get; set; }
        public DateTime? tActualCooktime { get; set; }
        public DateTime? tPlannedStartTime { get; set; }
        public DateTime? tActualStartTime { get; set; }
        public DateTime? tBreakStartTime { get; set; }
        public DateTime? tBreakFinishedTime { get; set; }
        public DateTime? tPlannedCooktime { get; set; }
        public DateTime? tActualFinishedTime { get; set; }
        public DateTime? tActualFinishTime { get; set; }
        public bool? bIsTask { get; set; }
        public string vTaskDescription { get; set; }
        public int? iProductId { get; set; }
        public string vBatchNo { get; set; }
        public Nullable<int> iRecipeId { get; set; }
        public DateTime? Sys_CreatedDateTime { get; set; }
        public string Sys_CreatedBy { get; set; }
        public DateTime? Sys_ModifyDateTime { get; set; }
        public string Sys_ModifyBy { get; set; }
        public Nullable<bool> bIsCancelled { get; set; }
        public Nullable<System.DateTime> tCookDate { get; set; }
        public Nullable<bool> bPackingConsumptionCheck { get; set; }
        public Nullable<bool> bFinishedPlan { get; set; }


        public virtual Product tProduct { get; set; }
        public virtual Recipe tRecipe { get; set; }


        //Additional Fields
        public string ProductCode { get; set; }

    }
}