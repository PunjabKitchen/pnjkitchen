﻿using System;
using System.ComponentModel.DataAnnotations;

namespace PunjabKitchen.Models
{
    public class Product
    {
        public int iProductId { get; set; }
        public int? iProductTypeId { get; set; }
        [Required]
        public string vBatchCode { get; set; }
        [Required]
        public string vName { get; set; }
        public string vDescription { get; set; }
        public DateTime? Sys_CreatedDateTime { get; set; }
        public string Sys_CreatedBy { get; set; }
        public DateTime? Sys_ModifyDateTime { get; set; }
        public string Sys_ModifyBy { get; set; }
        public int? tGoodsInId { get; set; }
        public bool? bProductVisuallyOK { get; set; }
        public bool? bProductTemp { get; set; }
        public string dCCP1aBestBefore { get; set; }
        public DateTime? dCCP1aUseByDate { get; set; }
        public DateTime? dCCP1aPackDate { get; set; }
        [Required]
        public string vLotNumber { get; set; }
        public bool? bIntakeChecksCompleted { get; set; }
        public bool? bContainsAllergen { get; set; }
        public bool? bQualityApproved { get; set; }
        public string vComments { get; set; }
        public int? iStockId { get; set; }
        public int? iThreshold { get; set; }
        [Required]
        public string vProductCode { get; set; }
        public int? iUnitId { get; set; }
        public int? iUnitWeight { get; set; }
        public int? iQuantity { get; set; }
        public bool addMoreProduct { get; set; }
        public string stockLocationName { get; set; }

        public virtual Unit tUnit { get; set; }
        public virtual Stock tStock { get; set; }
    }
}