﻿using System;
using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchen.Models
{
    public class PureeShapesStarchAdditionRecord
    {
        public int iPureeShapeStarchRecordAdditionId { get; set; }
        public Nullable<int> iPureeShapeStarchRecordId { get; set; }
        public string vAdditionName { get; set; }
        public string vAdditionBatchNo { get; set; }
        public Nullable<int> vAdditionAmount { get; set; }
        public Nullable<int> iAdditionUnit { get; set; }
        public Nullable<System.DateTime> Sys_CreatedDateTime { get; set; }
        public string Sys_CreatedBy { get; set; }
        public Nullable<System.DateTime> Sys_ModifyDateTime { get; set; }
        public string Sys_ModifyBy { get; set; }

        public virtual tPureeShapesStarchRecord tPureeShapesStarchRecord { get; set; }
        public virtual tUnit tUnit { get; set; }
    }
}