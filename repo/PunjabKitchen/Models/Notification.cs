﻿
using System;
using PunjabKitchenEntities.EntityModel;

// ReSharper disable All

namespace PunjabKitchen.Models
{
    public class Notification
    {
        public int iNotificationId { get; set; }
        public string vProcess { get; set; }
        public Nullable<int> iRefrenceId { get; set; }
        public string vDescription { get; set; }
        public Nullable<int> iNotificationTypeId { get; set; }
        public string iNotificationSenderId { get; set; }
        public Nullable<int> iNotificationReceiverGroupId { get; set; }
        public Nullable<bool> bNotificationReceived { get; set; }
        public string iWorkStarterId { get; set; }
        public Nullable<bool> bEnabled { get; set; }
        public Nullable<bool> bDeleted { get; set; }
        public string vSubject { get; set; }
        public Nullable<System.DateTime> Sys_CreatedDateTime { get; set; }
        public string Sys_CreatedBy { get; set; }
        public Nullable<System.DateTime> Sys_ModifyDateTime { get; set; }
        public string Sys_ModifyBy { get; set; }
        //public string UserCreatedBy { get; set; }

        public virtual AspNetUser AspNetUser { get; set; }
        public virtual tNotificationType tNotificationType { get; set; }
    }
}