﻿using System.Collections.Generic;
using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchen.Models
{
    public class PureeShapesRecordCombinedModel
    {
        public PureeShapeStarchRecord PureeShapeStarchRecord { get; set; }
        public PureeShapesStarchRecordDetail PureeShapesStarchRecordDetail { get; set; }
        public PureeShapesStarchAdditionRecord PureeShapesStarchAdditionRecord { get; set; }
        public List<PureeShapesStarchAdditionRecord> PureeShapesStarchAdditionRecordsList { get; set; }
        public string BatchCodesList { get; set; }
        public string TotalWeight { get; set; }
    }
}