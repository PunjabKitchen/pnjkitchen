﻿using System;
using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchen.Models
{
    public class PureeMealBatchAssemblyDetail
    {
        public int iPureeMealBatchAssemblyDetailId { get; set; }
        public Nullable<int> iPureeMeanBatchAssemblyId { get; set; }
        public string vShape { get; set; }
        public string vShapeTemp { get; set; }
        public string vShapeBatchNo { get; set; }
        public Nullable<System.DateTime> dTime { get; set; }
        public string vSauce { get; set; }
        public string vSauceTemp { get; set; }
        public string vSauceBatchNo { get; set; }
        public Nullable<System.DateTime> tSauceTimeOfTransferFromPrep { get; set; }
        public string vMash { get; set; }
        public string vMashBatchNo { get; set; }
        public string vMashTemp { get; set; }
        public Nullable<System.DateTime> tMashTimeOfTransferFromPrep { get; set; }
        public Nullable<bool> bCriticalCheck { get; set; }
        public Nullable<System.DateTime> Sys_CreatedDateTime { get; set; }
        public string Sys_CreatedBy { get; set; }
        public Nullable<System.DateTime> Sys_ModifyDateTime { get; set; }
        public string Sys_ModifyBy { get; set; }

        public virtual tPureeMeanBatchAssembly tPureeMeanBatchAssembly { get; set; }
    }
}