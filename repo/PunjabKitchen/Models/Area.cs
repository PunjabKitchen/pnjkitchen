﻿using System;
 

namespace PunjabKitchen.Models
{
    public class Area
    {
        
        public int iAreaId { get; set; }
        public string vAreaName { get; set; }
        public DateTime? Sys_CreatedDateTime { get; set; }
        public string Sys_CreatedBy { get; set; }
        public DateTime? Sys_ModifyDateTime { get; set; }
        public string Sys_ModifyBy { get; set; } 
    }
}