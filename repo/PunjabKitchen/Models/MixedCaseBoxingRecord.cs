﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchen.Models
{
    public class MixedCaseBoxingRecord
    {
        public int iMixedCaseBoxingRecordId { get; set; }
        public Nullable<System.DateTime> dDateCaseMade { get; set; }
        public string iUserId { get; set; }
        public Nullable<int> iNoOfCases { get; set; }
        public string vCaseName { get; set; }
        public string vDateCaseGoingOut { get; set; }
        public Nullable<System.DateTime> dDayOfBuild { get; set; }
        public Nullable<System.DateTime> dLabelBestBefore { get; set; }
        public Nullable<bool> bLabelChecked { get; set; }
        public Nullable<bool> bCopyOnBack { get; set; }
        public Nullable<int> iBuildByUserId { get; set; }
        public string vBuildBySign { get; set; }
        public Nullable<int> iCheckedByUserId { get; set; }
        public string vCheckedBySign { get; set; }
        public string vComments { get; set; }
        public string vManagerSign { get; set; }
        public Nullable<bool> bEnabled { get; set; }
        public Nullable<bool> bDeleted { get; set; }
        public Nullable<System.DateTime> Sys_CreatedDateTime { get; set; }
        public string Sys_CreatedBy { get; set; }
        public Nullable<System.DateTime> Sys_ModifyDateTime { get; set; }
        public string Sys_ModifyBy { get; set; }
        public string vCustomer { get; set; }
        public string vBatchNo { get; set; }
        public string iCaseNo { get; set; }
        public virtual ICollection<tMixedCaseBoxingRecordDetail> tMixedCaseBoxingRecordDetails { get; set; }

        //MixedCaseBoxingRecordDetail
        public int iMixedCaseBoxingRecordDetailId { get; set; }
        public Nullable<int> iProductId { get; set; }
        public string vBatchCode { get; set; }
        public string vNameOfPack { get; set; }
        public string vNumber { get; set; }
    }
}