﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PunjabKitchen.Models
{
    public class ProductType
    {
        public int iProductTypeId { get; set; }
        public string vName { get; set; }
        public string vDescription { get; set; }
        public Nullable<System.DateTime> Sys_CreatedDateTime { get; set; }
        public string Sys_CreatedBy { get; set; }
        public Nullable<System.DateTime> Sys_ModifyDateTime { get; set; }
        public string Sys_ModifyBy { get; set; }
    }
}