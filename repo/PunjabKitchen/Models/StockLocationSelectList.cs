﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PunjabKitchen.Models
{
    public class StockLocationSelectList
    {
        public int iStockId { get; set; }
        public string vStorageLocation { get; set; }
    }
}