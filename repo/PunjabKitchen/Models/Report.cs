﻿using PunjabKitchenBLL.Interfaces;
using System;
using System.ComponentModel;

namespace PunjabKitchen.Models
{
    public class Report
    {
        public Report()
        {
        }

        public PkReportNames ReportName { get; set; }

        public IService Service { get; set; }

        public int Id { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
    }

    public enum PkReportNames
    {
        [Description("Goods In")]
        GoodsIn = 1,

        [Description("Cooking Plan")]
        CookingPlan = 2,

        [Description("Packing Plan")]
        PackingPlan = 3,

        [Description("Boxing")]
        Boxing = 4,

        [Description("Goods Out")]
        GoodsOut = 5,

        [Description("Stock Count")]
        StockCount = 6,

        [Description("Stock & Storage Record")]
        StockAndStorageRecord = 7,

        [Description("Cooking Plan Record")]
        CookingPlanRecord = 8,

        [Description("Packing Plan Record")]
        PackingPlanRecord = 9,

        [Description("GoodsOut Plan Record")]
        GoodsOutPlanRecord = 10,

        [Description("Delivery Record")]
        DeliveryRecord = 11,

        [Description("Boxing Record")]
        BoxingRecord = 12,

        [Description("Mixed Case Boxing Record")]
        MixedCaseBoxingRecord = 13,

        [Description("GoodsOut PickList")]
        GoodsOutPickList = 14,

        [Description("Slicing Record")]
        SlicingRecord = 15,

        [Description("Rice Cooling Temperature Record")]
        RiceCoolingTempRecord = 16,

        [Description("Meat Mincing Record")]
        MeatMincingRecord = 17,

        [Description("Batch Cooling Temperature Record")]
        BatchCoolingTempRecord = 18,

        [Description("Freezer Record")]
        FreezerRecord = 19,

        [Description("Cold Dessert Packing Record")]
        ColdDessertPackingRecord = 20,

        [Description("Hot Packing Record")]
        HotPackingRecord = 21,

        [Description("Shapes Control Packing Record")]
        ShapesControlPackingRecord = 22,

        [Description("Batch Change Check Puree Record")]
        BatchChangeCheckPureeRecord = 23,

        [Description("Metal Detection And Pack Weight Record")]
        MetalDetectionAndPackWeightRecord = 24,

        [Description("Ferrous Detection And Pack Weight Record")]
        FerrousDetectionAndPackWeightRecord = 25,

        [Description("Spot Weight And Metal Check Record")]
        SpotWeightAndMetalCheckRecord = 26,

        [Description("Portion Weights Check Packing Record")]
        PortionWeightsCheckPackingRecord = 27,

        [Description("Puree Shape Starch Record")]
        PureeShapeStarchRecord = 28,

        [Description("Packing Record")]
        PackingRecord = 29,

        [Description("Packing Room Record")]
        PackingRoomRecord = 30,

        [Description("Vegetarian Packing Record")]
        VegetarianPackingRecord = 31,

        [Description("Puree Meal Batch Assembly Record")]
        PureeMealBatchAssemblyRecord = 32,
    }
}