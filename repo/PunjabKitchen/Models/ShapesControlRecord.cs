﻿using System;
using PunjabKitchenEntities.EntityModel;
using System.Collections.Generic;

namespace PunjabKitchen.Models
{
    public class ShapesControlRecord
    {
        public int iShapeControlRecordId { get; set; }
        public Nullable<int> iCookingPlanId { get; set; }
        public Nullable<System.DateTime> tCookingFinishTime { get; set; }
        public Nullable<System.DateTime> tPureeStartTime { get; set; }
        public string vBatchNo { get; set; }
        public Nullable<int> iQuantityMade { get; set; }
        public Nullable<System.DateTime> tTimeIntoBlastFreezer { get; set; }
        public Nullable<System.DateTime> tTimeExInBlastFreezer { get; set; }
        public string vTempExBlastFreezerBelow { get; set; }
        public Nullable<System.DateTime> tTimeToStorageFreezer { get; set; }
        public Nullable<int> iPackerUserId { get; set; }
        public string vSuperVisorSign { get; set; }
        public string vManagerSign { get; set; }
        public Nullable<System.DateTime> Sys_CreatedDateTime { get; set; }
        public string Sys_CreatedBy { get; set; }
        public Nullable<System.DateTime> Sys_ModifyDateTime { get; set; }
        public string Sys_ModifyBy { get; set; }
        public Nullable<System.DateTime> dShapesControl { get; set; }
        public Nullable<int> iUnitId { get; set; }
        public string vProductName { get; set; }
        public Nullable<bool> bCriticalCheck { get; set; }
        public string packersList { set; get; }
        public string batchCodesList { set; get; }

        public virtual tCookingPlan tCookingPlan { get; set; }

        public virtual tUnit tUnit { get; set; }
        public virtual ICollection<tShapeControlRecordDetail> tShapeControlRecordDetails { get; set; }
        public virtual ICollection<tShapesControlRecordBatchDetail> tShapesControlRecordBatchDetails { get; set; }
    }
}