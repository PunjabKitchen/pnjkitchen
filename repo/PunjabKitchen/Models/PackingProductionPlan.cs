﻿using PunjabKitchenEntities.EntityModel;
using System;
using System.ComponentModel.DataAnnotations;

namespace PunjabKitchen.Models
{
    public class PackingProductionPlan
    {
        public int iPackingProductionPlanId { get; set; }
        public Nullable<int> iCookingPlanId { get; set; }
        public string vBatchNo { get; set; }
        public string vCookingBatchNo { get; set; }
        public Nullable<System.DateTime> dPackingPlan { get; set; }
        public Nullable<int> iWeight { get; set; }
        public Nullable<int> iQuantityRequired { get; set; }
        public Nullable<int> iQuantityCompleted { get; set; }
        public string vShapes { get; set; }
        public Nullable<int> iPackers { get; set; }
        public Nullable<int> iPacksPerMin { get; set; }
        public Nullable<System.DateTime> tRunTime { get; set; }
        public Nullable<System.DateTime> tPlannedStart { get; set; }
        public Nullable<System.DateTime> tActualStart { get; set; }
        public Nullable<System.DateTime> tBreakStart { get; set; }
        public Nullable<System.DateTime> tBreakFinish { get; set; }
        public Nullable<System.DateTime> tPlannedFinish { get; set; }
        public Nullable<System.DateTime> tActualFinish { get; set; }
        public Nullable<System.DateTime> tActualRunTime { get; set; }
        public Nullable<bool> bEnabled { get; set; }
        public Nullable<bool> bDeleted { get; set; }
        public Nullable<int> iLineNo { get; set; }
        public Nullable<bool> bIsTask { get; set; }
        public string vTaskDescription { get; set; }
        public Nullable<System.DateTime> Sys_CreatedDateTime { get; set; }
        public string Sys_CreatedBy { get; set; }
        public Nullable<System.DateTime> Sys_ModifyDateTime { get; set; }
        public string Sys_ModifyBy { get; set; }
        public Nullable<bool> bFinishedPlan { get; set; }

        public virtual tCookingPlan tCookingPlan { get; set; }
    }
}