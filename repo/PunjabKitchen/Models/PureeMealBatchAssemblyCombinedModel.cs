﻿using System.Collections.Generic;

namespace PunjabKitchen.Models
{
    public class PureeMealBatchAssemblyCombinedModel
    {
        public PureeMealBatchAssembly PureeMealBatchAssembly { get; set; }
        public PureeMealBatchAssemblyDetail PureeMealBatchAssemblyDetail { get; set; }
        public PureeMealBatchAssemblyPackerDetail PureeMealBatchAssemblyPackerDetail { get; set; }
        public PureeMealInBlastFreezer PureeMealInBlastFreezer { get; set; }
        public string packersList { get; set; }
        public IEnumerable<PureeMealInBlastFreezer> BlastFreezersList { get; set; }
    }
}