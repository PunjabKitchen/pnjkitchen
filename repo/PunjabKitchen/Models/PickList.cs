﻿using PunjabKitchenEntities.EntityModel;
using System;

namespace PunjabKitchen.Models
{
    public class PickList
    {
        public int iPickListId { get; set; }
        public Nullable<int> iCookingPlanId { get; set; }
        public Nullable<int> iProductId { get; set; }
        public Nullable<double> iQuantity { get; set; }
        public string vDescription { get; set; }
        public Nullable<int> iUnitId { get; set; }
        public string vBatchCode { get; set; }
        public Nullable<System.DateTime> Sys_CreatedDateTime { get; set; }
        public string Sys_CreatedBy { get; set; }
        public Nullable<System.DateTime> Sys_ModifyDateTime { get; set; }
        public string Sys_ModifyBy { get; set; }

        public virtual AspNetUser AspNetUser { get; set; }
        public virtual AspNetUser AspNetUser1 { get; set; }
        public virtual tCookingPlan tCookingPlan { get; set; }
        public virtual tProduct tProduct { get; set; }
        public virtual tUnit tUnit { get; set; }
    }
}