﻿
using System;
using PunjabKitchenEntities.EntityModel;

namespace PunjabKitchen.Models
{
    public class GoodsOutPlanDetail
    {
        public int iGoodsOutPlanDetailId { get; set; }
        public int? iGoodsOutPlanId { get; set; }
        public int? iProductId { get; set; }
        public string vCategoryDescription { get; set; }
        public int? iNumberOfMeals { get; set; }
        public string vQuantity { get; set; }
        public DateTime? Sys_CreatedDateTime { get; set; }
        public string Sys_CreatedBy { get; set; }
        public DateTime? Sys_ModifyDateTime { get; set; }
        public string Sys_ModifyBy { get; set; }

        public virtual GoodsOutPlan tGoodsOutPlan { get; set; }
        public virtual Product tProduct { get; set; }
    }
}