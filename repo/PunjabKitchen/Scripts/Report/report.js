﻿//#region Document Ready Function
$(document).ready(function () {

    $("#FromDateDatePicker").datetimepicker({
        format: "DD-MM-YYYY"
    });
    $("#ToDateDatePicker").datetimepicker({
        format: "DD-MM-YYYY"
    });
    $('#ToDateDatePicker').data("DateTimePicker").date(moment());
    $('#FromDateDatePicker').data("DateTimePicker").date(moment().subtract(1, 'months'));
    hideLoader();
});
//#endregion

//#region Generate Report
function generateReport() {

    if (!$("#reportsForm").valid())
        return;

    showLoader();
    updateTimePickerOnSubmit();

    $.ajax({
        type: "GET",
        url: GENERATEREPORT_URL,
        data: $('#reportsForm').serialize(),
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            $('#iframeReportViewer').attr('src', data);
            $('#iframeReportViewer').load(function () {
                setTimeout(function () {
                    var mydiv = $("#iframeReportViewer").contents().find("#tblReport");
                    var h = mydiv.height();
                    $('#iframeReportViewer').css('min-height', (h + 100));
                    hideLoader();
                }, 1000);
            });
        },
        error: function () {
            showError("Something went wrong while generating report");
            hideLoader();
        }
    });
}
//#endregion

//#region DateTime functions
function getDateTimeForBoxing(date) {

    var datePart = date.split(" ")[0];

    var day = datePart.split("-")[0];
    var month = datePart.split("-")[1];
    var year = datePart.split("-")[2];

    return year + "-" + month + "-" + day;
}

//Update Time Picker On Submit
function updateTimePickerOnSubmit() {
    $("#FromDate").val(getDateTimeForBoxing($("#FromDateDatePicker").val()));
    $("#ToDate").val(getDateTimeForBoxing($("#ToDateDatePicker").val()));
}
//#endregion
