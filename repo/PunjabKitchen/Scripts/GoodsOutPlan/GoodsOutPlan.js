﻿
//#region Document Ready
$(document).ready(function () {
    hideLoader();

    $(".goodsOutPlanItem").click(function (e) {
        var tag = e.target.nodeName;
        if (tag === 'IMG') { }
        else {
            PackingPlanEdit($(e.target).parent());
        }
    });
});
//#endregion

//#region Create Goods Out plan
$('#CreateGoodsOutPlan').click(function () {

    showLoader();

    var url = CREATEGOODSOUTPLAN;
    $.get(url, function (data) {
        $('#goodsOutPlanBody').html(data);
        $('#goodsOutPlanModal').modal('show');
    });
});
//#endregion

//#region Edit Goods Out Plan
function PackingPlanEdit(ele) {

    showLoader();

    var idParam = $(ele).data('id');

    var url = GOODSOUTPLANEDIT_URL;
    $.get(url, { id: idParam }, function (data) {
        $("#goodsOutPlanBody").html(data);
        $("#goodsOutPlanModal").modal("show");

        $('#vBatchCode').attr('disabled', 'disabled');
        $('#ddlCategory').attr('disabled', 'disabled');
        $('#goodsOutPlanDatePicker').attr('disabled', 'disabled');
    });
}
//#endregion

//#region reload window if changes are there before closing
function closingWindow() {
    if ($('#hasChanges').val() == "1")
        window.location.reload();
}
//#endregion