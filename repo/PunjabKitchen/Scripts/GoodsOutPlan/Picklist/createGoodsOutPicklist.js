﻿//#region Document Ready functions
$(document).ready(function () {
    hideLoader();

    NumberValidation();
    initializeDateTimePicker();
    updateCheckBoxes();

    showLoader();
    //Triggering the Product Code's autocomplete property initially to bring it in running condition.
    initialAutocompleteRequest();

    if ($('#iGoodsOutPickListId').val() > 0) {
        callddlLocationOnChange($('#stockLocationListItem'));
    }

    $(".stockLocationListItem").on('change', function () {
        callddlLocationOnChange(this);
    });

    $(".stockSelectPointItem").on('change', function () {
        $('#iStockId').val(this.value);
    });

    $("#detail_iUnitId").on('change', function () {
        $('#detail_vUnitName').val($('#detail_iUnitId option:selected').html());
    });

    $("#detail_vItemCode").on("keyup", function (e) {
        var code = (e.keyCode ? e.keyCode : e.which);
        if (code === 13) { //Enter keycode
            return;
        }
        $("#detail_vItemCode").addClass("spinner");
        $("#detail_vItemName").val("");
        $("#detail_iRecipeId").val("");
        getRecipeInfo($(this).val());
    });

    $('#btnGoodsOutPicklistModal').on('click', function () {
        if (!$("#goodsOutPicklistForm").valid())
            return;
        else
            $("#goodsOutPickListModal").modal("show");
    });

    $('#btnSaveGoodsOutPicklistItem').on('click', function () {
        submitDetailForm();
    });

});
//#endregion

//#region Number Validations
function NumberValidation() {
    $("#iOrderNumber").keypress(function (evt) {
        var self = $(this);
        self.val(self.val().replace(/[^\d].+/, ""));
        if ((evt.which < 48 || evt.which > 57) && (evt.which !== 8) && (evt.keyCode !== 46) && (evt.keyCode !== 37) && (evt.keyCode !== 39)) {
            evt.preventDefault();
        }

        if ((this.value.length === 6) && (evt.which !== 8) && (evt.keyCode !== 46) && (evt.keyCode !== 37) && (evt.keyCode !== 39)) {
            return false;
        }
        return true;
    });
    $("#detail_iQuantity").keypress(function (evt) {
        var self = $(this);
        self.val(self.val().replace(/[^\d].+/, ""));
        if ((evt.which < 48 || evt.which > 57) && (evt.which !== 8) && (evt.keyCode !== 46) && (evt.keyCode !== 37) && (evt.keyCode !== 39)) {
            evt.preventDefault();
        }

        if ((this.value.length === 6) && (evt.which !== 8) && (evt.keyCode !== 46) && (evt.keyCode !== 37) && (evt.keyCode !== 39)) {
            return false;
        }
        return true;
    });
    $("#detail_vBatchNo").keypress(function (evt) {
        var self = $(this);
        self.val(self.val().replace(/[^\d].+/, ""));
        if ((evt.which < 48 || evt.which > 57) && (evt.which !== 8) && (evt.keyCode !== 46) && (evt.keyCode !== 37) && (evt.keyCode !== 39)) {
            evt.preventDefault();
        }

        if ((this.value.length === 6) && (evt.which !== 8) && (evt.keyCode !== 46) && (evt.keyCode !== 37) && (evt.keyCode !== 39)) {
            return false;
        }
        return true;
    });
    $("#detail_iToFollow").keypress(function (evt) {
        var self = $(this);
        self.val(self.val().replace(/[^\d].+/, ""));
        if ((evt.which < 48 || evt.which > 57) && (evt.which !== 8) && (evt.keyCode !== 46) && (evt.keyCode !== 37) && (evt.keyCode !== 39)) {
            evt.preventDefault();
        }

        if ((this.value.length === 6) && (evt.which !== 8) && (evt.keyCode !== 46) && (evt.keyCode !== 37) && (evt.keyCode !== 39)) {
            return false;
        }
        return true;
    });
}
//#endregion

//#region Product Code's Autocomplete Request
function initialAutocompleteRequest() {
    $.post(
            GETRECIPEBYCODE_URL,
            {
                code: "0"
            },
            function (data) {
                if (data.length > 0) {
                    var arrayList = [];
                    _.each(data, function (item) {
                        arrayList.push({
                            "description": item.description == null ? "" : item.description,
                            "value": item.code == null ? "" : item.code,
                            "id": item.id == null ? "" : item.id,
                            "label": item.code == null ? "" : item.code,
                            "code": item.code == null ? "" : item.code,
                            "name": item.name == null ? "" : item.name
                        });
                    });
                    $("#detail_vItemCode").autocomplete({
                        dataType: "json",
                        source: arrayList,
                        select: function (event, ui) {
                            $("#detail_vItemCode").val(ui.item.code);
                            $("#detail_iRecipeId").val(ui.item.id);
                            $("#detail_vItemName").val(ui.item.name);
                        }
                    });
                }
            });
}
//#endregion

//#region Initialize date time picker
function initializeDateTimePicker() {

    $("#DeliveryDateDatePicker").datetimepicker({
        format: "DD-MM-YYYY",
        minDate: moment()
    });

    if ($("#iGoodsOutPickListId").val() > 0) {
        $("#DeliveryDateDatePicker").val(moment($("#dDeliveryDate").val()).format("DD-MM-YYYY"));
    }
}
//#endregion

//#region date time formating
function getDateTimeForBoxing(date) {

    var datePart = date.split(" ")[0];

    var day = datePart.split("-")[0];
    var month = datePart.split("-")[1];
    var year = datePart.split("-")[2];

    return year + "-" + month + "-" + day;
}
//#endregion

//#region date time formating on submit
function updateTimePickerOnSubmit() {
    $("#dDeliveryDate").val(getDateTimeForBoxing($("#DeliveryDateDatePicker").val()));
}
//#endregion

//#region Get Stock point items
function getStockPointItems(location) {
    showLoader();
    $.ajax({
        type: "GET",
        url: GETSTOCKPOINTS_URL + "?stockLocation=" + location,
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            hideLoader();
            var storagePointElem = ".stockSelectPointItem";
            $(storagePointElem).empty();
            $(storagePointElem).append($('<option>').text("Select Stock Point").attr('value', ""));
            $.each(data, function (i, value) {
                $(storagePointElem).append($('<option>').text(value.vStoragePoint).attr('value', value.iStockId));
            });
            $(".stockSelectPointItem").removeClass('error');
            $("#stockLocationListItem").removeClass('error');

            if ($('#iGoodsOutPickListId').val() > 0) {
                $('.stockSelectPointItem').val($('#iStockId').val()).change();
            }

        },
        error: function () {
            hideLoader();
        }
    });
}
//#endregion

//#region get autocomplete receipe codes
function getRecipeInfo(text) {
    var array = new Array();
    if (text.length > 0) {
        $.post(
            GETRECIPEBYCODE_URL,
            {
                code: text
            },
            function (data) {
                $("#detail_vItemCode").removeClass("spinner");
                if (data.length > 0) {
                    var arrayList = [];
                    _.each(data, function (item) {
                        arrayList.push({
                            "description": item.description == null ? "" : item.description,
                            "value": item.code == null ? "" : item.code,
                            "id": item.id == null ? "" : item.id,
                            "label": item.code == null ? "" : item.code,
                            "code": item.code == null ? "" : item.code,
                            "name": item.name == null ? "" : item.name
                        });
                        //toastr.success(item.code);
                    });
                    $("#detail_vItemCode").autocomplete({
                        dataType: "json",
                        source: arrayList,
                        select: function (event, ui) {
                            $("#detail_vItemCode").val(ui.item.code);
                            $("#detail_iRecipeId").val(ui.item.id);
                            $("#detail_vItemName").val(ui.item.name);
                        }
                    });
                }
            });
    } else {
        hideLoader();
    }
    return array;
}
//#endregion

//#region delete detail item
function deleteDetailItem(id) {

    showLoader();

    var remainingRecords = $('#tableGoodsOutPlan tr').length;

    var obj = {
        iGoodsOutPicklistDetailId: id,
        iRemainingRecords: remainingRecords
    }

    $.ajax({
        type: "GET",
        url: DELETEGOODSOUTPICKLISTITEM,
        data: obj,
        success: function (result) {

            if (result.deletedRecordID === -1) {
                hideLoader();
                showError("Deletion Failed.");
                return;
            }

            $('#tableGoodsOutPlan tr[data-id="' + id + '"]').remove();

            showSuccess("Detail item successfully removed.");
            calculateTotal();
            hideLoader();
        },
        error: function () {
            showError("Something went wrong while deleting record");
            hideLoader();
        }
    });
}
//#endregion

//#region Confirm Event
function ConfirmDeletion(id) {
    $("#dialog-confirm").html("Are you sure you want to delete this record?");

    // Define the Dialog and its properties.
    $("#dialog-confirm").dialog({
        modal: true,
        title: "Delete Confirmation",
        height: 150,
        width: 300,
        buttons: {
            "Yes": function () {
                $(this).dialog('close');
                deleteDetailItem(id);
            },
            "No": function () {
                $(this).dialog('close');
            }
        }
    });
}
//#endregion

//#region toggle checkboxes
function updateCheckBoxes() {
    $('#detail_bOldLabel_hidden').val().toLowerCase() == "true" ? $('#bEquipVisualHygiene').prop('checked', true) : $('#detail_bOldLabel').prop('checked', false);
}
//#endregion

//#region update checkboxes values after submit
function updateCheckBoxesonSubmit() {
    $('#detail_bOldLabel').prop('checked') ? $('#detail_bOldLabel_hidden').val(true) : $('#detail_bOldLabel_hidden').val(false);
}
//#endregion

//#region submit the picklist
function submitDetailForm() {
    if (!$("#goodsOutPicklistItemForm").valid()) {
        return;
    }

    updateTimePickerOnSubmit();
    updateCheckBoxesonSubmit();

    showLoader();
    $.ajax({
        type: "POST",
        url: SUBMITGOODSOUTPICKLIST_URL,
        data: $("#goodsOutPicklistForm").serialize() + '&' + $("#goodsOutPicklistItemForm").serialize(),
        success: function (data) {
            showSuccess("Goods Out Picklist record saved successfully");
            hideLoader();

            $('#iGoodsOutPickListId').val(data[0].iGoodsOutPickListId);

            $("#tableGoodsOutPlan").find("tbody")
                    .append("<tr data-id=" + data[0].detail_iGoodsOutPickListDetailId + ">" +
                                "<td>" + data[0].detail_vShelf + "</td>" +
                                "<td>" + data[0].detail_vItemCode + " " + data[0].detail_vItemName + "</td>" +
                                "<td class='centered totalQuantity'>" + data[0].detail_iQuantity + "</td>" +
                                "<td>" + data[0].detail_vBatchNo + "</td>" +
                                "<td class='centered'><span class=" + (data[0].detail_bOldLabel == true ? "ico-ok" : "ico-cross") + " class='ico-action'></span>" + "</td>" +
                                "<td class='centered totalToFollow'>" + data[0].detail_iToFollow + "</td>" +
                                "<td>" + data[0].detail_vUnitName + "</td>" +
                                "<td>" + "<a href='#' data-id='" + data[0].detail_iGoodsOutPickListDetailId + "' class='stockLocEdit' onclick='deleteDetailItem(" + data[0].detail_iGoodsOutPickListDetailId + ")'><img src='../../images/ico-del.png' class='ico-action' alt='Delete'></a></td>" +
                            "</tr>");

            $(".pickListItem").val("");
            $('#detail_bOldLabel').prop('checked', false);

            calculateTotal();

            $("#goodsOutPickListModal").modal("hide");
        },
        error: function () {
            showError("Something went wrong while saving record");
            hideLoader();
        }
    });
}
//#endregion

//#region calculate total in footer
function calculateTotal() {
    var ttlQuantity = 0;
    $('#tableGoodsOutPlan').find('.totalQuantity').each(function (index, element) {
        ttlQuantity = getSum((index + 1), '.totalQuantity');
        $('#totalQuantity').text(ttlQuantity);
    });

    var ttlFollowUp = 0;
    $('#tableGoodsOutPlan').find('.totalToFollow').each(function (index, element) {
        ttlFollowUp = getSum((index + 1), '.totalToFollow');
        $('#totalToFollow').html(ttlFollowUp + "<br /><br /><span id='netTotal'></span>");
    });

    $('#netTotal').text(ttlFollowUp + ttlQuantity);
}

function getSum(colNumber, selector) {
    var sum = 0;

    $('#tableGoodsOutPlan').find(selector).each(function (index, element) {
        sum += parseInt($(element).text());
    });

    return sum;
}
//#endregion

//#region Location Function
function callddlLocationOnChange(ele) {
    var clickedElem = $(ele);
    $('.stockLocationListItem').removeClass("selected");
    $(this).addClass('selected');
    getStockPointItems($("#" + clickedElem.attr('id')).val());
}
//#endregion

//#region Submit Form
function submitForm() {
    if (!$("#goodsOutPicklistForm").valid()) {
        return;
    }

    updateTimePickerOnSubmit();
    updateCheckBoxesonSubmit();

    showLoader();
    $.ajax({
        type: "POST",
        url: SUBMITGOODSOUTPICKLIST_URL,
        data: $("#goodsOutPicklistForm").serialize(),
        success: function (data) {
            showSuccess("Goods Out Picklist record saved successfully");
            hideLoader();
            window.location.href = GOODSOUTPICKLISTLIST;
        },
        error: function () {
            showError("Something went wrong while saving record");
            hideLoader();
        }
    });
}
//#endregion