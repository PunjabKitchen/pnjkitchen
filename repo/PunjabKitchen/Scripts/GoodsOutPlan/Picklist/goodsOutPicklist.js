﻿//#region Document ready function
$(document).ready(function () {
    hideLoader();

    $(".goodsOutPickListItem").click(function (e) {
        var tag = e.target.nodeName;
        if (tag === 'IMG') { }
        else {
            EditGoodsOutPicklist($(e.target).parent());
        }
    });
});
//#endregion

//#region Create goods out picklist button click
$('#CreateGoodsOutPicklist').click(function () {
    showLoader();
    var url = CREATEGOODSOUTPICKLIST;
    window.location.href = url;
});
//#endregion

//#region edit goods out picklist
function EditGoodsOutPicklist(ele) {

    showLoader();

    var idParam = $(ele).data('id');

    var url = GOODSOUTPICKLISTEDIT_URL + '/' + idParam;
    window.location.href = url;
}
//#endregion
