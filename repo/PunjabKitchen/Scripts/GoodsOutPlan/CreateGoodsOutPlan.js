﻿//#region Document Ready
$(document).ready(function () {
    //initialize datepicker
    initializeDateTimePicker();

    //#region Product Code Auto Complete
    $("#ProductCode").on("keyup", function (e) {
        var productCode = document.getElementById("ProductCode").value;
        var code = (e.keyCode ? e.keyCode : e.which);
        if (code === 13) { //Enter keycode
            return;
        }
        $("#ProductName").val("");
        $("#iProductId").val("");
        $("#ProductCode").addClass("spinner");
            getProductInfo($(this).val());
    });
    //#endregion

    //Get Batch Code
    if ($("#iProductId").val() === "") {
        $("#vBatchCode").attr("readonly", "readonly");
        getUniqueCode("vBatchCode", 5);
    }

    //Triggering the Product Code's autocomplete property initially to bring it in running condition.
    initialAutocompleteRequest();

    $("#iNumberOfMeals").keypress(function (evt) {
        var self = $(this);
        self.val(self.val().replace(/[^\d].+/, ""));
        if ((evt.which < 48 || evt.which > 57) && (evt.which !== 8) && (evt.keyCode !== 46) && (evt.keyCode !== 37) && (evt.keyCode !== 39)) {
            evt.preventDefault();
        }

        if ((this.value.length === 6) && (evt.which !== 8) && (evt.keyCode !== 46) && (evt.keyCode !== 37) && (evt.keyCode !== 39)) {
            return false;
        }
        return true;
    });

    hideLoader();
});
//#endregion

//#region Product Code's Autocomplete Request
function initialAutocompleteRequest() {
    $.post(GETPRODUCTBYCODE_URL,
            {
                code: "0"
            },
            function (data) {
                hideLoader();
                if (data.length > 0) {
                    var arrayList = [];
                    _.each(data, function (item) {
                        arrayList.push({
                            "description": item.description == null ? "" : item.description,
                            "value": item.code == null ? "" : item.code,
                            "id": item.id == null ? "" : item.id,
                            "label": item.code == null ? "" : item.code,
                            "code": item.code == null ? "" : item.code,
                            "name": item.name == null ? "" : item.name
                        });
                    });
                    $("#ProductCode").autocomplete({
                        dataType: "json",
                        source: arrayList,
                        select: function (event, ui) {
                            var prodCode = ui.item.code;
                            $("#ProductCode").val(prodCode);
                            $("#recipeId").val(ui.item.id);
                            $("#ProductDescription").val(ui.item.name);
                        }
                    });
                }
            });
}
//#endregion

//#region Category onChange
function ddlCategory_onChange(ele) {
    $('#vCategory').val(ele.value);
}
//#endregion

//#region Date and Time Pickers Region
function initializeDateTimePicker() {

    $("#goodsOutPlanDatePicker").datetimepicker({
        format: "DD-MM-YYYY",
        minDate: moment()
    });

    if ($("#iGoodsOutPlanId").val() > 0) {
        $("#goodsOutPlanDatePicker").val(moment($("#dDate").val()).format("DD-MM-YYYY"));
    }
}

function getDateTimeForBoxing(date) {

    var datePart = date.split(" ")[0];

    var day = datePart.split("-")[0];
    var month = datePart.split("-")[1];
    var year = datePart.split("-")[2];

    return year + "-" + month + "-" + day;
}

//Update Time Picker On Submit
function updateTimePickerOnSubmit() {
    $("#dDate").val(getDateTimeForBoxing($("#goodsOutPlanDatePicker").val()));
}
//#endregion

//#region Get Recipe Info
function getProductInfo(text) {
    var array = new Array();
    if (text.length > 0) {
        $.post(
            GETPRODUCTBYCODE_URL,
            {
                code: text
            },
            function (data) {
                $("#ProductCode").removeClass("spinner");
                if (data.length > 0) {
                    var arrayList = [];
                    _.each(data, function (item) {
                        arrayList.push({
                            "description": item.description == null ? "" : item.description,
                            "value": item.code == null ? "" : item.code,
                            "id": item.id == null ? "" : item.id,
                            "label": item.code == null ? "" : item.code,
                            "code": item.code == null ? "" : item.code,
                            "name": item.name == null ? "" : item.name
                        });

                    });
                    $("#ProductCode").autocomplete({
                        dataType: "json",
                        source: arrayList,
                        select: function (event, ui) {
                            var prodCode = ui.item.code;
                            $("#ProductCode").val(prodCode);
                            $("#iProductId").val(ui.item.id);
                            $("#ProductName").val(ui.item.name + " - " + ui.item.description);
                        }
                    });
                }
            });
    } else {
        hideLoader();
    }
    return array;
}
//#endregion

//#region Next button functionality
function nextGoodsOutPlanForm() {
    acceptForm(0);
}
//#endregion

//#region Submit Form
function acceptForm(bClose) {
    if (!$("#goodsOutPlanForm").valid()) {
        return;
    }

    //Checking Product Code
    if ($("#ProductName").val() === "") {
        showError("Please enter valid product code");
        $("#ProductCode").val("");
        return;
    }

    updateTimePickerOnSubmit();

    showLoader();
    $.ajax({
        type: "POST",
        url: SUBMITGOODSOUTPLAN_URL,
        data: $("#goodsOutPlanForm").serialize(),
        success: function () {
            showSuccess("Goods Out Plan record saved successfully");
            hideLoader();

            if (bClose == 1) {
                $("#goodsOutPlanModal").modal("hide");
                window.location = GOODSOUTPLANLIST;
            }
            else {
                $('#vBatchCode').attr('disabled', 'disabled');
                $('#ddlCategory').attr('disabled', 'disabled');
                $('#goodsOutPlanDatePicker').attr('disabled', 'disabled');

                $('#vCategory').val($('#ddlCategory').val());

                $("#ProductCode").val("");
                $("#ProductName").val("");
                $("#vCategoryDescription").val("");
                $("#vQuantity").val("");
                $("#iNumberOfMeals").val("");
                $("#iProductId").val("");
                $("#iGoodsOutPlanDetailId").val("");
            }

            $('#hasChanges').val('1');
        },
        error: function () {
            showError("Something went wrong while saving record");
            hideLoader();
        }
    });
}
//#endregion

//#region Delete Addition from the Listview
function deleteDetailItem(id) {

    showLoader();

    var remainingRecords = $('#tableGoodsOutPlan tr').length;

    var obj = {
        iGoodsOutPlanDetailId: id,
        iRemainingRecords: remainingRecords
    }

    $.ajax({
        type: "GET",
        url: DELETEGOODSOUTPLANITEM,
        data: obj,
        success: function (result) {

            if (result.deletedRecordID === -1) {
                hideLoader();
                showError("Deletion Failed.");
                return;
            }

            $('#tableGoodsOutPlan tr[data-id="' + id + '"]').remove();

            showSuccess("Detail item successfully removed.");
            $('#hasChanges').val('1');
            hideLoader();
        },
        error: function (error) {
            hideLoader();
        }
    });
}
//#endregion

//#region Confirm Event
function ConfirmDeletion(id) {
    $("#dialog-confirm").html("Are you sure you want to delete this record?");

    // Define the Dialog and its properties.
    $("#dialog-confirm").dialog({
        modal: true,
        title: "Delete Confirmation",
        height: 150,
        width: 300,
        buttons: {
            "Yes": function () {
                $(this).dialog('close');
                deleteDetailItem(id);
            },
            "No": function () {
                $(this).dialog('close');
            }
        }
    });
}
//#endregion