﻿//#region Document Ready
$(document).ready(function () {
    hideLoader();
});
//#endregion

//#region Edit Attendance Record
function editAttendanceRecord(e) {
    showLoader();

    var createdDateTime = getDate($($(e).find("#createdDateTime")[0]).attr('value'));
    var employeeId = $($(e).find("#employeeId")[0]).attr('value');


    var url = EDITATTENDANCERECORD;
    $.get(url, { iEmployeeId: employeeId, Sys_CreatedDateTime: createdDateTime }, function (data) {
        $("#editAttendanceRec").html(data);
        $("#editAttendanceRecModal").modal("show");
        hideLoader();
    });
}
//#endregion
function getDate(dateParam) {
    var date = dateParam.split(" ")[0].split('-');
    return date[1] + "-" + date[0] + "-" + date[2];
}