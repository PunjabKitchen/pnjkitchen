﻿$(document).ready(function () {
    $("#dClockInTimePicker").datetimepicker({
        format: "HH:mm"
    });
    $("#dClockOutTimePicker").datetimepicker({
        format: "HH:mm"
    });
    $("#dLunchClockInTimePicker").datetimepicker({
        format: "HH:mm"
    });
    $("#dLunchClockOutTimePicker").datetimepicker({
        format: "HH:mm"
    });
});

function submitAttendanceRecForm() {

    if (!$("#attendanceRecordForm").valid()) {
        return;
    }

    if (!getMorningTimeDifference() || !getLunchTimeDifference()) {
        return;
    }

    showLoader();
    var dataObj = {
        iEmployeeId: $("#iEmployeeId").val(),
        dClockInTime: $("#dClockInTimePicker").val(),
        dClockOutTime: $("#dClockOutTimePicker").val(),
        dLunchClockInTime: $("#dLunchClockInTimePicker").val(),
        dLunchClockOutTime: $("#dLunchClockOutTimePicker").val(),
        dDateTime: readDate("dDateTime")
    }
    $.ajax({
        type: "GET",
        url: SUBMITATTENDANCERECORD,
        data: dataObj,
        success: function (result) {
            showSuccess("Attendance Record Modified Successfully.");
            $("#editAttendanceRecModal").modal("hide");
            showInfo("Waiting for interface to load.");
            location.reload();
        },
        error: function (error) {
            hideLoader();
        }
    });
}
function readDate(id) {
    var date = $("#" + id).val().split(" ")[0].split("/");
    return date[0] + "-" + date[1] + "-" + date[2];
}

function getMorningTimeDifference() {
    var startTime = moment($("#dClockOutTimePicker").val(), 'HH:mm:ss');
    var finishTime = moment($("#dClockInTimePicker").val(), 'HH:mm:ss');

    var timeDuration = moment.duration(finishTime.diff(startTime));
    var timeHours = parseInt(timeDuration.asHours());
    var timeMinutes = parseInt(timeDuration.asMinutes()) - timeHours * 60;

    if (isNaN(timeHours) || isNaN(timeMinutes)) {
        return false;
    }
    if ((timeHours <= 0 && timeMinutes <= 0) && startTime != "00:00:00" && finishTime != "00:00:00") {
        showInfo("Morning Clock In Time should be greater than Morning Clock Out time");
        return false;
    } else {
        return true;
    }
}

function getLunchTimeDifference() {
    var startTime = moment($("#dLunchClockOutTimePicker").val(), 'HH:mm:ss');
    var finishTime = moment($("#dLunchClockInTimePicker").val(), 'HH:mm:ss');

    var timeDuration = moment.duration(finishTime.diff(startTime));
    var timeHours = parseInt(timeDuration.asHours());
    var timeMinutes = parseInt(timeDuration.asMinutes()) - timeHours * 60;

    if (isNaN(timeHours) || isNaN(timeMinutes)) {
        return false;
    }
    if ((timeHours <= 0 && timeMinutes <= 0) && startTime != "00:00:00" && finishTime != "00:00:00") {
        showInfo("Lunch Clock In Time should be greater than Lunch Clock Out time");
        return false;
    } else {
        return true;
    }
}