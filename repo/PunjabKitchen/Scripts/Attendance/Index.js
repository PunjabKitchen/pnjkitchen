﻿//Document Ready
$(document).ready(function () {
    updateCurrentAttendanceStatus();
    //$("#FromDatePicker").datetimepicker({
    //    format: "DD-MM-YYYY"
    //});
    //$("#ToDatePicker").datetimepicker({
    //    format: "DD-MM-YYYY"
    //});
    //$('#ToDatePicker').data("DateTimePicker").date(moment());
    //$('#FromDatePicker').data("DateTimePicker").date(moment().subtract(1, 'months'));
});

function getAttendances() {
    //validate
    var reqData = {
        FromDate: $("#FromDatePicker").val() + " 00:00:00",
        ToDate: $("#ToDatePicker").val() + " 00:00:00"
    }
    $("#FromDate").val(getDate("FromDatePicker").toISOString());
    $("#ToDate").val(getDate("ToDatePicker").toISOString());

    $("#AttendanceForm").submit();
    //$.get('/Attendance/Index?FromDate=' + getDate("FromDate").toISOString() + "&ToDate=" + getDate("ToDate").toISOString(), function (result) {
    //    console.log(result);
    //
    //});
    //showLoader();
    //$.ajax({
    //    type: 'GET',
    //    url: SEARCHATTENDANCE_URL+"?FromDate=" + reqData.FromDate + "&ToDate=" + reqData.ToDate,
    //    contentType: 'application/json; charset=utf-8',
    //    //data: JSON.stringify(reqData),
    //    success: function(){},
    //    error: function(){}
    //});
    //$.ajax({
    //    type: "GET",
    //    url: "Attendance/Search",
    //    data:reqData,
    //    success: function (data) {
    //
    //    },
    //    error: function (error) {
    //
    //    }
    //});
}

//Function to update clock in and clouk out buttons
function updateCurrentAttendanceStatus() {
    showLoader();
    $.ajax({
        type: "GET",
        url: GETTODAYATTENDANCEDETAIL,
        success: function (data) {
            data.IsAlreadyCheckedIn ? disableButton("clockInBtn") : enableButton("clockInBtn");
            data.IsAlreadyCheckedOut ? disableButton("clockOutBtn") : enableButton("clockOutBtn");
            hideLoader();
        },
        error: function (error) {

            hideLoader();
        }
    });
}

function clockIn() {

    if (!$("#clockOutBtn").is(':disabled')) {
        showInfo("Clock Out first before Clocking In.");
        return;
    }

    showLoader();
    $.ajax({
        type: "POST",
        url: CLOCKIN,
        success: function (data) {
            showSuccess("Successfully Clocked in.");
            disableButton("clockInBtn");
            showInfo("Waiting for interface to reload.");
            location.reload();
        },
        error: function () {
            hideLoader();
        }
    });
}
function clockOut() {
    showLoader();
    $.ajax({
        type: "POST",
        url: CLOCKOUT,
        success: function (data) {
            showSuccess("Successfully Clocked Out.");
            disableButton("clockOutBtn");
            showInfo("Waiting for interface to reload.");
            location.reload();
        },
        error: function () {
            hideLoader();
        }
    });
}

function disableButton(id) {
    $("#" + id).attr("disabled", "disabled");
    $("#" + id).addClass("pointerNone");

}
function enableButton(id) {
    $("#" + id).removeAttr("disabled");
    $("#" + id).removeClass("pointerNone");

}
function getDate(id) {
    var date = $("#" + id).val().split("-");
    return new Date(date[1] + "-" + date[0] + "-" + date[2]);
}