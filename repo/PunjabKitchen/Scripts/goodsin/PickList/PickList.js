﻿//#region Document Ready
$(document).ready(function () {
    //Triggering the Product Code's autocomplete property initially to bring it in running condition.
    initialAutocompleteRequest();

    $('#notifyModalForPicklist').click(function () {
        showLoader();
        var url = NOTIFICATIONCREATE_URL;
        $.get(url, function (data) {
            $('#stockPickListModal').modal('hide');
            $('#newNotification').html(data);
            $('#newNotificationModal').modal('show');
            hideLoader();
        });
    });

    $(".unavailable-Product").hide();
});
//#endregion

//#region Show picklist dialogue
function showPickListDialog() {
    showLoader();
    $.ajax({
        type: "GET",
        url: STOCKPICKLISTMODAL_URL,
        success: function (data) {
            $("#CookingPickListModalBody").html(data);
            $("#stockPickListModal").modal("show");
            hideLoader();
        },
        error: function () {
            hideLoader();
        }
    });

    //Triggering the Product Code's autocomplete property initially to bring it in running condition.
    initialAutocompleteRequest();
}
//#endregion

//#region Product Code Auto Complete
$("#productAutoComplete").on("keyup", function (e) {
    var productCode = document.getElementById("productAutoComplete").value;
    var code = (e.keyCode ? e.keyCode : e.which);
    if (code === 13) { //Enter keycode
        return;
    }
    $("#productDescription").val("");
    $("#productAutoComplete").addClass("spinner");
    getProductInfo($(this).val());
});
//#endregion

//#region Autocomplete code's initial Request
function initialAutocompleteRequest() {
    $.post(GETPRODUCTBYCODE_URL,
            {
                code: "0"
            },
            function (data) {
                hideLoader();
                if (data.length > 0) {
                    var arrayList = [];
                    _.each(data, function (item) {
                        arrayList.push({
                            "description": item.description == null ? "" : item.description,
                            "value": item.code == null ? "" : item.code,
                            "id": item.id == null ? "" : item.id,
                            "label": item.code == null ? "" : item.code,
                            "code": item.code == null ? "" : item.code,
                            "name": item.name == null ? "" : item.name
                        });
                        //toastr.success(item.code);
                    });
                    $("#productAutoComplete").autocomplete({
                        dataType: "json",
                        source: arrayList,
                        select: function (event, ui) {
                            var prodCode = ui.item.code;
                            $("#ProductCode").val(prodCode);
                            $("#iProductId").val(ui.item.id);
                            $("#productDescription").val(ui.item.name);
                            getAllowedQuanity(prodCode);
                        }
                    });
                }
            });

    $.post(GETBATCHBYCODE_URL,
            {
                code: "0"
            },
            function (data) {
                hideLoader();
                if (data.length > 0) {
                    var arrayList = [];
                    _.each(data, function (item) {
                        arrayList.push({
                            "description": item.description == null ? "" : item.description,
                            "value": item.code == null ? "" : item.code,
                            "id": item.id == null ? "" : item.id,
                            "label": item.code == null ? "" : item.code,
                            "code": item.code == null ? "" : item.code,
                            "name": item.name == null ? "" : item.name,
                            "weight": item.weight == null ? "" : item.weight
                        });
                        //toastr.success(item.code);
                    });
                    $("#vBatchCode").autocomplete({
                        dataType: "json",
                        source: arrayList,
                        select: function (event, ui) {
                            var batchCode = ui.item.value;
                            $("#vBatchCode").val(batchCode);
                            $("#ProductDescription").val(ui.item.name);
                            $("#CookingPlanId").val(ui.item.id);
                        }
                    });
                }
            });
}
//#endregion

//#region Number Validations
$("#iQuantity").keypress(function (evt) {
    var self = $(this);
    self.val(self.val().replace(/[^\d].+/, ""));
    if ((evt.which < 48 || evt.which > 57) && (evt.which !== 8) && (evt.keyCode !== 46) && (evt.keyCode !== 37) && (evt.keyCode !== 39)) {
        evt.preventDefault();
    }

    if ((this.value.length === 6) && (evt.which !== 8) && (evt.keyCode !== 46) && (evt.keyCode !== 37) && (evt.keyCode !== 39)) {
        return false;
    }
    return true;
});
$("#vBatchCode").keypress(function (evt) {
    var self = $(this);
    self.val(self.val().replace(/[^\d].+/, ""));
    if ((evt.which < 48 || evt.which > 57) && (evt.which !== 8) && (evt.keyCode !== 46) && (evt.keyCode !== 37) && (evt.keyCode !== 39)) {
        evt.preventDefault();
    }

    if ((this.value.length === 6) && (evt.which !== 8) && (evt.keyCode !== 46) && (evt.keyCode !== 37) && (evt.keyCode !== 39)) {
        return false;
    }
    return true;
});
//#endregion

//#region Get Product Info
function getProductInfo(text) {
    var array = new Array();
    if (text.length > 0) {
        $.post(GETPRODUCTBYCODE_URL,
            {
                code: text
            },
            function (data) {
                $("#productAutoComplete").removeClass("spinner");
                if (data.length > 0) {
                    var arrayList = [];
                    _.each(data, function (item) {
                        arrayList.push({
                            "description": item.description == null ? "" : item.description,
                            "value": item.code == null ? "" : item.code,
                            "id": item.id == null ? "" : item.id,
                            "label": item.code == null ? "" : item.code,
                            "code": item.code == null ? "" : item.code,
                            "name": item.name == null ? "" : item.name
                        });
                        //toastr.success(item.code);
                    });
                    $("#productAutoComplete").autocomplete({
                        dataType: "json",
                        source: arrayList,
                        select: function (event, ui) {
                            var prodCode = ui.item.code;
                            $("#ProductCode").val(prodCode);
                            $("#iProductId").val(ui.item.id);
                            $("#productDescription").val(ui.item.name);
                            getAllowedQuanity(prodCode);
                        }
                    });
                }
            });
    } else {
        hideLoader();
    }
    return array;
}
//#endregion

//#region Get Allowed Quantity
function getAllowedQuanity(productCode) {
    showLoader();
    $.ajax({
        type: "GET",
        url: GETALLOWEDQUANTITY,
        data: { productCode: productCode },
        success: function (data) {
            if (data.result === 0) {
                $(".unavailable-Product").show();
            } else {
                $(".unavailable-Product").hide();
            }

            hideLoader();
            $("#AllowedQty").removeClass("hide").text(" (Available: " + data.result + ")");
            $("#iQuantity").attr("allowedQty", data.result);
        },
        error: function () {
            hideLoader();
            $("#AllowedQty").addClass("hide");
        }
    });
}
//#endregion

//#region Batch Code Auto Complete
$("#vBatchCode").on("keyup", function (e) {
    var batchCode = document.getElementById("vBatchCode").value;
    var code = (e.keyCode ? e.keyCode : e.which);
    if (code === 13) { //Enter keycode
        hideLoader();
        return;
    }
    $("#vBatchCode").addClass("spinner");
    getBatchInfo($(this).val());
});
//#endregion

//#region Get Recipe Info from its Batch
function getBatchInfo(text) {
    var array = new Array();
    if (text.length > 0) {
        $.post(GETBATCHBYCODE_URL,
            {
                code: text
            },
            function (data) {
                $("#vBatchCode").removeClass("spinner");
                if (data.length > 0) {
                    var arrayList = [];
                    _.each(data, function (item) {
                        arrayList.push({
                            "description": item.description == null ? "" : item.description,
                            "value": item.code == null ? "" : item.code,
                            "id": item.id == null ? "" : item.id,
                            "label": item.code == null ? "" : item.code,
                            "code": item.code == null ? "" : item.code,
                            "name": item.name == null ? "" : item.name,
                            "weight": item.weight == null ? "" : item.weight
                        });
                        //toastr.success(item.code);
                    });
                    $("#vBatchCode").autocomplete({
                        dataType: "json",
                        source: arrayList,
                        select: function (event, ui) {
                            var batchCode = ui.item.value;
                            $("#vBatchCode").val(batchCode);
                            $("#ProductDescription").val(ui.item.name);
                            $("#CookingPlanId").val(ui.item.id);
                        }
                    });
                }
            });
    } else {
        hideLoader();
    }
    return array;
}
//#endregion

//#region Pick Product
function validatePickedItem() {

    if ($("#productDescription").val() === "") {
        $("#productAutoComplete").val("");
        toastr.error("Please enter valid product code");
        return false;
    }

    else if (!$("#ProductPickList").valid()) {
        return false;
    }

    else if ($("#iQuantity").attr("allowedqty") <= 0) {
        showError("Allowed Quantity should be greater than 0");
        return false;
    }
    else if (parseInt($("#iQuantity").val()) <= 0 || parseInt($("#iQuantity").val()) > parseInt($("#iQuantity").attr("allowedqty"))) {
        showError("Quantity should be greater than 0 and less than " + $("#iQuantity").attr("allowedqty"));
        return false;
    }
    return true;
}

function savePickListitem() {
    //Validate Picked Product
    if (!validatePickedItem()) {
        return;
    }

    //Checking Batch Code
    if ($("#productDescription").val() === "") {
        showError("Please enter a valid product code.");
        $("#vBatchCode").val("");
        return;
    }

    showLoader();
    var obj = {
        iProductId: $("#iProductId").val(),
        iQuantity: $("#iQuantity").val(),
        iUnitId: $("#iUnitId").val(),
        vBatchCode: $("#vBatchCode").val(),
        iCookingPlanId: $("#CookingPlanId").val()
    }

    $.ajax({
        type: "POST",
        url: STOCKPICKLISTSAVE_URL,
        data: obj,
        success: function (data) {
            showSuccess("Item saved successfully!");

            $("#vBatchCode").attr("disabled", "disabled");
            $("#AllowedQty").addClass("hide");

            hideLoader();
            $("#pickListTable").find("tbody")
                .append("<tr><td data-id=" + data.iPickListId + ">" +
                    $("#ProductCode").val() + "</td><td>" +
                    $("#productDescription").val() + "</td><td>" +
                    $("#iQuantity").val() + " " + $("#iUnitId :selected").text() + "</td></tr>");
            $("#iProductId").val("");
            $("#iQuantity").val("");
            $("#ProductCode").val("");
            $("#productDescription").val("");
            $("#productAutoComplete").val("");
        },
        error: function () {
            hideLoader();
        }
    });
}
//#endregion