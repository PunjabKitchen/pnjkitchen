﻿//#region Document Ready
$(document).ready(function () {
    $(".stockLocationListItem").on('change', function () {
        var clickedElem = $(this);
        $('.stockLocationListItem').removeClass("selected");
        $(this).addClass('selected');
        getStockPointItems($("#" + clickedElem.attr('id')).val());
    });
});
//#endregion

//#region Get Stock Point Items
function getStockPointItems(location) {
    showLoader();
    $.ajax({
        type: "GET",
        url: GETSTOCKPOINTS_URL + "?stockLocation=" + location,
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            hideLoader();
            var selectedItemId = $('.stockLocationListItem.selected').attr('id').split("_")[1];
            var storagePointElem = ".stockPoint_" + selectedItemId;
            $(storagePointElem).empty();
            $(storagePointElem).append($('<option>').text("Select Stock Point").attr('value', "undefined"));
            $.each(data, function (i, value) {
                $(storagePointElem).append($('<option>').text(value.vStoragePoint).attr('value', value.iStockId));
            });

            $(".stockPoint_" + selectedItemId).removeClass('error');
            $("#stockLoc_" + selectedItemId).removeClass('error');
        },
        error: function () {
            hideLoader();
        }
    });
}
//#endregion

//#region Submit Stock Point Item

function submitStockPointItem(prodId) {
    showLoader();
    var locationId = $(".stockPoint_" + prodId).val();
    //check validity
    if (locationId === "undefined") {
        $(".stockPoint_" + prodId).addClass('error');
        $("#stockLoc_" + prodId).addClass('error');
        hideLoader();
        return;
    }
    $.ajax({
        type: "GET",
        url: SUBMITSTOCKLOC_URL + "?productId=" + prodId + "&stockLocId=" + locationId,
        contentType: "application/json; charset=utf-8",
        success: function () {
            $(".stockPoint_" + prodId).closest('tr').remove();
            $('.stockSelectItem').prop('selectedIndex', 0);
            for (var i = 0; i < $(".stockSelectPointItem").length; i++) {
                $($(".stockSelectPointItem")[i]).empty();
                $($(".stockSelectPointItem")[i]).append($('<option>').text("Select Stock Point").attr('value', "undefined"));
            }
            hideLoader();
        },
        error: function () {
            hideLoader();
        }
    });
}
//#endregion
