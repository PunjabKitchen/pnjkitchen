﻿//#region Document Ready Function
$(document).ready(function () {
    $('#bContainsAllergen_hidden').val().toLowerCase() === "true" ? $('#bContainsAllergen').prop('checked', true) : $('#bContainsAllergen').prop('checked', false);
    $('#bIntakeChecksCompleted_hidden').val().toLowerCase() === "true" ? $('#bIntakeChecksCompleted').prop('checked', true) : $('#bIntakeChecksCompleted').prop('checked', false);
    $('#bQualityApproved_hidden').val().toLowerCase() === "true" ? $('#bQualityApproved').prop('checked', true) : $('#bQualityApproved').prop('checked', false);
    
    initializeDatePicker();
    getUniqueProductCode();

    $("#iUnitWeight").keypress(function (evt) {
        var self = $(this);
        self.val(self.val().replace(/[^\d].+/, ""));
        if ((evt.which < 48 || evt.which > 57) && (evt.which !== 8) && (evt.keyCode !== 46) && (evt.keyCode !== 37) && (evt.keyCode !== 39)) {
            evt.preventDefault();
        }

        if ((this.value.length === 6) && (evt.which !== 8) && (evt.keyCode !== 46) && (evt.keyCode !== 37) && (evt.keyCode !== 39)) {
            return false;
        }
        return true;
    });
    $("#iQuantity").keypress(function (evt) {
        var self = $(this);
        self.val(self.val().replace(/[^\d].+/, ""));
        if ((evt.which < 48 || evt.which > 57) && (evt.which !== 8) && (evt.keyCode !== 46) && (evt.keyCode !== 37) && (evt.keyCode !== 39)) {
            evt.preventDefault();
        }

        if ((this.value.length === 6) && (evt.which !== 8) && (evt.keyCode !== 46) && (evt.keyCode !== 37) && (evt.keyCode !== 39)) {
            return false;
        }
        return true;
    });

});
//#endregion

//#region Initialize Date Picker
function initializeDatePicker() {
    $("#dCCP1aBestBeforeDatePicker").datetimepicker({
        format: "DD-MM-YYYY",
        minDate: moment()
    });
}
//#endregion

//#region Get Unique Product Code
function getUniqueProductCode() {
    //Update Product Type Id
    var productTypeId = $("#iProductTypeId").val();
    $("#productTypId").val(productTypeId);
    $("#vProductCode").attr("readonly", "readonly");

    //Get Product Code
    getUniqueCode("vProductCode", 1, productTypeId);
}
//#endregion

//#region Submit Goods In Products
function submitGoodsInProduct(closeModal) {
    if (!$("#createGoodsInForm").valid()) {
        return;
    }
    $('#bContainsAllergen').prop('checked') ? $('#bContainsAllergen_hidden').val(true) : $('#bContainsAllergen_hidden').val(false);
    $('#bIntakeChecksCompleted').prop('checked') ? $('#bIntakeChecksCompleted_hidden').val(true) : $('#bIntakeChecksCompleted_hidden').val(false);
    $('#bQualityApproved').prop('checked') ? $('#bQualityApproved_hidden').val(true) : $('#bQualityApproved_hidden').val(false);

    var bestBeforeDate = $("#dCCP1aBestBeforeDatePicker").val();
    $("#dCCP1aBestBefore").val(getDateTimeForBestBefore(bestBeforeDate));

    if (closeModal) {
        $('#newGoodsIntakeModal').modal('hide');
        $("#addMoreProduct").val(true);
        $("#createGoodsInForm").submit();
    }
    else {
        showLoader();
        $.ajax({
            type: "POST",
            url: CREATEGOODSINAJAX_URL,
            data: $("#createGoodsInForm").serialize(), // serializes the form's elements.
            success: function () {
                $('#createGoodsInForm').trigger("reset");
                hideLoader();
                getUniqueProductCode();
            },
            error: function () {
                hideLoader();
            }
        });
    }
}
//#endregion

//#region Datetime functions
function getDateTimeForBestBefore(date) {

    var day, month, year;

    //If the format has changed in update mode.
    if (date[2] == '/' || date[1] == '/') {
        month = date.split("/")[0];
        day = date.split("/")[1];
        year = date.split("/")[2];
    }
    else {
        day = date.split("-")[0];
        month = date.split("-")[1];
        year = date.split("-")[2];
    }

    return year + "-" + month + "-" + day;
}
//#endregion