﻿//#region Document Ready Function
$(document).ready(function () {
    //Update Vehicle Checks Upon Product types
    $('#iProductTypeId').on('change', function () {
        updateVehicleChecks();
    });
    updateVehicleChecks();
    updateFormStructure();
    updateCheckBoxes();

    $("#ccp1aTemp").keypress(function (evt) {
        if ((evt.which < 48 || evt.which > 57) && (evt.which !== 8) && (evt.which !== 45) && (evt.which !== 46) && (evt.keyCode !== 46) && (evt.keyCode !== 37) && (evt.keyCode !== 39)) {
            evt.preventDefault();
        }

        if ((this.value.length === 6) && (evt.which !== 8) && (evt.which !== 45) && (evt.which !== 46) && (evt.keyCode !== 46) && (evt.keyCode !== 37) && (evt.keyCode !== 39)) {
            return false;
        }
        return true;
    });
});
//#endregion

//#region Accept goods in form
//accept goods in form
function acceptForm() {
    $("#bVehicleChecksAccepted").val(true);
    updateCheckBoxesonSubmit();
}
//#endregion

//#region Reject form
//reject form
function rejectForm() {
    $("#bVehicleChecksAccepted").val(true);
    updateCheckBoxesonSubmit();
}
//#endregion

//#region Update Check Boxes on Submit
//Update Check Boxes on Submit
function updateCheckBoxesonSubmit() {
    $('#vehicleInternalChk').prop('checked') ? $('#bVehicleInternalCheck_hidden').val(true) : $('#bVehicleInternalCheck_hidden').val(false);
    $('#bRawMaterial').prop('checked') ? $('#bRawMaterial_hidden').val(true) : $('#bRawMaterial_hidden').val(false);
    $('#ccp1a').prop('checked') ? $('#ccp1a_hidden').val(true) : $('#ccp1a_hidden').val(false);
    $('#ccp1b').prop('checked') ? $('#ccp1b_hidden').val(true) : $('#ccp1b_hidden').val(false);
}
//#endregion

//#region Update Vehicle Checks
//Update Vehicle Checks
function updateVehicleChecks() {
    var productTyp = $('#iProductTypeId :selected').text().toLowerCase();
    switch (productTyp) {
        case "ambient goods":
        case "other": {
            $("#ccp1a_check").show();
            $("#ccp1b_check").hide();
            return;
        }
        case "chilled goods":
        case "factored goods":
        case "frozen goods":
        case "prepared foods":
        case "raw meat": {
            $("#ccp1a_check").show();
            $("#ccp1a_check").show();
            return;
        }
        case "cleaning chemicals":
        case "packaging": {
            $("#ccp1a").hide();
            $("#ccp1b").hide();
            return;
        }
    }
}
//#endregion

//#region update Check Boxes
//update Check Boxes
function updateCheckBoxes() {
    $('#bVehicleInternalCheck_hidden').val().toLowerCase() == "true" ? $('#vehicleInternalChk').prop('checked', true) : $('#vehicleInternalChk').prop('checked', false);
    $('#bRawMaterial_hidden').val().toLowerCase() == "true" ? $('#bRawMaterial').prop('checked', true) : $('#bRawMaterial').prop('checked', false);
    $('#ccp1a_hidden').val().toLowerCase() == "true" ? $('#ccp1a').prop('checked', true) : $('#ccp1a').prop('checked', false);
    $('#ccp1b_hidden').val().toLowerCase() == "true" ? $('#ccp1b').prop('checked', true) : $('#ccp1b').prop('checked', false);
}
//#endregion

//#region Update Form Structure
//Update Form Structure
function updateFormStructure() {
    //$('#vehicleInternalChk').prop('checked', false);
    var vehicleChecksCompleted = $("#bVehicleChecksAccepted").val();
    if (vehicleChecksCompleted) {
        $("#vehicleCheckSection").hide();
        $("#goodsIntakeSection").show();
        $("#vSupplier").attr('disabled', true);
        $("#iProductTypeId").attr('disabled', true);
    }
    else {
        $("#vehicleCheckSection").show();
        $("#goodsIntakeSection").hide();
        $("#vSupplier").attr('disabled', false);
        $("#iProductTypeId").attr('disabled', false);
    }

}
//#endregion

//#region binding goodsInModal click
$('#goodsInModal').click(function () {
    openGoodsInModal();
});
//#endregion

//#region Open Goods In Modal
function openGoodsInModal() {
    showLoader();
    var url = CREATEGOODSIN_URL + "/" + $("#currentGoodsIn").val();
    $.get(url, function (data) {
        $('#newGoodsIn').html(data);
        $('#newGoodsIntakeModal').modal('show');
        hideLoader();
    });
}
//#endregion

//#region Finish Adding Goods In
function finishAddingGoodsIn() {
    showLoader();
    var url = FINISHGOODSIN + "/" + $("#currentGoodsIn").val();
    $.get(url, function (data) {
        if (data.result === true) {
            window.location.href = GOODSTOSTOCK + "?goodsInId=" + $("#currentGoodsIn").val();
        } else {
            showError("Please add any goods-in item to proceed !");
        }
        hideLoader();
    });
    return false;
}
//#endregion