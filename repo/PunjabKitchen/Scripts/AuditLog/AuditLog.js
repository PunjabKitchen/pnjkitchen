﻿//#region Document Ready
$(document).ready(function () {
    //initialize datepicker
    initializeDatePicker();

    //For initial loading of the filter selected by default
    //showLoader();
    //updateView();
});
//#endregion

//#region Initialize Date Picker
function initializeDatePicker() {

    $("#FromDateTimePicker").datetimepicker({
        format: "DD-MM-YYYY"
    });

    $("#ToDateTimePicker").datetimepicker({
        format: "DD-MM-YYYY"
    });

    //Setting datePicker values
    var currentDate = moment().format("DD-MM-YYYY");
    var previousMonthDate = moment().subtract(1, 'months').calendar();
    var formattedPreviousMonthDate = moment(previousMonthDate).format("DD-MM-YYYY");
    $("#ToDateTimePicker").val(currentDate);
    $("#FromDateTimePicker").val(formattedPreviousMonthDate);
}
//#endregion

//#region Update View
function updateView() {
    showLoader();
    var searchObj = {
        Module: $("#Module").find(":selected").val(),
        User: $("#User").find(":selected").val(),
        StartDateTime: getDate($("#FromDateTimePicker").val()),
        EndDateTime: getDate($("#ToDateTimePicker").val())
    }
    $.ajax({
        type: "GET",
        url: GETAUDITLOGBYFILTER,
        data: searchObj,
        success: function (data) {
            $("#auditLogsListBody").html(data);
            hideLoader();
        },
        error: function (error) {
            hideLoader();
        }
    });
}
//#endregion