﻿//#region Document Ready
$(document).ready(function () {
    if ($("#bQuantityShiftCheck").val() === "True") {
        $("#bQuantityShiftChecker").prop("checked", true);
    }
});
//#endregion

//#region Function Submit Form
function submitStockLocForm() {
    showLoader();

    var opIsChecked = $('#bQuantityShiftChecker').prop('checked');
    $("#bQuantityShiftCheck").val(opIsChecked);

    $.ajax({
        type: "GET",
        url: SUBMITSTOCKPOINT_URL,
        data: $("#stockLocationForm").serialize(),
        success: function (result) {
            $("#newStockandStorageModal").modal("hide");
            hideLoader();
            window.location = STOCKANDSTORAGELISTVIEW;
        },
        error: function (error) {
            hideLoader();
        }
    });
}
//#endregion

//#region Number fields validations
$("#iInspectedQuantity").keypress(function (evt) {
    var self = $(this);
    self.val(self.val().replace(/[^\d].+/, ""));
    if ((evt.which < 48 || evt.which > 57) && (evt.which !== 8) && (evt.keyCode !== 46) && (evt.keyCode !== 37) && (evt.keyCode !== 39)) {
        evt.preventDefault();
    }

    if ((this.value.length === 6) && (evt.which !== 8) && (evt.keyCode !== 46) && (evt.keyCode !== 37) && (evt.keyCode !== 39)) {
        return false;
    }
    return true;
});
//#endregion

//#region Function Validate Form
function validateForm() {
    if (!$("#stockLocationForm").valid()) {
        return;
    }
    showLoader();
    $.ajax({
        type: "GET",
        url: VALIDATESTOCKPOINT_URL,
        data: $("#stockLocationForm").serialize(),
        success: function (data) {
            hideLoader();
            if (data.result === true) {
                submitStockLocForm();
            }
            else {
                alert("Same stock point already exist in this location!");
            }
        },
        error: function (error) {
            hideLoader();
        }
    });
}
//#endregion

//#region Confirm Event
function Confirm() {
    var opIsChecked = $('#bQuantityShiftChecker').prop('checked');
    $("#bQuantityShiftCheck").val(opIsChecked);

    if ($("#bQuantityShiftCheck").val() === "true")
    {
        $("#dialog-confirm").html("Are you sure?");

        // Define the Dialog and its properties.
        $("#dialog-confirm").dialog({
            modal: true,
            title: "Confirmation",
            height: 150,
            width: 300,
            buttons: {
                "Yes": function () {
                    $(this).dialog('close');
                    validateForm();
                },
                "No": function () {
                    $(this).dialog('close');
                    showInfo("Switch the option back to \"No\" if you don't want to avail it.");
                }
            }
        });
    } else {
        validateForm();
    }
}
//#endregion