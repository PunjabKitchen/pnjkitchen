﻿//#region Document Ready
$(document).ready(function() {
    NumberValidation();
});
//#endregion

//#region Number Validations
function NumberValidation() {
    $("#iMaxLoad").keypress(function (evt) {
        var self = $(this);
        self.val(self.val().replace(/[^\d].+/, ""));
        if ((evt.which < 48 || evt.which > 57) && (evt.which !== 8) && (evt.keyCode !== 46) && (evt.keyCode !== 37) && (evt.keyCode !== 39)) {
            evt.preventDefault();
        }

        if ((this.value.length === 6) && (evt.which !== 8) && (evt.keyCode !== 46) && (evt.keyCode !== 37) && (evt.keyCode !== 39)) {
            return false;
        }
        return true;
    });
}
//#endregion

//#region Function Submit Daily Truck Inspection Check
function submitDailyTruckCheck() {
    if (!$("#DailyTruckInspectionForm").valid()) {
        return;
    } else {
        updateChecks();
        showLoader();
        $.ajax({
            type: "POST",
            url: SUBMITDAILYTRUCKINSPECTIONCHECK,
            data: $("#DailyTruckInspectionForm").serialize(),
            success: function () {
                showSuccess("Daily Truck Inspection Check saved successfully.");
                $("#assignNotificationModal").modal('hide');
                window.location = window.NOTIFICATIONSLIST;
                hideLoader();
            },
            error: function () {
                showError("Something went wrong while saving the check.");
                hideLoader();
            }
        });
    }
}
//#endregion

//#region Update Checks
function updateChecks() {
    var check = $("#ForkArms").prop('checked');
    $("#bForkArms").val(check);
    check = $("#Mast").prop('checked');
    $("#bMast").val(check);
    check = $("#CarriagePlate").prop('checked');
    $("#bCarriagePlate").val(check);
    check = $("#LiftChainAndLubrication").prop('checked');
    $("#bLiftChainAndLubrication").val(check);
    check = $("#WheelAndTyres").prop('checked');
    $("#bWheelAndTyres").val(check);
    check = $("#BackRest").prop('checked');
    $("#bBackRest").val(check);
    check = $("#SeatAndSeatBelt").prop('checked');
    $("#bSeatAndSeatBelt").val(check);
    check = $("#Steering").prop('checked');
    $("#bSteering").val(check);
    check = $("#LightAndIndicators").prop('checked');
    $("#bLightAndIndicators").val(check);
    check = $("#HornAndBeeper").prop('checked');
    $("#bHornAndBeeper").val(check);
    check = $("#MastController").prop('checked');
    $("#bMastController").val(check);
    check = $("#HandAndParkingBreak").prop('checked');
    $("#bHandAndParkingBreak").val(check);
    check = $("#DrivingAndServiceBreak").prop('checked');
    $("#bDrivingAndServiceBreak").val(check);
    check = $("#Hydraulics").prop('checked');
    $("#bHydraulics").val(check);
    check = $("#OilLevel").prop('checked');
    $("#bOilLevel").val(check);
    check = $("#FuelAndPower").prop('checked');
    $("#bFuelAndPower").val(check);
    check = $("#Batteries").prop('checked');
    $("#bBatteries").val(check);
    check = $("#LPG").prop('checked');
    $("#bLPG").val(check);
}
//#endregion