﻿//#region Document Ready
$(document).ready(function () {

    //initialize datepicker
    initializeDatePicker();

    //Update 
    updateDailyWeelyCheck();

    //For initial loading of the filter selected by default
    showLoader();
    updateHeader();
    updateView();

    $('#DailyWeeklyChecksItems').on("change", function () {
        resetData();
    });
});
//#endregion

//#region Initialize Date Picker
function initializeDatePicker() {

    $("#FromDateTimePicker").datetimepicker({
        format: "DD-MM-YYYY"
    });

    $("#ToDateTimePicker").datetimepicker({
        format: "DD-MM-YYYY"
    });

    //Setting datePicker values
    var currentDate = moment().format("DD-MM-YYYY");
    var previousMonthDate = moment().subtract(1, 'months').calendar();
    var formattedPreviousMonthDate = moment(previousMonthDate).format("DD-MM-YYYY");
    $("#ToDateTimePicker").val(currentDate);
    $("#FromDateTimePicker").val(formattedPreviousMonthDate);
}
//#endregion

//#region Reset Data
function resetData() {
    $("#dailyWeeklyChecksBody").html("");
    updateHeader('Click Search to load data...');
}
//#endregion

//#region Update Daily Weekly Checks
function updateDailyWeelyCheck() {

    $("#searchFilterBtn").click(function () {
        showLoader();

        //Update Header
        updateHeader();

        //Update View
        updateView();
    });
}
//#endregion

//#region Update View
function updateView(page) {
    var currentPage = 1;
    if (page != undefined)
        currentPage = page;

    var searchObj = {
        Id: $('#DailyWeeklyChecksItems').find(":selected").val(),
        StartDateTime: getDate($("#FromDateTimePicker").val()),
        EndDateTime: getDate($("#ToDateTimePicker").val()),
        page: currentPage
    }
    $.ajax({
        type: "GET",
        url: GETGOODSOUTDAILYCHECKS,
        data: searchObj,
        success: function (data) {
            $("#dailyWeeklyChecksBody").html(data);
            hideLoader();

            $('.liPage').on("click", function () {
                var nPage = $(this)[0].children[0].text;
                if (nPage == "…")
                    return false;

                if (nPage == "»" || nPage == "«") {
                    var selected = $('.pagination').find('.active');
                    var rawPage = $(selected)[0].children[0].text;
                    if (nPage == "»")
                        nPage = parseInt(rawPage) + parseInt(1);
                    else if (nPage == "«")
                        nPage = parseInt(rawPage) - parseInt(1);
                }
                showLoader();
                updateView(nPage);
                return false;
            });
        },
        error: function (error) {
            hideLoader();
        }
    });
}
//#endregion

//#region Update Header
function updateHeader(str) {
    if (str != undefined)
        $("#selectedCheckHeading").text(str);
    else {
        str = $('#DailyWeeklyChecksItems').find(":selected").text();
        $("#selectedCheckHeading").text(str);
    }
}
//#endregion