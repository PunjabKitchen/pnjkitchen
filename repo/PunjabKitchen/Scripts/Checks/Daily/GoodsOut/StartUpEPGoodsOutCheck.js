﻿//#region Function Submit Start Up EP Goods Out Check
function submitStartUpEPGoodsOutCheck() {
    if (!$("#StartupEPGoodsOutForm").valid()) {
        return;
    } else {
        updateChecks();
        showLoader();
        $.ajax({
            type: "POST",
            url: SUBMITSTARTUPEPGOODSOUTCHECK,
            data: $("#StartupEPGoodsOutForm").serialize(),
            success: function () {
                showSuccess("Start Up Check - EP Goods Out saved successfully.");
                $("#assignNotificationModal").modal('hide');
                window.location = window.NOTIFICATIONSLIST;
                hideLoader();
            },
            error: function () {
                showError("Something went wrong while saving the check.");
                hideLoader();
            }
        });
    }
}
//#endregion

//#region Update Checks
function updateChecks() {
    var check = $("#SwitchLightsOn").prop('checked');
    $("#bSwitchLightsOn").val(check);
    check = $("#CheckElectric").prop('checked');
    $("#bCheckElectric").val(check);
    check = $("#SwitchOnComputer").prop('checked');
    $("#bSwitchOnComputer").val(check);
    check = $("#CheckBlade").prop('checked');
    $("#bCheckBlade").val(check);
    check = $("#CheckHoldingFreezers").prop('checked');
    $("#bCheckHoldingFreezers").val(check);
    check = $("#CheckDisplayTemperature").prop('checked');
    $("#bCheckDisplayTemperature").val(check);
    check = $("#RingOffice").prop('checked');
    $("#bRingOffice").val(check);
    check = $("#CollectOrder").prop('checked');
    $("#bCollectOrder").val(check);
    check = $("#CheckAllStaffComply").prop('checked');
    $("#bCheckAllStaffComply").val(check);
    check = $("#CheckAllStaffSuitably").prop('checked');
    $("#bCheckAllStaffSuitably").val(check);
}
//#endregion