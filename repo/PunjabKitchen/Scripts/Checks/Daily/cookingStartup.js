﻿//#region Document Ready Function
$(document).ready(function () {
    NumberValidations();
    $(".bladeRecordsRow").hide();
    //#region Submit DailyCheck
    $('#submitDailychecks').click(function () {
        if (!$('#submitDailychecks').closest('form').valid()) {
            return;
        }
        var valueArray = [];
        var y = ',';
        $("#tblDailyChecks tr").each(function () {
            var elemId = $(this).closest('tr').children('td:first').text();
            var isBool;
            if (elemId !== '') {
                var id = '#check_' + elemId;
                var value;
                if ($(id).attr("type") === "number") {
                    value = $(id).val();
                    isBool = false;
                } else {
                    $(this).find(id).each(function () {
                        value = $(this).prop('checked');
                    });
                    isBool = true;
                }
                var rowValue = value + y + elemId + y + isBool;
                valueArray.push(rowValue);

            }
        });

        //If the Blade check is true.
        if ($("#check_15").prop('checked') === true)
        {
            var rowValue;

            //Red Blade
            if ($("#redBlade").val() !== "")
            { rowValue = $("#redBlade").val() + y + 16 + y + false; }
            else
            { rowValue = 0 + y + 16 + y + false; }
            valueArray.push(rowValue);

            //Green Blade
            if ($("#greenBlade").val() !== "")
            { rowValue = $("#greenBlade").val() + y + 17 + y + false; }
            else
            { rowValue = 0 + y + 17 + y + false; }
            valueArray.push(rowValue);

            //Black Blade
            if ($("#blackBlade").val() !== "")
            { rowValue = $("#blackBlade").val() + y + 18 + y + false; }
            else
            { rowValue = 0 + y + 18 + y + false; }
            valueArray.push(rowValue);
        }

        showLoader();
        callAJAXToSaveDailyCheck(valueArray);
    });
    //#endregion
});
//#endregion

//#region Number validation
function NumberValidations() {
    $("#redBlade").keypress(function (evt) {
        var self = $(this);
        self.val(self.val().replace(/[^\d].+/, ""));
        if ((evt.which < 48 || evt.which > 57) && (evt.which !== 8) && (evt.keyCode !== 46) && (evt.keyCode !== 37) && (evt.keyCode !== 39)) {
            evt.preventDefault();
        }

        if ((this.value.length === 6) && (evt.which !== 8) && (evt.keyCode !== 46) && (evt.keyCode !== 37) && (evt.keyCode !== 39)) {
            return false;
        }
        return true;
    });
    $("#greenBlade").keypress(function (evt) {
        var self = $(this);
        self.val(self.val().replace(/[^\d].+/, ""));
        if ((evt.which < 48 || evt.which > 57) && (evt.which !== 8) && (evt.keyCode !== 46) && (evt.keyCode !== 37) && (evt.keyCode !== 39)) {
            evt.preventDefault();
        }

        if ((this.value.length === 6) && (evt.which !== 8) && (evt.keyCode !== 46) && (evt.keyCode !== 37) && (evt.keyCode !== 39)) {
            return false;
        }
        return true;
    });
    $("#blackBlade").keypress(function (evt) {
        var self = $(this);
        self.val(self.val().replace(/[^\d].+/, ""));
        if ((evt.which < 48 || evt.which > 57) && (evt.which !== 8) && (evt.keyCode !== 46) && (evt.keyCode !== 37) && (evt.keyCode !== 39)) {
            evt.preventDefault();
        }

        if ((this.value.length === 6) && (evt.which !== 8) && (evt.keyCode !== 46) && (evt.keyCode !== 37) && (evt.keyCode !== 39)) {
            return false;
        }
        return true;
    });
}
//#endregion

//#region Blade Check
function bladeCheck() {
    if (document.getElementById("check_15").checked) {
        $(".bladeRecordsRow").show();
    }
    else if (!document.getElementById("check_15").checked) {
        $(".bladeRecordsRow").hide();
    }
}
//#endregion

//#region Save Daily Check
function callAJAXToSaveDailyCheck(valueArray) {
    $.ajax({
        type: "POST",
        url: SaveDailyCheck_URL,
        data: $.param({ valueArray: valueArray }, true),
        success: function (data) {
            showSuccess("Saved successfully");
            window.location = window.NOTIFICATIONSLIST;
            $('#assignNotificationModal').modal('hide');
            hideLoader();
        },
        error: function () {
            hideLoader();
        }
    });
}
//#endregion