﻿//#region Document Ready
$(document).ready(function () {
    NumberValidations();
});
//#endregion

//#region Number validation
function NumberValidations() {
    $("#Fridge1Temp").keypress(function (evt) {
        if ((evt.which !== 46) && (evt.which < 48 || evt.which > 57) && (evt.which !== 8) && (evt.keyCode !== 46) && (evt.keyCode !== 37) && (evt.keyCode !== 39)) {
            evt.preventDefault();
        }

        if ((this.value.length === 6) && (evt.which !== 8) && (evt.keyCode !== 46) && (evt.keyCode !== 37) && (evt.keyCode !== 39)) {
            return false;
        }
        return true;
    });
    $("#Fridge2Temp").keypress(function (evt) {
        if ((evt.which !== 46) && (evt.which < 48 || evt.which > 57) && (evt.which !== 8) && (evt.keyCode !== 46) && (evt.keyCode !== 37) && (evt.keyCode !== 39)) {
            evt.preventDefault();
        }

        if ((this.value.length === 6) && (evt.which !== 8) && (evt.keyCode !== 46) && (evt.keyCode !== 37) && (evt.keyCode !== 39)) {
            return false;
        }
        return true;
    });
    $("#Fridge3Temp").keypress(function (evt) {
        if ((evt.which !== 46) && (evt.which < 48 || evt.which > 57) && (evt.which !== 8) && (evt.keyCode !== 46) && (evt.keyCode !== 37) && (evt.keyCode !== 39)) {
            evt.preventDefault();
        }

        if ((this.value.length === 6) && (evt.which !== 8) && (evt.keyCode !== 46) && (evt.keyCode !== 37) && (evt.keyCode !== 39)) {
            return false;
        }
        return true;
    });
}
//#endregion

//#region Function Submit Canteen fridge Temperature Record
function sbmtCantFrdgTempRec() {
    if (!$("#CanteenFridgeTempRecordForm").valid()) {
        return;
    } else {
        showLoader();
        $.ajax({
            type: "POST",
            url: SUBMITCANTEENFRIDGETEMPRECORD_URL,
            data: $("#CanteenFridgeTempRecordForm").serialize(),
            success: function () {
                showSuccess("Canteen Fridge Temperature record saved successfully");
                $("#assignNotificationModal").modal('hide');
                window.location = window.NOTIFICATIONSLIST;
                hideLoader();
            },
            error: function () {
                showError("Something went wrong while saving record");
                hideLoader();
            }
        });
    }
}
//#endregion