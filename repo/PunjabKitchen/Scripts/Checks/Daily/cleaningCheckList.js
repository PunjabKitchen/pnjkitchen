﻿//#region Document Ready Function
$(document).ready(function () {
    //#region Submit DailyCheck
    $('#submitDailychecks').click(function () {

        var valueArray = [];
        var y = ',';
        $("#tblDailyChecks tr").each(function () {
            var areadItemId;
            $(this).find("#areaItemId").each(function () {
                areadItemId = $(this).val();
            });

            var areaid;
            $(this).find("#areaId").each(function () {
                areaid = $(this).val();
            });

            if (areadItemId != '') {

                var valueOp = '#OP_' + areadItemId;
                var opIsChecked;
                $(this).find(valueOp).each(function () {
                    opIsChecked = $(this).prop('checked');
                });

                var valueRc = '#RC_' + areadItemId;
                var rcIsChecked;
                $(this).find(valueRc).each(function () {
                    rcIsChecked = $(this).prop('checked');
                });

                var issue = "";
                $(this).find("#txtIssue_" + areadItemId).each(function () {
                    issue = $(this).val();
                });

                var action = "";
                $(this).find("#txtAction_" + areadItemId).each(function () {
                    action = $(this).val();
                });

                var rowValue;
                rowValue = areaid + y + areadItemId + y + opIsChecked + y + rcIsChecked + y + issue + y + action;
                valueArray.push(rowValue);
            }
        });
        showLoader();
        callAJAXToSaveDailyCheck(valueArray);

    });
    //#endregion
});
//#endregion

//#region Save Daily Check
function callAJAXToSaveDailyCheck(valueArray) {

    $.ajax({
        type: "POST",
        url: SaveCleaningChecklisDaily_URL,
        data: $.param({ valueArray: valueArray }, true),
        success: function (data) {
            showSuccess("Saved successfully");
            window.location = window.NOTIFICATIONSLIST;
            $('#assignNotificationModal').modal('hide');
            hideLoader();
        },
        error: function () {
            hideLoader();
        }
    });

}
//#endregion