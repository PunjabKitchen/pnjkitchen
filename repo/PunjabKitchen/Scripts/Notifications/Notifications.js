﻿//#region Document Ready Function
$(document).ready(function () {

});
//#endregion

//#region Function to submit user action on notification
function performAction(itemClicked, id, type) {
    var notificationText = $(itemClicked).children("#subject").text();
    showNotificationActionDialog(notificationText, id, type);
}
//#endregion

//#region Submit Notification
function submitNotification() {
    if (($("#vSubject").val() === "") && ($("#vDescription").val() === "")) {
        $("#createNotificationForm").valid();
        return;
    }

    showLoader();
    $.ajax({
        type: "GET",
        url: SAVENOTIFICATION,
        data: $("#createNotificationForm").serialize(),
        success: function (result) {
            hideLoader();
            if (result.role === "Admin") {
                showSuccess("Notification assigned successfully.");
                window.location = HOME_URL;
            }
        },
        error: function (error) {
            hideLoader();
        }
    });
};
//#endregion

//#region Submit Notification
function submitNotificationForPicklist() {
    if (($("#vSubject").val() === "") && ($("#vDescription").val() === "")) {
        $("#createNotificationForm").valid();
        return;
    }

    showLoader();
    $.ajax({
        type: "GET",
        url: SAVENOTIFICATION,
        data: $("#createNotificationForm").serialize(),
        success: function (result) {
            hideLoader();
            if (result.role === "Staff") {
                $('#newNotificationModal').modal('hide');
                $('#stockPickListModal').modal('show');
                showSuccess("Notification successfully sent to Admin.");
            }
        },
        error: function (error) {
            hideLoader();
        }
    });
};
//#endregion

//#region Function to submit user action on notification checks
function showCheckActionDialog(clickedId, screen) {
    showLoader();
    var notificationText, notificationType;

    switch (clickedId) {
        case "2":
            notificationText = "Startup Check - LR Cooking Room";
            notificationType = 2;
            break;
        case "3":
            notificationText = "Daily Cleaning Checklist";
            notificationType = 3;
            break;
        case "4":
            notificationText = "Canteen Fridge Temperature Records";
            notificationType = 4;
            break;
        case "5":
            notificationText = "Shoe Cleaning Check";
            notificationType = 5;
            break;
        case "11":
            notificationText = "Daily Truck Inspection Check";
            notificationType = 11;
            break;
        case "12":
            notificationText = "Goods Out Freezer Unit Check";
            notificationType = 12;
            break;
        case "13":
            notificationText = "Start Up EP Goods Out";
            notificationType = 13;
            break;
    }


    $.ajax({
        type: "GET",
        dataType: "json",
        url: GETLATESTNOTIFICATIONID,
        // ReSharper disable once UsageOfPossiblyUnassignedValue
        data: { notificationTypeID: notificationType },
        success: function (data) {
            if (data !== 0) {
                showNotificationActionDialog(notificationText, data, notificationType, screen);
            }
            else {
                showError("No Notification Record Found.");
            }
        }
    });
}

function canPerformShoesCleaningCheck() {
    $.ajax({
        type: "GET",
        url: CANPERFORMSHOESCHECK,
        success: function (data) {

            if (data.result === true) {
                showCheckActionDialog("5");
            } else {
                toastr.error("Show Cleaning check already performed");
            }
        },
        error: function () { }
    });
}

//Show Notification Action Dialog
function showNotificationActionDialog(dialogBodyText, id, type, screen) {
    hideLoader();
    $("#notificationText").text(dialogBodyText);
    $("#selectedNotification").attr("data-id", id);
    $("#selectedNotification").attr("data-type", type);
    $("#selectedNotification").attr("data-text", dialogBodyText);
    //check if this is shoe cleaning type then show its dialog
    if (type === 5) {
        $("#ShoeCleaningCheck").modal("show");
    }
    else if (type === 3) {
        showAreaSelectionScreen();
    }
    else {
        if (screen==="Menu") {
            assignNotification();
        } else {
            $("#notificationAction").modal("show");
            if ($("#selectedNotification").attr("data-type")==="12") {
                $("#assignNotificationButton").attr("onclick", "canPerformGoodsOutFreezerUnitCheck()");
            }  
        }            
    }
}

//Show Areas Selection Screen
function showAreaSelectionScreen() {
    var url = GETCLEANINGCHECKAREAS_URL;
    $.get(url, function (data) {
        $("#modal-cleaningAreas").html(data);
        $("#cleaningCheckAreasModal").modal("show");
    });
}

//Submit Shoe cleaning check
function submitShoeCleaning() {
    showLoader();
    if ($("#shoeCleaningCheckBox").is(":checked")) {
        $.ajax({
            type: "POST",
            url: SAVESHOECLEANINGCHECKDAILY,
            data: {},
            success: function (data) {
                showSuccess("Shoe Cleaned Check Performed");
                showInfo("Wait Updating View ...");
                window.location = NOTIFICATIONSLIST;
                hideLoader();
            },
            error: function () {
                hideLoader();
            }
        });
    } else {
        hideLoader();
    }
}

//Assign Notification to user
function assignNotification() {
    showLoader();
    $("#notificationAction").modal("hide");
    $.ajax({
        type: "POST",
        url: ASSIGNNOTIFICATION,
        data: { notificationId: $("#selectedNotification").attr("data-id") },
        success: function (data) {
            if (data.result === true) {
                showSuccess("Task Assigned to you successfully");
                showLoader();
                getNotificationModal($("#selectedNotification").attr("data-type"), 1, $("#selectedNotification").attr("data-text"));
            } else {
                hideLoader();
                showError("Task already assigned or performed!");
            }
        },
        error: function () {
            hideLoader();
        }
    });
}

function performDailyCleaningCheck(areaId) {
    showLoader();
    $("#cleaningCheckAreasModal").modal("hide");
    getNotificationModal(3, areaId, "");
}

//Get Notification Modal
function getNotificationModal(notificaitonTypeIdParam, areaIdParam, textParam) {
    $.ajax({
        type: "GET",
        url: GETNOTIFICATIONMODEL,
        data: { notificationTypeId: notificaitonTypeIdParam, areaId: areaIdParam },
        success: function (dataa) {
            //2 - Start Up Check - LR Cooking Room
            //3- Daily Cleaning Checklist
            //4- Canteen Fridge temperature Check
            //5- Shoe Cleaning Checklist
            //6 - Goods in
            //7- Stock take
            //8- cooking
            //9- goods out
            //10- packing
            //11- Daily Truck Inspection Check
            //12- Goods Out Freezer Unit Check
            if (notificaitonTypeIdParam == "2"
                || notificaitonTypeIdParam == "3"
                || notificaitonTypeIdParam == "4"
                || notificaitonTypeIdParam == "5"
                || notificaitonTypeIdParam == "11"
                || notificaitonTypeIdParam == "12"
                || notificaitonTypeIdParam == "13") {
                $("#notifiactionModalBody").html(dataa);
                $("#assignNotificationModal").modal("show");
            } else {
                switch (notificaitonTypeIdParam) {
                    case "6":
                        window.location = window.GOODSINDELIVERIES;
                        break;
                    case "7":
                        window.location = "";
                        break;
                    case "8":
                        window.location.href = window.GOODSINDELIVERIES;
                        break;
                    case "9":
                        window.location = "";
                        break;
                    case "10":
                        window.location = "";
                    case "1":
                        if (textParam.toLowerCase().includes("threshold"))
                            window.location = window.GOODSINLIST_URL;
                        break;
                    default:
                        window.location = "";
                }
                showInfo("Wait Redirecting ...");
                hideLoader();
            }

            hideLoader();
        },
        error: function () {
            window.location = window.NOTIFICATIONSLIST;
            showInfo("Wait Redirecting ...");
            hideLoader();
        }
    });
}

//Cancel/Un-assign Notification
function cancelNotificationCheck() {

    showLoader();
    $.ajax({
        type: "POST",
        url: CANCELNOTIFICATIONCHECK,
        data: { notificationId: $("#selectedNotification").attr("data-id") },
        success: function (data) {
            showSuccess("Check Un Assigned");
            $("#assignNotificationModal").modal("hide");
            window.location = window.NOTIFICATIONSLIST;
            hideLoader();
        },
        error: function () {
            hideLoader();
        }
    });

}

//#region Function: Check if Goods Out Freezer Check Can Be Performed
function canPerformGoodsOutFreezerUnitCheck() {
    showLoader();
    $.ajax({
        type: "POST",
        url: CANPERFORMGOODSOUTFREEZERUNITCHECK,
        success: function (data) {
            if (data.value === true) {
                showLoader();
                assignNotification();
            } else {
                $("#notificationAction").modal("hide");
                hideLoader();
                showError("Task either already assigned for this time period (Morning/Evening) or it won't be available until next time period (Evening).");
            }
        },
        error: function () {
            hideLoader();
        }
    });
}
//#endregion