﻿//#region Document Ready
$(document).ready(function () {
    var id = $("#hdnLineId").val();
    var ident = "#line" + id;
    $(ident).addClass("active");

    $(".packingPlanItem").click(function (e) {
        var tag = e.target.nodeName;
        if (tag === 'IMG') { }
        else {
            if ($(e.target).parent().hasClass('canEdit')) {
                var id = $(e.target).parent().find('.packingPlanId').val();
                PackingPlanEdit(id);
            }
        }
    });

    isCookingPlanExist('canCreatePlan');
});
//#endregion

//#region Create PACKING plan
$('#CreatePackingPackingPlan').click(function () {
    var canCreatePlan = $('#canCreatePlan').val().toLowerCase();
    if (canCreatePlan == 'true') {
        showLoader();
        var url = CREATEPACKINGPLAN;
        $.get(url, function (data) {
            $('#packingPlanBody').html(data);
            $('#packingPlanModal').modal('show');
            hideLoader();
        });
    }
    else
        showError(cookingPlanNotFoundMsg);
});
//#endregion

//#region Edit packing Plan
function PackingPlanEdit(idParam) {

    $("#createPackingProdBtn").addClass('disabled');
    var url = PACKINGPLANEDIT_URL;
    $.get(url, { id: idParam }, function (data) {
        $("#packingPlanBody").html(data);
        $("#packingPlanModal").modal("show");
    });

}
//#endregion