﻿//#region Document Ready
$(document).ready(function () {

    if (!($("#PackingProductionPlanId").val() > 0)) {
        getUniqueCode("vBatchNo", 3);
    }

    //Triggering the Product Code's autocomplete property initially to bring it in running condition.
    initialAutocompleteRequest();

    $("#vBatchNo").hide();

    if ($("#PackingProductionPlanId").val() > 0) {
        $("#nextButton").hide();
    }

    //Check for product of task
    if ($("#isTask").val() === "" || $("#isTask").val().toLowerCase() === "false") {
        $("#isTask").val(false);
        showProductSection();
    } else {
        $("#isTask").val(true);
        showTaskSection();
    }

    //update cancel Button
    if ($("#isCancelled").val() === "true") {
        $("#cancelBtn").prop("disabled", true);
    }

    //initialize datepicker
    initializeDatePicker();

    //update required fields
    updateRequiredFields();

    NumberValidation();

    //Disabling inputs in edit mode
    disableFields();
});
//#endregion

//#region Number Validations
function NumberValidation() {
    $("#ProductCode").keypress(function (evt) {
        var self = $(this);
        self.val(self.val().replace(/[^\d].+/, ""));
        if ((evt.which < 48 || evt.which > 57) && (evt.which !== 8) && (evt.keyCode !== 46) && (evt.keyCode !== 37) && (evt.keyCode !== 39)) {
            evt.preventDefault();
        }

        if ((this.value.length === 6) && (evt.which !== 8) && (evt.keyCode !== 46) && (evt.keyCode !== 37) && (evt.keyCode !== 39)) {
            return false;
        }
        return true;
    });
    $("#iWeight").keypress(function (evt) {
        var self = $(this);
        self.val(self.val().replace(/[^\d].+/, ""));
        if ((evt.which < 48 || evt.which > 57) && (evt.which !== 8) && (evt.keyCode !== 46) && (evt.keyCode !== 37) && (evt.keyCode !== 39)) {
            evt.preventDefault();
        }

        if ((this.value.length === 6) && (evt.which !== 8) && (evt.keyCode !== 46) && (evt.keyCode !== 37) && (evt.keyCode !== 39)) {
            return false;
        }
        return true;
    });
    $("#iQuantity").keypress(function (evt) {
        var self = $(this);
        self.val(self.val().replace(/[^\d].+/, ""));
        if ((evt.which < 48 || evt.which > 57) && (evt.which !== 8) && (evt.keyCode !== 46) && (evt.keyCode !== 37) && (evt.keyCode !== 39)) {
            evt.preventDefault();
        }

        if ((this.value.length === 6) && (evt.which !== 8) && (evt.keyCode !== 46) && (evt.keyCode !== 37) && (evt.keyCode !== 39)) {
            return false;
        }
        return true;
    });
    $("#iPacksPerMin").keypress(function (evt) {
        var self = $(this);
        self.val(self.val().replace(/[^\d].+/, ""));
        if ((evt.which < 48 || evt.which > 57) && (evt.which !== 8) && (evt.keyCode !== 46) && (evt.keyCode !== 37) && (evt.keyCode !== 39)) {
            evt.preventDefault();
        }

        if ((this.value.length === 6) && (evt.which !== 8) && (evt.keyCode !== 46) && (evt.keyCode !== 37) && (evt.keyCode !== 39)) {
            return false;
        }
        return true;
    });
    $("#iQuantityCompleted").keypress(function (evt) {
        var self = $(this);
        self.val(self.val().replace(/[^\d].+/, ""));
        if ((evt.which < 48 || evt.which > 57) && (evt.which !== 8) && (evt.keyCode !== 46) && (evt.keyCode !== 37) && (evt.keyCode !== 39)) {
            evt.preventDefault();
        }

        if ((this.value.length === 6) && (evt.which !== 8) && (evt.keyCode !== 46) && (evt.keyCode !== 37) && (evt.keyCode !== 39)) {
            return false;
        }
        return true;
    });
    $("#iPackers").keypress(function (evt) {
        var self = $(this);
        self.val(self.val().replace(/[^\d].+/, ""));
        if ((evt.which < 48 || evt.which > 57) && (evt.which !== 8) && (evt.keyCode !== 46) && (evt.keyCode !== 37) && (evt.keyCode !== 39)) {
            evt.preventDefault();
        }

        if ((this.value.length === 6) && (evt.which !== 8) && (evt.keyCode !== 46) && (evt.keyCode !== 37) && (evt.keyCode !== 39)) {
            return false;
        }
        return true;
    });
}
//#endregion

//#region Product Code's Autocomplete Request
function initialAutocompleteRequest() {
    $.post(GETBYCODEFORPACKING_URL,
            {
                code: "0"
            },
            function (data) {
                hideLoader();
                if (data.length > 0) {
                    var arrayList = [];
                    _.each(data, function (item) {
                        arrayList.push({
                            "description": item.description == null ? "" : item.description,
                            "value": item.code == null ? "" : item.code,
                            "id": item.id == null ? "" : item.id,
                            "label": item.code == null ? "" : item.code,
                            "code": item.code == null ? "" : item.code,
                            "name": item.name == null ? "" : item.name
                        });
                    });
                    $("#ProductCode").autocomplete({
                        dataType: "json",
                        source: arrayList,
                        select: function (event, ui) {
                            var prodCode = ui.item.code;
                            $("#ProductCode").val(prodCode);
                            $("#recipeId").val(ui.item.id);
                            $("#ProductDescription").val(ui.item.name);
                        }
                    });
                }
            });
}
//#endregion

//#region Function Update Fields Disablity
function disableFields() {
    if ($("#PackingProductionPlanId").val() > 0) {
        $("#dPackingPlanPicker").attr("disabled", true);
        $("#iLineNo").attr("disabled", true);
        $("#ProductCode").attr("disabled", true);
        $("#iQuantity").attr("disabled", true);
        $("#iWeight").attr("disabled", true);
        $("#vBatchNo").attr("disabled", true);
        $("#tRunTime").attr("disabled", true);
        $("#tRunTimePicker").attr("disabled", true);
        $("#tPlannedStart").attr("disabled", true);
        $("#tPlannedStartPicker").attr("disabled", true);
        $("#tActualRunTime").attr("disabled", true);
        $("#tActualRunTimePicker").attr("disabled", true);
        $("#tPlannedFinish").attr("disabled", true);
        $("#tPlannedFinishPicker").attr("disabled", true);
        $("#iPacksPerMin").attr("disabled", true);
        $("#vBatchNo").val($("#iBatchNo").val());

        packingTaskButtonAction = function () {
            return false;
        }
    }
}
//#endregion

//#region Create Cooking Plan Button Action
$("#createPackingProdBtn").on("click", function () {
    if ($("#PackingProductionPlanId").val() > 0) { return; }
    $("#isTask").val(false);
    showProductSection();
    updateRequiredFields();
});
//#endregion

//#region Function Show Task Section
function showTaskSection() {

    $(".packingProdItems").hide().removeClass("active");
    $(".packingTaskItems").show().addClass("active");
    $("#createPackingTaskBtn").addClass("active");
    $("#createPackingProdBtn").removeClass("active");
}
//#endregion

//#region Function Show product Section
function showProductSection() {

    $(".packingProdItems").show();
    $(".packingTaskItems").hide().removeClass("active");
    $("#createPackingProdBtn").addClass("active");
    $("#createPackingTaskBtn").removeClass("active");
}
//#endregion

//#region Create Cooking Task Button Action
function packingTaskButtonAction() {
    $("#isTask").val(true);
    showTaskSection();
    updateRequiredFields();
}
//#endregion

//#region initialize Date Picker
function initializeDatePicker() {

    $("#dPackingPlanPicker").datetimepicker({
        format: "DD-MM-YYYY HH:mm:ss",
        minDate: moment()
    });

    $("#tRunTimePicker").datetimepicker({
        format: "HH:mm"
    });

    $("#tActualRunTimePicker").datetimepicker({
        format: "HH:mm"
    });
    $("#tPlannedFinishPicker").datetimepicker({
        format: "HH:mm"
    });
    $("#tActualStartPicker").datetimepicker({
        format: "HH:mm"
    });
    $("#tBreakStartPicker").datetimepicker({
        format: "HH:mm"
    });
    $("#tBreakFinishPicker").datetimepicker({
        format: "HH:mm"
    });
    $("#tActualFinishPicker").datetimepicker({
        format: "HH:mm"
    });
    $("#tPlannedStartPicker").datetimepicker({
        format: "HH:mm"
    });

    if ($("#PackingProductionPlanId").val() > 0) {
        $("#dPackingPlanPicker").val($("#dPackingPlan").val());
        $("#tRunTimePicker").val(moment($('#tRunTime').val(), 'HH:mm').format('HH:mm'));
        $("#tActualRunTimePicker").val(moment($('#tActualRunTime').val()).format("HH:mm"));
        $("#tPlannedStartPicker").val(moment($("#tPlannedStart").val()).format("HH:mm"));
        $("#tPlannedFinishPicker").val(moment($('#tPlannedFinish').val()).format('HH:mm'));

        if ($("#tActualFinish").val() === "") {
            $("#tActualFinishPicker").val(moment($('#tPlannedFinish').val()).format('HH:mm'));
        } else {
            $("#tActualFinishPicker").val(moment($('#tActualFinish').val()).format('HH:mm'));
        }
        if ($('#tActualStart').val() === "") {
            $("#tActualStartPicker").val(moment($('#tPlannedStart').val()).format('HH:mm'));
        } else {
            $("#tActualStartPicker").val(moment($('#tActualStart').val()).format('HH:mm'));
        }

        if ($('#tBreakStart').val() === "") {
            $("#tBreakStartPicker").val("00:00");
        } else {
            $("#tBreakStartPicker").val(moment($('#tBreakStart').val()).format('HH:mm'));
        }

        if ($('#tBreakFinish').val() === "") {
            $("#tBreakFinishPicker").val("00:00");
        } else {
            $("#tBreakFinishPicker").val(moment($('#tBreakFinish').val()).format('HH:mm'));
        }
    }
}
//#endregion

//#region Product Code Auto Complete
$("#ProductCode").on("keyup", function (e) {
    var productCode = document.getElementById("ProductCode").value;
    var code = (e.keyCode ? e.keyCode : e.which);
    if (code === 13) { //Enter keycode
        return;
    }
    $("#ProductDescription").val("");
    $("#ProductCode").addClass("spinner");
    getRecipeInfo($(this).val());
});
//#endregion

//#region Get Recipe Info
function getRecipeInfo(text) {
    var array = new Array();
    if (text.length > 0) {
        $.post(GETBYCODEFORPACKING_URL,
            {
                code: text
            },
            function (data) {
                $("#ProductCode").removeClass("spinner");
                if (data.length > 0) {
                    var arrayList = [];
                    _.each(data, function (item) {
                        arrayList.push({
                            "description": item.description == null ? "" : item.description,
                            "value": item.code == null ? "" : item.code,
                            "id": item.id == null ? "" : item.id,
                            "label": item.code == null ? "" : item.code,
                            "code": item.code == null ? "" : item.code,
                            "name": item.name == null ? "" : item.name
                        });
                    });
                    $("#ProductCode").autocomplete({
                        dataType: "json",
                        source: arrayList,
                        select: function (event, ui) {
                            var batchCode = ui.item.batchNo;
                            $("#ProductCode").val(batchCode);
                            $("#iCookingPlanId").val(ui.item.id);
                            $("#ProductDescription").val(ui.item.name);
                        }
                    });
                }
            });
    } else {
        hideLoader();
    }
    return array;
}
//#endregion

//#region Function Submit Form
function submitPackingPlan() {
    if (!$("#PackingPlanForm").valid()) {
        if ($("#tRunTimePicker").val() === "") {
            $("#tRunTimePicker").addClass("error");
        }
        if ($("#tPlannedStartPicker").val() === "") {
            $("#tPlannedStartPicker").addClass("error");
        }
        if ($("#tPlannedFinishPicker").val() === "") {
            $("#tPlannedFinishPicker").addClass("error");
        }
        return;
    } else {
        if ($("#PackingProductionPlanId").val() > 0) {
            if (!getActualTimeDifference())
            { return }
        }
        if ($("#PackingProductionPlanId").val() > 0) {
            if (!getBreakeTimeDifference())
            { return }
        }
        if (!getPlannedTimeDifference())
        { return }


        if ($("#createPackingProdBtn").hasClass("active")) {
            //Checking Batch Code
            if ($("#ProductDescription").val() === "") {
                showError("Please enter valid Product Code.");
                $("#ProductCode").val("");
                return;
            }
        }

        if ($("#createPackingTaskBtn").hasClass("active")) {
            $("#ProductCode").val("");
            $("#ProductDescription").val("");
            $("#iWeight").val("");
            $("#iQuantity").val("");
            $("#iPacksPerMin").val("");
        }

        //Check Time Slot Availability
        checkTimeSlotAvailability("Submit");
    }
}
//#endregion

//#region Function: "Next" Click Event
function nextPackingPlanForm() {
    if (!$("#PackingPlanForm").valid()) {
        if ($("#tRunTimePicker").val() === "") {
            $("#tRunTimePicker").addClass("error");
        }
        if ($("#tPlannedStartPicker").val() === "") {
            $("#tPlannedStartPicker").addClass("error");
        }
        if ($("#tPlannedFinishPicker").val() === "") {
            $("#tPlannedFinishPicker").addClass("error");
        }
        return;
    } else {
        if ($("#PackingProductionPlanId").val() > 0) {
            if (!getActualTimeDifference())
            { return }
        }
        if ($("#PackingProductionPlanId").val() > 0) {
            if (!getBreakeTimeDifference())
            { return }
        }
        if (!getPlannedTimeDifference())
        { return }

        //validating Quantity
        if (!validateQuantity()) {
            return;
        }

        if ($("#createPackingProdBtn").hasClass("active")) {
            //Checking Batch Code
            if ($("#ProductDescription").val() === "") {
                showError("Please enter valid Product Code.");
                $("#ProductCode").val("");
                return;
            }
        }

        //Check Time Slot Availability
        checkTimeSlotAvailability("Next");
    }
}
//#endregion

//#region Function: Resets fields for next record
function fieldsResetForNextRecord() {
    $("#cookingPlanId").val("");
    $("#ProductCode").val("");
    $("#ProductDescription").val("");
    $("#iWeight").val("");
    $("#iQuantity").val("");
    $("#vBatchNo").val("");
    $("#tRunTimePicker").val("");
    $("#tPlannedStartPicker").val("");
    $("#tActualRunTimePicker").val("");
    $("#tPlannedFinishPicker").val("");
    $("#iPacksPerMin").val("");
    $("#vTaskDescription").val("");
    //document.getElementById("iLineNo").disabled = true;
}
//#endregion

//#region Submit Data
function submitData(button) {
    showLoader();

    $("#tPackingPlan").val($("#tPackingPlanPicker").val());
    $("#tRunTime").val($("#tRunTimePicker").val());
    $("#tPlannedStart").val($("#tPlannedStartPicker").val());
    $("#tPlannedFinish").val($("#tPlannedFinishPicker").val());
    $("#tActualStart").val($("#tActualStartPicker").val());
    $("#tActualFinish").val($("#tActualFinishPicker").val());
    $("#tBreakStart").val($("#tBreakStartPicker").val());
    $("#tBreakFinish").val($("#tBreakFinishPicker").val());
    $("#iLineNoId").val($("#iLineNo").val());
    $("#vCookingBatchNo").val($("#ProductCode").val());
    $("#iBatchNo").val($("#vBatchNo").val());

    $("#tActualRunTime").val($("#tActualRunTimePicker").val()); //duration

    var packingDate = $("#dPackingPlanPicker").val();

    $("#dPackingPlan").val(getDateTimeForPackingPlan(packingDate));

    $.ajax({
        type: "GET",
        url: SUBMITPACKINGPLAN_URL,
        data: $("#PackingPlanForm").serialize(),
        success: function (result) {
            if (button === "Next") {
                fieldsResetForNextRecord();
                showSuccess("Record added successfully. Add the next now.");
                getUniqueCode("vBatchNo", 3);
            } else if (button === "Submit") {
                showSuccess("Packing Plan record saved successfully");
                $("#packingPlanModal").modal("hide");
                window.location = PACKINGPLANLIST;
            }
            hideLoader();
        },
        error: function (error) {
            hideLoader();
        }
    });
}
//#endregion

//#region Update Required Fields
function updateRequiredFields() {
    var isTask = $("#isTask").val();
    //If is Product
    if (isTask === "false") {
        $("#ProductCode").attr("required", true);
        $("#ProductDescription").attr("required", true);
        $("#iQuantity").attr("required", true);
        $("#vBatchNo").attr("required", true);
    } else {
        $("#ProductCode").attr("required", false);
        $("#ProductDescription").attr("required", false);
        $("#iQuantity").attr("required", false);
        $("#vBatchNo").attr("required", false);
    }
}
//#endregion

$("#tPlannedStartPicker").on("dp.change", function () {
    getTimeDifference();
});
$("#tPlannedFinishPicker").on("dp.change", function () {
    getTimeDifference();
});

function resetForm() {
    getUniqueCode("vBatchNo", 3);
    $("#PackingPlanForm").trigger("reset");
}

//#region VALIDATION FUNCTIONS
function getTimeDifference() {
    var startTime = moment($("#tPlannedStartPicker").val(), "HH:mm:ss");
    var endTime = moment($("#tPlannedFinishPicker").val(), "HH:mm:ss");

    var duration = moment.duration(endTime.diff(startTime));
    var hours = parseInt(duration.asHours());
    var minutes = parseInt(duration.asMinutes()) - hours * 60;
    if (isNaN(hours) || isNaN(minutes)) {
        return false;
    }
    if ((hours < 0 || minutes < 0) && startTime !== "00:00:00" && endTime !== "00:00:00") {
        $("#tActualRunTimePicker").val("");
        return false;
    } else {
        var val = moment(hours + ":" + minutes, "HH:mm").format("HH:mm");
        $("#tActualRunTimePicker").val(val);
        return true;
    }

}
function getPlannedTimeDifference() {

    var actualStartTime = moment($("#tPlannedStartPicker").val(), "HH:mm");
    var actualEndTime = moment($("#tPlannedFinishPicker").val(), "HH:mm");

    var actualTimeDuration = moment.duration(actualEndTime.diff(actualStartTime));
    var actualTimeHours = parseInt(actualTimeDuration.asHours());
    var actualTimeMinutes = parseInt(actualTimeDuration.asMinutes()) - actualTimeHours * 60;

    if (isNaN(actualTimeHours) || isNaN(actualTimeMinutes)) {
        return false;
    }
    if ((actualTimeHours <= 0 && actualTimeMinutes <= 0) && actualStartTime != "00:00:00" && actualEndTime != "00:00:00") {
        showInfo("Planned Finish Time should be greater than planned start time");
        $("#tPlannedFinishPicker").val("");
        return false;
    } else {
        var val = moment(actualTimeHours + ":" + actualTimeMinutes, "HH:mm").format("HH:mm");
        $("#tActualRunTime").val(val);
        return true;
    }
}
function getActualTimeDifference() {
    var actualStartTime = moment($("#tActualStartPicker").val(), "HH:mm");
    var actualEndTime = moment($("#tActualFinishPicker").val(), "HH:mm");

    var actualTimeDuration = moment.duration(actualEndTime.diff(actualStartTime));
    var actualTimeHours = parseInt(actualTimeDuration.asHours());
    var actualTimeMinutes = parseInt(actualTimeDuration.asMinutes()) - actualTimeHours * 60;

    if (isNaN(actualTimeHours) || isNaN(actualTimeMinutes)) {
        return false;
    }
    if ((actualTimeHours <= 0 && actualTimeMinutes <= 0) && actualStartTime != "00:00:00" && actualEndTime != "00:00:00") {
        showInfo("Actual Finish Time should be greater then actual start time");
        $("#tActualFinishPicker").val("");
        return false;
    } else {

        return true;
    }
}
function getBreakeTimeDifference() {

    var actualStartTime = moment($("#tBreakStartPicker").val(), "HH:mm");
    var actualEndTime = moment($("#tBreakFinishPicker").val(), "HH:mm");

    var actualTimeDuration = moment.duration(actualEndTime.diff(actualStartTime));
    var actualTimeHours = parseInt(actualTimeDuration.asHours());
    var actualTimeMinutes = parseInt(actualTimeDuration.asMinutes()) - actualTimeHours * 60;

    if (isNaN(actualTimeHours) || isNaN(actualTimeMinutes)) {
        return false;
    }
    if ((actualTimeHours <= 0 && actualTimeMinutes <= 0) && actualStartTime != "00:00:00" && actualEndTime != "00:00:00") {
        showInfo("Break Finish Time should be greater then break start time");
        $("#tBreakFinishPicker").val("");
        return false;
    } else {

        return true;
    }
}
function getDateTimeForPackingPlan(date) {

    var datePart = date.split(" ")[0];
    var timePart = date.split(" ")[1];

    var day = datePart.split("-")[0];
    var month = datePart.split("-")[1];
    var year = datePart.split("-")[2];

    var hours = timePart.split(":")[0];
    var minutes = timePart.split(":")[1];
    var seconds = timePart.split(":")[2];

    return year + "-" + month + "-" + day + " " + hours + ":" + minutes + ":" + seconds;
}
function validateQuantity() {

    if (parseInt($("#iQuantity").val()) < parseInt($("#iQuantityCompleted").val())) {

        $("#iQuantityCompleted").val(0);
        showError("Quantity should be less or equal to required quantity!");
        return false;
    }

    return true;
}
//#endregion

//#region Check Time Slot Availability
function checkTimeSlotAvailability(button) {

    if ($("#PackingProductionPlanId").val() > 0) {
        checkFinishingSequence();
    } else {
        $("#iLineNoId").val($("#iLineNo").val());
        $("#tPlannedStart").val($("#tPlannedStartPicker").val());
        $("#tPlannedFinish").val($("#tPlannedFinishPicker").val());

        var obj = {
            iLineNo: $("#iLineNoId").val(),
            startDateTime: $("#tPlannedStart").val(),
            finishDateTime: $("#tPlannedFinish").val()
        }

        $.ajax({
            type: "GET",
            url: GETLASTPACKINGPLANSFINISHTIME,
            data: obj,
            success: function (result) {
                if (result.slot === "Time slot already filled.") {
                    showError(result.slot);
                    hideLoader();
                    return;
                }
                submitData(button);
            },
            error: function () {
                hideLoader();
            }
        });
    }
}
//#endregion

//#region Check Time Slot Availability
function checkFinishingSequence() {
    if ($("#PackingProductionPlanId").val() > 0) {
        if ($("#bFinishedPlan").val() === "False") {
            var obj = {
                iPackingProductionPlanId: $("#PackingProductionPlanId").val()
            }

            $.ajax({
                type: "GET",
                url: CHECKPLANNINGFINISHINGSEQUENCE,
                data: obj,
                success: function (result) {
                    if (result.validity === "Please finish the plans in sequence.") {
                        showError(result.validity);
                        hideLoader();
                        return;
                    }
                    submitData("Submit");
                },
                error: function () {
                    hideLoader();
                }
            });
        } else {
            submitData("Submit");
        }
    }
}
//#endregion