﻿
$(document).ready(function () {
    getNotificationsCount();

    //#region Create Notification
    $('#notifyModal').click(function () {
        showLoader();
        var url = NOTIFICATIONCREATE_URL;
        $.get(url, function (data) {
            hideLoader();
            $('#newNotification').html(data);
            $('#newNotificationModal').modal('show');
        });
    });
    //#endregion

    //#region Create DailyCheck
    $('#dailyCheckModal').click(function () {
        //var url = DailyCheck_URL;
        var url = CleaningCheckListDaily_URL;
        $.get(url, function (data) {
            $('#divDailyCheck').html(data);
            $('#newDailyCheckModal').modal('show');
        });
    });
    //#endregion

    //#region Create Stock and Storage
    $('#stockLocModal').click(function () {
        var url = STOCKANDSTORAGECREATE_URL;
        $.get(url, function (data) {
            $('#newStockandStorageLocation').html(data);
            $('#newStockandStorageModal').modal('show');
        });
    });
    //#endregion

    //#region Edit Stock and Storage

    $(".stockLocEdit").on('click', function () {
        var url = STOCKANDSTORAGEEDIT_URL + "/" + $(this).attr("data-id");
        $.get(url, function (data) {
            $('#newStockandStorageLocation').html(data);
            $('#newStockandStorageModal').modal('show');
        });
    });
    //#endregion

    //#region Menu Collapse
    $(".btn-sidepannel").click(function () {
        $(".left-pannel").toggle("");
    });

    //#endregion

    //#region Date Picker
    //our date input has the name "date"

    //#endregion

    //#region Input type number validation for browsers Firefox, IE, Safari
    $('[type="number"]').keypress(function (e) {
        if (e.which !== 46 && e.which !== 45 && e.which !== 46 && e.which !== 8 &&
            !(e.which >= 48 && e.which <= 57)) {
            return false;
        }
    })
    .on("cut copy paste", function (e) {
        //e.preventDefault();
    });
    //#endregion

    //Set Toastr Basic Properties
    toastr.options = {
        "progressBar": true,
        "positionClass": "toast-bottom-right"
    }
});
//show loader function
function showLoader() {
    $(".preloader").addClass("showpreload");
}
//hide loader function
function hideLoader() {
    $(".preloader").removeClass("showpreload");
}

//show success toastr
function showSuccess(msg) {
    toastr.success(msg);
}
//show error toastr
function showError(msg) {
    toastr.error(msg);
}
//show warning toastr
function showWarning(msg) {
    toastr.warning(msg);
}
//show info toastr
function showInfo(msg) {
    toastr.info(msg);
}

//#region Get Notifications Count Every 30 Seconds
var loadingDOM = true;
setInterval(function () {
    getNotificationsCount();
    loadingDOM = false;
}, 30000);

function getNotificationsCount() {
    $.get(NOTIFICATIONCOUNT_URL, function (data, status) {
        if (data.count === 0) {
            $("#notificationCount").hide();
        }
        else {
            $("#notificationCount").text(data.count);
            $("#notificationCount").show();

            var playSound = compareNotifications(data.userid, data.ids);

            if (playSound) {
                var oAudio = $('#audioNotification')[0];

                if (oAudio.paused)
                    oAudio.play();
            }

            saveCurrentNotifications(data.userid, data.ids);
        }
    });
}

function saveCurrentNotifications(pUserId, pIds) {
    if (pIds && pIds.length > 0) {
        var notifications = new Object();
        notifications.userId = pUserId;

        var ids = [];
        for (var id = 0; id < pIds.length; id++)
            ids.push(pIds[id]);

        notifications.Ids = new Object();
        notifications.Ids = ids;

        localStorage.setItem("pkNotifications", JSON.stringify(notifications));
    }
}

function compareNotifications(pUserId, pIds) {
    var prevNotifications = $.parseJSON(localStorage.getItem("pkNotifications"));

    if (prevNotifications) {
        if (prevNotifications.userId == pUserId) {
            if (prevNotifications.Ids && prevNotifications.Ids.length > 0) {
                if (pIds && pIds.length > 0) {
                    for (var i = 0; i < pIds.length; i++) {
                        if (!_.contains(prevNotifications.Ids, pIds[i]))
                            return true;
                    }
                }
                else return true;
            }
            else return true;
        }
        else return true;
    }
    else return true;

    return false;
}

//#endregion

//#region Input type number validation for browsers Firefox, IE, Safari
function initializeNumberValidation() {
    $('[type="number"]').keypress(function (e) {
        if (e.which !== 46 && e.which !== 45 && e.which !== 46 && e.which !== 8 &&
        !(e.which >= 48 && e.which <= 57)) {
            return false;
        }
    })
     .on("cut copy paste", function (e) {
         //e.preventDefault();
     });
}
//#endregion

function getDateTime(date) {

    var datePart = date.split(" ")[0];
    var timePart = date.split(" ")[1];

    var day = datePart.split("-")[0];
    var month = datePart.split("-")[1];
    var year = datePart.split("-")[2];

    var hours = timePart.split(":")[0];
    var minutes = timePart.split(":")[1];
    var seconds = timePart.split(":")[2];

    //return new Date(year, month, day, hours, minutes, seconds);
    return year + "-" + month + "-" + day + " " + hours + ":" + minutes + ":" + seconds;
}

function getDate(date) {
    var datePart = date.split(" ")[0];
    var day = datePart.split("-")[0];
    var month = datePart.split("-")[1];
    var year = datePart.split("-")[2];
    return year + "-" + month + "-" + day + " " + "00:00:00";
}

//function getCheckHistoryModal
function getCheckHistoryModal(notificaitonTypeIdParam, areaIdParam, id) {
    $.ajax({
        type: "GET",
        url: GETNOTIFICATIONMODEL,
        data: { notificationTypeId: notificaitonTypeIdParam, areaId: areaIdParam },
        success: function (data) {
            //2 - Start Up Check - LR Cooking Room
            //3- Daily Cleaning Checklist
            //4- Canteen Fridge temperature Check
            //5- Shoe Cleaning Checklist
            //6 - Goods in
            //7- Stock take
            //8- cooking
            //9- goods out
            //10- packing
            if (notificaitonTypeIdParam == "2"
                || notificaitonTypeIdParam == "3"
                || notificaitonTypeIdParam == "4"
                || notificaitonTypeIdParam == "5") {

                $("#historyItemModalBody").html(data);
                $("#historyItemModal").modal("show");
            } else {
                switch (notificaitonTypeIdParam) {
                    case "6":
                        window.location = GOODSINDELIVERIES;
                        break;
                    case "7":
                        window.location = "";
                        break;
                    case "8":
                        window.location.href = GOODSINDELIVERIES;
                        break;
                    case "9":
                        window.location = "";
                        break;
                    case "10":
                        window.location = "";
                        break;
                    default:
                        window.location = "";
                }
                hideLoader();
            }
            hideLoader();
        },
        error: function () {
            showError("Something went wrong...");
            hideLoader();
        }
    });
}

//function get code
/// If Type = 1 => Get product code by using productTypeId recieved in param
/// If Type = 2 => Get Cooking Batch Code
/// If Type = 3 => Get Packing Batch Code
/// If Type = 4 => Get Boxing Batch Code
function getUniqueCode(domId, id, prodTypeId) {
    showLoader();
    var getProductCodeObj = {
        id: id,
        productTypeId: prodTypeId == null ? 0 : prodTypeId
    }
    $.ajax({
        type: "GET",
        url: GETUNIQUECODE,
        data: getProductCodeObj,
        async: false,
        success: function (data) {
            $("#" + domId).val(data.result);
            hideLoader();
        },
        error: function () {
            showError("Something went wrong while generating Code, please contact administrator!");
        }
    });
}

//only numeric
function onlyNumeric(e) {
}

//Check if there is any / more cooking plan exists in the system
function isCookingPlanExist(domId, checkPackingConsumption) {
    showLoader();
    $.ajax({
        type: "GET",
        data: { isPackingConsumptionCheck: (checkPackingConsumption != undefined ? checkPackingConsumption : true) },
        url: ISCOOKINGPLANEXISTS,
        success: function (data) {
            if (data)
                $("#" + domId).val(data.canCreate);
            hideLoader();
        },
        error: function () {
            showError("Something went wrong while checking cooking plans in system!");
            hideLoader();
        }
    });
}

var cookingPlanNotFoundMsg = "No Cooking Plan exists in the system. Please create a Cooking Plan first.";