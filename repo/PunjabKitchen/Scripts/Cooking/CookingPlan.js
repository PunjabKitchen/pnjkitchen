﻿//#region Document Ready
$(document).ready(function () {
    hideLoader();
    var id = $("#hdnCookPotId").val();
    var ident = "#cp" + id;
    $(ident).addClass("active");
    editCheck = false;
});
//#endregion

//#region Cookpot Buttons Click
$(".btn-default").click(function () {
    showLoader();
});
//#endregion

//#region Create Cooking Plan
$("#createPlanBtn").click(function () {
    var url = CREATECOOKINGPLAN;
    $.get(url, function (data) {
        $("#cookingPlanBody").html(data);
        $("#cookingPlanModal").modal("show");
        hideLoader();
    });
});
//#endregion

//#region Edit Cooking Plan
function editCookingPlan(idParam) {
    showLoader();
    var url = EDITCOOKINGPLAN;
    $.get(url, { id: idParam }, function (data) {
        $("#cookingPlanBody").html(data);
        $("#cookingPlanModal").modal("show");
        hideLoader();
    });
    editCheck = false;
}
//#endregion

//#region Delete Cooking Record
function deleteCookingRecord(id) {
    showLoader();

    var obj = {
        iCookingPlanId: id
    }

    $.ajax({
        type: "GET",
        url: DELETECOOKINGRECORD,
        data: obj,
        success: function (result) {
            hideLoader();

            if (result.status === "Reference Error") {
                showError("Can't delete as this cooking plan has been used in the record.");
                return;
            }

            if (result.status !== "Success") {
                showError("Deletion Failed.");
                return;
            }

            showSuccess("Cooking Plan record successfully removed.");
            window.location = COOKINGPLANLIST;
        },
        error: function (error) {
            hideLoader();
        }
    });
}
//#endregion

//#region Confirm Event
function ConfirmDeletion(id) {
    $("#dialog-confirm").html("Are you sure you want to delete this Cooking Plan?");

    // Define the Dialog and its properties.
    $("#dialog-confirm").dialog({
        modal: true,
        title: "Delete Confirmation",
        height: 150,
        width: 300,
        buttons: {
            "Yes": function () {
                $(this).dialog('close');
                deleteCookingRecord(id);
            },
            "No": function () {
                $(this).dialog('close');
            }
        }
    });
}
//#endregion