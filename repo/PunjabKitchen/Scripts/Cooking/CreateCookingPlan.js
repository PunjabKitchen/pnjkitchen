﻿//#region Document Ready
$(document).ready(function () {

    //if (!($("#cookingPlanId").val() > 0)) {
    //    getUniqueCode("iBatchNo", 2);
    //}

    showLoader();
    //Triggering the Product Code's autocomplete property initially to bring it in running condition.
    initialAutocompleteRequest();

    if ($("#cookingPlanId").val() > 0) {
        $("#nextButton").hide();
    }

    //Check for product of task
    if ($("#isTask").val() === "" || $("#isTask").val().toLowerCase() === "false") {
        $("#isTask").val(false);
        showProductSection();
    } else {
        $("#isTask").val(true);
        showTaskSection();
    }

    //update cancel Button
    if ($("#isCancelled").val().toLowerCase() === "true") {
        $("#cancelBtn").prop("disabled", true);
    }

    //initialize datepicker
    initializeDatePicker();

    //update required fields
    updateRequiredFields();

    NumberValidation();

    //Disabling inputs in edit mode
    disableFields();
});
//#endregion

//#region Product Code's Autocomplete Request
function initialAutocompleteRequest() {
    $.post(GETRECIPEBYCODE_URL,
            {
                code: "0"
            },
            function (data) {
                hideLoader();
                if (data.length > 0) {
                    var arrayList = [];
                    _.each(data, function (item) {
                        arrayList.push({
                            "description": item.description == null ? "" : item.description,
                            "value": item.code == null ? "" : item.code,
                            "id": item.id == null ? "" : item.id,
                            "label": item.code == null ? "" : item.code,
                            "code": item.code == null ? "" : item.code,
                            "name": item.name == null ? "" : item.name
                        });
                    });
                    $("#ProductCode").autocomplete({
                        dataType: "json",
                        source: arrayList,
                        select: function (event, ui) {
                            var prodCode = ui.item.code;
                            $("#ProductCode").val(prodCode);
                            $("#recipeId").val(ui.item.id);
                            $("#ProductDescription").val(ui.item.name);
                        }
                    });
                }
            });
}
//#endregion

//#region Function Update Fields Disablity
function disableFields() {
    if ($("#cookingPlanId").val() > 0) {
        $("#cookPlanDatePicker").attr("disabled", true);
        //$("#tCookPot").attr("disabled", true);
        $("#ProductCode").attr("disabled", true);
        $("#iQuantity").attr("disabled", true);
        $("#iEstMeals").attr("disabled", true);
        $("#iBatchNo").attr("disabled", true);
        $("#tPlannedCooktime").attr("disabled", true);
        $("#tCookDate").attr("disabled", true);
        $("#tPlannedStartTime").attr("disabled", true);
        $("#tPlannedStartTimePicker").attr("disabled", true);
        $("#tActualFinishTime").attr("disabled", true);
        $("#tActualFinishTimePicker").attr("disabled", true);
        $("#iBatchNo").val($("#vBatchNo").val());
        cookingTaskButtonAction = function () {
            return false;
        }
    }
}
//#endregion

//#region Number Validations
function NumberValidation() {
    $("#iQuantity").keypress(function (evt) {
        var self = $(this);
        self.val(self.val().replace(/[^\d].+/, ""));
        if ((evt.which < 48 || evt.which > 57) && (evt.which !== 8) && (evt.keyCode !== 46) && (evt.keyCode !== 37) && (evt.keyCode !== 39)) {
            evt.preventDefault();
        }

        if ((this.value.length === 6) && (evt.which !== 8) && (evt.keyCode !== 46) && (evt.keyCode !== 37) && (evt.keyCode !== 39)) {
            return false;
        }
        return true;
    });
    $("#iQuantityCompleted").keypress(function (evt) {
        var self = $(this);
        self.val(self.val().replace(/[^\d].+/, ""));
        if ((evt.which < 48 || evt.which > 57) && (evt.which !== 8) && (evt.keyCode !== 46) && (evt.keyCode !== 37) && (evt.keyCode !== 39)) {
            evt.preventDefault();
        }

        if ((this.value.length === 6) && (evt.which !== 8) && (evt.keyCode !== 46) && (evt.keyCode !== 37) && (evt.keyCode !== 39)) {
            return false;
        }
        return true;
    });
    $("#iEstMeals").keypress(function (evt) {
        var self = $(this);
        self.val(self.val().replace(/[^\d].+/, ""));
        if ((evt.which < 48 || evt.which > 57) && (evt.which !== 8) && (evt.keyCode !== 46) && (evt.keyCode !== 37) && (evt.keyCode !== 39)) {
            evt.preventDefault();
        }

        if ((this.value.length === 6) && (evt.which !== 8) && (evt.keyCode !== 46) && (evt.keyCode !== 37) && (evt.keyCode !== 39)) {
            return false;
        }
        return true;
    });
    $("#iBatchNo").keypress(function (evt) {
        var self = $(this);
        self.val(self.val().replace(/[^\d].+/, ""));
        if ((evt.which < 48 || evt.which > 57) && (evt.which !== 8) && (evt.keyCode !== 46) && (evt.keyCode !== 37) && (evt.keyCode !== 39)) {
            evt.preventDefault();
        }

        if ((this.value.length === 6) && (evt.which !== 8) && (evt.keyCode !== 46) && (evt.keyCode !== 37) && (evt.keyCode !== 39)) {
            return false;
        }
        return true;
    });
}
//#endregion

//#region Create Cooking Plan Button Action
$("#createCookingProdBtn").on("click", function () {
    if ($("#cookingPlanId").val() > 0) { return; }
    $("#isTask").val(false);
    showProductSection();
    updateRequiredFields();
});

//Function Show Product Section
function showProductSection() {

}

//Function Show Product Section
function showProductSection() {
    $(".cookingProdItems").show();
    $(".cookingTaskItems").hide().removeClass("active");
    $("#createCookingProdBtn").addClass("active");
    $("#createCookingTaskBtn").removeClass("active");
}
//#endregion

//#region Create Cooking Task Button Action
function cookingTaskButtonAction() {
    $("#isTask").val(true);
    showTaskSection();
    updateRequiredFields();
}
//#endregion

//#region Function Show Task Section
function showTaskSection() {
    $(".cookingProdItems").hide().removeClass("active");
    $(".cookingTaskItems").show().addClass("active");
    $("#createCookingTaskBtn").addClass("active");
    $("#createCookingProdBtn").removeClass("active");
}
//#endregion

//#region Initialize Date Picker
function initializeDatePicker() {
    $("#cookPlanDatePicker").datetimepicker({
        format: "DD-MM-YYYY HH:mm:ss",
        minDate: moment()
    });
    $("#tPlannedStartTimePicker").datetimepicker({
        format: "HH:mm"

    });
    $("#tPlannedCooktimePicker").datetimepicker({ format: "HH:mm" });
    $("#tActualFinishTimePicker").datetimepicker({
        format: "HH:mm"

    });
    $("#tActualStartTimePicker").datetimepicker({
        format: "HH:mm"

    });

    $("#tActualFinishedTimePicker").datetimepicker({
        format: "HH:mm"
    });

    if ($("#cookingPlanId").val() > 0) {
        $("#cookPlanDatePicker").val($("#tCookDate").val());
        $("#tPlannedStartTimePicker").val(moment($("#tPlannedStartTime").val()).format("HH:mm"));
        $("#tPlannedCooktimePicker").val(moment($('#tPlannedCooktime').val()).format("HH:mm"));
        $("#tActualFinishTimePicker").val(moment($("#tActualFinishTime").val()).format("HH:mm"));
    }
}
//#endregion

//#region Product Code Auto Complete
$("#ProductCode").on("keyup", function (e) {
    var code = (e.keyCode ? e.keyCode : e.which);
    if (code === 13) { //Enter keycode
        return;
    }
    $("#ProductDescription").val("");
    $("#ProductCode").addClass("spinner");
    getRecipeInfo($(this).val());
});
//#endregion

//#region Get Recipe Info
function getRecipeInfo(text) {
    var array = new Array();
    if (text.length > 0) {
        $.post(GETRECIPEBYCODE_URL,
            {
                code: text
            },
            function (data) {
                $("#ProductCode").removeClass("spinner");
                if (data.length > 0) {
                    var arrayList = [];
                    _.each(data, function (item) {
                        arrayList.push({
                            "description": item.description == null ? "" : item.description,
                            "value": item.code == null ? "" : item.code,
                            "id": item.id == null ? "" : item.id,
                            "label": item.code == null ? "" : item.code,
                            "code": item.code == null ? "" : item.code,
                            "name": item.name == null ? "" : item.name
                        });
                    });
                    $("#ProductCode").autocomplete({
                        dataType: "json",
                        source: arrayList,
                        select: function (event, ui) {
                            var prodCode = ui.item.code;
                            $("#ProductCode").val(prodCode);
                            $("#recipeId").val(ui.item.id);
                            $("#ProductDescription").val(ui.item.name);                       
                        }
                    });
                }
            });
    } else {
        hideLoader();
    }
    return array;
}
//#endregion

//#region Get Allowed Quantity
function getAllowedQuanity(productCode) {
    showLoader();
    $.ajax({
        type: "GET",
        url: GETALLOWEDQUANTITY,
        data: { productCode: productCode },
        success: function (data) {
            hideLoader();
            $("#AllowedQty").removeClass("hide").text(" (Available: " + data.result + ")");
            $("#iQuantity").attr("allowedQty", data.result);
        },
        error: function () {
            hideLoader();
            $("#AllowedQty").addClass("hide");
        }
    });
}
//#endregion

//#region Validate Quantity
function validateQuantity() {
    if (parseInt($("#iQuantity").val()) > parseInt($("#iQuantity").attr("allowedQty"))) {
        $("#iQuantity").val(0);
        showError("Quantity should be less or equal to available quantity!");
        return false;
    }
    return true;
}
//#endregion

//#region Submit Cookng Plan
function submitCookingPlan() {

    if (!$("#cookingPlanForm").valid()) {
        if ($("#tPlannedStartTimePicker").val() === "") {
            $("#tPlannedStartTimePicker").addClass("error");
        }
        if ($("#tActualFinishTimePicker").val() === "") {
            $("#tActualFinishTimePicker").addClass("error");
        }
        return;
    } else if (($("#tPlannedStartTimePicker").val() === "")) {
        $("#tPlannedStartTimePicker").addClass("error");
        return;
    } else if ($("#tActualFinishTimePicker").val() === "") {
        $("#tActualFinishTimePicker").addClass("error");
        return;
    } else {

        if ($("#createCookingProdBtn").hasClass("active")) {
            //Checking Batch Code
            if ($("#ProductDescription").val() === "") {
                showError("Please enter valid Product Code.");
                $("#ProductCode").val("");
                return;
            }
        }

        if ($("#createCookingTaskBtn").hasClass("active")) {
            $("#ProductCode").val("");
            $("#ProductDescription").val("");
            $("#iQuantity").val("");
            $("#iEstMeals").val("");
        }

        //Checking if start and end time are not valid
        if (!getPlannedTimeDifference())
        { return }

        if (($("#cookingPlanId").val() > 0) && !getActualTimeDifference())
        { return }

        //validating Quantity
        if (!validateQuantity()) {
            return;
        }
        showLoader();

        //Check Batch Code Availability
        if (!checkBatchCodeAvailability("Submit")) {
            return;
        };
    }
}
//#endregion

//#region Function: "Next" Click Event
function nextCookingPlanForm() {
    if (!$("#cookingPlanForm").valid()) {
        if ($("#tPlannedStartTimePicker").val() === "") {
            $("#tPlannedStartTimePicker").addClass("error");
        }
        if ($("#tActualFinishTimePicker").val() === "") {
            $("#tActualFinishTimePicker").addClass("error");
        }
        return;
    } else if (($("#tPlannedStartTimePicker").val() === "")) {
        $("#tPlannedStartTimePicker").addClass("error");
        return;
    } else if ($("#tActualFinishTimePicker").val() === "") {
        $("#tActualFinishTimePicker").addClass("error");
        return;
    } else {

        if ($("#createCookingProdBtn").hasClass("active")) {
            //Checking Batch Code
            if ($("#ProductDescription").val() === "") {
                showError("Please enter valid Product Code.");
                $("#ProductCode").val("");
                return;
            }
        }

        //Checking if start and end time are not valid
        if (!getPlannedTimeDifference())
        { return }

        if (($("#cookingPlanId").val() > 0) && !getActualTimeDifference())
        { return }

        //validating Quantity
        if (!validateQuantity()) {
            return;
        }

        showLoader();

        //Check Batch Code Availability
        if (!checkBatchCodeAvailability("Next")) {
            return;
        };
    }
}
//#endregion

//#region Function: Resets fields for next record
function fieldsResetForNextRecord() {
    $("#cookingPlanId").val("");
    $("#ProductCode").val("");
    $("#ProductDescription").val("");
    $("#iQuantity").val("");
    $("#iQuantityCompleted").val("");
    $("#iEstMeals").val("");
    $("#iBatchNo").val("");
    $("#tPlannedStartTimePicker").val("");
    $("#tPlannedCooktimePicker").val("");
    $("#tActualFinishTimePicker").val("");
    $("#tActualStartTimePicker").val("");
    $("#tActualFinishedTimePicker").val("");
    $("#vTaskDescription").val("");
    //document.getElementById("tCookPot").disabled = true;
}
//#endregion

//#region Update Required Fields
function updateRequiredFields() {
    var isTask = $("#isTask").val().toLowerCase();
    //If is Product
    if (isTask === "false") {
        $("#ProductCode").attr("required", true);
        $("#ProductDescription").attr("required", true);
        $("#iQuantity").attr("required", true);
        //$("#iEstMeals").attr("required", true);
        $("#vBatchNo").attr("required", true);
    } else {
        $("#ProductCode").attr("required", false);
        $("#ProductDescription").attr("required", false);
        $("#iQuantity").attr("required", false);
        //$("#iEstMeals").attr("required", false);
        $("#vBatchNo").attr("required", false);
    }
}
//#endregion

//#region Submit Data
function submitData(button) {
    var cookDate = $("#cookPlanDatePicker").val();

    $("#tCookDate").val(getDateTimeForCookingPlan(cookDate));
    $("#tPlannedStartTime").val($("#tPlannedStartTimePicker").val());
    $("#tPlannedCooktime").val($("#tPlannedCooktimePicker").val());
    $("#tActualFinishTime").val($("#tActualFinishTimePicker").val());
    $("#tActualStartTime").val($("#tActualStartTimePicker").val());
    $("#tActualFinishedTime").val($("#tActualFinishedTimePicker").val());
    $("#iCookPotNo").val($("#tCookPot").val());
    $("#vBatchNo").val($("#iBatchNo").val());

    showLoader();
    $.ajax({
        type: "POST",
        url: SUBMITCOOKINGPLAN_URL,
        data: $("#cookingPlanForm").serialize(),
        success: function () {

            if (button === "Next") {
                fieldsResetForNextRecord();
                showSuccess("Record added successfully. Add the next now.");
                //getUniqueCode("iBatchNo", 2);
            } else if (button === "Submit") {
                showSuccess("Cooking Plan record saved successfully");
                $("#cookingPlanModal").modal("hide");
                window.location = COOKINGPLANLIST;
            }
            hideLoader();
        },
        error: function () {
            showError("Something went wrong while saving record");
            hideLoader();
        }
    });
}
//#endregion

//#region datepicker change events
$("#tPlannedStartTimePicker").on("dp.change", function () {
    getTimeDifference();
});

$("#tActualFinishTimePicker").on("dp.change", function () {
    getTimeDifference();
});

$("#tActualStartTimePicker").on("dp.change", function () {
    //getTimeDifference();
});

$("#tActualFinishedTimePicker").on("dp.change", function () {
    getTimeDifference();
});
//#endregion

//#region Datetime functions
function getTimeDifference() {
    var startTime = moment($("#tPlannedStartTimePicker").val(), "HH:mm:ss");
    var endTime = moment($("#tActualFinishTimePicker").val(), "HH:mm:ss");

    var duration = moment.duration(endTime.diff(startTime));
    var hours = parseInt(duration.asHours());
    var minutes = parseInt(duration.asMinutes()) - hours * 60;

    if (isNaN(hours) || isNaN(minutes)) {
        return false;
    }
    if ((hours < 0 || minutes < 0) && startTime !== "00:00:00" && endTime !== "00:00:00") {
        $("#tPlannedCooktimePicker").val("");
        return false;
    } else {
        var val = moment(hours + ":" + minutes, "HH:mm").format("HH:mm");
        $("#tPlannedCooktimePicker").val(val);
        return true;
    }

}

function getPlannedTimeDifference() {
    var actualStartTime = moment($("#tPlannedStartTimePicker").val(), "HH:mm");
    var actualEndTime = moment($("#tActualFinishTimePicker").val(), "HH:mm");

    var actualTimeDuration = moment.duration(actualEndTime.diff(actualStartTime));
    var actualTimeHours = parseInt(actualTimeDuration.asHours());
    var actualTimeMinutes = parseInt(actualTimeDuration.asMinutes()) - actualTimeHours * 60;

    if (isNaN(actualTimeHours) || isNaN(actualTimeMinutes)) {
        return false;
    }
    if ((actualTimeHours <= 0 && actualTimeMinutes <= 0) && actualStartTime !== "00:00:00" && actualEndTime !== "00:00:00") {
        showInfo("Finish Time should be greater than the Start Time");
        $("#tPlannedCooktimePicker").val("");
        return false;
    } else {
        var val = moment(actualTimeHours + ":" + actualTimeMinutes, "HH:mm").format("HH:mm");
        $("#tPlannedCooktimePicker").val(val);
        return true;
    }
}

function getActualTimeDifference() {
    var actualStartTime = moment($("#tActualStartTimePicker").val(), "HH:mm:ss");
    var actualEndTime = moment($("#tActualFinishedTimePicker").val(), "HH:mm:ss");

    var actualTimeDuration = moment.duration(actualEndTime.diff(actualStartTime));
    var actualTimeHours = parseInt(actualTimeDuration.asHours());
    var actualTimeMinutes = parseInt(actualTimeDuration.asMinutes()) - actualTimeHours * 60;

    //showInfo(hours + ' hour and ' + minutes + ' minutes');
    if (isNaN(actualTimeHours) || isNaN(actualTimeMinutes)) {
        return false;
    }
    if ((actualTimeHours < 0 || actualTimeMinutes < 0) && actualStartTime != "00:00:00" && actualEndTime != "00:00:00") {
        showInfo("Actual Finish Time should be greater then actual start time");
        $("#tPlannedCooktimePicker").val("");
        return false;
    } else {
        var val = moment(actualTimeHours + ":" + actualTimeMinutes, "HH:mm").format("HH:mm");
        $("#tPlannedCooktimePicker").val(val);
        return true;
    }
}

function getTime(timeVal) {
    var today = new Date();
    var year = today.getYear();
    var day = today.getDate();
    var month = today.getMonth() + 1;

    var hours = timeVal.split(":")[0];
    var minutes = timeVal.split(":")[1];
    var seconds = timeVal.split(":")[2];

    return new Date(year, month, day, hours, minutes, seconds);
}

function getDateTimeForCookingPlan(date) {

    var datePart = date.split(" ")[0];
    var timePart = date.split(" ")[1];

    var day = datePart.split("-")[0];
    var month = datePart.split("-")[1];
    var year = datePart.split("-")[2];

    var hours = timePart.split(":")[0];
    var minutes = timePart.split(":")[1];
    var seconds = timePart.split(":")[2];

    return year + "-" + month + "-" + day + " " + hours + ":" + minutes + ":" + seconds;
}
//#endregion

//#region Check Time Slot Availability
function checkTimeSlotAvailability(button) {

    if ($("#cookingPlanId").val() > 0) {
        checkFinishingSequence();
    } else {
        $("#iCookPotNo").val($("#tCookPot").val());
        $("#tPlannedStartTime").val($("#tPlannedStartTimePicker").val());
        $("#tActualFinishTime").val($("#tActualFinishTimePicker").val());

        var obj = {
            iCookPotNo: $("#iCookPotNo").val(),
            startDateTime: $("#tPlannedStartTime").val(),
            finishDateTime: $("#tActualFinishTime").val()
        }

        $.ajax({
            type: "GET",
            url: GETLASTPLANSFINISHTIME,
            data: obj,
            success: function (result) {
                if (result.slot === "Time slot already filled.") {
                    showError(result.slot);
                    hideLoader();
                    return;
                }
                submitData(button);
            },
            error: function () {
                hideLoader();
            }
        });
    }
}
//#endregion

//#region Check Finishing Sequence
function checkFinishingSequence() {

    if ($("#cookingPlanId").val() > 0) {
        if ($("#bFinishedPlan").val() === "False") {
            var obj = {
                iCookingPlanId: $("#cookingPlanId").val()
            }

            $.ajax({
                type: "GET",
                url: CHECKFINISHINGSEQUENCE,
                data: obj,
                success: function (result) {
                    if (result.validity === "Please finish the plans in sequence.") {
                        showError(result.validity);
                        hideLoader();
                        return;
                    }
                    submitData("Submit");
                },
                error: function () {
                    hideLoader();
                }
            });
        } else {
            submitData("Submit");
        }
    }
}
//#endregion

//#region Check Batch Code Availability
function checkBatchCodeAvailability(button) {
    if ($("#cookingPlanId").val() > 0) {
        //Check Time Slot Availability
        checkTimeSlotAvailability(button);
    } else {
        var obj = {
            iBatchNo: $("#iBatchNo").val()
        }

        $.ajax({
            type: "POST",
            url: ISCOOKINGBATCHCODEAVAILABLE,
            data: obj,
            success: function(data) {
                if (data.availability === true) {
                    showInfo("Batch Code not available. Please re-enter another Batch Code.");
                    hideLoader();
                    return false;
                } else {
                    //Check Time Slot Availability
                    checkTimeSlotAvailability(button);
                }
            },
            error: function() {
                showError("Something went wrong while checking batch code availability. Please try again.");
                hideLoader();
            }
        });
    }
}
//#endregion