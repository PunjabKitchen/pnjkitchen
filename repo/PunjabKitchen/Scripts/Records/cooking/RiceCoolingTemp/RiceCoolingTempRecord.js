﻿$(document).ready(function () {
    $(".editable").click(function (e) {
        var tag = e.target.nodeName;
        if (tag === 'IMG') { }
        else {
            var id = $($(e.target).parent()).data('id');
            editRiceCoolingTempRecord(id);
        }
    });

    isCookingPlanExist('canCreatePlan', false);
});

//#region Create Rice Cooling Temp Record
$("#RiceCoolingTempRecordModal").click(function () {
    var canCreatePlan = $('#canCreatePlan').val().toLowerCase();
    if (canCreatePlan == 'true') {
        showLoader();
        var url = CREATERICECOOLINGTEMPRECORD_URL;
        $.get(url, function (data) {
            $("#newRiceCoolingTempRec").html(data);
            $("#newRiceCoolingRecord").modal("show");
            hideLoader();
        });
    }
    else
        showError(cookingPlanNotFoundMsg);
});
//#endregion

//#region Edit Rice Cooling Temp Record
function editRiceCoolingTempRecord(idParam) {
    var url = EDITRICECOOLINGTEMPRECORD;
    $.get(url, { id: idParam }, function (data) {
        $('#newRiceCoolingTempRec').html(data);
        $('#newRiceCoolingRecord').modal('show');
    });
}
//#endregion