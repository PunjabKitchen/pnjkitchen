﻿//#region Document Ready
$(document).ready(function () {
    //initialize datepicker
    initializeDatePicker();
    initializeNumberValidation();
    showLoader();
    //Triggering the Product Code's autocomplete property initially to bring it in running condition.
    initialAutocompleteRequest();
});
//#endregion

//#region Product Code's Autocomplete Request
function initialAutocompleteRequest() {
    $.post(GETBATCHBYCODE_URL,
            {
                code: "0"
            },
            function (data) {
                hideLoader();
                if (data.length > 0) {
                    var arrayList = [];
                    _.each(data, function (item) {
                        arrayList.push({
                            "description": item.description == null ? "" : item.description,
                            "value": item.code == null ? "" : item.code,
                            "id": item.id == null ? "" : item.id,
                            "label": item.code == null ? "" : item.code,
                            "code": item.code == null ? "" : item.code,
                            "name": item.name == null ? "" : item.name
                        });
                    });
                    $("#BatchNo").autocomplete({
                        dataType: "json",
                        source: arrayList,
                        select: function (event, ui) {
                            var batchCode = ui.item.batchNo;
                            $("#BatchNo").val(batchCode);
                            $("#ProductDescription").val(ui.item.name);
                            $("#CookingPlanId").val(ui.item.id);
                        }
                    });
                }
            });
}
//#endregion

//#region Initialize Date Picker
function initializeDatePicker() {
    $("#tTimeExCookPicker").datetimepicker({
        format: 'HH:mm'
    });

    $("#tTimeToWIPChillerPicker").datetimepicker({
        format: 'HH:mm'
    });
}
//#endregion

//#region Batch No. Auto Complete
$("#BatchNo").on("keyup", function (e) {
    var code = (e.keyCode ? e.keyCode : e.which);
    if (code === 13) { //Enter keycode
        return;
    }
    $("#ProductDescription").val("");
    $("#BatchNo").addClass("spinner");
    getBatchInfo($(this).val());
});
//#endregion

//#region Get Batch Info
function getBatchInfo(text) {
    var array = new Array();
    if (text.length > 0) {
        $.post(GETBATCHBYCODE_URL,
            {
                code: text
            },
            function (data) {
                $("#BatchNo").removeClass("spinner");
                if (data.length > 0) {
                    var arrayList = [];
                    _.each(data, function (item) {
                        arrayList.push({
                            "description": item.description == null ? "" : item.description,
                            "value": item.code == null ? "" : item.code,
                            "id": item.id == null ? "" : item.id,
                            "label": item.code == null ? "" : item.code,
                            "code": item.code == null ? "" : item.code,
                            "name": item.name == null ? "" : item.name
                        });
                    });
                    $("#BatchNo").autocomplete({
                        dataType: "json",
                        source: arrayList,
                        select: function (event, ui) {
                            var batchCode = ui.item.batchNo;
                            $("#BatchNo").val(batchCode);
                            $("#ProductDescription").val(ui.item.name);
                            $("#CookingPlanId").val(ui.item.id);
                        }
                    });
                }
            });
    } else {
        hideLoader();
    }
    return array;
}
//#endregion

//#region Function Submit Form
function submitRiceCoolingTempRecForm() {
    if (!$("#riceCoolingTempRecordForm").valid()) {
        return;
    }
    showLoader();

    $("#bCriticalCheck").val(false);

    if (parseInt($("#vTempAfterWaterRinse").val()) > 3) {
        $("#bCriticalCheck").val(true);
    }

    if ($("#ProductDescription").val() === "") {
        showError("Please enter valid batch Code.");
        $("#ProductCode").val("");
        return;
    }

    $.ajax({
        type: "GET",
        url: SUBMITRICECOOLINGTEMPRECORD_URL,
        data: $("#riceCoolingTempRecordForm").serialize(),
        success: function (result) {
            showSuccess("Rice Cooling Temp record saved successfully.");
            $("#newRiceCoolingRecord").modal("hide");
            hideLoader();
            window.location = RICECOOLINGTEMPRECORDSLIST;
        },
        error: function (error) {
            hideLoader();
        }
    });
}
//#endregion