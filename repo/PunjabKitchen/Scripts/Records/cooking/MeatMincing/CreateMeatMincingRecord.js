﻿//#region Document Ready
$(document).ready(function () {
    //initialize datepicker
    initializeDatePicker();
    initializeNumberValidation();
    showLoader();
    //Triggering the Product Code's autocomplete property initially to bring it in running condition.
    initialAutocompleteRequest();
});
//#endregion

//#region Product Code's Autocomplete Request
function initialAutocompleteRequest() {
    $.post(GETBATCHBYCODE_URL,
            {
                code: "0"
            },
            function (data) {
                hideLoader();
                if (data.length > 0) {
                    var arrayList = [];
                    _.each(data, function (item) {
                        arrayList.push({
                            "description": item.description == null ? "" : item.description,
                            "value": item.code == null ? "" : item.code,
                            "id": item.id == null ? "" : item.id,
                            "label": item.code == null ? "" : item.code,
                            "code": item.code == null ? "" : item.code,
                            "name": item.name == null ? "" : item.name
                        });
                    });
                    $("#BatchNo").autocomplete({
                        dataType: "json",
                        source: arrayList,
                        select: function (event, ui) {
                            var batchCode = ui.item.batchNo;
                            $("#BatchNo").val(batchCode);
                            $("#ProductDescription").val(ui.item.name);
                            $("#CookingPlanId").val(ui.item.id);
                        }
                    });
                }
            });
}
//#endregion

//#region Initialize Date Picker
function initializeDatePicker() {
    $("#bestBeforeDatePicker").datetimepicker({
        format: "DD-MM-YYYY",
        minDate: moment()
    });

    $("#tStartTimePicker").datetimepicker({
        format: 'HH:mm'
    });

    $("#tFinishTimePicker").datetimepicker({
        format: 'HH:mm'
    });

    if ($("#tMeatMincingRecordID").val() > 0) {

        var bestBeforeDate = $("#tBestBeforeDate").val();
        bestBeforeDate = bestBeforeDate.split(' ')[0];
        $("#bestBeforeDatePicker").val(bestBeforeDate);
        
        if ($("#tPreUseCleanlinessCheckSign").val() == 'checked')
        {
            $("#preCheck").prop("checked", true);
        }

    }
}
//#endregion

//#region Batch No. Auto Complete
$("#BatchNo").on("keyup", function (e) {
    var code = (e.keyCode ? e.keyCode : e.which);
    if (code === 13) { //Enter keycode
        return;
    }
    $("#ProductDescription").val("");
    $("#BatchNo").addClass("spinner");
    getBatchInfo($(this).val());
});
//#endregion

//#region Get Batch Info
function getBatchInfo(text) {
    var array = new Array();
    if (text.length > 0) {
        $.post(GETBATCHBYCODE_URL,
            {
                code: text
            },
            function (data) {
                $("#BatchNo").removeClass("spinner");
                if (data.length > 0) {
                    var arrayList = [];
                    _.each(data, function (item) {
                        arrayList.push({
                            "description": item.description == null ? "" : item.description,
                            "value": item.code == null ? "" : item.code,
                            "id": item.id == null ? "" : item.id,
                            "label": item.code == null ? "" : item.code,
                            "code": item.code == null ? "" : item.code,
                            "name": item.name == null ? "" : item.name
                        });
                    });
                    $("#BatchNo").autocomplete({
                        dataType: "json",
                        source: arrayList,
                        select: function (event, ui) {
                            var batchCode = ui.item.batchNo;
                            $("#BatchNo").val(batchCode);
                            $("#ProductDescription").val(ui.item.name);
                            $("#CookingPlanId").val(ui.item.id);
                        }
                    });
                }
            });
    } else {
        hideLoader();
    }
    return array;
}
//#endregion

//#region Function Submit Form
function submitMeatMincingRecForm() {
    if (!$("#meatMincingRecordForm").valid()) {
        return;
    }

    //Checking if start and finish time are not valid
    if (!getTimeDifference()) {
        return;
    }
    showLoader();

    var bestBeforeDate = $("#bestBeforeDatePicker").val();
    $("#tBestBeforeDate").val(getDateTimeForBestBefore(bestBeforeDate));

    var opIsChecked = $('#preCheck').prop('checked');
    $("#tPreUseCleanlinessCheckSign").val(opIsChecked);

    if ($("#ProductDescription").val() === "") {
        showError("Please enter valid batch Code.");
        $("#ProductCode").val("");
        return;
    }

    showLoader();
    $.ajax({
        type: "GET",
        url: SUBMITMEATMINCINGRECORD_URL,
        data: $("#meatMincingRecordForm").serialize(),
        success: function (result) {
            showSuccess("Meat Mincing record saved successfully");
            $("#newMeatMincingRecord").modal("hide");
            hideLoader();
            window.location = MEATMINCINGRECORDSLIST;
        },
        error: function (error) {
            hideLoader();
        }
    });
}
//#endregion

//#region datetime functions
function getTimeDifference() {
    var startTime = moment($("#tStartTimePicker").val(), 'HH:mm:ss');
    var finishTime = moment($("#tFinishTimePicker").val(), 'HH:mm:ss');

    var timeDuration = moment.duration(finishTime.diff(startTime));
    var timeHours = parseInt(timeDuration.asHours());
    var timeMinutes = parseInt(timeDuration.asMinutes()) - timeHours * 60;

    if (isNaN(timeHours) || isNaN(timeMinutes)) {
        return false;
    }
    if ((timeHours <= 0 && timeMinutes <= 0) && startTime != "00:00:00" && finishTime != "00:00:00") {
        showInfo("Finish Time should be greater than Start time");
        return false;
    } else {
        return true;
    }
}

function getDateTimeForBestBefore(date) {

    var day, month, year;

    //If the format has changed in update mode.
    if (date[2] == '/' || date[1] == '/') {
        month = date.split("/")[0];
        day = date.split("/")[1];
        year = date.split("/")[2];
    }
    else {
        day = date.split("-")[0];
        month = date.split("-")[1];
        year = date.split("-")[2];
    }

    return year + "-" + month + "-" + day;
}
//#endregion