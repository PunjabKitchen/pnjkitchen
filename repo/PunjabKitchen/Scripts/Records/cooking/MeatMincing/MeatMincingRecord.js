﻿$(document).ready(function () {
    $(".editable").click(function (e) {
        var tag = e.target.nodeName;
        if (tag === 'IMG') { }
        else {
            var id = $($(e.target).parent()).data('id');
            editMeatMincingRecord(id);
        }
    });
    isCookingPlanExist('canCreatePlan', false);
});

//#region Create Meat Mincing Record
$("#MeatMincingRecordModal").click(function () {
    var canCreatePlan = $('#canCreatePlan').val().toLowerCase();
    if (canCreatePlan == 'true') {
        showLoader();
        var url = CREATEMEATMINCINGRECORD_URL;
        $.get(url, function (data) {
            $("#newMeatMincingRec").html(data);
            $("#newMeatMincingRecord").modal("show");
            hideLoader();
        });
    }
    else
        showError(cookingPlanNotFoundMsg);
});
//#endregion

//#region Edit Meat Mincing Record
function editMeatMincingRecord(idParam) {
    showLoader();
    var url = EDITMEATMINCINGRECORD;
    $.get(url, { id: idParam }, function (data) {
        $('#newMeatMincingRec').html(data);
        $('#newMeatMincingRecord').modal('show');
        hideLoader();
    });
}
//#endregion