﻿//#region Document Ready Function
$(document).ready(function () {
    $(".editable").click(function (e) {
        var tag = e.target.nodeName;
        if (tag === 'IMG') { }
        else {
            var id = $($(e.target).parent()).data('id');
            editFreezerRecord(id);
        }
    });
    hideLoader();
});
//#endregion

//#region Create freezer record
$('#FreezerRecordModal').click(function () {
    showLoader();
    var url = CREATEFREEZERRECORD_URL;
    $.get(url, function (data) {
        $('#newFreezerRec').html(data);
        $('#newFreezerRecModal').modal('show');
        hideLoader();
    });
});
//#endregion

//#region Edit Freezer Record
function editFreezerRecord(idParam) {
    showLoader();
    var url = FREEZERRECORDEDIT_URL;
    $.get(url, { id: idParam }, function (data) {
        $("#newFreezerRec").html(data);
        $("#newFreezerRecModal").modal("show");
        hideLoader();
    });
}
//#endregion