﻿//#region Document Ready Function
$(document).ready(function () {
    initializeNumberValidation();
});
//#endregion

//#region Function Submit Form
function submitFreezerRecForm() {
   
    if (!$("#freezerRecordForm").valid()) {
        return;
    }
    showLoader();

    $.ajax({
        type: "GET",
        url: SUBMITFREEZERRECORD_URL,
        data: $("#freezerRecordForm").serialize(),
        success: function (result) {
            $("#newFreezerRecModal").modal("hide");
            hideLoader();
            window.location = FREEZERRECORDSLIST ;
        },
        error: function (error) {
            hideLoader();
        }
    });
}
//#endregion


