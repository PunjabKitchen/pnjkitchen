﻿//#region Document Ready
$(document).ready(function () {
    //initialize datepicker
    initializeDatePicker();
    //  initializeNumberValidation();
    showLoader();
    //Triggering the Product Code's autocomplete property initially to bring it in running condition.
    initialAutocompleteRequest();
});
//#endregion

//#region Product Code's Autocomplete Request
function initialAutocompleteRequest() {
    $.post(GETBATCHBYCODE_URL,
            {
                code: "0"
            },
            function (data) {
                hideLoader();
                if (data.length > 0) {
                    var arrayList = [];
                    _.each(data, function (item) {
                        arrayList.push({
                            "description": item.description == null ? "" : item.description,
                            "value": item.code == null ? "" : item.code,
                            "id": item.id == null ? "" : item.id,
                            "label": item.code == null ? "" : item.code,
                            "code": item.code == null ? "" : item.code,
                            "name": item.name == null ? "" : item.name
                        });
                    });
                    $("#BatchNo").autocomplete({
                        dataType: "json",
                        source: arrayList,
                        select: function (event, ui) {
                            var batchCode = ui.item.batchNo;
                            $("#BatchNo").val(batchCode);
                            $("#ProductName").val(ui.item.name);
                            $("#cookingId").val(ui.item.id);
                        }
                    });
                }
            });
}
//#endregion

//#region initialize Date Picker
function initializeDatePicker() {
    $("#tTimeExCookPicker").datetimepicker({
        format: "HH:mm"

    });
    $("#tTimeInBlastChillerPicker").datetimepicker({
        format: "HH:mm"
        

    });
    $("#tTimeToWipChillerPicker").datetimepicker({
        format: "HH:mm"

    });

    if ($("#BatchCoolingTempRecrdId").val() > 0) {
        $("#tTimeExCookPicker").val(moment($('#tTimeExCook').val(), 'HH:mm').format('HH:mm'));
        $("#tTimeInBlastChillerPicker").val(moment($('#tTimeInBlastChiller').val(), 'HH:mm').format('HH:mm'));
        $("#tTimeToWipChillerPicker").val(moment($('#tTimeToWipChiller').val(), 'HH:mm').format('HH:mm'));
    }
}
//#endregion

//#region Batch No Auto Complete
$("#BatchNo").on("keyup", function (e) {
    var code = (e.keyCode ? e.keyCode : e.which);
    if (code === 13) { //Enter keycode
        return;
    }
    $("#ProductName").val("");
    $("#BatchNo").addClass("spinner");
    getBatchInfo($(this).val());
});
//#endregion

//#region Get Batch Info
function getBatchInfo(text) {
    var array = new Array();
    if (text.length > 0) {
        $.post(GETBATCHBYCODE_URL,
            {
                code: text
            },
            function (data) {
                $("#BatchNo").removeClass("spinner");
                if (data.length > 0) {
                    var arrayList = [];
                    _.each(data, function (item) {
                        arrayList.push({
                            "description": item.description == null ? "" : item.description,
                            "value": item.code == null ? "" : item.code,
                            "id": item.id == null ? "" : item.id,
                            "label": item.code == null ? "" : item.code,
                            "code": item.code == null ? "" : item.code,
                            "name": item.name == null ? "" : item.name
                        });
                    });          
                    $("#BatchNo").autocomplete({
                        dataType: "json",
                        source: arrayList,
                        select: function (event, ui) {
                            var batchCode = ui.item.batchNo;
                            $("#BatchNo").val(batchCode);
                            $("#ProductName").val(ui.item.name);
                            $("#cookingId").val(ui.item.id);
                        }
                    });
                }
            });
    } else {
        hideLoader();
    }
    return array;
}
//#endregion

//#region Function Submit Form
function submitBatchCoolingForm() {

    if (!$("#BatchCoolingTempRecordForm").valid() || $("#tTimeExCookPicker").val() === "" || $("#tTimeInBlastChillerPicker").val() === "" || $("#tTimeToWipChillerPicker").val() === "") {
        $("#tTimeToWipChillerPicker").valid();
        $("#tTimeInBlastChiller").valid();
        $("#tTimeExCook").valid();
        return;
    }
    showLoader();

    $("#tTimeExCook").val($("#tTimeExCookPicker").val());
    $("#tTimeInBlastChiller").val($("#tTimeInBlastChillerPicker").val());
    $("#tTimeToWipChiller").val($("#tTimeToWipChillerPicker").val());
    
    $("#bCriticalCheck").val(false);

    if (parseInt($("#vTempInBlastChiller").val()) < 70) {
        $("#bCriticalCheck").val(true);
    }

    if (parseInt($("#vTempExInBlastChiller").val()) > 3) {
        $("#bCriticalCheck").val(true);
    }

    if ($("#ProductName").val() === "") {
        showError("Please enter valid batch Code.");
        $("#ProductCode").val("");
        return;
    }

    $.ajax({
        type: "GET",
        url: SUBMITBATCHCOOLINGTEMPRECORD_URL,
        data: $("#BatchCoolingTempRecordForm").serialize(),
        success: function (result) {
            $("#newBatchCoolingTempRecModal").modal("hide");
            hideLoader();
            window.location = BATCHCOOLINGTEMPRECORDSLIST;
        },
        error: function (error) {
            hideLoader();
        }
    });
}
//#endregion