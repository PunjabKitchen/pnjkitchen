﻿//#region Document Ready
$(document).ready(function () {
    $(".editable").click(function (e) {
        var tag = e.target.nodeName;
        if (tag === 'IMG') { }
        else {
            var id = $($(e.target).parent()).data('id');
            editBatchCoolingTempRecord(id);
        }
    });
    isCookingPlanExist('canCreatePlan', false);
});
//#endregion

//#region Create Batch Cooling temp
$("#BatchCoolingTempRecordModal").click(function () {
    var canCreatePlan = $('#canCreatePlan').val().toLowerCase();
    if (canCreatePlan == 'true') {
        showLoader();
        var url = CREATEBATCHCOOLINGTEMPRECORD_URL;
        $.get(url, function (data) {
            $("#newBatchCoolingTempRec").html(data);
            $("#newBatchCoolingTempRecModal").modal("show");
            hideLoader();
        });
    }
    else
        showError(cookingPlanNotFoundMsg);
});
//#endregion


//#region Edit Batch Cooling Temp Record
function editBatchCoolingTempRecord(idParam) {
    showLoader();
    var url = BATCHCOOLINGTEMPRECORDEDIT_URL;
    $.get(url, { id: idParam }, function (data) {
        $("#newBatchCoolingTempRec").html(data);
        $("#newBatchCoolingTempRecModal").modal("show");
        hideLoader();
    });
}
//#endregion