﻿$(document).ready(function () {
    $(".editable").click(function (e) {
        var tag = e.target.nodeName;
        if (tag === 'IMG') { }
        else {
            var id = $($(e.target).parent()).data('id');
            editSlicingRecord(id);
        }
    });

    isCookingPlanExist('canCreatePlan', false);
});

//#region Create Slicing Record
$('#SlicingRecordModal').click(function () {
    var canCreatePlan = $('#canCreatePlan').val().toLowerCase();
    if (canCreatePlan == 'true') {
        showLoader();
        var url = CREATESLICINGRECORD_URL;
        $.get(url, function (data) {
            $("#newSlicingRec").html(data);
            $("#newSlicingRecModal").modal("show");
            hideLoader();
        });
    }
    else
        showError(cookingPlanNotFoundMsg);
});
//#endregion

//#region Edit Slicing Record
function editSlicingRecord(idParam) {
    showLoader();
    var url = EDITSLICINGRECORD;
    $.get(url, { id: idParam }, function (data) {
        $("#newSlicingRec").html(data);
        $("#newSlicingRecModal").modal("show");
        hideLoader();
    });
}
//#endregion