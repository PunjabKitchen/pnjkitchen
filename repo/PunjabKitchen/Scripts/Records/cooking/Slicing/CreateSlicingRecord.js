﻿//#region Document Ready
$(document).ready(function () {
    //initialize datepicker
    initializeDatePicker();
    initializeNumberValidation();
    showLoader();
    //Triggering the Product Code's autocomplete property initially to bring it in running condition.
    initialAutocompleteRequest();
});
//#endregion

//#region Product Code's Autocomplete Request
function initialAutocompleteRequest() {
    $.post(GETBATCHBYCODE_URL,
            {
                code: "0"
            },
            function (data) {
                if (data.length > 0) {
                    var arrayList = [];
                    _.each(data, function (item) {
                        arrayList.push({
                            "description": item.description == null ? "" : item.description,
                            "value": item.code == null ? "" : item.code,
                            "id": item.id == null ? "" : item.id,
                            "label": item.code == null ? "" : item.code,
                            "code": item.code == null ? "" : item.code,
                            "name": item.name == null ? "" : item.name
                        });
                    });
                    $("#BatchNo").autocomplete({
                        dataType: "json",
                        source: arrayList,
                        select: function (event, ui) {
                            var batchCode = ui.item.value;
                            $("#BatchNo").val(batchCode);
                            $("#ProductDescription").val(ui.item.name);
                            $("#CookingPlanId").val(ui.item.id);
                        }
                    });
                }
            });
}
//#endregion

//#region Initialize Date Picker
function initializeDatePicker() {
    $("#tStartTimePicker").datetimepicker({
        format: 'HH:mm'
    });

    $("#tFinishTimePicker").datetimepicker({
        format: 'HH:mm'
    });
}
//#endregion

//#region Batch No. Auto Complete
$("#BatchNo").on("keyup", function (e) {
    var code = (e.keyCode ? e.keyCode : e.which);
    if (code === 13) { //Enter keycode
        return;
    }
    $("#BatchNo").addClass("spinner");
    $("#ProductDescription").val("");
    getBatchInfo($(this).val());
});
//#endregion

//#region Get Batch Info
function getBatchInfo(text) {
    var array = new Array();
    if (text.length > 0) {
        $.post(GETBATCHBYCODE_URL,
            {
                code: text
            },
            function (data) {
                $("#BatchNo").removeClass("spinner");
                if (data.length > 0) {
                    var arrayList = [];
                    _.each(data, function (item) {
                        arrayList.push({
                            "description": item.description == null ? "" : item.description,
                            "value": item.code == null ? "" : item.code,
                            "id": item.id == null ? "" : item.id,
                            "label": item.code == null ? "" : item.code,
                            "code": item.code == null ? "" : item.code,
                            "name": item.name == null ? "" : item.name
                        });
                    });
                    $("#BatchNo").autocomplete({
                        dataType: "json",
                        source: arrayList,
                        select: function (event, ui) {
                            var batchCode = ui.item.value;
                            $("#BatchNo").val(batchCode);
                            $("#ProductDescription").val(ui.item.name);
                            $("#CookingPlanId").val(ui.item.id);
                        }
                    });
                }
            });
    } else {
        hideLoader();
    }
    return array;
}
//#endregion

//#region Function Submit Form
function submitSlicingRecForm() {
    if (!$("#slicingRecordForm").valid()) {
        return;
    }

    //Checking if start and end times are not valid
    if (!getTimeDifference()) {
        return;
    }
    showLoader();

    //Setting values of time field from the respective DatePickers
    $("#tStartTime").val($("#tStartTimePicker").val());
    $("#tFinishTime").val($("#tFinishTimePicker").val());

    $("#bCriticalCheck").val(false);

    if (parseInt($("#vTempProductMaxStart").val()) > 3) {
        $("#bCriticalCheck").val(true);
    }

    if (parseInt($("#vTempProductMaxEnd").val()) > 3) {
        $("#bCriticalCheck").val(true);
    }

    if ($("#ProductDescription").val() === "") {
        showError("Please enter valid batch Code.");
        $("#ProductCode").val("");
        return;
    }
    
    showLoader();
    $.ajax({
        type: "GET",
        url: SUBMITSLICINGRECORD_URL,
        data: $("#slicingRecordForm").serialize(),
        success: function (result) {
            showSuccess("Slicing record saved successfully.");
            $("#newSlicingRecModal").modal("hide");
            hideLoader();
            window.location = SLICINGRECORDSLIST;
        },
        error: function (error) {
            hideLoader();
        }
    });
}
//#endregion

//#region Check Time Difference Between Start & End Time
function getTimeDifference() {
    var startTime = moment($("#tStartTimePicker").val(), 'HH:mm:ss');
    var finishTime = moment($("#tFinishTimePicker").val(), 'HH:mm:ss');

    var timeDuration = moment.duration(finishTime.diff(startTime));
    var timeHours = parseInt(timeDuration.asHours());
    var timeMinutes = parseInt(timeDuration.asMinutes()) - timeHours * 60;

    if (isNaN(timeHours) || isNaN(timeMinutes)) {
        return false;
    }
    if ((timeHours <= 0 && timeMinutes <= 0) && startTime != "00:00:00" && finishTime != "00:00:00") {
        showInfo("Finish Time should be greater than the Start Time.");
        return false;
    } else {
        return true;
    }
}
//#endregion