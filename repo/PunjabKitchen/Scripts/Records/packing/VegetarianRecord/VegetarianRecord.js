﻿//#region Document Ready function
$(document).ready(function () {
    showLoader();
    $(".editable").click(function (e) {
        var tag = e.target.nodeName;
        if (tag === 'IMG') { }
        else {
            var id = $($(e.target).parent()).data('id');
            editVegetarianRecord(id);
        }
    });
    isCookingPlanExist('canCreatePlan', false);
});
//#endregion

//#region Create Packing Record
$("#vegetarianRecordModal").click(function () {
    var canCreatePlan = $('#canCreatePlan').val().toLowerCase();
    if (canCreatePlan == 'true') {
        showLoader();
        window.location = NEWVEGETARIANRECORD;
    }
    else
        showError(cookingPlanNotFoundMsg);
});
//#endregion

//#region Edit Vegetarian Record
function editVegetarianRecord(idParam) {
    showLoader();
    location.href = "EditVegetarianRecord/" + idParam;
};
//#endregion