﻿//#region Document Ready
$(document).ready(function () {
    updateChecksForEdit();
    initializeNumberValidation();
    weightsNumberValidations();  

    if (!($("#iDetectionPackWeightTestId").val() > 0)) {
        $("#weightCount").val("0");

        //Initially setting the value of table cells to "-"
        $("td").each(function () {
            if (typeof $("weightPack" + ($(this).index() + 1)).val() == "undefined") {
                $(this).children("input").hide();
                $(this).children("span").text("-");
            }
        });
    }

    //In Edit Mode.
    if ($("#iDetectionPackWeightTestId").val() > 0) {
        $("#weightCountLabel").text("10 out of 10 Completed");
        $("#weightCountLabel").addClass("completed");
        $("#ProductDescription").val($("#vRecipeName").val());
        $("#ProductWeight").val($("#iProductWeight").val());
        $("#averagePackWeight").text($("#iAveragePackWeight").val());
        $("#minimumPackWeight").text($("#iMinimumpackWeight").val());
        $("#nominalPackWeight").text($("#iNominalPackWeight").val());
        $("#weightCount").val("10");
        $("#nextButton").hide();
        $("#currentPackWeight").attr("disabled", "disabled");

        $("td").each(function () {
            $(this).children("span").text($(this).children("input").val());
            $(this).children("input").hide();
        });
    }
    hideLoader();

    showLoader();
    //Triggering the Product Code's autocomplete property initially to bring it in running condition.
    initialAutocompleteRequest();
});
//#endregion

//#region Product Code's Autocomplete Request
function initialAutocompleteRequest() {
    $.post(GETBATCHBYCODE_URL,
        {
            code: "0"
        },
        function(data) {
            $("#vBatchNo").removeClass("spinner");
            if (data.length > 0) {
                var arrayList = [];
                _.each(data, function(item) {
                    arrayList.push({
                        "description": item.description == null ? "" : item.description,
                        "value": item.code == null ? "" : item.code,
                        "id": item.id == null ? "" : item.id,
                        "label": item.code == null ? "" : item.code,
                        "code": item.code == null ? "" : item.code,
                        "name": item.name == null ? "" : item.name,
                        "weight": item.weight == null ? "" : item.weight
                    });
                });
                $("#vBatchNo").autocomplete({
                    dataType: "json",
                    source: arrayList,
                    select: function(event, ui) {
                        var batchCode = ui.item.value;
                        $("#vBatchNo").val(batchCode);
                        $("#ProductDescription").val(ui.item.name);
                        $("#CookingPlanId").val(ui.item.id);
                        $("#ProductWeight").val(ui.item.weight);
                        $("#nominalPackWeight").text(ui.item.weight);
                    }
                });
            }
        });
}
//#endregion

//#region Weight fields number validation
function weightsNumberValidations() {
    $("#iWeightOfPacks1").keypress(function (evt) {
        var self = $(this);
        self.val(self.val().replace(/[^\d].+/, ""));
        if ((evt.which < 48 || evt.which > 57) && (evt.which !== 8) && (evt.keyCode !== 46) && (evt.keyCode !== 37) && (evt.keyCode !== 39)) {
            evt.preventDefault();
        }
    });
    $("#iWeightOfPacks2").keypress(function (evt) {
        var self = $(this);
        self.val(self.val().replace(/[^\d].+/, ""));
        if ((evt.which < 48 || evt.which > 57) && (evt.which !== 8) && (evt.keyCode !== 46) && (evt.keyCode !== 37) && (evt.keyCode !== 39)) {
            evt.preventDefault();
        }
    });
    $("#iWeightOfPacks3").keypress(function (evt) {
        var self = $(this);
        self.val(self.val().replace(/[^\d].+/, ""));
        if ((evt.which < 48 || evt.which > 57) && (evt.which !== 8) && (evt.keyCode !== 46) && (evt.keyCode !== 37) && (evt.keyCode !== 39)) {
            evt.preventDefault();
        }
    });
    $("#iWeightOfPacks4").keypress(function (evt) {
        var self = $(this);
        self.val(self.val().replace(/[^\d].+/, ""));
        if ((evt.which < 48 || evt.which > 57) && (evt.which !== 8) && (evt.keyCode !== 46) && (evt.keyCode !== 37) && (evt.keyCode !== 39)) {
            evt.preventDefault();
        }
    });
    $("#iWeightOfPacks5").keypress(function (evt) {
        var self = $(this);
        self.val(self.val().replace(/[^\d].+/, ""));
        if ((evt.which < 48 || evt.which > 57) && (evt.which !== 8) && (evt.keyCode !== 46) && (evt.keyCode !== 37) && (evt.keyCode !== 39)) {
            evt.preventDefault();
        }
    });
    $("#iWeightOfPacks6").keypress(function (evt) {
        var self = $(this);
        self.val(self.val().replace(/[^\d].+/, ""));
        if ((evt.which < 48 || evt.which > 57) && (evt.which !== 8) && (evt.keyCode !== 46) && (evt.keyCode !== 37) && (evt.keyCode !== 39)) {
            evt.preventDefault();
        }
    });
    $("#iWeightOfPacks7").keypress(function (evt) {
        var self = $(this);
        self.val(self.val().replace(/[^\d].+/, ""));
        if ((evt.which < 48 || evt.which > 57) && (evt.which !== 8) && (evt.keyCode !== 46) && (evt.keyCode !== 37) && (evt.keyCode !== 39)) {
            evt.preventDefault();
        }
    });
    $("#iWeightOfPacks8").keypress(function (evt) {
        var self = $(this);
        self.val(self.val().replace(/[^\d].+/, ""));
        if ((evt.which < 48 || evt.which > 57) && (evt.which !== 8) && (evt.keyCode !== 46) && (evt.keyCode !== 37) && (evt.keyCode !== 39)) {
            evt.preventDefault();
        }
    });
    $("#iWeightOfPacks9").keypress(function (evt) {
        var self = $(this);
        self.val(self.val().replace(/[^\d].+/, ""));
        if ((evt.which < 48 || evt.which > 57) && (evt.which !== 8) && (evt.keyCode !== 46) && (evt.keyCode !== 37) && (evt.keyCode !== 39)) {
            evt.preventDefault();
        }
    });
    $("#iWeightOfPacks10").keypress(function (evt) {
        var self = $(this);
        self.val(self.val().replace(/[^\d].+/, ""));
        if ((evt.which < 48 || evt.which > 57) && (evt.which !== 8) && (evt.keyCode !== 46) && (evt.keyCode !== 37) && (evt.keyCode !== 39)) {
            evt.preventDefault();
        }
    });
    $("#currentPackWeight").keypress(function (evt) {
        var self = $(this);
        self.val(self.val().replace(/[^\d].+/, ""));
        if ((evt.which < 48 || evt.which > 57) && (evt.which !== 8) && (evt.keyCode !== 46) && (evt.keyCode !== 37) && (evt.keyCode !== 39)) {
            evt.preventDefault();
        }
    });
}
//#endregion

//#region Function to increase weight count.
$("#addweightBtn").click(function () {
    if (document.getElementById("currentPackWeight").disabled) {
        showError("All weight values are already filled.");
        return;
    }

    if (($("#currentPackWeight").val() == "") || ($("#currentPackWeight").val() < 1) || ($("#currentPackWeight").val() > 999999)) {
        showError("Please enter a value between 1 and 999999.");
        return;
    }

    var weightCountVal = parseInt($("#weightCount").val());
    var nextWeightCountVal = weightCountVal + 1;
    if (weightCountVal !== 10) {
        $("#weightCount").val(nextWeightCountVal);
        $("#weightCountLabel").text(nextWeightCountVal + " out of 10 Completed");

        $("#iWeightOfPacks" + nextWeightCountVal).val($("#currentPackWeight").val());
        $("#currentPackWeight").val("");

        var weight = $("#iWeightOfPacks" + nextWeightCountVal).val();
        $("#iWeightOfPacks" + nextWeightCountVal).hide();
        $("#weightPack" + nextWeightCountVal).text(weight);

        if (nextWeightCountVal === 10) {
            $("#weightCountLabel").addClass("completed");
            $("#currentPackWeight").attr("disabled", "disabled");
        }
    }

    //Calculating Average of the Weights
    calculateAverageWeight(nextWeightCountVal);

    //Calculating Minimum of the Weights
    calculateMinimumWeight(nextWeightCountVal);
});
//#endregion

//#region Calculate Average Weight
function calculateAverageWeight(nextWeightCountVal) {
    var sum = 0;
    for (var i = 1; i <= nextWeightCountVal; i++) {
        sum += parseInt($("#weightPack" + i).text());
    }
    var average = sum / nextWeightCountVal;
    $("#averagePackWeight").text(average.toFixed(2));
};
//#endregion

//#region Calculate Minimum Weight
function calculateMinimumWeight(nextWeightCountVal) {
    var minimum = parseInt($("#weightPack1").text());
    for (var i = 2; i <= nextWeightCountVal; i++) {
        if (parseInt($("#weightPack" + i).text()) < minimum) {
            minimum = parseInt($("#weightPack" + i).text());
        }
    }
    $("#minimumPackWeight").text(minimum);
};
//#endregion

//#region Recipe Code Auto Complete
$("#vBatchNo").on("keyup", function (e) {
    var code = (e.keyCode ? e.keyCode : e.which);
    if (code === 13) { //Enter keycode
        return;
    }
    $("#vBatchNo").addClass("spinner");
    $("#ProductDescription").val("");
    $("#ProductWeight").val("");
    $("#nominalPackWeight").text("");
    getBatchInfoForFerrous($(this).val());
});
//#endregion

//#region Get Recipe Info from its Batch
function getBatchInfoForFerrous(text) {
    var array = new Array();
    if (text.length > 0) {
        $.post(GETBATCHBYCODE_URL,
            {
                code: text
            },
            function (data) {
                $("#vBatchNo").removeClass("spinner");
                if (data.length > 0) {
                    var arrayList = [];
                    _.each(data, function (item) {
                        arrayList.push({
                            "description": item.description == null ? "" : item.description,
                            "value": item.code == null ? "" : item.code,
                            "id": item.id == null ? "" : item.id,
                            "label": item.code == null ? "" : item.code,
                            "code": item.code == null ? "" : item.code,
                            "name": item.name == null ? "" : item.name,
                            "weight": item.weight == null ? "" : item.weight
                        });
                    });
                    $("#vBatchNo").autocomplete({
                        dataType: "json",
                        source: arrayList,
                        select: function (event, ui) {
                            var batchCode = ui.item.value;
                            $("#vBatchNo").val(batchCode);
                            $("#ProductDescription").val(ui.item.name);
                            $("#CookingPlanId").val(ui.item.id);
                            $("#ProductWeight").val(ui.item.weight);
                            $("#nominalPackWeight").text(ui.item.weight);
                        }
                    });
                }
            });
    } else {
        hideLoader();
    }
    return array;
}
//#endregion

//#region Function Submit Form
function submitFerrousDetectionAndPackWeightRecForm() {

    if (!$("#ferrousDetectionAndPackWeightForm").valid()) {
        return;
    }

    //Checking Batch Code
    if ($("#ProductDescription").val() === "") {
        showError("Please enter a valid batch code.");
        $("#vBatchNo").val("");
        return;
    }

    if (!validateWeights()) {
        showError("Please fill all the weight values first.");
        return;
    }

    showLoader();
    updateChecks();
    $("#iProductWeight").val($("#ProductWeight").val());

    $("#iAveragePackWeight").val($("#averagePackWeight").text());
    $("#iMinimumpackWeight").val($("#minimumPackWeight").text());
    $("#iNominalPackWeight").val($("#nominalPackWeight").text());

    $.ajax({
        type: "GET",
        url: SUBMITFERROUSDETECTIONANDPACKWEIGHTRECORD,
        data: $("#ferrousDetectionAndPackWeightForm").serialize(),
        success: function (result) {
            hideLoader();
            window.location = FERROUSDETECTIONANDPACKWEIGHTRECORDSLIST;
        },
        error: function (error) {
            hideLoader();
        }
    });
}
//#endregion

//#region Function: "Next" Click Event
function nextFerrousDetectionRecForm() {
    if (!$("#ferrousDetectionAndPackWeightForm").valid()) {
        return;
    }

    //Checking Batch Code
    if ($("#ProductDescription").val() === "") {
        showError("Please enter valid batch code.");
        $("#vBatchNo").val("");
        return;
    }

    if (!validateWeights()) {
        showError("Please fill all the weight values first.");
        return;
    }

    showLoader();
    updateChecks();
    $("#iProductWeight").val($("#ProductWeight").val());

    $("#iAveragePackWeight").val($("#averagePackWeight").text());
    $("#iMinimumpackWeight").val($("#minimumPackWeight").text());
    $("#iNominalPackWeight").val($("#nominalPackWeight").text());

    $.ajax({
        type: "GET",
        url: SUBMITFERROUSDETECTIONANDPACKWEIGHTRECORD,
        data: $("#ferrousDetectionAndPackWeightForm").serialize(),
        success: function (result) {
            //Resetting the fields
            fieldsResetForNextRecord();
            showSuccess("Record added successfully. Add the next now.");
            hideLoader();
        },
        error: function (error) {
            hideLoader();
        }
    });
}
//#endregion

//#region Function: Resets fields for next record
function fieldsResetForNextRecord() {
    $("#iDetectionPackWeightTestId").val("");
    $("#vLocation").val("");
    $("#vBatchNo").val("");
    $("#ProductDescription").val("");
    $("#iProductWeight").val("");
    $("#currentPackWeight").val("");
    $("#bFerrous2_8mmPassCheck").prop("checked", false);
    $("#bBeltStopWorkingCheck").prop("checked", false);
    document.getElementById("currentPackWeight").disabled = false;
    $("#weightCount").val("0");
    $("#ProductWeight").val("");
    $("#weightCountLabel").removeClass("completed");
    $("#averagePackWeight").text("");
    $("#minimumPackWeight").text("");
    $("#nominalPackWeight").text("");
    $("#weightCountLabel").text("0 out of 10 Completed");

    $("td").each(function () {
        if (typeof $("weightPack" + ($(this).index() + 1)).val() == "undefined") {
            $(this).children("input").hide();
            $(this).children("span").text("-");
        }
    });
}
//#endregion

//#region Function Cancel Form
function cancelFerrousDetectionAndPackWeightRecForm() {
    showLoader();
    window.location = FERROUSDETECTIONANDPACKWEIGHTRECORDSLIST;
}
//#endregion

//#region Validate weights
function validateWeights() {
    return document.getElementById("currentPackWeight").disabled;
}
//#endregion

//#region Function to update the check fields in Edit mode.
function updateChecksForEdit() {
    if ($("#iDetectionPackWeightTestId").val() > 0) {

        if ($("#bFerrous2_8mmPass").val() === "True") {
            $("#bFerrous2_8mmPassCheck").prop("checked", true);
        }

        if ($("#bBeltStopWorking").val() === "True") {
            $("#bBeltStopWorkingCheck").prop("checked", true);
        }
    }
}
//#endregion

//#region Function to update the check fields.
function updateChecks() {
    var check = $("#bFerrous2_8mmPassCheck").prop("checked");
    $("#bFerrous2_8mmPass").val(check);
    check = $("#bBeltStopWorkingCheck").prop("checked");
    $("#bBeltStopWorking").val(check);
}
//#endregion

//#region Function to show clicked input field.
$(".packWeights td").click(function () {

    if ($(this).index() >= $("#weightCount").val()) {
        showError("Only the filled weights are editable.");
        return;
    }

    if (($(this).index() >= 0) && ($(this).index() <= 9)) {
        $(this).children("span").hide();

        if ((!isNaN(parseInt($(this).children("span").text()))) && ($(this).children("span").text() === $(this).children("input").val())) {
            $("#selectedWeight").val($(this).children("span").text());
            $(this).children("input").val($(this).children("span").text());
        }

        $(this).children("input").show();
        $(this).children("input").focus();
    }
});
//#endregion

//#region Function to hide the out of focus input field.
$(".packWeights td").focusout(function () {
    if ($(this).children("input").val() >= 1 && $(this).children("input").val() <= 999999) {
        $(this).children("span").text($(this).children("input").val());
        //Calculating Average of the Weights
        calculateAverageWeight($("#weightCount").val());

        //Calculating Minimum of the Weights
        calculateMinimumWeight($("#weightCount").val());
    } else {
        $(this).children("span").text($("#selectedWeight").val()).show();
        $(this).children("input").val("").hide();
        showError("Please enter a value between 1 and 999999.");
        return;
    }

    $(this).children("input").hide();
    if ($(this).children("input").val() !== "") {
        $(this).children("span").text($(this).children("input").val());
    } else {
        $(this).children("span").text("-");
    }
    $(this).children("span").show();
});
//#endregion