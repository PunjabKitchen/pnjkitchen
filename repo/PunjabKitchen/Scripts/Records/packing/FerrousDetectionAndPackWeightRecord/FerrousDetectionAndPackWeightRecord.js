﻿//#region Document Ready function
$(document).ready(function () {
    $(".editable").click(function (e) {
        var tag = e.target.nodeName;
        if (tag === 'IMG') { }
        else {
            var id = $($(e.target).parent()).data('id');
            editFerrousDetectionAndPackWeightRecord(id);
        }
    });
    isCookingPlanExist('canCreatePlan', false);
});
//#endregion

//#region Create Ferrous Detection And Pack Weight Record
$("#ferrousDetectionAndPackRecordModal").click(function () {
    var canCreatePlan = $('#canCreatePlan').val().toLowerCase();
    if (canCreatePlan == 'true') {
        showLoader();
        window.location = CREATEFERROUSDETECTIONANDPACKWEIGHTRECORD;
    }
    else
        showError(cookingPlanNotFoundMsg);
});
//#endregion

//#region Edit Ferrous Detection And Pack Weight Record
function editFerrousDetectionAndPackWeightRecord(idParam) {
    showLoader();
    location.href = "EditFerrousDetectionAndPackWeightRecord/" + idParam;
};
//#endregion