﻿//#region Document Ready function
$(document).ready(function () {
    $(".editable").click(function (e) {
        var tag = e.target.nodeName;
        if (tag === 'IMG') { }
        else {
            var id = $($(e.target).parent()).data('id');
            editPackingRecord2(id);
        }
    });
    isCookingPlanExist('canCreatePlan', false);
});
//#endregion

//#region Create Packing Room Record
$("#packingRecord2Modal").click(function () {
    var canCreatePlan = $('#canCreatePlan').val().toLowerCase();
    if (canCreatePlan == 'true') {
        showLoader();
        var url = NEWPACKINGRECORD2;
        $.get(url, function (data) {
            $("#newPackingRec2").html(data);
            $("#newPackingRec2Modal").modal("show");
            hideLoader();
        });
    }
    else
        showError(cookingPlanNotFoundMsg);
});
//#endregion

//#region Edit Packing Room Record
function editPackingRecord2(idParam) {
    showLoader();
    var url = EDITPACKINGRECORD2;
    $.get(url, { id: idParam }, function (data) {
        $("#newPackingRec2").html(data);
        $("#newPackingRec2Modal").modal("show");
        hideLoader();
    });
};
//#endregion