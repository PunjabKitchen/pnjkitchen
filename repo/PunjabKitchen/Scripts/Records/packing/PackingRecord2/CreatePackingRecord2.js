﻿//#region Document Ready
$(document).ready(function () {
    initializeDatePicker();

    showLoader();
    //Triggering the Product Code's autocomplete property initially to bring it in running condition.
    initialAutocompleteRequest();

    $(".selectpicker").selectpicker({
    });

    //For Edit Mode
    if ($("#iPackingRecordId").val() > 0) {
        $("#vProductCode").val($("#vRecipeCode").val());
        $("#ProductDescription").val($("#vRecipeName").val());
        $("#LineNo").val($("#iLineNo").val());

        var packersArray = $("#packersList").val().split(",");
        dropDown = document.getElementById("packersDropdown");
        values = [];

        for (i = 0; i < dropDown.options.length; i++) {
            if (packersArray.includes(dropDown.options[i].value)) {
                values.push(dropDown.options[i].value);
                $("[data-original-index=" + i + "]").addClass("selected");
                $("[tabindex=" + i + "]").attr("aria-selected", true);
                $("[data-id=packersDropdown]").attr("title", values);
                $("[data-id=packersDropdown]").removeClass("bs-placeholder");
            }
        }
        $("#packersDropdown").val(values).change();
    }
});
//#endregion

//#region Product Code's Autocomplete Request
function initialAutocompleteRequest() {
    $.post(GETPACKINGBATCHBYCODE_URL,
            {
                code: "0"
            },
            function (data) {
                $("#vBatchNo").removeClass("spinner");
                if (data.length > 0) {
                    var arrayList = [];
                    _.each(data, function (item) {
                        arrayList.push({
                            "description": item.description == null ? "" : item.description,
                            "value": item.code == null ? "" : item.code,
                            "id": item.id == null ? "" : item.id,
                            "label": item.code == null ? "" : item.code,
                            "code": item.code == null ? "" : item.code,
                            "name": item.name == null ? "" : item.name,
                            "lineNo": item.lineNo == null ? "" : item.lineNo,
                            "recipeCode": item.recipeCode == null ? "" : item.recipeCode,
                            "weight": item.weight == null ? "" : item.weight
                        });
                    });
                    $("#vBatchNo").autocomplete({
                        dataType: "json",
                        source: arrayList,
                        select: function (event, ui) {
                            var batchCode = ui.item.code;
                            $("#vBatchNo").val(batchCode);
                            $("#CookingPlanId").val(ui.item.id);
                            $("#vProductCode").val(ui.item.recipeCode);
                            $("#LineNo").val(ui.item.lineNo);
                            $("#ProductDescription").val(ui.item.name);
                        }
                    });
                }
            });
}
//#endregion

//#region "Quantity Made" field validation
$("#iQuantityMade").keypress(function (evt) {
    var self = $(this);
    self.val(self.val().replace(/[^\d].+/, ""));
    if ((evt.which < 48 || evt.which > 57) && (evt.which !== 8) && (evt.keyCode !== 46) && (evt.keyCode !== 37) && (evt.keyCode !== 39)) {
        evt.preventDefault();
    }
});
//#endregion

//#region Initialize Date Picker
function initializeDatePicker() {
    $("#tStartTimePicker").datetimepicker({
        format: 'HH:mm'
    });

    $("#tFinishTimePicker").datetimepicker({
        format: 'HH:mm'
    });
}
//#endregion

//#region Batch Code Auto Complete
$("#vBatchNo").on("keyup", function (e) {
    var code = (e.keyCode ? e.keyCode : e.which);
    if (code === 13) { //Enter keycode
        return;
    }
    $("#vBatchNo").addClass("spinner");
    $("#ProductDescription").val("");
    $("#vProductCode").val("");
    $("#LineNo").val("");
    getBatchInfo($(this).val());
});
//#endregion

//#region Get Recipe Info from its Batch
function getBatchInfo(text) {
    var array = new Array();
    if (text.length > 0) {
        $.post(GETPACKINGBATCHBYCODE_URL,
            {
                code: text
            },
            function (data) {
                $("#vBatchNo").removeClass("spinner");
                if (data.length > 0) {
                    var arrayList = [];
                    _.each(data, function (item) {
                        arrayList.push({
                            "description": item.description == null ? "" : item.description,
                            "value": item.code == null ? "" : item.code,
                            "id": item.id == null ? "" : item.id,
                            "label": item.code == null ? "" : item.code,
                            "code": item.code == null ? "" : item.code,
                            "name": item.name == null ? "" : item.name,
                            "lineNo": item.lineNo == null ? "" : item.lineNo,
                            "recipeCode": item.recipeCode == null ? "" : item.recipeCode,
                            "weight": item.weight == null ? "" : item.weight
                        });
                    });
                    $("#vBatchNo").autocomplete({
                        dataType: "json",
                        source: arrayList,
                        select: function (event, ui) {
                            var batchCode = ui.item.code;
                            $("#vBatchNo").val(batchCode);
                            $("#CookingPlanId").val(ui.item.id);
                            $("#vProductCode").val(ui.item.recipeCode);
                            $("#LineNo").val(ui.item.lineNo);
                            $("#ProductDescription").val(ui.item.name);
                        }
                    });
                }
            });
    } else {
        hideLoader();
    }
    return array;
}
//#endregion

//#region Function Submit Form
function submitPackingRec2Form() {
    if (!$("#packingRecord2Form").valid()) {
        return;
    }

    //Checking Batch Code
    if ($("#ProductDescription").val() === "") {
        showError("Please enter valid batch number.");
        $("#vBatchNo").val("");
        return;
    }

    //Checking if start and finish time are not valid
    if (!getTimeDifference()) {
        return;
    }
    showLoader();

    $("#packersList").val($("#packersDropdown").val().join());
    $("#iLineNo").val($("#LineNo").val());

    $.ajax({
        type: "GET",
        url: SUBMITPACKINGRECORD2,
        data: $("#packingRecord2Form").serialize(),
        success: function (result) {

            if ($("#iPackingRecordId").val() > 0) {
                showSuccess("Packing Room Record updated successfully.");
            } else {
                showSuccess("Packing Room Record saved successfully.");
            }

            $("#newPackingRec2Modal").modal("hide");
            hideLoader();
            window.location = PACKINGRECORDS2LIST;
        },
        error: function (error) {
            hideLoader();
        }
    });
}
//#endregion

//#region Get Time Difference
function getTimeDifference() {
    var startTime = moment($("#tStartTimePicker").val(), 'HH:mm:ss');
    var finishTime = moment($("#tFinishTimePicker").val(), 'HH:mm:ss');

    var timeDuration = moment.duration(finishTime.diff(startTime));
    var timeHours = parseInt(timeDuration.asHours());
    var timeMinutes = parseInt(timeDuration.asMinutes()) - timeHours * 60;

    //showInfo(hours + ' hour and ' + minutes + ' minutes');
    if (isNaN(timeHours) || isNaN(timeMinutes)) {
        return false;
    }
    if ((timeHours <= 0 && timeMinutes <= 0) && startTime != "00:00:00" && finishTime != "00:00:00") {
        showInfo("Finish Time should be greater than Start time");
        return false;
    } else {
        var val = moment(timeHours + ":" + timeMinutes, "HH:mm").format("HH:mm");
        return true;
    }
}
//#endregion