﻿//#region Document Ready
$(document).ready(function () {

    //update cancel Button
    if ($("#isCancelled").val() === "true") {
        $("#cancelBtn").prop("disabled", true);
    }

    $("#doneButton").hide();

    if (!$("#iPureeShapeStarchRecordId").val() > 0) {
        $(".picklist-area").hide();
    } else {
        var batchCodesArray = $("#batchCodesList").val().split(",");
        var dropDown = document.getElementById("vBatchCode");
        var values = [];

        for (var i = 0; i < dropDown.options.length; i++) {
            if (batchCodesArray.includes(dropDown.options[i].value.split(" ")[0])) {
                values.push(dropDown.options[i].value);
                $("[data-original-index=" + i + "]").addClass("selected");
                $("[tabindex=" + i + "]").attr("aria-selected", true);
                $("[data-id=vBatchCode]").attr("title", values);
                $("[data-id=vBatchCode]").removeClass("bs-placeholder");
            }
        }
        $("#vBatchCode").val(values).change();
        $("#submitButton").text("Done");
        $("#vTotal").val("0");
        $("#vTotal").val($("#Total").val());
    }

    //initialize datepicker
    initializeDatePicker();
    numberFieldValidations();
    $('.selectpicker').selectpicker({
    });
});
//#endregion

//#region Number Validations
function numberFieldValidations() {
    $("#iProductAmount").keypress(function (evt) {
        var self = $(this);
        self.val(self.val().replace(/[^\d].+/, ""));
        if ((evt.which < 48 || evt.which > 57) && (evt.which !== 8) && (evt.keyCode !== 46) && (evt.keyCode !== 37) && (evt.keyCode !== 39)) {
            evt.preventDefault();
        }
    });
    $("#iUltraTex2").keypress(function (evt) {
        var self = $(this);
        self.val(self.val().replace(/[^\d].+/, ""));
        if ((evt.which < 48 || evt.which > 57) && (evt.which !== 8) && (evt.keyCode !== 46) && (evt.keyCode !== 37) && (evt.keyCode !== 39)) {
            evt.preventDefault();
        }
    });
    $("#iMetalose").keypress(function (evt) {
        var self = $(this);
        self.val(self.val().replace(/[^\d].+/, ""));
        if ((evt.which < 48 || evt.which > 57) && (evt.which !== 8) && (evt.keyCode !== 46) && (evt.keyCode !== 37) && (evt.keyCode !== 39)) {
            evt.preventDefault();
        }
    });
    $("#iPurityW").keypress(function (evt) {
        var self = $(this);
        self.val(self.val().replace(/[^\d].+/, ""));
        if ((evt.which < 48 || evt.which > 57) && (evt.which !== 8) && (evt.keyCode !== 46) && (evt.keyCode !== 37) && (evt.keyCode !== 39)) {
            evt.preventDefault();
        }
    });
    $("#vAdditionAmount").keypress(function (evt) {
        var self = $(this);
        self.val(self.val().replace(/[^\d].+/, ""));
        if ((evt.which < 48 || evt.which > 57) && (evt.which !== 8) && (evt.keyCode !== 46) && (evt.keyCode !== 37) && (evt.keyCode !== 39)) {
            evt.preventDefault();
        }
    });
}
//#endregion

//#region initialize Date Picker
function initializeDatePicker() {

    $("#dDate").datetimepicker({
        format: "DD-MM-YYYY HH:mm:ss",
        minDate: moment()
    });
}
//#endregion

//#region iProductAmount input event
$("#iProductAmount").bind("keyup mouseup", function () {
    calculateTotal();
});
//#endregion

//#region iUltraTex2 input event
$("#iUltraTex2").bind("keyup mouseup", function () {
    calculateTotal();
});
//#endregion

//#region iMetalose input event
$("#iMetalose").bind("keyup mouseup", function () {
    calculateTotal();
});
//#endregion

//#region iPurityW input event
$("#iPurityW").bind("keyup mouseup", function () {
    calculateTotal();
});
//#endregion

//#region Calculate Total Weight
function calculateTotal() {
    var total = 0;

    if (!isNaN($("#iProductAmount").val()) && ($("#iProductAmount").val() !== "")) {
        total = total + parseInt($("#iProductAmount").val());
    }

    if (!isNaN($("#iUltraTex2").val()) && ($("#iUltraTex2").val() !== "")) {
        total = total + (parseInt($("#iUltraTex2").val()) / 1000);
    }

    if (!isNaN($("#iMetalose").val()) && ($("#iMetalose").val() !== "")) {
        total = total + (parseInt($("#iMetalose").val()) / 1000);
    }

    if (!isNaN($("#iPurityW").val()) && ($("#iPurityW").val() !== "")) {
        total = total + (parseInt($("#iPurityW").val()) / 1000);
    }

    $("#vTotal").val(total);
}
//#endregion

//#region Function Add Addition Product
function addAdditionProduct() {
    if (!$("#PureeShapesStarchRecordForm").valid()) {
        showInfo("Complete the above form first in order to add the Additions.");
        return;
    }

    $("#Total").val($("#vTotal").val());

    if ($("#iPureeShapeStarchRecordId").val() > 0 && ($("#Sys_CreatedBy").val() !== "")) {
        showLoader();
        $("#batchCodesList").val($("#vBatchCode").val().join());
        $.ajax({
            type: "GET",
            url: SUBMITPUREESHAPESTARCH_URL,
            data: $("#PureeShapesStarchRecordForm").serialize(),
            success: function (result) {

                if (result.savedRecordID === -1) {
                    hideLoader();
                    showError("Please select the Batch Codes containing the same product.");
                    return;
                }

                $("#iPureeShapeStarchRecordAdditionId").val(result.savedRecordID);
                showSuccess("Puree Shape Record modified successfully. You can further add/remove Additions.");
                $("#vBatchCode").attr("disabled", "disabled");
                $("#vProductName").attr("disabled", "disabled");
                $("#iProductAmount").attr("disabled", "disabled");
                $("#iUltraTex2").attr("disabled", "disabled");
                $("#vUltraTex2BatchNo").attr("disabled", "disabled");
                $("#iMetalose").attr("disabled", "disabled");
                $("#vMetaloseBatchNo").attr("disabled", "disabled");
                $("#iPurityW").attr("disabled", "disabled");
                $("#vPurityWBatchNo").attr("disabled", "disabled");
                $("#firstDiv").show();
                $("#secondDiv").show();
                $("#AdditionButton").addClass("pull-left");
                $("#submitButton").hide();
                $("#doneButton").show();
                document.getElementById("AdditionButton").setAttribute("onclick", "addAdditionProductAfterModification()");
                hideLoader();
            },
            error: function (error) {
                hideLoader();
            }
        });
    }

    if (!$("#AdditionButton").hasClass("pull-left") && ($("#Sys_CreatedBy").val() === "")) {
        showLoader();

        $("#batchCodesList").val($("#vBatchCode").val().join());

        $.ajax({
            type: "GET",
            url: SUBMITPUREESHAPESTARCH_URL,
            data: $("#PureeShapesStarchRecordForm").serialize(),
            success: function (result) {

                if (result.savedRecordID === -1) {
                    hideLoader();
                    showError("Please select the Batch Codes containing the same product.");
                    return;
                }

                showSuccess("Puree Shape Record created. You can further add the Additions.");
                $("#iPureeShapeStarchRecordId").val(result.savedRecordID);
                $("#vBatchCode").attr("disabled", "disabled");
                $("#vProductName").attr("disabled", "disabled");
                $("#iProductAmount").attr("disabled", "disabled");
                $("#iUltraTex2").attr("disabled", "disabled");
                $("#vUltraTex2BatchNo").attr("disabled", "disabled");
                $("#iMetalose").attr("disabled", "disabled");
                $("#vMetaloseBatchNo").attr("disabled", "disabled");
                $("#iPurityW").attr("disabled", "disabled");
                $("#vPurityWBatchNo").attr("disabled", "disabled");
                $("#firstDiv").show();
                $("#secondDiv").show();
                $("#AdditionButton").addClass("pull-left");
                $("#submitButton").hide();
                $("#doneButton").show();
                $(".picklist-area").show();
                hideLoader();
            },
            error: function (error) {
                hideLoader();
            }
        });
    }

    if ($("#iPureeShapeStarchRecordId").val() > 0 && ($("#Sys_CreatedBy").val() === "")) {
        if (($("#vAdditionName").val() === "") || ($("#vAdditionBatchNo").val() === "") || ($("#vAdditionAmount").val() === "")) {
            showInfo("Fill all the Addition fields first to add more.");
            return;
        } else {
            //Updating total weight
            var total;
            if ($("#vAdditionAmount").val() !== "") {
                total = parseFloat($("#vTotal").val()) + parseInt($("#vAdditionAmount").val());
                $("#vTotal").val(total);
                $("#Total").val($("#vTotal").val());
            }

            showLoader();

            $.ajax({
                type: "GET",
                url: SUBMITPUREESHAPESTARCH_URL,
                data: $("#PureeShapesStarchRecordForm").serialize(),
                success: function (result) {
                    $("#iPureeShapeStarchRecordAdditionId").val(result.savedRecordID);
                    insertItemInList();
                    showSuccess("Addition for Puree Shape Record created successfully. You can add further Additions.");
                    $("#vAdditionName").val("");
                    $("#vAdditionBatchNo").val("");
                    $("#vAdditionAmount").val("");
                    hideLoader();
                },
                error: function (error) {
                    hideLoader();
                }
            });
        }
    }
}
//#endregion

//#region Function Add Additions After Master Table Modification
function addAdditionProductAfterModification() {
    if ($("#iPureeShapeStarchRecordId").val() > 0 && ($("#Sys_CreatedBy").val() !== "")) {

        //Updating total weight
        var total;
        if ($("#vAdditionAmount").val() !== "") {
            total = parseFloat($("#vTotal").val()) + parseInt($("#vAdditionAmount").val());
            $("#vTotal").val(total);
            $("#Total").val($("#vTotal").val());
        }

        if (($("#vAdditionName").val() === "") || ($("#vAdditionBatchNo").val() === "") || ($("#vAdditionAmount").val() === "")) {
            showInfo("Fill all the Addition fields first to add more.");
            return;
        } else {
            showLoader();
            $.ajax({
                type: "GET",
                url: ADDPUREESHAPESTARCHADDITION,
                data: $("#PureeShapesStarchRecordForm").serialize(),
                success: function (result) {
                    $("#iPureeShapeStarchRecordAdditionId").val(result.savedRecordID);
                    insertItemInList();
                    showSuccess("Addition for Puree Shape Record created successfully. You can add/remove further Additions.");
                    $("#vAdditionName").val("");
                    $("#vAdditionBatchNo").val("");
                    $("#vAdditionAmount").val("");
                    hideLoader();
                },
                error: function (error) {
                    hideLoader();
                }
            });
        }
    }
}
//#endregion

//#region Insert Items in the Portions List
function insertItemInList() {
    var element = document.getElementById("iAdditionUnit");
    var unit = element.options[element.selectedIndex].text;

    $("#additionsTable").find("tbody").append(
      "<tr data-id=" + $("#iPureeShapeStarchRecordAdditionId").val() + ">" +
        "<td>" + $("#vAdditionName").val() + "</td>" +
        "<td>" + $("#vAdditionBatchNo").val() + "</td>" +
        "<td>" + $("#vAdditionAmount").val() + " " + unit + "</td>" +
        "<td>" + "<span class='ico-cross cursor' onclick='deleteAdditionItem(" + $("#iPureeShapeStarchRecordAdditionId").val() + ")'></span>" + "</td>" +
      "</tr>");
}
//#endregion

//#region Delete Addition from the Listview
function deleteAdditionItem(id) {

    showLoader();

    var obj = {
        iPureeShapeStarchRecordAdditionId: id
    }

    $.ajax({
        type: "GET",
        url: DELETEADDITIONITEM,
        data: obj,
        success: function (result) {

            if (result.deletedRecordID === -1) {
                hideLoader();
                showError("Deletion Failed.");
                return;
            }

            $('#additionsTable tr[data-id="' + id + '"]').remove();
            //Updating total weight
            var total = parseFloat($("#vTotal").val()) - parseInt(result.deletedAdditionWeight);
            $("#vTotal").val(total);
            $("#Total").val($("#vTotal").val());
            showSuccess("Addition item successfully removed.");
            hideLoader();
        },
        error: function (error) {
            hideLoader();
        }
    });
}
//#endregion

//#region Function Submit Form
function submitPureeShapeStarchForm() {
    if (!$("#PureeShapesStarchRecordForm").valid()) {
        return;
    }

    showLoader();

    $("#batchCodesList").val($("#vBatchCode").val().join());
    var total;
    if ($("#vAdditionAmount").val() !== "") {
        total = parseFloat($("#vTotal").val()) + parseInt($("#vAdditionAmount").val());
        $("#vTotal").val(total);
    }

    $("#Total").val($("#vTotal").val());

    $.ajax({
        type: "GET",
        url: SUBMITPUREESHAPESTARCH_URL,
        data: $("#PureeShapesStarchRecordForm").serialize(),
        success: function (result) {

            if (result.savedRecordID === -1) {
                hideLoader();
                showError("Please select the Batch Codes containing the same product.");
                return;
            }

            if ($("#submitButton").text() === "Submit") {
                showSuccess("Puree Shape Record created successfully.");
            } else {
                showSuccess("Puree Shape Record modified successfully.");
            }

            $("#newPureeShapeStarchRecModal").modal("hide");
            hideLoader();
            window.location = PUREESHAPESTARCHLIST;
        },
        error: function (error) {
            hideLoader();
        }
    });

}
//#endregion

//#region Function Done Form
function donePureeShapeStarchForm() {
    showLoader();
    var total;
    if ($("#vAdditionAmount").val() !== "") {
        total = parseFloat($("#vTotal").val()) + parseInt($("#vAdditionAmount").val());
        $("#vTotal").val(total);
        $("#Total").val($("#vTotal").val());
    }

    if (($("#vAdditionName").val() !== "") && ($("#vAdditionBatchNo").val() !== "") && ($("#vAdditionAmount").val() !== "")) {
        $("#batchCodesList").val($("#vBatchCode").val().join());

        $.ajax({
            type: "GET",
            url: ADDPUREESHAPESTARCHADDITION,
            data: $("#PureeShapesStarchRecordForm").serialize(),
            success: function (result) {
                showSuccess("Addition for Puree Shape Record created successfully.");
                $("#newPureeShapeStarchRecModal").modal("hide");
                cancelPureeShapeStarchForm();
                hideLoader();
            },
            error: function (error) {
                hideLoader();
            }
        });
    } else {
        showLoader();
        var obj = {
            iPureeShapeStarchRecordAdditionId: $("#iPureeShapeStarchRecordId").val(),
            totalWeight: $("#vTotal").val()
        }

        $.ajax({
            type: "GET",
            url: VALIDATETOTALWEIGHT,
            data: obj,
            success: function (result) {
                cancelPureeShapeStarchForm();
            },
            error: function (error) {
                hideLoader();
            }
        });
    }
}
//#endregion

//#region Function Cancel Form
function cancelPureeShapeStarchForm() {
    showLoader();
    window.location = PUREESHAPESTARCHLIST;
}
//#endregion