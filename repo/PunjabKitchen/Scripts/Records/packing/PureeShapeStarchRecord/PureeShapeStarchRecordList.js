﻿//#region Document Ready function
$(document).ready(function () {
    $(".editable").click(function (e) {
        var tag = e.target.nodeName;
        if (tag === 'IMG') { }
        else {
            var id = $($(e.target).parent()).data('id');
            editPureeShapeStarchRecord(id);
        }
    });
    isCookingPlanExist('canCreatePlan', false);
});
//#endregion

//#region Create puree Shapes starch control record
$("#pureeShapeStarchRecordModal").click(function () {
    var canCreatePlan = $('#canCreatePlan').val().toLowerCase();
    if (canCreatePlan == 'true') {
        showLoader();
        var url = CREATEPUREESHAPESTARCHRECORD_URL;
        $.get(url, function (data) {
            $("#newPureeShapeStarchRec").html(data);
            $("#newPureeShapeStarchRecModal").modal("show");
            hideLoader();
        });
    }
    else
        showError(cookingPlanNotFoundMsg);
});
//#endregion

//#region Edit puree Shapes starch record
function editPureeShapeStarchRecord(idParam) {
    showLoader();
    var url = EDITPUREESHAPESTARCHRECORD_URL;
    $.get(url, { id: idParam }, function (data) {
        $("#newPureeShapeStarchRec").html(data);
        $("#newPureeShapeStarchRecModal").modal("show");
        hideLoader();
    });
}
//#endregion