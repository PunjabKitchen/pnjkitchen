﻿//#region Document Ready function
$(document).ready(function () {
    $(".editable").click(function (e) {
        var tag = e.target.nodeName;
        if (tag === 'IMG') { }
        else {
            var id = $($(e.target).parent()).data('id');
            editSpotWeightAndMetalCheckRecord(id);
        }
    });
    isCookingPlanExist('canCreatePlan', false);
});
//#endregion

//#region Create Spot Weight And Metal Check Record
$("#spotWeightAndMetalCheckRecordModal").click(function () {
    var canCreatePlan = $('#canCreatePlan').val().toLowerCase();
    if (canCreatePlan == 'true') {
        showLoader();
        window.location = CREATESPOTWEIGHTANDMETALCHECKRECORD;
    }
    else
        showError(cookingPlanNotFoundMsg);
});
//#endregion

//#region Edit Spot Weight And Metal Check Record
function editSpotWeightAndMetalCheckRecord(idParam) {
    showLoader();
    location.href = "EditSpotWeightAndMetalCheckRecord/" + idParam;
};
//#endregion