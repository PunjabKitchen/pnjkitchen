﻿//#region Document Ready function
$(document).ready(function () {
    $(".editable").click(function (e) {
        var tag = e.target.nodeName;
        if (tag === 'IMG') { }
        else {
            var id = $($(e.target).parent()).data('id');
            ColdDessertRecEdit(id);
        }
    });
    isCookingPlanExist('canCreatePlan', false);
});
//#endregion

//#region Create cold dessert packing
$('#coldDesertRecordModal').click(function () {
    var canCreatePlan = $('#canCreatePlan').val().toLowerCase();
    if (canCreatePlan == 'true') {
        showLoader();
        var url = CREATECOLDDESSERTPACKINGRECORD_URL;
        $.get(url, function (data) {
            $('#newColdDessertRec').html(data);
            $('#newColdDessertPackingRecModal').modal('show');
            hideLoader();
        });
    }
    else
        showError(cookingPlanNotFoundMsg);
});
//#endregion

//#region Edit cold dessert packing Record
function ColdDessertRecEdit(idParam) {


    var url = COLDDESSERTPACKINGRECORDEDIT_URL;
    $.get(url, { id: idParam }, function (data) {
        $("#newColdDessertRec").html(data);
        $("#newColdDessertPackingRecModal").modal("show");
    });

}
//#endregion