﻿//#region Document Ready
$(document).ready(function () {

    //update cancel Button
    if ($("#isCancelled").val() === "true") {
        $("#cancelBtn").prop("disabled", true);
    }

    //initialize datepicker
    initializeDatePicker();
    showLoader();
    //Triggering the Product Code's autocomplete property initially to bring it in running condition.
    initialAutocompleteRequest();
});
//#endregion

//#region Product Code's Autocomplete Request
function initialAutocompleteRequest() {
    $.post(GETBATCHBYCODE_URL,
            {
                code: "0"
            },
            function (data) {
                hideLoader();
                if (data.length > 0) {
                    var arrayList = [];
                    _.each(data, function (item) {
                        arrayList.push({
                            "description": item.description == null ? "" : item.description,
                            "value": item.code == null ? "" : item.code,
                            "id": item.id == null ? "" : item.id,
                            "label": item.code == null ? "" : item.code,
                            "code": item.code == null ? "" : item.code,
                            "name": item.name == null ? "" : item.name
                        });
                    });
                    $("#vBatchNo").autocomplete({
                        dataType: "json",
                        source: arrayList,
                        select: function (event, ui) {
                            var batchCode = ui.item.batchNo;
                            $("#vBatchNo").val(batchCode);
                            $("#ProductDescription").val(ui.item.name);
                            $("#CookingPlanId").val(ui.item.id);
                        }
                    });
                }
            });
}
//#endregion

//#region Date time picker onchange
$("#tTimeExMixingPicker").on("dp.change", function () {
    getTimeDifference("tTimeExMixingPicker");
});

$("#tTimeOfLastTrayInBlastFreezerPicker").on("dp.change", function () {
    getTimeDifference("tTimeOfLastTrayInBlastFreezerPicker");
});
//#endregion

//#region initialize Date Picker
function initializeDatePicker() {

    $("#dDate").datetimepicker({
        format: "DD-MM-YYYY HH:mm:ss",
        minDate: moment()
    });

    $("#tTimeExMixingPicker").datetimepicker({
        format: "HH:mm"
    });

    $("#tTimeOfLastTrayInBlastFreezerPicker").datetimepicker({
        format: "HH:mm"
    });
    $("#tTimeTakenPicker").datetimepicker({
        format: "HH:mm"
    });

    if ($("#ColdDesertPackingRecordId").val() > 0) {
        $("#tTimeExMixingPicker").val(moment($("#tTimeExMixing").val()).format("HH:mm"));
        $("#tTimeOfLastTrayInBlastFreezerPicker").val(moment($("#tTimeOfLastTrayInBlastFreezer").val()).format("HH:mm"));
        $("#tTimeTakenPicker").val(moment($("#tTimeTaken").val()).format("HH:mm"));
    }
}
//#endregion

//#region Batch Code Auto Complete
$("#vBatchNo").on("keyup", function (e) {
    var code = (e.keyCode ? e.keyCode : e.which);
    if (code === 13) { //Enter keycode
        return;
    }
    $("#ProductDescription").val("");
    $("#vBatchNo").addClass("spinner");
    getRecipeInfo($(this).val());
});
//#endregion

//#region Get Recipe Info
function getRecipeInfo(text) {
    var array = new Array();
    if (text.length > 0) {
        $.post(GETBATCHBYCODE_URL,
            {
                code: text
            },
            function (data) {
                $("#vBatchNo").removeClass("spinner");
                if (data.length > 0) {
                    var arrayList = [];
                    _.each(data, function (item) {
                        arrayList.push({
                            "description": item.description == null ? "" : item.description,
                            "value": item.code == null ? "" : item.code,
                            "id": item.id == null ? "" : item.id,
                            "label": item.code == null ? "" : item.code,
                            "code": item.code == null ? "" : item.code,
                            "name": item.name == null ? "" : item.name
                        });
                     });
                    $("#vBatchNo").autocomplete({
                        dataType: "json",
                        source: arrayList,
                        select: function (event, ui) {
                            var batchCode = ui.item.batchNo;
                            $("#vBatchNo").val(batchCode);
                            $("#ProductDescription").val(ui.item.name);
                            $("#CookingPlanId").val(ui.item.id);
                        }
                    });
                }
            });
    } else {
        hideLoader();
    }
    return array;
}
//#endregion

//#region Function Submit Form
function submitColdDessertPackingRecForm() {
    if (!$("#coldDessertPackingRecordForm").valid()) {
        if ($("#tTimeExMixingPicker").val() === "") {
            $("#tTimeExMixingPicker").addClass("error");
        }
        if ($("#tTimeOfLastTrayInBlastFreezerPicker").val() === "") {
            $("#tTimeOfLastTrayInBlastFreezerPicker").addClass("error");
        }
        return;
    } else if (($("#tTimeExMixingPicker").val() === "")) {
        $("#tTimeExMixingPicker").addClass("error");
        return;
    } else if ($("#tTimeOfLastTrayInBlastFreezerPicker").val() === "") {
        $("#tTimeOfLastTrayInBlastFreezerPicker").addClass("error");
        return;
    }

    //Checking Batch Code
    if ($("#ProductDescription").val() === "") {
        showError("Please enter valid Batch No.");
        $("#ProductCode").val("");
        return;
    }

    showLoader();

    var dt = new Date();
    $("#dDate").val(dt);
    $("#tTimeExMixing").val($("#tTimeExMixingPicker").val());
    $("#tTimeOfLastTrayInBlastFreezer").val($("#tTimeOfLastTrayInBlastFreezerPicker").val());
    $("#tTimeTaken").val($("#tTimeTakenPicker").val());

    $.ajax({
        type: "GET",
        url: SUBMITCOLDDESSERTPACKINGRECORD_URL,
        data: $("#coldDessertPackingRecordForm").serialize(),
        success: function (result) {
            $("#newColdDessertPackingRecModal").modal("hide");
            hideLoader();
            window.location = COLDDESSERTPACKINGRECORDSLIST;
        },
        error: function (error) {
            hideLoader();
        }
    });
}
//#endregion

//#region Get Time Difference
function getTimeDifference(picker) {
    if (($("#tTimeExMixingPicker").val() == "") && (picker === "tTimeOfLastTrayInBlastFreezerPicker")) {
        $("#tTimeOfLastTrayInBlastFreezerPicker").val("");
        $("#bCriticalCheck").val(false);
        showError("First fill \"Time Ex Mixing\".");
        return false;
    } else if (picker === "") {
        return false;
    }
    var startTime = moment($("#tTimeExMixingPicker").val(), "HH:mm:ss");
    var endTime = moment($("#tTimeOfLastTrayInBlastFreezerPicker").val(), "HH:mm:ss");

    var duration = moment.duration(endTime.diff(startTime));
    var hours = parseInt(duration.asHours());
    var minutes = parseInt(duration.asMinutes()) - hours * 60;

    if (isNaN(hours) || isNaN(minutes)) {
        return false;
    }
    if ((hours < 0 || minutes < 0) && startTime !== "00:00:00" && endTime !== "00:00:00") {

        if (($("#tTimeExMixingPicker").val() != "") && ($("#tTimeOfLastTrayInBlastFreezerPicker").val() != "")) {
            if (picker === "tTimeExMixingPicker") {
                $("#tTimeExMixingPicker").val("");
            } else if (picker === "tTimeOfLastTrayInBlastFreezerPicker") {
                $("#tTimeOfLastTrayInBlastFreezerPicker").val("");
            }
            showError("\"Time of Last Tray In Blast Freezer\" should be greater than \"Time Ex Mixing\"");
        }
        $("#tTimeTakenPicker").val("");
        $("#bCriticalCheck").val(false);
        return false;
    } else {
        var val = moment(hours + ":" + minutes, "HH:mm").format("HH:mm");
        $("#tTimeTakenPicker").val(val);

        if (hours >= 1 && minutes >= 0) {
            if (hours === 1 && minutes === 0) {
                $("#bCriticalCheck").val(false);
            } else {
                $("#bCriticalCheck").val(true);
            }           
        } else {
            $("#bCriticalCheck").val(false);
        }

        return true;
    }
}
//#endregion