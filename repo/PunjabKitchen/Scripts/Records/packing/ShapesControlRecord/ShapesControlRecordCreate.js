﻿//#region Document Ready
$(document).ready(function () {

    //update cancel Button
    if ($("#isCancelled").val() === "true") {
        $("#cancelBtn").prop("disabled", true);
    }

    if ($("#ShapeControlRecordId").val() > 0) {
        var batchCodesArray = $("#batchCodesList").val().split(",");
        var dropDown = document.getElementById("vBatchCode");
        var values = [];
        var i = 0;

        for (i = 0; i < dropDown.options.length; i++) {
            if (batchCodesArray.includes(dropDown.options[i].value.split(" ")[0])) {
                values.push(dropDown.options[i].value);
                $("[data-original-index=" + i + "]").addClass("selected");
                $("[tabindex=" + i + "]").attr("aria-selected", true);
                $("[data-id=vBatchCode]").attr("title", values);
                $("[data-id=vBatchCode]").removeClass("bs-placeholder");
            }
        }
        $("#vBatchCode").val(values).change();

        var packersArray = $("#packersList").val().split(",");
        dropDown = document.getElementById("packersDropdown");
        values = [];

        for (i = 0; i < dropDown.options.length; i++) {
            if (packersArray.includes(dropDown.options[i].value)) {
                values.push(dropDown.options[i].value);
                $("[data-original-index=" + i + "]").addClass("selected");
                $("[tabindex=" + i + "]").attr("aria-selected", true);
                $("[data-id=packersDropdown]").attr("title", values);
                $("[data-id=packersDropdown]").removeClass("bs-placeholder");
            }
        }
        $("#packersDropdown").val(values).change();
    }

    //Initialize datepicker and Numbers Validation
    initializeDatePicker();
    NumberValidations();

    $(".selectpicker").selectpicker({
    });

});
//#endregion

//#region Number Validation
function NumberValidations() {
    $("#iQuantityMade").keypress(function (evt) {
        var self = $(this);
        self.val(self.val().replace(/[^\d].+/, ""));
        if ((evt.which < 48 || evt.which > 57) && (evt.which !== 8) && (evt.keyCode !== 46) && (evt.keyCode !== 37) && (evt.keyCode !== 39)) {
            evt.preventDefault();
        }
    });
    $("#vTempExBlastFreezerBelow").keypress(function (evt) {    
        if (!(evt.which === 45 || (evt.which >= 48 && evt.which <= 57))) {
            evt.preventDefault();
        }
    });
}
//#endregion

//#region initialize Date Picker
function initializeDatePicker() {

    $("#dShapesControl").datetimepicker({
        format: "DD-MM-YYYY HH:mm:ss",
        minDate: moment()
    });

    $("#tCookingFinishTimePicker").datetimepicker({
        format: "HH:mm"
    });

    $("#tPureeStartTimePicker").datetimepicker({
        format: "HH:mm"
    });
    $("#tTimeIntoBlastFreezerPicker").datetimepicker({
        format: "HH:mm"
    });
    $("#tTimeExInBlastFreezerPicker").datetimepicker({
        format: "HH:mm"
    });
    $("#tTimeToStorageFreezerPicker").datetimepicker({
        format: "HH:mm"
    });

    if ($("#ShapeControlRecordId").val() > 0) {
         $("#tCookingFinishTimePicker").val(moment($('#tCookingFinishTime').val(), 'HH:mm').format('HH:mm'));
        $("#tPureeStartTimePicker").val(moment($('#tPureeStartTime').val()).format("HH:mm"));
        $("#tTimeIntoBlastFreezerPicker").val(moment($("#tTimeIntoBlastFreezer").val()).format("HH:mm"));
        $("#tTimeExInBlastFreezerPicker").val(moment($('#tTimeExInBlastFreezer').val()).format('HH:mm'));
        $("#tTimeToStorageFreezerPicker").val(moment($('#tTimeToStorageFreezer').val()).format('HH:mm'));

    }
}
//#endregion

//#region Function Submit Form
function submitShapesControlForm() {

    if (!$("#ShapesControlRecordForm").valid()) {
        if ($("#tCookingFinishTimePicker").val() === "") {
            $("#tCookingFinishTimePicker").addClass("error");
        }
        if ($("#tPureeStartTimePicker").val() === "") {
            $("#tPureeStartTimePicker").addClass("error");
        }
        if ($("#tTimeIntoBlastFreezerPicker").val() === "") {
            $("#tTimeIntoBlastFreezerPicker").addClass("error");
        }
        if ($("#tTimeExInBlastFreezerPicker").val() === "") {
            $("#tTimeExInBlastFreezerPicker").addClass("error");
        }
        if ($("#tTimeToStorageFreezerPicker").val() === "") {
            $("#tTimeToStorageFreezerPicker").addClass("error");
        }
        return;
    } else if (($("#tCookingFinishTimePicker").val() === "")) {
        $("#tCookingFinishTimePicker").addClass("error");
        return;
    } else if ($("#tPureeStartTimePicker").val() === "") {
        $("#tPureeStartTimePicker").addClass("error");
        return;
    } else if ($("#tTimeIntoBlastFreezerPicker").val() === "") {
        $("#tTimeIntoBlastFreezerPicker").addClass("error");
        return;
    } else if ($("#tTimeExInBlastFreezerPicker").val() === "") {
        $("#tTimeExInBlastFreezerPicker").addClass("error");
        return;
    } else if ($("#tTimeToStorageFreezerPicker").val() === "") {
        $("#tTimeToStorageFreezerPicker").addClass("error");
        return;
    }

    showLoader();

    $("#tCookingFinishTime").val($("#tCookingFinishTimePicker").val());
    $("#tPureeStartTime").val($("#tPureeStartTimePicker").val());
    $("#tTimeIntoBlastFreezer").val($("#tTimeIntoBlastFreezerPicker").val());
    $("#tTimeExInBlastFreezer").val($("#tTimeExInBlastFreezerPicker").val());
    $("#tTimeToStorageFreezer").val($("#tTimeToStorageFreezerPicker").val());
    $("#packersList").val($("#packersDropdown").val().join());
    $("#batchCodesList").val($("#vBatchCode").val().join());
    $("#vBatchCode").val("");

    var vTempExBlastFreezerBelow = parseInt($("#vTempExBlastFreezerBelow").val());
    if (vTempExBlastFreezerBelow >= -18) {
        $("#bCriticalCheck").val(true);
    } else {
        $("#bCriticalCheck").val(false);
    }

    $.ajax({
        type: "GET",
        url: SUBMITSHAPESCONTROL_URL,
        data: $("#ShapesControlRecordForm").serialize(),
        success: function (result) {

            if (result.savedRecordID === -1) {
                hideLoader();
                showError("Please select the Batch Numbers containing the same product.");
                return;
            }

            if ($("#ShapeControlRecordId").val() > 0) {
                showSuccess("Shapes Control Record updated successfully.");
            } else {
                showSuccess("Shapes Control Record created successfully.");
            }
            
            $("#newShapesControlRecModal").modal("hide");
            hideLoader();
            window.location = SHAPESCONTROLLIST;
        },
        error: function (error) {
            hideLoader();
        }
    });

}
//#endregion