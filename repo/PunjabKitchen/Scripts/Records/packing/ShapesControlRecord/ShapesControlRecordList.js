﻿//#region Document Ready
$(document).ready(function () {
    $(".editable").click(function (e) {
        var tag = e.target.nodeName;
        if (tag === 'IMG') { }
        else {
            var id = $($(e.target).parent()).data('id');
            EditShapesControlRecord(id);
        }
    });
    isCookingPlanExist('canCreatePlan', false);
});
//#endregion

//#region Create Shapes control record
$("#shapesControlRecordModal").click(function () {
    var canCreatePlan = $('#canCreatePlan').val().toLowerCase();
    if (canCreatePlan == 'true') {
        showLoader();
        var url = CREATESHAPESCONTROLRECORD_URL;
        $.get(url, function (data) {
            $("#newShapesControlRec").html(data);
            $("#newShapesControlRecModal").modal("show");
            hideLoader();
        });
    }
    else
        showError(cookingPlanNotFoundMsg);
});
//#endregion


//#region Edit shapes control record
function EditShapesControlRecord(idParam) {
    showLoader();
    var url = SHAPESCONTROLRECORDEDIT_URL;
    $.get(url, { id: idParam }, function (data) {
        $("#newShapesControlRec").html(data);
        $("#newShapesControlRecModal").modal("show");
        hideLoader();
    });
}
//#endregion
