﻿//#region Document Ready
$(document).ready(function () {

    showLoader();
    //Triggering the Product Code's autocomplete property initially to bring it in running condition.
    initialAutocompleteRequest();
    initialAutocompleteRequest();

    initializeDatePicker();
    NumberValidations();
    updateChecksForEdit();
    $("#packingRecordForm").validate().settings.ignore = "";

    $(".selectpicker").selectpicker({
    });
});
//#endregion

//#region Product Code's Autocomplete Request
function initialAutocompleteRequest() {
    $.post(GETBATCHBYCODE_URL,
            {
                code: "00"
            },
            function (data) {
                hideLoader();
                if (data.length > 0) {
                    var arrayList = [];
                    _.each(data, function (item) {
                        arrayList.push({
                            "description": item.description == null ? "" : item.description,
                            "value": item.code == null ? "" : item.code,
                            "id": item.id == null ? "" : item.id,
                            "label": item.code == null ? "" : item.code,
                            "code": item.code == null ? "" : item.code,
                            "name": item.name == null ? "" : item.name,
                            "weight": item.weight == null ? "" : item.weight
                        });
                    });
                    $("#vBatchNo").autocomplete({
                        dataType: "json",
                        source: arrayList,
                        select: function (event, ui) {
                            var batchCode = ui.item.value;
                            $("#vBatchNo").val(batchCode);
                            $("#ProductName").val(ui.item.name);
                            $("#CookingPlanId").val(ui.item.id);
                            $("#ProductWeight").val(ui.item.weight);
                        }
                    });
                }
            });
}
//#endregion

//#region Number fields validation
function NumberValidations() {
    $("#vMealStartTemp").keypress(function (evt) {
        if (!(evt.which === 45 || (evt.which >= 48 && evt.which <= 57))) {
            evt.preventDefault();
        }
    });
    $("#vMealFinishTemp").keypress(function (evt) {
        if (!(evt.which === 45 || (evt.which >= 48 && evt.which <= 57))) {
            evt.preventDefault();
        }
    });
    $("#vSauceStartTemp").keypress(function (evt) {
        if (!(evt.which === 45 || (evt.which >= 48 && evt.which <= 57))) {
            evt.preventDefault();
        }
    });
    $("#vSauceFinishTemp").keypress(function (evt) {
        if (!(evt.which === 45 || (evt.which >= 48 && evt.which <= 57))) {
            evt.preventDefault();
        }
    });
    $("#vVeg_RiceStartTemp").keypress(function (evt) {
        if (!(evt.which === 45 || (evt.which >= 48 && evt.which <= 57))) {
            evt.preventDefault();
        }
    });
    $("#vVeg_RiceFinishTemp").keypress(function (evt) {
        if (!(evt.which === 45 || (evt.which >= 48 && evt.which <= 57))) {
            evt.preventDefault();
        }
    });
    $("#iPackagingSize").keypress(function (evt) {
        var self = $(this);
        self.val(self.val().replace(/[^\d].+/, ""));
        if ((evt.which < 48 || evt.which > 57) && (evt.which !== 8) && (evt.keyCode !== 46) && (evt.keyCode !== 37) && (evt.keyCode !== 39)) {
            evt.preventDefault();
        }
    });
    $("#iLabelQuantity").keypress(function (evt) {
        var self = $(this);
        self.val(self.val().replace(/[^\d].+/, ""));
        if ((evt.which < 48 || evt.which > 57) && (evt.which !== 8) && (evt.keyCode !== 46) && (evt.keyCode !== 37) && (evt.keyCode !== 39)) {
            evt.preventDefault();
        }
    });
    $("#iNumberOfPacksMade").keypress(function (evt) {
        var self = $(this);
        self.val(self.val().replace(/[^\d].+/, ""));
        if ((evt.which < 48 || evt.which > 57) && (evt.which !== 8) && (evt.keyCode !== 46) && (evt.keyCode !== 37) && (evt.keyCode !== 39)) {
            evt.preventDefault();
        }
    });
}
//#endregion

//#region Time Pickers Validations
$("#tStartTimePicker").on("dp.change", function () {
    if ($("#tStartTimePicker").val() !== "") {
        $("#tStartTimePicker").removeClass("error");
    }
});

$("#tBatchChangeCheckTimePicker").on("dp.change", function () {
    if ($("#tBatchChangeCheckTimePicker").val() !== "") {
        $("#tBatchChangeCheckTimePicker").removeClass("error");
    }
});

$("#tFinishTimePicker").on("dp.change", function () {
    if ($("#tFinishTimePicker").val() !== "") {
        $("#tFinishTimePicker").removeClass("error");
    }
});

$("#tTimeToFreezerTimePicker").on("dp.change", function () {
    if ($("#tTimeToFreezerTimePicker").val() !== "") {
        $("#tTimeToFreezerTimePicker").removeClass("error");
    }
});
//#endregion

//#region vMealStartTemp input event
$("#vMealStartTemp").bind("keyup mouseup", function () {
    if ($("#vMealStartTemp").val() > 3) {
        $("#vMealStartTemp").addClass("error");
    } else {
        $("#vMealStartTemp").removeClass("error");
    }
});
//#endregion

//#region vMealFinishTemp input event
$("#vMealFinishTemp").bind("keyup mouseup", function () {
    if ($("#vMealFinishTemp").val() > 3) {
        $("#vMealFinishTemp").addClass("error");
    } else {
        $("#vMealFinishTemp").removeClass("error");
    }
});
//#endregion

//#region vSauceStartTemp input event
$("#vSauceStartTemp").bind("keyup mouseup", function () {
    if ($("#vSauceStartTemp").val() > 3) {
        $("#vSauceStartTemp").addClass("error");
    } else {
        $("#vSauceStartTemp").removeClass("error");
    }
});
//#endregion

//#region vSauceFinishTemp input event
$("#vSauceFinishTemp").bind("keyup mouseup", function () {
    if ($("#vSauceFinishTemp").val() > 3) {
        $("#vSauceFinishTemp").addClass("error");
    } else {
        $("#vSauceFinishTemp").removeClass("error");
    }
});
//#endregion

//#region vVeg_RiceStartTemp input event
$("#vVeg_RiceStartTemp").bind("keyup mouseup", function () {
    if ($("#vVeg_RiceStartTemp").val() > 3) {
        $("#vVeg_RiceStartTemp").addClass("error");
    } else {
        $("#vVeg_RiceStartTemp").removeClass("error");
    }
});
//#endregion

//#region vVeg_RiceFinishTemp input event
$("#vVeg_RiceFinishTemp").bind("keyup mouseup", function () {
    if ($("#vVeg_RiceFinishTemp").val() > 3) {
        $("#vVeg_RiceFinishTemp").addClass("error");
    } else {
        $("#vVeg_RiceFinishTemp").removeClass("error");
    }
});
//#endregion

//#region Initialize Date Picker
function initializeDatePicker() {
    $("#dLabelBestBeforeDatePicker").datetimepicker({
        format: "DD-MM-YYYY",
        minDate: moment()
    });

    $("#tStartTimePicker").datetimepicker({
        format: "HH:mm"
    });

    $("#tBatchChangeCheckTimePicker").datetimepicker({
        format: "HH:mm"
    });

    $("#tFinishTimePicker").datetimepicker({
        format: "HH:mm"
    });

    $("#tTimeToFreezerTimePicker").datetimepicker({
        format: "HH:mm"
    });
}
//#endregion

//#region Batch Code Auto Complete
$("#vBatchNo").on("keyup", function (e) {
    var code = (e.keyCode ? e.keyCode : e.which);
    if (code === 13) { //Enter keycode
        return;
    }
    $("#ProductName").val("");
    $("#ProductWeight").val("");
    $("#vBatchNo").addClass("spinner");
    getBatchInfoForPacking($(this).val());
});
//#endregion

//#region Get Batch Info
function getBatchInfoForPacking(text) {
    var array = new Array();
    if (text.length > 0) {
        $.post(GETBATCHBYCODE_URL,
            {
                code: text
            },
            function (data) {
                $("#vBatchNo").removeClass("spinner");
                if (data.length > 0) {
                    var arrayList = [];
                    _.each(data, function (item) {
                        arrayList.push({
                            "description": item.description == null ? "" : item.description,
                            "value": item.code == null ? "" : item.code,
                            "id": item.id == null ? "" : item.id,
                            "label": item.code == null ? "" : item.code,
                            "code": item.code == null ? "" : item.code,
                            "name": item.name == null ? "" : item.name,
                            "weight": item.weight == null ? "" : item.weight
                        });
                    });
                    $("#vBatchNo").autocomplete({
                        dataType: "json",
                        source: arrayList,
                        select: function (event, ui) {
                            var batchCode = ui.item.value;
                            $("#vBatchNo").val(batchCode);
                            $("#ProductName").val(ui.item.name);
                            $("#CookingPlanId").val(ui.item.id);
                            $("#ProductWeight").val(ui.item.weight);
                        }
                    });
                }
            });
    } else {
        hideLoader();
    }
    return array;
}
//#endregion

//#region Validate Fields
function validateFields() {
    $("#vBatchNo").valid();
    $("#vProductType").valid();
    $("#packersDropdown").valid();
    $("#vMealBatchNo").valid();
    $("#vMealStartTemp").valid();
    $("#vMealFinishTemp").valid();
    $("#vSauceBatchNo").valid();
    $("#vSauceStartTemp").valid();
    $("#vSauceStartTemp").valid();
    $("#vVeg_RiceName").valid();
    $("#vVeg_RiceBatchNo").valid();
    $("#vVeg_RiceStartTemp").valid();
    $("#vVeg_RiceFinishTemp").valid();
    $("#vPackagingType").valid();
    $("#iPackagingSize").valid();
    $("#vPackagingBatchNo").valid();
    $("#Lid_FilmBatchNo").valid();
    $("#vLabelProductCode").valid();
    $("#vLabelType").valid();
    $("#iLabelQuantity").valid();
    $("#vLabel_BatchNo").valid();
    $("#iNumberOfPacksMade").valid();
    if ($("#tStartTimePicker").val() === "") {
        $("#tStartTimePicker").addClass("error");
    }
    if ($("#tBatchChangeCheckTimePicker").val() === "") {
        $("#tBatchChangeCheckTimePicker").addClass("error");
    }
    if ($("#tFinishTimePicker").val() === "") {
        $("#tFinishTimePicker").addClass("error");
    }
    if ($("#tTimeToFreezerTimePicker").val() === "") {
        $("#tTimeToFreezerTimePicker").addClass("error");
    }

    if ((!$("#vBatchNo").valid()) || (!$("#vProductType").valid()) || ($("#tStartTimePicker").val() === "") || (!$("#packersDropdown").val())) {
        if (!$("#tab_1").hasClass("active")) {
            $("#tab_2").removeClass("active");
            $("#profile").removeClass("active");
            $("#tab_3").removeClass("active");
            $("#messages").removeClass("active");
            $("#tab_4").removeClass("active");
            $("#settings").removeClass("active");
            $("#tab_5").removeClass("active");
            $("#Finish").removeClass("active");
            $("#tab_1").addClass("active");
            $("#home").addClass("active");
        }
        showError("Fill all the required fields in all the tabs first.");
        return false;
    } else if ((!$("#vMealBatchNo").valid()) || (!$("#vMealStartTemp").valid()) || (!$("#vMealFinishTemp").valid()) || (!$("#vSauceBatchNo").valid()) || (!$("#vSauceStartTemp").valid()) || (!$("#vSauceFinishTemp").valid()) || (!$("#vVeg_RiceName").valid()) || (!$("#vVeg_RiceBatchNo").valid()) || (!$("#vVeg_RiceStartTemp").valid()) || (!$("#vVeg_RiceFinishTemp").valid())) {
        if (!$("#tab_2").hasClass("active")) {
            $("#tab_1").removeClass("active");
            $("#home").removeClass("active");
            $("#tab_3").removeClass("active");
            $("#messages").removeClass("active");
            $("#tab_4").removeClass("active");
            $("#settings").removeClass("active");
            $("#tab_5").removeClass("active");
            $("#Finish").removeClass("active");
            $("#tab_2").addClass("active");
            $("#profile").addClass("active");
        }
        showError("Fill all the required fields in all the tabs first.");
        return false;
    } else if (($("#tBatchChangeCheckTimePicker").val() === "") || (!$("#vPackagingType").valid()) || (!$("#iPackagingSize").valid()) || (!$("#vPackagingBatchNo").valid()) || (!$("#Lid_FilmBatchNo").valid())) {
        if (!$("#tab_3").hasClass("active")) {
            $("#tab_1").removeClass("active");
            $("#home").removeClass("active");
            $("#tab_2").removeClass("active");
            $("#profile").removeClass("active");
            $("#tab_4").removeClass("active");
            $("#settings").removeClass("active");
            $("#tab_5").removeClass("active");
            $("#Finish").removeClass("active");
            $("#tab_3").addClass("active");
            $("#messages").addClass("active");
        }
        showError("Fill all the required fields in all the tabs first.");
        return false;
    } else if ((!$("#vLabelProductCode").valid()) || (!$("#vLabelType").valid()) || (!$("#iLabelQuantity").valid()) || (!$("#vLabel_BatchNo").valid()) || ($("#dLabelBestBeforeDatePicker").val() === "")) {
        if (!$("#tab_4").hasClass("active")) {
            $("#tab_1").removeClass("active");
            $("#home").removeClass("active");
            $("#tab_2").removeClass("active");
            $("#profile").removeClass("active");
            $("#tab_3").removeClass("active");
            $("#messages").removeClass("active");
            $("#tab_5").removeClass("active");
            $("#Finish").removeClass("active");
            $("#tab_4").addClass("active");
            $("#settings").addClass("active");
        }
        showError("Fill all the required fields in all the tabs first.");
        return false;
    } else if (($("#tFinishTimePicker").val() === "") || (!$("#iNumberOfPacksMade").valid()) || ($("#tTimeToFreezerTimePicker").val() === "")) {
        if (!$("#tab_5").hasClass("active")) {
            $("#tab_1").removeClass("active");
            $("#home").removeClass("active");
            $("#tab_2").removeClass("active");
            $("#profile").removeClass("active");
            $("#tab_3").removeClass("active");
            $("#messages").removeClass("active");
            $("#tab_4").removeClass("active");
            $("#settings").removeClass("active");
            $("#tab_5").addClass("active");
            $("#Finish").addClass("active");
        }
        showError("Fill all the required fields in all the tabs first.");
        return false;
    }
    return true;
}
//#endregion

//#region Function Submit Form
function submitPackingRecForm() {

    if (!validateFields()) {
        return;
    }

    var bestBeforeEndDate = $("#dLabelBestBeforeDatePicker").val();
    $("#dLabelBestBefore").val(getDateTimeForBestBefore(bestBeforeEndDate));
    $("#tStartTime").val($("#tStartTimePicker").val());
    $("#tBatchChangeCheckTime").val($("#tBatchChangeCheckTimePicker").val());
    $("#tFinishTime").val($("#tFinishTimePicker").val());
    $("#tTimeToFreezer").val($("#tTimeToFreezerTimePicker").val());

    //Checking Batch Code
    if ($("#ProductName").val() === "") {
        showError("Please enter a valid Batch Number.");
        $("#vBatchNo").val("");
        $("#ProductName").val("");
        $("#ProductWeight").val("");
        return;
    }

    //Checking if start and finish time are not valid
    if (!getTimeDifference()) {
        return;
    }

    CriticalChecks();
    $("#packersList").val($("#packersDropdown").val().join());
    $("#vProductName").val($("#ProductName").val());
    $("#iProductWeight").val($("#ProductWeight").val());

    showLoader();
    updateChecks();

    $.ajax({
        type: "GET",
        url: SUBMITPACKINGRECORD,
        data: $("#packingRecordForm").serialize(),
        success: function (result) {

            if ($("#iPackingRecordId").val() > 0) {
                showSuccess("Packing Record updated successfully.");
            } else {
                showSuccess("Packing Record saved successfully.");
            }
            hideLoader();
            window.location = PACKINGRECORDSLIST;
        },
        error: function (error) {
            hideLoader();
        }
    });
}
//#endregion

//#region Function to set Critical Checks
function CriticalChecks() {
    var vMealStartTemp = parseInt($("#vMealStartTemp").val());
    if (vMealStartTemp > 3) {
        $("#vMealStartTempCriticalCheck").val(true);
    } else {
        $("#vMealStartTempCriticalCheck").val(false);
    }

    var vMealFinishTemp = parseInt($("#vMealFinishTemp").val());
    if (vMealFinishTemp > 3) {
        $("#vMealFinishTempCriticalCheck").val(true);
    } else {
        $("#vMealFinishTempCriticalCheck").val(false);
    }

    var vSauceStartTemp = parseInt($("#vSauceStartTemp").val());
    if (vSauceStartTemp > 3) {
        $("#vSauceStartTempCriticalCheck").val(true);
    } else {
        $("#vSauceStartTempCriticalCheck").val(false);
    }

    var vSauceFinishTemp = parseInt($("#vSauceFinishTemp").val());
    if (vSauceFinishTemp > 3) {
        $("#vSauceFinishTempCriticalCheck").val(true);
    } else {
        $("#vSauceFinishTempCriticalCheck").val(false);
    }

    var vVegRiceStartTemp = parseInt($("#vVeg_RiceStartTemp").val());
    if (vVegRiceStartTemp > 3) {
        $("#vVeg_RiceStartTempCriticalCheck").val(true);
    } else {
        $("#vVeg_RiceStartTempCriticalCheck").val(false);
    }

    var vVegRiceFinishTemp = parseInt($("#vVeg_RiceFinishTemp").val());
    if (vVegRiceFinishTemp > 3) {
        $("#vVeg_RiceFinishTempCriticalCheck").val(true);
    } else {
        $("#vVeg_RiceFinishTempCriticalCheck").val(false);
    }
}
//#endregion

//#region Function to update the check fields in Edit mode.
function updateChecksForEdit() {
    if ($("#iPackingRecordId").val() > 0) {

        $("#tStartTimePicker").val(moment($("#tStartTime").val()).format("HH:mm"));
        $("#tBatchChangeCheckTimePicker").val(moment($("#tBatchChangeCheckTime").val()).format("HH:mm"));
        $("#tFinishTimePicker").val(moment($("#tFinishTime").val()).format("HH:mm"));
        $("#tTimeToFreezerTimePicker").val(moment($("#tTimeToFreezerTime").val()).format("HH:mm"));

        var bestBeforeEndDate = $("#dLabelBestBefore").val();
        bestBeforeEndDate = bestBeforeEndDate.split(' ')[0];
        $("#dLabelBestBeforeDatePicker").val(bestBeforeEndDate);

        if ($("#bEquipVisualHygieneChk").val() === "True") {
            $("#bEquipVisualHygieneCheck").prop("checked", true);
        }

        if ($("#bOldPackaging_LabelsCleared").val() === "True") {
            $("#bOldPackaging_LabelsClearedCheck").prop("checked", true);
        }

        if ($("#bCheckDuring").val() === "True") {
            $("#bCheckDuringCheck").prop("checked", true);
        }

        if ($("#bCheckStart").val() === "True") {
            $("#bCheckStartCheck").prop("checked", true);
        }

        if ($("#bCheckIfChange").val() === "True") {
            $("#bCheckIfChangeCheck").prop("checked", true);
        }

        if ($("#bCheckEnd").val() === "True") {
            $("#bCheckEndCheck").prop("checked", true);
        }

        if ($("#bCheckPrintPositionlegibility").val() === "True") {
            $("#bCheckPrintPositionlegibilityCheck").prop("checked", true);
        }

        if ($("#bAllergen").val() === "True") {
            $("#bAllergenCheck").prop("checked", true);
        }

        if ($("#bCheckLabelStart").val() === "True") {
            $("#bCheckLabelStartCheck").prop("checked", true);
        }

        if ($("#bCheckLabelDuring").val() === "True") {
            $("#bCheckLabelDuringCheck").prop("checked", true);
        }

        if ($("#bCheckLabelIfChange").val() === "True") {
            $("#bCheckLabelIfChangeCheck").prop("checked", true);
        }

        if ($("#bCheckLabelEnd").val() === "True") {
            $("#bCheckLabelEndCheck").prop("checked", true);
        }

        $("#ProductName").val($("#vProductName").val());
        $("#ProductWeight").val($("#iProductWeight").val());

        var packersArray = $("#packersList").val().split(",");
        dropDown = document.getElementById("packersDropdown");
        values = [];

        for (i = 0; i < dropDown.options.length; i++) {
            if (packersArray.includes(dropDown.options[i].value)) {
                values.push(dropDown.options[i].value);
                $("[data-original-index=" + i + "]").addClass("selected");
                $("[tabindex=" + i + "]").attr("aria-selected", true);
                $("[data-id=packersDropdown]").attr("title", values);
                $("[data-id=packersDropdown]").removeClass("bs-placeholder");
            }
        }
        $("#packersDropdown").val(values).change();

        //Critical Checks
        if ($("#vMealStartTempCriticalCheck").val() === "True") {
            $("#vMealStartTemp").addClass("error");
        }
        if ($("#vMealFinishTempCriticalCheck").val() === "True") {
            $("#vMealFinishTemp").addClass("error");
        }
        if ($("#vSauceStartTempCriticalCheck").val() === "True") {
            $("#vSauceStartTemp").addClass("error");
        }
        if ($("#vSauceFinishTempCriticalCheck").val() === "True") {
            $("#vSauceFinishTemp").addClass("error");
        }
        if ($("#vVeg_RiceStartTempCriticalCheck").val() === "True") {
            $("#vVeg_RiceStartTemp").addClass("error");
        }
        if ($("#vVeg_RiceFinishTempCriticalCheck").val() === "True") {
            $("#vVeg_RiceFinishTemp").addClass("error");
        }
    }
}
//#endregion

//#region Function to update the check fields.
function updateChecks() {
    var check = $("#bEquipVisualHygieneCheck").prop("checked");
    $("#bEquipVisualHygieneChk").val(check);
    check = $("#bOldPackaging_LabelsClearedCheck").prop("checked");
    $("#bOldPackaging_LabelsCleared").val(check);
    check = $("#bCheckDuringCheck").prop("checked");
    $("#bCheckDuring").val(check);
    check = $("#bCheckStartCheck").prop("checked");
    $("#bCheckStart").val(check);
    check = $("#bCheckIfChangeCheck").prop("checked");
    $("#bCheckIfChange").val(check);
    check = $("#bCheckEndCheck").prop("checked");
    $("#bCheckEnd").val(check);
    check = $("#bAllergenCheck").prop("checked");
    $("#bAllergen").val(check);
    check = $("#bCheckLabelStartCheck").prop("checked");
    $("#bCheckLabelStart").val(check);
    check = $("#bCheckLabelDuringCheck").prop("checked");
    $("#bCheckLabelDuring").val(check);
    check = $("#bCheckLabelIfChangeCheck").prop("checked");
    $("#bCheckLabelIfChange").val(check);
    check = $("#bCheckLabelEndCheck").prop("checked");
    $("#bCheckLabelEnd").val(check);
}
//#endregion

//#region Get DateTime For "Best Before End"
function getDateTimeForBestBefore(date) {

    var day, month, year;

    //If the format has changed in update mode.
    if (date[2] == '/' || date[1] == '/') {
        month = date.split("/")[0];
        day = date.split("/")[1];
        year = date.split("/")[2];
    }
    else {
        day = date.split("-")[0];
        month = date.split("-")[1];
        year = date.split("-")[2];
    }

    return year + "-" + month + "-" + day;
}
//#endregion

//#region Function Cancel Form
function cancelPackingRecForm() {
    showLoader();
    window.location = PACKINGRECORDSLIST;
}
//#endregion

//#region Get Time Difference
function getTimeDifference() {
    var startTime = moment($("#tStartTimePicker").val(), 'HH:mm:ss');
    var finishTime = moment($("#tFinishTimePicker").val(), 'HH:mm:ss');

    var timeDuration = moment.duration(finishTime.diff(startTime));
    var timeHours = parseInt(timeDuration.asHours());
    var timeMinutes = parseInt(timeDuration.asMinutes()) - timeHours * 60;

    if (isNaN(timeHours) || isNaN(timeMinutes)) {
        return false;
    }
    if ((timeHours <= 0 && timeMinutes <= 0) && startTime != "00:00:00" && finishTime != "00:00:00") {
        showInfo("Finish Time should be greater than Start time.");
        return false;
    } else {
        return true;
    }
}
//#endregion