﻿//#region Document Ready function
$(document).ready(function () {
    $(".editable").click(function (e) {
        var tag = e.target.nodeName;
        if (tag === 'IMG') { }
        else {
            var id = $($(e.target).parent()).data('id');
            editPackingRecord(id);
        }
    });
    isCookingPlanExist('canCreatePlan', false);
});
//#endregion

//#region Create Packing Record
$("#packingRecordModal").click(function () {
    var canCreatePlan = $('#canCreatePlan').val().toLowerCase();
    if (canCreatePlan == 'true') {
        showLoader();
        window.location = NEWPACKINGRECORD;
    }
    else
        showError(cookingPlanNotFoundMsg);
});
//#endregion

//#region Edit Packing Record
function editPackingRecord(idParam) {
    showLoader();
    location.href = "EditPackingRecord/" + idParam;
};
//#endregion