﻿//#region Document Ready function
$(document).ready(function () {
    $(".editable").click(function (e) {
        var tag = e.target.nodeName;
        if (tag === 'IMG') { }
        else {
            var id = $($(e.target).parent()).data('id');
            editMetalDetectionAndPackWeightRecord(id);
        }
    });
    isCookingPlanExist('canCreatePlan', false);
});
//#endregion

//#region Create Metal Detection And Pack Weight Record
$("#metalDetectionAndPackRecordModal").click(function () {
    var canCreatePlan = $('#canCreatePlan').val().toLowerCase();
    if (canCreatePlan == 'true') {
        showLoader();
        window.location = CREATEMETALDETECTIONANDPACKWEIGHTRECORD;
    }
    else
        showError(cookingPlanNotFoundMsg);
});
//#endregion

//#region Edit Metal Detection And Pack Weight Record
function editMetalDetectionAndPackWeightRecord(idParam) {
    showLoader();
    location.href = "EditMetalDetectionAndPackWeightRecord/" + idParam;
};
//#endregion