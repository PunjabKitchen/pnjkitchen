﻿//#region Document Ready function
$(document).ready(function () {
    $(".editable").click(function (e) {
        var tag = e.target.nodeName;
        if (tag === 'IMG') { }
        else {
            var id = $($(e.target).parent()).data('id');
            editPureeMealBatchAssemblyRecord(id);
        }
    });
    isCookingPlanExist('canCreatePlan', false);
});
//#endregion

//#region Create Puree Meal Batch Assembly Record
$("#createPureeMealBatchAssemblyRec").click(function () {
    var canCreatePlan = $('#canCreatePlan').val().toLowerCase();
    if (canCreatePlan == 'true') {
        showLoader();
        window.location = NEWPUREEMEALBATCHASSEMBLYRECORD;
    }
    else
        showError(cookingPlanNotFoundMsg);
});
//#endregion

//#region Edit Puree Meal Batch Assembly Record
function editPureeMealBatchAssemblyRecord(idParam) {
    showLoader();
    location.href = "EditPureeMealBatchAssemblyRecord/" + idParam;
};
//#endregion