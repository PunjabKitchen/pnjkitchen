﻿//#region Document Ready
$(document).ready(function () {
    showLoader();
    //Triggering the Product Code's autocomplete property initially to bring it in running condition.
    initialAutocompleteRequest();

    initializeDatePicker();
    NumberValidations(); 

    $("#pureeMealBatchAssemblyForm").validate().settings.ignore = "";
    
    if ($("#iPureeMeanBatchAssemblyId").val() > 0) {
        updateChecksForEdit();
    } else {
        $("#iPureeMeanBatchAssemblyId").val(0);
        $("#lastBatchNo").val("");
        $("#lastPackersList").val("");
        $("#iNumberOfPacksInBlastFreezer").val(0);
        $("#iNumberOfPacksLabel").text(0);
        $("#numberOfPacksList").val("");
    }

    $(".selectpicker").selectpicker({
    });
});
//#endregion

//#region Product Code's Autocomplete Request
function initialAutocompleteRequest() {
    $.post(GETBATCHBYCODE_URL,
            {
                code: "0"
            },
            function (data) {
                hideLoader();
                if (data.length > 0) {
                    var arrayList = [];
                    _.each(data, function (item) {
                        arrayList.push({
                            "description": item.description == null ? "" : item.description,
                            "value": item.code == null ? "" : item.code,
                            "id": item.id == null ? "" : item.id,
                            "label": item.code == null ? "" : item.code,
                            "code": item.code == null ? "" : item.code,
                            "name": item.name == null ? "" : item.name,
                            "weight": item.weight == null ? "" : item.weight
                        });
                    });
                    $("#vBactchNo").autocomplete({
                        dataType: "json",
                        source: arrayList,
                        select: function (event, ui) {
                            var batchCode = ui.item.value;
                            $("#vBactchNo").val(batchCode);
                            $("#CookingPlanId").val(ui.item.id);
                            $("#ProductName").val(ui.item.name);
                        }
                    });
                }
            });
}
//#endregion

//#region Show Records List
function showRecordsList(mealComponentName) {
    var idParam = $("#iPureeMeanBatchAssemblyId").val();
    showLoader();
    var url = SHOWPUREEMEALDETAILSLIST;
    $.get(url, { iPureeMeanBatchAssemblyId: idParam, mealComponent: mealComponentName }, function (data) {
        $("#recordsList").html(data);
        $("#recordsListModal").modal("show");
        hideLoader();
    });
};
//#endregion

//#region Number fields validation
function NumberValidations() {
    $("#vShapeTemp").keypress(function (evt) {
        if (!(evt.which === 45 || (evt.which >= 48 && evt.which <= 57))) {
            evt.preventDefault();
        }
    });
    $("#vSauceTemp").keypress(function (evt) {
        if (!(evt.which === 45 || (evt.which >= 48 && evt.which <= 57))) {
            evt.preventDefault();
        }
    });
    $("#vMashTemp").keypress(function (evt) {
        if (!(evt.which === 45 || (evt.which >= 48 && evt.which <= 57))) {
            evt.preventDefault();
        }
    });
}
//#endregion

//#region Number Of Packs Validation
function numberOfPacksValidation(value) {
    $("#numberofPacksInput_" + value).keypress(function (evt) {
        var self = $(this);
        self.val(self.val().replace(/[^\d].+/, ""));
        if ((evt.which < 48 || evt.which > 57) && (evt.which !== 8) && (evt.keyCode !== 46) && (evt.keyCode !== 37) && (evt.keyCode !== 39)) {
            evt.preventDefault();
        }
    });
}
//#endregion

//#region DatePicker Initializer for Last Pack into Blast Freezer
function lastPackTimePicker(value) {
    $("#lastPackInput_" + value).datetimepicker({
        format: "HH:mm"
    });
}
//#endregion

//#region vShapeTemp input event
$("#vShapeTemp").bind("keyup mouseup", function () {
    if ($("#vShapeTemp").val() > -15) {
        $("#vShapeTemp").addClass("error");
    } else {
        $("#vShapeTemp").removeClass("error");
    }
});
//#endregion

//#region Initialize Date Picker
function initializeDatePicker() {
    $("#dDatePicker").datetimepicker({
        format: "DD-MM-YYYY",
        minDate: moment()
    });

    $("#dTimePicker").datetimepicker({
        format: "HH:mm"
    });

    $("#tSauceTimeOfTransferFromPrepTimePicker").datetimepicker({
        format: "HH:mm"
    });

    $("#tMashTimeOfTransferFromPrepTimePicker").datetimepicker({
        format: "HH:mm"
    });
}
//#endregion

//#region Batch Code Auto Complete
$("#vBactchNo").on("keyup", function (e) {
    var code = (e.keyCode ? e.keyCode : e.which);
    if (code === 13) { //Enter keycode
        return;
    }
    $("#vBactchNo").addClass("spinner");
    $("#ProductName").val("");
    getBatchInfoForPuree($(this).val());
});
//#endregion

//#region Get Batch Info
function getBatchInfoForPuree(text) {
    var array = new Array();
    if (text.length > 0) {
        $.post(GETBATCHBYCODE_URL,
            {
                code: text
            },
            function (data) {
                $("#vBactchNo").removeClass("spinner");
                if (data.length > 0) {
                    var arrayList = [];
                    _.each(data, function (item) {
                        arrayList.push({
                            "description": item.description == null ? "" : item.description,
                            "value": item.code == null ? "" : item.code,
                            "id": item.id == null ? "" : item.id,
                            "label": item.code == null ? "" : item.code,
                            "code": item.code == null ? "" : item.code,
                            "name": item.name == null ? "" : item.name,
                            "weight": item.weight == null ? "" : item.weight
                        });
                    });
                    $("#vBactchNo").autocomplete({
                        dataType: "json",
                        source: arrayList,
                        select: function (event, ui) {
                            var batchCode = ui.item.value;
                            $("#vBactchNo").val(batchCode);
                            $("#CookingPlanId").val(ui.item.id);
                            $("#ProductName").val(ui.item.name);
                        }
                    });
                }
            });
    } else {
        hideLoader();
    }
    return array;
}
//#endregion

//#region Function Submit Form
function submitPureeMealBatchForm() {
     
    if (!productDetailsValidations()) {
        return;
    }

    var dDate = $("#dDatePicker").val();
    $("#dDate").val(getDateTime(dDate));

    updateChecks();
    $("#packersList").val($("#packersDropdown").val().join());
    $("#vProductName").val($("#ProductName").val());

    showLoader();

    $.ajax({
        type: "GET",
        url: SUBMITPUREEMEALBATCHASSEMBLYRECORD,
        data: $("#pureeMealBatchAssemblyForm").serialize(),
        success: function (result) {
            showSuccess("Puree Meal Batch Assembly record saved.");
            window.location = PUREEMEALBATCHASSEMBLYRECORDSLIST;
        },
        error: function (error) {
            hideLoader();
        }
    });
}
//#endregion

//#region Function Tabs Events
function tab2Event() {
    if (!productDetailsValidations()) {
        return;
    }
    
    $("#tab_2").removeClass("active");
    $("#profile").removeClass("active");

    try {
        $("#packersList").val($("#packersDropdown").val().join());
    } catch (err) {
        $("#packersList").val("");
    }
    
    if ((($("#vBactchNo").val() !== $("#lastBatchNo").val()) || ($("#packersList").val() !== $("#lastPackersList").val())) && ($("#vBactchNo").val() !== "") && ($("#packersList").val() !== "")) {
        masterTableSaving("Tab_2");
    } else {
        tab2Switch();
        productDetailsValidations();
    }
}
function tab3Event() {
    if (!productDetailsValidations()) {
        return;
    }

    $("#tab_3").removeClass("active");
    $("#messages").removeClass("active");

    try {
        $("#packersList").val($("#packersDropdown").val().join());
    } catch (err) {
        $("#packersList").val("");
    }

    if ((($("#vBactchNo").val() !== $("#lastBatchNo").val()) || ($("#packersList").val() !== $("#lastPackersList").val())) && ($("#vBactchNo").val() !== "") && ($("#packersList").val() !== "")) {
        masterTableSaving("Tab_3");
    } else {
        tab3Switch();
        productDetailsValidations();
    }
}
//#endregion

//#region Function Tabs Switch
function tab2Switch() {
    if ($("#iPureeMeanBatchAssemblyId").val() > 0) {
        document.getElementById("tab_2Link").setAttribute("data-toggle", "tab");
        $("#tab_1").removeClass("active");
        $("#home").removeClass("active");
        $("#tab_3").removeClass("active");
        $("#messages").removeClass("active");
        $("#tab_2").addClass("active");
        $("#profile").addClass("active");
        return;
    }
    $("#tab_1").addClass("active");
    $("#home").addClass("active");
    $("#tab_2").removeClass("active");
    $("#profile").removeClass("active");
    $("#tab_3").removeClass("active");
    $("#messages").removeClass("active");
}
function tab3Switch() {
    if ($("#iPureeMeanBatchAssemblyId").val() > 0) {
        document.getElementById("tab_3Link").setAttribute("data-toggle", "tab");
        $("#tab_1").removeClass("active");
        $("#home").removeClass("active");
        $("#tab_2").removeClass("active");
        $("#profile").removeClass("active");
        $("#tab_3").addClass("active");
        $("#messages").addClass("active");
        return;
    }
    $("#tab_1").addClass("active");
    $("#home").addClass("active");
    $("#tab_2").removeClass("active");
    $("#profile").removeClass("active");
    $("#tab_3").removeClass("active");
    $("#messages").removeClass("active");
}
//#endregion

//#region Function Master Table Saving
function masterTableSaving(tab) {
    if (!productDetailsValidations()) {
        return;
    }

    $("#tab_1").addClass("active");
    $("#home").addClass("active");

    var dDate = $("#dDatePicker").val();
    $("#dDate").val(getDateTime(dDate));

    updateChecks();
    $("#packersList").val($("#packersDropdown").val().join());
    $("#vProductName").val($("#ProductName").val());

    showLoader();

    $.ajax({
        type: "GET",
        url: SUBMITPUREEMEALBATCHASSEMBLYRECORD,
        data: $("#pureeMealBatchAssemblyForm").serialize(),
        success: function (result) {
            hideLoader();
            if (result.savedRecordID > 0) {
                $("#iPureeMeanBatchAssemblyId").val(result.savedRecordID);
                showSuccess("Product Details saved. Fill the rest of the tabs now.");
                $("#lastBatchNo").val($("#vBactchNo").val());
                $("#lastPackersList").val($("#packersDropdown").val().join());
            } else {
                $("#iPureeMeanBatchAssemblyId").val(0);
            }

            if (tab === "Tab_2") {
                tab2Switch();
            } else if (tab === "Tab_3") {
                tab3Switch();
            }
        },
        error: function (error) {
            hideLoader();
        }
    });
}
//#endregion

//#region Product Details Validations
function productDetailsValidations() {
    if ((!$("#vBactchNo").valid()) || ($("#vBactchNo").val()==="") || (!$("#packersDropdown").valid())) {
        $("#vBactchNo").valid();
        $("#packersDropdown").valid();
        showInfo("Fill the Product Detail tab first.");
        return false;
    }

    if ($("#ProductName").val() === "") {
        showError("Please enter a valid Batch Number.");
        $("#vBatchNo").val("");
        $("#ProductName").val("");
        return false;
    }

    return true;
}
//#endregion

//#region Function Submit Shape Detail Form
function submitShapeDetailForm() {

    showLoader();

    $("#dTime").val($("#dTimePicker").val());
    CriticalChecks();

    $.ajax({
        type: "GET",
        url: SUBMITPUREEMEALBATCHASSEMBLYSHAPERECORD,
        data: $("#pureeMealBatchAssemblyForm").serialize(),
        success: function (result) {

            if (result.status === "Success") {
                showSuccess("Shape's Detail saved.");
            } else {
                showError("Saving operation failed.");
            }
            $("#vShape").val("");
            $("#vShapeBatchNo").val("");
            $("#vShapeTemp").val("");
            $("#dTimePicker").val("");
            $("#vShapeTemp").removeClass("error");
            hideLoader();
        },
        error: function (error) {
            hideLoader();
        }
    });
}
//#endregion

//#region Function Submit Sauce Detail Form
function submitSauceDetailForm() {

    showLoader();

    $("#tSauceTimeOfTransferFromPrep").val($("#tSauceTimeOfTransferFromPrepTimePicker").val());

    $.ajax({
        type: "GET",
        url: SUBMITPUREEMEALBATCHASSEMBLYSAUCERECORD,
        data: $("#pureeMealBatchAssemblyForm").serialize(),
        success: function (result) {

            if (result.status === "Success") {
                showSuccess("Sauce's Detail saved.");
            } else {
                showError("Saving operation failed.");
            }
            $("#vSauce").val("");
            $("#vSauceBatchNo").val("");
            $("#vSauceTemp").val("");
            $("#tSauceTimeOfTransferFromPrepTimePicker").val("");
            hideLoader();
        },
        error: function (error) {
            hideLoader();
        }
    });
}
//#endregion

//#region Function Submit Mash Detail Form
function submitMashDetailForm() {

    showLoader();

    $("#tMashTimeOfTransferFromPrep").val($("#tMashTimeOfTransferFromPrepTimePicker").val());

    $.ajax({
        type: "GET",
        url: SUBMITPUREEMEALBATCHASSEMBLYMASHRECORD,
        data: $("#pureeMealBatchAssemblyForm").serialize(),
        success: function (result) {

            if (result.status === "Success") {
                showSuccess("Mash's Detail saved.");
                $("#latestMealInBlastId").val(result.savedRecord);
                insertItemInList();
            } else {
                showError("Saving operation failed.");
            }
            $("#vMash").val("");
            $("#vMashBatchNo").val("");
            $("#vMashTemp").val("");
            $("#tMashTimeOfTransferFromPrepTimePicker").val("");
            hideLoader();
        },
        error: function (error) {
            hideLoader();
        }
    });
}
//#endregion

//#region Function to set Critical Checks
function CriticalChecks() {
    var vShapeTemp = parseInt($("#vShapeTemp").val());
    if (vShapeTemp > -15) {
        $("#bCriticalCheck").val(true);
    } else {
        $("#bCriticalCheck").val(false);
    }
}
//#endregion

//#region Function to update the check fields in Edit mode.
function updateChecksForEdit() {
    if ($("#iPureeMeanBatchAssemblyId").val() > 0) {

        var dDate = $("#dDate").val();
        dDate = dDate.split(' ')[0];
        $("#dDatePicker").val(dDate);

        if ($("#bProductReleased").val() === "True") {
            $("#bProductReleasedCheck").prop("checked", true);
        }
        if ($("#bProductMeatsQA").val() === "True") {
            $("#bProductMeatsQACheck").prop("checked", true);
        }
        if ($("#bProductMeatsPackingQuality").val() === "True") {
            $("#bProductMeatsPackingQualityCheck").prop("checked", true);
        }
        if ($("#bProductMeatsSpecs").val() === "True") {
            $("#bProductMeatsSpecsCheck").prop("checked", true);
        }

        $("#ProductName").val($("#vProductName").val());

        var packersArray = $("#packersList").val().split(",");
        var dropDown = document.getElementById("packersDropdown");
        var values = [];

        for (var i = 0; i < dropDown.options.length; i++) {
            if (packersArray.includes(dropDown.options[i].value)) {
                values.push(dropDown.options[i].value);
                $("[data-original-index=" + i + "]").addClass("selected");
                $("[tabindex=" + i + "]").attr("aria-selected", true);
                $("[data-id=packersDropdown]").attr("title", values);
                $("[data-id=packersDropdown]").removeClass("bs-placeholder");
            }
        }
        $("#packersDropdown").val(values).change();

        var table = document.getElementById('blastFreezerTable');
        var id;
        for (var row = 1; row < (table.rows.length - 1) ; row++) {
            id = table.rows[row].cells[1].children[0].id;
            numberOfPacksValidation(id.substring(14, id.length));
            lastPackTimePicker(id.substring(14, id.length));
        }

        $("#iNumberOfPacksLabel").text($("#iNumberOfPacksInBlastFreezer").val());
        updateNumberofPacks();
    }
}
//#endregion

//#region Function to update the check fields.
function updateChecks() {
    var check = $("#bProductReleasedCheck").prop("checked");
    $("#bProductReleased").val(check);
    check = $("#bProductMeatsQACheck").prop("checked");
    $("#bProductMeatsQA").val(check);
    check = $("#bProductMeatsPackingQualityCheck").prop("checked");
    $("#bProductMeatsPackingQuality").val(check);
    check = $("#bProductMeatsSpecsCheck").prop("checked");
    $("#bProductMeatsSpecs").val(check);
}
//#endregion

//#region Get DateTime
function getDateTime(date) {

    var day, month, year;

    //If the format has changed in update mode.
    if (date[2] == '/' || date[1] == '/') {
        month = date.split("/")[0];
        day = date.split("/")[1];
        year = date.split("/")[2];
    }
    else {
        day = date.split("-")[0];
        month = date.split("-")[1];
        year = date.split("-")[2];
    }

    return year + "-" + month + "-" + day;
}
//#endregion

//#region Function Cancel Form
function cancelPureeMealBatchForm() {
    showLoader();
    window.location = PUREEMEALBATCHASSEMBLYRECORDSLIST;
}
//#endregion

//#region Insert Items in the Portions List
function insertItemInList() {
    $("#blastFreezerTable").find("tbody").append(
      "<tr data-id=" + $("#latestMealInBlastId").val() + ">" +
        "<td>" + $("#tMashTimeOfTransferFromPrepTimePicker").val() + "</td>" +
        "<td> <input onfocusout='saveLastPackIntoBlastFreezer(" + $("#latestMealInBlastId").val() + ")' value='" + $("#tMashTimeOfTransferFromPrepTimePicker").val() + "' type='text' class='cursor' id='lastPackInput_" + $("#latestMealInBlastId").val() + "'> </td>" +
        "<td> <input onfocusout='saveEditedTableData(" + $("#latestMealInBlastId").val() + ")' value='0' type='number' id='numberofPacksInput_" + $("#latestMealInBlastId").val() + "'> </td>" +
        "<td>" + "<span class='ico-cross cursor' onclick='deleteFreezerRecord(" + $("#latestMealInBlastId").val() + ")'></span>" + "</td>" +
      "</tr>");

    numberOfPacksValidation($("#latestMealInBlastId").val());
    lastPackTimePicker($("#latestMealInBlastId").val());
}
//#endregion

//#region Save Edited Number of Packs
function saveEditedTableData(id) {
    var obj = {
        iPureeMealInBlastFreezerId: id,
        iNumberOfPacks: $("#numberofPacksInput_" + id).val()
    }

    $.ajax({
        type: "GET",
        url: EDITNUMBEROFPACKS,
        data: obj,
        success: function (result) {          
            showSuccess("Number of Packs Edited.");
            updateNumberofPacks();
            hideLoader();
        },
        error: function (error) {
            hideLoader();
        }
    });
}
//#endregion

//#region Save Edited Last Pack into Blast Freezer
function saveLastPackIntoBlastFreezer(id) {
    var obj = {
        iPureeMealInBlastFreezerId: id,
        tLastPackTimeInFreezer: $("#lastPackInput_" + id).val()
    }

    $.ajax({
        type: "GET",
        url: EDITLASTPACKINTOFREEZER,
        data: obj,
        success: function (result) {
            showSuccess("Time of Last Pack into Blast Freezer Edited.");
            hideLoader();
        },
        error: function (error) {
            hideLoader();
        }
    });
}
//#endregion

//#region Delete Blast Freezer Item from the Listview
function deleteFreezerRecord(id) {

    showLoader();

    var obj = {
        iPureeMealInBlastFreezerId: id
    }

    $.ajax({
        type: "GET",
        url: DELETEFREEZERRECORD,
        data: obj,
        success: function (result) {

            if (result.deletedRecordID === -1) {
                hideLoader();
                showError("Deletion Failed.");
                return;
            }
            
            $('#blastFreezerTable tr[data-id="' + id + '"]').remove();
            updateNumberofPacks();
            showSuccess("Blast Freezer item successfully removed.");
            hideLoader();
        },
        error: function (error) {
            hideLoader();
        }
    });
}
//#endregion

//#region Update Total Numberof Packs
function updateNumberofPacks() {
    var table = document.getElementById('blastFreezerTable');
    var numberOfPacks = 0;
    for (var row = 1; row < (table.rows.length - 1) ; row++) {
        numberOfPacks += parseInt(table.rows[row].cells[2].children[0].value);
    }

    $("#iNumberOfPacksLabel").text(numberOfPacks);
    $("#iNumberOfPacksInBlastFreezer").val(numberOfPacks);
}
//#endregion