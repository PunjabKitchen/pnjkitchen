﻿//#region Document Ready
$(document).ready(function () {
    showLoader();
    //Triggering the Product Code's autocomplete property initially to bring it in running condition.
    initialAutocompleteRequest();
    initializeDatePicker();
    initializeNumberValidation();
    weightsNumberValidations();
    updateChecksForEdit();
    $("#batchChangeCheckPureeRecordForm").validate().settings.ignore = "";  
});
//#endregion

//#region Product Code's Autocomplete Request
function initialAutocompleteRequest() {
    $.post(GETRECIPEBYCODE_URL,
            {
                code: "0"
            },
            function (data) {
                hideLoader();
                if (data.length > 0) {
                    var arrayList = [];
                    _.each(data, function (item) {
                        arrayList.push({
                            "description": item.description == null ? "" : item.description,
                            "value": item.code == null ? "" : item.code,
                            "id": item.id == null ? "" : item.id,
                            "label": item.code == null ? "" : item.code,
                            "code": item.code == null ? "" : item.code,
                            "name": item.name == null ? "" : item.name
                        });
                    });
                    $("#ProductCode").autocomplete({
                        dataType: "json",
                        source: arrayList,
                        select: function (event, ui) {
                            var prodCode = ui.item.code;
                            $("#ProductCode").val(prodCode);
                            $("#vProductCode").val(prodCode);
                            $("#recipeId").val(ui.item.id);
                            $("#ProductName").val(ui.item.name);
                        }
                    });
                }
            });
}
//#endregion

//#region Number fields validation
function weightsNumberValidations() {
    $("#PackagingSize").keypress(function (evt) {
        var self = $(this);
        self.val(self.val().replace(/[^\d].+/, ""));
        if ((evt.which < 48 || evt.which > 57) && (evt.which !== 8) && (evt.keyCode !== 46) && (evt.keyCode !== 37) && (evt.keyCode !== 39)) {
            evt.preventDefault();
        }
    });
    $("#ProductBatchNo").keypress(function (evt) {
        var self = $(this);
        self.val(self.val().replace(/[^\d].+/, ""));
        if ((evt.which < 48 || evt.which > 57) && (evt.which !== 8) && (evt.keyCode !== 46) && (evt.keyCode !== 37) && (evt.keyCode !== 39)) {
            evt.preventDefault();
        }
    });
    $("#LidFilmBatchNo").keypress(function (evt) {
        var self = $(this);
        self.val(self.val().replace(/[^\d].+/, ""));
        if ((evt.which < 48 || evt.which > 57) && (evt.which !== 8) && (evt.keyCode !== 46) && (evt.keyCode !== 37) && (evt.keyCode !== 39)) {
            evt.preventDefault();
        }
    });
    $("#vProductCode").keypress(function (evt) {
        var self = $(this);
        self.val(self.val().replace(/[^\d].+/, ""));
        if ((evt.which < 48 || evt.which > 57) && (evt.which !== 8) && (evt.keyCode !== 46) && (evt.keyCode !== 37) && (evt.keyCode !== 39)) {
            evt.preventDefault();
        }
    });
    $("#iLabelWeight").keypress(function (evt) {
        var self = $(this);
        self.val(self.val().replace(/[^\d].+/, ""));
        if ((evt.which < 48 || evt.which > 57) && (evt.which !== 8) && (evt.keyCode !== 46) && (evt.keyCode !== 37) && (evt.keyCode !== 39)) {
            evt.preventDefault();
        }
    });
    $("#vLabelBatchNo").keypress(function (evt) {
        var self = $(this);
        self.val(self.val().replace(/[^\d].+/, ""));
        if ((evt.which < 48 || evt.which > 57) && (evt.which !== 8) && (evt.keyCode !== 46) && (evt.keyCode !== 37) && (evt.keyCode !== 39)) {
            evt.preventDefault();
        }
    });
}
//#endregion

//#region Initialize Date Picker
function initializeDatePicker() {
    $("#dLabelBestBeforeEndDatePicker").datetimepicker({
        format: "DD-MM-YYYY",
        minDate: moment()
    });

    //Updating checks for edit mode.
    updateChecksForEdit();
}
//#endregion

//#region Product Code Auto Complete
$("#ProductCode").on("keyup", function (e) {
    var code = (e.keyCode ? e.keyCode : e.which);
    if (code === 13) { //Enter keycode
        return;
    }
    $("#ProductName").val("");
    $("#vProductCode").val("");
    $("#ProductCode").addClass("spinner");
    getRecipeInfo($(this).val());
});
//#endregion

//#region Get Recipe Info
function getRecipeInfo(text) {
    var array = new Array();
    if (text.length > 0) {
        $.post(GETRECIPEBYCODE_URL,
            {
                code: text
            },
            function (data) {
                $("#ProductCode").removeClass("spinner");
                if (data.length > 0) {
                    var arrayList = [];
                    _.each(data, function (item) {
                        arrayList.push({
                            "description": item.description == null ? "" : item.description,
                            "value": item.code == null ? "" : item.code,
                            "id": item.id == null ? "" : item.id,
                            "label": item.code == null ? "" : item.code,
                            "code": item.code == null ? "" : item.code,
                            "name": item.name == null ? "" : item.name
                        });
                    });
                    $("#ProductCode").autocomplete({
                        dataType: "json",
                        source: arrayList,
                        select: function (event, ui) {
                            var prodCode = ui.item.code;
                            $("#ProductCode").val(prodCode);
                            $("#vProductCode").val(prodCode);
                            $("#recipeId").val(ui.item.id);
                            $("#ProductName").val(ui.item.name);
                            //getAllowedQuanity(prodCode);
                        }
                    });
                }
            });
    } else {
        hideLoader();
    }
    return array;
}
//#endregion

//#region Function Submit Form
function submitBatchChangeCheckPureeRecForm() {
    if (!$("#batchChangeCheckPureeRecordForm").valid()) {

        //Switching tabs based on invalid fields.
        if ((!$("#ProductName").valid()) || (!$("#PackagingType").valid()) || (!$("#PackagingSize").valid()) || (!$("#ProductBatchNo").valid()) || (!$("#LidFilmBatchNo").valid())) {
            if (!$("#tab_1").hasClass("active")) {
                if ($("#tab_2").hasClass("active")) {
                    $("#tab_2").removeClass("active");
                    $("#profile").removeClass("active");
                }
                $("#tab_1").addClass("active");
                $("#home").addClass("active");
            }
        } else {
            if (!$("#tab_2").hasClass("active")) {
                if ($("#tab_1").hasClass("active")) {
                    $("#tab_1").removeClass("active");
                    $("#home").removeClass("active");
                }
                $("#tab_2").addClass("active");
                $("#profile").addClass("active");
            }
        }

        showError("Fill all the required fields in all the tabs first.");
        return;
    }

    var bestBeforeEndDate = $("#dLabelBestBeforeEndDatePicker").val();
    $("#dLabelBestBeforeEnd").val(getDateTimeForBestBefore(bestBeforeEndDate));

    //Checking Batch Code
    if ($("#ProductName").val() === "") {
        showError("Please enter a valid Product code.");
        $("#ProductCode").val("");
        return;
    }

    showLoader();
    updateChecks();

    $.ajax({
        type: "GET",
        url: SUBMITBATCHCHANGECHECKPUREERECORD,
        data: $("#batchChangeCheckPureeRecordForm").serialize(),
        success: function (result) {
            $("#newBatchChangeCheckPureeRecModal").modal("hide");
            hideLoader();
            window.location = BATCHCHANGECHECKPUREERECORDSLIST;
        },
        error: function (error) {
            hideLoader();
        }
    });
}
//#endregion

//#region Function to update the check fields in Edit mode.
function updateChecksForEdit() {
    if ($("#iBatchChangeChkPureeRecrdId").val() > 0) {

        var bestBeforeEndDate = $("#dLabelBestBeforeEnd").val();
        bestBeforeEndDate = bestBeforeEndDate.split(' ')[0];
        $("#dLabelBestBeforeEndDatePicker").val(bestBeforeEndDate);

        if ($("#bEquipVisualHygenChk").val() === "True") {
            $("#bEquipVisualHygenCheck").prop("checked", true);
        }

        if ($("#bOldPackagesLablesCleared").val() === "True") {
            $("#bOldPackagesLablesClearedCheck").prop("checked", true);
        }

        if ($("#bCheckStart").val() === "True") {
            $("#bCheckStartCheck").prop("checked", true);
        }

        if ($("#bCheckDuring").val() === "True") {
            $("#bCheckDuringCheck").prop("checked", true);
        }

        if ($("#bCheckIfChange").val() === "True") {
            $("#bCheckIfChangeCheck").prop("checked", true);
        }

        if ($("#bCheckEnd").val() === "True") {
            $("#bCheckEndCheck").prop("checked", true);
        }

        if ($("#bCheckPrintPositionlegibility").val() === "True") {
            $("#bCheckPrintPositionlegibilityCheck").prop("checked", true);
        }

        if ($("#bAllergin").val() === "True") {
            $("#bAllerginCheck").prop("checked", true);
        }

        if ($("#bCheckLabelStart").val() === "True") {
            $("#bCheckLabelStartCheck").prop("checked", true);
        }

        if ($("#bCheckLabelDuring").val() === "True") {
            $("#bCheckLabelDuringCheck").prop("checked", true);
        }

        if ($("#bCheckLabelIfChange").val() === "True") {
            $("#bCheckLabelIfChangeCheck").prop("checked", true);
        }

        if ($("#bCheckLabelEnd").val() === "True") {
            $("#bCheckLabelEndCheck").prop("checked", true);
        }
    }
}
//#endregion

//#region Function to update the check fields.
function updateChecks() {
    var check = $("#bEquipVisualHygenCheck").prop("checked");
    $("#bEquipVisualHygenChk").val(check);
    check = $("#bOldPackagesLablesClearedCheck").prop("checked");
    $("#bOldPackagesLablesCleared").val(check);
    check = $("#bCheckStartCheck").prop("checked");
    $("#bCheckStart").val(check);
    check = $("#bCheckDuringCheck").prop("checked");
    $("#bCheckDuring").val(check);
    check = $("#bCheckIfChangeCheck").prop("checked");
    $("#bCheckIfChange").val(check);
    check = $("#bCheckEndCheck").prop("checked");
    $("#bCheckEnd").val(check);
    check = $("#bCheckPrintPositionlegibilityCheck").prop("checked");
    $("#bCheckPrintPositionlegibility").val(check);
    check = $("#bAllerginCheck").prop("checked");
    $("#bAllergin").val(check);
    check = $("#bCheckLabelStartCheck").prop("checked");
    $("#bCheckLabelStart").val(check);
    check = $("#bCheckLabelDuringCheck").prop("checked");
    $("#bCheckLabelDuring").val(check);
    check = $("#bCheckLabelIfChangeCheck").prop("checked");
    $("#bCheckLabelIfChange").val(check);
    check = $("#bCheckLabelEndCheck").prop("checked");
    $("#bCheckLabelEnd").val(check);
}
//#endregion

//#region Get DateTime For "Best Before End"
function getDateTimeForBestBefore(date) {

    var day, month, year;

    //If the format has changed in update mode.
    if (date[2] == '/' || date[1] == '/') {
        month = date.split("/")[0];
        day = date.split("/")[1];
        year = date.split("/")[2];
    }
    else {
        day = date.split("-")[0];
        month = date.split("-")[1];
        year = date.split("-")[2];
    }

    return year + "-" + month + "-" + day;
}
//#endregion

//#region Function Cancel Form
function cancelBatchChangeCheckPureeRecForm() {
    showLoader();
    window.location = BATCHCHANGECHECKPUREERECORDSLIST;
}
//#endregion