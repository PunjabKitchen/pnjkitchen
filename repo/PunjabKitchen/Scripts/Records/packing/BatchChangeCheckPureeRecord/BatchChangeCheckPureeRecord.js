﻿//#region Document Ready function
$(document).ready(function () {
    $(".editable").click(function (e) {
        var tag = e.target.nodeName;
        if (tag === 'IMG') { }
        else {
            var id = $($(e.target).parent()).data('id');
            editBatchChangeCheckPureeRecord(id);
        }
    });
    isCookingPlanExist('canCreatePlan', false);
});
//#endregion

//#region Create Batch Change Check Puree Record
$("#batchChangeCheckPureeRecordModal").click(function () {
    var canCreatePlan = $('#canCreatePlan').val().toLowerCase();
    if (canCreatePlan == 'true') {
        showLoader();
        window.location = CREATEBATCHCHANGECHECKPUREERECORD;
    }
    else
        showError(cookingPlanNotFoundMsg);
});
//#endregion

//#region Edit Batch Change Check Puree Record
function editBatchChangeCheckPureeRecord(idParam) {
    showLoader();
    location.href = "EditBatchChangeCheckPureePackingRecord/" + idParam;
};
//#endregion