﻿//#region doucment ready
$(document).ready(function () {
    $(".editable").click(function (e) {
        var tag = e.target.nodeName;
        if (tag === 'IMG') { }
        else {
            var id = $($(e.target).parent()).data('id');
            HotPackingRecEdit(id);
        }
    });
    isCookingPlanExist('canCreatePlan', false);
});
//#endregion

//#region Create hot packing
$('#hotPackingRecordModal').click(function () {
    var canCreatePlan = $('#canCreatePlan').val().toLowerCase();
    if (canCreatePlan == 'true') {
        showLoader();
        var url = CREATEHOTPACKINGRECORD_URL;
        $.get(url, function (data) {
            $('#newHotPackingRec').html(data);
            $('#newHotPackingRecModal').modal('show');
            hideLoader();
        });
    }
    else
        showError(cookingPlanNotFoundMsg);
});
//#endregion

//#region Edit hot packing Record
function HotPackingRecEdit(idParam) {
    var url = HOTPACKINGRECORDEDIT_URL;
    $.get(url, { id: idParam }, function (data) {
        $("#newHotPackingRec").html(data);
        $("#newHotPackingRecModal").modal("show");
    });
}
//#endregion