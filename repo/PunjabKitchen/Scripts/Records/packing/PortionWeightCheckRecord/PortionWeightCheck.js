﻿//#region Document Ready function
$(document).ready(function () {
    $(".editable").click(function (e) {
        var tag = e.target.nodeName;
        if (tag === 'IMG') { }
        else {
            var id = $($(e.target).parent()).data('id');
            editPortionWeightCheckRecord(id);
        }
    });
    isCookingPlanExist('canCreatePlan', false);
});
//#endregion

//#region Create cold dessert packing
$("#createPortionWeightCheckRec").click(function () {
    var canCreatePlan = $('#canCreatePlan').val().toLowerCase();
    if (canCreatePlan == 'true') {
        showLoader();
        var url = NEWPORTIONWEIGHTCHECKSRECORD;
        $.get(url, function (data) {
            $("#portionWeightCheckRecModalBody").html(data);
            $("#portionWeightCheckRecModal").modal("show");
            hideLoader();
        });
    }
    else
        showError(cookingPlanNotFoundMsg);
});
//#endregion

//#region Edit Portion Weight Check Record
function editPortionWeightCheckRecord(idParam) {
    showLoader();
    var url = EDITPORTIONCHECKWEIGHTRECORD;
    $.get(url, { id: idParam }, function (data) {
        $("#portionWeightCheckRecModalBody").html(data);
        $("#portionWeightCheckRecModal").modal("show");
        hideLoader();
    });
};
//#endregion