﻿//#region Document Ready
$(document).ready(function () {
    initializeNumberValidation();

    showLoader();
    //Triggering the Product Code's autocomplete property initially to bring it in running condition.
    initialAutocompleteRequest();

    //In Edit Mode
    if ($("#iPortionWeightCheckId").val() > 0) {
        $("#ProductWeight").val($("#iProductWeight").val());
        $("#nextButton").hide();
        $("#submitDoneButton").hide();
        $(".picklist-area").hide();
        $("#vLocation").attr("disabled", "disabled");
        $("#vBatchNo").attr("disabled", "disabled");
        $("#ProductWeight").attr("disabled", "disabled");
    } else {
        $("#editDoneButton").hide();
    }
});
//#endregion

//#region Product Code's Autocomplete Request
function initialAutocompleteRequest() {
    $.post(GETBATCHBYCODE_URL,
            {
                code: "0"
            },
            function (data) {
                hideLoader();
                if (data.length > 0) {
                    var arrayList = [];
                    _.each(data, function (item) {
                        arrayList.push({
                            "description": item.description == null ? "" : item.description,
                            "value": item.code == null ? "" : item.code,
                            "id": item.id == null ? "" : item.id,
                            "label": item.code == null ? "" : item.code,
                            "code": item.code == null ? "" : item.code,
                            "name": item.name == null ? "" : item.name,
                            "weight": item.weight == null ? "" : item.weight
                        });
                    });
                    $("#vBatchNo").autocomplete({
                        dataType: "json",
                        source: arrayList,
                        select: function (event, ui) {
                            var batchCode = ui.item.value;
                            $("#BatchNo").val(batchCode);
                            $("#ProductDescription").val(ui.item.name);
                            $("#CookingPlanId").val(ui.item.id);
                            $("#ProductWeight").val(ui.item.weight);
                        }
                    });
                }
            });
}
//#endregion

//#region "Product Weight" field validation
$("#iProductWeight").keypress(function (evt) {
    var self = $(this);
    self.val(self.val().replace(/[^\d].+/, ""));
    if ((evt.which < 48 || evt.which > 57) && (evt.which !== 8) && (evt.keyCode !== 46) && (evt.keyCode !== 37) && (evt.keyCode !== 39)) {
        evt.preventDefault();
    }
});
//#endregion

//#region "Portion Weight" field validation
$("#iPortionWeight").keypress(function (evt) {
    var self = $(this);
    self.val(self.val().replace(/[^\d].+/, ""));
    if ((evt.which < 48 || evt.which > 57) && (evt.which !== 8) && (evt.keyCode !== 46) && (evt.keyCode !== 37) && (evt.keyCode !== 39)) {
        evt.preventDefault();
    }
});
//#endregion

//#region "Portion Check Weight" field validation
$("#iPortionCheckWeight").keypress(function (evt) {
    var self = $(this);
    self.val(self.val().replace(/[^\d].+/, ""));
    if ((evt.which < 48 || evt.which > 57) && (evt.which !== 8) && (evt.keyCode !== 46) && (evt.keyCode !== 37) && (evt.keyCode !== 39)) {
        evt.preventDefault();
    }
});
//#endregion

//#region Batch Code Auto Complete
$("#vBatchNo").on("keyup", function (e) {
    var code = (e.keyCode ? e.keyCode : e.which);
    if (code === 13) { //Enter keycode
        return;
    }
    $("#ProductDescription").val("");
    $("#ProductWeight").val("");
    $("#vBatchNo").addClass("spinner");
    getBatchInfo($(this).val());
});
//#endregion

//#region Get Recipe Info from its Batch
function getBatchInfo(text) {
    var array = new Array();
    if (text.length > 0) {
        $.post(GETBATCHBYCODE_URL,
            {
                code: text
            },
            function (data) {
                $("#vBatchNo").removeClass("spinner");
                if (data.length > 0) {
                    var arrayList = [];
                    _.each(data, function (item) {
                        arrayList.push({
                            "description": item.description == null ? "" : item.description,
                            "value": item.code == null ? "" : item.code,
                            "id": item.id == null ? "" : item.id,
                            "label": item.code == null ? "" : item.code,
                            "code": item.code == null ? "" : item.code,
                            "name": item.name == null ? "" : item.name,
                            "weight": item.weight == null ? "" : item.weight
                        });
                     });
                    $("#vBatchNo").autocomplete({
                        dataType: "json",
                        source: arrayList,
                        select: function (event, ui) {
                            var batchCode = ui.item.value;
                            $("#BatchNo").val(batchCode);
                            $("#ProductDescription").val(ui.item.name);
                            $("#CookingPlanId").val(ui.item.id);
                            $("#ProductWeight").val(ui.item.weight);
                        }
                    });
                }
            });
    } else {
        hideLoader();
    }
    return array;
}
//#endregion

//#region Function Submit Edited Form
function submitPortionCheckWeightRecForm() {
    if (!$("#portionWeightCheckForm").valid()) {
        return;
    }

    //Checking Batch Code
    if ($("#ProductDescription").val() === "") {
        showError("Please enter valid batch code");
        $("#vBatchNo").val("");
        return;
    }


    showLoader();

    $("#iProductWeight").val($("#ProductWeight").val());

    $.ajax({
        type: "GET",
        url: SUBMITPORTIONCHECKWEIGHTRECORD,
        data: $("#portionWeightCheckForm").serialize(),
        success: function (result) {
            showSuccess("Portion record saved successfully.");
            insertItemInList();
            $("#portionWeightCheckRecModal").modal("hide");
            hideLoader();
            window.location = PORTIONWEIGHTCHECKRECORDSLIST;
        },
        error: function (error) {
            hideLoader();
        }
    });
}
//#endregion

//#region Function Submit Completed Form
function submitCompletedForm() {
    showLoader();
    window.location = PORTIONWEIGHTCHECKRECORDSLIST;
}
//#endregion

//#region Function "Add" Click Event
function nextPortionCheckWeightRecForm() {
    if (!$("#portionWeightCheckForm").valid()) {
        return;
    }

    //Checking Batch Code
    if ($("#ProductDescription").val() === "") {
        showError("Please enter a valid batch code.");
        $("#vBatchNo").val("");
        return;
    }

    showLoader();

    $("#iProductWeight").val($("#ProductWeight").val());

    $.ajax({
        type: "GET",
        url: SUBMITPORTIONCHECKWEIGHTRECORD,
        data: $("#portionWeightCheckForm").serialize(),
        success: function (result) {
            if (result.status == "Success") {
                insertItemInList();
                $("#iPortionWeightCheckId").val(result.savedRecordID);
                $("#vLocation").attr("disabled", "disabled");
                $("#vBatchNo").attr("disabled", "disabled");
                $("#iProductWeight").attr("disabled", "disabled");
                $("#vPortionName").val("");
                $("#iPortionWeight").val("");
                $("#iPortionCheckWeight").val("");
                $("#nextButton").text("Add Next");
                showSuccess("Portion record saved successfully. Add next.");
            }
            hideLoader();
        },
        error: function (error) {
            hideLoader();
        }
    });
}
//#endregion

//#region Function Cancel Form
function cancelBatchChangeCheckPureeRecForm() {
    window.location = PORTIONWEIGHTCHECKRECORDSLIST;
}
//#endregion

//#region Insert Items in the Portions List
function insertItemInList() {
    $("#portionsTable").find("tbody").append(
      "<tr>" +
        "<td>" + $("#vPortionName").val() + "</td>" +
        "<td>" + $("#iPortionWeight").val() + "</td>" +
        "<td>" + $("#iPortionCheckWeight").val() + "</td>" +
      "</tr>");
}
//#endregion