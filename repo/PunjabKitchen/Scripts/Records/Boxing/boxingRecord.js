﻿//#region Document Ready
$(document).ready(function () {
    hideLoader();
    $(".boxingItem").click(function (e) {
        var tag = e.target.nodeName;
        if (tag === 'IMG') { }
        else {
            var idParam = $($(e.target).parent()).data('id');
            createBoxingrecord(idParam);
        }
    });
});
//#endregion

//Create Boxing Record
function createBoxingrecord(id) {
    showLoader();
    var url = id == undefined ? CREATEBOXINGRECORD_URL : CREATEBOXINGRECORD_URL + "/" + id;
    window.location = url;
}