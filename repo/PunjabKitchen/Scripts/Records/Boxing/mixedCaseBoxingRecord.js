﻿//#region Document Ready
$(document).ready(function () {
    hideLoader();

    $(".boxingItem").click(function (e) {
        debugger;
        var tag = e.target.nodeName;
        if (tag === 'IMG') { }
        else {
            var id = $($(e.target).parent()).data('id')
            editMixedBoxingRecord(id);
        }
    });
});
//#endregion

//#region Create Mixed Case Boxing Record
function createMixedCaseBoxingRecord() {
    showLoader();
    var url = CREATEMIXEDCASEBOXINGRECORD_URL;
    $.get(url, function (data) {
        $("#newMixedCaseBoxingRec").html(data);
        $("#newMixedCaseBoxingRecModal").modal('show');
        hideLoader();
    });
}
//#endregion

//#region Edit Mixed Boxing Record
function editMixedBoxingRecord(idParam) {
    showLoader();
    var url = EDITMIXEDBOXINGRECORD;
    $.get(url, { id: idParam }, function (data) {
        $("#newMixedCaseBoxingRec").html(data);
        $("#newMixedCaseBoxingRecModal").modal("show");
        hideLoader();
    });
    editCheck = false;
}
//#endregion