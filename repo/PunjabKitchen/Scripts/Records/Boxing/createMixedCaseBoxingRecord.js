﻿//#region Document Ready
$(document).ready(function () {
    //Initialize Number Validation
    NumberValidation();
    //initialize datepicker
    initializeDateTimePicker();
    //Update Check Boxes
    updateCheckBoxes();

    //#region Batch Code Auto Complete
    $("#vBatchCode").on("keyup", function (e) {
        var code = (e.keyCode ? e.keyCode : e.which);
        if (code === 13) { //Enter keycode
            return;
        }
        $("#ProductName").val("");
        $("#iProductId").val("");
        $("#vNameOfPack").val("");
        $("#vNumber").val("");
        $("#vBatchCode").addClass("spinner");
        getRecipeInfo($(this).val());
    });
    //#endregion

    if ($("#iMixedCaseBoxingRecordId").val() > 0) {
        $("#detailFields").hide();
        $("#detailButtonsRow").hide();
        $("#listTableRow").hide();
        $("#tableHeading").hide();
    }

    showLoader();
    //Triggering the Product Code's autocomplete property initially to bring it in running condition.
    initialAutocompleteRequest();
});
//#endregion

//#region Number Validations
function NumberValidation() {
    $("#iNoOfCases").keypress(function (evt) {
        var self = $(this);
        self.val(self.val().replace(/[^\d].+/, ""));
        if ((evt.which < 48 || evt.which > 57) && (evt.which !== 8) && (evt.keyCode !== 46) && (evt.keyCode !== 37) && (evt.keyCode !== 39)) {
            evt.preventDefault();
        }

        if ((this.value.length === 6) && (evt.which !== 8) && (evt.keyCode !== 46) && (evt.keyCode !== 37) && (evt.keyCode !== 39)) {
            return false;
        }
        return true;
    });
    $("#iCaseNo").keypress(function (evt) {
        var self = $(this);
        self.val(self.val().replace(/[^\d].+/, ""));
        if ((evt.which < 48 || evt.which > 57) && (evt.which !== 8) && (evt.keyCode !== 46) && (evt.keyCode !== 37) && (evt.keyCode !== 39)) {
            evt.preventDefault();
        }

        if ((this.value.length === 6) && (evt.which !== 8) && (evt.keyCode !== 46) && (evt.keyCode !== 37) && (evt.keyCode !== 39)) {
            return false;
        }
        return true;
    });
    $("#vBatchNo").keypress(function (evt) {
        var self = $(this);
        self.val(self.val().replace(/[^\d].+/, ""));
        if ((evt.which < 48 || evt.which > 57) && (evt.which !== 8) && (evt.keyCode !== 46) && (evt.keyCode !== 37) && (evt.keyCode !== 39)) {
            evt.preventDefault();
        }

        if ((this.value.length === 6) && (evt.which !== 8) && (evt.keyCode !== 46) && (evt.keyCode !== 37) && (evt.keyCode !== 39)) {
            return false;
        }
        return true;
    });
    $("#vBatchCode").keypress(function (evt) {
        var self = $(this);
        self.val(self.val().replace(/[^\d].+/, ""));
        if ((evt.which < 48 || evt.which > 57) && (evt.which !== 8) && (evt.keyCode !== 46) && (evt.keyCode !== 37) && (evt.keyCode !== 39)) {
            evt.preventDefault();
        }

        if ((this.value.length === 6) && (evt.which !== 8) && (evt.keyCode !== 46) && (evt.keyCode !== 37) && (evt.keyCode !== 39)) {
            return false;
        }
        return true;
    });
}
//#endregion

//#region Product Code's Autocomplete Request
function initialAutocompleteRequest() {
    $.post(
            GETBYCODE_URL,
            {
                code: "0"
            },
            function (data) {
                hideLoader();
                if (data.length > 0) {
                    var arrayList = [];
                    _.each(data, function (item) {
                        arrayList.push({
                            "description": item.description == null ? "" : item.description,
                            "value": item.code == null ? "" : item.code,
                            "id": item.id == null ? "" : item.id,
                            "label": item.code == null ? "" : item.code,
                            "code": item.code == null ? "" : item.code,
                            "name": item.name == null ? "" : item.name,
                            "recipeCode": item.recipeCode == null ? "" : item.recipeCode,
                            "bestBefore": item.bestBefore == null ? "" : item.bestBefore
                        });
                    });
                    $("#vBatchCode").autocomplete({
                        dataType: "json",
                        source: arrayList,
                        select: function (event, ui) {
                            $("#vNumber").val(ui.item.recipeCode);
                            $("#vNameOfPack").val(ui.item.name);
                            $("#vBatchCode").val(ui.item.batchCode);
                            $('#dProductBestBefore').val(ui.item.bestBefore);
                        }
                    });
                }
            });
}
//#endregion

//#region Date and Time Pickers Region
function initializeDateTimePicker() {

    $("#dDateCaseMadeDatePicker").datetimepicker({
        format: "DD-MM-YYYY",
        minDate: moment()
    });
    $("#dDayOfBuildDatePicker").datetimepicker({
        format: "DD-MM-YYYY",
        minDate: moment()
    });
    $("#vDateCaseGoingOutDatePicker").datetimepicker({
        format: "DD-MM-YYYY",
        minDate: moment()
    });
    $("#dLabelBestBeforeDatePicker").datetimepicker({
        format: "DD-MM-YYYY",
        minDate: moment()
    });

    if ($("#iMixedCaseBoxingRecordId").val() > 0) {
        document.getElementById("doneButton").innerText = "Save";
        $("#dDateCaseMadeDatePicker").val(moment($("#dDateCaseMade").val()).format("DD-MM-YYYY"));
        $("#dDayOfBuildDatePicker").val(moment($("#dDayOfBuild").val()).format("DD-MM-YYYY"));
        $("#vDateCaseGoingOutDatePicker").val(moment($("#vDateCaseGoingOut").val()).format("DD-MM-YYYY"));
        $("#dLabelBestBeforeDatePicker").val(moment($("#dLabelBestBefore").val()).format("DD-MM-YYYY"));
    }
}

//Update Time Picker On Submit
function updateDateTimePickerOnSubmit() {
    $("#dDateCaseMade").val(getDateTimeForBoxing($("#dDateCaseMadeDatePicker").val()));
    $("#dDayOfBuild").val(getDateTimeForBoxing($("#dDayOfBuildDatePicker").val()));
    $("#vDateCaseGoingOut").val(getDateTimeForBoxing($("#vDateCaseGoingOutDatePicker").val()));
    $("#dLabelBestBefore").val(getDateTimeForBoxing($("#dLabelBestBeforeDatePicker").val()));
}

//Calculate best before date
function calculateBestBeforeDate() {
    var besBeforeDates = [];
    $('#listTable').find('.dBestBefore').each(function (index, element) {
        var rawData = $(element).val().split('-');
        besBeforeDates.push(new Date(rawData[0], rawData[1] - 1, rawData[2]));
    });
    var minDate = new Date(Math.min.apply(null, besBeforeDates));
    $('#dLabelBestBeforeDatePicker').val(moment(minDate).format('DD-MM-YYYY'));
}
//#endregion 

//#region Check Boxes Region

//update Check Boxes
function updateCheckBoxes() {
    $('#bLabelCheckedCheck_hidden').val().toLowerCase() == "true" ? $('#bLabelChecked').prop('checked', true) : $('#bLabelChecked').prop('checked', false);
    $('#bCopyOnBackCheck_hidden').val().toLowerCase() == "true" ? $('#bCopyOnBack').prop('checked', true) : $('#bCopyOnBack').prop('checked', false);
}

//Update Check Boxes on Submit
function updateCheckBoxesonSubmit() {
    $('#bLabelChecked').prop('checked') ? $('#bLabelCheckedCheck_hidden').val(true) : $('#bLabelCheckedCheck_hidden').val(false);
    $('#bCopyOnBack').prop('checked') ? $('#bCopyOnBackCheck_hidden').val(true) : $('#bCopyOnBackCheck_hidden').val(false);
}

//#endregion

//#region Formatted Date
function getDateTimeForBoxing(date) {

    var datePart = date.split(" ")[0];

    var day = datePart.split("-")[0];
    var month = datePart.split("-")[1];
    var year = datePart.split("-")[2];

    return year + "-" + month + "-" + day;
}
//#endregion

//#region Submit Form
function submitForm(e) {
    if (!$("#mixedCaseBoxingRecForm").valid()) {
        return false;
    }

    updateDateTimePickerOnSubmit();
    updateCheckBoxesonSubmit();
    showLoader();

    if ((e != undefined) && (e.innerText === "Add")) {
        if (!insertItemInList()) {
            hideLoader();
            return;
        }
    }

    if (!moment($("#dProductBestBefore").val()).isValid()) {
        $("#dProductBestBefore").val(moment().format("YYYY-MM-DD"));
    }

    $.ajax({
        type: "POST",
        url: SUBMITMIXEDVASEBOXINGRECORD_URL,
        data: $("#mixedCaseBoxingRecForm").serialize(),
        success: function (data) {
            showSuccess("Boxing record saved successfully");
            $("#iMixedCaseBoxingRecordId").val(data.result);
            hideLoader();
            $("#vNumber, #vNameOfPack, #vBatchCode").val("");
            if ((e.innerText === "Done") || (e.innerText === "Save")) {
                window.location = MIXEDCASEBOXINGRECORDLIST;
            }
        },
        error: function () {
            showError("Something went wrong while saving record");
            hideLoader();
        }
    });
}
//#endregion

//#region Insert Items in the Portions List
function insertItemInList() {
    if ($("#vNameOfPack").val() === "") {
        showError("Enter a valid Batch Code.");
        return false;
    }

    if ($("#vNumber").val() != undefined && $("#vNumber").val() != '') {
        $("#listTable").find("tbody").append(
          "<tr>" +
            "<td>" + $("#vNumber").val() + "</td>" +
            "<td>" + $("#vNameOfPack").val() + "</td>" +
            "<td>" + $("#vBatchCode").val() + "<input type='hidden' value='" + $("#dProductBestBefore").val() + "' class='dBestBefore'></td>" +
          "</tr>");

        calculateBestBeforeDate();
        return true;
    }
}
//#endregion

//#region Get Recipe Info
function getRecipeInfo(text) {
    var array = new Array();
    if (text.length > 0) {
        $.post(
            GETBYCODE_URL,
            {
                code: text
            },
            function (data) {
                $("#vBatchCode").removeClass("spinner");
                if (data.length > 0) {
                    var arrayList = [];
                    _.each(data, function (item) {
                        arrayList.push({
                            "description": item.description == null ? "" : item.description,
                            "value": item.code == null ? "" : item.code,
                            "id": item.id == null ? "" : item.id,
                            "label": item.code == null ? "" : item.code,
                            "code": item.code == null ? "" : item.code,
                            "name": item.name == null ? "" : item.name,
                            "recipeCode": item.recipeCode == null ? "" : item.recipeCode,
                            "bestBefore": item.bestBefore == null ? "" : item.bestBefore
                        });
                    });
                    $("#vBatchCode").autocomplete({
                        dataType: "json",
                        source: arrayList,
                        select: function (event, ui) {
                            $("#vNumber").val(ui.item.recipeCode);
                            $("#vNameOfPack").val(ui.item.name);
                            $("#vBatchCode").val(ui.item.batchCode);
                            $('#dProductBestBefore').val(ui.item.bestBefore);
                        }
                    });
                }
            });
    } else {
        hideLoader();
    }
    return array;
}
//#endregion