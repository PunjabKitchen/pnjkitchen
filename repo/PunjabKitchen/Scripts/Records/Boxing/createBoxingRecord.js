﻿//#region Document Ready Function
$(document).ready(function () {

    //Initialize Number Validation
    NumberValidation();
    //initialize datepicker
    initializeDateTimePicker();
    //Update Check Boxes
    updateCheckBoxes();

    //Get Batch Code
    if ($("#iProductId").val() === "") {
        $("#vBatchNo").attr("readonly", "readonly");
        getUniqueCode("vBatchNo", 4);
    }

    $("#vBatchNo").keypress(function (evt) {
        var self = $(this);
        self.val(self.val().replace(/[^\d].+/, ""));
        if ((evt.which < 48 || evt.which > 57) && (evt.which !== 8) && (evt.keyCode !== 46) && (evt.keyCode !== 37) && (evt.keyCode !== 39)) {
            evt.preventDefault();
        }
    });

    //#region Product Code Auto Complete
    $("#ProductCode").on("keyup", function (e) {
        var code = (e.keyCode ? e.keyCode : e.which);
        if (code === 13) { //Enter keycode
            return;
        }
        $("#ProductName").val("");
        $("#iProductId").val("");
        $("#ProductCode").addClass("spinner");
        getRecipeInfo($(this).val());
    });
    //#endregion

    //Triggering the Product Code's autocomplete property initially to bring it in running condition.
    initialAutocompleteRequest();
});
//#endregion

//#region Number Validations
function NumberValidation() {
    $("#iQuatity").keypress(function (evt) {
        var self = $(this);
        self.val(self.val().replace(/[^\d].+/, ""));
        if ((evt.which < 48 || evt.which > 57) && (evt.which !== 8) && (evt.keyCode !== 46) && (evt.keyCode !== 37) && (evt.keyCode !== 39)) {
            evt.preventDefault();
        }

        if ((this.value.length === 6) && (evt.which !== 8) && (evt.keyCode !== 46) && (evt.keyCode !== 37) && (evt.keyCode !== 39)) {
            return false;
        }
        return true;
    });
    $("#vBoxBatchCode").keypress(function (evt) {
        var self = $(this);
        self.val(self.val().replace(/[^\d].+/, ""));
        if ((evt.which < 48 || evt.which > 57) && (evt.which !== 8) && (evt.keyCode !== 46) && (evt.keyCode !== 37) && (evt.keyCode !== 39)) {
            evt.preventDefault();
        }

        if ((this.value.length === 6) && (evt.which !== 8) && (evt.keyCode !== 46) && (evt.keyCode !== 37) && (evt.keyCode !== 39)) {
            return false;
        }
        return true;
    });
    $("#vProductTemp").keypress(function (evt) {
        if ((evt.which < 48 || evt.which > 57) && (evt.which !== 8) && (evt.which !== 45) && (evt.which !== 46) && (evt.keyCode !== 46) && (evt.keyCode !== 37) && (evt.keyCode !== 39)) {
            evt.preventDefault();
        }

        if ((this.value.length === 6) && (evt.which !== 8) && (evt.which !== 45) && (evt.which !== 46) && (evt.keyCode !== 46) && (evt.keyCode !== 37) && (evt.keyCode !== 39)) {
            return false;
        }
        return true;
    });
    $("#iTotalCases_Trays").keypress(function (evt) {
        var self = $(this);
        self.val(self.val().replace(/[^\d].+/, ""));
        if ((evt.which < 48 || evt.which > 57) && (evt.which !== 8) && (evt.keyCode !== 46) && (evt.keyCode !== 37) && (evt.keyCode !== 39)) {
            evt.preventDefault();
        }

        if ((this.value.length === 6) && (evt.which !== 8) && (evt.keyCode !== 46) && (evt.keyCode !== 37) && (evt.keyCode !== 39)) {
            return false;
        }
        return true;
    });
}
//#endregion

//#region Product Code's Autocomplete Request
function initialAutocompleteRequest() {
    $.post(
            GETBYCODE_URL,
            {
                code: "0"
            },
            function (data) {
                if (data.length > 0) {
                    var arrayList = [];
                    _.each(data, function (item) {
                        arrayList.push({
                            "description": item.description == null ? "" : item.description,
                            "value": item.code == null ? "" : item.code,
                            "id": item.id == null ? "" : item.id,
                            "label": item.code == null ? "" : item.code,
                            "code": item.code == null ? "" : item.code,
                            "name": item.name == null ? "" : item.name
                        });
                    });
                    $("#ProductCode").autocomplete({
                        dataType: "json",
                        source: arrayList,
                        select: function (event, ui) {
                            var batchCode = ui.item.batchCode;
                            $("#ProductCode").val(batchCode);
                            $("#iCookingPlanId").val(ui.item.id);
                            $("#ProductName").val(ui.item.name);
                        }
                    });
                }
            });
}
//#endregion

//#region Submit Form
function acceptForm() {
    if (!$("#BoxingRecordForm").valid()) {
        return;
    }

    //Checking Product Code
    if ($("#ProductName").val() === "") {
        showError("Please enter valid product code");
        $("#ProductCode").val("");
        return;
    }

    //Checking if start and end time are not valid
    if (!getTimeDifference()) {
        showInfo("Finish Time should be greater then start time");
        return;
    }

    updateTimePickerOnSubmit();
    updateCheckBoxesonSubmit();

    showLoader();
    $.ajax({
        type: "POST",
        url: SUBMITBOXINGRECORD_URL,
        data: $("#BoxingRecordForm").serialize(),
        success: function () {
            showSuccess("Boxing record saved successfully");
            $("#newBoxingRecModal").modal("hide");
            hideLoader();
            window.location = BOXINGRECORDLIST;
        },
        error: function () {
            showError("Something went wrong while saving record");
            hideLoader();
        }
    });
}
//#endregion

//#region Date and Time Pickers Region
function initializeDateTimePicker() {

    $("#dBestBeforeDatePicker").datetimepicker({
        format: "DD-MM-YYYY",
        minDate: moment()
    });
    $("#tStartTimeTimePicker").datetimepicker({
        format: "HH:mm"
    });
    $("#tFinishTimeTimePicker").datetimepicker({
        format: "HH:mm"
    });

    if ($("#iBoxingRecordId").val() > 0) {
        $("#dBestBeforeDatePicker").val(moment($("#dBestBefore").val()).format("DD-MM-YYYY"));
        $("#tStartTimeTimePicker").val(moment($("#tStartTime").val()).format("HH:mm"));
        $("#tFinishTimeTimePicker").val(moment($("#tFinishTime").val()).format("HH:mm"));
    }
}

//Update Time Picker On Submit
function updateTimePickerOnSubmit() {
    $("#dBestBefore").val(getDateTimeForBoxing($("#dBestBeforeDatePicker").val()));
    $("#tStartTime").val($("#tStartTimeTimePicker").val());
    $("#tFinishTime").val($("#tFinishTimeTimePicker").val());
}

$("#tStartTimeTimePicker").on("dp.change", function () {
    getTimeDifference();
});

$("#tFinishTimeTimePicker").on("dp.change", function () {
    getTimeDifference();
});

//#endregion 

//#region Check Boxes Region

//update Check Boxes
function updateCheckBoxes() {
    $('#bEquipVisualHygieneCheck_hidden').val().toLowerCase() == "true" ? $('#bEquipVisualHygiene').prop('checked', true) : $('#bEquipVisualHygiene').prop('checked', false);
    $('#bOldBoxesAndLabelsClearedCheck_hidden').val().toLowerCase() == "true" ? $('#bOldBoxesAndLabelsCleared').prop('checked', true) : $('#bOldBoxesAndLabelsCleared').prop('checked', false);
    $('#bBoxLabelProductCodeCheck_hidden').val().toLowerCase() == "true" ? $('#bBoxLabelProductCode').prop('checked', true) : $('#bBoxLabelProductCode').prop('checked', false);
    $('#bBoxLabelBatchNumberCheck_hidden').val().toLowerCase() == "true" ? $('#bBoxLabelBatchNumber').prop('checked', true) : $('#bBoxLabelBatchNumber').prop('checked', false);
    $('#bBoxLabelBestBeforeCheck_hidden').val().toLowerCase() == "true" ? $('#bBoxLabelBestBefore').prop('checked', true) : $('#bBoxLabelBestBefore').prop('checked', false);
    $('#bBoxLabelQuatityCheck_hidden').val().toLowerCase() == "true" ? $('#bBoxLabelQuatity').prop('checked', true) : $('#bBoxLabelQuatity').prop('checked', false);
    $('#bBoxTypeAndBatchNoCheck_hidden').val().toLowerCase() == "true" ? $('#bBoxTypeAndBatchNo').prop('checked', true) : $('#bBoxTypeAndBatchNo').prop('checked', false);
    $('#bChecksAtStartCheck_hidden').val().toLowerCase() == "true" ? $('#bChecksAtStart').prop('checked', true) : $('#bChecksAtStart').prop('checked', false);
    $('#bChecksDuringCheck_hidden').val().toLowerCase() == "true" ? $('#bChecksDuring').prop('checked', true) : $('#bChecksDuring').prop('checked', false);
    $('#bChecksIfChangeCheck_hidden').val().toLowerCase() == "true" ? $('#bChecksIfChange').prop('checked', true) : $('#bChecksIfChange').prop('checked', false);
    $('#bChecksAtEndCheck_hidden').val().toLowerCase() == "true" ? $('#bChecksAtEnd').prop('checked', true) : $('#bChecksAtEnd').prop('checked', false);
}

//Update Check Boxes on Submit
function updateCheckBoxesonSubmit() {
    $('#bEquipVisualHygiene').prop('checked') ? $('#bEquipVisualHygieneCheck_hidden').val(true) : $('#bEquipVisualHygieneCheck_hidden').val(false);
    $('#bOldBoxesAndLabelsCleared').prop('checked') ? $('#bOldBoxesAndLabelsClearedCheck_hidden').val(true) : $('#bOldBoxesAndLabelsClearedCheck_hidden').val(false);
    $('#bBoxLabelProductCode').prop('checked') ? $('#bBoxLabelProductCodeCheck_hidden').val(true) : $('#bBoxLabelProductCodeCheck_hidden').val(false);
    $('#bBoxLabelBatchNumber').prop('checked') ? $('#bBoxLabelBatchNumberCheck_hidden').val(true) : $('#bBoxLabelBatchNumberCheck_hidden').val(false);
    $('#bBoxLabelBestBefore').prop('checked') ? $('#bBoxLabelBestBeforeCheck_hidden').val(true) : $('#bBoxLabelBestBeforeCheck_hidden').val(false);
    $('#bBoxLabelQuatity').prop('checked') ? $('#bBoxLabelQuatityCheck_hidden').val(true) : $('#bBoxLabelQuatityCheck_hidden').val(false);
    $('#bBoxTypeAndBatchNo').prop('checked') ? $('#bBoxTypeAndBatchNoCheck_hidden').val(true) : $('#bBoxTypeAndBatchNoCheck_hidden').val(false);
    $('#bChecksAtStart').prop('checked') ? $('#bChecksAtStartCheck_hidden').val(true) : $('#bChecksAtStartCheck_hidden').val(false);
    $('#bChecksDuring').prop('checked') ? $('#bChecksDuringCheck_hidden').val(true) : $('#bChecksDuringCheck_hidden').val(false);
    $('#bChecksIfChange').prop('checked') ? $('#bChecksIfChangeCheck_hidden').val(true) : $('#bChecksIfChangeCheck_hidden').val(false);
    $('#bChecksAtEnd').prop('checked') ? $('#bChecksAtEndCheck_hidden').val(true) : $('#bChecksAtEndCheck_hidden').val(false);
}

//#endregion

//#region Get Time For Boxing
function getTimeForBoxing(timeVal) {
    var today = new Date();
    var year = today.getYear();
    var day = today.getDate();
    var month = today.getMonth() + 1;

    var hours = timeVal.split(":")[0];
    var minutes = timeVal.split(":")[1];
    var seconds = timeVal.split(":")[2];

    return new Date(year, month, day, hours, minutes, seconds);
}

function getDateTimeForBoxing(date) {

    var datePart = date.split(" ")[0];

    var day = datePart.split("-")[0];
    var month = datePart.split("-")[1];
    var year = datePart.split("-")[2];

    return year + "-" + month + "-" + day;
}
//#endregion

//#region Get Recipe Info
function getRecipeInfo(text) {
    var array = new Array();
    if (text.length > 0) {
        $.post(
            GETBYCODE_URL,
            {
                code: text
            },
            function (data) {
                $("#ProductCode").removeClass("spinner");
                if (data.length > 0) {
                    var arrayList = [];
                    _.each(data, function (item) {
                        arrayList.push({
                            "description": item.description == null ? "" : item.description,
                            "value": item.code == null ? "" : item.code,
                            "id": item.id == null ? "" : item.id,
                            "label": item.code == null ? "" : item.code,
                            "code": item.code == null ? "" : item.code,
                            "name": item.name == null ? "" : item.name
                        });
                    });
                    $("#ProductCode").autocomplete({
                        dataType: "json",
                        source: arrayList,
                        select: function (event, ui) {
                            var batchCode = ui.item.batchCode;
                            $("#ProductCode").val(batchCode);
                            $("#iCookingPlanId").val(ui.item.id);
                            $("#ProductName").val(ui.item.name);
                        }
                    });
                }
            });
    } else {
        hideLoader();
    }
    return array;
}
//#endregion

//#region Get Time Difference
function getTimeDifference() {
    var startTime = moment($("#tStartTimeTimePicker").val(), 'HH:mm:ss');
    var finishTime = moment($("#tFinishTimeTimePicker").val(), 'HH:mm:ss');

    var timeDuration = moment.duration(finishTime.diff(startTime));
    var timeHours = parseInt(timeDuration.asHours());
    var timeMinutes = parseInt(timeDuration.asMinutes()) - timeHours * 60;

    if (isNaN(timeHours) || isNaN(timeMinutes)) {
        return false;
    }
    if ((timeHours <= 0 && timeMinutes <= 0) && startTime != "00:00:00" && finishTime != "00:00:00") {
        return false;
    } else {
        return true;
    }
}
//#endregion

//#region Cancel button
function cancelBoxingRecForm() {
    showLoader();
    window.location = BOXINGRECORDLIST;
}
//#endregion