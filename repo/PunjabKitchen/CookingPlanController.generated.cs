// <auto-generated />
// This file was generated by a T4 template.
// Don't change it directly as your change would get overwritten.  Instead, make changes
// to the .tt file (i.e. the T4 template) and save it to regenerate this file.

// Make sure the compiler doesn't complain about missing Xml comments and CLS compliance
// 0108: suppress "Foo hides inherited member Foo. Use the new keyword if hiding was intended." when a controller and its abstract parent are both processed
// 0114: suppress "Foo.BarController.Baz()' hides inherited member 'Qux.BarController.Baz()'. To make the current member override that implementation, add the override keyword. Otherwise add the new keyword." when an action (with an argument) overrides an action in a parent controller
#pragma warning disable 1591, 3008, 3009, 0108, 0114
#region T4MVC

using System;
using System.Diagnostics;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using System.Web.Mvc.Html;
using System.Web.Routing;
using T4MVC;
namespace PunjabKitchen.Controllers
{
    public partial class CookingPlanController
    {
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        protected CookingPlanController(Dummy d) { }

        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        protected RedirectToRouteResult RedirectToAction(ActionResult result)
        {
            var callInfo = result.GetT4MVCResult();
            return RedirectToRoute(callInfo.RouteValueDictionary);
        }

        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        protected RedirectToRouteResult RedirectToAction(Task<ActionResult> taskResult)
        {
            return RedirectToAction(taskResult.Result);
        }

        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        protected RedirectToRouteResult RedirectToActionPermanent(ActionResult result)
        {
            var callInfo = result.GetT4MVCResult();
            return RedirectToRoutePermanent(callInfo.RouteValueDictionary);
        }

        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        protected RedirectToRouteResult RedirectToActionPermanent(Task<ActionResult> taskResult)
        {
            return RedirectToActionPermanent(taskResult.Result);
        }

        [NonAction]
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public virtual System.Web.Mvc.ActionResult Index()
        {
            return new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.Index);
        }
        [NonAction]
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public virtual System.Web.Mvc.ActionResult Edit()
        {
            return new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.Edit);
        }
        [NonAction]
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public virtual System.Web.Mvc.ActionResult CancelPlan()
        {
            return new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.CancelPlan);
        }
        [NonAction]
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public virtual System.Web.Mvc.ActionResult Post()
        {
            return new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.Post);
        }
        [NonAction]
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public virtual System.Web.Mvc.JsonResult GetAllowedStockQuanity()
        {
            return new T4MVC_System_Web_Mvc_JsonResult(Area, Name, ActionNames.GetAllowedStockQuanity);
        }
        [NonAction]
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public virtual System.Web.Mvc.JsonResult GetByCode()
        {
            return new T4MVC_System_Web_Mvc_JsonResult(Area, Name, ActionNames.GetByCode);
        }
        [NonAction]
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public virtual System.Web.Mvc.JsonResult GetByCodeForPacking()
        {
            return new T4MVC_System_Web_Mvc_JsonResult(Area, Name, ActionNames.GetByCodeForPacking);
        }
        [NonAction]
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public virtual System.Web.Mvc.JsonResult DeleteCookingRecord()
        {
            return new T4MVC_System_Web_Mvc_JsonResult(Area, Name, ActionNames.DeleteCookingRecord);
        }
        [NonAction]
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public virtual System.Web.Mvc.JsonResult GetTodaysTimeSlotAvailability()
        {
            return new T4MVC_System_Web_Mvc_JsonResult(Area, Name, ActionNames.GetTodaysTimeSlotAvailability);
        }
        [NonAction]
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public virtual System.Web.Mvc.JsonResult CheckFinishingSequence()
        {
            return new T4MVC_System_Web_Mvc_JsonResult(Area, Name, ActionNames.CheckFinishingSequence);
        }
        [NonAction]
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public virtual System.Web.Mvc.JsonResult CheckBatchCodeAvailability()
        {
            return new T4MVC_System_Web_Mvc_JsonResult(Area, Name, ActionNames.CheckBatchCodeAvailability);
        }
        [NonAction]
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public virtual System.Web.Mvc.ActionResult PrintRecord()
        {
            return new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.PrintRecord);
        }

        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public CookingPlanController Actions { get { return MVC.CookingPlan; } }
        [GeneratedCode("T4MVC", "2.0")]
        public readonly string Area = "";
        [GeneratedCode("T4MVC", "2.0")]
        public readonly string Name = "CookingPlan";
        [GeneratedCode("T4MVC", "2.0")]
        public const string NameConst = "CookingPlan";
        [GeneratedCode("T4MVC", "2.0")]
        static readonly ActionNamesClass s_actions = new ActionNamesClass();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionNamesClass ActionNames { get { return s_actions; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionNamesClass
        {
            public readonly string Index = "Index";
            public readonly string Create = "Create";
            public readonly string Edit = "Edit";
            public readonly string CancelPlan = "CancelPlan";
            public readonly string Post = "Post";
            public readonly string GetAllowedStockQuanity = "GetAllowedStockQuanity";
            public readonly string GetByCode = "GetByCode";
            public readonly string GetByCodeForPacking = "GetByCodeForPacking";
            public readonly string DeleteCookingRecord = "DeleteCookingRecord";
            public readonly string GetTodaysTimeSlotAvailability = "GetTodaysTimeSlotAvailability";
            public readonly string CheckFinishingSequence = "CheckFinishingSequence";
            public readonly string IsCookingPlanExists = "IsCookingPlanExists";
            public readonly string CheckBatchCodeAvailability = "CheckBatchCodeAvailability";
            public readonly string PrintRecord = "PrintRecord";
        }

        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionNameConstants
        {
            public const string Index = "Index";
            public const string Create = "Create";
            public const string Edit = "Edit";
            public const string CancelPlan = "CancelPlan";
            public const string Post = "Post";
            public const string GetAllowedStockQuanity = "GetAllowedStockQuanity";
            public const string GetByCode = "GetByCode";
            public const string GetByCodeForPacking = "GetByCodeForPacking";
            public const string DeleteCookingRecord = "DeleteCookingRecord";
            public const string GetTodaysTimeSlotAvailability = "GetTodaysTimeSlotAvailability";
            public const string CheckFinishingSequence = "CheckFinishingSequence";
            public const string IsCookingPlanExists = "IsCookingPlanExists";
            public const string CheckBatchCodeAvailability = "CheckBatchCodeAvailability";
            public const string PrintRecord = "PrintRecord";
        }


        static readonly ActionParamsClass_Index s_params_Index = new ActionParamsClass_Index();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionParamsClass_Index IndexParams { get { return s_params_Index; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionParamsClass_Index
        {
            public readonly string cookpotId = "cookpotId";
            public readonly string page = "page";
        }
        static readonly ActionParamsClass_Edit s_params_Edit = new ActionParamsClass_Edit();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionParamsClass_Edit EditParams { get { return s_params_Edit; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionParamsClass_Edit
        {
            public readonly string id = "id";
        }
        static readonly ActionParamsClass_CancelPlan s_params_CancelPlan = new ActionParamsClass_CancelPlan();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionParamsClass_CancelPlan CancelPlanParams { get { return s_params_CancelPlan; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionParamsClass_CancelPlan
        {
            public readonly string id = "id";
        }
        static readonly ActionParamsClass_Post s_params_Post = new ActionParamsClass_Post();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionParamsClass_Post PostParams { get { return s_params_Post; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionParamsClass_Post
        {
            public readonly string cookingPlan = "cookingPlan";
        }
        static readonly ActionParamsClass_GetAllowedStockQuanity s_params_GetAllowedStockQuanity = new ActionParamsClass_GetAllowedStockQuanity();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionParamsClass_GetAllowedStockQuanity GetAllowedStockQuanityParams { get { return s_params_GetAllowedStockQuanity; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionParamsClass_GetAllowedStockQuanity
        {
            public readonly string productCode = "productCode";
        }
        static readonly ActionParamsClass_GetByCode s_params_GetByCode = new ActionParamsClass_GetByCode();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionParamsClass_GetByCode GetByCodeParams { get { return s_params_GetByCode; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionParamsClass_GetByCode
        {
            public readonly string code = "code";
        }
        static readonly ActionParamsClass_GetByCodeForPacking s_params_GetByCodeForPacking = new ActionParamsClass_GetByCodeForPacking();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionParamsClass_GetByCodeForPacking GetByCodeForPackingParams { get { return s_params_GetByCodeForPacking; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionParamsClass_GetByCodeForPacking
        {
            public readonly string code = "code";
        }
        static readonly ActionParamsClass_DeleteCookingRecord s_params_DeleteCookingRecord = new ActionParamsClass_DeleteCookingRecord();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionParamsClass_DeleteCookingRecord DeleteCookingRecordParams { get { return s_params_DeleteCookingRecord; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionParamsClass_DeleteCookingRecord
        {
            public readonly string iCookingPlanId = "iCookingPlanId";
        }
        static readonly ActionParamsClass_GetTodaysTimeSlotAvailability s_params_GetTodaysTimeSlotAvailability = new ActionParamsClass_GetTodaysTimeSlotAvailability();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionParamsClass_GetTodaysTimeSlotAvailability GetTodaysTimeSlotAvailabilityParams { get { return s_params_GetTodaysTimeSlotAvailability; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionParamsClass_GetTodaysTimeSlotAvailability
        {
            public readonly string iCookPotNo = "iCookPotNo";
            public readonly string startDateTime = "startDateTime";
            public readonly string finishDateTime = "finishDateTime";
        }
        static readonly ActionParamsClass_CheckFinishingSequence s_params_CheckFinishingSequence = new ActionParamsClass_CheckFinishingSequence();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionParamsClass_CheckFinishingSequence CheckFinishingSequenceParams { get { return s_params_CheckFinishingSequence; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionParamsClass_CheckFinishingSequence
        {
            public readonly string iCookingPlanId = "iCookingPlanId";
        }
        static readonly ActionParamsClass_IsCookingPlanExists s_params_IsCookingPlanExists = new ActionParamsClass_IsCookingPlanExists();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionParamsClass_IsCookingPlanExists IsCookingPlanExistsParams { get { return s_params_IsCookingPlanExists; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionParamsClass_IsCookingPlanExists
        {
            public readonly string isPackingConsumptionCheck = "isPackingConsumptionCheck";
        }
        static readonly ActionParamsClass_CheckBatchCodeAvailability s_params_CheckBatchCodeAvailability = new ActionParamsClass_CheckBatchCodeAvailability();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionParamsClass_CheckBatchCodeAvailability CheckBatchCodeAvailabilityParams { get { return s_params_CheckBatchCodeAvailability; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionParamsClass_CheckBatchCodeAvailability
        {
            public readonly string vBatchNo = "vBatchNo";
        }
        static readonly ActionParamsClass_PrintRecord s_params_PrintRecord = new ActionParamsClass_PrintRecord();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionParamsClass_PrintRecord PrintRecordParams { get { return s_params_PrintRecord; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionParamsClass_PrintRecord
        {
            public readonly string pId = "pId";
        }
        static readonly ViewsClass s_views = new ViewsClass();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ViewsClass Views { get { return s_views; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ViewsClass
        {
            static readonly _ViewNamesClass s_ViewNames = new _ViewNamesClass();
            public _ViewNamesClass ViewNames { get { return s_ViewNames; } }
            public class _ViewNamesClass
            {
                public readonly string _Create = "_Create";
                public readonly string Index = "Index";
            }
            public readonly string _Create = "~/Views/CookingPlan/_Create.cshtml";
            public readonly string Index = "~/Views/CookingPlan/Index.cshtml";
        }
    }

    [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
    public partial class T4MVC_CookingPlanController : PunjabKitchen.Controllers.CookingPlanController
    {
        public T4MVC_CookingPlanController() : base(Dummy.Instance) { }

        [NonAction]
        partial void IndexOverride(T4MVC_System_Web_Mvc_ActionResult callInfo, int? cookpotId, int? page);

        [NonAction]
        public override System.Web.Mvc.ActionResult Index(int? cookpotId, int? page)
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.Index);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "cookpotId", cookpotId);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "page", page);
            IndexOverride(callInfo, cookpotId, page);
            return callInfo;
        }

        [NonAction]
        partial void CreateOverride(T4MVC_System_Web_Mvc_ActionResult callInfo);

        [NonAction]
        public override System.Web.Mvc.ActionResult Create()
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.Create);
            CreateOverride(callInfo);
            return callInfo;
        }

        [NonAction]
        partial void EditOverride(T4MVC_System_Web_Mvc_ActionResult callInfo, long id);

        [NonAction]
        public override System.Web.Mvc.ActionResult Edit(long id)
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.Edit);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "id", id);
            EditOverride(callInfo, id);
            return callInfo;
        }

        [NonAction]
        partial void CancelPlanOverride(T4MVC_System_Web_Mvc_ActionResult callInfo, long id);

        [NonAction]
        public override System.Web.Mvc.ActionResult CancelPlan(long id)
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.CancelPlan);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "id", id);
            CancelPlanOverride(callInfo, id);
            return callInfo;
        }

        [NonAction]
        partial void PostOverride(T4MVC_System_Web_Mvc_ActionResult callInfo, PunjabKitchen.Models.CookingPlan cookingPlan);

        [NonAction]
        public override System.Web.Mvc.ActionResult Post(PunjabKitchen.Models.CookingPlan cookingPlan)
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.Post);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "cookingPlan", cookingPlan);
            PostOverride(callInfo, cookingPlan);
            return callInfo;
        }

        [NonAction]
        partial void GetAllowedStockQuanityOverride(T4MVC_System_Web_Mvc_JsonResult callInfo, string productCode);

        [NonAction]
        public override System.Web.Mvc.JsonResult GetAllowedStockQuanity(string productCode)
        {
            var callInfo = new T4MVC_System_Web_Mvc_JsonResult(Area, Name, ActionNames.GetAllowedStockQuanity);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "productCode", productCode);
            GetAllowedStockQuanityOverride(callInfo, productCode);
            return callInfo;
        }

        [NonAction]
        partial void GetByCodeOverride(T4MVC_System_Web_Mvc_JsonResult callInfo, string code);

        [NonAction]
        public override System.Web.Mvc.JsonResult GetByCode(string code)
        {
            var callInfo = new T4MVC_System_Web_Mvc_JsonResult(Area, Name, ActionNames.GetByCode);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "code", code);
            GetByCodeOverride(callInfo, code);
            return callInfo;
        }

        [NonAction]
        partial void GetByCodeForPackingOverride(T4MVC_System_Web_Mvc_JsonResult callInfo, string code);

        [NonAction]
        public override System.Web.Mvc.JsonResult GetByCodeForPacking(string code)
        {
            var callInfo = new T4MVC_System_Web_Mvc_JsonResult(Area, Name, ActionNames.GetByCodeForPacking);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "code", code);
            GetByCodeForPackingOverride(callInfo, code);
            return callInfo;
        }

        [NonAction]
        partial void DeleteCookingRecordOverride(T4MVC_System_Web_Mvc_JsonResult callInfo, int iCookingPlanId);

        [NonAction]
        public override System.Web.Mvc.JsonResult DeleteCookingRecord(int iCookingPlanId)
        {
            var callInfo = new T4MVC_System_Web_Mvc_JsonResult(Area, Name, ActionNames.DeleteCookingRecord);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "iCookingPlanId", iCookingPlanId);
            DeleteCookingRecordOverride(callInfo, iCookingPlanId);
            return callInfo;
        }

        [NonAction]
        partial void GetTodaysTimeSlotAvailabilityOverride(T4MVC_System_Web_Mvc_JsonResult callInfo, int iCookPotNo, System.DateTime startDateTime, System.DateTime finishDateTime);

        [NonAction]
        public override System.Web.Mvc.JsonResult GetTodaysTimeSlotAvailability(int iCookPotNo, System.DateTime startDateTime, System.DateTime finishDateTime)
        {
            var callInfo = new T4MVC_System_Web_Mvc_JsonResult(Area, Name, ActionNames.GetTodaysTimeSlotAvailability);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "iCookPotNo", iCookPotNo);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "startDateTime", startDateTime);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "finishDateTime", finishDateTime);
            GetTodaysTimeSlotAvailabilityOverride(callInfo, iCookPotNo, startDateTime, finishDateTime);
            return callInfo;
        }

        [NonAction]
        partial void CheckFinishingSequenceOverride(T4MVC_System_Web_Mvc_JsonResult callInfo, int iCookingPlanId);

        [NonAction]
        public override System.Web.Mvc.JsonResult CheckFinishingSequence(int iCookingPlanId)
        {
            var callInfo = new T4MVC_System_Web_Mvc_JsonResult(Area, Name, ActionNames.CheckFinishingSequence);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "iCookingPlanId", iCookingPlanId);
            CheckFinishingSequenceOverride(callInfo, iCookingPlanId);
            return callInfo;
        }

        [NonAction]
        partial void IsCookingPlanExistsOverride(T4MVC_System_Web_Mvc_JsonResult callInfo, bool isPackingConsumptionCheck);

        [NonAction]
        public override System.Web.Mvc.JsonResult IsCookingPlanExists(bool isPackingConsumptionCheck)
        {
            var callInfo = new T4MVC_System_Web_Mvc_JsonResult(Area, Name, ActionNames.IsCookingPlanExists);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "isPackingConsumptionCheck", isPackingConsumptionCheck);
            IsCookingPlanExistsOverride(callInfo, isPackingConsumptionCheck);
            return callInfo;
        }

        [NonAction]
        partial void CheckBatchCodeAvailabilityOverride(T4MVC_System_Web_Mvc_JsonResult callInfo, string vBatchNo);

        [NonAction]
        public override System.Web.Mvc.JsonResult CheckBatchCodeAvailability(string vBatchNo)
        {
            var callInfo = new T4MVC_System_Web_Mvc_JsonResult(Area, Name, ActionNames.CheckBatchCodeAvailability);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "vBatchNo", vBatchNo);
            CheckBatchCodeAvailabilityOverride(callInfo, vBatchNo);
            return callInfo;
        }

        [NonAction]
        partial void PrintRecordOverride(T4MVC_System_Web_Mvc_ActionResult callInfo, int pId);

        [NonAction]
        public override System.Web.Mvc.ActionResult PrintRecord(int pId)
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.PrintRecord);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "pId", pId);
            PrintRecordOverride(callInfo, pId);
            return callInfo;
        }

    }
}

#endregion T4MVC
#pragma warning restore 1591, 3008, 3009, 0108, 0114
