﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.ServiceProcess;
using System.Timers;

namespace NotificationsService
{
    public partial class PunjabKitchenService : ServiceBase
    {
        private readonly Timer _timer;

        public PunjabKitchenService()
        {
            InitializeComponent();
            var delayTime = 0;
            using (AppConfig.Change("PKConfigSettings.config"))
            {
                delayTime = Convert.ToInt32(ConfigurationManager.AppSettings["DelayTime"]);
            }
            _timer = new System.Timers.Timer(delayTime);
            _timer.Elapsed += WorkProcess;
            _timer.Interval = delayTime;
            _timer.Enabled = true;

        }

        public void WorkProcess(object sender, ElapsedEventArgs e)
        {
            GenerateNotifications();
        }

        protected override void OnStart(string[] args)
        {
            _timer.Enabled = true;
        }

        protected override void OnStop()
        {
            _timer.Enabled = false;
        }
        
        private void GenerateNotifications()
        {
            string cs;

            using (AppConfig.Change("PKConfigSettings.config"))
            {
                cs = ConfigurationManager.AppSettings["ConnectionString"];
            }
            
            using (SqlConnection conn = new SqlConnection(cs))
            {
                using (var command = new SqlCommand("createNotifications", conn)
                {
                    CommandType = CommandType.StoredProcedure
                })
                {
                    conn.Open();
                    command.ExecuteNonQuery();
                    conn.Close();
                }
            }
        }
        private void LogService(string content)
        {
            FileStream fs = new FileStream(@"d:\TestServiceLog.txt", FileMode.OpenOrCreate, FileAccess.Write);
            StreamWriter sw = new StreamWriter(fs);
            sw.BaseStream.Seek(0, SeekOrigin.End);
            sw.WriteLine(content);
            sw.Flush();
            sw.Close();
        }
    }
}
